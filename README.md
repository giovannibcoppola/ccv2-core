accountsummaryaddon## Setup local development environment 
(:exclamation: this setup step-by-step is applicable only to developers, pls do not follow this step by step if you are setting up a server)

### Using  Git

This project uses the Git-flow workflow (tips [here](https://danielkummer.github.io/git-flow-cheatsheet/))

### Branch naming.

*master* is forbidden to commit to

*develop* branch is the branch that is deployed to the dev environment

For each feature you're working on, create its own branch using: feature/VIK-00-my_feature_name

## Minimal local code setup

* Get git repro and dependecies
	```bash
    git clone git@git.osudio.com:viking/viking_core.git
    git checkout develop
	```

* Get dependecies and compile
	```bash
    ./gradlew
	```

### Configure DB using init
* add  ```initialpassword.admin=nimda``` to your local.properties

* perform init
* remove initialpassword.admin from local.properties

### Configure simple hsqldb
* Remove in hybris/config/local.properties all properties that start with db.*

* Init system
    Start Server (See below)
    Go to https://localhost:9002/hac/login.jsp
    Init the system (WARNING ALL DATA WILL BE LOST)


### Configure MSSQL docker container with dump from other developer
* copy hybris/config/local.properties from other developer 
* replace hard usernames in path in hybris/config/local.properties
    * See tomcat.debugjavaoptions and orbeon.keystore.uri
* Get docker container dump (tar) from other developer
* Get media dump from other developer
* Unzip media folder
	```bash
    unzip media.zip
	```

* Move media folder
   ```bash	
   mv [path to media dump] hybris/data/media/
   ```

* import docker container. Replace [name.tar] with actual tar name. Note down returned dockerId
 	```bash
     docker import ./[name.tar]
     docker tag f842bf799468 mssql.viking:latest
     docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=reallyStrongPwd123' -e 'MSSQL_SA_PASSWORD=reallyStrongPwd123' --restart always --name mssql_viking -p 1433:1433 -d mssql.viking:latest /opt/mssql/bin/sqlservr
     docker exec -it mssql_viking /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'reallyStrongPwd123' -Q "SELECT Name FROM sys.Databases"
     ```
* Add dockerId to autoStart. Replace dockerId
 	```bash
     docker tag [dockerId] mssql.viking:latest
     docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=reallyStrongPwd123' -e 'MSSQL_SA_PASSWORD=reallyStrongPwd123' --restart always --name mssql_viking -p 1433:1433 -d mssql.viking:latest /opt/mssql/bin/sqlservr
     docker exec -it mssql_viking /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'reallyStrongPwd123' -Q "SELECT Name FROM sys.Databases"
     ```

* add to local.properties
    ```
    db.url=jdbc:sqlserver://localhost:1433;databaseName=hybris;responseBuffering=adaptive;loginTimeout=10
    db.driver=com.microsoft.sqlserver.jdbc.SQLServerDriver
    db.username=sa
    db.password=reallyStrongPwd123
     ```  
  
* Reset users f needed
 	```bash
     mssql -u sa -p reallyStrongPwd123
     UPDATE users SET createdTS=GETDATE(),modifiedTS=GETDATE();
     exit;
     ```

### /etc/hosts
* Add the below to your host file
* linux: /etc/hosts
* windows: C:\Windows\System32\drivers\etc\hosts
    ```bash
    127.0.0.1 viking.customer-portal.local
    127.0.0.1 viking.service-portal.local
    127.0.0.1 viking.safetyshop.local  
    ```

### Chmod start files
* run below
 	```bash
    chmod +x hybris/bin/platform/hybrisserver.sh
    chmod +x hybris/bin/platform/tomcat/bin/catalina.sh
    chmod +x hybris/bin/platform/tomcat/bin/wrapper.sh
     ```

### Start Server
* Start Server
 	```bash
     cd hybris/bin/platform/
     . ./setantenv.sh
     ./hybrisserver.sh
     exit;
     ```

### Other
* Fix error message related to Orbeon license
    * update orbeon.keystore.uri=file:/Users/basbolier/Projects/Viking/Code/hybris/bin/platform/tomcat/lib/keystore with your file path.
    * https://confluence.osudio.com/confluence/display/VIK/Local+envrionment+setup
    
* To debug
    * Replace in hybris/config/local.properties
	    * change tomcat.debugjavaoptions to match your local setup 

* IDEA
    * The git project contains intellij setup files   

* Advanced local setup
    * To delete everything and do a fresh install (for upgrades): 
        * see upgradeHybrisVersion.sh
    * To add more test data
        * scripts/setup/    
        * https://confluence.osudio.com/confluence/display/VIK/Local+envrionment+setup

### Local urls
    https://viking.customer-portal.local:9002/customerportal/en/login
    https://viking.service-portal.local:9002/servicepartnerportal/en/login
    https://viking.safetyshop.local:9002/
    https://viking.service-portal.local:9002/hac/
    https://viking.service-portal.local:9002/backoffice/
    https://viking.service-portal.local:9002/smartedit/

    
### ATHEA ENV urls
    https://confluence.osudio.com/confluence/display/VIK/ATHEA+ENV+urls
    
