#!/bin/bash

cd /opt
chown -R hybris:hybris hybris && \
    chmod +x /opt/hybris-wrap.sh && \
    chmod +x /opt/hybris/bin/platform/tomcat/bin/* && \
    chmod +x /opt/hybris/bin/platform/tomcat/lib/* 

env | grep ^HYBRIS_ | awk -f /opt/env.awk | augtool -A

chown -R hybris /opt/hybris/data
chown -R hybris /opt/hybris/log

cd /opt/hybris/bin/platform
/usr/local/bin/gosu hybris ./license.sh -temp CPS_MSS
. setantenv.sh
/usr/local/bin/gosu hybris echo "HYBRIS_RUNTIME_PROPERTIES: $HYBRIS_RUNTIME_PROPERTIES" 
/usr/local/bin/gosu hybris ant server
/usr/local/bin/gosu hybris ./hybrisserver.sh debug
