echo 'This script reinstalls ALL dependepencies and libraries'
echo 'Use it when you upgrade to a new hybris version'

echo 'Start remove gradle cache'
read -p 'Press enter to continue'
./gradlew cleanBuildCache
rm -r  ~/.gradle/caches/*
echo 'Gradle cache removed'

echo 'Start removing hybris code dirs'
read -p 'Press enter to continue'
cd hybris/bin/
ls -d ext-* | xargs rm -rf
rm -rf platform
cd -
echo 'Dirs removed'

echo 'Start Refreshing dependencies and call ant build'
read -p 'Press enter to continue'
./gradlew --refresh-dependencies --rerun-tasks
echo 'Refreshing dependencies and calling ant build done'

echo 'Start installing addonons'
read -p 'Press enter to continue'
./gradlew installvikingservicepartnerportaladdons
echo 'Installing addonons done'

echo 'ant clean all'
read -p 'Press enter to continue'
./gradlew clean all
echo 'ant clean all done '

echo 'Setting correct right in tomcat config'
read -p 'Press enter to continue'
chmod +x hybris/bin/platform/hybrisserver.sh
chmod +x hybris/bin/platform/tomcat/bin/catalina.sh
chmod +x hybris/bin/platform/tomcat/bin/wrapper.sh
echo 'Finished setting correct right in tomcat config'
