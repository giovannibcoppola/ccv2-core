package com.osudio.hybris.lifter;

import javax.servlet.ServletContext;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.web.context.WebApplicationContext;

import com.osudio.hybris.lifter.core.ExecutionStrategy;
import com.osudio.hybris.lifter.core.LifterContext;
import com.osudio.hybris.lifter.core.Phase;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class LifterContextLoaderListenerTest {

	private ExecutionStrategy executionStrategy;
	private WebApplicationContext applicationContext;
	
	@Before
	public void before() {
		executionStrategy = Mockito.mock(ExecutionStrategy.class);
		applicationContext = Mockito.mock(WebApplicationContext.class);
	}
	
	@Test
	public void testInitWebApplicationContextCallsStartup() {
		// given
		ArgumentCaptor<LifterContext> argumentCaptor = ArgumentCaptor.forClass(LifterContext.class);
		ServletContext servletContext = Mockito.mock(ServletContext.class);
		LifterContextLoaderListener lifterContextLoaderListener = new LifterContextLoaderListener() {
			@Override
			protected WebApplicationContext doInitWebApplicationContext(ServletContext servletContext) {
				return applicationContext;
			}
		};
		Mockito.when(applicationContext.getBean(ExecutionStrategy.class)).thenReturn(executionStrategy);
		
		// when
		lifterContextLoaderListener.initWebApplicationContext(servletContext);
		
		// then
		Mockito.verify(applicationContext).getBean(ExecutionStrategy.class);
		Mockito.verify(executionStrategy).execute(argumentCaptor.capture());
		Assertions.assertThat(argumentCaptor.getValue().getPhase()).isEqualTo(Phase.STARTUP);
	}
	
	@Test
	public void testExecuteStartupPhase() {
		// given
		LifterContextLoaderListener lifterContextLoaderListener = new LifterContextLoaderListener();
		ArgumentCaptor<LifterContext> argumentCaptor = ArgumentCaptor.forClass(LifterContext.class);
		Mockito.doNothing().when(executionStrategy).execute(Mockito.any());
		Mockito.when(applicationContext.getBean(ExecutionStrategy.class)).thenReturn(executionStrategy);
		
		// when
		lifterContextLoaderListener.executeStartupPhase(applicationContext);
		
		// then
		Mockito.verify(applicationContext).getBean(ExecutionStrategy.class);
		Mockito.verify(executionStrategy).execute(argumentCaptor.capture());
		Assertions.assertThat(argumentCaptor.getValue().getPhase()).isEqualTo(Phase.STARTUP);
	}

	
}
