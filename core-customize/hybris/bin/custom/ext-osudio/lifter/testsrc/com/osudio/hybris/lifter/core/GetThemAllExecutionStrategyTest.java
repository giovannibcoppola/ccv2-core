package com.osudio.hybris.lifter.core;

import static org.hamcrest.Matchers.empty;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import com.google.common.collect.ImmutableList;

import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.JspContext;
import com.osudio.hybris.lifter.core.policy.Policy;
import com.osudio.hybris.lifter.core.task.Lifter;

@UnitTest
public class GetThemAllExecutionStrategyTest {

	private GetThemAllExecutionStrategy getThemAllExecutionStrategy;
	private JspContext jspContext;
	private Policy policy;

	@Before
	public void setup() {
		getThemAllExecutionStrategy = new GetThemAllExecutionStrategy();
		jspContext = mock(JspContext.class);
		policy = mock(Policy.class);
	}
	
	private Lifter lifterFor(Phase phase, int order) {
		Lifter lifter = mock(Lifter.class);
		when(lifter.getPhase()).thenReturn(phase);
		when(lifter.getPolicy()).thenReturn(policy);
		when(lifter.getOrder()).thenReturn(order);
		return lifter;
	}
	
	@Test
	public void testExecuteEmptyList() {
		getThemAllExecutionStrategy.setLifters(ImmutableList.<Lifter>of());
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.STARTUP, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.ESSENTIAL_DATA, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.PROJECT_DATA, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.BEFORE_INIT, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.BEFORE_UPDATE, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.AFTER_INIT, jspContext));
		getThemAllExecutionStrategy.execute(new LifterContext(Phase.AFTER_UPDATE, jspContext));
		assertThat(getThemAllExecutionStrategy.getLifters(), empty());
	}
	
	@Test
	public void testExecuteStartup() {
		Lifter lifter = lifterFor(Phase.STARTUP, 0);
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter));
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		getThemAllExecutionStrategy.execute(context);
		verify(lifter).lift(any(LifterContext.class));
	}
	
	@Test
	public void testNotExecuteStartup() {
		Lifter lifter = lifterFor(Phase.STARTUP, 0);
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter));
		LifterContext context = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		getThemAllExecutionStrategy.execute(context);
		verify(lifter, never()).lift(any(LifterContext.class));
	}

	
	@Test
	public void testExecuteStartupOrderRegular() {
		Lifter lifter1 = lifterFor(Phase.STARTUP, 1);
		Lifter lifter2 = lifterFor(Phase.STARTUP, 2);
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter1, lifter2));
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		getThemAllExecutionStrategy.execute(context);
		InOrder inOrder = inOrder(lifter1, lifter2);
		inOrder.verify(lifter1).lift(any(LifterContext.class));
		inOrder.verify(lifter2).lift(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteStartupOrderReverse() {
		Lifter lifter1 = lifterFor(Phase.STARTUP, 1);
		Lifter lifter2 = lifterFor(Phase.STARTUP, 2);
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter2, lifter1));
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		getThemAllExecutionStrategy.execute(context);
		InOrder inOrder = inOrder(lifter1, lifter2);
		inOrder.verify(lifter1).lift(any(LifterContext.class));
		inOrder.verify(lifter2).lift(any(LifterContext.class));
	}

	@Test
	public void testExecuteEssentialOrderRegular() {
		Lifter lifter1 = lifterFor(Phase.BEFORE_INIT, 1);
		Lifter lifter2 = lifterFor(Phase.STARTUP, 2);
		Lifter lifter3 = lifterFor(Phase.ESSENTIAL_DATA, 4);
		Lifter lifter4 = lifterFor(Phase.ESSENTIAL_DATA, 3);
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter1, lifter2, lifter3, lifter4));
		LifterContext context = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		getThemAllExecutionStrategy.execute(context);
		InOrder inOrder = inOrder(lifter4, lifter3);
		inOrder.verify(lifter4).lift(any(LifterContext.class));
		inOrder.verify(lifter3).lift(any(LifterContext.class));
	}
	
	@Test
	public void testExceptionGetsSwallowed() {
		Lifter lifter = lifterFor(Phase.STARTUP, 0);
		doThrow(RuntimeException.class).when(lifter).lift(any(LifterContext.class));
		getThemAllExecutionStrategy.setLifters(ImmutableList.of(lifter));
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		getThemAllExecutionStrategy.execute(context);
		verify(lifter).lift(any(LifterContext.class));
	}
}
