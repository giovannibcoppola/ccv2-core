package com.osudio.hybris.lifter.core.task;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import com.osudio.hybris.lifter.core.ExecutionResult;
import com.osudio.hybris.lifter.core.LifterContext;
import com.osudio.hybris.lifter.core.Phase;
import com.osudio.hybris.lifter.core.policy.Policy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.JspContext;

@UnitTest
public class UpdateRunningSystemLifterTest {
	
	public class NonHybrisCallingURSLifter extends UpdateRunningSystemLifter {
		private boolean tenantInitializingGlobally;
		private Tenant currentTenant;
		private boolean systemInitialized = true;

		@Override
		protected void doUpdateRunningSystem(JspContext jspContext) {
			// do nothing
		}
		
		@Override
		protected boolean isTenantInitializingGlobally() {
			return tenantInitializingGlobally;
		}

		public void setTenantInitializingGlobally(boolean tenantInitializingGlobally) {
			this.tenantInitializingGlobally = tenantInitializingGlobally;
		}
		
		@Override
		protected Tenant getCurrentTenant() {
			return currentTenant;
		}

		public void setCurrentTenant(Tenant currentTenant) {
			this.currentTenant = currentTenant;
		}
		
		@Override
		protected boolean isSystemInitialized() {
			return systemInitialized;
		}

		public void setSystemInitialized(boolean systemInitialized) {
			this.systemInitialized = systemInitialized;
		}
	}

	private NonHybrisCallingURSLifter lifter;
	private Tenant tenant;
	private LifterContext context;
	private ConfigurationService configurationService;
	
	
	@Before
	public void setup() {
		this.lifter = mock(NonHybrisCallingURSLifter.class, Mockito.CALLS_REAL_METHODS);
		this.tenant = mock(Tenant.class, Mockito.RETURNS_DEEP_STUBS);
		this.lifter.setCurrentTenant(tenant);
		this.lifter.policy = mock(Policy.class);
		this.lifter.phase = Phase.STARTUP;
		this.configurationService = mock(ConfigurationService.class, Mockito.RETURNS_DEEP_STUBS);
		this.context = new LifterContext(Phase.STARTUP, JspContext.NULL_CONTEXT);
		ReflectionTestUtils.setField(this.lifter, "configurationService", configurationService, ConfigurationService.class);
	}
	
	@Test
	public void testCallsUpdateWhenChecksPass() {
		when(configurationService.getConfiguration().getString("lifter.autoupdate.essential", "true")).thenReturn("true");
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.TRUE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.SUCCEEDED), anyString());
	}
	
	@Test
	public void testSkipsUpdateWhenConfigNotEnabled() {
		doReturn(Boolean.FALSE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.TRUE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter, never()).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
	}
	
	@Test
	public void testSkipsUpdateWhenAlreadyInitializing() {
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.TRUE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.TRUE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter, never()).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
	}
	
	@Test
	public void testSkipsUpdateWhenNotInitialized() {
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.FALSE).when(lifter).isSystemInitialized();
		doReturn(Boolean.TRUE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.TRUE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter, never()).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
	}
	
	
	@Test
	public void testIgnoresUpdateWhenNotRequired() {
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.FALSE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.IGNORED), anyString());
	}
	
	@Test
	public void testFailsUpdateWhenExceptionIsRaised() {
		when(configurationService.getConfiguration().getString("lifter.autoupdate.essential", "true")).thenReturn("true");
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doReturn(Boolean.TRUE).when(lifter).isExecutionRequired();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		doThrow(RuntimeException.class).when(lifter).doUpdateRunningSystem(any(JspContext.class));
		try {
			this.lifter.lift(context);
			fail("Should have thrown an execption");
		} catch (RuntimeException e) {
			// expected
		}
		verify(this.lifter).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.FAILED), anyString());
	}
	
	
	@Test
	public void testChecksAutoUpdateProperty() {
		when(configurationService.getConfiguration().getString("lifter.autoupdate.tenants", "master")).thenReturn("no-tenant-at-all");
		when(tenant.getTenantID()).thenReturn("master");
		
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter, never()).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
	}

	@Test
	public void testIsExecutionRequiredSkipsVersion() throws Exception {
		when(configurationService.getConfiguration().getString(eq("lifter.autoupdate.version.property"), anyString())).thenReturn("version");
		String version = UUID.randomUUID().toString();
		when(configurationService.getConfiguration().getString(eq("version"))).thenReturn(version);
		when(lifter.policy.isExecutionGranted("auto-update-running-system", version)).thenReturn(false);
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.IGNORED), anyString());
	}
	

	@Test
	public void testIsExecutionRequiredSkipsEmptyVersion() throws Exception {
		when(configurationService.getConfiguration().getString(eq("lifter.autoupdate.version.property"), anyString())).thenReturn("");
		
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter, never()).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.IGNORED), anyString());
		verify(lifter.policy, never()).isExecutionGranted(eq("auto-update-running-system"), anyString());
	}
	
	@Test
	public void testIsExecutionRequiredRunsVersion() throws Exception {
		when(configurationService.getConfiguration().getString(eq("lifter.autoupdate.version.property"), anyString())).thenReturn("version");
		when(configurationService.getConfiguration().getString("lifter.autoupdate.essential", "true")).thenReturn("true");
		String version = UUID.randomUUID().toString();
		when(configurationService.getConfiguration().getString(eq("version"))).thenReturn(version);
		when(lifter.policy.isExecutionGranted("auto-update-running-system", version)).thenReturn(true);
		doReturn(Boolean.TRUE).when(lifter).isCurrentTenantEnabledForAutoUpdate();
		doReturn(Boolean.TRUE).when(lifter).isSystemInitialized();
		doReturn(Boolean.FALSE).when(lifter).isTenantInitializingGlobally();
		doNothing().when(lifter).registerExecutionResult(any(LifterContext.class), anyString(), any(ExecutionResult.class), anyString());
		this.lifter.lift(context);
		verify(this.lifter).doUpdateRunningSystem(any(JspContext.class));
		verify(lifter).registerExecutionResult(any(LifterContext.class), anyString(), eq(ExecutionResult.SUCCEEDED), anyString());
	}
	
	
}
