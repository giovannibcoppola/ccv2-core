package com.osudio.hybris.lifter.core.policy;

import com.osudio.hybris.lifter.model.LifterTaskModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@UnitTest
@RunWith(Parameterized.class)
public class RunAlwaysPolicyTest {
	
	@Parameters(name = "{index}: taskCode={0}, taskSignature={1}")
	public static Collection<Object[]> data() {
        return Arrays.asList(     
                 new Object[]{ "test-if", "signature1" },
                 new Object[]{ "test-if", "signature2" },
                 new Object[]{ "test-not-found", "signature3" },
                 new Object[]{ null, "signature1" },
                 new Object[]{ "test-not-found", null },
                 new Object[]{ "test-if", null },
                 new Object[]{ null, null }
                 );
	}
	
	private final String taskCode;
	private final String taskSignature;
	
	public RunAlwaysPolicyTest(String taskCode, String taskSignature) {
		this.taskCode = taskCode;
		this.taskSignature = taskSignature;
	}
	
	@Test
	public void testIsExecutionGranted() throws Exception {
		RunAlwaysPolicy policy = new RunAlwaysPolicy() {
			@Override
			public LifterTaskModel findLifterTask(String code) {
				LifterTaskModel task = new LifterTaskModel();
				task.setCode("test-if");
				task.setSignature("signature1");
				return task;
			}
		};
		assertTrue(policy.isExecutionGranted(taskCode, taskSignature));
	}

}
