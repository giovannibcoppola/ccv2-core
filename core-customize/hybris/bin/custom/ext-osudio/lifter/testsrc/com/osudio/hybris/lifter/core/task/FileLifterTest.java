package com.osudio.hybris.lifter.core.task;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.base.Charsets;

import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class FileLifterTest {

	private ConfigurationService configurationService;

	@Before
	public void setup() {
		configurationService = mock(ConfigurationService.class, Mockito.RETURNS_DEEP_STUBS);
	}

	@Test
	public void testCollectFilesFilePath() throws Exception {
		
		List<File> collectedFiles;
		
		FileLifter fileLifter = new FileLifter();
		fileLifter.setFileSortMode(FileLifter.FileSortMode.FILEPATH);
		fileLifter.setPath("/testpatches");
		fileLifter.setStructuralOnlyDirectories(Arrays.asList("0.1","0.2","_GENERIC_","atenant","btenant"));
				
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/_GENERIC_/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		Assert.assertEquals(2, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/_GENERIC_/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.2/_GENERIC_/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
		
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/atenant/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		Assert.assertEquals(2, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/atenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.2/atenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
		
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/btenant/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		
		Assert.assertEquals(2, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/btenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.2/btenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
	}	
	
	
	@Test
	public void testCollectFilesFileNameThenFilePath() throws Exception {
		
		List<File> collectedFiles;
		
		FileLifter fileLifter = new FileLifter();
		fileLifter.setFileSortMode(FileLifter.FileSortMode.FILENAME_FIRST_THEN_FILEPATH);
		fileLifter.setPath("/testpatches");
		fileLifter.setStructuralOnlyDirectories(Arrays.asList("0.1","0.2","0.11","_GENERIC_","atenant","btenant"));
				
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/_GENERIC_/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		Assert.assertEquals(2, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/_GENERIC_/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.2/_GENERIC_/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
		
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/atenant/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		Assert.assertEquals(2, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/atenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.2/atenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
		
		fileLifter.setPathMatcher("glob:**/testpatches/*.*/btenant/*.{impex,bs,sql}");
		collectedFiles = fileLifter.collectFiles();
		
		Assert.assertEquals(3, collectedFiles.size());
		Assert.assertEquals("/testpatches/0.1/btenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(0)));
		Assert.assertEquals("/testpatches/0.11/btenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(1)));
		Assert.assertEquals("/testpatches/0.2/btenant/01-test.impex", fileLifter.getFileId(collectedFiles.get(2)));
	}	
	
	@Test
	public void testGetFileId() {
			
		FileLifter fileLifter = new FileLifter();
		fileLifter.setUseFullPathAsId(true);
		fileLifter.setPath("/testpatches");
		
		String linuxPath = "/my/local/path/to/hybris/bin/custom/lifter/resources/testpatches/0.1/btenant/01-test.impex";
		String windowsPath = "C:\\/my\\local\\path\\to\\hybris\\bin\\custom\\lifter\\resources\\testpatches\\0.1\\btenant\\01-test.impex";
		
		File linuxFile = Mockito.mock(File.class);
		Mockito.when(linuxFile.getAbsolutePath()).thenReturn(linuxPath);
		
		File windowsFile = Mockito.mock(File.class);
		Mockito.when(windowsFile.getAbsolutePath()).thenReturn(windowsPath);
		Assert.assertEquals("/testpatches/0.1/btenant/01-test.impex", fileLifter.getFileId(linuxFile));
		Assert.assertEquals("/testpatches/0.1/btenant/01-test.impex", fileLifter.getFileId(windowsFile));				
	}
	
	@Test
	public void testGetFileIdOnlyFileName() {
		FileLifter fileLifter = new FileLifter();
		fileLifter.setUseFullPathAsId(false);
		fileLifter.setPath("/testpatches");
		
		String linuxPath = 
		FilenameUtils.separatorsToSystem("/my/local/path/to/hybris/bin/custom/lifter/resources/testpatches/0.1/btenant/01-test.impex");
		
		String windowsPath = 
				FilenameUtils.separatorsToSystem("c:\\my\\local\\path\\01-test.impex");
				
		File linuxFile = Paths.get(linuxPath).toFile();
		File windowsFile = Paths.get(windowsPath).toFile();
		Assert.assertEquals("01-test.impex", fileLifter.getFileId(linuxFile));
		Assert.assertEquals("01-test.impex", fileLifter.getFileId(windowsFile));
	}
	
	@Test
	public void testGetFileIdException() {
		FileLifter fileLifter = new FileLifter();
		fileLifter.setUseFullPathAsId(false);
		fileLifter.setPath("/testpatches");
		
		File linuxFile = Mockito.mock(File.class);
		Mockito.when(linuxFile.getName()).thenThrow(new NullPointerException());
		Assert.assertNull(fileLifter.getFileId(linuxFile));
	}


	@Test
	public void testSetExecutionEnvironmentDev() throws Exception {
		FileLifter fileLifter = new FileLifter();
		fileLifter.setConfigurationService(configurationService);
		when(configurationService.getConfiguration().getString("lifter.execution.environments", "local")).thenReturn("dev");
		fileLifter.setExecutionEnvironment();
		assertThat(fileLifter.getExecutionEnvironments(), hasSize(1));
		assertThat(fileLifter.getExecutionEnvironments(), contains("dev"));
	}
	
	@Test
	public void testSetExecutionEnvironmentUnconfigured() throws Exception {
		FileLifter fileLifter = new FileLifter();
		fileLifter.setConfigurationService(configurationService);
		when(configurationService.getConfiguration().getString("lifter.execution.environments", "local")).thenReturn("local");
		fileLifter.setExecutionEnvironment();
		assertThat(fileLifter.getExecutionEnvironments(), hasSize(1));
		assertThat(fileLifter.getExecutionEnvironments(), contains("local"));
	}
	
	@Test
	public void testSetExecutionEnvironmentConfiguredToBeEmpty() throws Exception {
		FileLifter fileLifter = new FileLifter();
		fileLifter.setConfigurationService(configurationService);
		when(configurationService.getConfiguration().getString("lifter.execution.environments", "local")).thenReturn("");
		fileLifter.setExecutionEnvironment();
		assertThat(fileLifter.getExecutionEnvironments(), empty());
	}

	@Test
	public void testGetSignature() throws Exception {
		File file = File.createTempFile("FileLifterTest", "testGetSignature");
		String contents = UUID.randomUUID().toString();
		FileUtils.write(file, contents);
		FileLifter fileLifter = new FileLifter();
		String signature = fileLifter.getSignature(file);
		assertThat(signature, equalTo(DigestUtils.sha256Hex(contents.getBytes(Charsets.UTF_8))));
	}
	
}
