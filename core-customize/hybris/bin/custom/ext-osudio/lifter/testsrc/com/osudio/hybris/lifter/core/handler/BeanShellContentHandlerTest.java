package com.osudio.hybris.lifter.core.handler;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.Reader;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;

import com.google.common.base.Charsets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import bsh.Interpreter;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
public class BeanShellContentHandlerTest {

	private BeanShellContentHandler contentHandler;
	private ModelService modelService;
	private ApplicationContext applicationContext;
	protected Interpreter beanshellInterpreter;

	@Before
	public void setup() {
		contentHandler = new BeanShellContentHandler() {
			@Override
			protected Interpreter createNewInterpreter() {
				return beanshellInterpreter;
			}
		};
		beanshellInterpreter= mock(Interpreter.class, Mockito.RETURNS_DEEP_STUBS);
		modelService = mock(ModelService.class);
		applicationContext = mock(ApplicationContext.class);
		contentHandler.setModelService(modelService);
		contentHandler.setApplicationContext(applicationContext);
	}
	

	@Test
	public void testHandle() throws Exception {
		String script = "bla";
		when(beanshellInterpreter.eval(any(Reader.class))).thenReturn(null);
		contentHandler.handle(new ByteArrayInputStream(script.getBytes(Charsets.UTF_8)));
		ArgumentCaptor<Reader> argumentCaptor = ArgumentCaptor.forClass(Reader.class);
		verify(beanshellInterpreter).eval(argumentCaptor.capture());
		assertThat(IOUtils.toString(argumentCaptor.getValue()), equalTo("bla"));
		
	}

	@Test
		public void testConfigureInterpreter() throws Exception {
			contentHandler.configureInterpreter(beanshellInterpreter);
			verify(beanshellInterpreter.getNameSpace()).importPackage("de.hybris.platform.servicelayer.search");
			verify(beanshellInterpreter).set("context", applicationContext);
			verify(beanshellInterpreter).set("modelService", modelService);
		}

	@Test
	public void testCanHandle() {
		assertTrue(contentHandler.canHandle("06_bazaarvoice_update_orders.bs"));
		assertTrue(contentHandler.canHandle("06_bazaarvoice_update_orders.draft.bs"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.impex"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.bs.impex"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.groovy"));
	}
}
