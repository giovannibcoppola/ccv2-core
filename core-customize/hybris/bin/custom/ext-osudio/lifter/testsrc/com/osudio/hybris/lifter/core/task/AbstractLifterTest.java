package com.osudio.hybris.lifter.core.task;

import com.osudio.hybris.lifter.core.ExecutionResult;
import com.osudio.hybris.lifter.core.LifterContext;
import com.osudio.hybris.lifter.core.Phase;
import com.osudio.hybris.lifter.core.policy.Policy;
import com.osudio.hybris.lifter.enums.LifterExecutionResult;
import com.osudio.hybris.lifter.model.LifterTaskLogModel;
import com.osudio.hybris.lifter.model.LifterTaskModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.JspContext;
import org.apache.commons.collections.EnumerationUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@UnitTest
public class AbstractLifterTest {
	
	private TestAbstractLifter lifter;
	private FlexibleSearchService flexibleSearchService;
	private ModelService modelService;
	private Policy policy;
	private JspContext jspContext;
	private Logger lifterLogger;
	private Level startLoggingLevel;
	private List<Appender> startLoggingAppenders;

	public class TestAbstractLifter extends AbstractLifter {
		@Override
		public void lift(LifterContext context) {
			// do nothing	
		}
	}
	
	@SuppressWarnings("unchecked")
	@Before
	public void setup() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		lifter = new TestAbstractLifter();
		modelService = mock(ModelService.class, Mockito.RETURNS_DEEP_STUBS);
		flexibleSearchService = mock(FlexibleSearchService.class, Mockito.RETURNS_DEEP_STUBS);
		policy = mock(Policy.class);
		lifter.setFlexibleSearchService(flexibleSearchService);
		lifter.setModelService(modelService);
		lifter.setPolicy(policy);
		jspContext = mock(JspContext.class);
		lifterLogger = (Logger) ReflectionTestUtils.getField(lifter, "LOGGER");
		startLoggingLevel = lifterLogger.getLevel();
		startLoggingAppenders = EnumerationUtils.toList(lifterLogger.getAllAppenders());
		lifterLogger.removeAllAppenders();
	}
	
	@After
	public void teardown() {
		lifterLogger.setLevel(startLoggingLevel);
		for (Appender appender : startLoggingAppenders) {	
			lifterLogger.addAppender(appender);	
		}
	}
	
	@Test
	public void testInjectectServices() {
		assertThat(lifter.getFlexibleSearchService(), sameInstance(flexibleSearchService));
		assertThat(lifter.getModelService(), sameInstance(modelService));
	}
	
	@Test
	public void testConfiguredProperties() {
		lifter.setPhase(Phase.ESSENTIAL_DATA);
		lifter.setPolicy(policy);
		lifter.setOrder(Integer.MAX_VALUE);
		assertThat(lifter.getPhase(), equalTo(Phase.ESSENTIAL_DATA));	
		assertThat(lifter.getPolicy(), sameInstance(policy));
		assertThat(lifter.getOrder(), equalTo(Integer.MAX_VALUE));
	}
	
	
	@Test
	public void testLogCallsJspContext() {
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		doNothing().when(jspContext).print(anyString());
		when(jspContext.getServletResponse()).thenReturn(mock(HttpServletResponse.class));
		lifter.log(lifterContext, Level.INFO, "Test Message");
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(jspContext).print(captor.capture());
		assertThat(captor.getValue(), equalTo("<li class=\"INFO\">Test&nbsp;Message</li>"));	
	}
	
	@Test
	public void testLogCallsJspContextDebug() {
		lifterLogger.setLevel(Level.DEBUG);
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		when(jspContext.getServletResponse()).thenReturn(mock(HttpServletResponse.class));
		doNothing().when(jspContext).print(anyString());
		lifter.logDebug(lifterContext, "Test Message debug");
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(jspContext).print(captor.capture());
		assertThat(captor.getValue(), equalTo("<li class=\"DEBUG\">Test&nbsp;Message&nbsp;debug</li>"));	
	}
	
	@Test
	public void testLogCallsJspContextInfo() {
		lifterLogger.setLevel(Level.INFO);
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		when(jspContext.getServletResponse()).thenReturn(mock(HttpServletResponse.class));
		doNothing().when(jspContext).print(anyString());
		lifter.logInfo(lifterContext, "Test Message info");
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(jspContext).print(captor.capture());
		assertThat(captor.getValue(), equalTo("<li class=\"INFO\">Test&nbsp;Message&nbsp;info</li>"));	
	}
	
	@Test
	public void testLogCallsJspContextWarn() {
		lifterLogger.setLevel(Level.WARN);
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		when(jspContext.getServletResponse()).thenReturn(mock(HttpServletResponse.class));
		doNothing().when(jspContext).print(anyString());
		lifter.logWarn(lifterContext, "Test Message warn");
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(jspContext).print(captor.capture());
		assertThat(captor.getValue(), equalTo("<li class=\"WARN\">Test&nbsp;Message&nbsp;warn</li>"));	
	}
	
	@Test
	public void testLogCallsJspContextError() {
		lifterLogger.setLevel(Level.ERROR);
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		when(jspContext.getServletResponse()).thenReturn(mock(HttpServletResponse.class));
		doNothing().when(jspContext).print(anyString());
		lifter.logError(lifterContext, "Test Message error");
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(jspContext).print(captor.capture());
		assertThat(captor.getValue(), equalTo("<li class=\"ERROR\">Test&nbsp;Message&nbsp;error</li>"));	
	}
	
	@Test
	public void testNeverOutputsBelowThresholdError() {
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		doNothing().when(jspContext).print(anyString());
		lifterLogger.setLevel(Level.FATAL);
		lifter.logError(lifterContext, "Test Message");
		verify(jspContext, never()).print(anyString());
	}
	
	@Test
	public void testNeverOutputsBelowThresholdWarn() {
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		doNothing().when(jspContext).print(anyString());
		lifterLogger.setLevel(Level.ERROR);
		lifter.logWarn(lifterContext, "Test Message");
		verify(jspContext, never()).print(anyString());
	}
	
	@Test
	public void testNeverOutputsBelowThresholdInfo() {
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		doNothing().when(jspContext).print(anyString());
		lifterLogger.setLevel(Level.WARN);
		lifter.logInfo(lifterContext, "Test Message");
		verify(jspContext, never()).print(anyString());
	}
	
	@Test
	public void testNeverOutputsBelowThresholdDebug() {
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		doNothing().when(jspContext).print(anyString());
		lifterLogger.setLevel(Level.INFO);
		lifter.logDebug(lifterContext, "Test Message");
		verify(jspContext, never()).print(anyString());
	}

	@Test
	public void testRegisterExecutionResultSuccess() throws Exception {
		Date now = new Date();
		String id = "code";
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		assertThat(lifterContext.getSucceeded(), equalTo(0));
		LifterTaskLogModel taskLogModel = new LifterTaskLogModel();
		when(modelService.create(LifterTaskLogModel.class)).thenReturn(taskLogModel);
		LifterTaskModel lifterTaskModel = new LifterTaskModel();
		when(policy.findLifterTask(id)).thenReturn(lifterTaskModel);
		
		lifter.registerExecutionResult(lifterContext, id, ExecutionResult.SUCCEEDED, "yay");
		
		assertThat(taskLogModel.getDate(), greaterThanOrEqualTo(now));
		assertThat(taskLogModel.getTask(), sameInstance(lifterTaskModel));
		assertThat(taskLogModel.getResult(), equalTo(LifterExecutionResult.SUCCESSFUL));
		assertThat(taskLogModel.getMessage(), equalTo("yay"));
		assertThat(lifterContext.getSucceeded(), equalTo(1));
		verify(modelService).save(taskLogModel);
	}
	
	@Test
	public void testRegisterExecutionResultIgnored() throws Exception {
		Date now = new Date();
		String id = "code";
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		assertThat(lifterContext.getSucceeded(), equalTo(0));
		assertThat(lifterContext.getIgnored(), equalTo(0));
		LifterTaskLogModel taskLogModel = new LifterTaskLogModel();
		when(modelService.create(LifterTaskLogModel.class)).thenReturn(taskLogModel);
		LifterTaskModel lifterTaskModel = new LifterTaskModel();
		when(policy.findLifterTask(id)).thenReturn(lifterTaskModel);
		
		lifter.registerExecutionResult(lifterContext, id, ExecutionResult.IGNORED, "yay2");
		
		assertThat(taskLogModel.getDate(), greaterThanOrEqualTo(now));
		assertThat(taskLogModel.getTask(), sameInstance(lifterTaskModel));
		assertThat(taskLogModel.getResult(), equalTo(LifterExecutionResult.SUCCESSFUL));
		assertThat(taskLogModel.getMessage(), equalTo("yay2"));
		assertThat(lifterContext.getIgnored(), equalTo(1));
		assertThat(lifterContext.getSucceeded(), equalTo(0));
		verify(modelService).save(taskLogModel);
	}
	
	@Test
	public void testRegisterExecutionResultFailure() throws Exception {
		Date now = new Date();
		String id = "code";
		LifterContext lifterContext = new LifterContext(Phase.ESSENTIAL_DATA, jspContext);
		assertThat(lifterContext.getSucceeded(), equalTo(0));
		assertThat(lifterContext.getIgnored(), equalTo(0));
		assertThat(lifterContext.getFailed(), equalTo(0));
		LifterTaskLogModel taskLogModel = new LifterTaskLogModel();
		when(modelService.create(LifterTaskLogModel.class)).thenReturn(taskLogModel);
		LifterTaskModel lifterTaskModel = new LifterTaskModel();
		when(policy.findLifterTask(id)).thenReturn(lifterTaskModel);
		
		lifter.registerExecutionResult(lifterContext, id, ExecutionResult.FAILED, "yay3");
		
		assertThat(taskLogModel.getDate(), greaterThanOrEqualTo(now));
		assertThat(taskLogModel.getTask(), sameInstance(lifterTaskModel));
		assertThat(taskLogModel.getResult(), equalTo(LifterExecutionResult.ERROR));
		assertThat(taskLogModel.getMessage(), equalTo("yay3"));
		assertThat(lifterContext.getIgnored(), equalTo(0));
		assertThat(lifterContext.getSucceeded(), equalTo(0));
		assertThat(lifterContext.getFailed(), equalTo(1));
		verify(modelService).save(taskLogModel);
	}
	
}
