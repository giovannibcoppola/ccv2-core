package com.osudio.hybris.lifter.core.handler;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.StringReader;

import org.apache.commons.io.input.ReaderInputStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.base.Charsets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class SqlContentHandlerTest {

	private SqlContentHandler sqlContentHandler;
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setup() {
		jdbcTemplate = mock(JdbcTemplate.class);
		sqlContentHandler = new SqlContentHandler() {
			@Override
			protected JdbcTemplate createJdbcTemplate() {
				return jdbcTemplate;
			}
		};
	}
	
	@Test
	public void testCanHandle() {
		assertTrue(sqlContentHandler.canHandle("06_bazaarvoice_update_orders.sql"));
		assertTrue(sqlContentHandler.canHandle("06_bazaarvoice_update_orders.draft.sql"));
		assertFalse(sqlContentHandler.canHandle("06_bazaarvoice_job_trigger.impex"));
	}

	@Test
	public void testSingleStatement() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1;");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1");
	}
	
	@Test
	public void testSingleStatementNoEndDelimiter() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1");
	}
	
	@Test
	public void testMultipleStatements() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1;\nselect 2=2;");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1");
		verify(jdbcTemplate).execute("select 2=2");
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testMultipleStatementsNoEndDelimiter() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1;\nselect 2=2");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1");
		verify(jdbcTemplate).execute("select 2=2");
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testSkipCommentDoubleSlash() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("// select 2=2");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testSkipCommentDoubleHyphen() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("-- select 2=2");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testSkipCommentDoubleSlashTrimmed() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("  	// select 2=2");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testSkipCommentDoubleHyphenTrimmed() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("    	-- select 2=2");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verifyNoMoreInteractions(jdbcTemplate);
	}
	
	@Test
	public void testStatementWithCommentOtherThanStartSentToJdbcTemplateHyphens() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1 -- this is comment");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1 -- this is comment");
	}
	
	@Test
	public void testStatementWithCommentOtherThanStartSentToJdbcTemplateSlashes() throws Exception {		
		sqlContentHandler.setDelimiter(";");
		StringReader stringReader = new StringReader("select 1=1 // this is comment, and the db will most likely fail");
		sqlContentHandler.handle(new ReaderInputStream(stringReader, Charsets.UTF_8));
		verify(jdbcTemplate).execute("select 1=1 // this is comment, and the db will most likely fail");
	}
}