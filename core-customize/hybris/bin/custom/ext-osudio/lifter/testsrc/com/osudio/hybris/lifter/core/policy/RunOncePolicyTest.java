package com.osudio.hybris.lifter.core.policy;

import com.google.common.collect.ImmutableList;
import com.osudio.hybris.lifter.model.LifterTaskLogModel;
import com.osudio.hybris.lifter.model.LifterTaskModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@UnitTest
public class RunOncePolicyTest {

	private RunOncePolicy runOncePolicy;
	private ModelService modelService;

	@Before
	public void setUp() {
		runOncePolicy = mock(RunOncePolicy.class);	
		modelService = mock(ModelService.class, Mockito.RETURNS_DEEP_STUBS);
		when(runOncePolicy.getModelService()).thenReturn(modelService);
	}

	@Test
	public void testIsExecutionGrantedFirstRun() throws Exception {
		String id = "task";
		String signature = "clkjckd";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(runOncePolicy.getOrCreateTask(id, signature, true)).thenReturn(lifterTaskModel);
		when(runOncePolicy.isFailOnSignature(id)).thenReturn(true);
		when(lifterTaskModel.getExecutionLogs()).thenReturn(ImmutableList.<LifterTaskLogModel>of());
		when(runOncePolicy.isExecutionGranted(id, signature)).thenCallRealMethod();
		assertThat(runOncePolicy.isExecutionGranted(id, signature), equalTo(true));
	}
	
	@Test
	public void testIsExecutionGrantedOnChangedSignatureRun() throws Exception {
		String id = "task";
		String signature = "clkjckd";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(lifterTaskModel.getSignature()).thenReturn("kmxjx");
		when(runOncePolicy.getOrCreateTask(id, signature, false)).thenReturn(lifterTaskModel);
		when(runOncePolicy.isFailOnSignature(id)).thenReturn(false);
		when(lifterTaskModel.getExecutionLogs()).thenReturn(ImmutableList.<LifterTaskLogModel>of(new LifterTaskLogModel()));
		when(runOncePolicy.isExecutionGranted(id, signature)).thenCallRealMethod();
		assertThat(runOncePolicy.isExecutionGranted(id, signature), equalTo(true));
	}
	
	@Test
	public void testUpdateSignature() throws Exception {
		String id = "task";
		String signature = "clkjckd";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(lifterTaskModel.getSignature()).thenReturn("kmxjx");
		when(runOncePolicy.getOrCreateTask(id, signature, false)).thenReturn(lifterTaskModel);
		when(runOncePolicy.isFailOnSignature(id)).thenReturn(false);
		when(lifterTaskModel.getExecutionLogs()).thenReturn(ImmutableList.<LifterTaskLogModel>of());
		when(runOncePolicy.isExecutionGranted(id, signature)).thenCallRealMethod();
		assertThat(runOncePolicy.isExecutionGranted(id, signature), equalTo(true));
		verify(lifterTaskModel).setSignature(signature);
		verify(modelService).save(lifterTaskModel);
	}
	
	
	@Test
	public void testLeaveSignatureWhenSame() throws Exception {
		String id = "task";
		String signature = "clkjckd";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(lifterTaskModel.getSignature()).thenReturn(signature);
		when(runOncePolicy.getOrCreateTask(id, signature, false)).thenReturn(lifterTaskModel);
		when(runOncePolicy.isFailOnSignature(id)).thenReturn(false);
		when(lifterTaskModel.getExecutionLogs()).thenReturn(ImmutableList.<LifterTaskLogModel>of());
		when(runOncePolicy.isExecutionGranted(id, signature)).thenCallRealMethod();
		assertThat(runOncePolicy.isExecutionGranted(id, signature), equalTo(true));
		verify(lifterTaskModel, never()).setSignature(signature);
		verify(modelService, never()).save(lifterTaskModel);
	}
}
