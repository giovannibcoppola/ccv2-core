package com.osudio.hybris.lifter.core;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.JspContext;

@UnitTest
public class LifterContextTest {

	@Test
	public void testIncrementCount() throws Exception {
		JspContext jspContext = mock(JspContext.class);
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		assertThat(context.getFailed(), equalTo(0));
		assertThat(context.getIgnored(), equalTo(0));
		assertThat(context.getSucceeded(), equalTo(0));
		context.incrementCount(ExecutionResult.IGNORED);
		assertThat(context.getFailed(), equalTo(0));
		assertThat(context.getIgnored(), equalTo(1));
		assertThat(context.getSucceeded(), equalTo(0));
		context.incrementCount(ExecutionResult.FAILED);		
		context.incrementCount(ExecutionResult.FAILED);
		assertThat(context.getFailed(), equalTo(2));
		assertThat(context.getIgnored(), equalTo(1));
		assertThat(context.getSucceeded(), equalTo(0));
		context.incrementCount(ExecutionResult.SUCCEEDED);
		assertThat(context.getSucceeded(), equalTo(1));
		context.incrementCount(ExecutionResult.SUCCEEDED);
		context.incrementCount(ExecutionResult.SUCCEEDED);
		assertThat(context.getFailed(), equalTo(2));
		assertThat(context.getIgnored(), equalTo(1));
		assertThat(context.getSucceeded(), equalTo(3));
	}
	
	@Test(expected=NullPointerException.class)
	public void testIncrementCountNull() throws Exception {
		JspContext jspContext = mock(JspContext.class);
		LifterContext context = new LifterContext(Phase.STARTUP, jspContext);
		context.incrementCount(null);
	}

}
