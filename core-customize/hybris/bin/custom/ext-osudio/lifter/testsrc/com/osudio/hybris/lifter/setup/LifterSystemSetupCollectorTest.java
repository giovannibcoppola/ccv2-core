package com.osudio.hybris.lifter.setup;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupCollector;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.util.JspContext;

@UnitTest
public class LifterSystemSetupCollectorTest {

	private SystemSetupAllowStrategy allowStrategy;
	private LifterSystemSetupCollector collector;
	private SystemSetupCollector delegateCollector;

	private SystemSetupContext systemSetupContext;

	private JspContext jspContext;

	@Before
	public void before() {
		systemSetupContext = mock(SystemSetupContext.class);
		
		jspContext = mock(JspContext.class); 
		when(systemSetupContext.getJspContext()).thenReturn(jspContext);
		doNothing().when(jspContext).println(anyString());
		allowStrategy = mock(SystemSetupAllowStrategy.class);
		delegateCollector = mock(SystemSetupCollector.class);
		collector = new LifterSystemSetupCollector();
		collector.setAllowStrategy(allowStrategy);
		collector.setDelegate(delegateCollector);
	}
	
	@Test
	public void testExecuteMethodsDisabled() {
		when(systemSetupContext.getExtensionName()).thenReturn("extname");
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(delegateCollector.hasEssentialData("extname")).thenReturn(Boolean.TRUE);
		when(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION)).thenReturn(Boolean.FALSE);
		collector.executeMethods(systemSetupContext);
		verify(allowStrategy).isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION);
		verify(allowStrategy, never()).isAllowed(systemSetupContext, SystemSetupSource.EXTENSION);
		verify(allowStrategy, never()).informEndUserAboutDenial(systemSetupContext, SystemSetupSource.EXTENSION);
		verify(allowStrategy).informEndUserAboutDenial(systemSetupContext, SystemSetupSource.ANNOTATION);
		verify(delegateCollector).hasEssentialData("extname");
		verify(delegateCollector, never()).executeMethods(systemSetupContext);
	}
	
	@Test
	public void testExecuteMethodsEnabled() {
		when(systemSetupContext.getExtensionName()).thenReturn("extname");
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(delegateCollector.hasProjectData("extname")).thenReturn(Boolean.TRUE);
		when(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION)).thenReturn(Boolean.TRUE);
		collector.executeMethods(systemSetupContext);
		verify(allowStrategy).isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION);
		verify(allowStrategy, never()).isAllowed(systemSetupContext, SystemSetupSource.EXTENSION);
		verify(allowStrategy, never()).informEndUserAboutDenial(systemSetupContext, SystemSetupSource.ANNOTATION);
		verify(allowStrategy, never()).informEndUserAboutDenial(systemSetupContext, SystemSetupSource.EXTENSION);
		verify(delegateCollector).hasProjectData("extname");
		verify(delegateCollector).executeMethods(systemSetupContext);
	}
	
	
}
