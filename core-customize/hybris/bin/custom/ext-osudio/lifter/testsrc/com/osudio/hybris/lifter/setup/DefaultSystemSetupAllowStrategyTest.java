package com.osudio.hybris.lifter.setup;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.JspContext;

@UnitTest
public class DefaultSystemSetupAllowStrategyTest {
	
	private static final String LIFTER_INIT_ESSENTIAL_ENABLED = "lifter.init.essentialdata.allow";
	private static final String LIFTER_UPDATE_PROJECT_ENABLED = "lifter.update.projectdata.allow";

	private static final Answer<Boolean> RETURN_DEFAULT_BOOLEAN_VALUE = new Answer<Boolean>() {
		@Override
		public Boolean answer(InvocationOnMock invocation) {
			return (Boolean) invocation.getArguments()[1];
		}
	};

	private ConfigurationService configurationService;
	private DefaultSystemSetupAllowStrategy allowStrategy;
	private Configuration configuration;

	private SystemSetupContext systemSetupContext;

	private JspContext jspContext;

	@Before
	public void before() {
		systemSetupContext = mock(SystemSetupContext.class);
		
		jspContext = mock(JspContext.class); 
		when(systemSetupContext.getJspContext()).thenReturn(jspContext);
		doNothing().when(jspContext).println(anyString());
		configurationService = mock(ConfigurationService.class);
		configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		allowStrategy = new DefaultSystemSetupAllowStrategy();
		allowStrategy.setConfigurationService(configurationService);
		
	}
	
	@Test
	public void testExecuteInitEssentialMethodsDisabled() {
		when(systemSetupContext.getProcess()).thenReturn(Process.INIT);
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean())).thenReturn(Boolean.FALSE);
		String extConfig = "ext."+LIFTER_INIT_ESSENTIAL_ENABLED;
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertFalse(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig, false);
		verify(configuration, never()).getBoolean(extConfig, true);
	}
	
	@Test
	public void testExecuteInitEssentialMethodsEnabled() {
		when(systemSetupContext.getProcess()).thenReturn(Process.INIT);
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.init.essentialdata.allow";
		when(configuration.getBoolean(anyString(), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, false);
	}

	@Test
	public void testExecuteInitEssentialMethodsDisabledExtension() {
		when(systemSetupContext.getProcess()).thenReturn(Process.INIT);
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.init.essentialdata.allow";
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.FALSE);
		assertFalse(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), eq(false));
		verify(configuration, never()).getBoolean(extConfig, false);
	}
	
	@Test
	public void testExecuteInitEssentialMethodsEnabledExtension() {
		when(systemSetupContext.getProcess()).thenReturn(Process.INIT);
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.init.essentialdata.allow";
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.TRUE);
		when(configuration.getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig, true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, false);
	}
	
	@Test
	public void testExecuteInitEssentialMethodsEnabledExtensionOverrideGlobal() {
		when(systemSetupContext.getProcess()).thenReturn(Process.INIT);
		when(systemSetupContext.getType()).thenReturn(Type.ESSENTIAL);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean())).thenReturn(Boolean.FALSE);
		String extConfig = "ext."+LIFTER_INIT_ESSENTIAL_ENABLED;
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.TRUE);
		when(configuration.getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_INIT_ESSENTIAL_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,false);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.init.essentialdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, true);
	}
	
	// Update ProjectData
	@Test
	public void testExecuteUpdateProjectMethodsDisabled() {
		when(systemSetupContext.getProcess()).thenReturn(Process.UPDATE);
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean())).thenReturn(Boolean.FALSE);
		String extConfig = "ext."+LIFTER_UPDATE_PROJECT_ENABLED;
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertFalse(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig, false);
		verify(configuration, never()).getBoolean(extConfig, true);
	}
	
	@Test
	public void testExecuteUpdateProjectMethodsEnabled() {
		when(systemSetupContext.getProcess()).thenReturn(Process.UPDATE);
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.update.projectdata.allow";
		when(configuration.getBoolean(anyString(), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, false);
	}

	@Test
	public void testExecuteUpdateProjectMethodsDisabledExtension() {
		when(systemSetupContext.getProcess()).thenReturn(Process.UPDATE);
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.update.projectdata.allow";
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.FALSE);
		assertFalse(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), eq(false));
		verify(configuration, never()).getBoolean(extConfig, false);
	}
	
	@Test
	public void testExecuteUpdateProjectMethodsEnabledExtension() {
		when(systemSetupContext.getProcess()).thenReturn(Process.UPDATE);
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean())).thenReturn(Boolean.TRUE);
		String extConfig = "ext.lifter.update.projectdata.allow";
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.TRUE);
		when(configuration.getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig, true);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, false);
	}
	
	@Test
	public void testExecuteUpdateProjectMethodsEnabledExtensionOverrideGlobal() {
		when(systemSetupContext.getProcess()).thenReturn(Process.UPDATE);
		when(systemSetupContext.getType()).thenReturn(Type.PROJECT);
		when(systemSetupContext.getExtensionName()).thenReturn("ext");
		when(configuration.getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean())).thenReturn(Boolean.FALSE);
		String extConfig = "ext."+LIFTER_UPDATE_PROJECT_ENABLED;
		when(configuration.getBoolean(eq(extConfig), anyBoolean())).thenReturn(Boolean.TRUE);
		when(configuration.getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), anyBoolean())).thenAnswer(RETURN_DEFAULT_BOOLEAN_VALUE);
		assertTrue(allowStrategy.isAllowed(systemSetupContext, SystemSetupSource.ANNOTATION));
		verify(configuration).getBoolean(eq(LIFTER_UPDATE_PROJECT_ENABLED), anyBoolean());
		verify(configuration).getBoolean(extConfig,false);
		verify(configuration).getBoolean(eq("ext.lifter.annotation.update.projectdata.allow"), eq(true));
		verify(configuration, never()).getBoolean(extConfig, true);
	}
	
}