package com.osudio.hybris.lifter.core.handler;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.common.base.Charsets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import bsh.Interpreter;
import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class GroovyShellContentHandlerTest {

	public static final ThreadLocal<String> TEST_SCRIPT_RESULT = new ThreadLocal<>();  
	
	private GroovyShellContentHandler contentHandler;
	private ApplicationContext applicationContext;
	protected Interpreter beanshellInterpreter;

	@Before
	public void setup() {
		contentHandler = new GroovyShellContentHandler();
		beanshellInterpreter= mock(Interpreter.class, Mockito.RETURNS_DEEP_STUBS);
		applicationContext = mock(ApplicationContext.class);
		ReflectionTestUtils.setField(contentHandler, "applicationContext", applicationContext, ApplicationContext.class);
	}
	
	@Test
	public void testCanHandle() {
		assertTrue(contentHandler.canHandle("06_bazaarvoice_update_orders.groovy"));
		assertTrue(contentHandler.canHandle("06_bazaarvoice_update_orders.draft.groovy"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.impex"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.groovy.impex"));
		assertFalse(contentHandler.canHandle("06_bazaarvoice_job_trigger.groovy/test.impex"));
	}

	@Test
	public void testHandle() throws Exception {
		assertThat(TEST_SCRIPT_RESULT.get(), nullValue());
		String randomValue = UUID.randomUUID().toString();
		when(applicationContext.getBean("value", String.class)).thenReturn(randomValue);
		when(applicationContext.getBean("threadLocal")).thenReturn(TEST_SCRIPT_RESULT);
		
		contentHandler.handle(new ByteArrayInputStream("ctx.getBean('threadLocal').set(ctx.getBean('value', String.class))".getBytes(Charsets.UTF_8)));
		verify(applicationContext).getBean("value", String.class);
		verify(applicationContext).getBean("threadLocal");
		assertThat(TEST_SCRIPT_RESULT.get(), equalTo(randomValue));
	}
}
