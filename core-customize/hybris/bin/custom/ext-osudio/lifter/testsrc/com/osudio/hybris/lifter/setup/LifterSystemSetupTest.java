package com.osudio.hybris.lifter.setup;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.osudio.hybris.lifter.core.ExecutionStrategy;
import com.osudio.hybris.lifter.core.LifterContext;
import com.osudio.hybris.lifter.core.Phase;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.JspContext;

@UnitTest
public class LifterSystemSetupTest {
	
	public class NonHybrisCallingLifterSystemSetup extends LifterSystemSetup {
		
		@Override
		protected String getTenantId() {
			return currentTenantId;
		}
		
	}

	private ExecutionStrategy executionStrategy;
	private String currentTenantId = "master";
	private NonHybrisCallingLifterSystemSetup setup;
	private SystemSetupContext systemSetupContext;
	private ConfigurationService configurationService;
	private Configuration configuration;
	
	@Before
	public void setup() {
		setup = new NonHybrisCallingLifterSystemSetup();
		executionStrategy = mock(ExecutionStrategy.class);
		setup.setExecutionStrategy(executionStrategy);
		systemSetupContext = mock(SystemSetupContext.class);
		configurationService = mock(ConfigurationService.class);
		setup.setConfigurationService(configurationService);
		configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean(anyString(), eq(true))).thenReturn(Boolean.TRUE);
	}

	@Test
	public void testEssentialData() {
		when(systemSetupContext.getJspContext()).thenReturn(JspContext.NULL_CONTEXT);
		setup.essentialData(systemSetupContext);
		ArgumentCaptor<LifterContext> contextCaptor = ArgumentCaptor.forClass(LifterContext.class);
		verify(executionStrategy).execute(contextCaptor.capture());
		assertThat(contextCaptor.getValue().getPhase(), equalTo(Phase.ESSENTIAL_DATA));
		assertThat(contextCaptor.getValue().getJspContext(), sameInstance(JspContext.NULL_CONTEXT));
	}

	@Test
	public void testProjectData() {
		when(systemSetupContext.getJspContext()).thenReturn(JspContext.NULL_CONTEXT);
		setup.projectData(systemSetupContext);
		ArgumentCaptor<LifterContext> contextCaptor = ArgumentCaptor.forClass(LifterContext.class);
		verify(executionStrategy).execute(contextCaptor.capture());
		assertThat(contextCaptor.getValue().getPhase(), equalTo(Phase.PROJECT_DATA));
		assertThat(contextCaptor.getValue().getJspContext(), sameInstance(JspContext.NULL_CONTEXT));
	}

	@Test
	public void testAnnotationOnProjectData() throws NoSuchMethodException, SecurityException {
		Method method = NonHybrisCallingLifterSystemSetup.class.getMethod("projectData", SystemSetupContext.class);
		SystemSetup annotation = method.getDeclaredAnnotation(SystemSetup.class);
		assertThat(annotation.type(), equalTo(SystemSetup.Type.PROJECT));
		assertThat(annotation.process(), equalTo(SystemSetup.Process.ALL));
	}
	
	@Test
	public void testAnnotationOnEssentialData() throws NoSuchMethodException, SecurityException {
		Method method = NonHybrisCallingLifterSystemSetup.class.getMethod("essentialData", SystemSetupContext.class);
		SystemSetup annotation = method.getDeclaredAnnotation(SystemSetup.class);
		assertThat(annotation.type(), equalTo(SystemSetup.Type.ESSENTIAL));
		assertThat(annotation.process(), equalTo(SystemSetup.Process.ALL));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedEssentialData() {
		when(configuration.getBoolean("lifter.essential_data.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.ESSENTIAL_DATA, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedProjectData() {
		when(configuration.getBoolean("lifter.project_data.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.PROJECT_DATA, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedBeforeUpdate() {
		when(configuration.getBoolean("lifter.before_update.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.BEFORE_UPDATE, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedBeforeInit() {
		when(configuration.getBoolean("lifter.before_init.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.BEFORE_INIT, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedAfterUpdate() {
		when(configuration.getBoolean("lifter.after_update.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.AFTER_UPDATE, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedAfterInit() {
		when(configuration.getBoolean("lifter.after_init.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.AFTER_INIT, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}
	
	@Test
	public void testExecuteSuppressedOnUnattendedStartup() {
		when(configuration.getBoolean("lifter.startup.unattended.allowed", true)).thenReturn(Boolean.FALSE);
		setup.execute(Phase.STARTUP, JspContext.NULL_CONTEXT);
		verify(executionStrategy, never()).execute(any(LifterContext.class));
	}

}
