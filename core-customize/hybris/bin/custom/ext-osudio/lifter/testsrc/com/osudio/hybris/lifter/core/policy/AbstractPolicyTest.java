package com.osudio.hybris.lifter.core.policy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableList;
import com.osudio.hybris.lifter.model.LifterTaskModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;


@UnitTest
public class AbstractPolicyTest {
	
	public class TestPolicyImpl extends AbstractPolicy {
		
		@Override
		public boolean isExecutionGranted(String id, String signature) {
			return false;
		}
	}

	private TestPolicyImpl testPolicy;
	private ModelService modelService;
	private FlexibleSearchService flexibleSearchService;

	@Before
	public void setUp() {
		this.testPolicy = mock(TestPolicyImpl.class);	
		modelService = mock(ModelService.class, Mockito.RETURNS_DEEP_STUBS);
		flexibleSearchService = mock(FlexibleSearchService.class, Mockito.RETURNS_DEEP_STUBS);
		doCallRealMethod().when(testPolicy).setFlexibleSearchService(flexibleSearchService);
		doCallRealMethod().when(testPolicy).getFlexibleSearchService();
		testPolicy.setFlexibleSearchService(flexibleSearchService);
		doCallRealMethod().when(testPolicy).getModelService();
		doCallRealMethod().when(testPolicy).setModelService(modelService);
		testPolicy.setModelService(modelService);
	}

	@Test
	public void testGetOrCreateTaskShouldCreate() {
		String code = "code-id";
		String signature = "dskdjkshk";
		when(testPolicy.getOrCreateTask(code, signature, false)).thenCallRealMethod();
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(modelService.create(LifterTaskModel.class)).thenReturn(lifterTaskModel);
		doNothing().when(modelService).save(lifterTaskModel);
		assertThat(testPolicy.findLifterTask(code), nullValue());
		testPolicy.getOrCreateTask(code, signature, false);
		verify(modelService).create(LifterTaskModel.class);
		verify(lifterTaskModel).setCode(code);
		verify(lifterTaskModel).setSignature(signature);
		verify(modelService).save(lifterTaskModel);
	}
	
	@Test
	public void testGetOrCreateTaskShouldCreateEmptySignature() {
		String code = "code-id";
		String signature = null;
		when(testPolicy.getOrCreateTask(code, signature, false)).thenCallRealMethod();
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(modelService.create(LifterTaskModel.class)).thenReturn(lifterTaskModel);
		doNothing().when(modelService).save(lifterTaskModel);
		assertThat(testPolicy.findLifterTask(code), nullValue());
		testPolicy.getOrCreateTask(code, signature, false);
		verify(modelService).create(LifterTaskModel.class);
		verify(lifterTaskModel).setCode(code);
		verify(lifterTaskModel).setSignature("");
		verify(modelService).save(lifterTaskModel);
	}

	@Test(expected=RuntimeException.class)
	public void testGetOrCreateTaskSignatureMismatch() {
		String code = "code-id";
		String signature = "dskdjkshk";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(lifterTaskModel.getCode()).thenReturn(code);
		when(lifterTaskModel.getSignature()).thenReturn("fjkdkjfjkd");
		when(testPolicy.findLifterTask(code)).thenReturn(lifterTaskModel);
		when(testPolicy.getOrCreateTask(code, signature, true)).thenCallRealMethod();
		testPolicy.getOrCreateTask(code, signature, true);
	}
	
	
	
	@Test
	public void testFindLifterTaskNonExistent() {
		String code = "code-id";
		when(flexibleSearchService.<LifterTaskModel>search(any(FlexibleSearchQuery.class))).thenReturn(new SearchResultImpl<LifterTaskModel>(ImmutableList.<LifterTaskModel>of(), 0,0,0));;
		when(testPolicy.findLifterTask(code)).thenCallRealMethod();
		assertThat(testPolicy.findLifterTask(code), nullValue());
		ArgumentCaptor<FlexibleSearchQuery> captor = ArgumentCaptor.forClass(FlexibleSearchQuery.class);
		verify(flexibleSearchService).search(captor.capture());
		assertThat(captor.getValue().getQueryParameters().entrySet(), hasSize(1));
		assertThat(captor.getValue().getQueryParameters().keySet(), contains(LifterTaskModel.CODE));
		assertThat((String)captor.getValue().getQueryParameters().values().iterator().next(), equalTo(code));
		assertThat(captor.getValue().isNeedTotal(), equalTo(false));
	}
	
	@Test
	public void testFindLifterTaskSingleResult() {
		String code = "code-id";
		String signature = "dskdjkshk";
		LifterTaskModel lifterTaskModel = new LifterTaskModel();
		lifterTaskModel.setCode(code);
		lifterTaskModel.setSignature(signature);
		when(testPolicy.findLifterTask(code)).thenCallRealMethod();
		when(flexibleSearchService.<LifterTaskModel>search(any(FlexibleSearchQuery.class)).getResult()).thenReturn(ImmutableList.<LifterTaskModel>of(lifterTaskModel));
		LifterTaskModel foundLifterTask = testPolicy.findLifterTask(code);
		assertThat(foundLifterTask, equalTo(lifterTaskModel));
		assertThat(foundLifterTask.getCode(), equalTo(code));
		assertThat(foundLifterTask.getSignature(), equalTo(signature));		
	}

	@Test
	public void testFindLifterTaskMultipleResultsReturnsFirst() {
		String code = "code-id";
		String signature = "dskdjkshk";
		LifterTaskModel lifterTaskModel = new LifterTaskModel();
		lifterTaskModel.setCode(code);
		lifterTaskModel.setSignature(signature);
		
		LifterTaskModel lifterTaskModel2 = new LifterTaskModel();
		lifterTaskModel2.setCode(code);
		lifterTaskModel2.setSignature("cjkdjkjdkl");
		when(testPolicy.findLifterTask(code)).thenCallRealMethod();
		when(flexibleSearchService.<LifterTaskModel>search(any(FlexibleSearchQuery.class)).getResult()).thenReturn(ImmutableList.<LifterTaskModel>of(lifterTaskModel, lifterTaskModel2));
		LifterTaskModel foundLifterTask = testPolicy.findLifterTask(code);
		assertThat(foundLifterTask, equalTo(lifterTaskModel));
		assertThat(foundLifterTask.getCode(), equalTo(code));
		assertThat(foundLifterTask.getSignature(), equalTo(signature));		
	}

	@Test
	public void testToString() {
		TestPolicyToStringInnerClass policy1 = new TestPolicyToStringInnerClass();
		TestPolicyOtherToStringInnerClass policy2 = new TestPolicyOtherToStringInnerClass();
		
		assertThat(policy1.toString(), equalTo("TestPolicyToStringInnerClass"));
		assertThat(policy2.toString(), equalTo("TestPolicyOtherToStringInnerClass"));
	}
	
	private class TestPolicyToStringInnerClass extends AbstractPolicy {
		@Override
		public boolean isExecutionGranted(String id, String signature) {
			return false;
		}
	}
	
	private class TestPolicyOtherToStringInnerClass extends AbstractPolicy {
		@Override
		public boolean isExecutionGranted(String id, String signature) {
			return false;
		}
	}
}
