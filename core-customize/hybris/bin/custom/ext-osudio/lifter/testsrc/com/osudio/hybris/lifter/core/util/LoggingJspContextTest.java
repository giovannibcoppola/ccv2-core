package com.osudio.hybris.lifter.core.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class LoggingJspContextTest {

	private Logger logger;
	private LoggingJspContext jspContext;

	@Before
	public void setup() {
		logger = mock(Logger.class);
		jspContext = new LoggingJspContext(logger);
	}
	
	@Test
	public void testLoggingJspContextPrint() throws Exception {
		String message = "text string";
		jspContext.print(message);
		verify(logger).info(message);
	}
	
	@Test
	public void testLoggingJspContextPrintLnNoExtraNewLine() throws Exception {
		String message = "text string";
		jspContext.println(message);
		verify(logger).info(message);
	}

}
