package com.osudio.hybris.lifter.core.policy;

import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class RunOncePolicyDevelopmentModeTest {

	private RunOncePolicy runOncePolicy;
	
	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;
	
	@Before
	public void setUp() {
		this.runOncePolicy = new RunOncePolicy();
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(runOncePolicy, "configurationService", configurationService, ConfigurationService.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
	}
	
	@Test
	public void shouldNotFailOnSignature_developmentmode() {

		// given
		when(configuration.getBoolean("lifter.developmentmode", false)).thenReturn(Boolean.TRUE);
		
		// when
		boolean failOnSignature = runOncePolicy.isFailOnSignature("myfile.impex");
		
		// then
		Assert.assertFalse(failOnSignature);
	}

	@Test
	public void shouldFailOnSignature() {

		// given
		when(configuration.getBoolean("lifter.developmentmode", false)).thenReturn(Boolean.FALSE);
		
		// when
		boolean failOnSignature = runOncePolicy.isFailOnSignature("myfile.impex");
		
		// then
		Assert.assertTrue(failOnSignature);
	}

	@Test
	public void shouldFailOnSignature_not_development_draft() {

		// given
		when(configuration.getBoolean("lifter.developmentmode", false)).thenReturn(Boolean.FALSE);
		
		// when
		boolean failOnSignature = runOncePolicy.isFailOnSignature("myfile.draft.impex");
		
		// then
		Assert.assertFalse(failOnSignature);
	}
	

	@Test
	public void shouldFailOnSignatureOnException() {

		// given
		when(configuration.getBoolean("lifter.developmentmode", false)).thenThrow(new RuntimeException("bla"));
		
		// when
		boolean failOnSignature = runOncePolicy.isFailOnSignature("myfile.impex");
		
		// then
		Assert.assertTrue(failOnSignature);
	}
}
