package com.osudio.hybris.lifter.core.policy;

import com.osudio.hybris.lifter.model.LifterTaskModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
public class RunIfChangedPolicyTest {

	private RunIfChangedPolicy runIfChangedPolicy;
	private ModelService modelService;

	@Before
	public void setUp() {
		this.runIfChangedPolicy = mock(RunIfChangedPolicy.class);	
		modelService = mock(ModelService.class, Mockito.RETURNS_DEEP_STUBS);
		when(runIfChangedPolicy.getModelService()).thenCallRealMethod();
		doCallRealMethod().when(runIfChangedPolicy).setModelService(modelService);
		runIfChangedPolicy.setModelService(modelService);
		
	}

	@Test
	public void testIsExecutionGrantedRejectedOnModelSaveException() {
		String code = "dsjfdfkjd";
		String signature = "kfdjfhdj";
		LifterTaskModel lifterTaskModel = mock(LifterTaskModel.class);
		when(lifterTaskModel.getCode()).thenReturn(code);
		when(lifterTaskModel.getSignature()).thenReturn("otherSignature");
		when(runIfChangedPolicy.findLifterTask(eq(code))).thenReturn(lifterTaskModel);
		when(runIfChangedPolicy.getOrCreateTask(eq(code), eq(signature), anyBoolean())).thenReturn(lifterTaskModel);
		doThrow(ModelSavingException.class).when(modelService).save(lifterTaskModel);
		when(runIfChangedPolicy.isExecutionGranted(code, signature)).thenCallRealMethod();
		runIfChangedPolicy.isExecutionGranted(code, signature);
		verify(runIfChangedPolicy).findLifterTask(code);
		verify(runIfChangedPolicy).getOrCreateTask(code, signature, false);
		verify(modelService).save(lifterTaskModel);
	}

}
