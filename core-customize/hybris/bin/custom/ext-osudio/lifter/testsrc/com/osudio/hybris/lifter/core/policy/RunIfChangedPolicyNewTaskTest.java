package com.osudio.hybris.lifter.core.policy;

import com.osudio.hybris.lifter.model.LifterTaskModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@UnitTest
@RunWith(Parameterized.class)
public class RunIfChangedPolicyNewTaskTest {
	
	@Parameters(name = "{index}: taskCode={0}, taskSignature={1}")
	public static Collection<Object[]> data() {
        return Arrays.asList(     
                 new Object[]{ "test-if", "signature1", true },
                 new Object[]{ "test-if", "signature2", true },
                 new Object[]{ "test-not-found", "signature3", true },
                 new Object[]{ null, "signature3", true }
                 );
	}
	
	private final String taskCode;
	private final String taskSignature;
	private final boolean granted;
	
	public RunIfChangedPolicyNewTaskTest(String taskCode, String taskSignature, boolean granted) {
		this.taskCode = taskCode;
		this.taskSignature = taskSignature;
		this.granted = granted;
	}
	
	@Test
	public void testIsExecutionGrantedNew() throws Exception {
		RunIfChangedPolicy policy = new RunIfChangedPolicy() {
			@Override
			public LifterTaskModel findLifterTask(String code) {
				return null;
			}
			@Override
			protected LifterTaskModel getOrCreateTask(String id, String signature, boolean failOnSignature) {
				LifterTaskModel task = new LifterTaskModel();
				task.setCode(id);
				task.setSignature(signature);
				return task;
			}
		};
		ModelService modelService = mock(ModelService.class);
		policy.setModelService(modelService);
		
		assertEquals(granted, policy.isExecutionGranted(taskCode, taskSignature));
	}
	
}
