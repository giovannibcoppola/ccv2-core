package com.osudio.hybris.lifter.core.handler;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.junit.Test;

import com.google.common.base.Charsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
public class FileContentHandlerTest {
	
	public class PatternFileContentHandler extends FileContentHandler {
		private String pattern;
		
		PatternFileContentHandler(String pattern) {
			this.pattern = pattern;
		}
		@Override
		protected String getAcceptableFileIdPattern() {
			return pattern;
		}
		@Override
		protected void handle(InputStream content) {
			// do nothing
		}
	}

	@Test
	public void testCanHandleNull() {
		String pattern = "pattern";
		FileContentHandler handler = new PatternFileContentHandler(pattern);
		assertThat(handler.canHandle(null), equalTo(false));
	}
	
	@Test(expected=NullPointerException.class)
	public void testRejectNullPattern() {
		String pattern = "pattern";
		FileContentHandler handler = new PatternFileContentHandler(null);
		assertThat(handler.canHandle(pattern), equalTo(false));
	}
	
	@Test
	public void testCanHandleSimple() {
		String pattern = "pattern";
		FileContentHandler handler = new PatternFileContentHandler(pattern);
		assertThat(handler.canHandle("pattern"), equalTo(true));
	}
	
	@Test
	public void testCanHandleRegex() {
		String pattern = "([a-z]-){2}[a-z]";
		FileContentHandler handler = new PatternFileContentHandler(pattern);
		assertThat(handler.canHandle("a-b-c"), equalTo(true));
		assertThat(handler.canHandle("a-b-c-d"), equalTo(false));
	}
	
	@Test
	public void testCanHandleRegexPath() {
		String pattern = "^/.*/c";
		FileContentHandler handler = new PatternFileContentHandler(pattern);
		assertThat(handler.canHandle("/a/b/c"), equalTo(true));
		assertThat(handler.canHandle("/a/b/d"), equalTo(false));
	}

	@Test
	public void testHandleFileEqualsContent() throws Exception {
		final String fileContent = "these are the contents of this temp file";
		final MutableBoolean called = new MutableBoolean(false);
		FileContentHandler handler = new FileContentHandler() {

			@Override
			protected void handle(InputStream content) {
				try {
					assertThat(IOUtils.toString((InputStream) content, Charsets.UTF_8), equalTo(fileContent));
				} catch (IOException e) {
					new ContentHandlerException(e);
				}
				called.setValue(true);
			}

			@Override
			protected String getAcceptableFileIdPattern() {
				return ".*";
			}
			
		};
		File file = File.createTempFile(this.getClass().getName(), "testHandleFileEqualsContent");
		try {
			FileUtils.write(file, fileContent);
			handler.handle(file);
			assertThat(called.booleanValue(), equalTo(true));
		} finally {
			FileUtils.deleteQuietly(file);
		}
	}
	
	@Test
	public void testHandleFileWithException() throws IOException {
		final String fileContent = "these are the contents of this temp file";
		FileContentHandler handler = new FileContentHandler() {

			@Override
			protected void handle(InputStream content) {
				throw new RuntimeException("testio");
			}

			@Override
			protected String getAcceptableFileIdPattern() {
				return ".*";
			}
			
		};
		File file = File.createTempFile(this.getClass().getName(), "testHandleFileWithException");
		try {
			FileUtils.write(file, fileContent);
			try {
				handler.handle(file);
				fail("Should have thrown");
			} catch (ContentHandlerException e) {
				assertThat(e.getMessage(), equalTo("testio"));
				assertEquals(RuntimeException.class, e.getCause().getClass());
			}
		} finally {
			FileUtils.deleteQuietly(file);
		}
	}

	@Test
	public void testProcessExceptionException() {
		PatternFileContentHandler handler = new PatternFileContentHandler(".*");
		try {
			handler.processException("value");
			fail("Should have thrown an exception");
		} catch (ContentHandlerException e) {
			assertThat(e.getMessage(), equalTo("value"));
		}
	}	
	
	@Test
	public void testProcessExceptionStringNull() {
		PatternFileContentHandler handler = new PatternFileContentHandler(".*");
		try {
			handler.processException((String)null);
			fail("Should have thrown an exception");
		} catch (ContentHandlerException e) {
			assertThat(e.getMessage(), nullValue());
		}
	}	
	
	@Test
	public void testProcessExceptionExceptionNull() {
		PatternFileContentHandler handler = new PatternFileContentHandler(".*");
		try {
			handler.processException((Exception)null);
			fail("Should have thrown an exception");
		} catch (ContentHandlerException e) {
			fail("Should have thrown a nullpointerexception");
		} catch (NullPointerException npe) {
			// expected
		}
	}	
	
	@Test
	public void testProcessExceptionWrapsException() {
		PatternFileContentHandler handler = new PatternFileContentHandler(".*");
		try {
			handler.processException(new IOException("test"));
			fail("Should have thrown an exception");
		} catch (ContentHandlerException e) {
			assertThat(e.getMessage(), equalTo("test"));
			assertThat(e.getCause(), instanceOf(IOException.class));
		}
	}	

	@Test
	public void testProcessExceptionPropagatesContentHandlerException() {
		PatternFileContentHandler handler = new PatternFileContentHandler(".*");
		IllegalArgumentException originalCause = new IllegalArgumentException();
		try {
			handler.processException(new ContentHandlerException("test", originalCause));
			fail("Should have thrown an exception");
		} catch (ContentHandlerException e) {
			assertThat(e.getMessage(), equalTo("test"));
			assertThat(e.getCause(), sameInstance((Throwable)originalCause));
		}
	}
	
}
