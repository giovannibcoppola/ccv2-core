package com.viking.commands.impl;

import com.pensio.api.PaymentRequestResponse;
import com.pensio.api.PensioAPIException;
import com.pensio.api.request.PaymentRequest;
import com.viking.commands.CreateRequestCommand;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class AltaPayCreateRequestCommand implements CreateRequestCommand {
    private static final Logger LOG = Logger.getLogger(AltaPayCreateRequestCommand.class);

    private AltapayHelper altapayHelper;

    @Override
    public PaymentRequestResponse perform(PaymentRequest paymentRequest) {
        PaymentRequestResponse paymentRequestResponse = null;

        try {
            boolean login =  getAltapayHelper().loginAltapay();
            if(login) {
                paymentRequestResponse = getAltapayHelper().getPensioMerchantAPI().createPaymentRequest(paymentRequest);
                LOG.info("payment identification number : " + paymentRequestResponse.getPaymentRequestId());
                LOG.info("AltaPay redirect url : " + paymentRequestResponse.getUrl());
            }else {
                LOG.error("AltaPay login : " + login);
            }
        } catch (PensioAPIException e) {
            LOG.error("Altapay createPaymentRequest failed.", e);
        }
        return paymentRequestResponse;
    }

    public AltapayHelper getAltapayHelper() {
        return altapayHelper;
    }

    @Required
    public void setAltapayHelper(AltapayHelper altapayHelper) {
        this.altapayHelper = altapayHelper;
    }
}
