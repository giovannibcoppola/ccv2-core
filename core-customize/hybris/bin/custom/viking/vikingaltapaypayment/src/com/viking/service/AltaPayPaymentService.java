package com.viking.service;

import com.pensio.api.PaymentRequestResponse;
import com.viking.altapaypayment.data.AltapayAuthResponseData;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

public interface AltaPayPaymentService {
    PaymentRequestResponse createPaymentRequest();

    PaymentTransactionEntryModel createPaymentPreAuthTransaction(final AltapayAuthResponseData altapayAuthResponseData, final String cartGuid);

    void handleFailedPaymentPreauth(final String cartGuid);
}
