package com.viking.facades;

import com.viking.altapaypayment.data.AltapayAuthResponseData;

import java.text.ParseException;

public interface AltaPayPaymentFacade {
    String createPaymentRequest() throws ParseException;

    boolean afterPaymentPreAuthorized(final AltapayAuthResponseData altapayAuthResponseData, final String cartGuid);

    void afterFailedPaymentPreAuth(final String cartGuid);
}
