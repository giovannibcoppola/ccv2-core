package com.viking.service.impl;

import com.pensio.Amount;
import com.pensio.api.PaymentRequestResponse;
import com.pensio.api.request.*;
import com.viking.altapaypayment.data.AltapayAuthResponseData;
import com.viking.commands.CreateRequestCommand;
import com.viking.constants.VikingaltapaypaymentConstants;
import com.viking.enums.PaymentProvider;
import com.viking.service.AltaPayPaymentService;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.commands.factory.CommandFactory;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.impl.DefaultPaymentServiceImpl;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;

import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.dto.TransactionStatus.REJECTED;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.INVALID_REQUEST;
import static de.hybris.platform.payment.dto.TransactionStatusDetails.SUCCESFULL;
import static de.hybris.platform.payment.enums.PaymentTransactionType.AUTHORIZATION;

public class AltaPayPaymentServiceImpl extends DefaultPaymentServiceImpl implements AltaPayPaymentService {
    private static final Logger LOG = Logger.getLogger(AltaPayPaymentServiceImpl.class);
    private CommandFactory commandFactory;
    private CommerceCartService commerceCartService;
    private BaseSiteService baseSiteService;
    private CartService cartService;

    private static final String ALTAPAY = PaymentProvider.ALTAPAY.getCode();
    private static final String TERMINAL = "altapay.merchant.terminal";
    private static final String PAYMENT_OK_URL = "altapay.payment.ok.url";
    private static final String PAYMENT_FAILED_URL = "altapay.payment.failed.url";
    private static final String PREAUTH  = "preauth";

    @Override
    public PaymentRequestResponse createPaymentRequest() {
        try {
            final CreateRequestCommand createRequestCommand = commandFactory.createCommand(CreateRequestCommand.class);
            return createRequestCommand.perform(fillPaymentRequest(cartService));
        } catch (CommandNotSupportedException e) {
            LOG.error("Create payment request failed", e);
            return null;
        }
    }

    @Override
    public PaymentTransactionEntryModel createPaymentPreAuthTransaction(final AltapayAuthResponseData altapayAuthResponseData, final String cartGuid) {
        final PaymentTransactionEntryModel paymentTransactionEntry = getModelService().create(PaymentTransactionEntryModel.class);
        final PaymentTransactionModel paymentTransaction = getModelService().create(PaymentTransactionModel.class);
        final CartModel cartModel = commerceCartService.getCartForGuidAndSite(cartGuid, baseSiteService.getCurrentBaseSite());

        String merchantTransactionCode = cartModel.getUser().getUid() + "-" + UUID.randomUUID();
        paymentTransaction.setCode(merchantTransactionCode);
        paymentTransaction.setPlannedAmount(BigDecimal.valueOf(cartModel.getTotalPrice()));
        paymentTransaction.setOrder(cartModel);
        paymentTransaction.setPaymentProvider(ALTAPAY);
        paymentTransaction.setRequestId(altapayAuthResponseData.getShopOrderid());
        paymentTransaction.setCurrency(cartModel.getCurrency());

        getModelService().save(paymentTransaction);
        getModelService().refresh(cartModel);

        paymentTransactionEntry.setCode(getNewPaymentTransactionEntryCode(paymentTransaction, AUTHORIZATION));
        paymentTransactionEntry.setPaymentId(altapayAuthResponseData.getPaymentId());
        paymentTransactionEntry.setRequestId(altapayAuthResponseData.getTransactionId());
        paymentTransactionEntry.setAmount(new BigDecimal(altapayAuthResponseData.getAmount()));
        paymentTransactionEntry.setCurrency(cartModel.getCurrency());
        paymentTransactionEntry.setType(AUTHORIZATION);
        paymentTransactionEntry.setTime(new Date());
        paymentTransactionEntry.setPaymentTransaction(paymentTransaction);

        if (cartModel.getCurrency() != null) {
            paymentTransactionEntry.setCurrency(cartModel.getCurrency());
        }

        if (isAuthSuccess(altapayAuthResponseData)) {
            paymentTransactionEntry.setTransactionStatus(ACCEPTED.toString());
            paymentTransactionEntry.setTransactionStatusDetails(SUCCESFULL.toString());
        } else {
            paymentTransactionEntry.setTransactionStatus(REJECTED.toString());
            paymentTransactionEntry.setTransactionStatusDetails(INVALID_REQUEST.toString());
        }

        getModelService().save(paymentTransactionEntry);
        getModelService().refresh(paymentTransaction);

        cartService.setSessionCart(cartModel);
        
        return paymentTransactionEntry;
    }

    @Override
    public void handleFailedPaymentPreauth(final String cartGuid) {
        final CartModel cartModel = commerceCartService.getCartForGuidAndSite(cartGuid, baseSiteService.getCurrentBaseSite());
        cartService.setSessionCart(cartModel);
    }

    private boolean isAuthSuccess(AltapayAuthResponseData altapayAuthResponseData) {
        return VikingaltapaypaymentConstants.SUCCEEDED.equalsIgnoreCase(altapayAuthResponseData.getStatus()) && altapayAuthResponseData.getPaymentStatus().equalsIgnoreCase(PREAUTH);
    }

    protected PaymentRequest fillPaymentRequest(final CartService cartService) {
        final String terminal = Config.getParameter(TERMINAL);
        final CartModel cartModel = cartService.getSessionCart();
        final CurrencyModel currency = cartModel.getCurrency();
        final String isocode = currency.getIsocode();
        final String orderCode = cartModel.getCode();
        final PaymentRequest paymentRequest = new PaymentRequest();

        paymentRequest.setShopOrderId(orderCode); //The id of the order in your webshop. This is what we will post back to you so you know which order a given payment is associated with.
        paymentRequest.setTerminal(terminal);
        paymentRequest.setAuthType(AuthType.payment);
        paymentRequest.setAmount(Amount.get(cartModel.getTotalPrice(), com.pensio.Currency.valueOf(isocode)));

        final PaymentRequestConfig paymentRequestConfig = new PaymentRequestConfig();
        final String languageIsoCode = cartModel.getStore().getDefaultLanguage().getIsocode();
        final String cartGuid = cartModel.getGuid();
        paymentRequestConfig.setCallbackOk(MessageFormat.format(Config.getParameter(PAYMENT_OK_URL), languageIsoCode, cartGuid));
        paymentRequestConfig.setCallbackFail(MessageFormat.format(Config.getParameter(PAYMENT_FAILED_URL), languageIsoCode, cartGuid));
        paymentRequest.setConfig(paymentRequestConfig);


        final CustomerInfo customerInfo = new CustomerInfo();
        final UserModel cartUser = cartModel.getUser();
        customerInfo.setEmail(cartUser.getUid());
        customerInfo.setUsername(cartUser.getName());
        customerInfo.setBillingAddress(getBillingAddress(cartModel));
        customerInfo.setShippingAddress(getShippingAddress(cartModel));

        paymentRequest.setCustomerInfo(customerInfo);

        return paymentRequest;
    }

    private CustomerInfoAddress getBillingAddress(final CartModel cartModel) {
        return createAddress(cartModel.getDeliveryAddress());
    }

    private CustomerInfoAddress getShippingAddress(final CartModel cartModel) {
        return createAddress(cartModel.getPaymentAddress());
    }

    private CustomerInfoAddress createAddress(final AddressModel addressModel) {
        final CustomerInfoAddress address = new CustomerInfoAddress();
        final StringBuilder paymentAddressLines = new StringBuilder(addressModel.getLine1());

        if (StringUtils.isNotBlank(addressModel.getLine2())) {
            paymentAddressLines.append(" " + addressModel.getLine2());
        }

        address.setAddress(paymentAddressLines.toString());
        address.setCity(addressModel.getTown());
        address.setCountry(addressModel.getCountry() != null ? addressModel.getCountry().getIsocode() : "");
        address.setFirstname(addressModel.getFirstname());
        address.setLastname(addressModel.getLastname());
        address.setPostal(addressModel.getPostalcode());
        address.setRegion(addressModel.getRegion() != null ? addressModel.getRegion().getName() : "");

        return address;
    }

    @Required
    public void setCommandFactory(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setCommerceCartService(CommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }

    @Required
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }
}