package com.viking.commands.impl;

import com.pensio.api.PensioAPIException;
import com.pensio.api.PensioMerchantAPI;
import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;

public class AltapayHelper {
    private static final Logger LOG = Logger.getLogger(AltapayHelper.class);

    private static final String BASE_URL = "altapay.testgateway.base.url";
    private static final String USERNAME = "altapay.merchant.username";
    private static final String PASSWORD = "altapay.merchant.password";

    private PensioMerchantAPI pensioMerchantAPI;

    public boolean loginAltapay() {
        final String baseUrl = Config.getParameter(BASE_URL);
        final String userName = Config.getParameter(USERNAME);
        final String password = Config.getParameter(PASSWORD);

        pensioMerchantAPI = new PensioMerchantAPI(baseUrl,userName, password);

        try {
            return pensioMerchantAPI.login();
        } catch (PensioAPIException e) {
            LOG.error("AltaPay login : " + e);

            return false;
        }
    }

    public PensioMerchantAPI getPensioMerchantAPI() {
        return pensioMerchantAPI;
    }
}
