package com.viking.cronjobs;


import com.viking.commands.impl.AltaPayCaptureCommand;
import com.viking.commands.impl.AltapayHelper;
import com.viking.model.AltapayCaptureCronJobModel;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.site.BaseSiteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.payment.enums.PaymentTransactionType.AUTHORIZATION;
import static de.hybris.platform.payment.enums.PaymentTransactionType.CAPTURE;

public class AltapayCaptureJob extends AbstractJobPerformable<AltapayCaptureCronJobModel> {

    private static final Logger LOG = LogManager.getLogger(AltapayCaptureJob.class);

    private static final String TRANSACTION_STATUS_DETAILS = "SUCCESFULL";
    private static final String PUBLICWEBSHOP_SITE_ID = "publicwebshop";

    private static final String ORDERS_FOR_CAPTURE_QUERY = "SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE + "} " +
            "WHERE {" + OrderModel.STATUS + "} = ?" + OrderModel.STATUS + " " +
            "AND {" + OrderModel.DELIVERYSTATUS + "} = ?" + OrderModel.DELIVERYSTATUS + " " +
            "AND {" + OrderModel.PAYMENTSTATUS + "} IS NULL " +
            "AND {" + OrderModel.SITE + "} = ?" + OrderModel.SITE;

    private AltapayHelper altapayHelper;
    private AltaPayCaptureCommand altaPayCaptureCommand;
    private BaseSiteService baseSiteService;

    @Override
    public PerformResult perform(final AltapayCaptureCronJobModel cronJob) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(ORDERS_FOR_CAPTURE_QUERY);
        query.addQueryParameter(OrderModel.STATUS, OrderStatus.COMPLETED);
        query.addQueryParameter(OrderModel.DELIVERYSTATUS, DeliveryStatus.SHIPPED);
        query.addQueryParameter(OrderModel.SITE, baseSiteService.getBaseSiteForUID(PUBLICWEBSHOP_SITE_ID));
        final List<OrderModel> orders = flexibleSearchService.<OrderModel>search(query).getResult();

        LOG.info(MessageFormat.format("Starting capture for {0} orders", orders.size()));

        final boolean login = altapayHelper.loginAltapay();

        if (!login) {
            LOG.error("Not able to login to Altapay to capture orders");
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        boolean hasCaptureErrors = false;

        for (final OrderModel order : orders) {
            if (clearAbortRequestedIfNeeded(cronJob)) {
                return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
            }

            LOG.debug("Capture payment for order " + order.getCode());
            final List<PaymentTransactionEntryModel> entries = getPaymentTransactionEntriesToCapture(order);

            hasCaptureErrors = capture(entries);
        }

        if (hasCaptureErrors) {
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        } else {
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }
    }

    private List<PaymentTransactionEntryModel> getPaymentTransactionEntriesToCapture(final OrderModel order) {
        return order.getPaymentTransactions().stream()
                .filter(this::isNotCaptured)
                .map(PaymentTransactionModel::getEntries)
                .flatMap(List::stream)
                .filter(entry -> AUTHORIZATION.equals(entry.getType()) && TRANSACTION_STATUS_DETAILS.equals(entry.getTransactionStatusDetails()))
                .collect(Collectors.toList());
    }

    private boolean capture(final List<PaymentTransactionEntryModel> entries) {
        boolean hasErrors = false;

        for (final PaymentTransactionEntryModel entry : entries) {
            boolean result = altaPayCaptureCommand.perform(entry.getPaymentId());

            if (result) {
                final PaymentTransactionEntryModel capturedEntry = modelService.clone(entry);
                capturedEntry.setType(CAPTURE);

                final AbstractOrderModel order = entry.getPaymentTransaction().getOrder();
                order.setPaymentStatus(PaymentStatus.PAID);

                modelService.saveAll(capturedEntry, order);
            } else {
                hasErrors = true;
            }
        }
        return hasErrors;
    }

    private boolean isNotCaptured(final PaymentTransactionModel transaction) {
        return !transaction.getEntries().stream().anyMatch(entry -> CAPTURE.equals(entry.getType()));
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

    @Required
    public void setAltapayHelper(final AltapayHelper altapayHelper) {
        this.altapayHelper = altapayHelper;
    }

    @Required
    public void setAltaPayCaptureCommand(final AltaPayCaptureCommand altaPayCaptureCommand) {
        this.altaPayCaptureCommand = altaPayCaptureCommand;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
