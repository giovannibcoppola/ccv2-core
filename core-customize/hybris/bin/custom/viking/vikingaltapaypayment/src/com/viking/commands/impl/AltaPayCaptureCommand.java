package com.viking.commands.impl;

import com.pensio.api.PensioAPIException;
import com.pensio.api.generated.APIResponse;
import com.pensio.api.request.CaptureReservationRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;

public class AltaPayCaptureCommand {
    private static final Logger LOG = Logger.getLogger(AltaPayCaptureCommand.class);

    private static final String CAPTURE_SUCCESS = "Success";
    private static final String CAPTURE_STATUS = "captured";

    private AltapayHelper altapayHelper;

    public boolean perform(String paymentId) {
        final CaptureReservationRequest captureReservationRequest = new CaptureReservationRequest(paymentId);

        boolean result = false;

        try {
            final APIResponse capture = altapayHelper.getPensioMerchantAPI().capture(captureReservationRequest);

            if(isSuccess(capture, paymentId) && isCaptured(capture, paymentId)) {
                result = true;
            }
        } catch (PensioAPIException e) {
            LOG.error(MessageFormat.format("Altapay capture for paymentId {0} failed.", paymentId), e);
        }

        return result;
    }

    private boolean isCaptured(final APIResponse capture, final String paymentId) {
        try {
            final String transactionStatus = capture.getBody().getTransactions().getTransaction().get(0).getTransactionStatus();
            return StringUtils.isNotBlank(transactionStatus) && transactionStatus.equalsIgnoreCase(CAPTURE_STATUS);
        } catch (NullPointerException e) {
            LOG.debug(MessageFormat.format("Not able to get transaction status for paymentID {0}", paymentId), e);
            return false;
        }
    }

    private boolean isSuccess(final APIResponse capture, final String paymentId) {
        try {
            final String captureResult = capture.getBody().getCaptureResult();
            return StringUtils.isNotBlank(captureResult) && captureResult.equalsIgnoreCase(CAPTURE_SUCCESS);
        } catch (NullPointerException e) {
            LOG.debug(MessageFormat.format("Not able to get capture result for paymentID {0}", paymentId), e);
            return false;
        }
    }

    @Required
    public void setAltapayHelper(AltapayHelper altapayHelper) {
        this.altapayHelper = altapayHelper;
    }
}
