package com.viking.commands;

import com.pensio.api.PaymentRequestResponse;
import com.pensio.api.request.PaymentRequest;
import de.hybris.platform.payment.commands.Command;

public interface CreateRequestCommand extends Command<PaymentRequest, PaymentRequestResponse> {
}
