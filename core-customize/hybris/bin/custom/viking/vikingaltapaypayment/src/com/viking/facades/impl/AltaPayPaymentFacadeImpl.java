package com.viking.facades.impl;

import com.pensio.api.PaymentRequestResponse;
import com.viking.altapaypayment.data.AltapayAuthResponseData;
import com.viking.facades.AltaPayPaymentFacade;
import com.viking.service.AltaPayPaymentService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.ObjectUtils;

public class AltaPayPaymentFacadeImpl implements AltaPayPaymentFacade {
    private AltaPayPaymentService altaPayPaymentService;
    private CartService cartService;
    private ModelService modelService;

    @Override
    public String createPaymentRequest() {
        final PaymentRequestResponse response = altaPayPaymentService.createPaymentRequest();
        if (!ObjectUtils.isEmpty(response) && StringUtils.isNotBlank(response.getUrl().toString())) {
            return response.getUrl().toString();
        } else {
            return null;
        }
    }

    @Override
    public boolean afterPaymentPreAuthorized(final AltapayAuthResponseData altapayAuthResponseData, final String cartGuid) {
        final PaymentTransactionEntryModel paymentTransactionEntryModel = altaPayPaymentService.createPaymentPreAuthTransaction(altapayAuthResponseData, cartGuid);

        return paymentTransactionEntryModel != null && TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntryModel.getTransactionStatus());
    }

    @Override
    public void afterFailedPaymentPreAuth(final String cartGuid) {
        altaPayPaymentService.handleFailedPaymentPreauth(cartGuid);
    }

    @Required
    public void setAltaPayPaymentService(AltaPayPaymentService altaPayPaymentService) {
        this.altaPayPaymentService = altaPayPaymentService;
    }

    @Required
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}