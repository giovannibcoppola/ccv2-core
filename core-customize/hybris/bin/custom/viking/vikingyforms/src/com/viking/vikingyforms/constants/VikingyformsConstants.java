/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.viking.vikingyforms.constants;

/**
 * Global class for all Vikingyforms constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class VikingyformsConstants extends GeneratedVikingyformsConstants
{
	public static final String EXTENSIONNAME = "vikingyforms";

	private VikingyformsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
