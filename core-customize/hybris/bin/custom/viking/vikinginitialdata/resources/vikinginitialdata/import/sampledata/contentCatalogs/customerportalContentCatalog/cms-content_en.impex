# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
##******************************This file is generated - please do not edit it. It will be regenerated after every build.********************************#

$contentCatalog=customerportalContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$picture=media(code, $contentCV);
$siteResource=jar:com.viking.initialdata.constants.VikingInitialDataConstants&/vikinginitialdata/import/sampledata/contentCatalogs/$contentCatalog
$medias=medias(code, $contentCV);
$mediaContainer=media(qualifier, $contentCV)[lang=$lang];


# Language
$lang=en

# Site Logo Component
UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;SiteLogoComponent;/images/theme/logo_hybris_b2b_responsive.svg

# CMS Link Components
UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
#;;AddressBookLink;"Address Book"
;;ApprovalDashboardLink;"Approval Dashboard"
;;FAQLink;"FAQ"
;;HomepageNavLink;"Home"
;;PersonalDetailsLink;"Personal Details"
#;;UpdateEmailLink;"Email Address"
;;UpdatePasswordLink;"Password"
;;VikingHomeLink;"Home"
;;VikingVesselsLink;"Manage Vessels"



# Lightbox Banner for Mini Cart (banner is not localizable so we must create a separate banner per language)
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];&imageRef;folder(qualifier)[default='images'];altText
;;Powertools_358x45_HomeDeliveryBanner_EN_01.gif;$siteResource/images/banners/site/Powertools_358x45_HomeDeliveryBanner_EN_01.gif;;Powertools_358x45_HomeDeliveryBanner_EN_01.gif;;"VIKING We protect and save human lives all over the world"

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;LightboxHomeDeliveryBannerComponent;Powertools_358x45_HomeDeliveryBanner_EN_01.gif

## CMS Mini Cart Component
#UPDATE MiniCartComponent;$contentCV[unique=true];uid[unique=true];name;title[lang=$lang]
#;;MiniCart;Mini Cart;"YOUR SHOPPING CART"

# CMS tabs components
UPDATE CMSTabParagraphComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang];content[lang=$lang]
;;deliveryTab;Delivery;"<div class=""tab-delivery"">Please be aware that some items might be classified as dangerous goods, which may result in extended delivery times.</div>"

# JSP include components
UPDATE JspIncludeComponent;$contentCV[unique=true];uid[unique=true];title[lang=en]
;;ProductDetailsTabComponent;Product Details
;;ProductReviewsTabComponent;Reviews
;;ProductSpecsTabComponent;Specs

# CMS Footer Component
UPDATE FooterNavigationComponent;$contentCV[unique=true];uid[unique=true];notice[lang=$lang]
;;FooterNavigationComponent;"&copy; VIKING LIFE-SAVING EQUIPMENT A/S"

# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]
#;;QuickOrderInfoParagraphComponent;"You can add up to 25 valid SKUs below and add to cart. Stock is reserved once products are added to cart."

# CMS ProductReferences Components
UPDATE ProductReferencesComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;Similar;"You may also like..."
;;accessories;"Accessories"

# CMS PurchasedCategorySuggestion Components
UPDATE PurchasedCategorySuggestionComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]

# CMS CartSuggestion Components
UPDATE CartSuggestionComponent;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
#;;CartSuggestions;"You may also like..."

# Category Pages
UPDATE CategoryPage;$contentCV[unique=true];uid[unique=true];title[lang=$lang]

# CMS Navigation Nodes
UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;MyAccountNavNode;"My Profile"
;;PowertoolsNavNode;"Viking Site"
;;SiteRootNode;"Site Root"



# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];
;;homepageContentParagraphComponent;"<h3>Welcome to the Customer Portal</h3><p>At the heart of VIKING lies an important mission to protect and save human lives all over the world.</p><p>Our products protect passengers and crew on many of the world's largest cruise liners and cargo ships as well as workers on the most advanced offshore platforms. At the same time, we safeguard the ability of shipowners and offshore asset owners or operators to manage their businesses by ensuring safety and regulatory compliance on board at all times.</p><p>VIKING is a global company with more than 55 years of innovation and leadership in maritime and offshore safety. Headquartered in Denmark, we employ over 1,900 people in more than 30 countries. No-compromise service is a cornerstone of VIKING's offering, delivered by a network of local experts and 260 certified servicing stations spread around the globe.</p>";
;;faqMenuParagraph;"<div class=""nav_column"">	<div class=""title_holder"">		<div class=""title"">			<div class=""title-top"">				<span></span>			</div>		</div>		<h2>VIKING FAQ</h2>	</div>	<div class=""item"">		<ul class=""facet_block indent"">			<li><a href=""#a00"">Re-inspection certificates</a></li>			<li><a href=""#b00"">Service manuals</a></li>			<li><a href=""#c00"">Training courses</a></li>			<li><a href=""#d00"">Miscellaneous</a></li>		</ul>	</div></div>";
;;faqTextParagraph;"<a name=""00""></a><div class=""textpage textpage-faqs""><div class=""item_container"">		<h2><a name=""a00""></a>Re-inspection certificates</h2>		<h3>Can I change any details on the certificate after it has been printed?</h3>     <p>Yes: for a period of 5 days after saving the certificate first time, you can change and re-print certificates issued by your own station, without extra charges.</p> <p></p>     <h3>Can I change the next date of inspection on a certificate? </h3>	   <p>Yes, the date can be changed when the certificate is created. Next inspection date is automatically suggested as one year ahead, but the date may be changed if an inspection frequency above one year is accepted for the equipment.</p>    <p></p>    <h3>Can I edit the owner details on the certificate?</h3>   <p>Yes, you can overrule the owner details automatically filled in by the system, by entering the new details in the 'Alternative owner details' section.</p>   <p>If the customer does not want the owner details to appear on the certificate, simply place a '.' in on of the fields in the 'Alternative owner details' section and the current owner details will disappear.</p> <p></p>    <h3>Can I see all equipment registered on a vessel? </h3>   <p>No, you can only see equipments if they have been serviced by your station earlier, and if you have issued the certificates through this portal.</p>  <p></p>     <h3>Can I see certificates issued by other service stations?</h3>       <p>No, you can only see certificates issued by your own service station.</p>    <p></p>    <h3>Do I need to return paper copies to DK?</h3>   <p>If certificates have been made via the portal, VIKING already has the information needed.</p><p>If for some reason you issue a paper certificate - please remember that you have to forward a paper copy to VIKING DK.</p> <p></p>    <h3>Which certificate credits are used for the various types of liferafts?</h3>     <p>The system automatically chooses the certificate for the raft in question:</p>   <ul><li>1058615 is used for yachting rafts</li><li>1058631 is used for 4-50 pers. standard rafts</li><li>1058633 is used for 100 and 2x50 pers. rafts</li><li>1058632 is used for S30 rafts</li><li>1058636 is used for 150 pers. rafts</li><li>1058637 is used for RS system rafts</li><li>1058634 is used for S30, 100 pers</li><li>1058636 is used for S30, 150 pers</li></ul>    <p></p>   <h3>How do I issue certificates for lifejackets and immersion suits?</h3>   <p>Certificates for lifejackets and immersion suits can not be issued via the service portal.</p><p>You have to issue paper certificates bought through your local VIKING office. They will also provide you with a special program for filling in the paper certificates. This is only possible for stations that have been trained and purchased the minimum equipment of tools and spare parts for PPE.</p>    <p></p>  <h3>How do I write liferaft serial numbers?</h3>    <p>Liferafts produced before 2005 have serial numbers consisting on raft type and number (eg: 25DK+-A010203). Please do not leave any space between the letters and digits.<br>Liferafts produced after 2005 have serial numbers consisting of 8 digits (eg: 11012901).</p><p><strong>The most common types of liferafts are:</strong><ul><li>UKL, UKCL, UKSL, CL</li><li>DK, DK+, DKS, DKS+ ,</li><li>DKF, DKF+, DKFS, DKFS+</li><li>DKR, DKR+, IBA</li><li>MOR</li><li>ESI rafts: S30</li></ul></p><p><strong>However, you might also see:</strong><ul><li>Old rafts: H, SELRA, CI, DKC, N, K, KF, KE, RDV</li><li>RS rafts: DK-RS, DK+RS, UT-RS, MEP</li><li>Navy rafts: DKN, DKSN, H</li><li>Training rafts: DK+TR, DKF+TR,</li><li>Yachting rafts: UK, UKC, UK+, VL</li><li>Polar rafts</li><li>E rafts</li></ul></p><p><strong>Some rafts are packed as 100 pers units:</strong><ul><li>2x50DK</li><li>2x50DKR</li><li>2x50IBA</li></ul></p><p>These units are listed in the system by their number for instance by serial number, which should have the following form: R010501-R020501</p> <p></p>  <h3>I cannot open a certificate, when I press the pdf-link? </h3>   <p>Make sure Adobe Reader is installed on your computer. If not, it can be downloaded for free on <a href=""http://www.adobe.com/products/reader.html"" target=""_blank"">http://www.adobe.com/products/reader.html</a>.</p><a class=""pagetop-link"" href=""#00"">back to top</a>	</div><div class=""item_container"">		<h2><a name=""b00""></a>Service manuals</h2>		<h3><a name=""b01""></a>Bulletins, can I get a paper copy?</h3>		<p>All stations approved for servicing of standard liferafts may modify a S30 raft to a standard raft. You do not need authorization for S30 in order to do the modification. A guide on how to do this can be found in the A-manual, section 19.62.1.</p>     <p>Please note that a S30 liferaft modified into a standard raft, can never become an S30 raft again.</p>		<p></p>	<h3><a name=""b03""></a>How do I confirm bulletins?</h3>		<p>All stations have to confirm that they have received the service bulletins without delay. When confirming a bulletin the station also confirms having updated their paper copy of the manual if this is still used at the station, as well as confirms having informed all technicians about the changes.</p>   <p>The ABC and the S30 manuals are confirmed via the service portal - please go to the section Service Manuals & Bulletins and select the manual and bulletin to be confirmed, and push the confirmation button.</p>    <p>All MES and PPE manuals are confirmed either via the link, attached to the e-mail advising you about the new bulletin, or via e-mail to <a href=""mailto:ssoo@viking-life.com"">ssoo@viking-life.com</a>. The bulletins themselves are found in the section Service Manuals & Bulletins. </p>    <p>PPE manuals are normally replaced in full as they are not very comprehensive.</p>    <p></p>    <h3>How do I get service manuals for S30 rafts and Polar liferafts?</h3>    <p>These manuals are available only to stations who have been trained and obtained certificate for servicing these types of rafts. <br>Also you need to invest in minimum equipment of tools and spare parts.</p>    <p></p>   <h3>How do I get service manuals for suits and lifejackets?</h3>    <p>These manuals are available only to stations that have been trained and have purchased the minimum equipment of tools and spare parts.</p>   <p>The manuals are not delivered from VIKING in paper copies, but you are requested to print them from the internet.</p>		<p></p>		<a class=""pagetop-link"" href=""#00"">back to top</a>	</div><div class=""item_container"">		<h2><a name=""c00""></a>Training courses</h2>		<h3>Training centres - can I choose any centre for training?</h3>  <p>Yes - all stations are free to choose between all the courses announced at our website no matter the geographical location.</p>		<a class=""pagetop-link"" href=""#00"">back to top</a>	</div><div class=""item_container"">		<h2><a name=""d00""></a>Miscellaneous</h2>		<h3>Does VIKING supply spare parts for rafts older than 20 years?</h3>    <p>VIKING supports service and supplies spare parts for liferafts up to 20 years. For liferafts older than 20 years we only supply a limited range of spare parts, according to availability on stock.</p>  <p></p>      <h3>How can I get an authorization to service lifejackets and suits?</h3>   <p>Lifejacket and suit service is part of the standard training course. In order to be authorized you need to invest in minimum spare parts and tools.</p> <p></p>  <h3>What do we need to do, if our station moves?</h3>   <p>When your station was approved to perform service on VIKING equipment, it was approved based on the premises of the station's location. If a station moves, it is not automatically approved, as we need to make sure that the new premises comply with the existing requirements. When you move your station, you need a new approval from VIKING and a renewed National Approval. </p> <p>Please consider your station as not approved, until you have received these documents.</p>   <p>Therefore, if you consider moving your station to new facilities, please notify your local VIKING Office, who will assist in the process.</p>		<a class=""pagetop-link"" href=""#00"">back to top</a>	</div></div>";

# Powertools Order Expired Page

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];
#;;orderExpiredMenuParagraph;;
#;;orderExpiredTextParagraph;"<div class='cms-content order-expired'><div class='information_message neutral'><span class='single'></span><p>This page has expired.</p></div><h2>Dear Customer,</h2><p>Order Status for guest users are only kept for 3 months following the date the order was placed.<br>If you have not received your delivery or have other inquiries, please contact our Customer Service Dept.</p><h3>Be prepared with:</h3><ul class='bullet-list'><li>Your Name</li><li>Order Number (Your Order Number can be found on the confirmation email sent shortly after your order was placed.)</li><li>Order Delivery Address or Pick Up in Store Location</li></ul><p>Our Customer Service professionals will be happy to update you on the status of your order.</p><p class='cms-content-emphasized-big'>Thank you for shopping at VIKING.</p></div>";

# Powertools Terms and Conditions Page

# CMS Paragraph Component (Contact information)
UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=en];
;;termsAndConditionsMenuParagraph;"<div class=""nav_column"">	<div class=""title_holder"">		<div class=""title"">			<div class=""title-top"">				<span></span>			</div>		</div>		<h2>Terms and Conditions</h2>	</div>	<div class=""item"">		<ul class=""facet_block indent"">			<li><a href=""#a00"">Information</a></li>		</ul>	</div></div>";
;;termsAndConditionsTextParagraph;"<a name=""00""></a><div class=""textpage textpage-termsAndConditions""><div class=""item_container"">		<h2><a name=""a00""></a>General conditions for the supply of products and services</h2>		<p><a href=""/vikingcustomerportalstorefront/_ui/pdf/viking-general-terms-and-conditions-for-supply-of-products-and-services_August2013.pdf"" target=""_blank"">Read our conditions here</a></p>		<a class=""pagetop-link"" href=""#00"">back to top</a>	</div></div>";

# Powertools Homepage

UPDATE ProductCarouselComponent;$contentCV[unique=true];uid[unique=true];title[lang=en]
 ;;PowertoolsHomepageNewProductCarouselComponent;"WHAT'S NEW"
 ;;PowertoolsHomepageProductCarouselComponent;"Our Bestselling Products"
 ;;PremiumbuyersgroupHomepageProductCarouselComponent;"Our Bestselling Products"

INSERT_UPDATE Media;mediaFormat(qualifier);code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite=true];realfilename;altText;mime[default='image/jpeg'];$contentCV[unique=true];folder(qualifier)[default=images];
;widescreen;Powertools_1400x480_BigSplash_EN_01_1400W.jpg;$siteResource/images/banners/homepage/responsive/viking-customer-portal-banner.jpg;viking-customer-portal-banner.jpg;"VIKING We protect and save human lives all over the world";
;widescreen;Powertools_1400x70_Brands_EN_01_1400W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_1400x70_Brands_EN_01_1400W.jpg;Powertools_1400x70_Brands_EN_01_1400W.jpg;"Bosch | Black & Decker | Einhall | Skil | Hitachi";
;mobile;Powertools_160x139_24Service_EN_01_160W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_160x139_24Service_EN_01_160W.jpg;Powertools_160x139_24Service_EN_01_160W.jpg;"24/7 - Great Service After You Buy";
;mobile;Powertools_160x139_25Deal_EN_01_160W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_160x139_25Deal_EN_01_160W.jpg;Powertools_160x139_25Deal_EN_01_160W.jpg;"25% Great Prices and Great Deals";
;mobile;Powertools_160x139_30Day_EN_01_160W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_160x139_30Day_EN_01_160W.jpg;Powertools_160x139_30Day_EN_01_160W.jpg;"30 Day - Shop with Confidence";
;mobile;Powertools_160x139_6pm_EN_01_160W.jpg;$siteResource/images/banners/homepage/responsive/servicepartner-checkout-liferafts.jpg;servicepartner-checkout-liferafts.jpg;"VIKING Customer Portal Checkout";
;desktop;Powertools_240x200_24Service_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_24Service_EN_01_240W.jpg;Powertools_240x200_24Service_EN_01_240W.jpg;"24/7 - Great Service After You Buy";
;desktop;Powertools_240x200_25Deal_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_25Deal_EN_01_240W.jpg;Powertools_240x200_25Deal_EN_01_240W.jpg;"25% Great Prices and Great Deals";
;desktop;Powertools_240x200_30Day_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_30Day_EN_01_240W.jpg;Powertools_240x200_30Day_EN_01_240W.jpg;"30 Day - Shop with Confidence";
;desktop;Powertools_240x200_6pm_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/servicepartner-checkout-liferafts.jpg;servicepartner-checkout-liferafts.jpg;"VIKING Customer Portal Checkout";
;desktop;Powertools_240x200_AngleGrinders_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_AngleGrinders_EN_01_240W.jpg;Powertools_240x200_AngleGrinders_EN_01_240W.jpg;"Angle Grinders";
;desktop;Powertools_240x200_PowerDrills_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_PowerDrills_EN_01_240W.jpg;Powertools_240x200_PowerDrills_EN_01_240W.jpg;"Power Drills";
;desktop;Powertools_240x200_Sanders_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_Sanders_EN_01_240W.jpg;Powertools_240x200_Sanders_EN_01_240W.jpg;"Sanders";
;desktop;Powertools_240x200_Screwdrivers_EN_01_240W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_240x200_Screwdrivers_EN_01_240W.jpg;Powertools_240x200_Screwdrivers_EN_01_240W.jpg;"Screwdrivers";
;mobile;Powertools_320x100_Brands_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_320x100_Brands_EN_01_320W.jpg;Powertools_320x100_Brands_EN_01_320W.jpg;"Bosch | Black & Decker | Einhall | Skil | Hitachi";
;mobile;Powertools_320x260_AngleGrinders_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_320x260_AngleGrinders_EN_01_320W.jpg;Powertools_320x260_AngleGrinders_EN_01_320W.jpg;"Angle Grinders";
;mobile;Powertools_320x260_PowerDrills_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_320x260_PowerDrills_EN_01_320W.jpg;Powertools_320x260_PowerDrills_EN_01_320W.jpg;"Power Drills";
;mobile;Powertools_320x260_Sanders_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_320x260_Sanders_EN_01_320W.jpg;Powertools_320x260_Sanders_EN_01_320W.jpg;"Sanders";
;mobile;Powertools_320x260_Screwdrivers_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_320x260_Screwdrivers_EN_01_320W.jpg;Powertools_320x260_Screwdrivers_EN_01_320W.jpg;"Screwdrivers";
;mobile;Powertools_320x300_BigSplash_EN_01_320W.jpg;$siteResource/images/banners/homepage/responsive/viking-customer-portal-banner.jpg;viking-customer-portal-banner.jpg;"VIKING We protect and save human lives all over the world";
;widescreen;Powertools_350x280_24Service_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x280_24Service_EN_01_350W.jpg;Powertools_350x280_24Service_EN_01_350W.jpg;"24/7 - Great Service After You Buy";
;widescreen;Powertools_350x280_25Deal_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x280_25Deal_EN_01_350W.jpg;Powertools_350x280_25Deal_EN_01_350W.jpg;"25% Great Prices and Great Deals";
;widescreen;Powertools_350x280_30Day_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x280_30Day_EN_01_350W.jpg;Powertools_350x280_30Day_EN_01_350W.jpg;"30 Day - Shop with Confidence";
;widescreen;Powertools_350x280_6pm_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/servicepartner-checkout-liferafts.jpg;servicepartner-checkout-liferafts.jpg;"VIKING Customer Portal Checkout";
;widescreen;Powertools_350x290_AngleGrinders_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x290_AngleGrinders_EN_01_350W.jpg;Powertools_350x290_AngleGrinders_EN_01_350W.jpg;"Angle Grinders";
;widescreen;Powertools_350x290_PowerDrills_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x290_PowerDrills_EN_01_350W.jpg;Powertools_350x290_PowerDrills_EN_01_350W.jpg;"Power Drills";
;widescreen;Powertools_350x290_Sanders_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x290_Sanders_EN_01_350W.jpg;Powertools_350x290_Sanders_EN_01_350W.jpg;"Sanders";
;widescreen;Powertools_350x290_Screwdrivers_EN_01_350W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_350x290_Screwdrivers_EN_01_350W.jpg;Powertools_350x290_Screwdrivers_EN_01_350W.jpg;"Screwdrivers";
;tablet;Powertools_385x280_24Service_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x280_24Service_EN_01_385W.jpg;Powertools_385x280_24Service_EN_01_385W.jpg;"24/7 - Great Service After You Buy";
;tablet;Powertools_385x280_25Deal_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x280_25Deal_EN_01_385W.jpg;Powertools_385x280_25Deal_EN_01_385W.jpg;"25% Great Prices and Great Deals";
;tablet;Powertools_385x280_30Day_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x280_30Day_EN_01_385W.jpg;Powertools_385x280_30Day_EN_01_385W.jpg;"30 Day - Shop with Confidence";
;tablet;Powertools_385x280_6pm_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/servicepartner-checkout-liferafts.jpg;servicepartner-checkout-liferafts.jpg;"VIKING Customer Portal Checkout";
;tablet;Powertools_385x290_AngleGrinders_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x290_AngleGrinders_EN_01_385W.jpg;Powertools_385x290_AngleGrinders_EN_01_385W.jpg;"Angle Grinders";
;tablet;Powertools_385x290_PowerDrills_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x290_PowerDrills_EN_01_385W.jpg;Powertools_385x290_PowerDrills_EN_01_385W.jpg;"Power Drills";
;tablet;Powertools_385x290_Sanders_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x290_Sanders_EN_01_385W.jpg;Powertools_385x290_Sanders_EN_01_385W.jpg;"Sanders";
;tablet;Powertools_385x290_Screwdrivers_EN_01_385W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_385x290_Screwdrivers_EN_01_385W.jpg;Powertools_385x290_Screwdrivers_EN_01_385W.jpg;"Screwdrivers";
;tablet;Powertools_770x350_BigSplash_EN_01_770W.jpg;$siteResource/images/banners/homepage/responsive/viking-customer-portal-banner.jpg;viking-customer-portal-banner.jpg;"VIKING We protect and save human lives all over the world";
;tablet;Powertools_770x70_Brands_EN_01_770W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_770x70_Brands_EN_01_770W.jpg;Powertools_770x70_Brands_EN_01_770W.jpg;"Bosch | Black & Decker | Einhall | Skil | Hitachi";
;desktop;Powertools_960x400_BigSplash_EN_01_960W.jpg;$siteResource/images/banners/homepage/responsive/viking-customer-portal-banner.jpg;viking-customer-portal-banner.jpg;"VIKING We protect and save human lives all over the world";
;desktop;Powertools_960x70_Brands_EN_01_960W.jpg;$siteResource/images/banners/homepage/responsive/Powertools_960x70_Brands_EN_01_960W.jpg;Powertools_960x70_Brands_EN_01_960W.jpg;"Bosch | Black & Decker | Einhall | Skil | Hitachi";

INSERT_UPDATE MediaContainer;qualifier[unique=true];$medias;$contentCV[unique=true]
;powertools-homepage-24/7service-en;Powertools_350x280_24Service_EN_01_350W.jpg,Powertools_240x200_24Service_EN_01_240W.jpg,Powertools_160x139_24Service_EN_01_160W.jpg,Powertools_385x280_24Service_EN_01_385W.jpg
;powertools-homepage-25%-deal-en;Powertools_350x280_25Deal_EN_01_350W.jpg,Powertools_240x200_25Deal_EN_01_240W.jpg,Powertools_160x139_25Deal_EN_01_160W.jpg,Powertools_385x280_25Deal_EN_01_385W.jpg
;powertools-homepage-30Day-en;Powertools_350x280_30Day_EN_01_350W.jpg,Powertools_240x200_30Day_EN_01_240W.jpg,Powertools_160x139_30Day_EN_01_160W.jpg,Powertools_385x280_30Day_EN_01_385W.jpg
;powertools-homepage-6pm-en;Powertools_350x280_6pm_EN_01_350W.jpg,Powertools_240x200_6pm_EN_01_240W.jpg,Powertools_160x139_6pm_EN_01_160W.jpg,Powertools_385x280_6pm_EN_01_385W.jpg
;powertools-homepage-anglegranders-en;Powertools_350x290_AngleGrinders_EN_01_350W.jpg,Powertools_240x200_AngleGrinders_EN_01_240W.jpg,Powertools_320x260_AngleGrinders_EN_01_320W.jpg,Powertools_385x290_AngleGrinders_EN_01_385W.jpg
;powertools-homepage-brands-en;Powertools_1400x70_Brands_EN_01_1400W.jpg,Powertools_960x70_Brands_EN_01_960W.jpg,Powertools_320x100_Brands_EN_01_320W.jpg,Powertools_770x70_Brands_EN_01_770W.jpg
;powertools-homepage-powerdrills-en;Powertools_350x290_PowerDrills_EN_01_350W.jpg,Powertools_240x200_PowerDrills_EN_01_240W.jpg,Powertools_320x260_PowerDrills_EN_01_320W.jpg,Powertools_385x290_PowerDrills_EN_01_385W.jpg
;powertools-homepage-sanders-en;Powertools_350x290_Sanders_EN_01_350W.jpg,Powertools_240x200_Sanders_EN_01_240W.jpg,Powertools_320x260_Sanders_EN_01_320W.jpg,Powertools_385x290_Sanders_EN_01_385W.jpg
;powertools-homepage-screwdrivers-en;Powertools_350x290_Screwdrivers_EN_01_350W.jpg,Powertools_240x200_Screwdrivers_EN_01_240W.jpg,Powertools_320x260_Screwdrivers_EN_01_320W.jpg,Powertools_385x290_Screwdrivers_EN_01_385W.jpg
;powertools-homepage-spalsh-en;Powertools_1400x480_BigSplash_EN_01_1400W.jpg,Powertools_960x400_BigSplash_EN_01_960W.jpg,Powertools_320x300_BigSplash_EN_01_320W.jpg,Powertools_770x350_BigSplash_EN_01_770W.jpg

INSERT_UPDATE SimpleResponsiveBannerComponent;$contentCV[unique=true];uid[unique=true];$mediaContainer
;;PowertoolsHompage24/7BannerComponent;powertools-homepage-24/7service-en
;;PowertoolsHompage25%BannerComponent;powertools-homepage-25%-deal-en
;;PowertoolsHompage30dayBannerComponent;powertools-homepage-30Day-en
;;PowertoolsHompage6pmBannerComponent;powertools-homepage-6pm-en
;;PowertoolsHompageAngleGrindersBannerComponent;powertools-homepage-anglegranders-en
;;PowertoolsHompageBrandsBannerComponent;powertools-homepage-brands-en
;;PowertoolsHompagePowerDrillsBannerComponent;powertools-homepage-powerdrills-en
;;PowertoolsHompageSandersBannerComponent;powertools-homepage-sanders-en
;;PowertoolsHompageScrewdriversBannerComponent;powertools-homepage-screwdrivers-en
;;PowertoolsHompageSplashBannerComponent;powertools-homepage-spalsh-en


# Powertools Search Results Page

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText
;;Powertools_148x318_SearchResultsSideBanner_EN_01.jpg;$siteResource/images/banners/site/Powertools_148x318_SearchResultsSideBanner_EN_01.jpg;;;""

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;SearchResultsSideBanner;Powertools_148x318_SearchResultsSideBanner_EN_01.jpg

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]
#;;EmptyCartParagraphComponent;"<h2>Your shopping cart is empty</h2><p>Suggestions</p><ul><li>Browse our products by selecting a category above</li></ul>"
;;NoSearchResultsParagraphComponent;"<h2>Sorry, we couldn't find any results for your search</h2><p>Suggestions:</p><ul><li>You may have typed your keywords incorrectly - please check for misspellings.</li><li>You may have been too specific - please broaden your search by using fewer keywords.</li><li>Browse our products by selecting a category above.</li></ul>"

# Powertools Cart Page

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText
;;Powertools_148x300_HelpBanner_EN_01.jpg;$siteResource/images/banners/site/Powertools_148x300_HelpBanner_EN_01.jpg;;;"Need Help? Click here for help with checking out"
;;Powertools_788x120_HomeDeliveryBanner_EN_01.jpg;$siteResource/images/banners/site/servicepartner-checkout-liferafts.jpg;;;"VIKING Customer Portal Checkout"

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
#;;HelpBanner;Powertools_148x300_HelpBanner_EN_01.jpg
#;;NextDayDeliveryBanner120;Powertools_788x120_HomeDeliveryBanner_EN_01.jpg

UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]
#;;EmptyCartParagraphComponent;"<h2>Your shopping cart is empty</h2><p>Suggestions</p><ul><li>Browse our products by clicking a category above</li></ul>"
#;;HelpParagraphComponent;"<p>For support please contact your local VIKING office or <a href=""mailto:globalserviceportal@viking-life.com"">globalserviceportal@viking-life.com</a>.</p><p>If you have questions related to your placed orders, please contact your VIKING sales representative.</p><p>If you experience any issues with placing your order, please contact <a href=""mailto:globalserviceportal@viking-life.com"">globalserviceportal@viking-life.com</a>.</p>"

# Powertools Store Finder Page

# Media Content
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images'];altText
;;Powertools_148x318_StoreFinderSideBanner_EN_01.jpg;$siteResource/images/banners/site/Powertools_148x318_StoreFinderSideBanner_EN_01.jpg;;;""

UPDATE SimpleBannerComponent;$contentCV[unique=true];uid[unique=true];$picture[lang=$lang]
;;StoreFinderSideBanner;Powertools_148x318_StoreFinderSideBanner_EN_01.jpg;


INSERT_UPDATE Media;mediaFormat(qualifier);code[unique=true];@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite=true];realfilename;altText;mime[default='image/svg+xml'];$contentCV[unique=true];folder(qualifier)[default=images]
;widescreen;Powertools_1400x401_ErrorPage_EN_1400W.svg;$siteResource/images/banners/site/Powertools_1400x401_ErrorPage_EN_1400W.svg;Powertools_1400x401_ErrorPage_EN_1400W.svg;"Page not found";
;mobile;Powertools_480x170_ErrorPage_EN_480W.svg;$siteResource/images/banners/site/Powertools_1400x401_ErrorPage_EN_1400W.svg;Powertools_1400x401_ErrorPage_EN_1400W.svg;"Page not found";
;tablet;Powertools_770x221_ErrorPage_EN_770W.svg;$siteResource/images/banners/site/Powertools_1400x401_ErrorPage_EN_1400W.svg;Powertools_1400x401_ErrorPage_EN_1400W.svg;"Page not found";
;desktop;Powertools_960x275_ErrorPage_EN_960W.svg;$siteResource/images/banners/site/Powertools_1400x401_ErrorPage_EN_1400W.svg;Powertools_1400x401_ErrorPage_EN_1400W.svg;"Page not found";

INSERT_UPDATE MediaContainer;qualifier[unique=true];$medias;$contentCV[unique=true]
;powertools-errorpage-pagenotfound-en;Powertools_480x170_ErrorPage_EN_480W.svg,Powertools_770x221_ErrorPage_EN_770W.svg,Powertools_960x275_ErrorPage_EN_960W.svg,Powertools_1400x401_ErrorPage_EN_1400W.svg

INSERT_UPDATE SimpleResponsiveBannerComponent;$contentCV[unique=true];uid[unique=true];name;&componentRef;$mediaContainer
;;PowertoolstronicsErrorpageBannerComponent;Powertoolstronics Error Page Banner Component;PowertoolstronicsErrorpageBannerComponent;powertools-errorpage-pagenotfound-en

# CMS Link Components
UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=$lang]
;;UsersLink;"Users"
;;AssignVessels;"Assign Vessels"

# CMS Link Components
UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];linkName[lang=en]

# CMS Navigation Nodes
UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];title[lang=$lang]
;;MyCompanyNavNode;"My Company"

# CMS Navigation Nodes
UPDATE CMSNavigationNode;$contentCV[unique=true];uid[unique=true];title[lang=en]


