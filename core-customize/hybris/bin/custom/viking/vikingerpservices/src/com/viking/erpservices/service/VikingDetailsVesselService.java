/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.service;

import com.viking.search.data.SearchVesselOverviewData;

import java.util.Map;

public interface VikingDetailsVesselService {
    //Map<String, String> detailsVessel(String input);

    SearchVesselOverviewData detailsVessel(String input);

}
