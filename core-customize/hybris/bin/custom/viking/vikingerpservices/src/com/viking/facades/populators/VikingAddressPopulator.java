package com.viking.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.lang3.StringUtils;

/**
 * The type Viking address populator.
 */
public class VikingAddressPopulator extends AddressPopulator {

    @Override
    protected void populateAddressFields(final AddressModel source, final AddressData target) {
        super.populateAddressFields(source, target);
        target.setLine1(StringUtils.replace(source.getLine1(), "null", StringUtils.EMPTY));
    }
}
