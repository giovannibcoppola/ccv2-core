package com.viking.erpservices.service;

import com.viking.erpservices.service.forms.xml.VikingFormsPreFillData;

/**
 * Service to handle notification yForm.
 */
public interface VikingFormsPreFillDataService {

    /**
     * Get the notification data from SAP used to prefill yForm.
     * @param notificationId
     * @param prefillId
     * @return
     */
    VikingFormsPreFillData getData(String notificationId, String prefillId);
    boolean isCreditCheckPass(String serviceStation, String formID);
}
