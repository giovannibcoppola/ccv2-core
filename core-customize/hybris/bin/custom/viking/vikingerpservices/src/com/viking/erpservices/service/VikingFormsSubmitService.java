package com.viking.erpservices.service;

public interface VikingFormsSubmitService {
    void submitYFormDataToSap(final String notificationId, final String serviceStation, final String xmlInput);
    String submitYFormDataToSapAndGetPDFUrl(final String notificationId, final String serviceStation, final String xmlInput, final String print);
    void submitCondemnationToSap(final String input);
}
