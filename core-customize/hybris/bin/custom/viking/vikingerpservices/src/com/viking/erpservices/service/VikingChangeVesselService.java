package com.viking.erpservices.service;

import com.viking.search.data.ChangeVesselResponseData;

public interface VikingChangeVesselService {
    ChangeVesselResponseData changeVessel(String vesselID, String description, String flagState, String signal);
}
