package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.erpservices.service.VikingIPADNotificationService;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;

/**
 * The type Default viking ipad notification service.
 */
public class DefaultVikingIPADNotificationService implements VikingIPADNotificationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingIPADNotificationService.class);
    private static final String ZSEND_TO_IPAD = "ZSEND_TO_IPAD";
    private static final String IM_QMNUM = "IM_QMNUM";
    private static final String IM_SERV_STAT_NO = "IM_SERV_STAT_NO";
    public static final String O_RETURN = "O_RETURN";
    public static final String MESSAGE = "MESSAGE";
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Override
    public void sendNotificationToIPad(String serviceStation, String notificationNumber) {
        LOG.info("Send NotificationTo IPAD !!!!!!!!!!!!");
        JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function;
        try {
            final String notificationNumber_padded = StringUtils.leftPad(notificationNumber, 12, "0");

            function = managedConnection.getFunction(ZSEND_TO_IPAD);
            function.getImportParameterList().setValue(IM_QMNUM, notificationNumber_padded);
            function.getImportParameterList().setValue(IM_SERV_STAT_NO, serviceStation);

            managedConnection.execute(function);
            JCoTable table = function.getTableParameterList().getTable(O_RETURN);
            final String message = table.getString(MESSAGE);

            LOG.info("VIK-1260: iPad notification: Received message from SAP for serviceStation {} and notificationNumber {}: {}", serviceStation, notificationNumber_padded, message);

            if (LOG.isDebugEnabled()) {
                LOG.debug(MessageFormat.format("Send Notification To IPad - ServiceStation [{0}], notificationNumber [{1}]", serviceStation, notificationNumber));
            }
        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (sendNotificationToIPad) for module : " + ZSEND_TO_IPAD, e);
        }
    }


    /**
     * Sets co managed connection factory.
     *
     * @param jCoManagedConnectionFactory the j co managed connection factory
     */
    @Autowired
    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }
}
