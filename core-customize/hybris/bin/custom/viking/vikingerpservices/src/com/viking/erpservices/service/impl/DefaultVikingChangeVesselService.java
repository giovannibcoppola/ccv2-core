package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.erpservices.service.VikingChangeVesselService;
import com.viking.search.data.ChangeVesselResponseData;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.viking.erpservices.service.impl.DefaultVikingCreateVesselService.ZCREATE_FUNCLOC;

public class DefaultVikingChangeVesselService implements VikingChangeVesselService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingChangeVesselService.class);
    public static final String ZCHANGE_FUNCLOC = "ZCHANGE_FUNCLOC";
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Override
    public ChangeVesselResponseData changeVessel(String vesselID, String description, String flagState, String signal) {
        try {
            LOG.info("Change vessel Service class with data  "+vesselID+" "+description+" "+flagState+" "+signal);

            ChangeVesselResponseData changeVesselResponseData = new ChangeVesselResponseData();
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZCHANGE_FUNCLOC);
            function.getImportParameterList().setValue("IM_FUNCLOC", vesselID);
            function.getImportParameterList().setValue("IM_FUNCLOC_DESCR", description);
            function.getImportParameterList().setValue("IM_FLAGSTATE", flagState);
            function.getImportParameterList().setValue("IM_SIGNAL", signal);
            managedConnection.execute(function);


            JCoTable FunctionReturn = function.getTableParameterList().getTable("O_RETURN");
            LOG.info("RFC return message : "+FunctionReturn.getString("MESSAGE"));

            changeVesselResponseData.setMessage(FunctionReturn.getString("MESSAGE"));

            String message = changeVesselResponseData.getMessage();
            return changeVesselResponseData;

        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (Change Vessel) for module : " + ZCHANGE_FUNCLOC, e);
        }
    return null;
    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }
}
