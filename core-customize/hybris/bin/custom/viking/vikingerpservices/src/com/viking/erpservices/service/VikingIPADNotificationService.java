package com.viking.erpservices.service;

/**
 * The interface Viking ipad notification service.
 */
public interface VikingIPADNotificationService {
    /**
     * Send notification to i pad.
     *
     * @param serviceStation     the service station
     * @param notificationNumber the notification number
     * @throws Exception the exception
     */
    void sendNotificationToIPad(String serviceStation, String notificationNumber);
}
