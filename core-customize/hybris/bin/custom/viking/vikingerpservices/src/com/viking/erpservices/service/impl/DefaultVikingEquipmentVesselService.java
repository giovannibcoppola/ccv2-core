package com.viking.erpservices.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.data.EquipmentData;
import com.viking.erpservices.service.VikingEquipmentVesselService;
import com.viking.search.data.VesselEquipmentData;
import com.viking.search.data.VesselEquipmentDataList;

import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public class DefaultVikingEquipmentVesselService implements VikingEquipmentVesselService {
    public static final String ZGET_EQUIP_BY_FUNCLOC2 = "ZGET_EQUIP_BY_FUNCLOC2";
    public static final String ZGET_EQUIP_BY_SERNO = "ZGET_EQUIP_BY_SERNO";
    public static final String ZEQUIPMENT_DISMANTLE = "ZEQUIPMENT_DISMANTLE";
    public static final String ZMOVE_EQUIPMENT = "ZMOVE_EQUIPMENT";
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingEquipmentVesselService.class);
    public static final String J_CO_STATELESS = "JCoStateless";
    public static final String HYBRIS = "Hybris";
    public static final String IM_SERNR = "IM_SERNR";
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Override
    public VesselEquipmentDataList getVesselEquipmentList(String funcLocID) {

        final VesselEquipmentDataList equipVesselDataList = new VesselEquipmentDataList();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
            final JCoFunction function = managedConnection.getFunction(ZGET_EQUIP_BY_FUNCLOC2);
            function.getImportParameterList().setValue("IM_FUNCLOC", funcLocID);

            managedConnection.execute(function);

            JCoTable table = function.getExportParameterList().getTable("EX_EQUIP_TAB");

            final List<VesselEquipmentData> equipVesselData = new ArrayList<>();

            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final VesselEquipmentData equipVesselDataEntry = new VesselEquipmentData();
                equipVesselDataEntry.setEquipmentName(table.getString("EQKTX"));
                equipVesselDataEntry.setEquipmentID(table.getString("EQUNR"));
                equipVesselDataEntry.setSerialNo(table.getString("SERNR"));
                equipVesselDataEntry.setType(table.getString("OBJ_TYPE"));
                equipVesselDataEntry.setMonth(table.getString("MONTH_OF_CONST"));
                equipVesselDataEntry.setYear(table.getString("YEAR_OF_CONST"));
                equipVesselData.add(equipVesselDataEntry);
            }

            equipVesselDataList.setVesselEquipmentDataList(equipVesselData);

            return equipVesselDataList;
        } catch (Exception e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_EQUIP_BY_FUNCLOC2, e);
        }

        return equipVesselDataList;
    }

    @Override
    public void dismantleEquipment(String serialNumber) {
        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
            final JCoFunction function = managedConnection.getFunction(ZEQUIPMENT_DISMANTLE);
            function.getImportParameterList().setValue(IM_SERNR, serialNumber);

            managedConnection.execute(function);

        } catch (Exception e) {
            LOG.error("Error during sap erp rfc call (dismantleEquipment) for module : " + ZEQUIPMENT_DISMANTLE, e);
        }
    }

    @Override
    public void addEquipment(String serialNumber, String vesselID) throws BackendException {
        JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
        final JCoFunction function = managedConnection.getFunction(ZMOVE_EQUIPMENT);
        function.getImportParameterList().setValue(IM_SERNR, serialNumber);
        function.getImportParameterList().setValue("IM_FUNCLOC", vesselID);

        managedConnection.execute(function);
    }

    @Override
    public EquipmentData searchEquipment(final String serialNumber) {
        final EquipmentData data = new EquipmentData();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
            final JCoFunction function = managedConnection.getFunction(ZGET_EQUIP_BY_SERNO);
            function.getImportParameterList().setValue(IM_SERNR, serialNumber);

            managedConnection.execute(function);

            final JCoTable table = function.getTableParameterList().getTable("EX_EQUIP_TAB");

            data.setType(table.getString("TASK_TYPE"));
            data.setMonth(table.getString("MONTH_OF_CONST"));
            data.setYear(table.getString("YEAR_OF_CONST"));
            data.setGroup(table.getString("OBJ_TYPE"));

            return data;
        } catch (Exception e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_EQUIP_BY_SERNO, e);
        }

        return data;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

}
