package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.dao.DateConversionKeyDao;
import com.viking.core.dao.InitialPrefillValueDao;
import com.viking.core.model.DateConversionKeyModel;
import com.viking.core.model.InitialPrefillValueModel;
import com.viking.erpservices.service.VikingFormsPreFillDataService;
import com.viking.erpservices.service.forms.xml.VikingFormData;
import com.viking.erpservices.service.forms.xml.VikingFormsPreFillData;
import com.viking.search.data.CheckCreditData;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.viking.erpservices.constants.VikingerpservicesConstants.HYBRIS;
import static com.viking.erpservices.constants.VikingerpservicesConstants.J_CO_STATELESS;


/**
 * {@inheritDoc}
 */
public class DefaultVikingFormsPreFillDataService implements VikingFormsPreFillDataService {
    private static final Logger LOG = Logger.getLogger(DefaultVikingFormsPreFillDataService.class);

    private static final String FUNCTION_NAME = "ZGET_NOTIF_BY_QMNUM";
    private static final String ZGET_CREDIT_ZCERT = "ZGET_CREDIT_ZCERT";
    private static final String RESULT_TABLE_NAME = "ZT_NOTIF_ITEM";
    private static final String IM_QMNUM = "IM_QMNUM";
    private static final String NEW_DL_SAP = "NEW-D/L";
    private static final String NEW_DL_HYBRIS = "NEW-DL";
    public static final String FEGRP = "FEGRP";
    public static final String FETXT = "FETXT";
    public static final String FECOD = "FECOD";

    @Autowired
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    private DateConversionKeyDao dateConversionKeyDao;
    private InitialPrefillValueDao initialPrefillValueDao;

    /**
     * {@inheritDoc}
     */
    public VikingFormsPreFillData getData(String notificationId, String prefillId) {
        JCoTable resultTable = null;

        //duplicate keys are possible in the reply, we want to overwrite the keys with its most recent value
        final Map<String, String> notificationHistoryEntries = new HashMap<>();

        try {
            resultTable = getResultTableFromSAP(RESULT_TABLE_NAME, prefillId);
        } catch (Exception e) {
            LOG.error("Error during call to SAP ERP backend for notification prefill data.", e);
        }
        //iterate through table and add values

        if (resultTable != null && !resultTable.isEmpty()) {
            LOG.info("VIK-2883 Pre fill data from SAP for notification [" + prefillId + "]");
            for (int i = 0; i < resultTable.getNumRows(); i++) {
                resultTable.setRow(i);
                String fegrp = resultTable.getString(FEGRP);
                final String fetxt = resultTable.getString(FETXT);
                final String fecod = resultTable.getString(FECOD);
                LOG.info(MessageFormat.format("VIK-2883: Row: {0} FETXT [{1}] - FEGRP [{2}] - FECOD [{3}]", i, fetxt, fegrp, fecod));
                if (!fetxt.isEmpty()) {

                    if (NEW_DL_SAP.equals(fegrp)) {
                        LOG.info("VIK-2883: Changing incomming FEGRP from [" + NEW_DL_SAP + "] to [" + NEW_DL_HYBRIS + "]");
                        fegrp = NEW_DL_HYBRIS;
                    }

                    final String key = fegrp + "-" + fecod;
                    LOG.info("VIK-2883: Adding key " + key);

                    if (isDateConversionRequired(key)) {
                        final String value = formatDate(fetxt);
                        LOG.info("VIK-2883: (is a date) Adding key value pair " + key + " : " + fetxt);
                        notificationHistoryEntries.put(key, value);
                    } else {
                        LOG.info("VIK-2883: (is not a date) Adding key value pair " + key + " : " + fetxt);
                        notificationHistoryEntries.put(key, fetxt);
                    }
                }
            }
        }

        final VikingFormsPreFillData preFillData = new VikingFormsPreFillData();
        final List<VikingFormData> vikingFormDataList = new ArrayList<>();

        for(Map.Entry<String, String> entry : notificationHistoryEntries.entrySet()) {
            final VikingFormData vikingFormData = new VikingFormData();
            vikingFormData.setKey(entry.getKey());
            vikingFormData.setValue(entry.getValue());

            LOG.info("VIK-2883: KEY: "  + entry.getKey() + " VALUE: " + entry.getValue());

            vikingFormDataList.add(vikingFormData);
        }

        // If the prefillId and notificationId is different, it is a new certificate and we need to clear some fields.
        if (!notificationId.equals(prefillId)) {
            LOG.info("VIK-2883: New certificate for notification " + notificationId + ". Clearing prefill values.");
            clearPrefillValues(vikingFormDataList);
        }

        preFillData.setVikingFormData(vikingFormDataList);

        LOG.info("VIK-2883: Return prefill data with [" + preFillData.getVikingFormData().size() + "] rows to prefill.");

        return preFillData;
    }

    private void clearPrefillValues(final List<VikingFormData> vikingFormDataList) {
        final List<InitialPrefillValueModel> initialPrefillValues = initialPrefillValueDao.getAll();

        final List<String> initialKeys = initialPrefillValues.stream().map(value -> value.getKey()).collect(Collectors.toList());

        // Remove the VikingFormData values that are not initial
        vikingFormDataList.forEach(data -> {
            if (!initialKeys.contains(data.getKey())) {
                LOG.info("VIK-2883: (Initial Value Cleanup) Clearing key: " + data.getKey());
                data.setValue(StringUtils.EMPTY);
            }
        });
    }

    private boolean isDateConversionRequired(String key) {
        List<DateConversionKeyModel> dateConversions = dateConversionKeyDao.getAll();

        for (DateConversionKeyModel dateConversionKey : dateConversions) {
            if (dateConversionKey.getKey().equalsIgnoreCase(key)) {
                return true;
            }
        }

        return false;
    }

    private String formatDate(final String dateString) {
        // If the date string matches format 01-01-0001 we need to flip it to match 0001-01-01
        if (dateString.matches("^\\d{2}\\-\\d{2}\\-\\d{4}$")) {
            final String[] strings = dateString.split("-");
            return strings[2] + "-" + strings[1] + "-" + strings[0];
        } else {
            return dateString;
        }
    }

    public CheckCreditData checkCredit(String serviceStation, String formID) {
        CheckCreditData creditData = new CheckCreditData();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
            final JCoFunction function = managedConnection.getFunction(ZGET_CREDIT_ZCERT);
            function.getImportParameterList().setValue("IM_SERV_STAT_NO", serviceStation);
            function.getImportParameterList().setValue("IM_EQART", formID);

            managedConnection.execute(function);

            final String ex_credit = function.getExportParameterList().getString("EX_CREDIT");
            final String ex_negative_qty = function.getExportParameterList().getString("EX_NEGATIVE_QTY");

            LOG.debug("Received EX_CREDIT [" + ex_credit + "] and EX_NEGATIVE_QTY [" + ex_negative_qty + "] for station [" + serviceStation + "] and equipment art [" + formID + "]");

            creditData.setCredit(ex_credit);
            creditData.setNegativeAllowed(ex_negative_qty);

        } catch (Exception e) {
            creditData.setCredit("0");
            creditData.setNegativeAllowed("N");
            LOG.error("Error during sap erp rfc call (check credit) for module : " + ZGET_CREDIT_ZCERT, e);
        }

        return creditData;
    }

    protected JCoTable getResultTableFromSAP(String resultTableName, String notificationId) throws BackendException {
        JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);

        LOG.debug(MessageFormat.format("Get data from for SAP for FUNCTION [{0}] with [{1}] [{2}] and resultTable [{2}]", FUNCTION_NAME, IM_QMNUM, notificationId, resultTableName));
        final JCoFunction function = managedConnection.getFunction(FUNCTION_NAME);
        function.getImportParameterList().setValue(IM_QMNUM, notificationId);

        managedConnection.execute(function);

        return function.getTableParameterList().getTable(resultTableName);
    }

    @Override
    public boolean isCreditCheckPass(String serviceStation, String formID) {
        final CheckCreditData checkCreditData = checkCredit(serviceStation, formID);
        final Boolean creditAmountPositive = Double.valueOf(checkCreditData.getCredit()) > 0;
        final Boolean negativeCreditAllowed = checkCreditData.getNegativeAllowed().equalsIgnoreCase("Y");

       return creditAmountPositive || (!creditAmountPositive && negativeCreditAllowed);
    }

    private boolean containsNumericsOnly(final String input) {
        return input.matches("[0-9]+");
    }

    @Required
    public void setDateConversionKeyDao(DateConversionKeyDao dateConversionKeyDao) {
        this.dateConversionKeyDao = dateConversionKeyDao;
    }

    @Required
    public void setInitialPrefillValueDao(InitialPrefillValueDao initialPrefillValueDao) {
        this.initialPrefillValueDao = initialPrefillValueDao;
    }
}
