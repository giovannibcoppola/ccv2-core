/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.setup;

import static com.viking.erpservices.constants.VikingerpservicesConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.viking.erpservices.constants.VikingerpservicesConstants;
import com.viking.erpservices.service.VikingDummyService;


@SystemSetup(extension = VikingerpservicesConstants.EXTENSIONNAME)
public class VikingerpservicesSystemSetup
{

		private final VikingDummyService vikingerpservicesService;

	public VikingerpservicesSystemSetup(final VikingDummyService vikingerpservicesService)
		{
			this.vikingerpservicesService = vikingerpservicesService;
		}

		@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
		public void createEssentialData()
		{

			//vikingerpservicesService.createLogo(PLATFORM_LOGO_CODE);
		}

		private InputStream getImageStream()
		{
			return VikingerpservicesSystemSetup.class.getResourceAsStream("/vikingerpservices/sap-hybris-platform.png");
		}
	}
