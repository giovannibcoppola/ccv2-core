/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.viking.erpservices.service.VikingDummyService;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultVikingDummyService implements VikingDummyService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingDummyService.class);
    public static final String HYBRIS_TEST_FM_01 = "HYBRIS_TEST_FM_01";

    private JCoManagedConnectionFactory jCoManagedConnectionFactory;


    @Override
    public String getDummyName(String name) {
        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(HYBRIS_TEST_FM_01);
            function.getImportParameterList().setValue("IV_NAME", "Adam and Kris");
            //		JCoStructure orderHeaderIn = function.getImportParameterList().getStructure("ORDER_HEADER_IN");
            //		orderHeaderIn.setValue("DOC_TYPE", "OR");
            //		orderHeaderIn.setValue("SALES_ORG", b2bSalesUnit);
            //		orderHeaderIn.setValue("DISTR_CHAN", "01");
            //		orderHeaderIn.setValue("DIVISION", "00");
            //		orderHeaderIn.setValue("ORD_REASON", "ZR");

            //		JCoTable orderItemsIn = function.getTableParameterList().getTable("ORDER_ITEMS_IN");
            //		orderItemsIn.appendRow();
            //		orderItemsIn.setValue("ITM_NUMBER", "000010");
            //		orderItemsIn.setValue("HG_LV_ITEM", "000000");
            //		orderItemsIn.setValue("MATERIAL", material);
            //		orderItemsIn.setValue("DLV_GROUP", "000");
            //		orderItemsIn.setValue("TARGET_QTY", "0000000000001");
            //		orderItemsIn.setValue("TARGET_QU", "PCS");
            //		orderItemsIn.setValue("REQ_QTY", "0000000000000");
            //		orderItemsIn.setValue("REQ_TIME", "00:00:00");


            managedConnection.execute(function);
            //LOG.info("GMESSEGE for call "+HYBRIS_TEST_FM_01+" : " + function.getExportParameterList().getValue("GMESSAGE"));

            return function.getExportParameterList().getString("EV_NEW_NAME");
        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call for module : " + HYBRIS_TEST_FM_01, e);
        }
        return "";
    }


    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }
}
