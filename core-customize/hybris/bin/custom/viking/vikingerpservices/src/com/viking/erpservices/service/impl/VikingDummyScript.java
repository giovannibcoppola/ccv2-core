package com.viking.erpservices.service.impl;

import com.viking.erpservices.service.VikingDummyService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.site.BaseSiteService;

public class VikingDummyScript {



    public String test() {
        BaseSiteService baseSiteService = (BaseSiteService) Registry.getApplicationContext().getBean("baseSiteService");
        baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID("servicepartnerportal"),true);

        VikingDummyService vikingDummyService = (VikingDummyService) Registry.getApplicationContext().getBean("vikingDummyService");

        return vikingDummyService.getDummyName("Adam & Kris");
    }
}
