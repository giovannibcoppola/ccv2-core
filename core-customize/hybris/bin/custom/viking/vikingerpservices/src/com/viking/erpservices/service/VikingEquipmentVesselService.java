package com.viking.erpservices.service;

import com.viking.core.data.EquipmentData;
import com.viking.search.data.VesselEquipmentDataList;

import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public interface VikingEquipmentVesselService {

    VesselEquipmentDataList getVesselEquipmentList(String funcLocID);

    void dismantleEquipment(String serialNumber);

    void addEquipment(String serialNumber, String vesselID) throws BackendException;

    EquipmentData searchEquipment(String serialNumber);
}
