package com.viking.erpservices.service.forms.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VikingFormsPreFillData
{


	@XmlElement (name = "vikingFormData")
	private List<VikingFormData> vikingFormData;


	public VikingFormsPreFillData()
	{
	}

	public List<VikingFormData> getVikingFormData()
	{
		return vikingFormData;
	}

	public void setVikingFormData(List<VikingFormData> vikingFormData)
	{
		this.vikingFormData = vikingFormData;
	}
}

