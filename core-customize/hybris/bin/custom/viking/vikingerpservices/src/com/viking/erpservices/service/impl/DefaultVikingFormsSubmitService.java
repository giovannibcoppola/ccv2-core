package com.viking.erpservices.service.impl;

import javax.xml.stream.XMLStreamException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoTable;
import com.viking.core.util.YFormXMLhelper;
import com.viking.erpservices.service.VikingFormsSubmitService;
import com.viking.erpservices.service.VikingNotificationService;

import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;

import static com.viking.erpservices.constants.VikingerpservicesConstants.HYBRIS;
import static com.viking.erpservices.constants.VikingerpservicesConstants.J_CO_STATELESS;
import static com.viking.erpservices.constants.VikingerpservicesConstants.ZMODIFY_NOTIFICATION_HYBRIS_01;

public class DefaultVikingFormsSubmitService implements VikingFormsSubmitService {

    private static final Logger LOG = Logger.getLogger(DefaultVikingFormsSubmitService.class);
    private static final String CERT_NOTIFICATION_TABLE = "I_VIQMFE_T";
    private static final String NEW_DL_SAP = "NEW-D/L";
    private static final String NEW_DL_HYBRIS = "NEW-DL";
    private static final String I_EQUNR = "I_EQUNR";
    private static final String I_STATUS = "I_STATUS";
    private static final int I_EQUNR_LENGTH = 18;
    private static final String CONDEMNATION_FUNCTION = "Y_CONDEMN";
    private static final String CONDEMNATION_TABLE = "IT_CHAR_NEW";
    private static final String Z_CONDEMNDATE = "Z_CONDEMNDATE";
    private static final String CHARACT = "CHARACT";
    private static final String VALUE_CHAR = "VALUE_CHAR";
    public static final String CERT_0020 = "CERT-0020";
    public static final String FETXT = "FETXT";
    public static final String IM_QMNUM = "IM_QMNUM";
    public static final String IM_PRINT = "IM_PRINT";
    public static final String IM_SERV_STAT_NO = "IM_SERV_STAT_NO";
    public static final String ZGET_NOTIF_BY_QMNUM = "ZGET_NOTIF_BY_QMNUM";
    public static final String ZT_NOTIF_ITEM = "ZT_NOTIF_ITEM";
    public static final String QMNUM = "QMNUM";
    public static final String FEGRP = "FEGRP";
    public static final String FECOD = "FECOD";


    @Autowired
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    private YFormXMLhelper yFormXMLhelper;
    private VikingNotificationService vikingNotificationVesselService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void submitYFormDataToSap(String notificationId, String serviceStation, final String input) {

        LOG.info("VIK-2883: Notification ID: " + notificationId);
        LOG.info("VIK-2883: Service Station: " + serviceStation);
        LOG.info("VIK-2883: Form input XML: " + input);

        JCoFunction function = null;

        try {

            Map<String, Map<String, String>> transformationResult = yFormXMLhelper.transformNotificationYFormXML(input);
            final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);

            LOG.info("VIK-2883: station number is:" + serviceStation);
            LOG.info("VIK-2883: Function data used [IM_QUMNUM - " + notificationId + "], [IM_PRINT - ] " + "[IM_SERV_STAT_NO - " + serviceStation + "]");

            function = managedConnection.getFunction(ZMODIFY_NOTIFICATION_HYBRIS_01);
            function.getImportParameterList().setValue(IM_QMNUM, notificationId);
            function.getImportParameterList().setValue(IM_PRINT, "");
            function.getImportParameterList().setValue(IM_SERV_STAT_NO, serviceStation);
            JCoTable table = function.getTableParameterList().getTable(CERT_NOTIFICATION_TABLE);

            transformationResult.values().forEach(map -> {
                table.appendRow();
                LOG.info("VIK-2883: ROW:");
                map.forEach((key, value) -> {
                    LOG.info("VIK-2883:  [Key - " + key + "], [Value - " + value + "]");
                    if (NEW_DL_HYBRIS.equals(value)) {
                        LOG.info("VIK-2883:  Changing from " + NEW_DL_HYBRIS + " to " + NEW_DL_SAP);
                        value = NEW_DL_SAP;
                    }
                    table.setValue(key, value);
                });
            });

            managedConnection.execute(function);
            final String url = function.getExportParameterList().getValue("EX_URL").toString();
            LOG.info("SAVE action for notificationID: " + notificationId +", received URL : '" + url + "'");
        } catch (XMLStreamException exception) {
            LOG.error("Unable to parse yForm XML data", exception);
        } catch (BackendException exception) {
            LOG.error("Error during call to SAP ERP backend for adding yForm notification data.", exception);
        } finally {
            JCoParameterList list = function.getTableParameterList();
            if (list == null){
                LOG.error("List is null will throw eror");
                throw new IllegalStateException("List is null");
            }
            JCoTable o_return = list.getTable("O_RETURN");
            LOG.info("VIK-2883: O_RETURN from executing connection - " + o_return);
        }
    }

    private void updateDatedItemsList(String notificationId) throws BackendException {
        if(vikingNotificationVesselService.updateDatedItemsList(notificationId)) {
            LOG.info("VIK-765: Successfully sent Dated Item List generation request update to SAP!");
        } else {
            LOG.error("VIK-765: Something went wrong sending Dated Items List generation request to SAP");
        }
        /*
        if(transformationResult != null && transformationResult.get(CERT_0020) != null && transformationResult.get(CERT_0020).get(FETXT) != null) {
            final String newNextInspectionDateText = transformationResult.get(CERT_0020).get(FETXT) ;
            LOG.info("VIK-765: New date from YForm: " + newNextInspectionDateText); //30-10-2022
            final JCoFunction functionNotifications = managedConnection.getFunction(ZGET_NOTIF_BY_QMNUM);
            functionNotifications.getImportParameterList().setValue(IM_QMNUM, notificationId);
            managedConnection.execute(functionNotifications);
            final JCoTable table = functionNotifications.getTableParameterList().getTable(ZT_NOTIF_ITEM);

            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                if(notificationId.equals(table.getString(QMNUM)) && "CERT".equals(table.getString(FEGRP)) && "0020".equals(table.getString(FECOD))) {
                    LOG.info("VIK-765: Old new inspection date from SAP: " + oldNextInspectionDate); //24-10-2022
                    if(StringUtils.isNotEmpty(newNextInspectionDateText) && StringUtils.isNotEmpty(oldNextInspectionDate) && !oldNextInspectionDate.equals(newNextInspectionDateText)) {
                        LOG.info("VIK-765: Generating new dated items list for inspection date: " + newNextInspectionDateText);
                        if(vikingNotificationVesselService.updateDatedItemsList(notificationId)) {
                            LOG.info("Succesfully sent Dated Item List generation request update to SAP!");
                        } else {
                            LOG.error("Something went wrong sending Dated Items List generation request to SAP");
                        }
                    }
                }
            }
        }
        */
    }

    private String getOldNextInspectionDateFromSAP(final String notificationId, final JCoConnection managedConnection) throws BackendException {
        final JCoFunction functionNotifications = managedConnection.getFunction(ZGET_NOTIF_BY_QMNUM);
        functionNotifications.getImportParameterList().setValue(IM_QMNUM, notificationId);
        managedConnection.execute(functionNotifications);
        final JCoTable table = functionNotifications.getTableParameterList().getTable(ZT_NOTIF_ITEM);

        for (int i = 0; i < table.getNumRows(); i++) {
            table.setRow(i);
            if(notificationId.equals(table.getString(QMNUM)) && "CERT".equals(table.getString(FEGRP)) && "0020".equals(table.getString(FECOD))) {
                return table.getString(FETXT);
            }
        }
        return StringUtils.EMPTY;
    }

    @Override
    public String submitYFormDataToSapAndGetPDFUrl(String notificationId, String serviceStation, String input, String print) {
        try {
            Map<String, Map<String, String>> transformationResult = yFormXMLhelper.transformNotificationYFormXML(input);

            final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);

            LOG.debug("Function data used [IM_QUMNUM - " + notificationId + "], [IM_PRINT - " + print + "] " + "[IM_SERV_STAT_NO - " + serviceStation + "]");

            final JCoFunction function = managedConnection.getFunction(ZMODIFY_NOTIFICATION_HYBRIS_01);
            function.getImportParameterList().setValue(IM_QMNUM, notificationId);
            function.getImportParameterList().setValue(IM_PRINT, print);
            function.getImportParameterList().setValue(IM_SERV_STAT_NO, serviceStation);
            JCoTable table = function.getTableParameterList().getTable(CERT_NOTIFICATION_TABLE);

            transformationResult.values().forEach(map -> {
                table.appendRow();
                LOG.debug("ROW:");
                map.forEach((key, value) -> {
                    LOG.debug("[Key - " + key + "], [Value - " + value + "]");
                    if (NEW_DL_HYBRIS.equals(value)) {
                        LOG.debug("Changing from " + NEW_DL_HYBRIS + " to " + NEW_DL_SAP);
                        value = NEW_DL_SAP;
                    }
                    table.setValue(key, value);
                });
            });

            managedConnection.execute(function);
            final String url = function.getExportParameterList().getValue("EX_URL").toString();
            LOG.info("PRINT action for notificationID: " + notificationId +", received URL : '" + url + "'");
            updateDatedItemsList(notificationId);
            return url;
        } catch (XMLStreamException exception) {
            LOG.error("Unable to parse yForm XML data", exception);
        } catch (BackendException exception) {
            LOG.error("Error during call to SAP ERP backend for adding yForm notification data.", exception);
        }

        LOG.error("Not able to return EX_URL. Returning empty string instead for notificationID: " + notificationId + ".");
        return StringUtils.EMPTY;
    }

    @Override
    public void submitCondemnationToSap(final String input) {
        LOG.info("VIK-2791: INPUT XML " + input);
        try {
            final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);
            final Map<String, String> transformationResult = yFormXMLhelper.transformCondemnationXML(input);

            String equipmentNumber = transformationResult.get(I_EQUNR);

            if (containsNumericsOnly(equipmentNumber)) {
                LOG.debug("Leftpadding I_EQUNR input parameter with zero's. Original value: " + equipmentNumber);
                equipmentNumber = StringUtils.leftPad(equipmentNumber, I_EQUNR_LENGTH, "0");
            }

            LOG.debug("Function data used [I_EQUNR - " + equipmentNumber + "], [I_STATUS - " + transformationResult.get(I_STATUS) + "]");

            final JCoFunction function = managedConnection.getFunction(CONDEMNATION_FUNCTION);
            function.getImportParameterList().setValue(I_EQUNR, equipmentNumber);
            function.getImportParameterList().setValue(I_STATUS, transformationResult.get(I_STATUS));

            final JCoTable table = function.getTableParameterList().getTable(CONDEMNATION_TABLE);
            transformationResult.forEach((key, value) -> {
                LOG.info("VIK-2791: AFTER TRANSFORMATION " + key + " : " + value);
                if (shouldPopulateKeyAndValue(key)) {

                    if (key.equalsIgnoreCase(Z_CONDEMNDATE)) {
                        LOG.debug("Formatting date for condemnation: " + value);
                        value = formatDateForCondemnation(value);
                    }

                    table.appendRow();
                    table.setValue(CHARACT, key);
                    table.setValue(VALUE_CHAR, value);
                    LOG.info(" VIK-2791: [CHARACT - " + key + "], [VALUE_CHAR - " + value + "]");
                }
            });

            managedConnection.execute(function);
        } catch (Exception exception) {
            LOG.error("Something happened trying to submit condemnation to SAP", exception);
        }
    }

    private boolean containsNumericsOnly(final String input) {
        return input.matches("[0-9]+");
    }

    private boolean shouldPopulateKeyAndValue(final String key) {
        if (key.equalsIgnoreCase(I_EQUNR) || key.equalsIgnoreCase(I_STATUS)) {
            LOG.debug("Should not populate key: " + key);
            return false;
        }

        return true;
    }

    private String formatDateForCondemnation(final String dateString) {
        String formattedDateString = dateString;

        if (dateString.matches("^\\d{4}\\-\\d{2}\\-\\d{2}$")) {
            final String[] strings = dateString.split("-");
            formattedDateString = strings[2] + "-" + strings[1] + "-" + strings[0];
        }

        return formattedDateString.replace("-", ".");
    }

    @Required
    public void setyFormXMLhelper(YFormXMLhelper yFormXMLhelper) {
        this.yFormXMLhelper = yFormXMLhelper;
    }

    @Required
    public void setVikingNotificationVesselService(VikingNotificationService vikingNotificationVesselService) {
        this.vikingNotificationVesselService = vikingNotificationVesselService;
    }
}
