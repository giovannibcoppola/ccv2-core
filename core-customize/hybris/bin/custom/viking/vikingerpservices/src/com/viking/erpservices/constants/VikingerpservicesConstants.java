/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.constants;

/**
 * Global class for all Vikingerpservices constants. You can add global constants for your extension into this class.
 */
public final class VikingerpservicesConstants extends GeneratedVikingerpservicesConstants
{
	public static final String EXTENSIONNAME = "vikingerpservices";
	public static final String J_CO_STATELESS = "JCoStateless";
	public static final String HYBRIS = "Hybris";
	public static final String ZMODIFY_NOTIFICATION_HYBRIS_01 = "ZMODIFY_NOTIFICATION_HYBRIS_01";

	private VikingerpservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "vikingerpservicesPlatformLogo";
}
