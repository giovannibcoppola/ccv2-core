package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;

import com.sap.conn.jco.JCoTable;
import com.viking.erpservices.service.VikingDetailsVesselService;
import com.viking.search.data.FormerVesselData;
import com.viking.search.data.SearchVesselOverviewData;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class DefaultVikingDetailsVesselService implements VikingDetailsVesselService {
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingFindVesselsService.class);
    public static final String ZGET_FUNCLOC_DETAILS = "ZGET_FUNCLOC_DETAILS";

    @Override
    public SearchVesselOverviewData detailsVessel(String input) {

        final SearchVesselOverviewData searchVesselOverviewData = new SearchVesselOverviewData();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZGET_FUNCLOC_DETAILS);
            function.getImportParameterList().setValue("IM_FUNCLOC", input);
            managedConnection.execute(function);
            JCoParameterList exportList = function.getExportParameterList();

            // searchVesselOverviewData.setCallSignal(); setting value from controller
            // searchVesselOverviewData.setVesselID(exportList.getString("EX_INVNR")); need to reconfirm the return

            searchVesselOverviewData.setFlagState(exportList.getString("EX_FLAG_STATE"));

            final String ex_owner_name1 = exportList.getString("EX_OWNER_NAME1");
            final String ex_owner_name2 = exportList.getString("EX_OWNER_NAME2");
            final String ex_owner_street = exportList.getString("EX_OWNER_STREET");
            final String ex_owner_postlcode = exportList.getString("EX_OWNER_POSTLCODE");
            final String ex_owner_city = exportList.getString("EX_OWNER_CITY");
            final String ex_owner_country = exportList.getString("EX_OWNER_COUNTRY");

            final StringBuilder sb = new StringBuilder();

            if (StringUtils.isNotEmpty(ex_owner_name1)) {
                sb.append(ex_owner_name1);
            }
            if (StringUtils.isNotEmpty(ex_owner_name2)) {
                sb.append(" ");
                sb.append(ex_owner_name2);
            }
            if( StringUtils.isNotEmpty(ex_owner_street)) {
                sb.append(", ");
                sb.append(ex_owner_street);
            }
            if( StringUtils.isNotEmpty(ex_owner_postlcode)) {
                sb.append(", ");
                sb.append(ex_owner_postlcode);
            }
            if( StringUtils.isNotEmpty(ex_owner_city)) {
                sb.append(" ");
                sb.append(ex_owner_city);
            }
            if( StringUtils.isNotEmpty(ex_owner_country)) {
                sb.append(", ");
                sb.append(ex_owner_country);
            }

            searchVesselOverviewData.setOwner(sb.toString());
            searchVesselOverviewData.setVesselName(exportList.getString("EX_FUNCLOC_NAME"));

            JCoTable table = function.getTableParameterList().getTable("ET_FUNCLOC_INF");
            final List<FormerVesselData> formerVesselData = new ArrayList<>();


            for (int i = 0; i < table.getNumRows(); i++)
            {
                table.setRow(i);
                final FormerVesselData formerDataEntry = new FormerVesselData();
                formerDataEntry.setFormerName(table.getString("SHIP_NAME"));
                formerVesselData.add(formerDataEntry);
            }

            searchVesselOverviewData.setFormerName(formerVesselData);

            // searchVesselOverviewData.setFormerVesselName();
            return searchVesselOverviewData;
        } catch (Exception e) {
            LOG.error("Error during sap erp rfc call (DetailsVessel) for module : " + ZGET_FUNCLOC_DETAILS, e);
        }
        return searchVesselOverviewData;
    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }


}
