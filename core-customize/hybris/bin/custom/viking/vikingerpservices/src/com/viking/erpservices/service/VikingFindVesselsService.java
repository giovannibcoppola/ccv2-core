/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.service;

import com.viking.search.data.SearchVesselDataList;

public interface VikingFindVesselsService
{

	SearchVesselDataList findVesselByName(String input);
	SearchVesselDataList findVesselByFormerName(String input);
	SearchVesselDataList findVesselByIMO(String input);
	SearchVesselDataList findVesselBySerialNumber(String input);
}
