package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.jalo.NotificationPrefill;
import com.viking.core.service.VikingNotificationPrefillService;
import com.viking.core.util.ContentHelper;
import com.viking.erpservices.service.VikingNotificationService;
import com.viking.search.data.VesselNotificationData;
import com.viking.search.data.VesselNotificationDataList;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.sap.core.jco.exceptions.BackendRuntimeException;
import de.hybris.platform.util.Config;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The type Default viking notification vessel service.
 */
public class DefaultVikingNotificationVesselService implements VikingNotificationService {
    private static final String ZGET_NOTIF_BY_SERNO2 = "ZGET_NOTIF_BY_SERNO2";
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingNotificationVesselService.class);
    private static final String IM_SERV_STAT_NO = "IM_SERV_STAT_NO";
    private static final String OPEN_NOTIF = "OPEN_NOTIF";
    private static final String IM_QMNUM = "IM_QMNUM";
    public static final String ZT_NOTIF = "ZT_NOTIF";
    public static final String URL = "URL";
    public static final String ZCREATE_CONTAINERLABEL_HYB = "ZCREATE_CONTAINERLABEL_HYB";
    private static final String ZGET_IPAD_SERVSTAT = "ZGET_IPAD_SERVSTAT";
    private static final String ZGET_CONTLABEL_SERVSTAT = "ZGET_CONTLABEL_SERVSTAT";
    private static final String IM_EQUNR = "IM_EQUNR";
    private static final String EX_IPAD = "EX_IPAD";
    private static final String IM_SERIALNR = "IM_SERIALNR";
    private static final String QMNUM = "QMNUM";
    private static final String ADDAT = "ADDAT";
    private static final String STRMN = "STRMN";
    private static final String SPC = "SPC";
    private static final String URL_DATED_ITEMS = "URL_DATED_ITEMS";
    private static final String DIL = "DIL";
    private static final String CLP = "CLP";
    private static final String EX_CONTLABEL = "EX_CONTLABEL";
    private static final String ZCREATE_DATEDITEMS_HYB = "ZCREATE_DATEDITEMS_HYB";
    private static final DateTimeFormatter OUTBOUND_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private static final String ZERO_DATE = "0000-00-00";

    private JCoManagedConnectionFactory jCoManagedConnectionFactory;
    private ContentHelper contentHelper;
    private VikingNotificationPrefillService vikingNotificationPrefillService;


    @Override
    public VesselNotificationDataList getNotifications(String serialNumber, String serviceStation) throws Exception {
        return getNotifications(serialNumber, serviceStation, null);
    }

    @Override
    public VesselNotificationDataList getNotifications(String serialNumber, String serviceStation, String equipmentId) throws Exception {
        LOG.info("Start getNotifications : {}", new Timestamp(System.currentTimeMillis()));
        VesselNotificationDataList notifVesselDataList = new VesselNotificationDataList();

        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("SerialNumber [{0}], ServiceStation [{1}]", serialNumber, serviceStation));
        }
        JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function = managedConnection.getFunction(ZGET_NOTIF_BY_SERNO2);
        function.getImportParameterList().setValue(IM_SERIALNR, serialNumber);

        managedConnection.execute(function);

        JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        LOG.debug("Response from ZGET_NOTIF_BY_SERNO2 :" + table);
        final List<VesselNotificationData> notificationDatas = new ArrayList<>();

        LOG.info("Before setIPadNotification : {}", new Timestamp(System.currentTimeMillis()));
        String ipadNotification = getIpadNotification(serviceStation, equipmentId, managedConnection);
        LOG.info("After setIPadNotification : {}", new Timestamp(System.currentTimeMillis()));

        LOG.info("Before isAuthorisedForContainerLabel : {}", new Timestamp(System.currentTimeMillis()));
        boolean isAuthorisedForContainerLabel = isAuthorisedForContainerLabel(serviceStation, equipmentId, managedConnection);
        LOG.info("After isAuthorisedForContainerLabel : {}", new Timestamp(System.currentTimeMillis()));

        for (int i = 0; i < table.getNumRows(); i++) {
            LOG.info("For loop start iteration {} : {}", i , new Timestamp(System.currentTimeMillis()));
            table.setRow(i);

            final VesselNotificationData notifVesselDataEntry = new VesselNotificationData();
            final String notificationNumber = table.getString(QMNUM);
            notifVesselDataEntry.setNotificationNumber(notificationNumber);
            notifVesselDataEntry.setPrefillNotification(notificationNumber);
            final String addat = table.getString(ADDAT);
            notifVesselDataEntry.setInspectionDate(convertIfNotZeroDate(addat));
            final String strmn = table.getString(STRMN);
            notifVesselDataEntry.setDateForNextInspection(convertIfNotZeroDate(strmn));
            notifVesselDataEntry.setOpenNotification(table.getString(OPEN_NOTIF));
            notifVesselDataEntry.setIpadNotification(ipadNotification);

            notifVesselDataEntry.setCertificate(StringUtils.isNotEmpty(table.getString(URL)));
            notifVesselDataEntry.setDatedItemsList(StringUtils.isNotEmpty(table.getString(URL_DATED_ITEMS)));
            notifVesselDataEntry.setContainerLabel(isAuthorisedForContainerLabel);

            LOG.debug("Notification data: [Notification - " + notificationNumber + "], [InspectionDate - " + addat + " ], [NextInspection - " + strmn + "], [OpenNotification - " + table.getString(OPEN_NOTIF) + "] " + "[URL - " + table.getString(URL) + "]" + "[URL_DATED_ITEMS - " + table.getString(URL_DATED_ITEMS) + "]");

            notificationDatas.add(notifVesselDataEntry);
            LOG.info("For loop end iteration {} : {}", i , new Timestamp(System.currentTimeMillis()));
        }

        notifVesselDataList.setVesselNotificationDataList(notificationDatas);
        LOG.info("Before Collection Sort : {}", new Timestamp(System.currentTimeMillis()));
        Collections.sort(notificationDatas, (object1, object2) -> object2.getNotificationNumber().compareTo(object1.getNotificationNumber()));
        LOG.info("After Collection Sort : {}", new Timestamp(System.currentTimeMillis()));

        List<VesselNotificationData> notificationDataList = notifVesselDataList.getVesselNotificationDataList();

        LOG.info("Before prefill : {}", new Timestamp(System.currentTimeMillis()));
        if (CollectionUtils.isNotEmpty(notificationDataList)) {
            VesselNotificationData vesselNotificationData = notificationDataList.get(0);
            List<NotificationPrefill> notificationPrefills = vikingNotificationPrefillService.getNotifications(vesselNotificationData.getNotificationNumber());

            if (CollectionUtils.isEmpty(notificationPrefills) && notificationDataList.size() > 1) {
                vesselNotificationData.setPrefillNotification(notificationDataList.get(1).getNotificationNumber());
            }
        }
        LOG.info("After prefill : {}", new Timestamp(System.currentTimeMillis()));

        LOG.info("Before moving dates around : {}", new Timestamp(System.currentTimeMillis()));
        // Move date for next inspection around to match the actual certificate values when displayed on frontend
        if (CollectionUtils.isNotEmpty(notificationDataList)) {
            for (int i = notificationDataList.size() - 1; i >= 0; i--) {
                VesselNotificationData vesselNotificationData = notificationDataList.get(i);

                if (i > 0) {
                    final String dateForNextInspection = notificationDataList.get(i - 1).getDateForNextInspection();
                    vesselNotificationData.setDateForNextInspection(dateForNextInspection);
                } else {
                    final String inspectionDate = vesselNotificationData.getInspectionDate();
                    vesselNotificationData.setDateForNextInspection(inspectionDate);
                }
            }
        }
        LOG.info("After moving dates around : {}", new Timestamp(System.currentTimeMillis()));

        LOG.info("End getNotifications : {}", new Timestamp(System.currentTimeMillis()));
        return notifVesselDataList;
    }

    /**
     *
     * @param notificationId
     * @param serialNo
     * @throws BackendException
     */
    @Override
    public String getCertificate(final String notificationId, final String serialNo) throws BackendException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Get Certificate : notificationNumber [{0}] ", notificationId));
        }

        final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function = managedConnection.getFunction(ZGET_NOTIF_BY_SERNO2);
        function.getImportParameterList().setValue(IM_SERIALNR, serialNo);

        managedConnection.execute(function);
        final JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        for (int i = 0; i < table.getNumRows(); i++) {
            table.setRow(i);

            if (notificationId.equals(table.getString(QMNUM)) && StringUtils.isNotBlank(table.getString(URL))) {
                final Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia(SPC + notificationId, table.getString(URL));
                if (mediaOptional.isPresent()) {
                    return Config.getParameter("media.servicepartnerportal.https") + mediaOptional.get().getURL();
                }
            }
        }
        throw new IllegalStateException("Could not download certificate for: " + notificationId);
    }

    /**
     *
     * @param notificationId
     * @param serialNo
     * @throws BackendException
     */
    @Override
    public String getDatedItemsList(final String notificationId, final String serialNo) throws BackendException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Get Dated Items List : notificationNumber [{0}] ", notificationId));
        }

        final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function = managedConnection.getFunction(ZGET_NOTIF_BY_SERNO2);
        function.getImportParameterList().setValue(IM_SERIALNR, serialNo);
        managedConnection.execute(function);
        final JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        for (int i = 0; i < table.getNumRows(); i++) {
            table.setRow(i);

            if (notificationId.equals(table.getString(QMNUM)) && StringUtils.isNotBlank(table.getString(URL_DATED_ITEMS))) {
                final Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia(DIL + notificationId, table.getString(URL_DATED_ITEMS));
                if (mediaOptional.isPresent()) {
                    return Config.getParameter("media.servicepartnerportal.https") + mediaOptional.get().getURL();
                }
            }
        }
        throw new IllegalStateException("Could not download Dated Items List for: " + notificationId);
    }

    /**
     *
     * @param notificationId
     * @param serviceStation
     * @param equipmentId
     * @throws BackendException
     */
    @Override
    public String createContainerLabel(final String notificationId, final String serviceStation, final String equipmentId) throws BackendException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Get Container Label : notificationNumber [{0}] ", notificationId));
        }
        final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        boolean isAuthorisedForContainerLabel = isAuthorisedForContainerLabel(serviceStation, equipmentId, managedConnection);
        if(!isAuthorisedForContainerLabel) {
            throw new IllegalAccessError("User " + serviceStation + " is not allowed to print container labels!");
        }

        final JCoFunction function = managedConnection.getFunction(ZCREATE_CONTAINERLABEL_HYB);
        function.getImportParameterList().setValue(IM_QMNUM, notificationId);
        managedConnection.execute(function);
        final JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        if (StringUtils.isNotBlank(table.getString(URL))) {
            final Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia(CLP + notificationId, table.getString(URL));
            if(mediaOptional.isPresent()) {
                return Config.getParameter("media.servicepartnerportal.https") + mediaOptional.get().getURL();
            }
        }
        throw new IllegalStateException("Could not download container label for " + notificationId);
    }

    /**
     *
     * @param notificationId
     * @throws BackendException
     */
    @Override
    public String createDatedItemsList(final String notificationId) throws BackendException {
        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Create Dated Items List : notificationNumber [{0}] ", notificationId));
        }

        final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function = managedConnection.getFunction(ZCREATE_DATEDITEMS_HYB);
        final String notificationNumber_padded = StringUtils.leftPad(notificationId, 12, "0");
        function.getImportParameterList().setValue(IM_QMNUM, notificationNumber_padded);
        managedConnection.execute(function);
        final JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        if (StringUtils.isNotBlank(table.getString(URL))) {
            final Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia(DIL + notificationId, table.getString(URL));
            if(mediaOptional.isPresent()) {
                return Config.getParameter("media.servicepartnerportal.https") + mediaOptional.get().getURL();
            }
        }
        return StringUtils.EMPTY;
    }

    @Override
    public boolean updateDatedItemsList(String notificationId) throws BackendException {
        final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
        final JCoFunction function = managedConnection.getFunction(ZCREATE_DATEDITEMS_HYB);
        final String notificationNumber_padded = StringUtils.leftPad(notificationId, 12, "0");
        function.getImportParameterList().setValue(IM_QMNUM, notificationNumber_padded);
        managedConnection.execute(function);
        final JCoTable table = function.getTableParameterList().getTable(ZT_NOTIF);
        return StringUtils.isNotBlank(table.getString(URL));
    }

    private String convertIfNotZeroDate(final String dateString) {
        if(dateString != null && !ZERO_DATE.equals(dateString)) {
            final LocalDate localDate = LocalDate.parse(dateString);
            return localDate.format(OUTBOUND_FORMATTER);
        } else if(ZERO_DATE.equals(dateString)) {
            return "00-00-0000";
        }
        return dateString;
    }

    /**
     *
     * @param serviceStation
     * @param equipmentId
     * @param managedConnection
     * @throws BackendException
     */
    private String getIpadNotification(String serviceStation, String equipmentId, JCoConnection managedConnection) throws BackendException {
        LOG.debug(MessageFormat.format("Has iPad notification : equipmentId [{0}] ", equipmentId));
        LOG.info("In the hasIpadNotification : " + new Timestamp(System.currentTimeMillis()));

        if (StringUtils.isBlank(equipmentId)) {
            LOG.info("equipmentId is empty. Cannot evaluate notification for iPad.");
            return StringUtils.EMPTY;
        }

        final JCoFunction function = managedConnection.getFunction(ZGET_IPAD_SERVSTAT);
        function.getImportParameterList().setValue(IM_SERV_STAT_NO, serviceStation);
        function.getImportParameterList().setValue(IM_EQUNR, equipmentId);
        LOG.info("Ready to call ZGET_IPAD_SERVSTAT : " + new Timestamp(System.currentTimeMillis()));


        try {
            managedConnection.execute(function);
        } catch (BackendException | BackendRuntimeException e) {
            if(e.getMessage().contains("COMBINATION_NOT_IPAD")) {
                LOG.info("VIK-2691: COMBINATION_NOT_IPAD found, treating as non-error");
                return StringUtils.EMPTY;
            }
            LOG.error("Ipad Notification exception thrown: ", e);
            throw e;
        }


        LOG.info("Just done ZGET_IPAD_SERVSTAT : " + new Timestamp(System.currentTimeMillis()));
        String iPadNotification = function.getExportParameterList().getString(EX_IPAD);
        LOG.info("ZGET_IPAD_SERVSTAT EX_IPAD : " + iPadNotification);
        return iPadNotification;
    }

    /**
     *
     * @param serviceStation
     * @param equipmentId
     * @param managedConnection
     * @throws BackendException
     */
    private boolean isAuthorisedForContainerLabel(String serviceStation, String equipmentId, JCoConnection managedConnection) throws BackendException {
        LOG.debug(MessageFormat.format("isAuthorisedForContainerLabel : equipmentId [{0}] - serviceStation [{1}]", equipmentId, serviceStation));
        LOG.info("In the isAuthorisedForContainerLabel : " + new Timestamp(System.currentTimeMillis()));

        if (StringUtils.isBlank(equipmentId)) {
            LOG.info("equipmentId is empty. Cannot evaluate if Authorised for Container Label.");
            return false;
        }

        final JCoFunction function = managedConnection.getFunction(ZGET_CONTLABEL_SERVSTAT);
        function.getImportParameterList().setValue(IM_SERV_STAT_NO, serviceStation);
        function.getImportParameterList().setValue(IM_EQUNR, equipmentId);
        LOG.info("Ready to call ZGET_CONTLABEL_SERVSTAT : " + new Timestamp(System.currentTimeMillis()));

        try {
            managedConnection.execute(function);
        } catch (BackendException | BackendRuntimeException e) {
            if(e.getMessage().contains("COMBINATION_NOT_CONTLABEL")) {
                LOG.info("VIK-2691: COMBINATION_NOT_CONTLABEL found, treating as non-error");
                return false;
            }
            LOG.error("Container Label exception thrown: ", e);
            throw e;
        }

        LOG.info("Just done ZGET_CONTLABEL_SERVSTAT : " + new Timestamp(System.currentTimeMillis()));
        String isAuthorised = function.getExportParameterList().getString(EX_CONTLABEL);
        LOG.info("ZGET_CONTLABEL_SERVSTAT EX_CONTLABEL : " + isAuthorised);
        return StringUtils.isNotBlank(isAuthorised);
    }





    @Autowired
    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    @Autowired
    public void setContentHelper(ContentHelper contentHelper) {
        this.contentHelper = contentHelper;
    }

    @Autowired
    public void setVikingNotificationPrefillService(VikingNotificationPrefillService vikingNotificationPrefillService) {
        this.vikingNotificationPrefillService = vikingNotificationPrefillService;
    }
}
