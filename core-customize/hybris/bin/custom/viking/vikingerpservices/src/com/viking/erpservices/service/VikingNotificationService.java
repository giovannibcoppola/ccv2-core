package com.viking.erpservices.service;

import com.viking.search.data.VesselNotificationDataList;

import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public interface VikingNotificationService {
    VesselNotificationDataList getNotifications(final String serialNumber, final String serviceStation) throws Exception;
    VesselNotificationDataList getNotifications(final String serialNumber, final String serviceStation, String equipmentId) throws Exception;
    String getCertificate(final String notificationId, final String serialNo) throws BackendException;
    String getDatedItemsList(final String notificationId, final String serialNo) throws BackendException;
    String createContainerLabel(final String notificationId, final String serviceStation, final String equipmentId) throws BackendException;
    String createDatedItemsList(final String notificationId) throws BackendException;
    boolean updateDatedItemsList(final String notificationId) throws BackendException;
}
