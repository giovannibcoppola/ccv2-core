/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.erpservices.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.erpservices.service.VikingFindVesselsService;
import com.viking.search.data.SearchVesselData;
import com.viking.search.data.SearchVesselDataList;
import de.hybris.platform.commercefacades.product.data.FutureStockData;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class DefaultVikingFindVesselsService implements VikingFindVesselsService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingFindVesselsService.class);
    public static final String ZGET_FUNCLOC_BYNAME = "ZGET_FUNCLOC_BYNAME";

    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Override
    public SearchVesselDataList findVesselByName(String input) {
        try {

            SearchVesselDataList searchVesselDataList = new SearchVesselDataList();

            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZGET_FUNCLOC_BYNAME);
            function.getImportParameterList().setValue("IM_NEW_NAME", input);
            managedConnection.execute(function);


            JCoTable table = function.getTableParameterList().getTable("ET_FUNCLOC_INF");

            final List<SearchVesselData> searchVesselData = new ArrayList<SearchVesselData>();


            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final SearchVesselData searchVesselDataEntry = new SearchVesselData();
                searchVesselDataEntry.setVesselName(table.getString("FUNCLOC_NAME"));
                searchVesselDataEntry.setVesselID(table.getString("FUNCLOC"));
                searchVesselDataEntry.setFlagState(table.getString("STATE"));
                searchVesselDataEntry.setCallSignal(table.getString("SIGN"));
                searchVesselDataEntry.setOwner(table.getString("OWNER"));
                searchVesselData.add(searchVesselDataEntry);
            }


            searchVesselDataList.setVesselDataList(searchVesselData);


            return searchVesselDataList;


        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_FUNCLOC_BYNAME, e);
        }
        return null;
    }

    @Override
    public SearchVesselDataList findVesselByFormerName(String input) {
        //IM_OLD_NAME,IM_FUNCLOC,IM_SERNR
        try {
            SearchVesselDataList searchVesselDataList = new SearchVesselDataList();

            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZGET_FUNCLOC_BYNAME);
            function.getImportParameterList().setValue("IM_OLD_NAME", input);
            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable("ET_FUNCLOC_INF");

            final List<SearchVesselData> searchVesselData = new ArrayList<SearchVesselData>();


            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final SearchVesselData searchVesselDataEntry = new SearchVesselData();
                searchVesselDataEntry.setVesselName(table.getString("FUNCLOC_NAME"));
                searchVesselDataEntry.setVesselID(table.getString("FUNCLOC"));
                searchVesselDataEntry.setFlagState(table.getString("STATE"));
                searchVesselDataEntry.setCallSignal(table.getString("SIGN"));
                searchVesselDataEntry.setOwner(table.getString("OWNER"));
                searchVesselData.add(searchVesselDataEntry);
            }


            searchVesselDataList.setVesselDataList(searchVesselData);


            return searchVesselDataList;


        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_FUNCLOC_BYNAME, e);
        }
        return null;
    }

    @Override
    public SearchVesselDataList findVesselByIMO(String input) {

        try {
            SearchVesselDataList searchVesselDataList = new SearchVesselDataList();

            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZGET_FUNCLOC_BYNAME);
            function.getImportParameterList().setValue("IM_FUNCLOC", input);
            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable("ET_FUNCLOC_INF");

            final List<SearchVesselData> searchVesselData = new ArrayList<SearchVesselData>();


            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final SearchVesselData searchVesselDataEntry = new SearchVesselData();
                searchVesselDataEntry.setVesselName(table.getString("FUNCLOC_NAME"));
                searchVesselDataEntry.setVesselID(table.getString("FUNCLOC"));
                searchVesselDataEntry.setFlagState(table.getString("STATE"));
                searchVesselDataEntry.setCallSignal(table.getString("SIGN"));
                searchVesselDataEntry.setOwner(table.getString("OWNER"));
                searchVesselData.add(searchVesselDataEntry);
            }


            searchVesselDataList.setVesselDataList(searchVesselData);


            return searchVesselDataList;


        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_FUNCLOC_BYNAME, e);
        }
        return null;
    }

    @Override
    public SearchVesselDataList findVesselBySerialNumber(String input) {

        try {
            SearchVesselDataList searchVesselDataList = new SearchVesselDataList();

            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZGET_FUNCLOC_BYNAME);
            function.getImportParameterList().setValue("IM_SERNR", input);
            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable("ET_FUNCLOC_INF");

            final List<SearchVesselData> searchVesselData = new ArrayList<SearchVesselData>();


            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final SearchVesselData searchVesselDataEntry = new SearchVesselData();
                searchVesselDataEntry.setVesselName(table.getString("FUNCLOC_NAME"));
                searchVesselDataEntry.setVesselID(table.getString("FUNCLOC"));
                searchVesselDataEntry.setFlagState(table.getString("STATE"));
                searchVesselDataEntry.setCallSignal(table.getString("SIGN"));
                searchVesselDataEntry.setOwner(table.getString("OWNER"));
                searchVesselData.add(searchVesselDataEntry);
            }


            searchVesselDataList.setVesselDataList(searchVesselData);


            return searchVesselDataList;


        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (findVesselByName) for module : " + ZGET_FUNCLOC_BYNAME, e);
        }
        return null;
    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }


}
