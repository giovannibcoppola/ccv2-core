package com.viking.erpservices.service.impl;

import com.viking.erpservices.service.VikingCreateVesselService;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.search.data.CreateVesselResponseData;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultVikingCreateVesselService implements VikingCreateVesselService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingCreateVesselService.class);
    public static final String ZCREATE_FUNCLOC = "ZCREATE_FUNCLOC";
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Override
    public CreateVesselResponseData createVessel(String description, String flagState, String signal) {

        try {
            LOG.info("Create vessel Service class with data  "+description+" "+flagState+" "+signal);

            CreateVesselResponseData createVesselResponseData = new CreateVesselResponseData();
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
            final JCoFunction function = managedConnection.getFunction(ZCREATE_FUNCLOC);
            function.getImportParameterList().setValue("IM_FUNCLOC_DESCR", description);
            function.getImportParameterList().setValue("IM_FLAGSTATE", flagState);
            function.getImportParameterList().setValue("IM_SIGNAL", signal);
            managedConnection.execute(function);

            String value = function.getExportParameterList().getString("EX_FUNCLOC");
            LOG.info("Created VESSEL "+value);
            JCoTable FunctionReturn = function.getTableParameterList().getTable("O_RETURN");
            LOG.info("RFC return message : "+FunctionReturn.getString("MESSAGE"));

            createVesselResponseData.setVesselId(value);
            createVesselResponseData.setMessage(FunctionReturn.getString("MESSAGE"));
            return createVesselResponseData;

        } catch (BackendException e) {
            LOG.error("Error during sap erp rfc call (CreateVessel) for module : " + ZCREATE_FUNCLOC, e);
        }
        return null;

    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }
}
