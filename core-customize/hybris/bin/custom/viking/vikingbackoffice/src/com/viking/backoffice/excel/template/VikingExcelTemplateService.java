package com.viking.backoffice.excel.template;

import com.google.common.base.Preconditions;
import com.hybris.backoffice.excel.template.DefaultExcelTemplateService;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.function.Function;

public class VikingExcelTemplateService extends DefaultExcelTemplateService {

    private static final Function<String, String> EMPTY_PATTERN = (arg) -> {
        return String.format("%s cannot be empty or null", arg);
    };
    private static final Function<String, String> NULL_PATTERN = (arg) -> {
        return String.format("%s cannot be null", arg);
    };

    @Override
    public Sheet createTypeSheet(String typeCode, Workbook workbook) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(typeCode), EMPTY_PATTERN.apply("typeCode"));
        Preconditions.checkNotNull(workbook, NULL_PATTERN.apply("Workbook"));
        ComposedTypeModel composedType = this.getTypeService().getComposedTypeForCode(typeCode);
        String typeName = StringUtils.substring(composedType.getCode(), 0, 30);
        Sheet sheet = workbook.getSheet(typeName);
        if (sheet == null) {
            this.addTypeSheet(typeName, workbook);
            this.populateTypeSystemSheet(composedType, workbook);
            this.hideUtilitySheet(workbook, "TypeSystem");
            return workbook.getSheet(typeName);
        } else {
            return sheet;
        }
    }
}
