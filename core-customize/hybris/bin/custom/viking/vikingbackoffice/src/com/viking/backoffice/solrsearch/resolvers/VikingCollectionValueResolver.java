package com.viking.backoffice.solrsearch.resolvers;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import com.hybris.backoffice.solrsearch.resolvers.CollectionValueResolver;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;

/**
 * VikingCollectionValueResolver extends CollectionValueResolver to resolve an issue when only 1 attributevalue is provided.
 */
public class VikingCollectionValueResolver extends CollectionValueResolver {
    @Override
    protected void addFieldValues(InputDocument document, IndexerBatchContext batchContext, IndexedProperty indexedProperty, ItemModel model, ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException {
        Object attributeValue = null;
        String fieldName = null;
        if (indexedProperty.isLocalized()) {
            if (resolverContext.getQualifier() != null) {
                LanguageModel lang = resolverContext.getQualifier().getValueForType(LanguageModel.class);
                Locale locale = resolverContext.getQualifier().getValueForType(Locale.class);
                if (lang != null && locale != null) {
                    attributeValue = super.getModelService().getAttributeValue(model, indexedProperty.getName(), locale);
                    fieldName = String.format("%s_%s_%s_%s", indexedProperty.getName(), lang.getIsocode(), indexedProperty.getType(), "mv");
                }
            }
        } else {
            attributeValue = super.getModelService().getAttributeValue(model, indexedProperty.getName());
            fieldName = String.format("%s_%s_%s", indexedProperty.getName(), indexedProperty.getType(), "mv");
        }

        handleAttributeValue(document, attributeValue, fieldName);
    }

    private void handleAttributeValue(InputDocument document, Object attributeValue, String fieldName) throws FieldValueProviderException {
        if (attributeValue instanceof Collection) {

            for (Object value : (Collection) attributeValue) {
                if (value instanceof ItemModel) {
                    document.addField(fieldName, ((ItemModel) value).getPk().getLong());
                } else {
                    if(value instanceof Collection){
                        document.addField(fieldName, value);
                    }else {
                        document.addField(fieldName, List.of(value));
                    }
                }
            }
        }
    }
}
