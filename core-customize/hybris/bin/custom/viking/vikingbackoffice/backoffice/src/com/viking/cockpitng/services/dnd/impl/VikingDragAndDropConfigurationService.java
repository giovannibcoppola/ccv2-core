package com.viking.cockpitng.services.dnd.impl;


import com.hybris.cockpitng.dnd.DragAndDropActionType;
import com.hybris.cockpitng.services.dnd.impl.DefaultDragAndDropConfigurationService;

/**
 * Viking Drag And Drop Configuration Service class
 *
 */
public class VikingDragAndDropConfigurationService extends DefaultDragAndDropConfigurationService {

    @Override
    public DragAndDropActionType getDefaultActionType() {
        Object attribute = this.getCockpitSessionService().getAttribute("default_backoffice_dnd_behaviour");
        if (attribute instanceof DragAndDropActionType) {
            return (DragAndDropActionType)attribute;
        } else {
            DragAndDropActionType defaultActionType = DragAndDropActionType.APPEND;
            this.setDefaultActionType(defaultActionType);
            return defaultActionType;
        }
    }
}
