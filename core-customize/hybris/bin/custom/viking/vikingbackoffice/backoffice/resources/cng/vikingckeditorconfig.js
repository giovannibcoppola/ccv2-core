CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

config.removeButtons = 'Save,NewPage,ExportPdf,Preview,Print,Templates,Font,FontSize,About,Maximize,ShowBlocks,BGColor,TextColor,Cut,Copy,Paste,PasteText,PasteFromWord,Find,Replace,Scayt,SelectAll,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,BidiLtr,BidiRtl,Language,Anchor,PageBreak,Iframe,Smiley,Image';
config.contentsCss = 'widgetClasspathResource/widgets/editors/wysiwygEditor/validationstyles.css';

// replace part of CKEditor base path to allow load skin from wysiwyg widget directory
// (CKEditor path contains protocol, domain, port and application root path)
config.skin = 'bootstrapck,' + CKEDITOR.basePath.replace('zkau/web/js/ckez/ext/CKeditor/', 'widgetClasspathResource/widgets/editors/wysiwygEditor/vendor/skins/bootstrapck-1.0.0/');
};

CKEDITOR.on('instanceReady', function( ev ) {
    ev.editor.dataProcessor.writer.setRules( 'p', {
        breakAfterClose : false
    });
});

