package com.viking.backoffice.solrsearch.resolvers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hybris.backoffice.solrsearch.resolvers.CollectionValueResolver;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingCollectionValueResolverTest extends CollectionValueResolver {
    @Mock
    InputDocument document;
    @Mock
    IndexerBatchContext batchContext;
    @Mock
    IndexedProperty indexedProperty;
    @Mock
    ItemModel model;
    @InjectMocks
    VikingCollectionValueResolver vikingCollectionValueResolver;
    @Mock
    private ModelService modelService;

    @Test
    public void addFieldValuesIsCorrect() throws FieldValueProviderException {
        final String theName = "theName";
        final String theType = "theType";
        final String attribute1 = "a";
        final String attribute2 = "b";

        when(indexedProperty.getName()).thenReturn(theName);
        when(indexedProperty.getType()).thenReturn(theType);
        when(modelService.getAttributeValue(model, theName)).thenReturn(new HashSet<>(Arrays.asList(attribute1, attribute2)));

        vikingCollectionValueResolver.setModelService(modelService);
        AbstractValueResolver.ValueResolverContext<Object, Object> theContext = null;
        vikingCollectionValueResolver.addFieldValues(document, batchContext, indexedProperty, model, theContext);

        verify(document, times(1)).addField(theName + "_" + theType + "_mv", List.of(attribute1));
        verify(document, times(1)).addField(theName + "_" + theType + "_mv", List.of(attribute2));
    }

    @Test
    public void addCollectionFieldValueIsCorrect() throws FieldValueProviderException {
        final String theName = "theName";
        final String theType = "theType";
        final List<String> attribute1 = List.of("a");

        when(indexedProperty.getName()).thenReturn(theName);
        when(indexedProperty.getType()).thenReturn(theType);
        when(modelService.getAttributeValue(model, theName)).thenReturn(new HashSet<>(Arrays.asList(attribute1)));

        vikingCollectionValueResolver.setModelService(modelService);
        AbstractValueResolver.ValueResolverContext<Object, Object> theContext = null;
        vikingCollectionValueResolver.addFieldValues(document, batchContext, indexedProperty, model, theContext);

        verify(document, times(1)).addField(theName + "_" + theType + "_mv", attribute1);
    }
}