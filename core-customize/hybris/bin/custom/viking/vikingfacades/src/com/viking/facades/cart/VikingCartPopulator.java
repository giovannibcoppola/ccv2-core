package com.viking.facades.cart;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

public class VikingCartPopulator extends CartPopulator {


    @Override
    public void populate(final CartModel source, final CartData target) {
        super.populate(source, target);
        target.setContainsDangerousGoods(false);
        source.getEntries().stream().forEach(e -> {

            if (e.getProduct().getVikingdangerlevel() != null && !e.getProduct().getVikingdangerlevel().isEmpty()) {
                target.setContainsDangerousGoods(true);
            }
        });
        target.setDeliveryNote(source.getDeliveryNote());
    }


}
