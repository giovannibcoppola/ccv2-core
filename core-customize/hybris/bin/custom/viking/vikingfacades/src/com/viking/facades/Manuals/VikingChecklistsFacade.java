package com.viking.facades.Manuals;

import com.viking.core.integration.azure.ChecklistsData;

import java.io.IOException;
import java.util.List;

public interface VikingChecklistsFacade {
    ChecklistsData getChecklist(String id);
}
