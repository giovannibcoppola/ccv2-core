package com.viking.facades.country;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;

public interface VikingCountryFacade {
    String getIsoCodeByName(String name);

    List<CountryData> getAllCountries();

    List<CountryData> getCountriesForCountryPicker();
}
