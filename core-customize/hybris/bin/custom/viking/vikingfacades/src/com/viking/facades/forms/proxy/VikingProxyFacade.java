package com.viking.facades.forms.proxy;

import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;

public interface VikingProxyFacade {

    String getInlineFormHtml(final String applicationId, final String formId, final YFormDataActionEnum action,
                             final String formDataId) throws YFormServiceException;
}
