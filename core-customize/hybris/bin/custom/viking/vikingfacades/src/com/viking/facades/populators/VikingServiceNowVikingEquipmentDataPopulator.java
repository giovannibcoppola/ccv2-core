/**
 *
 */
package com.viking.facades.populators;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.viking.core.integration.servicenow.data.VesselEquipmentData;
import com.viking.facades.integration.data.VikingEquipmentData;


/**
 * @author Prabhakar
 *
 */
public class VikingServiceNowVikingEquipmentDataPopulator implements Populator<VesselEquipmentData, VikingEquipmentData>
{
	private static final DateTimeFormatter OUTBOUND_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");

	@Override
	public void populate(final VesselEquipmentData source, final VikingEquipmentData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setActionRequired(source.getActionRequired());
		target.setBookedPort(source.getBookedPort());
		target.setCertificateStatus(source.getCertificateStatus());
		target.setEquipment(source.getEquipment());
		target.setEquipmentDescription(source.getEquipmentDescription());
		target.setEta(source.getEta());
		target.setEtd(source.getEtd());
		target.setLastCertificate(source.getLastCertificate());

		if(source.getNextInspection() != null) {
			final LocalDate localDate = LocalDate.parse(source.getNextInspection());
			target.setNextInspection(localDate.format(OUTBOUND_FORMATTER));
		}

		target.setQuantity(source.getQuantity());
		target.setServiceBooked(source.getServiceBooked());
		target.setCategory(source.getModelCategory());
		target.setLastNotificationNumber(source.getLastNotificationNumber());
	}

}
