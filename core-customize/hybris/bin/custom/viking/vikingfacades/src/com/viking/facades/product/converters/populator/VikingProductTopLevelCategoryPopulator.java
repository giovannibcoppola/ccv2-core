package com.viking.facades.product.converters.populator;

import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingProductTopLevelCategoryPopulator implements Populator<ProductModel, ProductData> {

    private static final Logger LOG = Logger.getLogger(VikingProductTopLevelCategoryPopulator.class);
    private static final String SDS_PRODUCT_CATALOG = "sdsProductCatalog";
    private static final String ONLINE = "Online";

    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        if(CollectionUtils.isNotEmpty(source.getSupercategories())) {
            Optional<CategoryModel> currentSDSCategory = findSDSOnlineSuperCategory(source);
            if(currentSDSCategory.isPresent()) {
                Optional<CategoryModel> rootSDSCategory = findRootSDSCategory(currentSDSCategory.get());

                if (rootSDSCategory.isPresent()) {
                    final CategoryModel categoryModel = rootSDSCategory.get();
                    target.setTopLevelCategory(categoryModel.getName());
                    target.setTopLevelCategoryCode(categoryModel.getCode());
                }
            }
        }
    }

    private Optional<CategoryModel> findSDSOnlineSuperCategory(final CategoryModel categoryModel) {
        if(CollectionUtils.isEmpty(categoryModel.getSupercategories())) {
            return Optional.empty();
        }
        return categoryModel.getSupercategories().stream()
                .filter(cat -> SDS_PRODUCT_CATALOG.equals(cat.getCatalogVersion().getCatalog().getId()) && ONLINE.equals(cat.getCatalogVersion().getVersion()))
                .findFirst();
    }

    private Optional<CategoryModel> findRootSDSCategory(final CategoryModel current) {

        if(current == null) {
            return Optional.empty();
        }

        int count = 0;

        LOG.info("Input: " + current.getName());

        CategoryModel currentCategory = current;

        Optional<CategoryModel> parent = findSDSOnlineSuperCategory(currentCategory);
        Optional<CategoryModel> grandParent = Optional.empty();
        Optional<CategoryModel> grandGrandParent = Optional.empty();
        while(parent.isPresent()) {
            LOG.info("Parent: " + parent.get().getName());
            grandParent = findSDSOnlineSuperCategory(parent.get());
            if(grandParent.isPresent()) {
                LOG.info("Grand Parent: " + grandParent.get().getName());
                grandGrandParent = findSDSOnlineSuperCategory(grandParent.get());
                if(grandGrandParent.isPresent()) {
                    LOG.info("Grand Grand Parent: " + grandGrandParent.get().getName());
                    parent = findSDSOnlineSuperCategory(parent.get());
                } else {
                    LOG.info("No grand grand parent");
                    return parent;
                }
            } else {
                LOG.info("No grand parent");
                return Optional.of(currentCategory);
            }
            count++;

            if(count > 100) {
                LOG.warn("Breaking");
                break;
            }
        }

        return Optional.empty();
    }

    private Optional<CategoryModel> findSDSOnlineSuperCategory(final ProductModel productModel) {
        if(CollectionUtils.isEmpty(productModel.getSupercategories())) {
            return Optional.empty();
        }
        return productModel.getSupercategories().stream()
                .filter(cat -> SDS_PRODUCT_CATALOG.equals(cat.getCatalogVersion().getCatalog().getId()) && ONLINE.equals(cat.getCatalogVersion().getVersion()))
                .findFirst();
    }
}
