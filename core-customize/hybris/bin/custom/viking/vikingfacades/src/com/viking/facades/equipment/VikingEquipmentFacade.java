package com.viking.facades.equipment;

import com.viking.core.data.EquipmentData;

import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public interface VikingEquipmentFacade {
    void addEquipment(String serialNumber, String vesselID) throws BackendException;

    void dismantleEquipment(String serialNumber);

    EquipmentData searchEquipment(String serialNumber);
}
