package com.viking.facades.order;

import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;


public class VikingOrderPopulator extends OrderPopulator {

    @Override
    public void populate(final OrderModel source, final OrderData target) {
        super.populate(source, target);
        target.setVikingErpOrderId(
                source.getVikingerporderid() ==null
                        || source.getVikingerporderid().isEmpty()
                        ? "(Not available yet)" : source.getVikingerporderid());
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
    }


}
