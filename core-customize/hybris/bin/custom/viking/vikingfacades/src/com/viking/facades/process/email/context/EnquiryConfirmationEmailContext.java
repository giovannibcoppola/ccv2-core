package com.viking.facades.process.email.context;

import com.viking.core.model.EnquiryConfirmationProcessModel;
import com.viking.core.model.EnquiryModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Velocity context for a enquiry confirmation email.
 */
public class EnquiryConfirmationEmailContext extends AbstractEmailContext<EnquiryConfirmationProcessModel>
{
	private static final String BCC_EMAIL = "bccEmail";
	private static final String ORDER_NUMBER = "orderNumber";

	private SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

	@Resource
	private EnumerationService enumerationService;

	private List<OrderEntryData> entries;
	private String firstName;
	private String lastName;
	private String addressLine1;
	private String addressLine2;
	private String enquiryTitle;
	private String enquiryEmail;
	private String phone;
	private String country;
	private String postalCode;
	private String comment;
	private String city;
	private String date;
	private String preferredContactMethod;
	private String customerReferenceNumber;
	private String company;

	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	@Override
	public void init(final EnquiryConfirmationProcessModel process, final EmailPageModel emailPageModel)
	{
		super.init(process, emailPageModel);

		final EnquiryModel enquiry = process.getEnquiry();
		final VikingEnquiryOrderModel vikingEnquiryOrderModel = process.getVikingEnquiryOrder();
		entries = orderEntryConverter.convertAll(vikingEnquiryOrderModel.getEntries());
		final AddressModel addressModel = vikingEnquiryOrderModel.getDeliveryAddress();
		if(addressModel != null) {
				company = addressModel.getCompany();
				firstName = addressModel.getFirstname();
				lastName = addressModel.getLastname();
				addressLine1 = addressModel.getLine1();
				addressLine2 = addressModel.getLine2();
				enquiryEmail = addressModel.getEmail();
				phone = addressModel.getPhone1();
				country = addressModel.getCountry().getName();
				postalCode = addressModel.getPostalcode();
				comment = getCommentFromOrder(vikingEnquiryOrderModel);
				city = addressModel.getTown();
				date = formatter.format(new Date());
				preferredContactMethod = getPreferredContactMethodValue(addressModel);
				customerReferenceNumber = addressModel.getCustomerReferenceNumber();

				put(DISPLAY_NAME, addressModel.getFirstname() + " " + addressModel.getLastname());
				put(EMAIL, addressModel.getEmail());
			}

		put(BCC_EMAIL, process.getBccEmail());
		put(ORDER_NUMBER, vikingEnquiryOrderModel.getCode());
	}

	private String getPreferredContactMethodValue(AddressModel addressModel) {
		return addressModel.getPreferredContactMethod() != null ? enumerationService.getEnumerationName(addressModel.getPreferredContactMethod()) : StringUtils.EMPTY;
	}

	private String getCommentFromOrder(VikingEnquiryOrderModel vikingEnquiryOrderModel) {
		if(CollectionUtils.isNotEmpty(vikingEnquiryOrderModel.getB2bcomments())) {
			return vikingEnquiryOrderModel.getB2bcomments().iterator().next().getComment();
		}
		return StringUtils.EMPTY;
	}

	@Override
	protected BaseSiteModel getSite(final EnquiryConfirmationProcessModel businessProcessModel) {
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final EnquiryConfirmationProcessModel businessProcessModel) {
		return null;
	}

	@Override
	protected LanguageModel getEmailLanguage(final EnquiryConfirmationProcessModel businessProcessModel) {
		return businessProcessModel.getLanguage();
	}

	public List<OrderEntryData> getEntries() {
		return entries;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getPhone() {
		return phone;
	}

	public String getCountry() {
		return country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getComment() {
		return comment;
	}

	public String getEnquiryTitle() {
		return enquiryTitle;
	}

	public String getEnquiryEmail() {
		return enquiryEmail;
	}

	public String getCity() {
		return city;
	}

	public String getDate() {
		return date;
	}

	public String getBccEmail() {
		return (String) get(BCC_EMAIL);
	}

	public String getPreferredContactMethod() {
		return preferredContactMethod;
	}

	public String getCustomerReferenceNumber() {
		return customerReferenceNumber;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getCompany() {
		return company;
	}


	@Required
	public void setOrderEntryConverter(Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter) {
		this.orderEntryConverter = orderEntryConverter;
	}
}
