package com.viking.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductVariantMatrixPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Chiranjit Chakraborty
 */
public class VikingProductVariantMatrixPopulator extends ProductVariantMatrixPopulator {

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {
        final Collection<VariantProductModel> variants = getVariants(productModel);
        productData.setMultidimensional(Boolean.valueOf(CollectionUtils.isNotEmpty(variants)));

        if (productData.getMultidimensional().booleanValue())
        {
            final VariantMatrixElementData nodeZero = createNode(null, null);
            final VariantProductModel starterVariant = getStarterVariant(productModel, variants);

            createNodesForVariant(starterVariant, nodeZero);

            for (final VariantProductModel variant : variants)
            {
                if (variant instanceof ERPVariantProductModel)
                {
                    //Don't process the first variant again
                    if (!variant.getCode().equals(productModel.getCode()))
                    {
                        createNodesForVariant(variant, nodeZero);
                    }
                }
            }

            orderTree(nodeZero.getElements());
            productData.setVariantMatrix(nodeZero.getElements());
        }
    }

    @Override
    protected Collection<VariantProductModel> getVariants(final ProductModel productModel)
    {
        Collection<VariantProductModel> variants = Collections.<VariantProductModel> emptyList();
        if (CollectionUtils.isNotEmpty(productModel.getVariants()))
        {
            variants = productModel.getVariants();
        }
        if (productModel instanceof ERPVariantProductModel)
        {
            variants = ((ERPVariantProductModel) productModel).getBaseProduct().getVariants();
        }

        return variants;
    }
}
