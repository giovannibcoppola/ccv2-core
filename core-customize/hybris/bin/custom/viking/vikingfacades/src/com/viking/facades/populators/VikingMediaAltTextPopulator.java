package com.viking.facades.populators;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;

/**
 * VikingMediaPopulator to populate MediaData
 */
public class VikingMediaAltTextPopulator implements Populator<MediaModel, MediaData> {

    @Override
    public void populate(MediaModel source, MediaData target) {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        final String altTextLocalized = source.getAltTextLocalized();
        target.setAltText(StringUtils.isEmpty(altTextLocalized) ? source.getRealFileName() : altTextLocalized);
    }
}
