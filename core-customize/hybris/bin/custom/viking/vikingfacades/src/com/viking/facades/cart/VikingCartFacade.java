package com.viking.facades.cart;

import de.hybris.platform.commercefacades.order.CartFacade;

public interface VikingCartFacade extends CartFacade {

    void updateSessionCart();

    /**
     * Remove point of service from all entries on current cart.
     */
    void removeDeliveryPointOfServiceFromCart();

    /**
     * Setting the provided point of service on current cart.
     * @param pointOfService
     */
    void setPointOfServiceOnCart(String pointOfService);

    /**
     * Copy the point of service from cart to all entries.
     */
    void copyPointOfServiceFromCartEntries();
}
