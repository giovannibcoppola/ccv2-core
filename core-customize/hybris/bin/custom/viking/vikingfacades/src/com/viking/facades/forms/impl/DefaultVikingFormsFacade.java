package com.viking.facades.forms.impl;

import com.viking.core.model.NotificationPrefillModel;
import com.viking.core.service.VikingNotificationPrefillService;
import com.viking.core.util.ContentHelper;
import com.viking.erpservices.service.VikingFormsPreFillDataService;
import com.viking.erpservices.service.VikingFormsSubmitService;
import com.viking.facades.forms.VikingFormsFacade;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static  org.apache.commons.lang3.StringUtils.isEmpty;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;


import java.util.Optional;

/**
 * {@inheritDoc}
 */
public class DefaultVikingFormsFacade implements VikingFormsFacade {

    private static final Logger LOG = Logger.getLogger(DefaultVikingFormsFacade.class);
    private VikingFormsSubmitService vikingFormsSubmitService;
    private ModelService modelService;
    private VikingNotificationPrefillService vikingNotificationPrefillService;
    private ContentHelper contentHelper;

    /**
     * {@inheritDoc}
     */
    @Override
    public void submitNotificationYFormToSAP(String notificationId, String serviceStation, final String xml) {
        vikingFormsSubmitService.submitYFormDataToSap(notificationId, serviceStation, xml);
        if (Collections.isEmpty(vikingNotificationPrefillService.getNotifications(notificationId))) {
            NotificationPrefillModel notificationPrefillModel = modelService.create(NotificationPrefillModel.class);
            notificationPrefillModel.setNotificationId(notificationId);
            modelService.save(notificationPrefillModel);
        }
    }

    @Override
    public String submitNotificationYFormToSAPandSavePDF(String notificationId, String serviceStation, String xml, String print) {
        final String url = vikingFormsSubmitService.submitYFormDataToSapAndGetPDFUrl(notificationId, serviceStation, xml, print);
        if (Collections.isEmpty(vikingNotificationPrefillService.getNotifications(notificationId))) {
            LOG.debug("Notification " + notificationId + " is saved for the first time, and a notificationPrefillModel is created");
            NotificationPrefillModel notificationPrefillModel = modelService.create(NotificationPrefillModel.class);
            notificationPrefillModel.setNotificationId(notificationId);
            modelService.save(notificationPrefillModel);
        }

        Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia("form_pdf_" + notificationId, url);
        if (mediaOptional.isPresent()) {
            return mediaOptional.get().getCode();
        }

        LOG.error("Empty optional received from contentHelper for URL '" + url + "'. No media was downloaded to Hybris");
        return StringUtils.EMPTY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void submitCondemnationToSAP(String xml) {
        vikingFormsSubmitService.submitCondemnationToSap(xml);
    }

    @Required
    public void setVikingNotificationPrefillService(VikingNotificationPrefillService vikingNotificationPrefillService) {
        this.vikingNotificationPrefillService = vikingNotificationPrefillService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setContentHelper(ContentHelper contentHelper) {
        this.contentHelper = contentHelper;
    }

    @Required
    public void setVikingFormsSubmitService(VikingFormsSubmitService vikingFormsSubmitService) {
        this.vikingFormsSubmitService = vikingFormsSubmitService;
    }
}
