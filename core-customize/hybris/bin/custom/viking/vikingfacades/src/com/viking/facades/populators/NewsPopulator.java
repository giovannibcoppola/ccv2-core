package com.viking.facades.populators;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import com.viking.core.model.FlagStateModel;
import com.viking.facades.flagstate.data.FlagStateData;
import com.viking.news.data.NewsData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class NewsPopulator implements Populator<CMSMediaParagraphComponentModel, NewsData> {

    @Override
    public void populate(final CMSMediaParagraphComponentModel model, final NewsData data) throws ConversionException {
        data.setHeadline(model.getHeaderText());
        data.setPk(model.getPk().toString());
    }
}
