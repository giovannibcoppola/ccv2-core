package com.viking.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductVariantOptionDataPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Chiranjit Chakraborty
 */
public class VikingProductVariantOptionDataPopulator extends ProductVariantOptionDataPopulator {

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {

        final Collection<VariantProductModel> variants = getVariants(productModel);
        for (final VariantProductModel variant : variants)
        {
            populateNodes(productData.getVariantMatrix(), variant, productModel);
        }
        if (!(productModel instanceof ERPVariantProductModel))
        {
            copyDataFromNodes(productData);
        }

    }

    protected Collection<VariantProductModel> getVariants(final ProductModel productModel)
    {
        Collection<VariantProductModel> variants = Collections.<VariantProductModel> emptyList();

        if (CollectionUtils.isNotEmpty(productModel.getVariants()))
        {
            variants = productModel.getVariants();
        }
        else if (productModel instanceof ERPVariantProductModel)
        {
            variants = ((ERPVariantProductModel) productModel).getBaseProduct().getVariants();
        }

        return variants;
    }


}
