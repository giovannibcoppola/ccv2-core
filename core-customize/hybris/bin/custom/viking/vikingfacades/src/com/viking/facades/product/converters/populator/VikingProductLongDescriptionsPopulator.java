package com.viking.facades.product.converters.populator;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.viking.core.model.LongChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import com.viking.facades.integration.data.ChannelTextData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class VikingProductLongDescriptionsPopulator implements Populator<ProductModel, ProductData> {
    private final Converter<LongChannelTextModel, ChannelTextData> vikingLongChannelTextConverter;
    private final ProductChannelDataService productChannelDataService;

    @Autowired
    public VikingProductLongDescriptionsPopulator(Converter<LongChannelTextModel, ChannelTextData> vikingLongChannelTextConverter, ProductChannelDataService productChannelDataService) {
        this.vikingLongChannelTextConverter = vikingLongChannelTextConverter;
        this.productChannelDataService = productChannelDataService;
    }

    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        final Collection<LongChannelTextModel> filteredLongDescriptionsForChannel = productChannelDataService.getFilteredLongDescriptionsForChannel(source.getLongDescriptions());
        if(CollectionUtils.isNotEmpty(filteredLongDescriptionsForChannel)) {
            target.setLongDescriptions(vikingLongChannelTextConverter.convertAll(filteredLongDescriptionsForChannel));
        }
    }
}
