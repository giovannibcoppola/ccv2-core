package com.viking.facades.checkout.impl;

import com.viking.facades.checkout.VikingCheckoutFacade;
import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Optional;

public class VikingCheckoutFacadeImpl extends DefaultAcceleratorCheckoutFacade implements VikingCheckoutFacade {

    @Override
    public void setBillingAddress(final AddressData billingAddressData) {
        final CartModel cartModel = getCart();
        if (cartModel != null && billingAddressData != null) {
            AddressModel addressModel = billingAddressData.getId() == null ? createBillingAddressModel(billingAddressData, cartModel)
                        : getDeliveryAddressModelForCode(billingAddressData.getId());

            cartModel.setPaymentAddress(addressModel);
            addressModel.setBillingAddress(Boolean.TRUE);
            getModelService().saveAll(addressModel, cartModel);
        }
    }

    protected AddressModel createBillingAddressModel(final AddressData addressData, final CartModel cartModel) {
        final AddressModel addressModel = getModelService().create(AddressModel.class);
        getAddressReversePopulator().populate(addressData, addressModel);
        //TODO: User setted for address of owner
        addressModel.setOwner(cartModel.getUser());

        return addressModel;
    }
}