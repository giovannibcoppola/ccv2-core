package com.viking.facades.Manuals.impl;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualData;
import com.viking.core.integration.azure.VesselDocsData;
import com.viking.core.service.VikingManualsService;
import com.viking.facades.Manuals.VikingManualsFacade;

import java.io.IOException;
import java.util.List;


public class DefaultVikingManualsFacade implements VikingManualsFacade {

    private VikingManualsService vikingManualsService;

    @Override
    public ManualData getManual(String id)  {
        return vikingManualsService.getManual(id);
    }

    @Override
    public List<BulletinsData> getBulletins(String id)  {
        return vikingManualsService.getBulletins(id);
    }

    @Override
    public void setManualBulletinsAccepted(String bulletinsId, String manualsId, String userId) throws IOException {
        vikingManualsService.setManualBulletinsAccepted(bulletinsId, manualsId, userId);
    }

    @Override
    public VesselDocsData getVesselDocuments(final String imo) {
        return vikingManualsService.getVesselDocuments(imo);
    }

    @Override
    public String download(final String url, final String legacy, final boolean extensionReq, final String filename) {
        return vikingManualsService.download(url, legacy, extensionReq, filename);
    }

    public void setVikingManualsService(VikingManualsService vikingManualsService) {
        this.vikingManualsService = vikingManualsService;
    }
}
