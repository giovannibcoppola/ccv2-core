package com.viking.facades.checkout;

import de.hybris.platform.b2bacceleratorfacades.api.cart.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;

public interface VikingB2BCheckoutFacade extends CheckoutFacade {
    void saveNewAddress(CartData cartData, AddressData addressData, Boolean saveInAddressBook);

    boolean isOnlyCertificateProductsInCart();
}
