package com.viking.facades.product.converters.populator;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Populator from {@link ProductModel} into a {@link ProductData} to populate videos
 *
 * @author javier.gomez
 */
public class VikingProductVideosPopulator implements Populator<ProductModel, ProductData> {

    private ProductChannelDataService productChannelDataService;
    private Converter<MediaModel, ImageData> imageConverter;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) {
        List<MediaModel> filteredVideosModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getVideos());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredVideosModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredVideosModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getVideos());
        }

        if (CollectionUtils.isNotEmpty(filteredVideosModelList)) {
            populateVideos(filteredVideosModelList, productData);
        }
    }

    private void populateVideos(final List<MediaModel> mediaModelList, final ProductData productData) {
        int galleryIndex = getMaxIndex(productData);
        List<MediaModel> videos = new ArrayList<>();
        videos.addAll(mediaModelList);

        final List<ImageData> imageList = new ArrayList<>();
        for (MediaModel video : videos) {
            final ImageData imageData = getImageConverter().convert(video);
            imageData.setFormat("thumbnail");
            imageData.setImageType(ImageDataType.GALLERY);
            imageData.setGalleryIndex(Integer.valueOf(++galleryIndex));
            imageData.setYoutubeVideoId(video.getYoutubeVideoId());
            imageList.add(imageData);
        }

        productData.setVideos(imageList);
    }

    private int getMaxIndex(ProductData productData) {
        int maxIndex = -1;
        if (CollectionUtils.isNotEmpty(productData.getImages())) {
            for (ImageData imageData : productData.getImages()) {
                if (imageData.getGalleryIndex() != null && imageData.getGalleryIndex().intValue() > maxIndex) {
                    maxIndex = imageData.getGalleryIndex().intValue();
                }
            }
        }

        return maxIndex;
    }

    public ProductChannelDataService getProductChannelDataService() {
        return productChannelDataService;
    }

    public void setProductChannelDataService(ProductChannelDataService productChannelDataService) {
        this.productChannelDataService = productChannelDataService;
    }

    public Converter<MediaModel, ImageData> getImageConverter() {
        return imageConverter;
    }

    public void setImageConverter(Converter<MediaModel, ImageData> imageConverter) {
        this.imageConverter = imageConverter;
    }
}
