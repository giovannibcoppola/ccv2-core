package com.viking.facades.populators;

import com.viking.core.enums.PreferredContactMethod;
import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;

public class VikingAddressReversePopulator extends AddressReversePopulator {

    @Resource
    private EnumerationService enumerationService;

    @Override
    public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException {
        super.populate(addressData, addressModel);

        addressModel.setEmail(addressData.getEmail());
        addressModel.setCustomerReferenceNumber(addressData.getCustomerReferenceNumber());
        if (StringUtils.isNotBlank(addressData.getPreferredContactMethod()))
        {
            PreferredContactMethod preferredContactMethod = enumerationService.getEnumerationValue(PreferredContactMethod.class, addressData.getPreferredContactMethod().toUpperCase());

            addressModel.setPreferredContactMethod(preferredContactMethod);
        } else {
            addressModel.setPreferredContactMethod(null);
        }
    }
}
