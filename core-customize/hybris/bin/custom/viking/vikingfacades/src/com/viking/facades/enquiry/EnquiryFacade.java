package com.viking.facades.enquiry;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

/**
 * Enquiry facade
 */
public interface EnquiryFacade {

    /**
     * Check if enquiry flow is enabled on current basestore
     *
     * @return
     */
    boolean isEnquiryFlow();

    /**
     * Save the enquiry
     *
     * @param address
     * @param comment
     */
    void saveEnquiry(AddressData address, String comment);
}
