/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.facades.productcarousel.impl;

import de.hybris.platform.acceleratorfacades.productcarousel.impl.DefaultProductCarouselFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Facade to fetch list of products for a given product carousel component.
 */
public class VikingProductCarouselFacade extends DefaultProductCarouselFacade {

    protected static final List<ProductOption> BASIC_PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC);


    /**
     * Fetches list of products for a given product carousel component when not in preview (i.e., no cmsTicketId in
     * present in the session).
     *
     * @param component The product carousel component model
     * @return List<ProductData> list of available products
     */
    protected List<ProductData> fetchProductsForNonPreviewMode(final ProductCarouselComponentModel component) {

        final List<ProductData> products = new ArrayList<>();

        for (final ProductModel productModel : component.getProducts()) {
            products.add(getProductFacade().getProductForCodeAndOptions(productModel.getCode(), BASIC_PRODUCT_OPTIONS));
        }

        for (final CategoryModel categoryModel : component.getCategories()) {
            for (final ProductModel productModel : categoryModel.getProducts()) {
                products.add(getProductFacade().getProductForCodeAndOptions(productModel.getCode(), BASIC_PRODUCT_OPTIONS));
            }
        }

        return products;

    }

    /**
     * Fetches list of products for a given product carousel component when in preview (i.e., cmsTicketId in present in
     * the session).
     *
     * @param component The product carousel component model
     * @return List<ProductData> list of available products
     */
    protected List<ProductData> fetchProductsForPreviewMode(final ProductCarouselComponentModel component) {

        return getSessionService().executeInLocalView(new SessionExecutionBody() {

            @Override
            public Object execute() {
                try {
                    getSearchRestrictionService().disableSearchRestrictions();

                    final List<ProductData> products = new ArrayList<>();

                    for (final ProductModel productModel : getProductCarouselRendererService().getDisplayableProducts(component)) {
                        products.add(getProductForOptions(productModel, BASIC_PRODUCT_OPTIONS));
                    }

                    for (final CategoryModel categoryModel : getProductCarouselRendererService().getListOfCategories(component)) {
                        for (final ProductModel productModel : getProductCarouselRendererService()
                                .getDisplayableProducts(categoryModel)) {
                            products.add(getProductForOptions(productModel, BASIC_PRODUCT_OPTIONS));
                        }
                    }

                    return products;


                } finally {
                    getSearchRestrictionService().enableSearchRestrictions();
                }
            }

        });

    }
}
