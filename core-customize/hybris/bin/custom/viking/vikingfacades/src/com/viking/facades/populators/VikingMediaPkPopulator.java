package com.viking.facades.populators;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingMediaPkPopulator implements Populator<MediaModel, MediaData> {

    @Override
    public void populate(MediaModel mediaModel, MediaData mediaData) throws ConversionException {
        mediaData.setPk(mediaModel.getPk().toString());
    }

}
