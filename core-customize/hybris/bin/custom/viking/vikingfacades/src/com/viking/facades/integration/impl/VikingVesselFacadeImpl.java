/**
 *
 */
package com.viking.facades.integration.impl;

import com.viking.core.integration.servicenow.data.VesselData;
import com.viking.core.integration.servicenow.data.VesselEquipmentData;
import com.viking.core.integration.servicenow.data.VesselEquipmentDataWrapper;
import com.viking.core.integration.servicenow.service.VikingVesselServiceNowService;
import com.viking.core.model.VikingVesselModel;
import com.viking.facades.integration.VikingVesselFacade;
import com.viking.facades.integration.data.VikingEquipmentDataWrapperFrontEnd;
import com.viking.facades.integration.data.VikingVesselData;
import com.viking.facades.integration.data.VikingEquipmentData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author prabhakar
 *
 */
public class VikingVesselFacadeImpl implements VikingVesselFacade
{
	private final Logger LOG = Logger.getLogger(VikingVesselFacadeImpl.class);
	private VikingVesselServiceNowService vikingVesselServiceNowService;
	private Converter serviceNowVesselDataConverter;
	private Converter serviceNowVikingEquipmentDataConverter;
	private Converter vikingVesselModelToDataConverter;

	@Override
	public List<VikingVesselData> getAllVesselDataList()
	{
		return null;

	}

	@Override
	public List<VikingVesselData> getAllVesselDataListByCustomerId(final String customerId)
	{

		try
		{
			final List<VesselData> vesselServiceNowDataList = vikingVesselServiceNowService.getAllVesselDataList(customerId);
			if (CollectionUtils.isNotEmpty(vesselServiceNowDataList))
			{
				List<VikingVesselData> vikingVesselDataList = serviceNowVesselDataConverter.convertAll(vesselServiceNowDataList);
				sort(vikingVesselDataList);
				return vikingVesselDataList;
			}
		}
		catch (final Exception e)
		{
			LOG.error("ServiceNow threw an error while fetching all vessels for customer: " + customerId, e);
		}
		return Collections.emptyList();
	}

	@Override
	public VesselEquipmentDataWrapper getVikingEquipmentDataList(final String imoId, final String customerId)
	{
		try {
			return vikingVesselServiceNowService.getVesselEquipmentData(imoId, customerId);
		} catch (IOException e) {
			LOG.error("Something went wrong while fetching the equipment data list ", e);
		}
		return new VesselEquipmentDataWrapper();
	}

	@Override
	public List<VikingVesselData> getVesselDataListByUserId(final String userId)
	{
		final List<VikingVesselModel> vessels = vikingVesselServiceNowService.getAssignedVesselById(userId);
		if (CollectionUtils.isNotEmpty(vessels))
		{
			List<VikingVesselData> vesselDatas = vikingVesselModelToDataConverter.convertAll(vessels);
			sort(vesselDatas);
			return vesselDatas;
		}
		return Collections.emptyList();
	}

	@Override
	public VikingEquipmentDataWrapperFrontEnd getVikingEquipmentDataListMap(final String imoId, final String customerId) {

		final VikingEquipmentDataWrapperFrontEnd wrapperFrontEnd = new VikingEquipmentDataWrapperFrontEnd();
		try
		{
			final VesselEquipmentDataWrapper wrapper = vikingVesselServiceNowService.getVesselEquipmentData(imoId, customerId);
			wrapperFrontEnd.setImo(wrapper.getImo());
			wrapperFrontEnd.setFlagState(wrapper.getFlagState());
			wrapperFrontEnd.setVesselName(wrapper.getVesselName());

			if (CollectionUtils.isNotEmpty(wrapper.getEquipmentDataList()))
			{

				Map<String, List<VesselEquipmentData>> mapOnCategory = wrapper.getEquipmentDataList().stream()
						.filter(data -> StringUtils.isNotEmpty(data.getModelCategory()))
						.collect(Collectors.groupingBy(data -> data.getModelCategory()));
				Map<String, List<VikingEquipmentData>> mapOfCategoryToVesselData = mapOnCategory.entrySet().stream()
						.collect(Collectors.toMap(Map.Entry::getKey,
								e -> serviceNowVikingEquipmentDataConverter.convertAll(e.getValue())));

				wrapperFrontEnd.setEquipmentMap(mapOfCategoryToVesselData);
				return wrapperFrontEnd;
			}
		}
		catch (final Exception e)
		{
			LOG.info("Some net work issue occured while connectiong with service now" + e.getMessage());
		}
		return wrapperFrontEnd;
	}

	private void sort (List<VikingVesselData> datas) {
		Collections.sort(datas, Comparator.comparing(VikingVesselData::getName));
	}


	/**
	 * @param vikingVesselServiceNowService
	 *           the vikingVesselServiceNowService to set
	 */
	public void setVikingVesselServiceNowService(final VikingVesselServiceNowService vikingVesselServiceNowService)
	{
		this.vikingVesselServiceNowService = vikingVesselServiceNowService;
	}

	/**
	 * @param serviceNowVesselDataConverter
	 *           the serviceNowVesselDataConverter to set
	 */
	public void setServiceNowVesselDataConverter(final Converter serviceNowVesselDataConverter)
	{
		this.serviceNowVesselDataConverter = serviceNowVesselDataConverter;
	}

	/**
	 * @param serviceNowVikingEquipmentDataConverter
	 *           the serviceNowVikingEquipmentDataConverter to set
	 */
	public void setServiceNowVikingEquipmentDataConverter(final Converter serviceNowVikingEquipmentDataConverter)
	{
		this.serviceNowVikingEquipmentDataConverter = serviceNowVikingEquipmentDataConverter;
	}

	/**
	 * @param vikingVesselModelToDataConverter
	 *           the vikingVesselModelToDataConverter to set
	 */
	public void setVikingVesselModelToDataConverter(final Converter vikingVesselModelToDataConverter)
	{
		this.vikingVesselModelToDataConverter = vikingVesselModelToDataConverter;
	}

}
