package com.viking.facades.product.converters.populator;

import com.viking.core.enums.ApprovalAuthority;
import com.viking.core.model.USPModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Chiranjit Chakraborty
 */
public class VikingProductApprovalPopulator implements Populator<ProductModel, ProductData>
{
    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {
        Set<ApprovalAuthority> authorities = new HashSet<ApprovalAuthority>(productModel.getApprovalAuthorities());
        if(CollectionUtils.isEmpty(authorities) && productModel instanceof ERPVariantProductModel) {
            authorities.addAll(((ERPVariantProductModel) productModel).getBaseProduct().getApprovalAuthorities());
        }
        if(CollectionUtils.isNotEmpty(authorities)) {
            List<String> authoriesCodes = authorities.stream()
                    .map(authority -> authority.getCode())
                    .collect(Collectors.toList());
           productData.setApprovalAuthorities(authoriesCodes);
        }
    }
}