package com.viking.facades.process.email.context;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualsData;
import com.viking.core.model.BulletinReminderProcessModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.text.MessageFormat;
import java.util.*;


/**
 * Velocity context for a bulletin reminder email.
 */
public class BulletinReminderEmailContext extends AbstractEmailContext<BulletinReminderProcessModel> {
    private TreeMap<String, List<BulletinsData>> manualsToEmail = new TreeMap<>();
    private Map<String, String> manualIdMap = new HashMap<>();
    private String urlPrefix;

    @Override
    public void init(final BulletinReminderProcessModel process, final EmailPageModel emailPageModel) {
        super.init(process, emailPageModel);

        // Populate a treemap to get the correct sorting in email.
        final Map<ManualsData, List<BulletinsData>> manualsToEmailFromProcess = process.getManualsToEmail();
        final Set<ManualsData> keys = manualsToEmailFromProcess.keySet();
        keys.forEach(key -> {
            manualsToEmail.put(key.getName(), manualsToEmailFromProcess.get(key));
            manualIdMap.put(key.getName(), key.getId());
        });

        urlPrefix = getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true, "/service-manuals/");

        final B2BCustomerModel customer = getCustomer(process);

        if (customer.getApprovedFacility() == null) {
            throw new IllegalStateException(MessageFormat.format("Customer with ID {0} has no approved facility, so it is not possible to send reminder email", customer));
        }

        put(EMAIL, customer.getApprovedFacility().getEmail());
    }

    @Override
    protected BaseSiteModel getSite(final BulletinReminderProcessModel process) {
        return process.getSite();
    }

    @Override
    protected B2BCustomerModel getCustomer(final BulletinReminderProcessModel process) {
        return (B2BCustomerModel) process.getCustomer();
    }

    @Override
    protected LanguageModel getEmailLanguage(final BulletinReminderProcessModel process) {
        return process.getLanguage();
    }

    public TreeMap<String, List<BulletinsData>> getManualsToEmail() {
        return manualsToEmail;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public Map<String, String> getManualIdMap() {
        return manualIdMap;
    }
}
