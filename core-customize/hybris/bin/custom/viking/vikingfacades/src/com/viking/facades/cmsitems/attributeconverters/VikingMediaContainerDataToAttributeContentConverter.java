/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.facades.cmsitems.attributeconverters;

import de.hybris.platform.acceleratorfacades.cmsitems.attributeconverters.MediaContainerDataToAttributeContentConverter;
import de.hybris.platform.cms2.servicelayer.daos.CMSMediaFormatDao;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;


/**
 * Converts a Map<String, String> representation of a {@link MediaContainerModel} into an actual
 * {@link MediaContainerModel}
 */
public class VikingMediaContainerDataToAttributeContentConverter extends MediaContainerDataToAttributeContentConverter {

    private CMSMediaFormatDao cmsMediaFormatDao;

    @Override
    public MediaContainerModel convert(final Map<String, String> source) {
        if (isNull(source)) {
            return null;
        }

        final Map<String, MediaFormatModel> mediaFormats = cmsMediaFormatDao.getAllMediaFormats()
                .stream()
                .collect(toMap(MediaFormatModel::getQualifier, identity()));

        return getMediaContainerFunction(mediaFormats).apply(source);
    }

    @Required
    public void setCmsMediaFormatDao(CMSMediaFormatDao cmsMediaFormatDao) {
        this.cmsMediaFormatDao = cmsMediaFormatDao;
    }
}
