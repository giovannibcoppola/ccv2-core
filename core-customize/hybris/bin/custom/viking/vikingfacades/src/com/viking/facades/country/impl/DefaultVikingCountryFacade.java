package com.viking.facades.country.impl;

import com.viking.facades.country.VikingCountryFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.collections.CollectionUtils;
import org.drools.core.util.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DefaultVikingCountryFacade implements VikingCountryFacade {
    private FlexibleSearchService flexibleSearchService;
    private Converter<CountryModel, CountryData> countryConverter;
    private CommonI18NService commonI18NService;
    private CMSSiteService cmsSiteService;

    @Override
    public String getIsoCodeByName(String name) {

        SearchResult<CountryModel> searchResult = flexibleSearchService.search("SELECT {PK} FROM {Country} WHERE {name} = '" + name + "'");

        List<CountryModel> result = searchResult.getResult();

        if (CollectionUtils.isNotEmpty(result)) {
            return result.get(0).getIsocode();
        }

        return StringUtils.EMPTY;
    }

    @Override
    public List<CountryData> getAllCountries() {
        final Collection<CountryModel> allCountries = commonI18NService.getAllCountries();
        return countryConverter.convertAll(allCountries);
    }

    @Override
    public List<CountryData> getCountriesForCountryPicker() {
        final List<BaseStoreModel> stores = cmsSiteService.getCurrentSite().getStores();

        final List<CountryModel> countriesForCountrySelector = stores.stream()
                .map(BaseStoreModel::getDeliveryCountries)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        return countryConverter.convertAll(countriesForCountrySelector);
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setCountryConverter(Converter<CountryModel, CountryData> countryConverter) {
        this.countryConverter = countryConverter;
    }

    @Required
    public void setCmsSiteService(CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }
}
