/**
 *
 */
package com.viking.facades.vessels;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.List;

import com.viking.facades.vessels.data.VikingVesselPageData;







/**
 * @author Srikanth
 *
 */
public interface VikingAssignVesselsFacade
{
	void saveVesselsToUser(final VesselData vesselData);
	void removeVesselFromUser(final String vesselId, final String userId);
	List<CustomerData> getAllUsers();
	int vesselAndUserAssignments();
	List<VikingVesselPageData> getVessels();
}
