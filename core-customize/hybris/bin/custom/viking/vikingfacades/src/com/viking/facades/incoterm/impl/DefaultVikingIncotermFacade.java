package com.viking.facades.incoterm.impl;

import com.viking.core.dao.VikingIncotermDao;
import com.viking.core.model.VikingIncotermModel;
import com.viking.core.service.VikingIncotermService;
import com.viking.facades.incoterm.VikingIncotermData;
import com.viking.facades.incoterm.VikingIncotermFacade;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

public class DefaultVikingIncotermFacade implements VikingIncotermFacade {

    private VikingIncotermService vikingIncotermService;

    private Converter vikingIncotermConverter;

    @Override
    public List<VikingIncotermData> findVikingIncotermForCode(String code){
        return getVikingIncotermConverter().convertAll(getVikingIncotermService().findVikingIncotermForCode(code));
    }

    @Override
    public List<VikingIncotermData> findAllVikingIncoterms(){
        return getVikingIncotermConverter().convertAll(getVikingIncotermService().findAllVikingIncoterms());
    }

    public VikingIncotermService getVikingIncotermService() {
        return vikingIncotermService;
    }

    public void setVikingIncotermService(VikingIncotermService vikingIncotermService) {
        this.vikingIncotermService = vikingIncotermService;
    }

    public Converter getVikingIncotermConverter() {
        return vikingIncotermConverter;
    }

    public void setVikingIncotermConverter(Converter vikingIncotermConverter) {
        this.vikingIncotermConverter = vikingIncotermConverter;
    }
}
