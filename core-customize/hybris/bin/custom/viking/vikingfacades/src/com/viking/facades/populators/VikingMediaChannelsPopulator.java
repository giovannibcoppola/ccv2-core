package com.viking.facades.populators;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.viking.core.enums.Channel;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingMediaChannelsPopulator implements Populator<MediaModel, MediaData> {
    @Override
    public void populate(MediaModel source, MediaData target) throws ConversionException {
        if(CollectionUtils.isNotEmpty(source.getChannels())) {
            target.setChannels(source.getChannels().stream().map(Channel::getCode).collect(Collectors.toSet()));
        }
    }
}
