/**
 *
 */
package com.viking.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.viking.core.integration.servicenow.data.VesselData;
import com.viking.facades.integration.data.VikingVesselData;


/**
 * @author Prabhakar
 *
 */
public class VikingServiceNowVesselDataPopulator implements Populator<VesselData, VikingVesselData>
{
	private static final String BOOK_SERVICE_URL = "servicenow.client.book.service.url";

	private ConfigurationService configurationService;

	@Override
	public void populate(final VesselData source, final VikingVesselData target) throws ConversionException
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setName(source.getName());
		target.setBookServiceUrl(configurationService.getConfiguration().getString(BOOK_SERVICE_URL) + source.getTicketId());
		target.setActionRequired(source.getActionRequired());
		target.setCertificateStatus(source.getCertificateStatus());
		target.setImo(source.getImo());
		//target.setNextPortCall(source.getU_next_port_call());
		//target.setOwner(source.getU_owner());
		//target.setFlagState(source.getU_flag_state());
		//target.setCallSign(source.getU_call_sign());
		//target.setClassedBy(source.getU_classed_by());
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
