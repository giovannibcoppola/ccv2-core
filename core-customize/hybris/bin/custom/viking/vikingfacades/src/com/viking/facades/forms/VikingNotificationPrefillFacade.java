package com.viking.facades.forms;

import com.viking.core.jalo.NotificationPrefill;

import java.util.List;

public interface VikingNotificationPrefillFacade {
    public List<NotificationPrefill> getNotifications(String notificationId);
}
