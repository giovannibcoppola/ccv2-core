package com.viking.facades.product.converters.populator;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.viking.core.enums.Channel;
import com.viking.core.model.LongChannelTextModel;
import com.viking.facades.integration.data.ChannelTextData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingLongChannelTextPopulator implements Populator<LongChannelTextModel, ChannelTextData> {
    @Override
    public void populate(LongChannelTextModel source, ChannelTextData target) throws ConversionException {
        if(source.getLongText() != null) {
            target.setText(source.getLongText());
        }
        if(CollectionUtils.isNotEmpty(source.getChannels())) {
            target.setChannels(source.getChannels().stream().map(Channel::getCode).collect(Collectors.toSet()));
        }
    }
}
