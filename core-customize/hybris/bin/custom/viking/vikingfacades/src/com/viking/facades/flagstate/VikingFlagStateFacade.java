package com.viking.facades.flagstate;

import com.viking.facades.flagstate.data.FlagStateData;

import java.util.List;

/**
 * Flagstate facade
 */
public interface VikingFlagStateFacade {

    /**
     * Get all Flagstates
     * @return
     */
    List<FlagStateData> getAll();
}
