/**
 *
 */
package com.viking.facades.converter.populator;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.viking.core.model.VikingVesselModel;
import com.viking.facades.vessels.data.VikingVesselPageData;


/**
 * @author Srikanth
 *
 */
public class VikingVesselPagePopulator implements Populator<VikingVesselModel, VikingVesselPageData>
{
	@Resource(name = "customerConverter")
	private Converter<UserModel, CustomerData> customerConverter;

	private Converter<B2BCustomerModel, CustomerData> b2BCustomerConverter;

	/*-@Resource(name = "customerConverter")
	private Converter<VikingVesselModel, CustomerData> customerConverter;
	*/
	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final VikingVesselModel source, final VikingVesselPageData target) throws ConversionException
	{
		// YTODO Auto-generated method stub
		target.setVesselId(source.getVesselId());
		target.setName(source.getName());
		final Set<UserModel> users = source.getUsers();
		if (users != null)
		{
			final List<CustomerData> convertAll = Converters.convertAll(users, customerConverter);
			target.setCustomerData(convertAll);
		}
	}


}
