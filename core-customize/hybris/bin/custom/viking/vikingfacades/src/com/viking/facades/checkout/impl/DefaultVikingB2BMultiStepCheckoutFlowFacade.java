package com.viking.facades.checkout.impl;

import com.viking.facades.checkout.VikingB2BCheckoutFacade;
import com.viking.facades.checkout.VikingB2BMultiStepCheckoutFlowFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.B2BMultiStepCheckoutFlowFacade;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BAcceleratorCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Collections;
import java.util.List;

public class DefaultVikingB2BMultiStepCheckoutFlowFacade extends B2BMultiStepCheckoutFlowFacade implements VikingB2BMultiStepCheckoutFlowFacade {

    @Override
    protected AddressData getDeliveryAddress() {
        final CartModel cart = getCart();
        if (cart != null) {
            final AddressModel deliveryAddress = cart.getDeliveryAddress();
            if(deliveryAddress != null ) {
                return getAddressConverter().convert(deliveryAddress);
            }

        }
        return null;
    }
}
