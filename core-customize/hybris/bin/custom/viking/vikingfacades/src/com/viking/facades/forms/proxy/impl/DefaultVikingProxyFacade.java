package com.viking.facades.forms.proxy.impl;

import com.viking.core.forms.proxy.VikingProxyService;
import com.viking.erpservices.service.impl.DefaultVikingFormsPreFillDataService;
import com.viking.facades.forms.proxy.VikingProxyFacade;
import com.viking.search.data.CheckCreditData;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.xyformsfacades.proxy.impl.DefaultProxyFacade;
import de.hybris.platform.xyformsservices.enums.YFormDataActionEnum;
import de.hybris.platform.xyformsservices.exception.YFormServiceException;
import de.hybris.platform.xyformsservices.proxy.ProxyException;
import de.hybris.platform.xyformsservices.utils.YHttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class DefaultVikingProxyFacade extends DefaultProxyFacade implements VikingProxyFacade {

    private static final String CONDEMNATION_FORMID = "Condemnation";

    @Resource (name = "defaultVikingProxyService")
    VikingProxyService vikingProxyService;

    @Autowired
    private DefaultVikingFormsPreFillDataService vikingFormsPreFillDataService;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;
    private CheckCreditData formId;


    @Override
    public String getInlineFormHtml(final String applicationId, final String formId, final YFormDataActionEnum action,
                                    final String formDataId) throws YFormServiceException
    {
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        final StringWriter out = new StringWriter();
        final OutputStream os = new WriterOutputStream(out, "UTF-8");

        try
        {
            final boolean editable = !YFormDataActionEnum.VIEW.equals(action);

            String serviceSation = customerFacade.getCurrentCustomer().getUid();

            Map<String,String[]> requestParams = new HashMap<>(request.getParameterMap());

            if (!CONDEMNATION_FORMID.equalsIgnoreCase(formId)) {
                CheckCreditData checkCreditData = vikingFormsPreFillDataService.checkCredit(serviceSation, request.getParameter("formId"));
                Boolean creditAmountPositive = Double.valueOf(checkCreditData.getCredit()) > 0;
                Boolean negativeCreditAllowed = checkCreditData.getNegativeAllowed().equalsIgnoreCase("Y");

                Boolean checkCreditPass = creditAmountPositive || (!creditAmountPositive && negativeCreditAllowed);

                requestParams.put("creditCheckPass", new String[]{checkCreditPass.toString()});
            }

            final String url = vikingProxyService.rewriteURL(applicationId, formId, formDataId, editable, requestParams);

            final String namespace = getProxyService().getNextRandomNamespace();
            final Map<String, String> extraHeaders = getProxyService().getExtraHeaders();

            // The proxy needs a Response object... we provide a mocked one.
            final HttpServletResponse response = new YHttpServletResponse(os);

            getProxyService().proxy(request, response, namespace, url, true, extraHeaders);
            os.flush();
            return out.toString();
        }
        catch (final ProxyException | IOException e)
        {
            throw new YFormServiceException(e);
        }
        finally
        {
            IOUtils.closeQuietly(os);
        }
    }
}
