
package com.viking.facades.solr.impl;

import com.google.common.collect.Sets;
import com.viking.core.dao.VikingCategoryDao;
import com.viking.core.enquiry.service.EnquiryService;
import com.viking.facades.solr.VikingSolrProductSearchService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.*;
import de.hybris.platform.commerceservices.search.solrfacetsearch.impl.DefaultSolrProductSearchService;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class DefaultVikingSolrProductSearchService<ITEM> extends DefaultSolrProductSearchService<ITEM> implements
        VikingSolrProductSearchService {


    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "defaultVikingCategoryDao")
    VikingCategoryDao vikingCategoryDao;

    @Resource(name = "catalogVersionService")
    private CatalogVersionService catalogVersionService;

    private CMSSiteService cmsSiteService;

    private ConfigurationService configurationService;

    @Autowired
    private EnquiryService enquiryService;

    @Override
    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> categorySearch(final String categoryCode,
                                                                                                  final PageableData pageableData) {
        PageableData pageable = pageableData;
        if(pageableData==null) {
            pageable = new PageableData();
        }
        
        pageable.setPageSize(getConfigurationService().getConfiguration().getInt("storefront.search.pageSize.Desktop"));
        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setCategoryCode(categoryCode);
        searchQueryData.setFilterTerms(Collections.emptyList());
        final CMSSiteModel currentBaseSite = cmsSiteService.getCurrentSite();

        populateSearchFilter(searchQueryData, currentBaseSite);

        return doSearch(searchQueryData, pageable);
    }

    @Override
    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> categorySearch(final String categoryCode,
                                                                                                  final SearchQueryContext searchQueryContext, final PageableData pageableData)
    {
        PageableData pageable = pageableData;
        if(pageableData==null) {
            pageable = new PageableData();
        }
        pageable.setPageSize(getConfigurationService().getConfiguration().getInt("storefront.search.pageSize.Desktop"));

        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setCategoryCode(categoryCode);
        searchQueryData.setFilterTerms(Collections.emptyList());

        final CMSSiteModel currentBaseSite = cmsSiteService.getCurrentSite();

        populateSearchFilter(searchQueryData, currentBaseSite);

        searchQueryData.setSearchQueryContext(searchQueryContext);

        return doSearch(searchQueryData, pageable);
    }

    @Override
    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> textSearch(final String text,
                                                                                              final PageableData pageableData)
    {
        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setFreeTextSearch(text);
        searchQueryData.setFilterTerms(Collections.<SolrSearchQueryTermData> emptyList());

        final CMSSiteModel currentBaseSite = cmsSiteService.getCurrentSite();

        populateSearchFilter(searchQueryData, currentBaseSite);

        return doSearch(searchQueryData, pageableData);
    }

    @Override
    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> textSearch(final String text,
                                                                                              final SearchQueryContext searchQueryContext, final PageableData pageableData)
    {
        final SolrSearchQueryData searchQueryData = createSearchQueryData();
        searchQueryData.setFreeTextSearch(text);
        searchQueryData.setFilterTerms(Collections.<SolrSearchQueryTermData> emptyList());

        final CMSSiteModel currentBaseSite = cmsSiteService.getCurrentSite();

        populateSearchFilter(searchQueryData, currentBaseSite);

        searchQueryData.setSearchQueryContext(searchQueryContext);

        return doSearch(searchQueryData, pageableData);
    }

    @Override
    public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> searchAgain(
            final SolrSearchQueryData searchQueryData, final PageableData pageableData)
    {
        final CMSSiteModel currentBaseSite = cmsSiteService.getCurrentSite();

        populateSearchFilter(searchQueryData, currentBaseSite);

        return doSearch(searchQueryData, pageableData);
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    private void populateSearchFilter(SolrSearchQueryData searchQueryData, CMSSiteModel currentBaseSite) {
        if (!currentBaseSite.isDisableUserGroupsAuthOnSearch()) {
            SolrSearchFilterQueryData solrSearchFilterQueryData = getSolrSearchQueryTermDataForCategories();
            searchQueryData.setFilterQueries(Collections.singletonList(solrSearchFilterQueryData));
        }
    }

    private SolrSearchFilterQueryData getSolrSearchQueryTermDataForCategories()
    {
        Set<String> authorizedCategories = getAuthorizedCategories();

        SolrSearchFilterQueryData solrSearchFilterQueryData = new SolrSearchFilterQueryData();
        solrSearchFilterQueryData.setKey("allCategories");
        solrSearchFilterQueryData.setOperator(FilterQueryOperator.OR);
        if (CollectionUtils.isNotEmpty(authorizedCategories)) {
            solrSearchFilterQueryData.setValues(authorizedCategories);
        } else {
            solrSearchFilterQueryData.setValues(Sets.newHashSet("noAuthorizedCategoriesAvailable"));
        }

        return solrSearchFilterQueryData;
    }

    private Set<String> getAuthorizedCategories(){

        final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("partnerProductCatalog", "Online");
        if(enquiryService.isEnquiryFlow()) {
            final Collection<CategoryModel> categoryList = vikingCategoryDao.findCategories(catalogVersion);
            return categoryList.stream().map(CategoryModel::getCode).collect(Collectors.toSet());
        }

        final Set<PrincipalGroupModel> userAuthorizations = userService.getCurrentUser().getAllGroups()
                .stream()
                .filter(pgm -> pgm.getUid().contains("AUTH_"))
                .collect(Collectors.toSet());

        final Collection<CategoryModel> categoriesAllowedForPrincipalGroupModels = vikingCategoryDao.findCategoriesAllowedForPrincipalGroupModels(catalogVersion, userAuthorizations);
        return categoriesAllowedForPrincipalGroupModels.stream().map(CategoryModel::getCode).collect(Collectors.toSet());
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setCmsSiteService(
        CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }
}
