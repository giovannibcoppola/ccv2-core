package com.viking.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Populator from {@link ProductModel} into a {@link ProductData} to add the metadescription
 *
 * @author javier.gomez
 */
public class VikingProductMetadescriptionPopulator implements Populator<ProductModel, ProductData> {

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {
        productData.setMetadescription(productModel.getMetadescription());
    }
}
