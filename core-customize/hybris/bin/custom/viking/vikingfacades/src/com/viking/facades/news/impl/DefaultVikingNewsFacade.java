package com.viking.facades.news.impl;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import com.viking.core.service.VikingNewsService;
import com.viking.facades.news.VikingNewsFacade;
import com.viking.news.data.NewsData;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingNewsFacade implements VikingNewsFacade {

    private BaseSiteService baseSiteService;
    private VikingNewsService vikingNewsService;
    private Converter<CMSMediaParagraphComponentModel, NewsData> vikingNewsConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NewsData> getLatestNewsForCurrentSite() {
        final BaseSiteModel site = baseSiteService.getCurrentBaseSite();
        final List<CMSMediaParagraphComponentModel> latestsNews = vikingNewsService.getLatestsNewsForSite(site);

        return vikingNewsConverter.convertAll(latestsNews);
    }

    @Required
    public void setVikingNewsConverter(Converter<CMSMediaParagraphComponentModel, NewsData> vikingNewsConverter) {
        this.vikingNewsConverter = vikingNewsConverter;
    }

    @Required
    public void setVikingNewsService(VikingNewsService vikingNewsService) {
        this.vikingNewsService = vikingNewsService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
