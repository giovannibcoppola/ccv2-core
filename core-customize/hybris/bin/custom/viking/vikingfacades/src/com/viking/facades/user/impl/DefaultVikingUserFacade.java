package com.viking.facades.user.impl;

import com.viking.core.model.LoginHistoryModel;
import com.viking.core.model.VikingVesselModel;
import com.viking.core.service.VikingUserService;
import com.viking.facades.user.VikingUserFacade;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelCreationException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


public class DefaultVikingUserFacade extends DefaultUserFacade implements VikingUserFacade {

    private static final Logger LOG = LogManager.getLogger(DefaultVikingUserFacade.class);

    @Resource(name = "userService")
    VikingUserService vikingUserService;

    @Override
    public void updateCustomerAuthorizationGroupsOnLogin() {
        vikingUserService.updateCustomerAuthorizationGroupsFromSAP();
    }

    @Override
    public void removeUser(String uid) throws IllegalArgumentException {
        final UserModel currentUser = vikingUserService.getCurrentUser();

        if (currentUser.getUid().equals(uid)) {
            throw new IllegalArgumentException("Not possible delete current user.");
        }

        final UserModel user = vikingUserService.getUserForUID(uid);
        final Set<VikingVesselModel> vessels = user.getVikingVessel();

        getModelService().removeAll(vessels);
        getModelService().remove(user);
    }

    @Override
    public void setLastLogin() {
        final UserModel currentUser = vikingUserService.getCurrentUser();

        if (currentUser != null && !vikingUserService.isAnonymousUser(currentUser)) {
            try {
                final Date today = new Date();
                currentUser.setLastLogin(today);

                final LoginHistoryModel loginHistory = getModelService().create(LoginHistoryModel.class);
                loginHistory.setDate(today);
                loginHistory.setUser(currentUser);
                getModelService().save(loginHistory);
            } catch (Exception ex) {
                LOG.error("Not able to create new login history entry for user: " + currentUser, ex);
            }
        }
    }
}
