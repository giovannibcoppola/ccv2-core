package com.viking.facades.product.converters.populator;

import com.viking.core.model.SimpleChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Populator from {@link ProductModel} into a {@link ProductData} to populate Image files
 *
 * @author Chiranjit Chakraborty
 */
public class VikingProductImageFilesPopulator extends ProductGalleryImagesPopulator<ProductModel, ProductData> {

    @Resource
    private ProductChannelDataService productChannelDataService;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) {
        List<MediaContainerModel> filteredMediaContainerModelList = getProductChannelDataService().getFilteredMediaContainersForChannel(productModel.getGalleryImages());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredMediaContainerModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredMediaContainerModelList = getProductChannelDataService().getFilteredMediaContainersForChannel(variantProductModel.getBaseProduct().getGalleryImages());
        }

        if (CollectionUtils.isNotEmpty(filteredMediaContainerModelList)) {
            populateImageFiles(filteredMediaContainerModelList, productModel, productData);
        }
    }

    private void populateImageFiles(List<MediaContainerModel> mediaContainers, ProductModel productModel, ProductData productData) {
        final List<ImageData> imageList = new ArrayList<>();

        // fill our image list with the product's existing images
        if (productData.getImages() != null) {
            imageList.addAll(productData.getImages());
        }

        // Use all the images as gallery images
        int galleryIndex = 0;
        for (final MediaContainerModel mediaContainer : mediaContainers) {
            addImagesInFormats(mediaContainer, ImageDataType.GALLERY, galleryIndex++, imageList);
        }
        String teaser = getTeaser(productModel);
        for (final ImageData imageData : imageList) {
            if (imageData.getAltText() == null) {
                imageData.setAltText(teaser);
            }
        }
        // Overwrite the existing list of images
        productData.setImages(imageList);
    }

    private String getTeaser(ProductModel productModel) {
        Optional<String> teaserOptional = getProductChannelDataService().getTeaserForChannel(productModel);
        if (teaserOptional.isPresent()) {
            return teaserOptional.get();
        } else if (productModel instanceof ERPVariantProductModel) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            teaserOptional = getProductChannelDataService().getTeaserForChannel(variantProductModel.getBaseProduct());
            if (teaserOptional.isPresent()) {
                return teaserOptional.get();
            }
        }

        return "";
    }

    public ProductChannelDataService getProductChannelDataService() {
        return productChannelDataService;
    }

    public void setProductChannelDataService(ProductChannelDataService productChannelDataService) {
        this.productChannelDataService = productChannelDataService;
    }

    @Override
    protected void addImagesInFormats(MediaContainerModel mediaContainer, ImageDataType imageType, int galleryIndex, List<ImageData> list)
    {
        for (final String imageFormat : getImageFormats())
        {
            try
            {
                final String mediaFormatQualifier = getImageFormatMapping().getMediaFormatQualifierForImageFormat(imageFormat);
                if (mediaFormatQualifier != null)
                {
                    final MediaFormatModel mediaFormat = getMediaService().getFormat(mediaFormatQualifier);
                    if (mediaFormat != null)
                    {
                        final MediaModel media = getMediaContainerService().getMediaForFormat(mediaContainer, mediaFormat);
                        if (media != null)
                        {
                            final ImageData imageData = getImageConverter().convert(media);
                            imageData.setFormat(imageFormat);
                            imageData.setImageType(imageType);
                            if (ImageDataType.GALLERY.equals(imageType))
                            {
                                imageData.setGalleryIndex(Integer.valueOf(galleryIndex));
                            }
                            list.add(imageData);
                        }
                    }
                }
            }
            catch (final ModelNotFoundException ignore)
            {
                // Ignore
            }
        }
    }
}
