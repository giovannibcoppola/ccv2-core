package com.viking.facades.populators;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingMediaLanguagesPopulator implements Populator<MediaModel, MediaData> {
    @Override
    public void populate(MediaModel source, MediaData target) throws ConversionException {
        if(CollectionUtils.isNotEmpty(source.getLanguages())) {
            target.setLanguages(source.getLanguages().stream().map(LanguageModel::getIsocode).collect(Collectors.toSet()));
        }
    }
}
