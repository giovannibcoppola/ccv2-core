package com.viking.facades.flagstate.impl;

import com.viking.core.model.FlagStateModel;
import com.viking.core.service.VikingFlagStateService;
import com.viking.facades.flagstate.VikingFlagStateFacade;
import com.viking.facades.flagstate.data.FlagStateData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingFlagStateFacade implements VikingFlagStateFacade {

    private VikingFlagStateService vikingFlagStateService;
    private Converter<FlagStateModel, FlagStateData> vikingFlagStateConverter;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FlagStateData> getAll() {
        final List<FlagStateModel> flagStates = vikingFlagStateService.getAll();
        return vikingFlagStateConverter.convertAll(flagStates);
    }

    @Required
    public void setVikingFlagStateConverter(final Converter<FlagStateModel, FlagStateData> vikingFlagStateConverter) {
        this.vikingFlagStateConverter = vikingFlagStateConverter;
    }

    @Required
    public void setVikingFlagStateService(final VikingFlagStateService vikingFlagStateService) {
        this.vikingFlagStateService = vikingFlagStateService;
    }
}
