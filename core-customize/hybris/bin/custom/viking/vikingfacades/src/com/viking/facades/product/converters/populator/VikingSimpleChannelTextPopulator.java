package com.viking.facades.product.converters.populator;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.viking.core.enums.Channel;
import com.viking.core.model.SimpleChannelTextModel;
import com.viking.facades.integration.data.ChannelTextData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingSimpleChannelTextPopulator implements Populator<SimpleChannelTextModel, ChannelTextData> {
    @Override
    public void populate(SimpleChannelTextModel source, ChannelTextData target) throws ConversionException {
        if(source.getText() != null) {
            target.setText(source.getText());
        }
        if(CollectionUtils.isNotEmpty(source.getChannels())) {
            target.setChannels(source.getChannels().stream().map(Channel::getCode).collect(Collectors.toSet()));
        }
    }
}
