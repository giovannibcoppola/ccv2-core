package com.viking.facades.product.converters.populator;


import java.util.stream.Collectors;

import com.viking.core.enums.Application;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingProductApplicationsPopulator implements Populator<ProductModel, ProductData> {
    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        if(source.getApplications() != null) {
            target.setApplications(source.getApplications().stream().map(Application::getCode).collect(Collectors.toList()));
        }
    }
}
