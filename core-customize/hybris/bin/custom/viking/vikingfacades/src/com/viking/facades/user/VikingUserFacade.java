package com.viking.facades.user;

import de.hybris.platform.commercefacades.user.UserFacade;


public interface VikingUserFacade extends UserFacade {
    void updateCustomerAuthorizationGroupsOnLogin();

    void removeUser(String uid) throws IllegalArgumentException;

    void setLastLogin();
}
