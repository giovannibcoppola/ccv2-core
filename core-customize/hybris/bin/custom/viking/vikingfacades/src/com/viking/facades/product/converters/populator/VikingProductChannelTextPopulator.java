package com.viking.facades.product.converters.populator;

import com.viking.core.model.LongChannelTextModel;
import com.viking.core.model.SimpleChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;

import java.util.Optional;

/**
 * Populator from {@link ProductModel} into a {@link ProductData} to populate Channel Text data
 *
 * @author javier.gomez
 */
public class VikingProductChannelTextPopulator implements Populator<ProductModel, ProductData> {

    private ProductChannelDataService productChannelDataService;

    @Override
    public void populate(ProductModel productModel, ProductData productData) {
        populateTeaser(productModel, productData);
        populateLongDescription(productModel, productData);
    }

    private void populateTeaser(ProductModel productModel, ProductData productData) {
        Optional<String> teaserOptional = getProductChannelDataService().getTeaserForChannel(productModel);
        if (teaserOptional.isPresent()) {
            productData.setTeaser(teaserOptional.get());
        } else if (productModel instanceof ERPVariantProductModel) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            teaserOptional = getProductChannelDataService().getTeaserForChannel(variantProductModel.getBaseProduct());
            if (teaserOptional.isPresent()) {
                productData.setTeaser(teaserOptional.get());
            }
        }
    }

    private void populateLongDescription(ProductModel productModel, ProductData productData) {
        Optional<String> longDescriptionOptional = getProductChannelDataService().getLongDescriptionForChannel(productModel);
        if (longDescriptionOptional.isPresent()) {
            productData.setLongDescription(longDescriptionOptional.get());
        } else if (productModel instanceof ERPVariantProductModel) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            longDescriptionOptional = getProductChannelDataService().getLongDescriptionForChannel(variantProductModel.getBaseProduct());
            if (longDescriptionOptional.isPresent()) {
                productData.setLongDescription(longDescriptionOptional.get());
            }
        }
    }

    public ProductChannelDataService getProductChannelDataService() {
        return productChannelDataService;
    }

    public void setProductChannelDataService(ProductChannelDataService productChannelDataService) {
        this.productChannelDataService = productChannelDataService;
    }
}
