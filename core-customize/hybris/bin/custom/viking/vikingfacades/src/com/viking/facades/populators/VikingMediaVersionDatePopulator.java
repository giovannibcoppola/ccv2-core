package com.viking.facades.populators;

import java.text.SimpleDateFormat;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingMediaVersionDatePopulator implements Populator<MediaModel, MediaData> {
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public void populate(MediaModel source, MediaData target) throws ConversionException {
        if(source.getVersionDate() != null) {
            target.setVersionDate(sdf.format(source.getVersionDate()));
        }
    }
}
