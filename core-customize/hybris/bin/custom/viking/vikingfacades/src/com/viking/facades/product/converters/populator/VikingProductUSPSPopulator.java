package com.viking.facades.product.converters.populator;

import com.viking.core.model.USPModel;
import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Chiranjit Chakraborty
 */
public class VikingProductUSPSPopulator implements Populator<ProductModel, ProductData>
{
    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {
        List<USPModel> usps = new ArrayList<USPModel>(productModel.getUsps());
        if(CollectionUtils.isEmpty(usps) && productModel instanceof ERPVariantProductModel) {
            usps.addAll(((ERPVariantProductModel) productModel).getBaseProduct().getUsps());
        }
        if(CollectionUtils.isNotEmpty(usps)) {
            List<String> uspsValues = usps.stream()
                    .map(usp -> usp.getUspText())
                    .collect(Collectors.toList());
            productData.setUsps(uspsValues);
        }
    }
}