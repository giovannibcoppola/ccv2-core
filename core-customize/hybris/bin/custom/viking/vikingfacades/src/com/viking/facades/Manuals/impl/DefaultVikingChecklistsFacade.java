package com.viking.facades.Manuals.impl;

import com.viking.core.integration.azure.ChecklistsData;
import com.viking.core.service.VikingChecklistsService;
import com.viking.facades.Manuals.VikingChecklistsFacade;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.util.List;

public class DefaultVikingChecklistsFacade implements VikingChecklistsFacade {
    private VikingChecklistsService vikingChecklistsService;

    @Override
    public ChecklistsData getChecklist(String id) {
        return vikingChecklistsService.getChecklist(id);
    }

    @Required
    public void setVikingChecklistsService(VikingChecklistsService vikingChecklistsService) {
        this.vikingChecklistsService = vikingChecklistsService;
    }
}
