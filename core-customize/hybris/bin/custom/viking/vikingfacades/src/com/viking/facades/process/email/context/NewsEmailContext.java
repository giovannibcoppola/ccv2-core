package com.viking.facades.process.email.context;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import com.viking.core.model.NewsEmailProcessModel;
import com.viking.core.service.VikingNewsService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;


/**
 * Velocity context for a bulletin reminder email.
 */
public class NewsEmailContext extends AbstractEmailContext<NewsEmailProcessModel> {
    private static final Logger LOG = LogManager.getLogger(NewsEmailContext.class);
    private static final String CATALOG = "servicepartnerportalContentCatalog";
    private static final String CATALOG_VERSION_STAGED = "Staged";
    private static final String CATALOG_VERSION_ONLINE = "Online";

    private String urlToNews;
    private String newsHeadline;

    private VikingNewsService vikingNewsService;
    private CatalogVersionService catalogVersionService;

    @Override
    public void init(final NewsEmailProcessModel process, final EmailPageModel emailPageModel) {
        super.init(process, emailPageModel);

        CMSMediaParagraphComponentModel newsToSend = process.getNewsToSend();

        if (newsToSend.getCatalogVersion().equals(catalogVersionService.getCatalogVersion(CATALOG, CATALOG_VERSION_STAGED))) {
            final CMSMediaParagraphComponentModel onlineNews = vikingNewsService.findNewsForCatalogVersion(newsToSend.getUid(), catalogVersionService.getCatalogVersion(CATALOG, CATALOG_VERSION_ONLINE));

            if (onlineNews != null) {
                LOG.debug("Found news in online catalog. Switching.");
                newsToSend = onlineNews;
            }
        }

        final String urlPrefix = getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true, "/news?newsId=");

        newsHeadline = newsToSend.getHeaderText();
        urlToNews = urlPrefix + newsToSend.getPk().toString();

        LOG.debug("URL to news: " + urlToNews);
    }

    @Override
    protected BaseSiteModel getSite(final NewsEmailProcessModel process) {
        return process.getSite();
    }

    @Override
    protected B2BCustomerModel getCustomer(final NewsEmailProcessModel process) {
        return (B2BCustomerModel) process.getCustomer();
    }

    @Override
    protected LanguageModel getEmailLanguage(final NewsEmailProcessModel process) {
        return process.getLanguage();
    }

    public String getUrlToNews() {
        return urlToNews;
    }

    public String getNewsHeadline() {
        return newsHeadline;
    }

    @Required
    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    @Required
    public void setVikingNewsService(VikingNewsService vikingNewsService) {
        this.vikingNewsService = vikingNewsService;
    }
}
