package com.viking.facades.equipment.impl;

import com.viking.core.data.EquipmentData;
import com.viking.erpservices.service.impl.DefaultVikingEquipmentVesselService;
import com.viking.facades.equipment.VikingEquipmentFacade;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public class DefaultVikingEquipmentFacade implements VikingEquipmentFacade {
    private DefaultVikingEquipmentVesselService equipmentVesselService;
    @Override
    public void addEquipment(String serialNumber, String vesselID) throws BackendException {
        equipmentVesselService.addEquipment(serialNumber, vesselID);
    }

    @Override
    public void dismantleEquipment(String serialNumber) {
        equipmentVesselService.dismantleEquipment(serialNumber);
    }

    @Override
    public EquipmentData searchEquipment(final String serialNumber) {
        return equipmentVesselService.searchEquipment(serialNumber);
    }

    @Required
    public void setEquipmentVesselService(DefaultVikingEquipmentVesselService equipmentVesselService) {
        this.equipmentVesselService = equipmentVesselService;
    }
}
