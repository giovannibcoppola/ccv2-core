package com.viking.facades.price;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DefaultVikingPriceDataFactory extends DefaultPriceDataFactory {

    @Override
    public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
    {
        final BigDecimal twoDecimalValue = value.setScale(2, BigDecimal.ROUND_HALF_UP);
        return super.create(priceType, twoDecimalValue, currency);
    }

    @Override
    protected DecimalFormat adjustSymbol(final DecimalFormat format, final CurrencyModel currencyModel) {
        final String symbol = currencyModel.getSymbol();
        if (symbol != null) {
            final DecimalFormatSymbols symbols = format.getDecimalFormatSymbols(); // does cloning
            final String iso = currencyModel.getIsocode();
            boolean changed = false;
            if (!iso.equalsIgnoreCase(symbols.getInternationalCurrencySymbol())) {
                symbols.setInternationalCurrencySymbol(iso + " ");
                changed = true;
            }
            if (!symbol.equals(symbols.getCurrencySymbol())) {
                symbols.setCurrencySymbol(symbol + " ");
                changed = true;
            }
            if (changed) {
                format.setDecimalFormatSymbols(symbols);
            }
        }
        return format;
    }
}
