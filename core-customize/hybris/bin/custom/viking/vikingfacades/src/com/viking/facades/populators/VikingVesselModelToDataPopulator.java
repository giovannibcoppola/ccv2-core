/**
 *
 */
package com.viking.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.viking.core.model.VikingVesselModel;
import com.viking.facades.integration.data.VikingVesselData;


/**
 * @author Prabhakar
 *
 */
public class VikingVesselModelToDataPopulator implements Populator<VikingVesselModel, VikingVesselData>
{

	@Override
	public void populate(final VikingVesselModel source, final VikingVesselData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setImo(source.getImo());
		target.setVesselId(source.getVesselId());
		target.setName(source.getName());
	}

}
