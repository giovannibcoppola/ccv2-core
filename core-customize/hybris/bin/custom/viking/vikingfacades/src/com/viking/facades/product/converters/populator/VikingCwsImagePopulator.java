package com.viking.facades.product.converters.populator;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.viking.core.enums.Channel;

import de.hybris.platform.commercefacades.product.converters.populator.ImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingCwsImagePopulator extends ImagePopulator {

    @Override
    public void populate(final MediaModel source, final ImageData target) throws ConversionException {
        super.populate(source, target);

        if (StringUtils.isNotEmpty(source.getYoutubeVideoId())) {
            target.setYoutubeVideoId(source.getYoutubeVideoId());
        }
        if(CollectionUtils.isNotEmpty(source.getChannels())) {
            target.setChannels(source.getChannels().stream().map(Channel::getCode).collect(Collectors.toSet()));
        }
        if(source.getOrder() != null) {
            target.setOrder(String.valueOf(source.getOrder()));
        }
    }
}
