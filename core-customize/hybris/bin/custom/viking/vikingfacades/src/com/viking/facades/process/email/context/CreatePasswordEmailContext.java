package com.viking.facades.process.email.context;

import com.viking.core.model.CreatePasswordProcessModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * Velocity context for a forgotten password email.
 */
public class CreatePasswordEmailContext extends CustomerEmailContext {
    private int expiresInMinutes = 2440;
    private String token;
    private String defaultB2BUnitId;

    public int getExpiresInMinutes() {
        return expiresInMinutes;
    }

    public void setExpiresInMinutes(final int expiresInMinutes) {
        this.expiresInMinutes = expiresInMinutes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public void setDefaultB2BUnitId(String defaultB2BUnitId) {
        this.defaultB2BUnitId = defaultB2BUnitId;
    }

    public String getDefaultB2BUnitId() {
        return defaultB2BUnitId;
    }

    public String getURLEncodedToken() throws UnsupportedEncodingException {
        return URLEncoder.encode(token, "UTF-8");
    }

    public String getSecureResetPasswordUrl() throws UnsupportedEncodingException {
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true, "/login/pw/change",
                "token=" + getURLEncodedToken());
    }

    public String getDisplaySecureResetPasswordUrl() throws UnsupportedEncodingException {
        return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true, "/login/pw/create");
    }

    @Override
    public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel) {
        super.init(storeFrontCustomerProcessModel, emailPageModel);
        if (storeFrontCustomerProcessModel instanceof CreatePasswordProcessModel) {
            setToken(((CreatePasswordProcessModel) storeFrontCustomerProcessModel).getToken());
            setDefaultB2BUnitId(((CreatePasswordProcessModel) storeFrontCustomerProcessModel).getDefaultB2BUnitId());
        }
    }
}
