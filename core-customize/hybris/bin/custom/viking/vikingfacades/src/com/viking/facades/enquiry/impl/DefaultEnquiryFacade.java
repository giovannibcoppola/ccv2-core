package com.viking.facades.enquiry.impl;

import com.viking.core.event.EnquirySubmitEvent;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.enquiry.service.EnquiryService;
import com.viking.facades.enquiry.EnquiryFacade;
import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.services.B2BCommentService;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.ordercloning.CloneAbstractOrderStrategy;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;

/**
 * {@inheritDoc}
 */
public class DefaultEnquiryFacade implements EnquiryFacade {

    private static final Logger LOG = Logger.getLogger(DefaultEnquiryFacade.class);

    @Resource
    private EnquiryService enquiryService;
    @Resource
    private CartService cartService;
    @Resource
    private ModelService modelService;

    @Resource(name = "defaultVikingB2BCheckoutFacade")
    private CheckoutFacade checkoutFacade;

    @Resource(name="b2bCommentService")
    private B2BCommentService<AbstractOrderModel> b2bCommentService;

    @Resource
    private UserService userService;


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEnquiryFlow() {
        return enquiryService.isEnquiryFlow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveEnquiry(final AddressData addressData, final String comment) {

        checkoutFacade.setDeliveryAddress(addressData);
        if(StringUtils.isNotEmpty(comment)) {
            setComment(comment);
        }

        try {
            checkoutFacade.placeOrder();
        } catch (InvalidCartException e) {
            LOG.error(e);
        }
    }

    private void setComment(String comment) {
        final B2BCommentModel b2bComment = modelService.create(B2BCommentModel.class);
        b2bComment.setComment(comment);
        b2bComment.setOwner(userService.getCurrentUser());
        b2bCommentService.addComment(cartService.getSessionCart(), b2bComment);
    }

}
