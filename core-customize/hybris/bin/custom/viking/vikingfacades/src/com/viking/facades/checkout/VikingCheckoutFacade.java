package com.viking.facades.checkout;

import de.hybris.platform.commercefacades.user.data.AddressData;

public interface VikingCheckoutFacade {
    void setBillingAddress(AddressData billingAddressData);
}
