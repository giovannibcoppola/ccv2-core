package com.viking.facades.populators;

import com.viking.core.service.VikingCommercePriceService;
import com.viking.core.service.VikingPriceInformation;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;

public class VikingProductPricePopulator extends ProductPricePopulator<ProductModel, ProductData> {

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
    {
        final PriceDataType priceType;
        final PriceInformation info;
        if (CollectionUtils.isEmpty(productModel.getVariants()))
        {
            priceType = PriceDataType.BUY;
            info = getCommercePriceService().getWebPriceForProduct(productModel);
        }
        else
        {
            priceType = PriceDataType.FROM;
            info = getCommercePriceService().getFromPriceForProduct(productModel);
        }

        if (info != null)
        {
            final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
                    info.getPriceValue().getCurrencyIso());
            if (info instanceof VikingPriceInformation) {
                priceData.setUom(((VikingPriceInformation) info).getUom());
            }
            productData.setPrice(priceData);
        }
        else
        {
            productData.setPurchasable(Boolean.FALSE);
        }
    }
}
