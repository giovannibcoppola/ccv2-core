package com.viking.facades.populators;

import com.viking.core.model.ApprovedFacilityModel;
import com.viking.facades.approvedfacility.data.ApprovedFacilityData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class ApprovedFacilityPopulator implements Populator<ApprovedFacilityModel, ApprovedFacilityData> {
    @Override
    public void populate(ApprovedFacilityModel approvedFacilityModel, ApprovedFacilityData approvedFacilityData) throws ConversionException {
        if (approvedFacilityModel.getCustomer() != null) {
            approvedFacilityData.setUserId(approvedFacilityModel.getCustomer().getUid());
        }
        approvedFacilityData.setName(approvedFacilityModel.getName());
        approvedFacilityData.setAddress1(approvedFacilityModel.getAddress1());
        approvedFacilityData.setAddress2(approvedFacilityModel.getAddress2());
        approvedFacilityData.setAddress3(approvedFacilityModel.getAddress3());
        approvedFacilityData.setPostalCode(approvedFacilityModel.getPostalCode());
        approvedFacilityData.setCity(approvedFacilityModel.getCity());
        approvedFacilityData.setCountry(approvedFacilityModel.getCountry());
        approvedFacilityData.setPhoneCountryCode(approvedFacilityModel.getPhoneCountryCode());
        approvedFacilityData.setPhone(approvedFacilityModel.getPhone());
        approvedFacilityData.setFaxCountryCode(approvedFacilityModel.getFaxCountryCode());
        approvedFacilityData.setFax(approvedFacilityModel.getFax());
        approvedFacilityData.setPrimaryEmail(approvedFacilityModel.getEmail());
        approvedFacilityData.setAdditionalEmail2(approvedFacilityModel.getAdditionalEmail2());
        approvedFacilityData.setAdditionalEmail3(approvedFacilityModel.getAdditionalEmail3());
        approvedFacilityData.setAdditionalEmail4(approvedFacilityModel.getAdditionalEmail4());
        approvedFacilityData.setAdditionalEmail5(approvedFacilityModel.getAdditionalEmail5());
        approvedFacilityData.setAdditionalEmail6(approvedFacilityModel.getAdditionalEmail6());
        approvedFacilityData.setWebsite(approvedFacilityModel.getWebsite());
        approvedFacilityData.setServiceStationManager(approvedFacilityModel.getServiceStationManager());
        approvedFacilityData.setServiceStationPhone(approvedFacilityModel.getServiceStationPhone());
        approvedFacilityData.setServiceStationEmail(approvedFacilityModel.getServiceStationEmail());
        approvedFacilityData.setAfterHoursPhone(approvedFacilityModel.getAfterHoursPhone());
    }
}
