package com.viking.facades.product.converters.populator;

import java.util.stream.Collectors;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;

public class VikingProductStoreListPopulator implements Populator<ProductModel, ProductData> {
    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        if(source.getStoreList() != null) {
            target.setStoreList(source.getStoreList().stream().map(BaseStoreModel::getUid).collect(Collectors.toList()));
        }
    }
}
