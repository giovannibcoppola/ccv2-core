package com.viking.facades.forms.impl;

import com.viking.core.jalo.NotificationPrefill;
import com.viking.core.service.VikingNotificationPrefillService;
import com.viking.facades.forms.VikingNotificationPrefillFacade;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class DefaultVikingNotificationPrefillFacade implements VikingNotificationPrefillFacade {
    private final Logger LOG = Logger.getLogger(DefaultVikingNotificationPrefillFacade.class);
    private VikingNotificationPrefillService vikingNotificationPrefillService;

    @Override
    public List<NotificationPrefill> getNotifications(String notificationId) {
        return vikingNotificationPrefillService.getNotifications(notificationId);
    }

    @Required
    public void setVikingNotificationPrefillService(VikingNotificationPrefillService vikingNotificationPrefillService) {
        this.vikingNotificationPrefillService = vikingNotificationPrefillService;
    }
}
