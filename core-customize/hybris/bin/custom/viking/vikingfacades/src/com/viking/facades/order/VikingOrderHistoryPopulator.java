/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.facades.order;

import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.core.model.order.OrderModel;

public class VikingOrderHistoryPopulator extends OrderHistoryPopulator {

    @Override
    public void populate(final OrderModel source, final OrderHistoryData target) {
        super.populate(source, target);
        target.setVikingErpOrderId(
                source.getVikingerporderid() ==null
                        || source.getVikingerporderid().isEmpty()
                        ? "(Not available yet)" : source.getVikingerporderid());
    }

}
