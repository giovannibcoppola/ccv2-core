package com.viking.facades.incoterm;

import java.util.List;

public interface VikingIncotermFacade {
    List<VikingIncotermData> findVikingIncotermForCode(String code);

    List<VikingIncotermData> findAllVikingIncoterms();
}
