package com.viking.facades.converter.populator;

import com.viking.core.model.VikingInvoiceModel;
import com.viking.core.service.VikingInvoiceService;
import com.viking.facades.data.InvoiceData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

public class VikingOrderInvoicePopulator implements Populator<OrderModel, OrderData> {

    private VikingInvoiceService vikingInvoiceService;

    @Override
    public void populate(OrderModel order, OrderData orderData) throws ConversionException {
        final List<InvoiceData> invoiceDatas = new ArrayList<>();

        order.getInvoices().forEach(invoice -> {
            final InvoiceData invoiceData = populateInvoiceData(invoice);
            invoiceDatas.add(invoiceData);
        });

        orderData.setInvoices(invoiceDatas);
    }

    private InvoiceData populateInvoiceData(VikingInvoiceModel invoice) {
        final InvoiceData data = new InvoiceData();
        data.setDateIssued(invoice.getDateIssued());
        data.setNumber(invoice.getId());
        data.setUrl(invoice.getLinkToPdf());
        return data;
    }

    @Required
    public void setVikingInvoiceService(VikingInvoiceService vikingInvoiceService) {
        this.vikingInvoiceService = vikingInvoiceService;
    }
}
