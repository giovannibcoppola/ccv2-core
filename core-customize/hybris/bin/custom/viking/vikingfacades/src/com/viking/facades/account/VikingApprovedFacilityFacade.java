package com.viking.facades.account;

import com.viking.facades.approvedfacility.data.ApprovedFacilityData;

public interface VikingApprovedFacilityFacade {
    ApprovedFacilityData getApprovedFacility();

    void setApprovedFacility(ApprovedFacilityData approvedFacility);
}
