package com.viking.facades.news;

import com.viking.news.data.NewsData;

import java.util.List;

/**
 * Facade to handle news
 */
public interface VikingNewsFacade {

    /**
     * Get latest news for specified site
     *
     * @return
     */
    List<NewsData> getLatestNewsForCurrentSite();
}
