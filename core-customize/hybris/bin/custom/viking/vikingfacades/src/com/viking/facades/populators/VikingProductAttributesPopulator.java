package com.viking.facades.populators;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Populates {@link ProductData} with certificate, files, drawing and datasheets
 */
public class VikingProductAttributesPopulator implements Populator<ProductModel, ProductData> {
    private Converter<MediaModel, MediaData> mediaConverter;

    @Override
    public void populate(final ProductModel source, final ProductData target) throws ConversionException {

        if (CollectionUtils.isNotEmpty(source.getViking_certificationDocuments())) {
            target.setCertificates(mediaConverter.convertAll(source.getViking_certificationDocuments()));
        }
        if (CollectionUtils.isNotEmpty(source.getViking_drawings())) {
            target.setDrawings(mediaConverter.convertAll(source.getViking_drawings()));
        }
        if (CollectionUtils.isNotEmpty(source.getVikingdatasheet())) {
            target.setDatasheets(mediaConverter.convertAll(source.getVikingdatasheet()));
        }
        if (CollectionUtils.isNotEmpty(source.getViking_manuals())) {
            target.setManuals(mediaConverter.convertAll(source.getViking_manuals()));
        }

    }

    /**
     * Sets media converter.
     *
     * @param mediaConverter the media converter
     */
    @Autowired
    public void setMediaConverter(Converter<MediaModel, MediaData> mediaConverter) {
        this.mediaConverter = mediaConverter;
    }
}
