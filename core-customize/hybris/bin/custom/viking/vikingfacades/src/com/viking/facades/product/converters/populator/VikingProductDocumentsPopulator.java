package com.viking.facades.product.converters.populator;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * Populator from {@link ProductModel} into a {@link ProductData} to populate Documents data
 *
 * @author Chiranjit Chakraborty
 */
public class VikingProductDocumentsPopulator implements Populator<ProductModel, ProductData> {

    private Converter<MediaModel, MediaData> mediaConverter;
    private ProductChannelDataService productChannelDataService;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) {
        populateManuals(productModel, productData);
        populateDatasheets(productModel, productData);
        populateBrochures(productModel, productData);
        populateAdditionalFiles(productModel, productData);
        populateCertificates(productModel, productData);
        populateVendorDatasheets(productModel, productData);
        populateDrawings(productModel, productData);
    }

    private void populateManuals(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredManualsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getManuals());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredManualsModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredManualsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getManuals());
        }

        if (CollectionUtils.isNotEmpty(filteredManualsModelList)) {
            productData.setManuals(getMediaConverter().convertAll(filteredManualsModelList));
        }
    }

    private void populateDatasheets(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredDatasheetsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getDataSheets());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredDatasheetsModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredDatasheetsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getDataSheets());
        }

        if (CollectionUtils.isNotEmpty(filteredDatasheetsModelList)) {
            productData.setDatasheets(getMediaConverter().convertAll(filteredDatasheetsModelList));
        }
    }

    private void populateBrochures(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredBrochuresModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getBrochures());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredBrochuresModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredBrochuresModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getBrochures());
        }

        if (CollectionUtils.isNotEmpty(filteredBrochuresModelList)) {
            productData.setBrochures(getMediaConverter().convertAll(filteredBrochuresModelList));
        }
    }

    private void populateAdditionalFiles(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredAdditionalFilesModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getAdditionalFiles());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredAdditionalFilesModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredAdditionalFilesModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getAdditionalFiles());
        }

        if (CollectionUtils.isNotEmpty(filteredAdditionalFilesModelList)) {
            productData.setAdditionalFiles(getMediaConverter().convertAll(filteredAdditionalFilesModelList));
        }
    }

    private void populateCertificates(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredCertificatesModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getCertificates());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredCertificatesModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredCertificatesModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getCertificates());
        }

        if (CollectionUtils.isNotEmpty(filteredCertificatesModelList)) {
            productData.setCertificates(getMediaConverter().convertAll(filteredCertificatesModelList));
        }
    }

    private void populateVendorDatasheets(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredVendorDatasheetsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getVendorDataSheets());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredVendorDatasheetsModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredVendorDatasheetsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getVendorDataSheets());
        }

        if (CollectionUtils.isNotEmpty(filteredVendorDatasheetsModelList)) {
            productData.setVendorDatasheets(getMediaConverter().convertAll(filteredVendorDatasheetsModelList));
        }
    }

    private void populateDrawings(ProductModel productModel, ProductData productData) {
        List<MediaModel> filteredDrawingsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) productModel.getDrawings());
        if (productModel instanceof ERPVariantProductModel && CollectionUtils.isEmpty(filteredDrawingsModelList)) {
            final ERPVariantProductModel variantProductModel = (ERPVariantProductModel) productModel;
            filteredDrawingsModelList = getProductChannelDataService().getFilteredMediasForChannel((List<MediaModel>) variantProductModel.getBaseProduct().getDrawings());
        }

        if (CollectionUtils.isNotEmpty(filteredDrawingsModelList)) {
            productData.setDrawings(getMediaConverter().convertAll(filteredDrawingsModelList));
        }
    }

    public ProductChannelDataService getProductChannelDataService() {
        return productChannelDataService;
    }

    public void setProductChannelDataService(ProductChannelDataService productChannelDataService) {
        this.productChannelDataService = productChannelDataService;
    }

    public Converter<MediaModel, MediaData> getMediaConverter() {
        return mediaConverter;
    }

    public void setMediaConverter(Converter<MediaModel, MediaData> mediaConverter) {
        this.mediaConverter = mediaConverter;
    }

}