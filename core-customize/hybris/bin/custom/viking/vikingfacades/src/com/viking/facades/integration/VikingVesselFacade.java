/**
 *
 */
package com.viking.facades.integration;

import java.util.List;
import java.util.Map;

import com.viking.core.integration.servicenow.data.VesselEquipmentDataWrapper;
import com.viking.facades.integration.data.VikingEquipmentDataWrapperFrontEnd;
import com.viking.facades.integration.data.VikingVesselData;
import com.viking.facades.integration.data.VikingEquipmentData;


/**
 * @author prabhakar
 *
 */
public interface VikingVesselFacade
{

	List<VikingVesselData> getAllVesselDataList();

	List<VikingVesselData> getAllVesselDataListByCustomerId(final String customerId);

	VesselEquipmentDataWrapper getVikingEquipmentDataList(final String imoId, final String customerId);

	List<VikingVesselData> getVesselDataListByUserId(final String userId);

	VikingEquipmentDataWrapperFrontEnd getVikingEquipmentDataListMap(final String imoId, final String customerId);
}
