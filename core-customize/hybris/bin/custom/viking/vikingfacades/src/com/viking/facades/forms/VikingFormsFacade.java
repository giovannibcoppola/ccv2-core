package com.viking.facades.forms;

/**
 * Facade to handle yForm requests
 */
public interface VikingFormsFacade {

    /**
     * Forward xml input to service
     * @param xml
     */
    void submitNotificationYFormToSAP(String notificationId, String serviceStation, String xml);

    String submitNotificationYFormToSAPandSavePDF(String notificationId, String serviceStation, String xml, String print);

    /**
     * Submit condemnation form to SAP.
     * @param xml
     */
    void submitCondemnationToSAP(String xml);
}
