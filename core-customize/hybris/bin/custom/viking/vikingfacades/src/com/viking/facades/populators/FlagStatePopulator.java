package com.viking.facades.populators;

import com.viking.core.model.FlagStateModel;
import com.viking.facades.flagstate.data.FlagStateData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class FlagStatePopulator implements Populator<FlagStateModel, FlagStateData> {

    @Override
    public void populate(FlagStateModel flagStateModel, FlagStateData flagStateData) throws ConversionException {
        flagStateData.setCode(flagStateModel.getCode());
        flagStateData.setName(flagStateModel.getName());
    }
}
