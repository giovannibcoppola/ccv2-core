/**
 *
 */
package com.viking.facades.vessels;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.viking.core.model.VikingVesselModel;
import com.viking.core.user.service.VikingUserPageService;
import com.viking.facades.vessels.data.VikingVesselPageData;


/**
 * @author Srikanth
 *
 */
public class VikingAssignVesselsImpl implements VikingAssignVesselsFacade
{
	private final Logger LOG = Logger.getLogger(VikingAssignVesselsImpl.class);

	@Resource(name = "vikingUserPageService")
	private VikingUserPageService vikingUserPageService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "b2BCustomerConverter")
	private Converter<B2BCustomerModel, CustomerData> b2BCustomerConverter;

	private Converter<VikingVesselModel, VikingVesselPageData> vikingVesselPageDataConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.facades.vessels.VikingAssignVesselsFacade#saveVesselsToUser(com.viking.vesselform.VesselForm)
	 */
	@Override
	public void saveVesselsToUser(final VesselData vesselData)
	{
		// YTODO Auto-generated method stub
		final String userName = vesselData.getUserName();
		final String vesselID = vesselData.getVesselId();
		final Set<VikingVesselModel> vesselSet = new HashSet<VikingVesselModel>();
		final B2BCustomerModel user = vikingUserPageService.getUser(userName);
		final VikingVesselModel vesselIds = vikingUserPageService.getVesselIds(vesselID);
		if (vesselIds != null && user != null)
		{
			final Set<VikingVesselModel> vikingVessel = user.getVikingVessel();
			vesselSet.addAll(vikingVessel);
			vesselSet.add(vesselIds);
			user.setVikingVessel(vesselSet);
			modelService.save(user);
		}
		else
		{
			final VikingVesselModel vikingVesselModel = modelService.create(VikingVesselModel.class);
			vikingVesselModel.setVesselId(vesselID);
			vikingVesselModel.setName(vesselData.getVesselName());
			//vesselSet.add(vikingVesselModel);
			if (user != null)
			{
				final Set<VikingVesselModel> vikingVessel = user.getVikingVessel();
				vesselSet.addAll(vikingVessel);
				vesselSet.add(vikingVesselModel);
				user.setVikingVessel(vesselSet);
			}
			modelService.save(vikingVesselModel);
			modelService.save(user);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.facades.vessels.VikingAssignVesselsFacade#getAllUsers()
	 */
	@Override
	public List<CustomerData> getAllUsers()
	{
		// YTODO Auto-generated method stub

		final List<B2BCustomerModel> allB2bCustomers = vikingUserPageService.getAllUsers();
		if (CollectionUtils.isNotEmpty(allB2bCustomers))
		{
			final List<CustomerData> customerData = Converters.convertAll(allB2bCustomers, b2BCustomerConverter);
			return customerData;
		}
		else
		{
			return null;
		}
	}

	@Override
	public void removeVesselFromUser(final String vesselIds, final String userId)
	{
		// YTODO Auto-generated method stub
		final B2BCustomerModel removeUser = vikingUserPageService.removeUser(userId);
		final VikingVesselModel vesselId = vikingUserPageService.getVesselIds(vesselIds);
		final Set<VikingVesselModel> vikingVesselSet = new HashSet<VikingVesselModel>();
		final Set<VikingVesselModel> vikingVessels = removeUser.getVikingVessel();
		if (vikingVessels != null && vikingVessels.contains(vesselId))
		{
			vikingVesselSet.addAll(vikingVessels);
			vikingVesselSet.remove(vesselId);
			removeUser.setVikingVessel(vikingVesselSet);
			modelService.save(removeUser);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.facades.vessels.VikingAssignVesselsFacade#vesselAndUserAssignments()
	 */
	@Override
	public int vesselAndUserAssignments()
	{
		// YTODO Auto-generated method stub
		//final List<UserModel> allUsers = vikingUserPageService.getAllUsers();
		int a = 0;
		final List<VikingVesselModel> allVikingVessels = vikingUserPageService.getAllVikingVessels();

		if (CollectionUtils.isNotEmpty(allVikingVessels))
		{
			final List<VikingVesselPageData> convertAll = vikingVesselPageDataConverter.convertAll(allVikingVessels);

			for (final VikingVesselPageData vikingVesselPageData : convertAll)
			{
				if (vikingVesselPageData.getCustomerData() != null)
				{
					final List<CustomerData> customerData = vikingVesselPageData.getCustomerData();
					for (final CustomerData customers : customerData)
					{
						a++;
					}
				}
			}
		}

		return a;
	}

	/**
	 * @return the vikingVesselPageDataConverter
	 */
	public Converter<VikingVesselModel, VikingVesselPageData> getVikingVesselPageDataConverter()
	{
		return vikingVesselPageDataConverter;
	}

	/**
	 * @param vikingVesselPageDataConverter
	 *           the vikingVesselPageDataConverter to set
	 */
	public void setVikingVesselPageDataConverter(
			final Converter<VikingVesselModel, VikingVesselPageData> vikingVesselPageDataConverter)
	{
		this.vikingVesselPageDataConverter = vikingVesselPageDataConverter;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.facades.vessels.VikingAssignVesselsFacade#getVessels()
	 */
	@Override
	public List<VikingVesselPageData> getVessels()
	{
		// YTODO Auto-generated method stub
		final List<VikingVesselModel> allVikingVessels = vikingUserPageService.getAllVikingVessels();

		if (CollectionUtils.isNotEmpty(allVikingVessels))
		{
			final List<VikingVesselPageData> convertAll = vikingVesselPageDataConverter.convertAll(allVikingVessels);
			return convertAll;
		}
		else
		{
			return null;
		}
	}



}
