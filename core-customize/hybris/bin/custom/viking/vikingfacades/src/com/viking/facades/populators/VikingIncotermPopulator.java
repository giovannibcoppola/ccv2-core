/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.facades.populators;

import com.viking.core.model.VikingIncotermModel;
import com.viking.facades.incoterm.VikingIncotermData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingIncotermPopulator implements Populator<VikingIncotermModel, VikingIncotermData> {

    @Override
    public void populate(final VikingIncotermModel source, final VikingIncotermData target) throws ConversionException {

        if (source != null) {
            target.setCode(source.getCode());
            target.setName(source.getName());
            target.setDescription(source.getDescription());
        }

    }
}