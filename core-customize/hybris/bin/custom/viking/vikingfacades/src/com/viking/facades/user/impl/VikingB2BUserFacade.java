package com.viking.facades.user.impl;

import com.viking.core.event.CreatePasswordEvent;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bcommercefacades.company.impl.DefaultB2BUserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.ForgottenPwdEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Date;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class VikingB2BUserFacade extends DefaultB2BUserFacade {

    private SecureTokenService secureTokenService;
    private EventService eventService;
    private BaseStoreService baseStoreService;
    private BaseSiteService baseSiteService;
    private CommonI18NService commonI18NService;

    @Override
    public void updateCustomer(final CustomerData customerData) {
        validateParameterNotNullStandardMessage("customerData", customerData);
        Assert.hasText(customerData.getTitleCode(), "The field [TitleCode] cannot be empty");
        Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
        Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
        B2BCustomerModel customerModel;
        if (StringUtils.isEmpty(customerData.getUid())) {
            customerModel = this.getModelService().create(B2BCustomerModel.class);
            getModelService().save(getB2BCustomerReverseConverter().convert(customerData, customerModel));
            getModelService().refresh(customerModel);
            publishEvent(customerModel);
        } else {
            customerModel = getUserService().getUserForUID(customerData.getUid(), B2BCustomerModel.class);
            getModelService().save(getB2BCustomerReverseConverter().convert(customerData, customerModel));
        }
    }

    public void publishEvent(final CustomerModel customerModel) {
        validateParameterNotNullStandardMessage("customerModel", customerModel);
        final long timeStamp = new Date().getTime();
        final SecureToken data = new SecureToken(customerModel.getUid(), timeStamp);
        final String token = secureTokenService.encryptData(data);
        customerModel.setToken(token);
        getModelService().save(customerModel);
        eventService.publishEvent(initializeEvent(new CreatePasswordEvent(token), customerModel));
    }

    protected AbstractCommerceUserEvent initializeEvent(final CreatePasswordEvent event, final CustomerModel customerModel) {
        event.setBaseStore(baseStoreService.getCurrentBaseStore());
        event.setSite(baseSiteService.getCurrentBaseSite());
        event.setCustomer(customerModel);
        event.setLanguage(commonI18NService.getCurrentLanguage());
        event.setCurrency(commonI18NService.getCurrentCurrency());
        event.setDefaultB2BUnitId(((B2BCustomerModel) customerModel).getDefaultB2BUnit().getUid());
        return event;
    }

    @Required
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setSecureTokenService(SecureTokenService secureTokenService) {
        this.secureTokenService = secureTokenService;
    }
}
