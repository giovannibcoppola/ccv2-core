package com.viking.facades.station;

import com.viking.core.data.VikingTechnicianData;
import com.viking.core.data.VikingTotalCreditData;

import java.util.List;

/**
 * VikingStationFacade to handle stations
 */
public interface VikingStationFacade {

    List<VikingTechnicianData> getTechnicians();

    List<VikingTotalCreditData>getCredits();
}
