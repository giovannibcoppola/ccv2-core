package com.viking.facades.process.email.context;

import com.viking.core.model.ApprovedFacilityModel;
import com.viking.core.model.UpdateProfileProcessModel;
import com.viking.core.model.UpdateProfileReceiverMailModel;
import com.viking.core.model.VikingSapSalesOrganizationModel;
import com.viking.facades.approvedfacility.data.ApprovedFacilityData;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;


/**
 * Velocity context for a profile update email.
 */
public class UpdateProfileEmailContext extends CustomerEmailContext {

    private final static Logger LOG = LogManager.getLogger(UpdateProfileEmailContext.class);

    private static final String DEFAULT_RECIEVER_EMAIL = "profile.update.default.receiver.mail";

    private Converter<AddressModel, AddressData> addressConverter;
    private Converter<ApprovedFacilityModel, ApprovedFacilityData> vikingApprovedFacilityConverter;
    private FlexibleSearchService flexibleSearchService;

    private String b2BUnitId;
    private AddressData address;
    private ApprovedFacilityData approvedFacility;

    @Override
    public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel) {
        super.init(storeFrontCustomerProcessModel, emailPageModel);
        if (storeFrontCustomerProcessModel instanceof UpdateProfileProcessModel) {
            setB2BUnitId(((UpdateProfileProcessModel) storeFrontCustomerProcessModel).getB2BUnitId());
            setAddress(addressConverter.convert(((UpdateProfileProcessModel) storeFrontCustomerProcessModel).getAddress()));
            setApprovedFacility(vikingApprovedFacilityConverter.convert(((UpdateProfileProcessModel) storeFrontCustomerProcessModel).getApprovedFacility()));
        }

        final CustomerModel customer = storeFrontCustomerProcessModel.getCustomer();
        if (customer != null) {
            put(TITLE, "");
            put(DISPLAY_NAME, "");
            put(EMAIL, findReceiverEmail(customer));
        }

    }

    private String findReceiverEmail(CustomerModel customer) {
        final VikingSapSalesOrganizationModel salesOrganization = ((B2BCustomerModel) customer).getDefaultB2BUnit().getSapSalesOrganization();
        final UpdateProfileReceiverMailModel example = new UpdateProfileReceiverMailModel();
        example.setSalesOrganization(salesOrganization);

        try {
            final UpdateProfileReceiverMailModel updateProfileReceiverMail = flexibleSearchService.getModelByExample(example);
            return updateProfileReceiverMail.getReceiverEmail();
        } catch (ModelNotFoundException | AmbiguousIdentifierException excecption) {
            LOG.debug(MessageFormat.format("No ProfileUpdateReceiverEmail found for salesOrganization with code: {0} - using default receiver", salesOrganization.getCode()));
            return getConfigurationService().getConfiguration().getString(DEFAULT_RECIEVER_EMAIL);
        }
    }

    public void setB2BUnitId(String b2BUnitId) {
        this.b2BUnitId = b2BUnitId;
    }

    public String getB2BUnitId() {
        return b2BUnitId;
    }

    public AddressData getAddress() {
        return address;
    }

    public void setAddress(AddressData address) {
        this.address = address;
    }

    public ApprovedFacilityData getApprovedFacility() {
        return approvedFacility;
    }

    public void setApprovedFacility(ApprovedFacilityData approvedFacility) {
        this.approvedFacility = approvedFacility;
    }

    @Required
    public void setAddressConverter(Converter<AddressModel, AddressData> addressConverter) {
        this.addressConverter = addressConverter;
    }

    @Required
    public void setVikingApprovedFacilityConverter(Converter<ApprovedFacilityModel, ApprovedFacilityData> vikingApprovedFacilityConverter) {
        this.vikingApprovedFacilityConverter = vikingApprovedFacilityConverter;
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
