package com.viking.facades.customer.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;

public class VikingCustomerFacade extends DefaultCustomerFacade {

    @Override
    public void updateProfile(final CustomerData customerData) throws DuplicateUidException {
        validateDataBeforeUpdate(customerData);

        final String name = getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName());
        final CartModel sessionCart = getCartService().getSessionCart();

        if (sessionCart != null && sessionCart.getUser() instanceof CustomerModel) {
            final CustomerModel customer = (CustomerModel) sessionCart.getUser();
            getCustomerAccountService().updateProfile(customer, customerData.getTitleCode(), name, customerData.getUid());
        } else {
            super.updateProfile(customerData);
        }
    }
}
