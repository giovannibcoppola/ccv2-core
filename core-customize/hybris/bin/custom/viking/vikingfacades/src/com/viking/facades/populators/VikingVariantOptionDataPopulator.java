package com.viking.facades.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class VikingVariantOptionDataPopulator extends AcceleratorVariantOptionDataPopulator {

    @Override
    public void populate(final VariantProductModel source, final VariantOptionData target) {
        super.populate(source, target);

        // Remove all the variantOptionQualifier with value = "".
        final Collection<VariantOptionQualifierData> variantOptionQualifiers = new ArrayList<>();
        variantOptionQualifiers.addAll(target.getVariantOptionQualifiers());

        final List<VariantOptionQualifierData> emptyVariantOptions = variantOptionQualifiers.stream()
                .filter(option -> StringUtils.isEmpty(option.getValue()))
                .collect(Collectors.toList());
        
        variantOptionQualifiers.removeAll(emptyVariantOptions);

        target.setVariantOptionQualifiers(variantOptionQualifiers);
    }
}
