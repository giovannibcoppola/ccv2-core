package com.viking.facades.process.email.context;

import com.viking.core.model.OrderNotificationForSalesRespProcessModel;
import com.viking.core.model.VikingSapSalesOrganizationModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Velocity context for a order notification email.
 */
public class OrderNotificationForSalesRespEmailContext extends AbstractEmailContext<OrderNotificationForSalesRespProcessModel> {
    private Converter<OrderModel, OrderData> orderConverter;
    private OrderData orderData;
    private String salesOrg;

    @Override
    public void init(final OrderNotificationForSalesRespProcessModel process, final EmailPageModel emailPageModel) {
        super.init(process, emailPageModel);
        orderData = orderConverter.convert(process.getOrder());

        final VikingSapSalesOrganizationModel salesOrg = process.getOrder().getUnit().getSapSalesOrganization();
        this.salesOrg = salesOrg.getCode();

        if (StringUtils.isNotEmpty(salesOrg.getEmail())) {
            put(EMAIL, salesOrg.getEmail());
        } else {
            put(EMAIL, getConfigurationService().getConfiguration().getString("receiver.email.for.order.placed.notification"));
        }
    }

    @Override
    protected BaseSiteModel getSite(final OrderNotificationForSalesRespProcessModel orderProcessModel) {
        return orderProcessModel.getOrder().getSite();
    }

    @Override
    protected CustomerModel getCustomer(final OrderNotificationForSalesRespProcessModel orderProcessModel) {
        return (CustomerModel) orderProcessModel.getOrder().getUser();
    }

    @Override
    protected LanguageModel getEmailLanguage(final OrderNotificationForSalesRespProcessModel orderProcessModel) {
        return orderProcessModel.getOrder().getLanguage();
    }

    public OrderData getOrder() {
        return orderData;
    }

    public String getSalesOrg() {
        return salesOrg;
    }

    @Required
    public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter) {
        this.orderConverter = orderConverter;
    }
}
