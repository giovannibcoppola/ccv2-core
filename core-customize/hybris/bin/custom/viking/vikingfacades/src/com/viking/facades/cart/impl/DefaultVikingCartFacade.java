package com.viking.facades.cart.impl;

import com.viking.core.service.VikingPointOfServiceService;
import com.viking.facades.cart.VikingCartFacade;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Optional;

public class DefaultVikingCartFacade extends DefaultCartFacade implements VikingCartFacade {

    private BaseStoreService baseStoreService;
    private VikingPointOfServiceService vikingPointOfServiceService;

    @Override
    public void updateSessionCart() {
        final CartModel sessionCart = getCartService().getSessionCart();
        final BaseStoreModel store = baseStoreService.getCurrentBaseStore();
        sessionCart.setStore(store);

        List<AbstractOrderEntryModel> entries = sessionCart.getEntries();
        getModelService().removeAll(entries);
        getModelService().save(sessionCart);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeDeliveryPointOfServiceFromCart() {
        final CartModel sessionCart = getCartService().getSessionCart();
        sessionCart.getEntries().forEach(entry -> {
            entry.setDeliveryPointOfService(null);
            getModelService().save(entry);
        });

        sessionCart.setPointOfService(null);
        getModelService().save(sessionCart);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPointOfServiceOnCart(final String pointOfService) {
        final Optional<PointOfServiceModel> pos = vikingPointOfServiceService.getPointOfServiceByDisplayName(pointOfService);

        if (pos.isPresent()) {
            final CartModel sessionCart = getCartService().getSessionCart();
            sessionCart.setPointOfService(pos.get());
            getModelService().save(sessionCart);
        }
    }

    @Override
    public void copyPointOfServiceFromCartEntries() {
        final CartModel sessionCart = getCartService().getSessionCart();
        sessionCart.getEntries().forEach(entry -> {
            entry.setDeliveryPointOfService(sessionCart.getPointOfService());
            getModelService().save(entry);
        });
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setVikingPointOfServiceService(VikingPointOfServiceService vikingPointOfServiceService) {
        this.vikingPointOfServiceService = vikingPointOfServiceService;
    }
}
