package com.viking.facades.station.impl;

import com.viking.core.data.VikingTechnicianData;
import com.viking.core.data.VikingTotalCreditData;
import com.viking.core.service.VikingStationService;
import com.viking.facades.station.VikingStationFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingStationFacade implements VikingStationFacade {

    private UserService userService;
    private VikingStationService vikingStationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VikingTechnicianData> getTechnicians() {
        final UserModel currentUser = userService.getCurrentUser();

        return vikingStationService.getTechnicians(currentUser.getUid());
    }

    @Override
    public List<VikingTotalCreditData> getCredits() {
        final UserModel currentUser = userService.getCurrentUser();

        return vikingStationService.getCredits(currentUser.getUid());
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setVikingStationService(VikingStationService vikingStationService) {
        this.vikingStationService = vikingStationService;
    }
}
