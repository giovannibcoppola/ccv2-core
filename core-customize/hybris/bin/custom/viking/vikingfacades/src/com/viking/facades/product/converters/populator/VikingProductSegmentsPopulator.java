package com.viking.facades.product.converters.populator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.viking.core.enums.Segment;

import static java.util.stream.Collectors.toList;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingProductSegmentsPopulator implements Populator<ProductModel, ProductData> {
    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        if(source.getSegments() != null && CollectionUtils.isNotEmpty(source.getSegments())) {
            target.setSegments(source.getSegments()
                    .stream()
                    .filter(s -> StringUtils.isNotEmpty(s.getCode()))
                    .map(Segment::getCode)
                    .collect(toList()));
        }
    }
}
