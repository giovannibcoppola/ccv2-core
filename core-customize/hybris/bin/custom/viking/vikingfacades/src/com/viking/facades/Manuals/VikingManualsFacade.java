package com.viking.facades.Manuals;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualData;
import com.viking.core.integration.azure.VesselDocsData;

import java.io.IOException;
import java.util.List;

public interface VikingManualsFacade {

    ManualData getManual(String id);

    List<BulletinsData> getBulletins(String id);

    void setManualBulletinsAccepted(String bulletinsId, String manualsId, String userId) throws  IOException;

    String download(String url, String legacy, boolean extensionReq, String filename);

    VesselDocsData getVesselDocuments(String imo);

}
