package com.viking.facades.product.converters.populator;

import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.viking.core.enums.Standard;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingProductStandardsPopulator implements Populator<ProductModel, ProductData> {
    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        if(CollectionUtils.isNotEmpty(source.getStandards())) {
            target.setStandards(source.getStandards().stream().map(Standard::getCode).collect(Collectors.toSet()));
        }
    }
}
