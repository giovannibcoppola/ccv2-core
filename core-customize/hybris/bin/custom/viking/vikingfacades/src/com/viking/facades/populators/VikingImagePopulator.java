package com.viking.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.StringUtils;

public class VikingImagePopulator extends ImagePopulator {

    @Override
    public void populate(final MediaModel source, final ImageData target) throws ConversionException {
        super.populate(source, target);

        if (StringUtils.isNotEmpty(source.getYoutubeVideoId())) {
            target.setYoutubeVideoId(source.getYoutubeVideoId());
        }

    }
}
