package com.viking.facades.product.converters.populator;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.viking.core.model.SimpleChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import com.viking.facades.integration.data.ChannelTextData;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class VikingProductTeasersPopulator implements Populator<ProductModel, ProductData> {
    private Converter<SimpleChannelTextModel, ChannelTextData> vikingSimpleChannelTextConverter;
    private final ProductChannelDataService productChannelDataService;

    @Autowired
    public VikingProductTeasersPopulator(Converter<SimpleChannelTextModel, ChannelTextData> vikingSimpleChannelTextConverter, ProductChannelDataService productChannelDataService) {
        this.vikingSimpleChannelTextConverter = vikingSimpleChannelTextConverter;
        this.productChannelDataService = productChannelDataService;
    }

    @Override
    public void populate(ProductModel source, ProductData target) throws ConversionException {
        final Collection<SimpleChannelTextModel> filteredTeasersForChannel = productChannelDataService.getFilteredTeasersForChannel(source.getTeasers());
        if(CollectionUtils.isNotEmpty(filteredTeasersForChannel)) {
            target.setTeasers(vikingSimpleChannelTextConverter.convertAll(filteredTeasersForChannel));
        }
    }
}
