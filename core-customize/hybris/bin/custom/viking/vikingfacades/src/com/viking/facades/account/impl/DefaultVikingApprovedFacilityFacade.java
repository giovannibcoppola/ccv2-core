package com.viking.facades.account.impl;

import com.viking.core.event.UpdateProfileEvent;
import com.viking.core.model.ApprovedFacilityModel;
import com.viking.core.service.VikingApprovedFacilityService;
import com.viking.facades.account.VikingApprovedFacilityFacade;
import com.viking.facades.approvedfacility.data.ApprovedFacilityData;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import org.springframework.beans.factory.annotation.Required;

public class DefaultVikingApprovedFacilityFacade implements VikingApprovedFacilityFacade {
    private VikingApprovedFacilityService vikingApprovedFacilityService;
    private Converter<ApprovedFacilityModel, ApprovedFacilityData> vikingApprovedFacilityConverter;
    private EventService eventService;
    private UserService userService;
    private BaseSiteService baseSiteService;
    private BaseStoreService baseStoreService;
    private CommonI18NService commonI18NService;

    @Override
    public ApprovedFacilityData getApprovedFacility() {
        return vikingApprovedFacilityConverter.convert(vikingApprovedFacilityService.getApprovedFacility());
    }

    @Override
    public void setApprovedFacility(final ApprovedFacilityData approvedFacility) {
        final ApprovedFacilityModel facility = vikingApprovedFacilityService.setApprovedFacility(approvedFacility);

        publishEmailEvent(facility);
    }

    public void publishEmailEvent(final ApprovedFacilityModel facility) {
        final UpdateProfileEvent event = new UpdateProfileEvent();
        event.setB2BUnitId(facility.getCustomer().getDefaultB2BUnit().getUid());
        event.setApprovedFacility(facility);
        event.setCustomer((CustomerModel) userService.getCurrentUser());
        event.setSite(baseSiteService.getCurrentBaseSite());
        event.setBaseStore(baseStoreService.getCurrentBaseStore());
        event.setLanguage(commonI18NService.getCurrentLanguage());
        event.setCurrency(commonI18NService.getCurrentCurrency());
        eventService.publishEvent(event);
    }

    @Required
    public void setVikingApprovedFacilityService(VikingApprovedFacilityService vikingApprovedFacilityService) {
        this.vikingApprovedFacilityService = vikingApprovedFacilityService;
    }

    @Required
    public void setVikingApprovedFacilityConverter(Converter<ApprovedFacilityModel, ApprovedFacilityData> vikingApprovedFacilityConverter) {
        this.vikingApprovedFacilityConverter = vikingApprovedFacilityConverter;
    }

    @Required
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }
}
