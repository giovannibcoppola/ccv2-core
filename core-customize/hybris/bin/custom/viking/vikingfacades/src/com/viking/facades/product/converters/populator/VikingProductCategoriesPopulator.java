package com.viking.facades.product.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.ProductCategoriesPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Viking implementation extending {@link ProductCategoriesPopulator} to add new functionality.
 *
 * @author javier.gomez
 */
public class VikingProductCategoriesPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends ProductCategoriesPopulator {

    @Resource(name = "productAndCategoryHelper")
    private ProductAndCategoryHelper productAndCategoryHelper;

    @Resource(name = "commerceCategoryService")
    private CommerceCategoryService commerceCategoryService;

    @Override
    public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
        super.populate(productModel, productData);

        productData.setLevel3categoryName(getLevel3CategoryName(productModel));
    }

    private String getLevel3CategoryName(ProductModel productModel) {
        if (CollectionUtils.isNotEmpty(productModel.getSupercategories())) {
            List<CategoryModel> categoryPath = getCategoryPath(productModel);
            if(CollectionUtils.isNotEmpty(categoryPath)) {
                final List<CategoryModel> topThreeCategories = categoryPath.size() > 3 ? categoryPath.subList(0, 3) : categoryPath;
                Collections.reverse(topThreeCategories);
                final CategoryModel level3Category = topThreeCategories.iterator().next();
                if(level3Category!=null) {
                    return level3Category.getName();
                }
            }
        }

        return null;
    }

    private List<CategoryModel> getCategoryPath(final ProductModel product)
    {
        final CategoryModel category = getPrimaryCategoryForProduct(product);
        if (category != null)
        {
            return getCategoryPath(category);
        }
        return Collections.emptyList();
    }

    protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product)
    {
        // Get the first super-category from the product that isn't a classification category
        for (final CategoryModel category : product.getSupercategories())
        {
            if (productAndCategoryHelper.isValidProductCategory(category))
            {
                return category;
            }
        }

        return null;
    }


    protected List<CategoryModel> getCategoryPath(final CategoryModel category)
    {
        final Collection<List<CategoryModel>> paths = commerceCategoryService.getPathsForCategory(category);
        // Return first - there will always be at least 1
        return paths.iterator().next();
    }

}
