package com.viking.facades.strategy;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceDeliveryAddressStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class DefaultVikingCommerceDeliveryAddressStrategy extends DefaultCommerceDeliveryAddressStrategy {

    @Override
    public boolean storeDeliveryAddress(final CommerceCheckoutParameter parameter) {

        final CartModel cartModel = parameter.getCart();
        final AddressModel addressModel = parameter.getAddress();
        final boolean flagAsDeliveryAddress = parameter.isIsDeliveryAddress();

        validateParameterNotNull(cartModel, "Cart model cannot be null");
        getModelService().refresh(cartModel);

        final UserModel user = cartModel.getUser();
        getModelService().refresh(user);

        cartModel.setDeliveryAddress(addressModel);

        getModelService().save(cartModel);

        final CommerceCartParameter calculateCartParameter = new CommerceCartParameter();
        calculateCartParameter.setEnableHooks(true);
        calculateCartParameter.setCart(cartModel);
        getCommerceCartCalculationStrategy().calculateCart(calculateCartParameter);

        // verify if the current delivery mode is still valid for this address
        getCommerceDeliveryModeValidationStrategy().validateDeliveryMode(parameter);
        getModelService().refresh(cartModel);

        return true;

    }

    @Override
    protected boolean isValidDeliveryAddress(final CartModel cartModel, final AddressModel addressModel) {
        return true;
    }
}
