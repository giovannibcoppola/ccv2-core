package com.viking.facades.populators;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingMediaExpiryDatePopulator implements Populator<MediaModel, MediaData> {
    private static final DateTimeFormatter OUTBOUND_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Override
    public void populate(MediaModel source, MediaData target) throws ConversionException {
        if(source.getExpiryDate() != null) {
            final LocalDate localDate = LocalDate.ofInstant(source.getExpiryDate().toInstant(), ZoneId.systemDefault());
            target.setExpiryDate(localDate.format(OUTBOUND_FORMATTER));
        }
    }
}
