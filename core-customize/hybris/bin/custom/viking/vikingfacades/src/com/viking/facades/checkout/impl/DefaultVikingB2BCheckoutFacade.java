package com.viking.facades.checkout.impl;

import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.service.VikingOrderEntryService;
import com.viking.facades.checkout.VikingB2BCheckoutFacade;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BCommentData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BReplenishmentRecurrenceEnum;
import de.hybris.platform.b2bacceleratorfacades.order.data.TriggerData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BAcceleratorCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

import static de.hybris.platform.util.localization.Localization.getLocalizedString;

public class DefaultVikingB2BCheckoutFacade extends DefaultB2BAcceleratorCheckoutFacade implements VikingB2BCheckoutFacade  {

    private static final Logger LOG = LogManager.getLogger(DefaultVikingB2BCheckoutFacade.class);

    private static final String CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED = "cart.transation.notAuthorized";
    private static final String CART_CHECKOUT_TERM_UNCHECKED = "cart.term.unchecked";
    private static final String CART_CHECKOUT_NO_QUOTE_DESCRIPTION = "cart.no.quote.description";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE = "cart.replenishment.no.startdate";
    private static final String CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY = "cart.replenishment.no.frequency";

    private KeyGenerator guidKeyGenerator;

    private VikingOrderEntryService vikingOrderEntryService;

    @Resource
    private EnquiryService enquiryService;

    @Override
    protected void beforePlaceOrder(final CartModel cartModel)
    {
        super.beforePlaceOrder(cartModel);
        cartModel.setStatus(OrderStatus.CREATED);
    }

    @Override
    public List<B2BPaymentTypeData> getPaymentTypes()
    {
        final List<CheckoutPaymentType> checkoutPaymentTypes = Collections.singletonList(CheckoutPaymentType.ACCOUNT);

        return Converters.convertAll(checkoutPaymentTypes, getB2bPaymentTypeDataConverter());
    }

    @Override
    public CartData getCheckoutCart()
    {
        final CartData cartData = getCartFacade().getSessionCart();
        if (cartData != null)
        {
            cartData.setDeliveryAddress(getDeliveryAddress());
            cartData.setDeliveryMode(getDeliveryMode());
            cartData.setPaymentInfo(getPaymentDetails());
            cartData.setCalculated(true);
        }
        return cartData;
    }

    @Override
    public CartData updateCheckoutCart(final CartData cartData)
    {
        final CartModel cartModel = getCart();
        if (cartModel == null)
        {
            return null;
        }
        // set payment type
        if (cartData.getPaymentType() != null)
        {
            final String newPaymentTypeCode = cartData.getPaymentType().getCode();

            // clear delivery address, delivery mode and payment details when changing payment type
            if (cartModel.getPaymentType() == null || !newPaymentTypeCode.equalsIgnoreCase(cartModel.getPaymentType().getCode()))
            {
                cartModel.setDeliveryAddress(null);
                cartModel.setDeliveryMode(null);
                cartModel.setPaymentInfo(null);
            }

            setPaymentTypeForCart(newPaymentTypeCode, cartModel);
            // cost center need to be be cleared if using card
            if (CheckoutPaymentType.CARD.getCode().equals(newPaymentTypeCode))
            {
                setCostCenterForCart(null, cartModel);
            }
        }
        cartModel.setIncoterm(cartData.getIncoterm());

        // set cost center
        if (cartData.getCostCenter() != null)
        {
            setCostCenterForCart(cartData.getCostCenter().getCode(), cartModel);
        }

        // set purchase order number
        if (cartData.getPurchaseOrderNumber() != null)
        {
            cartModel.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
        }

        // set delivery address
        if (cartData.getDeliveryAddress() != null)
        {
            setDeliveryAddress(cartData.getDeliveryAddress());
        }

        // set quote request description
        if (cartData.getB2BComment() != null)
        {
            final B2BCommentModel b2bComment = getModelService().create(B2BCommentModel.class);
            b2bComment.setComment(cartData.getB2BComment().getComment());

            // Clear existing comments before adding
            cartModel.setB2bcomments(Collections.emptyList());
            getModelService().save(cartModel);
            getModelService().refresh(cartModel);

            getB2bCommentService().addComment(cartModel, b2bComment);
        }

        // set Delivery Note
        if (StringUtils.isNotEmpty(cartData.getDeliveryNote()) || StringUtils.isNotEmpty(cartModel.getDeliveryNote())) {
            cartModel.setDeliveryNote(cartData.getDeliveryNote());
        }

        getModelService().save(cartModel);
        return getCheckoutCart();

    }

    @Override
    protected AddressData getDeliveryAddress() {
        final CartModel cart = getCart();
        if (cart != null) {
            final AddressModel deliveryAddress = cart.getDeliveryAddress();
            if(deliveryAddress != null ) {
                return getAddressConverter().convert(deliveryAddress);
            }

        }
        return null;
    }

    @Override
    public void saveNewAddress(CartData cartData, AddressData addressData, Boolean saveInAddressBook) {
        CartModel cartModel = getCartService().getSessionCart();
        AddressModel addressModel = getModelService().create(AddressModel.class);
        addressModel.setOwner(cartModel);
        getAddressReversePopulator().populate(addressData, addressModel);
        getModelService().save(addressModel);
        addressData.setId(addressModel.getPk().toString());
        cartModel.setDeliveryAddress(addressModel);
        getModelService().save(cartModel);

        if(saveInAddressBook) {
            //B2BUnitModel b2BUnitModel = ;
            B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) getUserService().getCurrentUser();
            AddressModel newAddressModel = getModelService().create(AddressModel.class);
            newAddressModel.setOwner(b2BCustomerModel.getDefaultB2BUnit());
            getAddressReversePopulator().populate(addressData, newAddressModel);
            getModelService().save(newAddressModel);
        }
    }

    @Override
    public boolean isOnlyCertificateProductsInCart() {
        final List<AbstractOrderEntryModel> entries = getCartService().getSessionCart().getEntries();

        for (final AbstractOrderEntryModel entry : entries) {
            if(!vikingOrderEntryService.isCertificateEntry(entry)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean isNewAddressEnabledForCart() {
        return true;
    }

    @Override
    public boolean isRemoveAddressEnabledForCart() {
        return true;
    }


    @Override
    public <T extends AbstractOrderData> T placeOrder(PlaceOrderData placeOrderData) throws InvalidCartException {

        // term must be checked
        if (!placeOrderData.getTermsCheck().equals(Boolean.TRUE))
        {
            throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED));
        }

        // for CARD type, transaction must be authorized before placing order
        final boolean isCardtPaymentType = CheckoutPaymentType.CARD.equals(getCart().getPaymentType());
        if (isCardtPaymentType)
        {
            final List<PaymentTransactionModel> transactions = getCart().getPaymentTransactions();
            boolean authorized = false;
            for (final PaymentTransactionModel transaction : transactions)
            {
                for (final PaymentTransactionEntryModel entry : transaction.getEntries())
                {
                    if (entry.getType().equals(PaymentTransactionType.AUTHORIZATION)
                            && TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus()))
                    {
                        authorized = true;
                        break;
                    }
                }
            }
            if (!authorized)
            {
                // FIXME - change error message
                throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED));
            }
        }

        if (isValidCheckoutCart(placeOrderData))
        {
            // validate quote negotiation
            if (placeOrderData.getNegotiateQuote() != null && placeOrderData.getNegotiateQuote().equals(Boolean.TRUE))
            {
                if (StringUtils.isBlank(placeOrderData.getQuoteRequestDescription()))
                {
                    throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NO_QUOTE_DESCRIPTION));
                }
                else
                {
                    final B2BCommentData b2BComment = new B2BCommentData();
                    b2BComment.setComment(placeOrderData.getQuoteRequestDescription());

                    final CartData cartData = new CartData();
                    cartData.setB2BComment(b2BComment);

                    updateCheckoutCart(cartData);
                }
            }

            // validate replenishment
            if (placeOrderData.getReplenishmentOrder() != null && placeOrderData.getReplenishmentOrder().equals(Boolean.TRUE))
            {
                if (placeOrderData.getReplenishmentStartDate() == null)
                {
                    throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_REPLENISHMENT_NO_STARTDATE));
                }

                if (placeOrderData.getReplenishmentRecurrence().equals(B2BReplenishmentRecurrenceEnum.WEEKLY)
                        && CollectionUtils.isEmpty(placeOrderData.getNDaysOfWeek()))
                {
                    throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_REPLENISHMENT_NO_FREQUENCY));
                }

                final TriggerData triggerData = new TriggerData();
                populateTriggerDataFromPlaceOrderData(placeOrderData, triggerData);

                return (T) scheduleOrder(triggerData);
            }

            return (T) placeOrder();
        }

        return null;
    }

    @Override
    public OrderData placeOrder() throws InvalidCartException {
        final CartModel cartModel = getCart();
        if (cartModel == null) {
            throw  new IllegalStateException("Can not placeOrder if cart is null");
        }
        if(enquiryService.isEnquiryFlow()) {
            return placeEnquiry(cartModel);
        } else {
            return placeVikingOrder(cartModel);
        }
    }

    private OrderData placeEnquiry(CartModel cartModel) throws InvalidCartException {
       final OrderModel orderModel = placeOrder(cartModel);
        if (orderModel != null)
        {
            return getOrderConverter().convert(orderModel);
        }
        return null;
    }

    private OrderData placeVikingOrder(CartModel cartModel) throws InvalidCartException {
        boolean certificateInCart = false;
        boolean normalInCart = false;
        List<AbstractOrderEntryModel> items = cartModel.getEntries();

        for(AbstractOrderEntryModel item : items) {
            if (vikingOrderEntryService.isCertificateEntry(item)) {
                certificateInCart = true;
            }
            else
            {
                normalInCart = true;
            }
        }

        LOG.debug(MessageFormat.format("Cart {0} has certificate {1} - has normal products {2}", cartModel.getCode(), certificateInCart, normalInCart));
        //Create two new carts if needed.
        if(certificateInCart&&normalInCart)
        {
            final CartModel certificatesCart = getModelService().create(CartModel.class);
            final CartModel normalCart = getModelService().create(CartModel.class);

            LOG.debug("Certificates found - Creating certificate cart");
            certificatesCart.setDeliveryAddress(cartModel.getDeliveryAddress());
            certificatesCart.setPaymentAddress(cartModel.getPaymentAddress());
            certificatesCart.setDeliveryMode(cartModel.getDeliveryMode());
            certificatesCart.setUser(cartModel.getUser());
            certificatesCart.setComments(cartModel.getComments());
            certificatesCart.setDate(cartModel.getDate());
            certificatesCart.setCurrency(cartModel.getCurrency());
            certificatesCart.setIncoterm(cartModel.getIncoterm());
            certificatesCart.setPaymentInfo(cartModel.getPaymentInfo());
            certificatesCart.setPaymentMode(cartModel.getPaymentMode());
            certificatesCart.setSite(cartModel.getSite());
            certificatesCart.setStore(cartModel.getStore());
            certificatesCart.setUnit(cartModel.getUnit());
            certificatesCart.setStatus(OrderStatus.CREATED);
            certificatesCart.setAllPromotionResults(Collections.EMPTY_SET);
            certificatesCart.setPaymentTransactions(Collections.EMPTY_LIST);
            certificatesCart.setPermissionResults(Collections.EMPTY_LIST);
            certificatesCart.setStatus(OrderStatus.CREATED);
            certificatesCart.setGuid(getGuidKeyGenerator().generate().toString());
            certificatesCart.setCalculated(true);
            certificatesCart.setPurchaseOrderNumber(cartModel.getPurchaseOrderNumber());
            certificatesCart.setDeliveryNote(cartModel.getDeliveryNote());
            populateComments(cartModel, certificatesCart);

            normalCart.setDeliveryAddress(cartModel.getDeliveryAddress());
            normalCart.setPaymentAddress(cartModel.getPaymentAddress());
            normalCart.setDeliveryMode(cartModel.getDeliveryMode());
            normalCart.setUser(cartModel.getUser());
            normalCart.setComments(cartModel.getComments());
            normalCart.setDate(cartModel.getDate());
            normalCart.setCurrency(cartModel.getCurrency());
            normalCart.setIncoterm(cartModel.getIncoterm());
            normalCart.setPaymentInfo(cartModel.getPaymentInfo());
            normalCart.setPaymentMode(cartModel.getPaymentMode());
            normalCart.setSite(cartModel.getSite());
            normalCart.setStore(cartModel.getStore());
            normalCart.setUnit(cartModel.getUnit());
            normalCart.setStatus(OrderStatus.CREATED);
            normalCart.setAllPromotionResults(Collections.EMPTY_SET);
            normalCart.setPaymentTransactions(Collections.EMPTY_LIST);
            normalCart.setPermissionResults(Collections.EMPTY_LIST);
            normalCart.setStatus(OrderStatus.CREATED);
            normalCart.setGuid(getGuidKeyGenerator().generate().toString());
            normalCart.setCalculated(true);
            normalCart.setPurchaseOrderNumber(cartModel.getPurchaseOrderNumber());
            normalCart.setDeliveryNote(cartModel.getDeliveryNote());
            normalCart.setDeliveryCost(cartModel.getDeliveryCost());
            populateComments(cartModel, normalCart);


            List<AbstractOrderEntryModel> normalCartItems = new ArrayList<AbstractOrderEntryModel>();
            List<AbstractOrderEntryModel> certificateItems = new ArrayList<AbstractOrderEntryModel>();

            //Move items to the correct carts
            for(AbstractOrderEntryModel item : items){
                final AbstractOrderEntryModel clonedEntry = getModelService().clone(item, item.getClass());
                getModelService().detach(clonedEntry);
                if(vikingOrderEntryService.isCertificateEntry(item)){

                    clonedEntry.setOrder(certificatesCart);
                    updateEntryNumber(clonedEntry, certificatesCart);

                    if (certificatesCart.getEntries() == null)
                    {
                        certificatesCart.setEntries(Collections.singletonList(clonedEntry));
                    }
                    else
                    {
                        final List<AbstractOrderEntryModel> entries = new ArrayList<>(certificatesCart.getEntries());
                        clonedEntry.setCalculated(true);
                        entries.add(clonedEntry);
                        certificatesCart.setEntries(entries);
                    }
                    getModelService().save(clonedEntry);
                    certificateItems.add(clonedEntry);

                    LOG.debug("Item " +item.getProduct().getCode() + " moved to certificate cart.");
                }
                else {

                    clonedEntry.setOrder(normalCart);
                    updateEntryNumber(clonedEntry, normalCart);

                    if (normalCart.getEntries() == null)
                    {
                        normalCart.setEntries(Collections.singletonList(clonedEntry));
                    }
                    else
                    {
                        final List<AbstractOrderEntryModel> entries = new ArrayList<>(normalCart.getEntries());
                        clonedEntry.setCalculated(true);
                        entries.add(clonedEntry);
                        normalCart.setEntries(entries);
                    }
                    getModelService().save(clonedEntry);
                    normalCartItems.add(clonedEntry);

                    LOG.debug("Item " + item.getProduct().getCode() + " moved to certificate cart.");

                }
            }

            LOG.debug("Save normal cart " + normalCart.getCode());
            normalCart.setCalculated(true);
            getModelService().save(normalCart);
            LOG.debug("Save certificate cart " + certificatesCart.getCode());
            certificatesCart.setCalculated(true);
            getModelService().save(certificatesCart);


            OrderModel orderCertificateModel = null;
            LOG.debug("Creating certificate Order");
            if (certificatesCart.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
            {
                beforePlaceOrder(certificatesCart);
                orderCertificateModel = placeOrder(certificatesCart);
                afterPlaceOrder(certificatesCart, orderCertificateModel);
            }
            LOG.debug("Certificate cart done " );
            LOG.debug("Create order with standard products");
            OrderModel orderModel = null;
            if (normalCart.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout()) {
                beforePlaceOrder(normalCart);
                orderModel = placeOrder(normalCart);
                afterPlaceOrder(normalCart, orderModel);
            }
            LOG.debug("Standard product cart done " );

            getModelService().remove(normalCart);
            getModelService().remove(certificatesCart);

            if (orderModel != null)
            {
                return getOrderConverter().convert(orderModel);
            }
        }
        else
        {
            LOG.debug( "Create order - either normal or certificate" );
            OrderModel orderModel = null;
            if (cartModel != null)
            {
                cartModel.setCalculated(true);
                getModelService().save(cartModel);
                if (cartModel.getUser().equals(getCurrentUserForCheckout()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
                {
                    beforePlaceOrder(cartModel);
                    orderModel = placeOrder(cartModel);
                    afterPlaceOrder(cartModel, orderModel);

                }
            }

            LOG.debug("Standard product cart done");
            if (orderModel != null)
            {
                return getOrderConverter().convert(orderModel);
            }
        }
        return null;
    }

    private void populateComments(final CartModel original, final CartModel newCart) {
        original.getB2bcomments().forEach(comment -> {
            final B2BCommentModel clonedComment = getModelService().create(B2BCommentModel.class);
            clonedComment.setComment(comment.getComment());
            clonedComment.setOrder(newCart);
            getModelService().save(clonedComment);
        });
    }

    protected void updateEntryNumber(final AbstractOrderEntryModel entry, final CartModel toCart)
    {
        if (toCart.getEntries() == null)
        {
            return;
        }
        boolean duplicate = false;
        int maxEntryGroupNumber = 0;
        for (final AbstractOrderEntryModel e : toCart.getEntries())
        {
            if (Objects.equals(entry.getEntryNumber(), e.getEntryNumber()))
            {
                duplicate = true;
            }
            if (e.getEntryNumber() != null)
            {
                maxEntryGroupNumber = Math.max(maxEntryGroupNumber, e.getEntryNumber().intValue());
            }
        }
        if (duplicate)
        {
            entry.setEntryNumber(Integer.valueOf(maxEntryGroupNumber + 1));
        }
    }

    protected KeyGenerator getGuidKeyGenerator()
    {
        return guidKeyGenerator;
    }
    public void setGuidKeyGenerator(final KeyGenerator guidKeyGenerator)
    {
        this.guidKeyGenerator = guidKeyGenerator;
    }

    @Required
    public void setVikingOrderEntryService(VikingOrderEntryService vikingOrderEntryService) {
        this.vikingOrderEntryService = vikingOrderEntryService;
    }

    @Override
    public boolean setDeliveryAddressIfAvailable() {
        final CartModel cartModel = getCart();

        if (cartModel == null || cartModel.getDeliveryAddress() != null) {
            return false;
        }

        final UserModel currentUser = getCurrentUserForCheckout();
        if(currentUser instanceof B2BCustomerModel) {
            B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) currentUser;
            if(b2BCustomerModel.getDefaultB2BUnit() != null && b2BCustomerModel.getDefaultB2BUnit().getShippingAddress() != null) {
                return  setDeliveryAddress(cartModel, b2BCustomerModel.getDefaultB2BUnit().getShippingAddress());
            }
        }
        return setDeliveryAddressForCartAndUser(cartModel, currentUser);
    }

    private boolean setDeliveryAddressForCartAndUser(CartModel cartModel, UserModel currentUser) {
        if (cartModel.getUser().equals(currentUser))
        {
            final Boolean supportedDeliveryAddress = setAddressFromUser(cartModel, currentUser);
            if (supportedDeliveryAddress) return supportedDeliveryAddress;
        }

        // Could not use default address, try any address
        final List<AddressModel> supportedDeliveryAddresses = getDeliveryService().getSupportedDeliveryAddressesForOrder(cartModel,
                true);
        if (supportedDeliveryAddresses != null && !supportedDeliveryAddresses.isEmpty())
        {
            return setDeliveryAddress(cartModel, supportedDeliveryAddresses.get(0));
        }
        return false;
    }

    private Boolean setAddressFromUser(CartModel cartModel, UserModel currentUser) {
        final AddressModel currentUserDefaultShipmentAddress = currentUser.getDefaultShipmentAddress();
        if (currentUserDefaultShipmentAddress != null)
        {
            final AddressModel supportedDeliveryAddress = getDeliveryAddressModelForCode(
                    currentUserDefaultShipmentAddress.getPk().toString());
            if (supportedDeliveryAddress != null)
            {
                return setDeliveryAddress(cartModel, supportedDeliveryAddress);
            }
        }
        return false;
    }

    private boolean setDeliveryAddress(CartModel cartModel, AddressModel supportedDeliveryAddress) {
        final CommerceCheckoutParameter parameter = createCommerceCheckoutParameter(cartModel, true);
        parameter.setAddress(supportedDeliveryAddress);
        parameter.setIsDeliveryAddress(false);
        return getCommerceCheckoutService().setDeliveryAddress(parameter);
    }
}
