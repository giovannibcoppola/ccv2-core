package com.viking.facades.product.converters.populator;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;


/**
 * @author Chiranjit Chakraborty
 */
@UnitTest
public class VikingProductDocumentsPopulatorTest {

    @InjectMocks
    private VikingProductDocumentsPopulator vikingProductDocumentsPopulator;

    @Mock
    private ProductData productData;

    @Mock
    private ProductModel productModel;

    @Mock
    private ProductChannelDataService productChannelDataService;

    @Mock
    private Converter<MediaModel, MediaData> mediaConverter;

    @SuppressWarnings("unused")
    @Mock
    private Populator<ProductModel, ProductData> vikingProductImageFilesPopulator;

    @SuppressWarnings("unused")
    @Mock
    private Populator<ProductModel, ProductData> vikingProductVideosPopulator;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulate_Base_NoChannelData() {
        when(productChannelDataService.getFilteredMediasForChannel(any())).thenReturn(Collections.emptyList());

        vikingProductDocumentsPopulator.populate(productModel, productData);

        verify(productData, never()).setManuals(any());
        verify(productData, never()).setDatasheets(any());
        verify(productData, never()).setBrochures(any());
        verify(productData, never()).setAdditionalFiles(any());
        verify(productData, never()).setCertificates(any());
        verify(productData, never()).setVendorDatasheets(any());
        verify(productData, never()).setDrawings(any());
    }

    @Test
    public void testPopulate_Base_ChannelData() {
        List<MediaModel> medias = Arrays.asList(mock(MediaModel.class));
        when(productChannelDataService.getFilteredMediasForChannel(any())).thenReturn(medias);

        vikingProductDocumentsPopulator.populate(productModel, productData);

        verify(productData, times(1)).setManuals(any());
        verify(productData, times(1)).setDatasheets(any());
        verify(productData, times(1)).setBrochures(any());
        verify(productData, times(1)).setAdditionalFiles(any());
        verify(productData, times(1)).setCertificates(any());
        verify(productData, times(1)).setVendorDatasheets(any());
        verify(productData, times(1)).setDrawings(any());
    }

    @Test
    public void testPopulate_Variant_ChannelData() {
        List<MediaModel> medias = Arrays.asList(mock(MediaModel.class));
        ProductModel baseProductModel = mock(ProductModel.class);
        when(baseProductModel.getManuals()).thenReturn(medias);
        when(baseProductModel.getDataSheets()).thenReturn(medias);
        when(baseProductModel.getBrochures()).thenReturn(medias);
        when(baseProductModel.getAdditionalFiles()).thenReturn(medias);
        when(baseProductModel.getCertificates()).thenReturn(medias);
        when(baseProductModel.getVendorDataSheets()).thenReturn(medias);
        when(baseProductModel.getDrawings()).thenReturn(medias);

        ERPVariantProductModel erpVariantProductModel = mock(ERPVariantProductModel.class);
        when(erpVariantProductModel.getManuals()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getDataSheets()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getBrochures()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getAdditionalFiles()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getCertificates()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getVendorDataSheets()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getDrawings()).thenReturn(Collections.emptyList());
        when(erpVariantProductModel.getBaseProduct()).thenReturn(baseProductModel);

        when(productChannelDataService.getFilteredMediasForChannel(medias)).thenReturn(medias);
        when(productChannelDataService.getFilteredMediasForChannel(Collections.emptyList())).thenReturn(Collections.emptyList());

        vikingProductDocumentsPopulator.populate(erpVariantProductModel, productData);

        verify(productData, times(1)).setManuals(any());
        verify(productData, times(1)).setDatasheets(any());
        verify(productData, times(1)).setBrochures(any());
        verify(productData, times(1)).setAdditionalFiles(any());
        verify(productData, times(1)).setCertificates(any());
        verify(productData, times(1)).setVendorDatasheets(any());
        verify(productData, times(1)).setDrawings(any());
    }

}
