package com.viking.facades.populators;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.accountsummaryaddon.document.data.MediaData;
import de.hybris.platform.core.model.media.MediaModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@UnitTest
public class VikingMediaAltTextPopulatorTest {

    @Mock
    MediaModel source;

    @InjectMocks
    VikingMediaAltTextPopulator vikingMediaPopulator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulator(){
        final String theAltText = "test";
        when(source.getAltTextLocalized()).thenReturn(theAltText);
        MediaData target = new MediaData();

        vikingMediaPopulator.populate(source, target);

        assertEquals(theAltText, target.getAltText());
    }

}