package com.viking.facades.product.converters.populator;

import com.viking.core.model.LongChannelTextModel;
import com.viking.core.model.SimpleChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author javier.gomez
 */
@UnitTest
public class VikingProductChannelTextPopulatorTest {

    @InjectMocks
    private VikingProductChannelTextPopulator vikingProductChannelTextPopulator;

    @Mock
    private ProductData productData;

    @Mock
    private ProductChannelDataService productChannelDataService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulate_NoChannelData() {
        ProductModel productModel = mock(ProductModel.class);

        when(productChannelDataService.getTeaserForChannel(productModel)).thenReturn(Optional.empty());
        when(productChannelDataService.getLongDescriptionForChannel(productModel)).thenReturn(Optional.empty());

        vikingProductChannelTextPopulator.populate(productModel, productData);

        verify(productData, never()).setTeaser(any());
        verify(productData, never()).setLongDescription(any());
    }

    @Test
    public void testPopulate_ChannelData() {
        SimpleChannelTextModel simpleChannelTextModel = mock(SimpleChannelTextModel.class);
        LongChannelTextModel longChannelTextModel = mock(LongChannelTextModel.class);

        ProductModel productModel = mock(ProductModel.class);

        when(productChannelDataService.getTeaserForChannel(productModel)).thenReturn(Optional.of("text"));
        when(productChannelDataService.getLongDescriptionForChannel(productModel)).thenReturn(Optional.of("text"));

        vikingProductChannelTextPopulator.populate(productModel, productData);

        verify(productData, times(1)).setTeaser(any());
        verify(productData, times(1)).setLongDescription(any());
    }
}
