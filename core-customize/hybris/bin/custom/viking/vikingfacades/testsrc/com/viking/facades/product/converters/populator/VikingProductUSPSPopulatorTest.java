package com.viking.facades.product.converters.populator;

import com.viking.core.model.USPModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.*;

/**
 * @author Chiranjit Chakraborty
 */
@UnitTest
public class VikingProductUSPSPopulatorTest {

    @InjectMocks
    private VikingProductUSPSPopulator vikingProductUSPSPopulator;

    @Mock
    private ProductData productData;

    @Mock
    private ProductModel productModel;

    @Mock
    private USPModel uspModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulate_no_usp() {
        vikingProductUSPSPopulator.populate(productModel,productData);

        verify(productData, never()).setUsps(anyList());
    }

    @Test
    public void testPopulate_has_usp() {
        when(productModel.getUsps()).thenReturn(Arrays.asList(uspModel));
        when(uspModel.getUspText()).thenReturn("test");

        vikingProductUSPSPopulator.populate(productModel,productData);

        verify(productData, times(1)).setUsps(eq(Arrays.asList("test")));
    }
}
