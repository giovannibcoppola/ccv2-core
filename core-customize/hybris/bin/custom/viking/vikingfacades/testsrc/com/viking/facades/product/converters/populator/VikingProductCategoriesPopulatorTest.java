package com.viking.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.helper.ProductAndCategoryHelper;
import de.hybris.platform.commerceservices.product.CommerceProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelContextUtils;
import org.apache.solr.common.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import org.junit.Assert;

/**
 * Unit test to cover populator {@link VikingProductCategoriesPopulator}
 *
 * @author javier.gomez
 */
@UnitTest
public class VikingProductCategoriesPopulatorTest {

    @InjectMocks
    private VikingProductCategoriesPopulator vikingProductCategoriesPopulator;

    @Mock
    private ProductAndCategoryHelper productAndCategoryHelper;

    @Mock
    private CommerceCategoryService commerceCategoryService;

    @Mock
    private CommerceProductService commerceProductService;

    @Mock
    private Converter<CategoryModel, CategoryData> categoryConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProductCategoryPopulator_ProductWithCategory4Level() {
        ProductData productData = new ProductData();

        ProductModel productModel = mock(ProductModel.class);
        CategoryModel mainCategory = mock(CategoryModel.class);

        when(productModel.getSupercategories()).thenReturn(Arrays.asList(mainCategory));

        when(productAndCategoryHelper.isValidProductCategory(mainCategory)).thenReturn(Boolean.TRUE);

        Collection<List<CategoryModel>> collection = getPathCategories(4);
        when(commerceCategoryService.getPathsForCategory(any())).thenReturn(collection);

        vikingProductCategoriesPopulator.populate(productModel, productData);

        Assert.assertEquals("Category Level 3", productData.getLevel3categoryName());
    }

    @Test
    public void testProductCategoryPopulator_ProductWithCategory2Level() {
        ProductData productData = new ProductData();

        ProductModel productModel = mock(ProductModel.class);
        CategoryModel mainCategory = mock(CategoryModel.class);

        when(productModel.getSupercategories()).thenReturn(Arrays.asList(mainCategory));

        when(productAndCategoryHelper.isValidProductCategory(mainCategory)).thenReturn(Boolean.TRUE);

        Collection<List<CategoryModel>> collection = getPathCategories(2);
        when(commerceCategoryService.getPathsForCategory(any())).thenReturn(collection);

        vikingProductCategoriesPopulator.populate(productModel, productData);

        Assert.assertEquals("Category Level 2", productData.getLevel3categoryName());
    }

    @Test
    public void testProductCategoryPopulator_ProductWithoutCategories() {
        ProductData productData = new ProductData();

        ProductModel productModel = mock(ProductModel.class);

        when(productModel.getSupercategories()).thenReturn(Collections.emptyList());

        vikingProductCategoriesPopulator.populate(productModel, productData);

        Assert.assertTrue(StringUtils.isEmpty(productData.getLevel3categoryName()));
    }

    private Collection<List<CategoryModel>> getPathCategories(int numLevelCategory) {
        List<CategoryModel> categoryModels = new ArrayList<>();

        final LocaleProvider localeProvider = mock(LocaleProvider.class);
        final Locale locale = Locale.ENGLISH;
        when(localeProvider.getCurrentDataLocale()).thenReturn(locale);

        for(int level=1;level<=numLevelCategory;level++) {
            CategoryModel categoryLevel = new CategoryModel();
            ((ItemModelContextImpl) ModelContextUtils.getItemModelContext(categoryLevel)).setLocaleProvider(localeProvider);
            categoryLevel.setName("Category Level "+level);
            categoryModels.add(categoryLevel);
        }

        return Arrays.asList(categoryModels);
    }
}
