package com.viking.facades.integration.impl;

import com.viking.core.integration.servicenow.data.VesselEquipmentData;
import com.viking.core.integration.servicenow.data.VesselEquipmentDataWrapper;
import com.viking.core.integration.servicenow.service.VikingVesselServiceNowService;
import com.viking.facades.integration.data.VikingEquipmentData;
import com.viking.facades.integration.data.VikingEquipmentDataWrapperFrontEnd;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VikingVesselFacadeImplTest {

    @InjectMocks
    private  VikingVesselFacadeImpl vikingVesselFacade;

    @Mock
    private VikingVesselServiceNowService vikingVesselServiceNowService;
    @Mock
    private Converter serviceNowVesselDataConverter;
    @Mock
    private Converter serviceNowVikingEquipmentDataConverter;
    @Mock
    private Converter vikingVesselModelToDataConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetVesselEquipmentDataMap() throws IOException {
        final String customerId = "123456'";
        final String imo = "654321'";
        final VesselEquipmentData vesselEquipmentDataRAFT = new VesselEquipmentData();
        vesselEquipmentDataRAFT.setModelCategory("RAFT");
        final VesselEquipmentData vesselEquipmentDataMFS = new VesselEquipmentData();
        vesselEquipmentDataMFS.setModelCategory("MFS");
        final VesselEquipmentDataWrapper wrapper = new VesselEquipmentDataWrapper();
        wrapper.setEquipmentDataList(Arrays.asList(vesselEquipmentDataRAFT, vesselEquipmentDataMFS));

        when(vikingVesselServiceNowService.getVesselEquipmentData(imo, customerId)).thenReturn(wrapper);
        when(serviceNowVikingEquipmentDataConverter.convertAll(anyList())).thenReturn(Arrays.asList(mock(VikingEquipmentData.class)));

        final VikingEquipmentDataWrapperFrontEnd result = vikingVesselFacade.getVikingEquipmentDataListMap(imo, customerId);

        Assert.assertEquals(2, result.getEquipmentMap().size());
        Assert.assertEquals(1, result.getEquipmentMap().get("RAFT").size());
        Assert.assertEquals(1, result.getEquipmentMap().get("MFS").size());
    }
}

