package com.viking.facades.product.converters.populator;

import com.viking.core.enums.ApprovalAuthority;
import com.viking.core.model.USPModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Set;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

/**
 * @author Chiranjit Chakraborty
 */
@UnitTest
public class VikingProductApprovalPopulatorTest {

    @InjectMocks
    private VikingProductApprovalPopulator vikingProductApprovalPopulator;

    @Mock
    private ProductData productData;

    @Mock
    private ProductModel productModel;

    @Mock
    private ApprovalAuthority approvalAuthority;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulate_no_usp() {
        vikingProductApprovalPopulator.populate(productModel,productData);

        verify(productData, never()).setApprovalAuthorities(anyList());
    }

    @Test
    public void testPopulate_has_usp() {
        when(productModel.getApprovalAuthorities()).thenReturn(Set.of(approvalAuthority));
        when(approvalAuthority.getCode()).thenReturn("test");

        vikingProductApprovalPopulator.populate(productModel,productData);

        verify(productData, times(1)).setApprovalAuthorities(eq(Arrays.asList("test")));
    }
}
