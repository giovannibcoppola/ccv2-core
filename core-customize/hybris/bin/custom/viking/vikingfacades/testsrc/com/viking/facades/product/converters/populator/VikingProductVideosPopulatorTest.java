package com.viking.facades.product.converters.populator;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author javier.gomez
 */
@UnitTest
public class VikingProductVideosPopulatorTest {

    @InjectMocks
    private VikingProductVideosPopulator vikingProductVideosPopulator;

    @Mock
    private ProductChannelDataService productChannelDataService;

    @Mock
    private Converter<MediaModel, ImageData> imageConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testNoProductChannelData() {

        when(productChannelDataService.getFilteredMediasForChannel(any())).thenReturn(Collections.emptyList());

        ProductData productData = mock(ProductData.class);
        ProductModel productModel = mock(ProductModel.class);

        vikingProductVideosPopulator.populate(productModel, productData);

        verify(productData, times(0)).setVideos((List<ImageData>) anyCollection());
    }

    @Test
    public void testPopulateVideo() {
        String videoYoutubeId = "123123123";
        MediaModel video = mock(MediaModel.class);
        when(video.getYoutubeVideoId()).thenReturn(videoYoutubeId);

        when(productChannelDataService.getFilteredMediasForChannel(any())).thenReturn(Arrays.asList(video));

        ImageData imageData = new ImageData();
        when(imageConverter.convert(video)).thenReturn(imageData);

        ProductData productData = new ProductData();
        vikingProductVideosPopulator.populate(mock(ProductModel.class), productData);

        Assert.assertTrue(productData.getVideos().size() == 1);
        Assert.assertEquals("thumbnail", productData.getVideos().get(0).getFormat());
        Assert.assertEquals(ImageDataType.GALLERY, productData.getVideos().get(0).getImageType());
        Assert.assertEquals(0, productData.getVideos().get(0).getGalleryIndex().intValue());
        Assert.assertEquals(videoYoutubeId, productData.getVideos().get(0).getYoutubeVideoId());
    }
}
