package com.viking.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

/**
 * @author javier.gomez
 */
@UnitTest
public class VikingProductMetadescriptionPopulatorTest {

    @InjectMocks
    private VikingProductMetadescriptionPopulator vikingProductMetadescriptionPopulator;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPopulatorMetadescription() {
        String metadescription = "Product Metadescription";
        ProductModel productModel = mock(ProductModel.class);
        when(productModel.getMetadescription()).thenReturn(metadescription);

        ProductData productData = new ProductData();
        vikingProductMetadescriptionPopulator.populate(productModel, productData);

        Assert.assertEquals(productModel.getMetadescription(), productData.getMetadescription());
    }
}
