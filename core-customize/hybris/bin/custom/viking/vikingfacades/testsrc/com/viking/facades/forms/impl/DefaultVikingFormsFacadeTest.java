package com.viking.facades.forms.impl;

import com.viking.core.model.NotificationPrefillModel;
import com.viking.core.service.VikingNotificationPrefillService;
import com.viking.erpservices.service.VikingFormsPreFillDataService;
import com.viking.erpservices.service.VikingFormsSubmitService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVikingFormsFacadeTest {

    @InjectMocks
    private DefaultVikingFormsFacade testObj;

    @Mock
    private VikingFormsSubmitService vikingFormsSubmitService;

    @Mock
    private VikingNotificationPrefillService vikingNotificationPrefillService;

    @Mock
    private ModelService modelService;

    @Before
    public void setUp(){
        testObj =  new DefaultVikingFormsFacade();
        MockitoAnnotations.initMocks(this);
        NotificationPrefillModel not = new NotificationPrefillModel();
        Mockito.when(modelService.create(NotificationPrefillModel.class)).thenReturn(not);
    }

    @Test
    public void submitNotificationYFormToSAP() {
        // Given
        final String xml = "SomeXML";
        final String notificationId = "SomeXML";
        final String serviceStation = "SomeXML";

        // When
        testObj.submitNotificationYFormToSAP(notificationId,serviceStation,xml);

        // Then
        verify(vikingFormsSubmitService).submitYFormDataToSap(notificationId,serviceStation,xml);
    }
}