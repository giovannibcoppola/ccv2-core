package com.viking.facades.product.converters.populator;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.anyCollection;
import static org.mockito.Mockito.*;

/**
 * @author Chiranjit Chakraborty
 */
@UnitTest
public class VikingProductImageFilesPopulatorTest {
    private static final String MEDIA_FORMAT_1 = "thumb";
    private static final String MEDIA_FORMAT_2 = "zoom";

    private List<String> imageFormats;
    private VikingProductImageFilesPopulator vikingProductImageFilesPopulator;
    @Mock
    private ProductChannelDataService productChannelDataService;

    @Mock
    private MediaContainerModel mediaContainerModel;
    @Mock
    private ProductData productData;
    @Mock
    private ProductModel productModel;
    @Mock
    private MediaService mediaService;
    @Mock
    private MediaContainerService mediaContainerService;
    @Mock
    private ImageFormatMapping imageFormatMapping;
    @Mock
    private ModelService modelService;

    @Mock
    private Converter<MediaModel, ImageData> imageConverter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        imageFormats = new ArrayList<>();
        imageFormats.add(MEDIA_FORMAT_1);
        imageFormats.add(MEDIA_FORMAT_2);
        vikingProductImageFilesPopulator = new VikingProductImageFilesPopulator();
        vikingProductImageFilesPopulator.setModelService(modelService);
        vikingProductImageFilesPopulator.setImageConverter(imageConverter);
        vikingProductImageFilesPopulator.setImageFormatMapping(imageFormatMapping);
        vikingProductImageFilesPopulator.setImageFormats(imageFormats);
        vikingProductImageFilesPopulator.setMediaContainerService(mediaContainerService);
        vikingProductImageFilesPopulator.setMediaService(mediaService);
        vikingProductImageFilesPopulator.setProductChannelDataService(productChannelDataService);
    }

    @Test
    public void testPopulate_Base_has_ChannelData() {
        when(productChannelDataService.getFilteredMediaContainersForChannel(any())).thenReturn(Arrays.asList(mediaContainerModel));
        when(productChannelDataService.getTeaserForChannel(productModel)).thenReturn(Optional.empty());

        vikingProductImageFilesPopulator.populate(productModel, productData);

        verify(productData, times(1)).setImages(anyCollection());
    }

}
