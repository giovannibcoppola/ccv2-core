/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.vikingsaporderfulfilment.actions;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.sap.sapmodel.enums.ConsignmentEntryStatus;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SetCompletionStatusActionTest {
    @InjectMocks
    private SetCompletionStatusAction testObj;

    @Mock
    private ModelService modelServiceMock;

    @Mock
    private OrderProcessModel processMock;
    @Mock
    private OrderModel orderMock;
    @Mock
    private ConsignmentModel consignmentMock;
    @Mock
    private ConsignmentEntryModel shippedConsignmentEntryMock, notShippedConsignmentEntryMock;

    @Before
    public void setUp () {
        when(shippedConsignmentEntryMock.getStatus()).thenReturn(ConsignmentEntryStatus.SHIPPED);
        when(notShippedConsignmentEntryMock.getStatus()).thenReturn(ConsignmentEntryStatus.WAITING);
    }

    @Test
    public void testOrderStatusIsCompletedAndShippedWhenAllProductsIsShipped() {
        // Given
        when(processMock.getOrder()).thenReturn(orderMock);
        when(orderMock.getConsignments()).thenReturn(Sets.newSet(consignmentMock));
        when(orderMock.getDeliveryStatus()).thenReturn(DeliveryStatus.SHIPPED);
        when(consignmentMock.getConsignmentEntries()).thenReturn(Sets.newSet(shippedConsignmentEntryMock));
        when(consignmentMock.getStatus()).thenReturn(ConsignmentStatus.SHIPPED);
        when(shippedConsignmentEntryMock.getStatus()).thenReturn(ConsignmentEntryStatus.SHIPPED);

        // When
        final String result = testObj.execute(processMock);

        // Then
        verify(consignmentMock).setStatus(ConsignmentStatus.SHIPPED);
        verify(orderMock).setStatus(OrderStatus.COMPLETED);
        verify(orderMock).setDeliveryStatus(DeliveryStatus.SHIPPED);
        verify(modelServiceMock).save(consignmentMock);
        verify(modelServiceMock, atLeastOnce()).save(orderMock);
        assertEquals("OK", result);
    }

    @Test
    public void testOrderStatusIsOpenAndPartlyShippedWhenNotAllProductsIsShipped() {
        // Given
        when(processMock.getOrder()).thenReturn(orderMock);
        when(orderMock.getConsignments()).thenReturn(Sets.newSet(consignmentMock));
        when(consignmentMock.getConsignmentEntries()).thenReturn(Sets.newSet(shippedConsignmentEntryMock, notShippedConsignmentEntryMock));
        when(consignmentMock.getStatus()).thenReturn(ConsignmentStatus.WAITING);

        // When
        final String result = testObj.execute(processMock);

        // Then
        verify(consignmentMock).setStatus(ConsignmentStatus.WAITING);
        verify(orderMock).setStatus(OrderStatus.PARTSHIPPED);
        verify(orderMock).setDeliveryStatus(DeliveryStatus.PARTSHIPPED);
        verify(modelServiceMock).save(consignmentMock);
        verify(modelServiceMock, atLeastOnce()).save(orderMock);
        assertEquals("PARTIAL", result);
    }
}
