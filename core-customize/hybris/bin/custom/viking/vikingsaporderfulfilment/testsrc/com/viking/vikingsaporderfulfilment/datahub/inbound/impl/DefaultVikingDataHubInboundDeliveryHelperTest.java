package com.viking.vikingsaporderfulfilment.datahub.inbound.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.sap.sapmodel.enums.ConsignmentEntryStatus;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVikingDataHubInboundDeliveryHelperTest {

    private static final String ORDER_CODE = "12345678";
    private static final String PRODUCT_CODE = "111111111";

    @InjectMocks
    private DefaultVikingDataHubInboundDeliveryHelper testObj;

    @Mock
    private FlexibleSearchService flexibleSearchServiceMock;
    @Mock
    private ModelService modelServiceMock;
    @Mock
    private BusinessProcessService businessProcessServiceMock;

    @Mock
    private OrderModel orderMock;
    @Mock
    private ConsignmentModel consignmentMock;
    @Mock (answer = Answers.RETURNS_DEEP_STUBS)
    private ConsignmentEntryModel consignmentEntryMock;

    @Test
    public void processDeliveryAndGoodsIssue() {
        // Given
        when(flexibleSearchServiceMock.searchUnique(any(FlexibleSearchQuery.class))).thenReturn(orderMock);
        when(orderMock.getCode()).thenReturn(ORDER_CODE);
        when(orderMock.getConsignments()).thenReturn(Sets.newSet(consignmentMock));
        when(consignmentMock.getConsignmentEntries()).thenReturn(Sets.newSet(consignmentEntryMock));
        when(consignmentEntryMock.getOrderEntry().getProduct().getCode()).thenReturn(PRODUCT_CODE);

        // When
        testObj.processDeliveryAndGoodsIssue(ORDER_CODE, "warehouse", "01-09-2018", PRODUCT_CODE, "1");

        // Then
        verify(modelServiceMock).saveAll();
        verify(businessProcessServiceMock).triggerEvent(DataHubInboundConstants.GOODS_ISSUE_EVENTNAME_PREFIX + ORDER_CODE);
        verify(consignmentEntryMock).setStatus(ConsignmentEntryStatus.SHIPPED);
        verify(consignmentEntryMock).setShippedQuantity(1L);
    }

    @Test
    public void processDeliveryAndGoodsIssueWihtNullDate() {
        // Given
        when(flexibleSearchServiceMock.searchUnique(any(FlexibleSearchQuery.class))).thenReturn(orderMock);
        when(orderMock.getCode()).thenReturn(ORDER_CODE);
        when(orderMock.getConsignments()).thenReturn(Sets.newSet(consignmentMock));
        when(consignmentMock.getConsignmentEntries()).thenReturn(Sets.newSet(consignmentEntryMock));
        when(consignmentEntryMock.getOrderEntry().getProduct().getCode()).thenReturn(PRODUCT_CODE);

        // When
        testObj.processDeliveryAndGoodsIssue(ORDER_CODE, "warehouse", "null", PRODUCT_CODE, "1");

        // Then
        verify(modelServiceMock).saveAll();
        verify(businessProcessServiceMock).triggerEvent(DataHubInboundConstants.GOODS_ISSUE_EVENTNAME_PREFIX + ORDER_CODE);
        verify(consignmentEntryMock).setStatus(ConsignmentEntryStatus.SHIPPED);
        verify(consignmentEntryMock).setShippedQuantity(1L);
    }
}