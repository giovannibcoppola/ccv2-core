package com.viking.vikingsaporderfulfilment.inbound.events;

import com.viking.vikingsaporderfulfilment.datahub.inbound.VikingDataHubInboundDeliveryHelper;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.sap.orderexchange.inbound.events.DataHubTranslator;
import org.apache.commons.lang.StringUtils;

public class VikingDataHubGoodsIssueTranslator extends DataHubTranslator<VikingDataHubInboundDeliveryHelper> {

    private static final String HELPER_BEAN = "vikingDataHubInboundDeliveryHelper";

    public VikingDataHubGoodsIssueTranslator() {
        super(HELPER_BEAN);
    }

    @Override
    public void performImport(final String delivInfo, final Item processedItem) throws ImpExException {
        String orderCode;

        try {
            orderCode = processedItem.getAttribute(DataHubInboundConstants.CODE).toString();
        } catch (final JaloSecurityException e) {
            throw new ImpExException(e);
        }

        if (delivInfo != null && !delivInfo.equals(DataHubInboundConstants.IGNORE)) {
            String[] deliveryInfo = delivInfo.split("#");

            final String warehouseId = deliveryInfo[0];
            final String goodsIssueDate = deliveryInfo[1];
            final String productCode = StringUtils.stripStart(deliveryInfo[2], "0");
            final String quantity = deliveryInfo[3];

            getInboundHelper().processDeliveryAndGoodsIssue(orderCode, warehouseId, goodsIssueDate, productCode, quantity);
        }
    }
}
