/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.vikingsaporderfulfilment.actions;


import com.viking.vikingsaporderfulfilment.datahub.inbound.impl.DefaultVikingDataHubInboundDeliveryHelper;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.sap.sapmodel.constants.GeneratedSapmodelConstants;
import de.hybris.platform.sap.sapmodel.enums.ConsignmentEntryStatus;
import de.hybris.platform.task.RetryLaterException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;


/**
 * Class for setting the Hybris order and consignment status after receiving an goods issue information from the SAP ERP
 * backend
 */
public class SetCompletionStatusAction extends AbstractAction<OrderProcessModel> {

    private static final Logger LOG = LogManager.getLogger(SetCompletionStatusAction.class);

    @Override
    public String execute(final OrderProcessModel process) {
        // Just wait 5 seconds to continue to be 110% sure the last step of the process is completed. This is due to distributed impex import
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            LOG.debug(exception);
        }

        final OrderModel order = process.getOrder();
        setConsignmentStatus(order);
        setOrderStatus(order);
        save(order);

        return DeliveryStatus.SHIPPED.equals(order.getDeliveryStatus()) ? Transition.OK.toString() : Transition.PARTIAL.toString();
    }

    protected void setOrderStatus(final OrderModel order) {
        boolean allShipped = order.getConsignments().stream().allMatch(consignment -> ConsignmentStatus.SHIPPED.equals(consignment.getStatus()));

        if (allShipped) {
            setOrderStatus(order, OrderStatus.COMPLETED);
            order.setDeliveryStatus(DeliveryStatus.SHIPPED);
        } else {
            setOrderStatus(order, OrderStatus.PARTSHIPPED);
            order.setDeliveryStatus(DeliveryStatus.PARTSHIPPED);
        }
    }

    protected void setConsignmentStatus(final OrderModel order) {
        for (final ConsignmentModel consignment : order.getConsignments()) {
            boolean allShipped = consignment.getConsignmentEntries().stream().allMatch(entry -> ConsignmentEntryStatus.SHIPPED.equals(entry.getStatus()));

            if (allShipped) {
                consignment.setStatus(ConsignmentStatus.SHIPPED);
            } else {
                consignment.setStatus(ConsignmentStatus.WAITING);
            }
            save(consignment);
        }
    }

    @Override
    public Set<String> getTransitions() {
        return Transition.getStringValues();
    }

    public enum Transition {
        OK, PARTIAL;

        public static Set<String> getStringValues() {
            final Set<String> res = new HashSet<>();

            for (final Transition t : Transition.values()) {
                res.add(t.toString());
            }
            return res;
        }
    }
}
