/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.vikingsaporderfulfilment.constants;

/**
 * Global class for all Vikingsaporderfulfilment constants. You can add global constants for your extension into this class.
 */
public final class VikingsaporderfulfilmentConstants extends GeneratedVikingsaporderfulfilmentConstants
{
	@SuppressWarnings("javadoc")
	public static final String EXTENSIONNAME = "vikingsaporderfulfilment";

	@SuppressWarnings("javadoc")
	public static final String ORDER_PROCESS_NAME = "sap-order-process";
	public static final String ENQUIRY_PROCESS_NAME = "sap-enquiry-process";

	private VikingsaporderfulfilmentConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
