package com.viking.vikingsaporderfulfilment.datahub.inbound;

import de.hybris.platform.sap.orderexchange.datahub.inbound.DataHubInboundDeliveryHelper;

/**
 * Viking specific methods.
 */
public interface VikingDataHubInboundDeliveryHelper extends DataHubInboundDeliveryHelper {

    /**
     * Update Hybris consingment and orderstatus according to the shipped consigment entries
     * @param orderCode
     * @param warehouseId
     * @param goodsIssueDate
     * @param productCode
     * @param quantity
     */
    void processDeliveryAndGoodsIssue(String orderCode, String warehouseId, String goodsIssueDate, String productCode, String quantity);
}
