package com.viking.vikingsaporderfulfilment.actions;

import de.hybris.platform.orderprocessing.events.SendDeliveryMessageEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class SendOrderShippedNotificationAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = LogManager.getLogger(SendOrderShippedNotificationAction.class);

    private EventService eventService;

    @Override
    public void executeAction(OrderProcessModel process) {
        final ConsignmentProcessModel consignmentProcess = modelService.create(ConsignmentProcessModel.class);
        // In current asynchronous order flow only one consignment is created to each order.
        consignmentProcess.setConsignment(process.getOrder().getConsignments().iterator().next());
        consignmentProcess.setParentProcess(process);
        consignmentProcess.setCode("ConsignmentProcess-" + process.getCode());
        modelService.save(consignmentProcess);
        eventService.publishEvent(new SendDeliveryMessageEvent(consignmentProcess));
        LOG.debug("SendDeliveryMessageEvent published for order " + process.getOrder().getCode());
    }

    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }
}
