package com.viking.vikingsaporderfulfilment.datahub.inbound.impl;

import com.viking.vikingsaporderfulfilment.datahub.inbound.VikingDataHubInboundDeliveryHelper;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.sap.orderexchange.constants.DataHubInboundConstants;
import de.hybris.platform.sap.orderexchange.datahub.inbound.impl.DefaultDataHubInboundDeliveryHelper;
import de.hybris.platform.sap.sapmodel.enums.ConsignmentEntryStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * {@inheritDoc}
 */
public class DefaultVikingDataHubInboundDeliveryHelper extends DefaultDataHubInboundDeliveryHelper implements VikingDataHubInboundDeliveryHelper {

    private static final Logger LOG = LogManager.getLogger(DefaultVikingDataHubInboundDeliveryHelper.class);

    @Override
    public void processDeliveryAndGoodsIssue(final String orderCode, final String warehouseId, final String issueDate, final String productCode, final String quantity) {
        final OrderModel order = readOrder(orderCode);

        if (order.getConsignments().isEmpty()) {
            createConsignmentAndPopulate(warehouseId, order);
        }

        order.getConsignments().forEach(consignment -> mapDeliveryToConsignment(issueDate, consignment, productCode, quantity));

        getModelService().saveAll();
        waitForBusinessProcessToBeReady(order);
        getBusinessProcessService().triggerEvent(DataHubInboundConstants.GOODS_ISSUE_EVENTNAME_PREFIX + order.getCode());
    }

    private void waitForBusinessProcessToBeReady(OrderModel order) {
        try {
            OrderProcessModel processExample = new OrderProcessModel();
            processExample.setOrder(order);
            List<OrderProcessModel> processes = getFlexibleSearchService().getModelsByExample(processExample);
            // If process is waiting then proceed. If process is running then wait
            Optional<OrderProcessModel> optionalProcess = processes.stream()
                    .filter(process -> process.getCode().contains("sap-order-process-" + order.getCode()) && process.getProcessState().equals(ProcessState.RUNNING))
                    .findFirst();

            if (optionalProcess.isPresent()) {
                Thread.sleep(5000);
                waitForBusinessProcessToBeReady(order);
            }
        } catch (InterruptedException ex) {
            LOG.debug(ex);
        }
    }

    private void mapDeliveryToConsignment(final String issueDate, final ConsignmentModel consignment, final String productCode, final String quantity) {
        if (!StringUtils.isEmpty(issueDate) && !"null".equalsIgnoreCase(issueDate)) {
            consignment.setShippingDate(convertStringToDate(issueDate));
        }

        Optional<ConsignmentEntryModel> entry = consignment.getConsignmentEntries().stream().filter(e -> e.getOrderEntry().getProduct().getCode().equals(productCode)).findFirst();

        entry.ifPresent(ce -> {
            final Long shippedQuantity = ce.getShippedQuantity();

            if (Objects.isNull(shippedQuantity)) {
                LOG.debug("Setting shipped quantity to " + (long) Double.parseDouble(quantity));
                ce.setShippedQuantity((long) Double.parseDouble(quantity));
            } else {
                LOG.debug("Setting shipped quantity to sum " + Long.sum(shippedQuantity, (long) Double.parseDouble(quantity)));
                ce.setShippedQuantity(Long.sum(shippedQuantity, (long) Double.parseDouble(quantity)));
            }

            if (ce.getShippedQuantity().compareTo(ce.getQuantity()) >= 0) {
                ce.setStatus(ConsignmentEntryStatus.SHIPPED);
            }

            getModelService().save(ce);
        });
    }
}
