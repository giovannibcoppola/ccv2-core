package com.viking.vikingsaporderfulfilment.enquiry.actions;

import com.viking.core.event.OrderNotificationForSalesRespEvent;
import com.viking.core.event.VikingEnquiryNotificationSalesEvent;
import com.viking.vikingsaporderfulfilment.actions.SendOrderPlacesNotificationForSalesRespAction;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class SendEnquiryPlacesNotificationForSalesRespAction extends AbstractProceduralAction<OrderProcessModel> {

    private static final Logger LOG = LogManager.getLogger(SendOrderPlacesNotificationForSalesRespAction.class);

    private EventService eventService;

    @Override
    public void executeAction(OrderProcessModel process) {
        eventService.publishEvent(new VikingEnquiryNotificationSalesEvent(process));
        LOG.debug("VikingEnquiryNotificationSalesEvent published for order " + process.getOrder().getCode());
    }

    @Required
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }
}
