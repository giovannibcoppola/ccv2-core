/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.viking.constants;

/**
 * Global class for all Vikingearlyloginaddon constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class VikingearlyloginaddonConstants extends GeneratedVikingearlyloginaddonConstants
{
	public static final String EXTENSIONNAME = "vikingearlyloginaddon";

	private VikingearlyloginaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
