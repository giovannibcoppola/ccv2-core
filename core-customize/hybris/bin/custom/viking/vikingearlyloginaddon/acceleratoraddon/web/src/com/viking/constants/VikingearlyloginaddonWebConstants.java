/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.viking.constants;

/**
 * Global class for all Vikingearlyloginaddon web constants. You can add global constants for your extension into this class.
 */
public final class VikingearlyloginaddonWebConstants // NOSONAR
{
    // implement here constants used by this extension
    public static final String ADD_ON_PREFIX = "addon:";
    public static final String VIEW_PAGE_PREFIX = ADD_ON_PREFIX + "/" + VikingearlyloginaddonConstants.EXTENSIONNAME;
    private VikingearlyloginaddonWebConstants() {
        //empty to avoid instantiating this constant class
    }
}
