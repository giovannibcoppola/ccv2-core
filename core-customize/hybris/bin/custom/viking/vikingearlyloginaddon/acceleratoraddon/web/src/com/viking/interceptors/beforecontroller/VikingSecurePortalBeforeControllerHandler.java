package com.viking.interceptors.beforecontroller;

import de.hybris.platform.secureportaladdon.interceptors.SecurePortalBeforeControllerHandler;
import org.apache.commons.lang3.StringUtils;

/**
 * A complete site url redirection was breaking the smartedit. So, fixing that by redirecting it to a relative url.
 */
public class VikingSecurePortalBeforeControllerHandler extends SecurePortalBeforeControllerHandler {

    @Override
    protected String getRedirectUrl(final String mapping, final boolean secured, final String otherParameters) {
        final String redirectUrl = super.getRedirectUrl(mapping,secured,otherParameters);
        return StringUtils.isNotEmpty(redirectUrl)?redirectUrl.substring(redirectUrl.indexOf(mapping)):StringUtils.EMPTY;
    }
}