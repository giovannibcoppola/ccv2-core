<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:loginPage pageTitle="${pageTitle}">
    <div class="flex-login-container">
        <cms:pageSlot position="MainContentSlot" var="feature" element="div" class="pad-bottom-pagefooter">
            <cms:component component="${feature}"  element="div" class="login-left-content-component"/>
        </cms:pageSlot>
    </div>
</template:loginPage>
