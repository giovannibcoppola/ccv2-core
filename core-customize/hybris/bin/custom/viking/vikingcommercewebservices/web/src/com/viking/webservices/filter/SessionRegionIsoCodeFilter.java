package com.viking.webservices.filter;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.webservices.context.ContextInformationLoader;

public class SessionRegionIsoCodeFilter extends OncePerRequestFilter {

    private static final Logger LOG = Logger.getLogger(SessionRegionIsoCodeFilter.class);
    private ContextInformationLoader contextInformationLoader;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        if(request != null) {
            LOG.info("VIK-1144: Request not null: " + request.getRequestURI());
            if(request.getSession() != null) {
                LOG.info("VIK-1144: Session not null: " + request.getSession().getCreationTime());
            }
        }

        getContextInformationLoader().setRegionIsoCodeFromRequest(request);
        filterChain.doFilter(request, response);
    }

    protected ContextInformationLoader getContextInformationLoader()
    {
        return contextInformationLoader;
    }

    @Required
    public void setContextInformationLoader(final ContextInformationLoader contextInformationLoader)
    {
        this.contextInformationLoader = contextInformationLoader;
    }
}
