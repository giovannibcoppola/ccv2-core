package com.viking.populators;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

public class VikingCwsProductPopulator extends ProductPopulator {

    private Populator<ProductModel, ProductData> vikingProductChannelTextPopulator;
    private Populator<ProductModel, ProductData> vikingProductModelNumberPopulator;
    private Populator<ProductModel, ProductData> vikingProductCatalogVersionPopulator;
    private Populator<ProductModel, ProductData> vikingProductApprovalStatusPopulator;
    private Populator<ProductModel, ProductData> vikingProductMaterialDescriptionSAPPopulator;
    private Populator<ProductModel, ProductData> vikingProductProductStatusSAPPopulator;
    private Populator<ProductModel, ProductData> vikingProductDocumentsPopulator;
    private Populator<ProductModel, ProductData> vikingProductSegmentsPopulator;
    private Populator<ProductModel, ProductData> vikingProductApprovalPopulator;
    private Populator<ProductModel, ProductData> vikingProductUSPSPopulator;
    private Populator<ProductModel, ProductData> vikingProductStoreListPopulator;
    private Populator<ProductModel, ProductData> vikingProductApplicationsPopulator;
    private Populator<ProductModel, ProductData> vikingProductStandardsPopulator;
    private Populator<ProductModel, ProductData> vikingProductBrandPopulator;
    private Populator<ProductModel, ProductData> vikingProductDefaultTeaserPopulator;
    private Populator<ProductModel, ProductData> vikingProductDefaultLongDescriptionPopulator;
    private Populator<ProductModel, ProductData> vikingProductTeasersPopulator;
    private Populator<ProductModel, ProductData> vikingProductLongDescriptionsPopulator;
    private Populator<ProductModel, ProductData> vikingProductTopLevelCategoryPopulator;



    @Override
    public void populate(ProductModel source, ProductData target) {
        vikingProductChannelTextPopulator.populate(source, target);
        vikingProductModelNumberPopulator.populate(source, target);
        vikingProductCatalogVersionPopulator.populate(source, target);
        vikingProductApprovalStatusPopulator.populate(source, target);
        vikingProductMaterialDescriptionSAPPopulator.populate(source, target);
        vikingProductProductStatusSAPPopulator.populate(source, target);
        vikingProductDocumentsPopulator.populate(source, target);
        vikingProductSegmentsPopulator.populate(source, target);
        vikingProductApprovalPopulator.populate(source, target);
        vikingProductUSPSPopulator.populate(source,target);
        vikingProductStoreListPopulator.populate(source, target);
        vikingProductApplicationsPopulator.populate(source, target);
        vikingProductStandardsPopulator.populate(source, target);
        vikingProductBrandPopulator.populate(source, target);
        vikingProductDefaultTeaserPopulator.populate(source, target);
        vikingProductDefaultLongDescriptionPopulator.populate(source, target);
        vikingProductTeasersPopulator.populate(source, target);
        vikingProductLongDescriptionsPopulator.populate(source, target);
        vikingProductTopLevelCategoryPopulator.populate(source, target);
        super.populate(source, target);
    }

    public Populator<ProductModel, ProductData> getVikingProductChannelTextPopulator() {
        return vikingProductChannelTextPopulator;
    }

    @Required
    public void setVikingProductChannelTextPopulator(Populator<ProductModel, ProductData> vikingProductChannelTextPopulator) {
        this.vikingProductChannelTextPopulator = vikingProductChannelTextPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductModelNumberPopulator() {
        return vikingProductModelNumberPopulator;
    }

    @Required
    public void setVikingProductModelNumberPopulator(Populator<ProductModel, ProductData> vikingProductModelNumberPopulator) {
        this.vikingProductModelNumberPopulator = vikingProductModelNumberPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductCatalogVersionPopulator() {
        return vikingProductCatalogVersionPopulator;
    }

    @Required
    public void setVikingProductCatalogVersionPopulator(Populator<ProductModel, ProductData> vikingProductCatalogVersionPopulator) {
        this.vikingProductCatalogVersionPopulator = vikingProductCatalogVersionPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductApprovalStatusPopulator() {
        return vikingProductApprovalStatusPopulator;
    }

    @Required
    public void setVikingProductApprovalStatusPopulator(Populator<ProductModel, ProductData> vikingProductApprovalStatusPopulator) {
        this.vikingProductApprovalStatusPopulator = vikingProductApprovalStatusPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductMaterialDescriptionSAPPopulator() {
        return vikingProductMaterialDescriptionSAPPopulator;
    }

    @Required
    public void setVikingProductMaterialDescriptionSAPPopulator(Populator<ProductModel, ProductData> vikingProductMaterialDescriptionSAPPopulator) {
        this.vikingProductMaterialDescriptionSAPPopulator = vikingProductMaterialDescriptionSAPPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductProductStatusSAPPopulator() {
        return vikingProductProductStatusSAPPopulator;
    }

    @Required
    public void setVikingProductProductStatusSAPPopulator(Populator<ProductModel, ProductData> vikingProductProductStatusSAPPopulator) {
        this.vikingProductProductStatusSAPPopulator = vikingProductProductStatusSAPPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductDocumentsPopulator() {
        return vikingProductDocumentsPopulator;
    }

    @Required
    public void setVikingProductDocumentsPopulator(Populator<ProductModel, ProductData> vikingProductDocumentsPopulator) {
        this.vikingProductDocumentsPopulator = vikingProductDocumentsPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductSegmentsPopulator() {
        return vikingProductSegmentsPopulator;
    }

    @Required
    public void setVikingProductSegmentsPopulator(Populator<ProductModel, ProductData> vikingProductSegmentsPopulator) {
        this.vikingProductSegmentsPopulator = vikingProductSegmentsPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductApprovalPopulator() {
        return vikingProductApprovalPopulator;
    }

    @Required
    public void setVikingProductApprovalPopulator(Populator<ProductModel, ProductData> vikingProductApprovalPopulator) {
        this.vikingProductApprovalPopulator = vikingProductApprovalPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductUSPSPopulator() {
        return vikingProductUSPSPopulator;
    }

    @Required
    public void setVikingProductUSPSPopulator(Populator<ProductModel, ProductData> vikingProductUSPSPopulator) {
        this.vikingProductUSPSPopulator = vikingProductUSPSPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductStoreListPopulator() {
        return vikingProductStoreListPopulator;
    }

    @Required
    public void setVikingProductStoreListPopulator(Populator<ProductModel, ProductData> vikingProductStoreListPopulator) {
        this.vikingProductStoreListPopulator = vikingProductStoreListPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductApplicationsPopulator() {
        return vikingProductApplicationsPopulator;
    }

    @Required
    public void setVikingProductApplicationsPopulator(Populator<ProductModel, ProductData> vikingProductApplicationsPopulator) {
        this.vikingProductApplicationsPopulator = vikingProductApplicationsPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductStandardsPopulator() {
        return vikingProductStandardsPopulator;
    }

    @Required
    public void setVikingProductStandardsPopulator(Populator<ProductModel, ProductData> vikingProductStandardsPopulator) {
        this.vikingProductStandardsPopulator = vikingProductStandardsPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductBrandPopulator() {
        return vikingProductBrandPopulator;
    }

    @Required
    public void setVikingProductBrandPopulator(Populator<ProductModel, ProductData> vikingProductBrandPopulator) {
        this.vikingProductBrandPopulator = vikingProductBrandPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductDefaultTeaserPopulator() {
        return vikingProductDefaultTeaserPopulator;
    }

    @Required
    public void setVikingProductDefaultTeaserPopulator(Populator<ProductModel, ProductData> vikingProductDefaultTeaserPopulator) {
        this.vikingProductDefaultTeaserPopulator = vikingProductDefaultTeaserPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductDefaultLongDescriptionPopulator() {
        return vikingProductDefaultLongDescriptionPopulator;
    }

    @Required
    public void setVikingProductDefaultLongDescriptionPopulator(Populator<ProductModel, ProductData> vikingProductDefaultLongDescriptionPopulator) {
        this.vikingProductDefaultLongDescriptionPopulator = vikingProductDefaultLongDescriptionPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductTeasersPopulator() {
        return vikingProductTeasersPopulator;
    }

    @Required
    public void setVikingProductTeasersPopulator(Populator<ProductModel, ProductData> vikingProductTeasersPopulator) {
        this.vikingProductTeasersPopulator = vikingProductTeasersPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductLongDescriptionsPopulator() {
        return vikingProductLongDescriptionsPopulator;
    }

    @Required
    public void setVikingProductLongDescriptionsPopulator(Populator<ProductModel, ProductData> vikingProductLongDescriptionsPopulator) {
        this.vikingProductLongDescriptionsPopulator = vikingProductLongDescriptionsPopulator;
    }

    public Populator<ProductModel, ProductData> getVikingProductTopLevelCategoryPopulator() {
        return vikingProductTopLevelCategoryPopulator;
    }

    @Required
    public void setVikingProductTopLevelCategoryPopulator(Populator<ProductModel, ProductData> vikingProductTopLevelCategoryPopulator) {
        this.vikingProductTopLevelCategoryPopulator = vikingProductTopLevelCategoryPopulator;
    }
}
