package com.viking.populators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.catalog.CatalogOption;
import de.hybris.platform.commercefacades.catalog.PageOption;
import de.hybris.platform.commercefacades.catalog.converters.populator.CategoryHierarchyPopulator;
import de.hybris.platform.commercefacades.catalog.data.CategoryHierarchyData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class VikingCwsCategoryHierarchyPopulator extends CategoryHierarchyPopulator {

    @Override
    public void populate(CategoryModel source, CategoryHierarchyData target, Collection<? extends CatalogOption> options, PageOption page) throws ConversionException {
        target.setId(source.getCode());
        target.setName(source.getName());
        target.setLastModified(source.getModifiedtime());
        target.setUrl(getCategoryUrlResolver().resolve(source));
        target.setProducts(new ArrayList<ProductData>());
        target.setSubcategories(new ArrayList<CategoryHierarchyData>());

        if (options.contains(CatalogOption.PRODUCTS))
        {
            final List<ProductModel> products = source.getProducts();
            for (final ProductModel product : products)
            {
                final ProductData productData = getProductConverter().convert(product);
                target.getProducts().add(productData);
            }
        }

        if (page.includeInformationAboutPages())
        {
            final Integer totalNumber = getProductService().getAllProductsCountForCategory(source);
            final Integer numberOfPages = Integer.valueOf((int) (Math.ceil(totalNumber.doubleValue() / page.getPageSize())));
            target.setTotalNumber(totalNumber);
            target.setCurrentPage(Integer.valueOf(page.getPageNumber()));
            target.setPageSize(Integer.valueOf(page.getPageSize()));
            target.setNumberOfPages(numberOfPages);
        }

        if (options.contains(CatalogOption.SUBCATEGORIES))
        {
            recursive(target, source, true, options);
        }
    }
}
