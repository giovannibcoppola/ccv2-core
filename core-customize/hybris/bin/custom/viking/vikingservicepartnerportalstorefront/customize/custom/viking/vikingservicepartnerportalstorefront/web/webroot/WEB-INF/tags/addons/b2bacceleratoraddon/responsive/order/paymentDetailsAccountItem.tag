<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="label-order"><spring:theme code="text.paymentDetails" text="Payment Details"/></div>
<ul>
    <li><spring:theme code="checkout.multi.summary.orderPlacedBy"/>:&nbsp;<spring:theme
            code="text.company.user.${order.b2bCustomerData.titleCode}.name"
            text=""/>&nbsp;${fn:escapeXml(order.b2bCustomerData.firstName)}&nbsp;${fn:escapeXml(order.b2bCustomerData.lastName)}</li>
    <c:if test="${(not empty order.costCenter) and (not empty order.costCenter.code)}">
        <li><spring:theme code="checkout.multi.costCenter.label"
                          htmlEscape="false"/>:&nbsp;${fn:escapeXml(order.costCenter.name)}</li>
    </c:if>
    <c:choose>
        <c:when test="${not empty order.purchaseOrderNumber}">
            <li><spring:theme code="checkout.multi.purchaseOrderNumber.label"
                              htmlEscape="false"/>:&nbsp;${fn:escapeXml(order.purchaseOrderNumber)}</li>
        </c:when>
        <c:otherwise>
            <li><spring:theme code="checkout.multi.purchaseOrderNumber.label" htmlEscape="false"/>:&nbsp;-</li>
        </c:otherwise>
    </c:choose>
    </br>
    <div class="label-order"><spring:theme code="payment.details.account.invoices"/></div>

    <ul class="col-sm-12">
        <li class="order-detail-invoice-header col-sm-12">
            <div class="col-sm-2"><spring:theme code="text.invoice.number.name"/></div>
            <div class="col-sm-2"><spring:theme code="text.invoice.date.name"/></div>
            <div class="col-sm-2"><spring:theme code="text.invoice.download.name"/></div>
        </li>
    </ul>
    <ul class="col-sm-12">
        <c:forEach items="${order.invoices}" var="invoice">
            <li class="responsive-table-items col-sm-12">
                <div class="col-sm-2">
                    <p>${invoice.number}</p>
                </div>
                <div class="col-sm-2">
                    <p><fmt:formatDate value="${invoice.dateIssued}" pattern="yyyy-MM-dd"/></p>
                </div>
                <div class="col-sm-2">
                    <c:if test="${not empty invoice.url}">
                        <a href="${invoice.url}" target="_blank"><i class="glyphicon glyphicon-file" style="color: #ffffff"></i></a>
                    </c:if>
                </div>
            </li>
        </c:forEach>
    </ul>
</ul>