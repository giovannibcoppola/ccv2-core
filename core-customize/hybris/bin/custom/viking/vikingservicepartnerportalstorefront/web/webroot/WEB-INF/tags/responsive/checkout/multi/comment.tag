<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty cartData.b2BComment.comment}">
	<div class="checkout-summary-comment-headline">
		<spring:theme code="basket.page.comment"/>
	</div>
	<div class="checkout-summary-comment">
		<p>
			${cartData.b2BComment.comment}
		</p>
	</div>
</c:if>

