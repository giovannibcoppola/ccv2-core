<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="container vessel-search">
    <input class="hidden" id="addEquipmentVesselID" value="${vesselId}" />
    <input class="hidden" id="addEquipmentSerialNo" value="${serialNumber}" />

    <h2><spring:theme code="text.vessel.addEquipment.title"/></h2>
    <div class="row add-equipment-header">
        <div class="col-sm-8">
            <div class="clr vessel-form">
                <div class="row form-group">
                    <label for="serialnumber" class="col-sm-3 col-md-3 col-lg-2 control-label label-input"><spring:theme
                            code="text.vessel.equipment.modal.serialnumber"/></label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                        <input type="text" id="serialnumber" name="serial"
                               placeholder="<spring:theme code="text.vessel.equipment.modal.serialnumber" />"
                               class="form-control" value="${serialNumber}"/>
                    </div>
                </div>

                <button type="button" class="btn btn-primary" id="searchEquipmentButton"><spring:theme
                        code="text.vessel.search.search"/></button>
            </div>
        </div>
    </div>

    <c:choose>
        <c:when test="${not empty equipmentData.year}">
            <h3><spring:theme code="text.vessel.addEquipment.info"/></h3>

            <div class="clr vessel-form">
                <div class="row">
                    <label for="year" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.addequipment.equipment.year" />:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                            ${equipmentData.year}
                    </div>
                </div>
                <div class="row">
                    <label for="month" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.addequipment.equipment.month" />:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                            ${equipmentData.month}
                    </div>
                </div>
                <div class="row">
                    <label for="group" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.addequipment.equipment.group" />:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                            ${equipmentData.group}
                    </div>
                </div>
                <div class="row">
                    <label for="type" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.addequipment.equipment.type" />:</label>
                    <div class="col-sm-8 col-md-9 col-lg-10">
                            ${equipmentData.type}
                    </div>
                </div>
                </br>
                <button type="button" class="btn btn-primary" id="addEquipmentButton"><spring:theme code="text.vessel.search.add" /></button>

                <input class="hidden" id="addEquipmentType" value="${equipmentData.group}" />
            </div>
        </c:when>
        <c:when test="${empty equipmentData}">
            <%--Display nothing on page if no equipment data is present--%>
        </c:when>
        <c:when test="${empty equipmentData.year}">
            <br/>
            <div class="addEquipmentNotFound clr vessel-form">
                <div><b><spring:theme code="text.vessel.addequipment.equipment.not.found" /></b></div>
                <br/>
                <div><spring:theme code="text.vessel.addequipment.equipment.recheck" /></div>
                <br/>
                <div><spring:theme code="text.vessel.addequipment.equipment.not.found.contact" /></div>
            </div>
        </c:when>
    </c:choose>

    <c:if test="${not empty notifications}">
        <h3><spring:theme code="text.vessel.service.list"/></h3>

        <ul class="clr vessel-results-list clearfix service-list">
            <li class="vessel-results-list-header sort-by-notification">
                <div class="col-sm-3"><spring:theme code="text.vessel.service.list.notification" /></div>
                <div class="col-sm-3"><spring:theme code="text.vessel.service.list.inspection" /></div>
                <div class="col-sm-3"><spring:theme code="text.vessel.service.list.nextinspection" /></div>
                <div class="col-sm-2"><spring:theme code="text.vessel.service.list.certificate" /></div>
                <div class="col-sm-1"></div>
            </li>
        </ul>
        <ul id="vesseltab" class="clr vessel-results-list clearfix service-list">
            <c:forEach items="${notifications}" var="result">
                <li>
                    <div class="col-sm-3 service-name">
                        <c:choose>
                            <c:when test="${result.certificate}" >
                                <p><strong>${result.notificationNumber}</strong></p>
                            </c:when>
                            <c:otherwise>
                                <p class="hidden"><strong>${result.notificationNumber}</strong></p>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-sm-3"><p>${result.inspectionDate}</p></div>
                    <div class="col-sm-3"><p>${result.dateForNextInspection}</p></div>
                    <div class="col-sm-2">
                        <c:if test="${result.certificate}">
                            <a href="${storefrontRoot}/vesselSearch/getCertificate?notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}&serialNo=${ycommerce:encodeUrl(serialNumber)}" target="_blank">
                                <i class="glyphicon glyphicon-file"></i>
                            </a>
                        </c:if>
                    </div>
                    <div class="col-sm-1 service-edit no-pad-right">
                        <c:if test="${not empty result.openNotification}">
                            <a href="${storefrontRoot}/vesselSearch/loadForm?notificationId=${ycommerce:encodeUrl(result.notificationNumber)}&formId=${ycommerce:encodeUrl(type)}&serviceStation=${ycommerce:encodeUrl(serviceStation)}&prefillId=${ycommerce:encodeUrl(result.prefillNotification)}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                        </c:if>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </c:if>
</div>