<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="container">
    <div class="yCmsContentSlot">
        <div class="yCmsComponent clr col-xs-12 hp-text-comp yComponentWrapper">
            <div class="content">
                <h2><spring:theme code="text.enquiry.created.successfully.title"/></h2>
                <p>
                    <spring:theme code="text.enquiry.created.successfully.message"/>
                </p>
            </div>
        </div>
    </div>
</div>