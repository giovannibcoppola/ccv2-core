<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-certificates">
	<ycommerce:testId code="productCertificates_content_label">
		<c:forEach items="${product.videos}" var="video">
			<li>
				<a class="pdp-videoLink item" href="https://www.youtube.com/embed/${video.youtubeVideoId}">
					<span class="pdp-videoIcon"><i class="glyphicon glyphicon-play"></i></span>
					<img src="https://img.youtube.com/vi/${video.youtubeVideoId}/0.jpg" class="lazyOwl" />
				</a>
			</li>
		</c:forEach>
	</ycommerce:testId>
</div>