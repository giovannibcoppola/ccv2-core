<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="addToCartBtnId" required="false" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div id="addToCartTitle" class="display-none">
	<c:choose>
		<c:when test="${isEnquiryFlow}">
			<spring:theme code="basket.added.to.enquiry"/>
		</c:when>
		<c:otherwise>
			<spring:theme code="basket.added.to.basket"/>
		</c:otherwise>
	</c:choose>
</div>







