<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ycommerce:testId code="productCertificates_content_label">
	<ul>
		<c:forEach items="${product.certificates}" var="certificate">
			<li>
				<a href="${certificate.downloadURL}">
					<span> ${certificate.altText}</span>
				</a>
			</li>
		</c:forEach>
	</ul>
</ycommerce:testId>
