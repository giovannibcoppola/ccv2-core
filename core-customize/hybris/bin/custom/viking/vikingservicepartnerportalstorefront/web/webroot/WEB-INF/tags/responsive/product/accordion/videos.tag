<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ycommerce:testId code="productCertificates_content_label">
	<ul>
		<c:forEach items="${product.videos}" var="video">
			<li>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="lazyOwl embed-responsive-item youtubeVideo" src="https://www.youtube.com/embed/${video.youtubeVideoId}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
				</div>
			</li>
		</c:forEach>
	</ul>
</ycommerce:testId>
