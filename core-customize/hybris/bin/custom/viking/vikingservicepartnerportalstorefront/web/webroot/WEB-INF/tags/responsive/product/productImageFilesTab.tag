<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-datasheets">
	<ycommerce:testId code="productDatasheets_content_label">
		<c:forEach items="${product.images}" var="image" varStatus="loop" >
			<c:if test="${'product'.equals(image.format)}">

				<li>
					<a href="${image.url}">
						<span>${image.altText}_${loop.index}</span>
					</a>
				</li>
			</c:if>
		</c:forEach>
	</ycommerce:testId>
</div>