<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="vikingPage3Section1" var="feature">
		<cms:component component="${feature}" element="div" class="" />
	</cms:pageSlot>

	<div class="clr container pad-content-container">
		<div class="row">
			<cms:pageSlot position="vikingPage3Section2A" var="feature" element="div" class="col-sm-8 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPage3Section2B" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>

		<div class="row">
			<cms:pageSlot position="vikingPage3Section3A" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPage3Section3B" var="feature" element="div" class="col-sm-8 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>

		<div class="row">
			<cms:pageSlot position="vikingPage3Section4A" var="feature" element="div" class="col-sm-8 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPage3Section4B" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>
	</div>

</template:page>
