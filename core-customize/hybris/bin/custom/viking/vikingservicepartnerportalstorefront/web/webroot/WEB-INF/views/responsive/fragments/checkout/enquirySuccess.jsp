<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="enquiryCreatedModal" class="enquiry-modal">
    <div class="enquiry_modal_text">
        <spring:theme code="text.enquiry.created.successfully" />
    </div>
    </br>
    <a href="${request.contextPath}" class="close_button">
        <button id="enquiry_created_modal_close_button" class="form btn-default btn btn-block modal-close" type="button">
            <spring:theme code="text.enquiry.modal.close"/>
        </button>
    </a>
</div>