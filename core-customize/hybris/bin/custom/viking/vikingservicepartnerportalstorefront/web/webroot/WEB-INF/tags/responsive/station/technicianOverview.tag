<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="container technician-overview">

    <h2><spring:theme code="text.station.technician.overview.title" /></h2>

    <div class="row technician-overview-header">
        <div class="col-xs-12 pad-bottom-20">
            <spring:theme code="text.station.technician.overview.stationid" arguments=" ${stationId}" />
        </div>
    </div>

    <ul class="clr technician-overview-list vessel-results-list clearfix notification-list">
        <li class="technician-overview-list-header vessel-results-list-header">
            <div class="col-sm-2 col-lg-2">
                <spring:theme code="text.station.technician.list.number" />
                <div class="sort-list"></div>
            </div>
            <div class="col-sm-2 col-lg-2">
                <spring:theme code="text.station.technician.list.name" />
                <div class="sort-list"></div>
            </div>
            <div class="col-sm-2 col-lg-2">
                <spring:theme code="text.station.technician.list.qualification.group" />
                <div class="sort-list"></div>
            </div>
            <div class="col-sm-2 col-lg-3">
                <spring:theme code="text.station.technician.list.qualification" />
                <div class="sort-list"></div>
            </div>
            <div class="col-sm-2 col-lg-1">
                <spring:theme code="text.station.technician.list.valid.from" />
                <div class="sort-list"></div>
            </div>
            <div class="col-sm-2 col-lg-1">
                <spring:theme code="text.station.technician.list.valid.to" />
                <div class="sort-list"></div>
            </div>
        </li>
    </ul>
    <ul id="techniciantab" class="clr technician-overview-list vessel-results-list clearfix technician-list sort-result-list">
        <c:forEach items="${technicians}" var="technician">
            <li>
                <div class="col-sm-2 col-lg-2 number-responsive sortable-line">
                    <p><strong class="sortable-content">${technician.number}</strong></p>
                </div>
                <div class="col-sm-2 col-lg-2 sortable-line"><p class="sortable-content">${technician.name}</p></div>
                <div class="col-sm-2 col-lg-2 sortable-line"><p class="sortable-content">${technician.group}</p></div>
                <div class="col-sm-2 col-lg-3 sortable-line"><p class="sortable-content">${technician.qualification}</p></div>
                <div class="col-sm-2 col-lg-1 sortable-line"><p class="sortable-content">${technician.validFrom}</p></div>
                <div class="col-sm-2 col-lg-2 sortable-line">
                    <p><span class="sortable-content">${technician.validTo}</span> &nbsp;&nbsp;&nbsp;
                        <c:choose>
                            <c:when test="${technician.isValid}">
                                <i class="glyphicon glyphicon-asterisk" style="color: #008000"><span class="hidden">ok</span></i>
                            </c:when>
                            <c:otherwise>
                                <i class="glyphicon glyphicon-asterisk" style="color: #FF0000"><span class="hidden">remove</span></i>
                            </c:otherwise>
                        </c:choose>
                    </p>
                </div>
            </li>
        </c:forEach>
    </ul>

</div>
