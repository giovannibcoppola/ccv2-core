<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="vikingPageFormSection" var="feature">
		<cms:component component="${feature}" element="div" class="" />
	</cms:pageSlot>

	<cms:pageSlot position="vikingPageFormSection1" var="feature">
		<cms:component component="${feature}" element="div" class="" />
	</cms:pageSlot>

	<div class="clr container pad-content-container">
		<cms:pageSlot position="vikingPageFormSection2" var="feature">
			<cms:component component="${feature}" element="div" class="col-xs-12 margin-bottom-20" />
		</cms:pageSlot>

		<cms:pageSlot position="vikingPageFormSection3" var="feature">
			<cms:component component="${feature}" element="div" class="clr col-xs-12 margin-bottom-20" />
		</cms:pageSlot>

		<cms:pageSlot position="vikingPageFormSection4" var="feature">
			<cms:component component="${feature}" element="div" class="clr col-xs-12" />
		</cms:pageSlot>
	</div>

</template:page>
