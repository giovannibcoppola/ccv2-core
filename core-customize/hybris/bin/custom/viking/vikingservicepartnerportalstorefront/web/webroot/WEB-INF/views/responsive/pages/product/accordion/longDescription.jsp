<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product/accordion"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty product.longDescription}">
	<a name="product-accordion-long-description-x" id="product-accordion-long-description-x"></a>
	<input type="checkbox" id="product-accordion-long-description" checked />
	<label for="product-accordion-long-description">
		<h3>${fn:escapeXml(title)}</h3>
		<i class="glyphicon glyphicon-chevron-down"></i>
	</label>
	<div class="product-accordion-item">
		<product:longDescription product="${product}" />
	</div>
</c:if>
