<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
	<div class="checkout-login container pad-bottom-pagefooter">
		<%@ page trimDirectiveWhitespaces="true"%>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
		<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

		<template:page pageTitle="${pageTitle}">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<cms:pageSlot position="MainContentSlot" var="feature" element="div" class="login-left-content-slot pad-bottom-pagefooter">
							<cms:component component="${feature}"  element="div" class="login-left-content-component"/>
						</cms:pageSlot>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<cms:pageSlot position="Banner1ContentSlot" var="feature" element="div" class="login-right-content-slot pad-bottom-pagefooter">
							<cms:component component="${feature}"  element="div" class="login-right-content-component"/>
						</cms:pageSlot>
					</div>
					<div class="col-md-3">
						<cms:pageSlot position="Banner2ContentSlot" var="feature" element="div" class="login-right-content-slot pad-bottom-pagefooter">
							<cms:component component="${feature}"  element="div" class="login-right-content-component"/>
						</cms:pageSlot>
					</div>
					<div class="col-md-3">
						<cms:pageSlot position="Banner3ContentSlot" var="feature" element="div" class="login-right-content-slot pad-bottom-pagefooter">
							<cms:component component="${feature}"  element="div" class="login-right-content-component"/>
						</cms:pageSlot>
					</div>
					<div class="col-md-3">
						<cms:pageSlot position="Banner4ContentSlot" var="feature" element="div" class="login-right-content-slot pad-bottom-pagefooter">
							<cms:component component="${feature}"  element="div" class="login-right-content-component"/>
						</cms:pageSlot>
					</div>
				</div>
			</div>
		</template:page>

	</div>
</template:page>
