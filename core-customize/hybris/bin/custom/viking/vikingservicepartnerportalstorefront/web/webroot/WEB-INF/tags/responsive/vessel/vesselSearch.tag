<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="container vessel-search">

    <h2><spring:theme code="text.vessel.search.title" /></h2>

    <div class="row vessel-header">
        <form:form action="rfcSearchResult" id="searchVesselForm" modelAttribute="searchVesselForm" method="GET">
            <div class="col-sm-3">
                <select id="dropDown" name="searchType" class="form-control">
                    <c:forEach items="${dropdownDataList}" var="result">
                        <option value="${result.id}">${result.name}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="col-sm-4 col-md-5 col-lg-6">
                <input type="text" id="searchTerm" name="searchTerm" class="form-control" placeholder='<spring:theme code="text.vessel.search.placeholder" />' value="${searchValue}">
            </div>
            <div class="col-sm-5 col-md-4 col-lg-3">
                <button type="submit" class="btn btn-primary" ><spring:theme code="text.vessel.search.searchbutton" /></button> <button type="button" id="createVesselModalLink" class="btn btn-default cboxElement" data-title='<spring:theme code="text.vessel.search.createbutton" />'><spring:theme code="text.vessel.search.createbutton" /></button>
            </div>
        </form:form>
    </div>

    <div class="vessel-results-header clearfix">
        <div class="col-xs-4 col-sm-6 vessel-results-header-title">
            <spring:theme code="text.vessel.search.items" />
            <c:if test="${pageID ne 'home' and entryList ne 'empty'}">
                (${fn:length(entryList.vesselDataList)})
            </c:if>
        </div>
        <div id="vessel-sort-container" class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
            <div id="sortVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
            <div id="filterVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i></div>
            <%--<a href="#"><i class="glyphicon glyphicon-list-alt"></i></a>
            <a href="#"><i class="glyphicon glyphicon-cog"></i></a>--%>
        </div>
        <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text" placeholder='<spring:theme code="text.vessel.search.placeholder" />'></div>
    </div>

    <ul class="clr vessel-results-list clearfix">
        <li class="vessel-results-list-header">
            <div class="col-sm-3"><spring:theme code="text.vessel.search.list.vessel" /></div>
            <div class="col-sm-3"><spring:theme code="text.vessel.search.list.flag" /></div>
            <div class="col-sm-3"><spring:theme code="text.vessel.search.list.call" /></div>
            <div class="col-sm-3"><spring:theme code="text.vessel.search.list.owner" /></div>
        </li>
    </ul>

    <c:if test="${pageID ne 'home' and entryList ne 'empty'}">
        <ul id="vesseltab" class="clr vessel-results-list clearfix">
        <c:forEach items="${entryList.vesselDataList}" var="result">
            <li>
                <a href="${contextPath}/vesselSearch/vesselOverview?vesselID=${result.vesselID}&signal=${result.callSignal}">
                <div class="col-sm-3 vessel-name"><span>${result.vesselName}</span>
                <p>${result.vesselID}</p></div>
                <div class="col-sm-3"><p>${result.flagState}</p></div>
                <div class="col-sm-3"><p>${result.callSignal}</p></div>
                <div class="col-sm-3"><p>${result.owner}</p></div></a>
            </li>
        </c:forEach>
        </ul>
     </c:if>

     <div class="hidden">
         <div id="createVesselModal" class="vessel-modal">

               <form:form method="GET" action="#" modelAttribute="createVesselForm">
                 <div class="clr vessel-form">
                     <div class="form-group">
                         <label for="createVesselDesc" class="custom-label"><spring:theme code="text.create.vessel.modal.vesselname" />:</label>
                         <input type="text" class="form-control input-sm" name="description" id="createVesselDesc" placeholder='<spring:theme code="text.create.vessel.modal.vesselname" />'/>
                     </div>
                     <div class="form-group">
                         <label for="createVesselFlagState" class="custom-label"><spring:theme code="text.create.vessel.modal.flagstate" />:</label>
                         <select class="form-control input-sm config-selector config_type" name="flagState" id="createVesselFlagState">
                             <option selected disabled><spring:theme code="text.createvessel.select" /></option>
                             <c:forEach items="${flagStates}" var="flagState" >
                                 <option value="${flagState.code}">${flagState.name}</option>
                             </c:forEach>
                         </select>
                     </div>
                     <div class="form-group">
                         <label for="createVesselSignal" class="custom-label"><spring:theme code="text.create.vessel.modal.signal" />:</label>
                         <input type="text" class="form-control input-sm" name="signal" id="createVesselSignal" placeholder='<spring:theme code="text.create.vessel.modal.signal" />'/>
                     </div>
                     <div class="form-group">
                     <label id="lblVesselId"></label>
                     </div>
                 </div>
                 <div class="modal-footer">

                     <button type="button" class="btn btn-default modal-close" id="createVesselCancel"><spring:theme code="text.vessel.modal.cancel" /></button>
                     <button type="button" class="btn btn-primary" id="createVesselSubmit"><spring:theme code="text.vessel.modal.create" /></button>
                 </div>

             </form:form>
         </div>
     </div>

</div>
