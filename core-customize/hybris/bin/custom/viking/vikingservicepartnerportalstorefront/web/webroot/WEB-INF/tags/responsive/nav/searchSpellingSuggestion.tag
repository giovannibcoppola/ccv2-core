<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="spellingSuggestion" required="true"
              type="de.hybris.platform.commerceservices.search.facetdata.SpellingSuggestionData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${not empty spellingSuggestion}">
    <div class="searchSpellingSuggestionPrompt">
        <c:url value="${spellingSuggestion.query.url}" var="spellingSuggestionQueryUrl"/>
       <cms:pageSlot position="didYouMean" var="feature">
            ${feature.content}:&nbsp;<a
                    href="${spellingSuggestionQueryUrl}">${fn:escapeXml(spellingSuggestion.suggestion)}</a>?
        </cms:pageSlot>
    </div>
</c:if>
