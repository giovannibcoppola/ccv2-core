<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${customerPortalLook}">
<script id="global-alert-info-template" type="text/x-jquery-tmpl">
    <div class="alert alert-info alert-dismissable getAccAlert">
        <div class="container">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
        </div>
    </div>
</script>

<script id="global-alert-warning-template" type="text/x-jquery-tmpl">
    <div class="alert alert-warning alert-dismissable getAccAlert">
        <div class="container">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
        </div>
    </div>
</script>

<script id="global-alert-danger-template" type="text/x-jquery-tmpl">
    <div class="alert alert-danger alert-dismissable getAccAlert">
        <div class="container">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
    </div>
</script>

    </c:when>
    <c:otherwise>


<script id="global-alert-info-template" type="text/x-jquery-tmpl">
    <div class="alert alert-info alert-dismissable getAccAlert">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
    </div>
</script>

<script id="global-alert-warning-template" type="text/x-jquery-tmpl">
    <div class="alert alert-warning alert-dismissable getAccAlert">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
    </div>
</script>

<script id="global-alert-danger-template" type="text/x-jquery-tmpl">
    <div class="alert alert-danger alert-dismissable getAccAlert">
        <button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
        \${message}
    </div>
</script>

    </c:otherwise>
</c:choose>