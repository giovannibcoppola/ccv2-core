<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<div class="carousel gallery-carousel gallery-thumbnails js-gallery-carousel hidden-xs hidden-sm">
    <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
        <c:choose>
            <c:when test="${not empty container.thumbnail.youtubeVideoId}">
                <a class="pdp-videoLink item">
                    <span class="pdp-videoIcon"><i class="glyphicon glyphicon-play"></i></span>
                    <img src="https://img.youtube.com/vi/${container.thumbnail.youtubeVideoId}/0.jpg" class="lazyOwl" />
                </a>
            </c:when>
            <c:otherwise>
                <a class="item"> <img class="lazyOwl" src="${container.thumbnail.url}" alt="${fn:escapeXml(container.thumbnail.altText)}"></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>
