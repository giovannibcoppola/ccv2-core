$(document).ready(function () {

    new Imager('.js-responsive-image');

    // add a CSRF request token to POST ajax request if its not available
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        // Modify options, control originalOptions, store jqXHR, etc
        if (options.type === "post" || options.type === "POST") {
            var noData = (typeof options.data === "undefined");
            if (noData || options.data.indexOf("CSRFToken") === -1) {
                options.data = (!noData ? options.data + "&" : "") + "CSRFToken=" + ACC.config.CSRFToken;
            }
        }
    });

    $(document).on("click", "#addressSubmit", function (e) {
        e.preventDefault();
        var addressForm = $('#addressForm');
        addressForm.submit();
    });

    $(document).on("click", "#deliveryMethodSubmit", function (e) {
        e.preventDefault();
        var selectDeliveryMethodForm = $('#selectDeliveryMethodForm');
        selectDeliveryMethodForm.submit();
    });

    $('.password-strength').pstrength();

    $(document).on("click", ".js-saved-payments", function (e) {
        e.preventDefault();

        $.colorbox({
            href: "#savedpayments",
            inline: true,
            maxWidth: "100%",
            opacity: 0.7,
            width: "320px",
            close: '<span class="glyphicon glyphicon-remove"></span>',
            title: '<div class="headline"><span class="headline-text">Saved Payment Methods</span></div>',
            onComplete: function () {
            }
        });
    })

    $('.flex-column').syncHeight({
        'parent': true,
        'updateOnResize': true
    });

    if ($('img[usemap]').length > 0) {
        $('img[usemap]').rwdImageMaps();
    }

    $(document).on("click", ".panel-group .js-next", function (e) {
        e.preventDefault();
        $(this).parents(".panel").next().find(".panel-title a").click();
    });

    $(document).on("click", ".checkout-steps .js-next", function (e) {
        e.preventDefault();
        $(this).parents(".step-body").next().click()
        if ($(this).data("next")) {
            var step = $(this).data("next");
            var href = window.location.href;
            var nhref = href.replace(new RegExp(/step./), "step" + step);
            if (!window.location.hash) {
                nhref = nhref + "#step" + step;
            } else {
                nhref = nhref.replace(new RegExp(/#.*/), "#step" + step);
            }
            window.location = nhref;
        }
    });

    addressBookPager();

});


function addressBookPager() {
    $(document).on("change", ".js-address-input", function (e) {
        e.preventDefault();
        var addressDetails = $(this).data("addressDetails");
        $(".js-addressbook-addressFull").html(addressDetails["addressFull"]);
    });


    $(document).on("click", ".js-select-store-label", function (e) {
        $(".js-addressbook-component").addClass("show-address");
    });

    $(document).on("click", ".js-back-to-addresslist", function (e) {
        $(".js-addressbook-component").removeClass("show-address");
    });

    // select the first store
    // dirty workaround for firefox
    var firstInput = $(".js-address-input")[1];
    $(firstInput).click();

    // select the first store
    var firstInput = $(".js-address-input")[0];
    $(firstInput).click();

    var listHeight = $(".js-address-list").height();
    var $listitems = $(".js-address-list > li");
    var listItemHeight = $listitems.height();
    var displayCount = 5;
    var totalCount = $listitems.length;
    var curPos = 0;
    var pageEndPos = ((Math.ceil(totalCount / displayCount) * listHeight) - listHeight) * -1;


    $(".js-address-pager-prev").hide();

    $(".js-address-pager-item-from").html("1");
    $(".js-address-pager-item-to").html(displayCount);
    $(".js-address-pager-item-all").html(totalCount);

    $(document).on("click", ".js-address-pager-prev", function (e) {
        e.preventDefault();
        $listitems.css("transform", "translateY(" + (curPos + listHeight) + "px)");
        curPos = curPos + listHeight;
        checkPosition("prev");
    })


    $(document).on("click", ".js-address-pager-next", function (e) {
        e.preventDefault();
        $listitems.css("transform", "translateY(" + (curPos - listHeight) + "px)");
        curPos = curPos - listHeight;
        checkPosition("next");
    })


    function checkPosition() {

        if (curPos < 0) {
            $(".js-address-pager-prev").show();
        }

        if (curPos == 0) {
            $(".js-address-pager-prev").hide();
        }

        if (curPos == pageEndPos) {
            $(".js-address-pager-next").hide();
        }

        if (curPos > pageEndPos) {
            $(".js-address-pager-next").show();
        }

        var curPage = ((curPos / (displayCount * listItemHeight)) * -1) + 1;

        $(".js-address-pager-item-from").html(curPage * displayCount - 4);
        $(".js-address-pager-item-to").html(curPage * displayCount);
    }

}

(function(document, history, location) {
  var HISTORY_SUPPORT = !!(history && history.pushState);

  var anchorScrolls = {
    ANCHOR_REGEX: /^#[^ ]+$/,
    OFFSET_HEIGHT_PX: 50,

    /**
     * Establish events, and fix initial scroll position if a hash is provided.
     */
    init: function() {
      this.scrollToCurrent();
      $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
      $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
    },

    /**
     * Return the offset amount to deduct from the normal scroll position.
     * Modify as appropriate to allow for dynamic calculations
     */
    getFixedOffset: function() {
      return this.OFFSET_HEIGHT_PX;
    },

    /**
     * If the provided href is an anchor which resolves to an element on the
     * page, scroll to it.
     * @param  {String} href
     * @return {Boolean} - Was the href an anchor.
     */
    scrollIfAnchor: function(href, pushToHistory) {
      var match, anchorOffset;

      if(!this.ANCHOR_REGEX.test(href)) {
        return false;
      }

      match = document.getElementById(href.slice(1));

      if(match) {
        anchorOffset = $(match).offset().top - this.getFixedOffset();
        $('html, body').animate({ scrollTop: anchorOffset});

        // Add the state to history as-per normal anchor links
        if(HISTORY_SUPPORT && pushToHistory) {
          history.pushState({}, document.title, location.pathname + href);
        }
      }

      return !!match;
    },

    /**
     * Attempt to scroll to the current location's hash.
     */
    scrollToCurrent: function(e) {
      if(this.scrollIfAnchor(window.location.hash) && e) {
      	e.preventDefault();
      }
    },

    /**
     * If the click event's target was an anchor, fix the scroll position.
     */
    delegateAnchors: function(e) {
      var elem = e.target;

      if(this.scrollIfAnchor(elem.getAttribute('href'), true)) {
        e.preventDefault();
      }
    }
  };

	$(document).ready($.proxy(anchorScrolls, 'init'));
})(window.document, window.history, window.location);
