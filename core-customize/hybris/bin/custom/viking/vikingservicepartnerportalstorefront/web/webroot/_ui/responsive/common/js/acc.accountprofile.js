ACC.accountprofile = {

    _autoload: [
        "init",
        ["updateProfile", $(".accountUpdateAction").length != 0]
    ],

    init: function () {

    },

    updateProfile: function () {
        $("#updateProfileSubmit").on('click', function (e) {
            e.preventDefault();

            var approvedFacilityCompany = $("#afcompany").val();
            var approvedFacilityAddress1 = $("#afaddress1").val();
            var approvedFacilityAddress2 = $("#afaddress2").val();
            var approvedFacilityAddress3 = $("#afaddress3").val();
            var approvedFacilityPostalCode = $("#afpostalCode").val();
            var approvedFacilityCity = $("#afcity").val();
            var approvedFacilityCountry = $("#afcountry").val();
            var approvedFacilityPhoneCountryCode = $("#afphoneCountryCode").val();
            var approvedFacilityPhone = $("#afphone").val();
            var approvedFacilityFaxCountryCode = $("#affaxCountryCode").val();
            var approvedFacilityFax = $("#affax").val();
            var approvedFacilityEmail = $("#afemail").val();
            var approvedFacilityAdditionalEmail2 = $("#afadditionalEmail2").val();
            var approvedFacilityAdditionalEmail3 = $("#afadditionalEmail3").val();
            var approvedFacilityAdditionalEmail4 = $("#afadditionalEmail4").val();
            var approvedFacilityAdditionalEmail5 = $("#afadditionalEmail5").val();
            var approvedFacilityAdditionalEmail6 = $("#afadditionalEmail6").val();
            var approvedFacilityWebsite = $("#afwebsite").val();
            var approvedFacilityServiceStationManager = $("#afserviceStationManager").val();
            var approvedFacilityServiceStationPhone = $("#afserviceStationPhone").val();
            var approvedFacilityServiceStationEmail = $("#afserviceStationEmail").val();
            var approvedFacilityAfterHoursPhone = $("#afafterHoursPhone").val();

            var addressCompanyName = $("#company").val();
            var addressAddress = $("#address").val();
            var addressPostalCode = $("#postalCode").val();
            var addressCity = $("#city").val();
            var addressCountry = $("#country").val();
            var addressPhone = $("#phone").val();
            var addressEmail = $("#email").val();


            document.getElementById("updateProfileCancel").disabled = true;
            document.getElementById("updateProfileSubmit").disabled = true;

            var formData = {
                'approvedfacilitycompany': approvedFacilityCompany,
                'approvedFacilityAddress1': approvedFacilityAddress1,
                'approvedFacilityAddress2': approvedFacilityAddress2,
                'approvedFacilityAddress3': approvedFacilityAddress3,
                'approvedFacilityPostalCode': approvedFacilityPostalCode,
                'approvedFacilityCity': approvedFacilityCity,
                'approvedFacilityCountry': approvedFacilityCountry,
                'approvedFacilityPhoneCountryCode': approvedFacilityPhoneCountryCode,
                'approvedFacilityPhone': approvedFacilityPhone,
                'approvedFacilityFaxCountryCode': approvedFacilityFaxCountryCode,
                'approvedFacilityFax': approvedFacilityFax,
                'approvedFacilityEmail': approvedFacilityEmail,
                'approvedFacilityAdditionalEmail2': approvedFacilityAdditionalEmail2,
                'approvedFacilityAdditionalEmail3': approvedFacilityAdditionalEmail3,
                'approvedFacilityAdditionalEmail4': approvedFacilityAdditionalEmail4,
                'approvedFacilityAdditionalEmail5': approvedFacilityAdditionalEmail5,
                'approvedFacilityAdditionalEmail6': approvedFacilityAdditionalEmail6,
                'approvedFacilityWebsite': approvedFacilityWebsite,
                'approvedFacilityServiceStationManager': approvedFacilityServiceStationManager,
                'approvedFacilityServiceStationPhone': approvedFacilityServiceStationPhone,
                'approvedFacilityServiceStationEmail': approvedFacilityServiceStationEmail,
                'approvedFacilityAfterHoursPhone': approvedFacilityAfterHoursPhone,
                'addressCompanyName': addressCompanyName,
                'addressAddress': addressAddress,
                'addressPostalCode': addressPostalCode,
                'addressCity': addressCity,
                'addressCountry': addressCountry,
                'addressPhone': addressPhone,
                'addressEmail': addressEmail

            };

            $.ajax({
                type: 'POST',
                url: ACC.config.contextPath + '/my-account/update-profile',
                data: formData,
                encode: true
            })
            // using the done promise callback
                .done(function (response) {
                    window.history.go(-1);
                })
                .fail(function () {
                    window.location.reload();
                });
        });
    }

};