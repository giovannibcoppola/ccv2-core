<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<template:page pageTitle="${pageTitle}">

    <div class="container vessel-search customer-portal-vessel-search pad-top-20">

        <%--<h2><spring:theme code="text.vesseluserpage.assignments.title" /></h2>--%>

        <div class="vessel-results-header clearfix">
            <div class="col-xs-4 col-sm-6 vessel-results-header-title">
                <spring:theme code="text.vesseluserpage.assignments.title" /> (${vesselSize})
                <%--<button class="btn-link vessel-results-header__link" data-toggle="modal" data-target="#vesselModal"><spring:theme code="text.vesseluserpage.assignments.relation" /></button>--%>
                <a id="vesselModalLink" class="btn-link vessel-results-header__link inline cboxElement" href="#vesselModal" data-title='<spring:theme code="text.vesseluserpage.assign.user" />'><spring:theme code="text.vesseluserpage.assignments.relation" /></a>
            </div>

            <div class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
                <div id="sortVesselAction" class="vessel-results-header__icon userSortLink" title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
                <div id="filterVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i></div>
                <!--  <a href="#"><i class="glyphicon glyphicon-list-alt"></i></a>
                <a href="#"><i class="glyphicon glyphicon-cog"></i></a> -->
            </div>
            <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text" placeholder='<spring:theme code="text.vessel.search.placeholder" />'></div>
        </div>

        <ul class="clr vessel-results-list clearfix">
            <li class="vessel-results-list-header">
                <div class="col-sm-6"><spring:theme code="text.vesseluserpage.vessel" /></div>
                <div class="col-sm-3"><spring:theme code="text.vesseluserpage.username" /></div>
                <div class="col-sm-3 actions-label text-right"></div>
            </li>
        </ul>

        <ul id="vesseltab" class="clr vessel-results-list clearfix service-list">
           <c:forEach items="${vesselPageData}" var="vessel" >
                 <c:if test="${not empty vessel.customerData}">
                    <c:forEach items="${vessel.customerData}" var="customer" varStatus="i">
                    <li>
                        <div class="col-sm-6 vessel-user-detail"><span><c:if test="${!empty vessel.name}"> ${vessel.name} - </c:if>${vessel.vesselId}</span><br>
                            <p></p>
                        </div>
                        <div class="col-sm-5 vessel-user-detail vessel-name"><span>${customer.name}</span></div>
                        <div class="col-sm-1 user-delete-icon"><a href="${request.contextPath}/AssignVessels/${vessel.vesselId}/deleteuser/${customer.uid}"><i class="glyphicon glyphicon-trash"></i></a></div>
                    </li>
                    </c:forEach>
                </c:if>
            </c:forEach>
        </ul>

        <div class="hidden">
            <div id="vesselModal">

                  <form:form method="post" action="${request.contextPath}/AssignVessels/saveDetails" modelAttribute="vessel">
                    <div class="clr vessel-form">
                        <div class="form-group">
                            <label for="vesselID" class="custom-label"><spring:theme code="text.vesseluserpage.vessel" />:</label>
                           <!--  <input type="text" class="form-control input-sm ktwPONumber" name="vesselID" id="vesselID" placeholder="vessel id"/> -->
                            <select class="" name="vesselID" id="vesselID" multiple="multiple">
                                <c:forEach items="${vesselsData}" var="vesselData" >
                                    <option value="${vesselData.name}_${vesselData.imo}">${vesselData.name} - ${vesselData.imo}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="userName" class="custom-label"><spring:theme code="text.vesseluserpage.user" />:</label>
                            <select class="form-control input-sm config-selector config_type" name="userName" id="userName" >
                                <option selected disabled><spring:theme code="text.vesseluserpage.select" /></option>
                                <c:forEach items="${customers}" var="customer" >
                                    <option value="${customer.name}" data-uid="${customer.uid}">${customer.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-close">Cancel</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form:form>
            </div>
        </div>


        <%--<div class="modal fade vesselModal" id="vesselModal" tabindex="-1" role="dialog" aria-labelledby="vesselModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="vesselModalLabel"><spring:theme code="text.vesseluserpage.assign.user" /></h4>
                    </div>
                    <div class="modal-body">
                        <form:form method="post" action="${request.contextPath}/AssignVessels/save" commandName="vessel">

                            <div class="clr vessel-form">
                                <div class="form-group">
                                    <label for="vesselID" class="custom-label"><spring:theme code="text.vesseluserpage.vessel" />:</label>
                                    <input type="text" class="form-control input-sm ktwPONumber" name="vesselID" id="vesselID" placeholder="vessel id"/>
                                </div>
                                <div class="form-group">
                                    <label for="userName" class="custom-label"><spring:theme code="text.vesseluserpage.user" />:</label>
                                    <select class="form-control input-sm config-selector config_type" name="userName" id="userName">
                                        <option selected disabled>SELECT</option>
                                        <c:forEach items="${allUsers}" var="users" >
                                            <option value="${users.name}">${users.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                        </form:form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><spring:theme code="text.vesseluserpage.button.cancel" /></button>
                        <button type="submit" class="btn btn-primary"><spring:theme code="text.vesseluserpage.button.submit" /></button>
                    </div>
                </div>
            </div>
        </div>--%>

    </div>

</template:page>
