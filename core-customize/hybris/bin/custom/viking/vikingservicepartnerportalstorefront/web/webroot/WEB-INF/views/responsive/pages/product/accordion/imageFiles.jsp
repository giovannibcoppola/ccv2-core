<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product/accordion"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty product.images}">
	<input type="checkbox" id="product-accordion-images" />
	<label for="product-accordion-images">
		<h3>${fn:escapeXml(title)}</h3>
		<i class="glyphicon glyphicon-chevron-down"></i>
	</label>
	<div class="product-accordion-item">
		<product:imageFiles product="${product}" />
	</div>
</c:if>
