<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="/j_spring_security_check" var="loginActionUrl" />

<div class="login-mainHeader col-xs-12">
	<div class="login-site-logo left">
		<img src="${themeResourcePath}/images/logo.png" />
	</div>
	<h1 class="left"><spring:theme code="text.viking.life.header" /></h1>
</div>

<div class="clr login-section">
	<user:login actionNameKey="login.login" action="${loginActionUrl}" />
</div>
