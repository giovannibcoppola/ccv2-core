<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="enquiryForm">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="enquiry_headline">
                <h1><spring:theme code="enquiry.page.headline"/></h1>
            </div>
        </div>

        <div class="col-xs-12 col-md-6">
            <div class="enquiry_sub_headline">
                <h2><spring:theme code="enquiry.page.form.headline"/></h2>
            </div>
            <div class="enquiry_address_form">
                <form:form modelAttribute="vikingEnquiryAddressForm" action="/inquiryform" method="POST"
                           id="vikingEnquiryAddressForm" >
                    <div class="row form-group">
                        <label for="company" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.company"/>*:</label>
                        <spring:theme var="invalidcompanyErrorMsg" code="vikingEnquiryAddressForm.company.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['company']}">error</c:if>" name="company" id="company"
                               value="${vikingEnquiryAddressForm.company}"  required oninvalid="this.setCustomValidity('${invalidcompanyErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['company']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['company']}"/>
                            </span>
                        </c:if>
                    </div>
                    <div class="row form-group">
                        <label for="name" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.name"/>*:</label>

                        <spring:theme var="invalidNameErrorMsg" code="vikingEnquiryAddressForm.name.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['name']}">error</c:if>" name="name" id="name"
                               value="${vikingEnquiryAddressForm.name}"  required oninvalid="this.setCustomValidity('${invalidNameErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['name']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['name']}"/>
                            </span>
                        </c:if>
                    </div>
                    <div class="row form-group">
                        <label for="line1" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.line1"/>*:</label>
                        <spring:theme var="invalidline1ErrorMsg" code="vikingEnquiryAddressForm.line1.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['line1']}">error</c:if>" name="line1" id="line1"
                               value="${vikingEnquiryAddressForm.line1}"  required oninvalid="this.setCustomValidity('${invalidline1ErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['line1']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['line1']}"/>
                            </span>
                        </c:if>
                    </div>
                    <div class="row form-group">
                        <label for="line2" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.line2"/>:</label>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory" name="line2" id="line2"
                               value="${vikingEnquiryAddressForm.line2}"/>
                    </div>
                    <div class="row form-group">
                        <label for="postcode" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.postcode"/>*:</label>
                        <spring:theme var="invalidpostcodeErrorMsg" code="vikingEnquiryAddressForm.postcode.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['postcode']}">error</c:if>" name="postcode" id="postcode"
                               value="${vikingEnquiryAddressForm.postcode}" required
                               oninvalid="this.setCustomValidity('${invalidpostcodeErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['postcode']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['postcode']}"/>
                            </span>
                        </c:if>
                    </div>
                    <div class="row form-group">
                        <label for="townCity" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.city"/>*:</label>
                        <spring:theme var="invalidcityErrorMsg" code="vikingEnquiryAddressForm.city.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['city']}">error</c:if>" name="city" id="city"
                               value="${vikingEnquiryAddressForm.city}" required
                               oninvalid="this.setCustomValidity('${invalidcityErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['city']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['city']}"/>
                            </span>
                        </c:if>
                    </div>
                    <div class="row form-group">
                        <label for="countryIso" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.country"/>*:</label>
                        <select  class="form-control-enquiry input-sm config-selector config_type col-xs-12 col-sm-8 enquiry_input_mandatory" name="countryIso" id="countryIso">
                            <c:forEach items="${countries}" var="country" >
                                <option value="${country.isocode}">${country.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="row form-group">
                        <label for="phone" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.phone"/>:</label>
                        <input type="text" class="col-xs-12 col-sm-8" name="phone" id="phone"
                               value="${vikingEnquiryAddressForm.phone}"/>
                    </div>
                    <div class="row form-group">
                        <label for="email" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.email"/>*:</label>
                        <spring:theme var="invalidEmailErrorMsg" code="vikingEnquiryAddressForm.email.invalid"/>
                        <input type="text" class="col-xs-12 col-sm-8 enquiry_input_mandatory <c:if test="${not empty errors['email']}">error</c:if>" name="email" id="email"
                               value="${vikingEnquiryAddressForm.email}" required
                               oninvalid="this.setCustomValidity('${invalidEmailErrorMsg}')"
                               oninput="this.setCustomValidity('')"/>
                        <c:if test="${not empty errors['email']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['email']}"/>
                            </span>
                        </c:if>
                    </div>

                    <div class="row form-group">
                        <label for="preferredContactMethod" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.preferredContactMethod"/>*:</label>
                        <select  class="form-control-enquiry input-sm config-selector config_type col-xs-12 col-sm-8 enquiry_input_mandatory" name="preferredContactMethod" id="preferredContactMethod">
                            <option value="email" selected>Email</option>
                            <option value="phone">Phone</option>
                        </select>
                    </div>
                    <div class="row form-group">
                        <label for="customerReferenceNumber" class="col-xs-12 col-sm-4 control-label"><spring:theme
                                code="vikingEnquiryAddressForm.customerReferenceNumber"/>:</label>
                        <input type="text" class="col-xs-12 col-sm-8" name="customerReferenceNumber" id="customerReferenceNumber"
                               value="${vikingEnquiryAddressForm.customerReferenceNumber}"/>
                    </div>
                    <div class="row form-group">
                        <label class="col-xs-12 col-sm-4 control-label"> <spring:theme code="vikingEnquiryAddressForm.hasComment"/>*: </label>
                        <div class="radio-inline">
                            <input  value="TRUE" type="radio" class="form-check-input enquiry_input_mandatory " id="yes" name="hasComment" >
                            <label class="form-check-label" for="yes">YES</label>
                        </div>

                        <div class="radio-inline">
                            <input  checked value="FALSE" type="radio" class="form-check-input enquiry_input_mandatory " id="no" name="hasComment"  />
                            <label class="form-check-label" for="no">NO</label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <spring:theme var="invalidhasCommentErrorMsg" code="vikingEnquiryAddressForm.comment.invalid"/>
                        <textarea class="enquiry__comment col-xs-12 col-sm-8 col-sm-offset-4 <c:if test="${not empty errors['comment']}">error</c:if>" id="comment" name="comment"
                                  placeholder="<spring:theme code="vikingEnquiryAddressForm.comment"/>" rows="3"
                                  maxlength="255" oninvalid="this.setCustomValidity('${invalidhasCommentErrorMsg}')"
                                  oninput="this.setCustomValidity('')"></textarea>
                        <c:if test="${not empty errors['comment']}">
                            <span class="col-xs-12 col-sm-4 control-label error">
                                <spring:theme code="${errors['comment']}"/>
                            </span>
                        </c:if>
                    </div>

                    <div class="row required_enquiry_fields">
                        <spring:theme code="required.enquiry.fields"/>
                    </div>

                    <div class="row form-group">
                        <div class="pull-right cart-actions--print">
                            <div class="cart__actions--enquiry">
                                <button type="submit" class="btn btn-primary pull-right btn--send-enquiry"
                                        id="submit" data-title='<spring:theme code="text.enquiry.created.title" />'>
                                    <spring:theme code="enquiry.send"/>
                                </button>
                            </div>
                        </div>
                    </div>

                    <%--            <div class="row">--%>
                    <%--                <div class="col-xs-12 pull-right cart-actions--print">--%>
                    <%--                    <div class="cart__actions--enquiry">--%>
                    <%--                        <div class="row">--%>
                    <%--                            <div class="col-sm-4 col-md-4">--%>
                    <%--                                <input type = "submit" value = "Submit" class="btn btn-primary btn-block btn--send-enquiry js-send-enquiry-button" disabled="true"--%>
                    <%--                                        data-SendEnquiryUrl="${sendEnquiryUrl}" id="createEnquiryModalLink" data-title='<spring:theme code="text.enquiry.created.title" />'>--%>
                    <%--                                    <spring:theme code="enquiry.send"/>--%>
                    <%--                                </input>--%>
                    <%--                            </div>--%>
                    <%--                        </div>--%>
                    <%--                    </div>--%>
                    <%--                </div>--%>
                    <%--            </div>--%>

                </form:form>
            </div>

        </div>
        <div class="hidden-xs hidden-sm col-md-1"></div>
        <div class="col-xs-12 col-md-5">
            <div class="enquiry_sub_headline">
                <h2><spring:theme code="enquiry.page.summary.headline"/></h2>
            </div>

            <ul class="item__list item__list__enquiry">
                <li class="hidden-xs hidden-sm">
                    <ul class="item__list--header">
                        <li class="item__toggle"></li>
                        <li class="item__image"></li>
                        <li class="item__info"><spring:theme code="enquiry.page.item"/></li>
                        <li class="item__quantity__enquiry"><spring:theme code="enquiry.page.qty"/></li>
                    </ul>
                </li>

                <c:forEach items="${cartEntries}" var="entry" varStatus="loop">
                    <tr>
                        <td>
                            <li class="item__list--item">
                                    <%-- product image --%>
                                <div class="item__image">
                                    <product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
                                </div>

                                    <%-- product name, code --%>
                                <div class="item__info">
                                    <ycommerce:testId code="cart_product_name">
                                        <span class="item__name">${fn:escapeXml(entry.product.name)}</span>
                                    </ycommerce:testId>

                                    <div class="item__code">${fn:escapeXml(entry.product.code)}</div>
                                </div>

                                    <%-- quantity --%>
                                <div class="item__quantity--enquiry">${entry.quantity}</div>
                            </li>
                        </td>
                    </tr>
                </c:forEach>

                <c:url value="/inquiryform" var="sendEnquiryUrl" scope="session"/>
                <c:url value="/cart" var="cartPageUrl" scope="session"/>

                <div class="row">
                    <div class="cart-actions--print">
                        <div class="cart__actions--enquiry">
                            <button class="btn btn-default pull-right btn--cancel-enquiry js-cancel-enquiry-button"
                                    data-cancelEnquiryUrl="${cartPageUrl}">
                                <spring:theme code="enquiry.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
    </div>

    <div class="hidden">
        <div id="enquiryCreatedModal" class="enquiry-modal">

            <div class="enquiry_modal_text">
                <spring:theme code="text.enquiry.created.successfully" />
            </div>

            <a href="#" class="close_button">
                <button id="enquiry_created_modal_close_button" class="form btn-default btn btn-block modal-close" type="button">
                    <spring:theme code="text.enquiry.modal.close"/>
                </button>
            </a>
        </div>
    </div>
</div>
