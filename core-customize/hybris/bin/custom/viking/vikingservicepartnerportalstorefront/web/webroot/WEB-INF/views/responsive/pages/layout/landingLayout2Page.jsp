<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
    <div class="container home-page__slots">
        <div class="col-xs-12 col-md-3 text-center margin-bottom-20">
            <cms:pageSlot position="Section2A" var="feature" element="div" class="">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-3 text-center margin-bottom-20">
            <cms:pageSlot position="Section2B" var="feature" element="div" class="">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-3 text-center margin-bottom-20">
            <cms:pageSlot position="Section2C" var="feature" element="div" class="">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-3 text-center margin-bottom-20">
            <cms:pageSlot position="Section2D" var="feature" element="div" class="">
                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
            </cms:pageSlot>
        </div>
    </div>

    <div class="container">
        <cms:pageSlot position="Section3NewestNews" var="feature" element="div">
            <cms:component component="${feature}" element="div" class="clr col-xs-12 hp-text-comp yComponentWrapper"/>
        </cms:pageSlot>

        <cms:pageSlot position="Section3" var="feature" element="div">
            <cms:component component="${feature}" element="div" class="clr col-xs-12 hp-text-comp yComponentWrapper"/>
        </cms:pageSlot>
    </div>

    <div class="pad-bottom-40">
        <cms:pageSlot position="Section4" var="feature" element="div" class="row no-margin">
            <cms:component component="${feature}" element="div" class="clr container no-space yComponentWrapper"/>
        </cms:pageSlot>
    </div>
    <cms:pageSlot position="Section5" var="feature" element="div">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>

</template:page>
