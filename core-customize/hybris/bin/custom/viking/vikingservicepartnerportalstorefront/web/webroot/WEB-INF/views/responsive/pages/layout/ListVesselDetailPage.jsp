<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">

    <div class="container vessel-search s customer-portal-vessel-search">

        <h2><strong><spring:theme code="text.vesseldetailpage.title" /></strong></h2>

        <div class="vessel-header vessel-header-frame">
            <div class="col-sm-3">
                <h3>${vesselName}</h3>
            </div>
            <div class="col-sm-3">
                <h3>IMO: ${imoNumber}</h3>
            </div>
            <div class="col-sm-3">
                <h3>Flag state: ${flagState}</h3>
            </div>
        </div>


        <div class="vessel-results-header clearfix">
            <div class="col-xs-4 col-sm-6 vessel-results-header-title">
                <spring:theme code="text.vesseldetailpage.equipment.title" /> (${totalEquipments})
            </div>
            <div class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
                <div id="sortVesselAction" class="vessel-results-header__icon vesselSortLink" title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
                <div id="filterVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i></div>
            </div>
            <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text" placeholder='<spring:theme code="text.vessel.search.placeholder" />'></div>
        </div>
        <ul class="clr vessel-results-list clearfix">
            <li class="vessel-results-list-header">
                <div class="col-sm-2"><spring:theme code="text.vesseldetailpage.equipment.equipment" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.quantity" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.certstatus" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.action" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.nextinspect" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.service" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.eta" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.etd" /></div>
                <div class="col-sm-1"><spring:theme code="text.vesseldetailpage.equipment.port" /></div>
                <c:choose>
                    <c:when test="${showAllCertificates}">
                        <div class="col-sm-2" align="center"><spring:theme code="text.vesseldetailpage.equipment.link.to.certificates" /></div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-sm-2" align="center"><spring:theme code="text.vesseldetailpage.equipment.lastcert" /></div>
                    </c:otherwise>
                </c:choose>

            </li>
        </ul>

        <ul id="vesselMaintab" class="clr vessel-results-list clearfix service-list">
            <c:forEach var="entry" items="${vesselDataMap}">
                <li >
                <div class="col-sm-2 vessel-category"><i class="glyphicon glyphicon-chevron-down"></i><span class="vessel-category-name" style="font-size: 22px; margin-left: 10px;">${entry.key}</span></div>
                    <ul id="vesseltab" class="vessel-category-list clr vessel-results-list clearfix service-list vessel-sort-default">
                        <c:forEach items="${entry.value}" var="vessel">
                            <c:set var="statusColor" value="" />
                            <c:choose>
                                <c:when test="${vessel.certificateStatus eq 'red'}" >
                                    <c:set var="statusColor" value=" status-red" />
                                </c:when>
                                <c:when test="${vessel.certificateStatus eq 'yellow'}" >
                                    <c:set var="statusColor" value=" status-yellow" />
                                </c:when>
                                <c:when test="${vessel.certificateStatus eq 'green'}" >
                                    <c:set var="statusColor" value=" " />
                                </c:when>
                                <c:otherwise>
                                    <c:set var="statusColor" value=" status-black" />
                                </c:otherwise>
                            </c:choose>
                            <li class="${statusColor}">
                                <div class="col-sm-2 vessel-name vessel-repeat-entry"><span>${vessel.equipmentDescription}</span></div>
                                <div class="col-sm-1 vessel-repeat-entry"><span>${vessel.quantity}</span></div>
                                <div class="col-sm-1 vessel-col-padding large-vessel-icon vessel-repeat-entry">
                                <c:choose>
                                    <c:when test="${vessel.certificateStatus eq 'green' }"><i class="glyphicon glyphicon-ok-circle"><span class="hidden">1</span></i></c:when>
                                    <c:when test="${vessel.certificateStatus eq 'red' }"><i class="glyphicon glyphicon-remove-circle status-red"><span class="hidden">3</span></i></c:when>
                                    <c:when test="${vessel.certificateStatus eq 'yellow' }"><i class="glyphicon glyphicon-remove-circle status-yellow"><span class="hidden">2</span></i></c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                                </div>
                                <div class="col-sm-1 vessel-col-padding vessel-repeat-entry">
                                    <c:if test="${vessel.actionRequired eq 'true'}">
                                        <span>Yes</span>
                                    </c:if>
                                    <c:if test="${vessel.actionRequired eq 'false'}">
                                        <span>No</span>
                                    </c:if>
                                </div>
                                <div class="col-sm-1 vessel-repeat-entry">
                                    <span>${vessel.nextInspection}</span>
                                </div>
                                <div class="col-sm-1 vessel-col-padding vessel-repeat-entry">
                                    <c:if test="${vessel.serviceBooked eq 'true'}">
                                        <span>Yes</span>
                                    </c:if>
                                    <c:if test="${vessel.serviceBooked eq 'false'}">
                                        <span>No</span>
                                    </c:if>
                                </div>
                                <div class="col-sm-1 vessel-repeat-entry"><span>${vessel.eta}</span></div>
                                <div class="col-sm-1 vessel-repeat-entry"><span>${vessel.etd}</span></div>
                                <div class="col-sm-1 vessel-repeat-entry"><span>${vessel.bookedPort}</span></div>
                                <div class="col-sm-2 vessel-repeat-entry" align="center">
                                    <c:if test="${not empty vessel.lastCertificate}">
                                        <a href="${vessel.lastCertificate}" target="_blank"><i class="glyphicon glyphicon-file"></i></a>
                                    </c:if>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                </li>
            </c:forEach>
        </ul>
    </div>
</template:page>
