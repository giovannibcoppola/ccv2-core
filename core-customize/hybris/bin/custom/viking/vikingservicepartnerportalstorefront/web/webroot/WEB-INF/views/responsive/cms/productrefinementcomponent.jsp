<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="product-facet" class="hidden-sm hidden-xs product__facet js-product-facet">
        <c:if test="${not empty searchPageData.subCategories}">
            <nav:categoryNavLinks categories="${searchPageData.subCategories}"/>
        </c:if>
        <c:if test="${isEnquiryFlow}">
            <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
            <nav:facetNavRefinements pageData="${searchPageData}"/>
        </c:if>
</div>
