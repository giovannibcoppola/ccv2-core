<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:if test="${crossSellingProducts.size()>0}">
	<div class="container">
		<div class="carousel__component">
			<div class="carousel__component--headline">Related Products</div>
			<div class="carousel__component--carousel js-owl-carousel js-owl-default">
				<c:forEach items="${crossSellingProducts}" var="product">
					<c:url value="${product.url}" var="productUrl"/>

					<div class="carousel__item">
						<a href="${productUrl}">
							<div class="carousel__item--thumb">
								<product:productPrimaryImage product="${product}" format="product"/>
							</div>
							<div class="carousel__item--name">${fn:escapeXml(product.name)}</div>
							<div class="carousel__item--price"><format:fromPrice priceData="${product.price}"/></div>
						</a>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</c:if>
