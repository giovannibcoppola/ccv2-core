<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="vikingPageGridSection1" var="feature">
		<cms:component component="${feature}" element="div" class="" />
	</cms:pageSlot>

	<div class="clr container pad-content-container">
		<div class="row">
			<cms:pageSlot position="vikingPageGridSection2A" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2B" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2C" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2D" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2E" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2F" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2G" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2H" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2I" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2J" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2K" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPageGridSection2L" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>

		<div class="row">
			<cms:pageSlot position="vikingPageGridSection3" var="feature" element="div" class="clr col-xs-12">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>
	</div>

</template:page>
