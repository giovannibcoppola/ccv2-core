<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="row">
			<div class="col-xs-3">
				<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="search-grid-page-left-refinements-slot">
					<cms:component component="${feature}" element="div" class="search-grid-page-left-refinements-component"/>
				</cms:pageSlot>
			</div>

			<div class="col-sm-12 col-md-9">

                <c:if test="${not empty spellingData}">
			    <cms:pageSlot position="spell-check" var="feature" element="div" class="search-grid-page-result-grid-slot">
                    <c:set var="content" value="${fn:replace(feature.content, '{0}', searchPageData.freeTextSearch)}"/>
                    <c:set var="content" value="${fn:replace(content, '{1}', spellingData)}"/>
                    ${content}
                </cms:pageSlot>
                </c:if>
				<cms:pageSlot position="SearchResultsGridSlot" var="feature" element="div" class="search-grid-page-result-grid-slot">
					<cms:component component="${feature}" element="div" class="search-grid-page-result-grid-component"/>
				</cms:pageSlot>
			</div>
		</div>

		<storepickup:pickupStorePopup />
	</div>
</template:page>
