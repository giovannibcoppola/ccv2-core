<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="Section0" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<cms:pageSlot position="Section1" var="feature" element="div" class="container">
		<cms:component component="${feature}" element="div" class="col-xs-12" />
	</cms:pageSlot>
	<div class="container pad-bottom-pagefooter">
	    <div class="col-xs-12 col-md-3 margin-bottom-20">
            <cms:pageSlot position="Section2A" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12 col-md-9 margin-bottom-20">
            <cms:pageSlot position="Section2B" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 yComponentWrapper"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12">
            <cms:pageSlot position="Section2C" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 yComponentWrapper margin-bottom-20"/>
            </cms:pageSlot>
        </div>
        <div class="col-xs-12">
            <cms:pageSlot position="Section2D" var="feature" element="div" class="row">
                <cms:component component="${feature}" element="div" class="col-xs-12 yComponentWrapper margin-bottom-20"/>
            </cms:pageSlot>
        </div>
	</div>

	<cms:pageSlot position="Section3" var="feature" element="div">
		<cms:component component="${feature}" element="div" class="clr container no-space yComponentWrapper"/>
	</cms:pageSlot>
</template:page>
