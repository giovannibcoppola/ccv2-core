<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product/accordion"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty product.drawings}">
	<input type="checkbox" id="product-accordion-drawings" />
	<label for="product-accordion-drawings">
		<h3>${fn:escapeXml(title)}</h3>
		<i class="glyphicon glyphicon-chevron-down"></i>
	</label>
	<div class="product-accordion-item">
		<product:drawings product="${product}" />
	</div>
</c:if>
