<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ycommerce:testId code="productApproval_content_label">
	<c:if test="${not empty product.approvalAuthorities}">
		<ul class="product-approvals-list">
			<c:forEach items="${product.approvalAuthorities}" var="authorities">
				<li>${ycommerce:sanitizeHTML(authorities)}</li>
			</c:forEach>
		</ul>
	</c:if>
</ycommerce:testId>
