<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<ycommerce:testId code="productDetails_content_label">
	${ycommerce:sanitizeHTML(product.longDescription)}
</ycommerce:testId>
