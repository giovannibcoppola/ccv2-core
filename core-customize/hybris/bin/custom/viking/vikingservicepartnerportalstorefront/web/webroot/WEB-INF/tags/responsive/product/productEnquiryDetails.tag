<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="product-details">
    <div class="product-short-description">
        ${ycommerce:sanitizeHTML(product.teaser)}
    </div>

    <c:if test="${not empty product.usps}">
        <ul class="product-usp-list">
            <c:forEach items="${product.usps}" var="usp">
                <li>${ycommerce:sanitizeHTML(usp)}</li>
            </c:forEach>
        </ul>
    </c:if>

    <c:if test="${not empty product.longDescription}">
        <p class="product-read-more">
            <a href="#product-accordion-long-description-x">
                <spring:theme code="product.text.button.read.more" />
            </a>
        </p>
    </c:if>
</div>
