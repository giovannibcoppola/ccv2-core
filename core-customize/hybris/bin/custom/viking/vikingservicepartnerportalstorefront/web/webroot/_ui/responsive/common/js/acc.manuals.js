ACC.manuals = {

    _autoload: [
        "init",
        "getDocument",
        ["bulletinsLink", $(".bulletins").length != 0],
        ["bulletinId", $(".bulletinId").length != 0],
        "toogle"
    ],

    init: function () {

    },
    
    getDocument: function () {
        $(".azureDownload").on('click', function () {
            var url = $(this).attr("data-url");
            var legacy = $(this).attr("data-legacy");
            var fileName = $(this).attr("file-name");
            var fileExtensionRequired = $(this).attr("fileExtensionRequired");

            if (fileExtensionRequired == null) {
                fileExtensionRequired = true;
            }

            if (legacy == null) {
                legacy = false;
            }

            var request = new XMLHttpRequest();
            request.open("GET", ACC.config.encodedContextPath + '/service-manuals/download?url=' + url + '&legacy=' + legacy + '&extensionReq=' + fileExtensionRequired + '&filename=' + fileName, true);
            request.send();

            request.onreadystatechange = function () {
                if (this.readyState == this.HEADERS_RECEIVED) {
                    var location = request.getResponseHeader("location");

                    if (location != null) {
                        var a = document.createElement('a');
                        a.href = location;
                        a.target ="_blank";
                        a.click();
                    }
                }
            }
        });
    },

    bulletinsLink: function () {
        $(".bulletins").colorbox({
            close:'<i class="glyphicon glyphicon-remove"></i>',
            inline:true,
            href: function () {
                var bulletinId = $(this).attr('bulletinId');
                var replacedBulletinId = bulletinId.replace(".", "-");
                return "#createConfirmationModal-" + replacedBulletinId;
            },
            width:"800px",
            height:"800px",
            onComplete: function(){
                var title = "Confirmation Letter";
                $('#cboxTitle').html('<div class="headline"><span class="headline-text">' + title + '</span></div>');

                var bulletinId = $(this).attr('bulletinId');
                var replacedBulletinId = bulletinId.replace(".", "-");

                var text = $(this).attr("confirmationText");
                $("#confirmationLetterText-" + replacedBulletinId).html(text);
            }
        });
        $(document).on("click",".modal-close", function(){
            ACC.colorbox.close();
        });

        $("#createConfirmationSubmit").on('click', function () {
            var sParameters;

            sParameters = "&bulletinsId=" + $(this).attr("bulletinId");
            sParameters += "&manualsId=" + $(this).attr("manualId");

            $.ajax({
                type: 'POST',
                url:  ACC.config.contextPath + '/service-manuals/confirmationLetterAccept',
                data: sParameters,
                dataType : 'json',
                encode: true
            })
            // using the done promise callback
                .done(function (response) {
                    window.history.go(-1);
                })
                .fail(function () {
                    window.location.reload();
                });
        });
    },

    toogle: function () {
        $(".toogle").on('click', function () {
            if(document.getElementById("group").classList.contains("accordion-toggle") == true){
                document.getElementById("group").classList.add("accordion-toggle-out");
                document.getElementById("group").classList.remove("accordion-toggle");

            }else{
                document.getElementById("group").classList.add("accordion-toggle");
                document.getElementById("group").classList.remove("accordion-toggle-out");
            }
        });
    }
};