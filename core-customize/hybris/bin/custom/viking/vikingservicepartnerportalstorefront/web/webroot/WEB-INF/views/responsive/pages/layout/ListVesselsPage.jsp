<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}">

    <div class="container vessel-search customer-portal-vessel-search">

        <h2><spring:theme code="text.vesselpage.title.list" /></h2>

        <div class="vessel-results-header clearfix">
            <div class="col-xs-4 col-sm-6 vessel-results-header-title">
                <spring:theme code="text.vesselpage.title.list" /> (${fn:length(vesselsData)})
            </div>
            <div class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
                <div id="sortVesselAction" class="vessel-results-header__icon vesselSortLink" title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
                <div id="filterVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i></div>
            </div>
            <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text" placeholder='<spring:theme code="text.vessel.search.placeholder" />'></div>
        </div>
        <ul class="clr vessel-results-list clearfix">
            <li class="vessel-results-list-header">
                <div class="col-sm-3"><spring:theme code="text.vesselpage.table.column.name" /></div>
                <div class="col-sm-2"><spring:theme code="text.vesselpage.table.column.imo" /></div>
                <div class="col-sm-3"><spring:theme code="text.vesselpage.table.column.certifcatestatus" /></div>
                <div class="col-sm-2"><spring:theme code="text.vesselpage.table.column.actionrequired" /></div>
                <div class="col-sm-2"><spring:theme code="text.vesselpage.table.column.bookservice" /></div>
            </li>
        </ul>

        <ul id="vesseltab" class="clr vessel-results-list clearfix vessel-sort-default">
            <c:forEach items="${vesselsData}" var="vesselData">
                <c:set var="statusColor" value="" />
                <c:choose>
                    <c:when test="${vesselData.certificateStatus eq 'red'}" >
                        <c:set var="statusColor" value=" status-red" />
                    </c:when>
                    <c:when test="${vesselData.certificateStatus eq 'yellow'}" >
                        <c:set var="statusColor" value=" status-yellow" />
                    </c:when>
                    <c:when test="${vesselData.certificateStatus eq 'green'}" >
                        <c:set var="statusColor" value=" " />
                    </c:when>
                    <c:otherwise>
                        <c:set var="statusColor" value=" status-black" />
                    </c:otherwise>
                </c:choose>
                <li class="${statusColor}">
                ${ycommerce:encodeUrl(result.serialNo)}
                    <a href="${contextPath}/vesselPage?imoNumber=${vesselData.imo}">
                        <div class="col-sm-3 vessel-name vessel-repeat-entry">
                            <span>${vesselData.name}</span>
                        </div>
                        <div class="col-sm-2 vessel-repeat-entry">
                            <span>${vesselData.imo}</span>
                        </div>
                        <div class="col-sm-3 vessel-col-padding large-vessel-icon vessel-repeat-entry">
                            <c:choose>
                                <c:when test="${vesselData.certificateStatus eq 'green' }"><i class="glyphicon glyphicon-ok-circle"><span class="hidden">1</span></i></c:when>
                                <c:when test="${vesselData.certificateStatus eq 'red' }"><i class="glyphicon glyphicon-remove-circle status-red"><span class="hidden">3</span></i></c:when>
                                <c:when test="${vesselData.certificateStatus eq 'yellow' }"><i class="glyphicon glyphicon-remove-circle status-yellow"><span class="hidden">2</span></i></c:when>
                                <c:otherwise>
                                    <span class="hidden"></span>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-sm-2 vessel-col-padding vessel-repeat-entry">
                            <c:if test="${vesselData.actionRequired eq 'true'}">
                                <span>Yes</span>
                            </c:if>
                            <c:if test="${vesselData.actionRequired eq 'false'}">
                                <span>No</span>
                            </c:if>
                        </div>
                <c:if test="${vesselData.actionRequired eq 'true'}">
                    <div class="col-sm-2 vessel-repeat-entry book-service-link vessel-col-padding" data-url="${vesselData.bookServiceUrl}">
                    <i class="glyphicon glyphicon-new-window"></i>
                    </div>
                </c:if>
                </a>
                </li>
            </c:forEach>
        </ul>

    </div>

</template:page>
