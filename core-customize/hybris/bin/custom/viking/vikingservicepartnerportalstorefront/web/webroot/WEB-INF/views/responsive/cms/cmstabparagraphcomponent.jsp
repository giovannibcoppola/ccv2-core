<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="tabhead">
	<a href="">${fn:escapeXml(component.title)}</a><span class="glyphicon"></span>
</div>
<div class="tabbody">

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-container">
					${ycommerce:sanitizeHTML(component.content)}
				</div>
			</div>
		</div>
	</div>
</div>
