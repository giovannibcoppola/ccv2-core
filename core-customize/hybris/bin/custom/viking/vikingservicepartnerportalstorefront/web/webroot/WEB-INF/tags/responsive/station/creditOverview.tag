<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="container credit-overview">

    <h2><spring:theme code="text.station.credit.overview.title" /></h2>

    <div class="row credit-overview-header">
        <div class="col-xs-12 pad-bottom-20">
            <spring:theme code="text.station.credit.overview.explanation"  />
        </div>
    </div>

    <div class="row credit-overview-header">
        <div class="col-xs-6 pad-bottom-20">
            <spring:theme code="text.station.credit.overview.stationid" arguments=" ${stationId}" />
        </div>
    </div>

    <ul class="clr credit-overview-list vessel-results-list clearfix notification-list">
        <li class="credit-overview-list-header vessel-results-list-header">
            <div align="left" class="col-sm-1 col-lg-1 text-center-desktop"><spring:theme code="text.station.credit.list.material.status" /></div>
            <div align="left" class="col-sm-3 col-lg-2 text-center-desktop"><spring:theme code="text.station.credit.list.material.number" /></div>
            <div align="left" class="col-sm-2 col-lg-1"><spring:theme code="text.station.credit.list.credit" /></div>
            <div align="left" class="col-sm-4 col-lg-4"><spring:theme code="text.station.credit.list.equipment" /></div>
        </li>
    </ul>
    <ul id="credittab" class="clr credit-overview-list vessel-results-list clearfix credit-list">
        <c:forEach items="${credits}" var="credit">
            <li>
                <div align="left" class="col-sm-1 col-lg-1 text-center-desktop"><p>
                <c:choose>
                    <c:when test="${credit.credit eq 0 }"><i class="glyphicon glyphicon-asterisk" style="color: #FF0000"><span class="hidden">ok</span></i></c:when>
                    <c:when test="${credit.credit lt 10 }"><i class="glyphicon glyphicon-asterisk" style="color: #FFB347"><span class="hidden">ok</span></i></c:when>
                    <c:otherwise>
                        <i class="glyphicon glyphicon-asterisk" style="color: #008000"><span class="hidden">remove</span></i>
                    </c:otherwise>
                </c:choose>
                </p>
                </div>
                <div align="left" class="col-sm-3 col-lg-2 number-responsive text-center-desktop"><p><strong>${credit.materialNumber}</strong></p></div>
                <div align="left" class="col-sm-2 col-lg-1 credit-responsive"><p><span>${credit.credit}</span></p></div>
                <div align="left" class="col-sm-4 col-lg-4"><p>${credit.equipment}</p></div>
            </li>
        </c:forEach>
    </ul>

</div>
