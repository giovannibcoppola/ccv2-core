ACC.sortlist = {

    _autoload: [
        ["bindSortList", $(".sort-list").length != 0],
        ["bindTechnicianListDefaultSort", $(".technician-overview-list-header").length != 0]
    ],

    bindSortList: function () {
        var $sortup = '<span class="glyphicon glyphicon-triangle-top sort-up"></span>';
        var $sortdown = '<span class="glyphicon glyphicon-triangle-bottom sort-down"></span>';
        var $sortreq = [];
        $('.sort-list').append($sortup);
        $('.sort-list').append($sortdown);

        $('.sort-list').on("click",function(){
            var _this = $(this);
            $(this).closest('li').children("div").each(function(index){
                if(_this.parent('div')[0] == $(this)[0]){
                    $sortreq["index"] = index;
                    if(_this.find('.active').length == 0){
                        _this.find('.sort-up').addClass('active');
                        $sortreq["type"] = "up";
                    }
                    else{
                        if(_this.find('.sort-up.active').length > 0){
                            _this.find('.sort-up').removeClass('active');
                            _this.find('.sort-down').addClass('active');
                            $sortreq["type"] = "down";
                        }
                        else if(_this.find('.sort-down.active').length > 0){
                            _this.find('.sort-up').addClass('active');
                            _this.find('.sort-down').removeClass('active');
                            $sortreq["type"] = "up";
                        }
                    }
                }
                else{
                    $(this).find('.active').removeClass("active");
                }
            });

            ACC.sortlist.sortAlgorithm($sortreq);
        });
    },

    sortAlgorithm: function(sortreq){

        if(sortreq["type"]==='up'){
            flag='ascending';
        }
        else{
            flag='descending';
        }
        var rows, switching, i, listx, listy, shouldSwitch, x1, y1;
        var $sortabletab = $(".sort-result-list");
        switching = true;
        while (switching) {
            switching = false;
            rows = $sortabletab.find("li");
            for (i = 0; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                listx = rows[i].getElementsByClassName("sortable-line")[sortreq["index"]];
                listy = rows[i+1].getElementsByClassName("sortable-line")[sortreq["index"]];
                x1=listx.getElementsByClassName("sortable-content")[0].innerHTML;
                y1=listy.getElementsByClassName("sortable-content")[0].innerHTML;
                if(flag==='ascending'){
                    if (x1.toLowerCase() > y1.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
                else{
                    if (x1.toLowerCase() < y1.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }

    },

    bindTechnicianListDefaultSort:function() {
        var $sortreq = [];
        $sortreq["type"] = "up";
        $sortreq["index"] = 0;
        ACC.sortlist.sortAlgorithm($sortreq);
        $('.sort-list:first').find('.sort-up').addClass('active');
    }


};