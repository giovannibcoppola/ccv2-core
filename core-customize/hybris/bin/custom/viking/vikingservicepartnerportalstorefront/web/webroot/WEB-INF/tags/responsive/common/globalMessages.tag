<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<c:if test="${(not empty accConfMsgs) || (not empty accInfoMsgs) || (not empty accErrorMsgs)}">
	<div class="global-alerts">

		<c:choose>
			<c:when test="${customerPortalLook}">

				<%-- Information (confirmation) messages --%>
				<c:if test="${not empty accConfMsgs}">
					<c:forEach items="${accConfMsgs}" var="msg">
						<div class="alert alert-info alert-dismissable getAccAlert">
							<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
							<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
						</div>
					</c:forEach>
				</c:if>

				<%-- Warning messages --%>
				<c:if test="${not empty accInfoMsgs}">
					<c:forEach items="${accInfoMsgs}" var="msg">
						<div class="alert alert-warning alert-dismissable getAccAlert">
							<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
							<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
						</div>
					</c:forEach>
				</c:if>

				<%-- Error messages (includes spring validation messages)--%>
				<c:if test="${not empty accErrorMsgs}">
					<c:forEach items="${accErrorMsgs}" var="msg">
						<div class="alert alert-danger alert-dismissable getAccAlert">
							<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
							<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
						</div>
					</c:forEach>
				</c:if>

			</c:when>
			<c:otherwise>

				<%-- Information (confirmation) messages --%>
				<c:if test="${not empty accConfMsgs}">
					<c:forEach items="${accConfMsgs}" var="msg">
						<div class="alert alert-info alert-dismissable getAccAlert">
							<div class="container">
								<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
								<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
							</div>
						</div>
					</c:forEach>
				</c:if>

				<%-- Warning messages --%>
				<c:if test="${not empty accInfoMsgs}">
					<c:forEach items="${accInfoMsgs}" var="msg">
						<div class="alert alert-warning alert-dismissable getAccAlert">
							<div class="container">
								<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
								<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
							</div>
						</div>
					</c:forEach>
				</c:if>

				<%-- Error messages (includes spring validation messages)--%>
				<c:if test="${not empty accErrorMsgs}">
					<c:forEach items="${accErrorMsgs}" var="msg">
						<div class="alert alert-danger alert-dismissable getAccAlert">
							<button class="close closeAccAlert" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>
							<spring:theme code="${msg.code}" arguments="${msg.attributes}"/>
						</div>
					</c:forEach>
				</c:if>
			</c:otherwise>
		</c:choose>

	</div>
</c:if>