ACC.vessels = {

	_autoload: [
        "init",
		["vesselSearch", $("#vessel-sort-container").length != 0],
        ["createVessel", $("#createVesselModal").length != 0],
        ["rfcVesselChange", $("#rfcVesselName").length != 0],
		["addEquipmentButton", $("#addEquipmentButton").length != 0],
		["searchEquipmentButton", $("#searchEquipmentButton").length != 0],
        ["dismantleEquipmentLink", $(".dismantle-equipment-link").length != 0]
    ],

    init: function () {

    },

    dismantleEquipmentLink: function(){
        $(document).on("click","a .dismantle-equipment-link",function(e){
            e.preventDefault();

            var url = $(this).attr("data-url");
            $.get( url, { } )
                .done(function( data ) {
                    location.reload();
                });

        });
    },

     rfcVesselChange: function () {
    		  $("#changeVesselRFC").on('click', function(e){
            			e.preventDefault();
            		                        var vesselID=$("#rfcVesselID").val();
                                            var description = $("#rfcVesselName").val();
                                            var flagState=$("#rfcFlagState").val();
                                            var signal=$("#rfcCallSignal").val();
                                    				var formData = {
                                    				    'vesselID' :vesselID,
                                    					'description' : description,
                                    					'flagState' : flagState,
                                    					'signal' : signal
                                    				};
                                    				$.ajax({
                                    					type : 'POST',
                                    					url : ACC.config.contextPath + '/vesselSearch/changeVesselRFC',
                                    					data : formData,
                                    					dataType : 'json',
                                    					encode : true
                                    				})
                                    				// using the done promise callback
                                    				.done(function(response) {
                                                        window.location.reload();
                                    				})
                                    				.fail(function() {
                                    					window.location.reload();
                                    				});

            		});

        },

	vesselSearch: function () {

			var flag='ascending';
			//document.getElementById("search-input-container").style.display='none';
		    $("#searchInput").on("keyup", function() {
		        var value = $(this).val().toLowerCase();
		        $("#vesseltab li").filter(function() {
		            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
		        });
		    });

			function FilterVessel(){
			    /*var x = document.getElementById("search-input-container");
			    if (x.style.display === "none") {
			        x.style.display = "block";
			    } else {
			        x.style.display = "none";
			    }*/
				var $searchContainer = $("#search-input-container");
				var $searchIcon = $('#filterVesselAction.vessel-results-header__icon');
			    if ($searchContainer.hasClass('open')) {
			        $searchContainer.slideUp().removeClass('open');
					$searchIcon.removeClass('hover');
			    } else {
			        $searchContainer.slideDown().addClass('open');
					$searchIcon.addClass('hover');
			    }
			}
			function SortVessel(param) {
			    if(flag==='ascending'){
			        flag='descending';
			    }
			    else{
			        flag='ascending';
			    }

			    var table, rows, switching, i, listx, listy, shouldSwitch, x1, y1;
			    vesseltab = document.getElementById("vesseltab");
			    switching = true;

			    while (switching) {
			        switching = false;
			        rows = vesseltab.querySelectorAll("li")
			        for (i = 0; i < (rows.length - 1); i++) {
			            shouldSwitch = false;
                        if(param==="notification"){
                            listx = rows[i].getElementsByClassName("col-sm-3")[0];
                            listy = rows[i+1].getElementsByClassName("col-sm-3")[0];
                        }
			            if(param==="vessel"){
			                listx = rows[i].getElementsByClassName("col-sm-3")[0];
			                listy = rows[i+1].getElementsByClassName("col-sm-3")[0];
			            }
			            if(param==="flag"){
			                listx= rows[i].getElementsByClassName("col-sm-3")[1];
			                listy= rows[i+1].getElementsByClassName("col-sm-3")[1];
			            }
			            if(param==="call"){
			                listx= rows[i].getElementsByClassName("col-sm-3")[2];
			                listy = rows[i+1].getElementsByClassName("col-sm-3")[2];
			            }
			            if(param==="owner"){
			                listx= rows[i].getElementsByClassName("col-sm-3")[3];
			                listy= rows[i+1].getElementsByClassName("col-sm-3")[3];
			            }
			            x1=listx.getElementsByTagName("p")[0].innerHTML;
			            y1=listy.getElementsByTagName("p")[0].innerHTML;
			            if(flag==='ascending'){
			                if (x1.toLowerCase() > y1.toLowerCase()) {
			                    shouldSwitch = true;
			                    break;
			                }
			            }
			            else{
			                if (x1.toLowerCase() < y1.toLowerCase()) {
			                    shouldSwitch = true;
			                    break;
			                }
			            }
			        }
			        if (shouldSwitch) {
			            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			            switching = true;
			        }
			    }

			}


			if($(".sort-by-notification").length>0){
				SortVessel('notification');
			}

        	$("#sortVesselAction").on('click', function(e){
				e.preventDefault();
				SortVessel('vessel');
			});

			$("#filterVesselAction").on('click', function(e){
				e.preventDefault();
				FilterVessel();
			});

    },

    addEquipmentButton: function () {
        $("#addEquipmentButton").on('click', function(e) {
            e.preventDefault();

            var serialnumber = encodeURIComponent(document.getElementById("addEquipmentSerialNo").value);
            var vesselID = encodeURIComponent(document.getElementById("addEquipmentVesselID").value);
            var type = encodeURIComponent(document.getElementById("addEquipmentType").value);

            window.location.replace(ACC.config.contextPath + '/vesselSearch/addEquipment/' + vesselID + '?serialnumber=' + serialnumber + '&add=true' + '&type=' + type);
        });
    },

    searchEquipmentButton: function () {
        $("#searchEquipmentButton").on('click', function(e){
            e.preventDefault();

            var serialnumber = encodeURIComponent(document.getElementById("serialnumber").value);
            var vesselID = encodeURIComponent(document.getElementById("addEquipmentVesselID").value);

            window.location.replace(ACC.config.contextPath + '/vesselSearch/addEquipment/' + vesselID + '?serialnumber=' + serialnumber + '&add=false');
        });
    },

    createVessel: function () {
		$("#createVesselModalLink").colorbox({
			close:'<i class="glyphicon glyphicon-remove"></i>',
			inline:true,
			href:"#createVesselModal",

			onComplete: function(){
	            var title = $(this).data("title");
	            $('#cboxTitle').html('<div class="headline"><span class="headline-text">' + title + '</span></div>');
	        }
		});
		$(document).on("click",".modal-close", function(){
            ACC.colorbox.close();
        });

        $("#createVesselSubmit").on('click', function(e){
			e.preventDefault();

			var description = $("#createVesselDesc").val();
            var flagState=$("#createVesselFlagState").val();
            var signal=$("#createVesselSignal").val();
            document.getElementById("createVesselCancel").disabled = true;
            document.getElementById("createVesselSubmit").disabled = true;
				var formData = {
					'description' : description,
					'flagState' : flagState,
					'signal' : signal
				};
				$.ajax({
					type : 'POST',
					url : ACC.config.contextPath + '/vesselSearch/createVesselRFC',
					data : formData,
					dataType : 'json',
					encode : true
				})
				// using the done promise callback
				.done(function(response) {

                    alert("Created Vessel ID is : "+ response.vesselID+"\n \n \nMessage : "+response.message);
                   // $("#lblVesselId").val(response.vesselID);
                    //document.getElementById("lblVesselId").innerHTML=response.vesselID;
                    window.location.reload();
				})
				.fail(function() {
					alert('error');
					window.location.reload();
				});

		});

    }
};
