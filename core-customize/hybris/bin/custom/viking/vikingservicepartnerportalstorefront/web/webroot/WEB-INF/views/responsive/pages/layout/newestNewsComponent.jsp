<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<div class="latestNews">
    <c:if test="${news.size()>0}">
        <h2>Latest News: </h2>
        <ul>
            <c:forEach items="${news}" var="news">
                <li><a href="news?newsId=${news.pk}"><span> ${news.headline}</span></a></li>
            </c:forEach>
        </ul>
    </c:if>
</div>


