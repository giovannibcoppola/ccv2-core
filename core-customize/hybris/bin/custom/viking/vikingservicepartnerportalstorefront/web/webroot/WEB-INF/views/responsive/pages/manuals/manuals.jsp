<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-12">
            <h1 style="color:#35625B;"><spring:theme code="text.manuals.overview.manual"/>&nbsp;${name}</h1>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 ">
        <div class="col-md-6">
            <h3><spring:theme code="text.manuals.bulletins"/></h3>
            <div class="bulletins-section-content">
                <table class="manuals-table" width="100%">
                    <tbody>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td class="checklist_header" colspan="4">New bulletins you need to confirm:</td>
                    </tr>
                    <c:set var="count" value="0" scope="page" />
                    <c:forEach items="${bulletins}" var="bulletin">
                        <c:if test="${bulletin.accepted == false}">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr class="bulletins" bulletinId="${bulletin.id}" confirmationText="${bulletin.confirmationLetter.text}">
                                <td>${bulletin.viewDate}</td>
                            </tr>

                            <div class="hidden">
                                <div id="createConfirmationModal-${fn:replace(bulletin.id, '.', '-')}" class="confirmation-modal">
                                    <div id="confirmationLetterText-${fn:replace(bulletin.id, '.', '-')}">

                                    </div>
                                    <br/>
                                    <div class="bulletins_table">
                                        <div class="bulletin_accepted_headline"><b><spring:theme code="bulletins.confirmation.attached.documents"/></b></div>
                                        <div class="bulletin_accepted_row">
                                            <div class="bulletinsaccepted azureDownload" data-url="${bulletin.uri}" data-legacy="${bulletin.legacy}" file-name="${bulletin.id}">
                                                <div>${bulletin.viewDate}</div>
                                            </div>
                                        </div>
                                        <div class="bulletin_attachment_rows">
                                            <c:forEach items="${bulletin.confirmationLetter.attachments}" var="attachment">
                                                <div class="bulletin_attachment_row bulletinsaccepted azureDownload" data-url="${attachment.uri}" data-legacy="${bulletin.legacy}" file-name="${attachment.id}">
                                                    <div>${attachment.name}</div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="createConfirmationSubmit" bulletinId="${bulletin.id}"
                                                manualId="${manuals.id}">Accept</button>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                    <c:if test="${count == 0}">
                        <tr>
                            <td>No new bulletins</td>
                        </tr>
                    </c:if>
                    </tbody>
                </table>
                <br><br><br>
                <div>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a id="group" class="accordion-toggle toogle" data-toggle="collapse" data-parent="#accordion" href="javascript:void(0)">
                                        <b style="display:inline;">Previously confirmed bulletins</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <c:forEach items="${bulletins}" var="bulletin">
                                        <c:if test="${bulletin.accepted == true}">
                                            <c:choose>
                                                <c:when test="${bulletin.legacy == true}">
                                                    <div class="bulletinsaccepted azureDownload" data-url="${bulletin.uri}" file-name="${bulletin.id}" data-legacy="${bulletin.legacy}">
                                                        <div>${bulletin.viewDate}</div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="bulletinsaccepted azureDownload" data-url="${bulletin.uri}" file-name="${bulletin.id}" data-legacy="${bulletin.legacy}">
                                                        <div>${bulletin.viewDate}</div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h3><spring:theme code="text.manuals.manuals"/></h3>
            <div class="manuals-section-content">
                <table class="manuals-table" width="100%">
                    <tbody>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <c:forEach items="${manuals.chapters}" var="chapter">
                        <tr class="manuals-section-header">
                            <td><b>${chapter.title}</b></td>
                            <td>Section</td>
                            <td colspan="2" height="5">Edition</td>
                        </tr>

                        <c:forEach items="${chapter.sections}" var="section">
                            <div class="section">
                                <tr>
                                    <td><b>&nbsp;&nbsp;${section.title}</b></td>
                                </tr>
                            </div>

                            <c:forEach items="${section.subSections}" var="subsection">
                                <div>
                                    <tr class="subsection azureDownload" data-url="${subsection.uri}"
                                        file-name="${subsection.fileId}">
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;${subsection.title}</td>
                                        <td>${subsection.section}</td>
                                        <td>${subsection.edition}</td>
                                        <c:choose>
                                            <c:when test="${subsection.isModified eq true}">
                                                <td>
                                                    <i class="glyphicon glyphicon-red-dot"></i>
                                                        <%--<img src="${commonResourcePath}/images/vwicn098.gif" height="20" /> --%>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td>&nbsp;</td>
                                            </c:otherwise>
                                        </c:choose>
                                    </tr>
                                </div>
                            </c:forEach>

                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr valign="top">
        <td width="100%" bgcolor="#505a69" height="3">
            <div align="center"><b><font color="#FFFFFF">Copyright to this manual belongs to VIKING LIFE-SAVING
                EQUIPMENT A/S.</font></b><br>
                <b><font color="#FFFFFF">All content is confidential and not to be used in other contexts.</font></b>.
            </div>
        </td>
    </tr>
    </tbody>
</table>