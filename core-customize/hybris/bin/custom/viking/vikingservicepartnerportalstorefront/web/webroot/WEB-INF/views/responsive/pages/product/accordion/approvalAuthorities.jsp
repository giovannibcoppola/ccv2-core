<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product/accordion"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty product.approvalAuthorities}">
	<input type="checkbox" id="product-accordion-approval-authorities" />
	<label for="product-accordion-approval-authorities">
		<h3><spring:theme code="product.approvals" /></h3>
		<i class="glyphicon glyphicon-chevron-down"></i>
	</label>
	<div class="product-accordion-item">
		<product:approvalAuthorities product="${product}" />
	</div>
</c:if>
