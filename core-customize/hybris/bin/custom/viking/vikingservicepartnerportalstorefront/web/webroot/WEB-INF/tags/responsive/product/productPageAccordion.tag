<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<div class="product-accordion-container">
	<cms:pageSlot position="Accordion" var="accordion">
		<cms:component component="${accordion}" />
	</cms:pageSlot>
</div>
