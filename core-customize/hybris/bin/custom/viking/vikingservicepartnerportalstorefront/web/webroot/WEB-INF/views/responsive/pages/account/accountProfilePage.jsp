<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-12">
            <spring:theme code="text.account.profile.personalDetails"/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 ">
        <div class="col-md-6">
            <h3><spring:theme code="text.account.profile.approvedFacility"/></h3>
            <div class="account-section-content">
                <div class="account-section-form">
                    <form:form method="GET" action="#" modelAttribute="displayApprovedFacilityForm">
                        <div class="row form-group">
                            <label for="company" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.company" />:</label>
                            <input type="text" class="col-md-6" readonly name="companyName" id="company" value="${approvedFacilityData.name}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="address1" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.address1" />:</label>
                            <input type="text" class="col-md-6" readonly name="address1" id="address1" value="${approvedFacilityData.address1}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="address2" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.address2" />:</label>
                            <input type="text" class="col-md-6" readonly name="address2" id="address2" value="${approvedFacilityData.address2}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="address3" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.address3" />:</label>
                            <input type="text" class="col-md-6" readonly name="address3" id="address3" value="${approvedFacilityData.address3}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="postalCode" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.postalCode" />:</label>
                            <input type="text" class="col-md-6" readonly name="postalCode" id="postalCode" value="${approvedFacilityData.postalCode}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="city" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.city" />:</label>
                            <input type="text" class="col-md-6" readonly name="city" id="city" value="${approvedFacilityData.city}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="country" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.country" />:</label>
                            <input type="text" class="col-md-6" readonly name="country" id="country" value="${approvedFacilityData.country}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="phoneCountryCode" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.phoneCountryCode" />:</label>
                            <input type="text" class="col-md-6" readonly name="phoneCountryCode" id="phoneCountryCode" value="${approvedFacilityData.phoneCountryCode}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="phone" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.phone" />:</label>
                            <input type="text" class="col-md-6" readonly name="phone" id="phone" value="${approvedFacilityData.phone}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="faxCountryCode" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.faxCountryCode" />:</label>
                            <input type="text" class="col-md-6" readonly name="faxCountryCode" id="faxCountryCode" value="${approvedFacilityData.faxCountryCode}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="fax" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.fax" />:</label>
                            <input type="text" class="col-md-6" readonly name="fax" id="fax" value="${approvedFacilityData.fax}" style="border: 0"/>
                        </div>
                        <!-- start-->
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.primaryEmail" />:</label>
                            <input type="text" class="col-md-6" readonly name="email" id="email" value="${approvedFacilityData.primaryEmail}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.additionalEmail2" />:</label>
                            <input type="text" class="col-md-6" readonly name="addtionalEmail2" id="additionalEmail2" value="${approvedFacilityData.additionalEmail2}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.additionalEmail3" />:</label>
                            <input type="text" class="col-md-6" readonly name="addtionalEmail3" id="additionalEmail3" value="${approvedFacilityData.additionalEmail3}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.additionalEmail4" />:</label>
                            <input type="text" class="col-md-6" readonly name="addtionalEmail4" id="additionalEmail4" value="${approvedFacilityData.additionalEmail4}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.additionalEmail5" />:</label>
                            <input type="text" class="col-md-6" readonly name="addtionalEmail5" id="additionalEmail5" value="${approvedFacilityData.additionalEmail5}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.additionalEmail6" />:</label>
                            <input type="text" class="col-md-6" readonly name="addtionalEmail6" id="additionalEmail6" value="${approvedFacilityData.additionalEmail6}" style="border: 0"/>
                        </div>
                        <!-- end-->
                        <div class="row form-group">
                            <label for="website" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.website" />:</label>
                            <input type="text" class="col-md-6" readonly name="website" id="website" value="${approvedFacilityData.website}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="serviceStationManager" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.serviceStationManager" />:</label>
                            <input type="text" class="col-md-6" readonly name="serviceStationManager" id="serviceStationManager" value="${approvedFacilityData.serviceStationManager}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="serviceStationPhone" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.serviceStationPhone" />:</label>
                            <input type="text" class="col-md-6" readonly name="serviceStationPhone" id="serviceStationPhone" value="${approvedFacilityData.serviceStationPhone}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="serviceStationEmail" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.serviceStationEmail" />:</label>
                            <input type="text" class="col-md-6" readonly name="serviceStationEmail" id="serviceStationEmail" value="${approvedFacilityData.serviceStationEmail}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="afterHoursPhone" class="col-md-4 control-label"><spring:theme code="text.account.profile.approvedFacility.afterHoursPhone" />:</label>
                            <input type="text" class="col-md-6" readonly name="afterHoursPhone" id="afterHoursPhone" value="${approvedFacilityData.afterHoursPhone}" style="border: 0"/>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h3><spring:theme code="text.account.profile.invoiceAddress"/></h3>
            <div class="account-section-content">
                <div class="account-section-form">
                    <form:form method="GET" action="#" modelAttribute="displayAddress">
                        <div class="row form-group">
                            <label for="companyName" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.company" />:</label>
                            <input type="text" class="col-md-6" readonly name="companyName" id="companyName" value="${addressData.companyName}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="address" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.address" />:</label>
                            <input type="text" class="col-md-6" readonly name="address" id="address" value="${addressData.line1}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="postalCode" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.postalCode" />:</label>
                            <input type="text" class="col-md-6" readonly name="postalCode" id="postalCode" value="${addressData.postalCode}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="city" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.city" />:</label>
                            <input type="text" class="col-md-6" readonly name="city" id="city" value="${addressData.town}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="country" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.country" />:</label>
                            <input type="text" class="col-md-6" readonly name="country" id="country" value="${addressData.country.name}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="phone" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.phone" />:</label>
                            <input type="text" class="col-md-6" readonly name="phone" id="phone" value="${addressData.phone}" style="border: 0"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme code="text.account.profile.invoiceAddress.email" />:</label>
                            <input type="text" class="col-md-6" readonly name="email" id="email" value="${addressData.email}" style="border: 0"/>
                        </div>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-4 col-sm-push-4" >
    <div class="accountActions">
        <ycommerce:testId code="personalDetails_savePersonalDetails_button">
            <%--<a href="${contextPath}/my-account/update-profile">--%>
           <button type="submit" class="btn btn-primary btn-block" onclick="window.location.href='${contextPath}/my-account/update-profile'"/>
               <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
           </button>

       </ycommerce:testId>
   </div>
</div>

