<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="vikingPage1Section1" var="feature">
		<cms:component component="${feature}" element="div" class="" />
	</cms:pageSlot>

	<div class="clr container pad-content-container">
		<cms:pageSlot position="vikingPage1Section2" var="feature">
			<cms:component component="${feature}" element="div" class="col-xs-12 margin-bottom-20" />
		</cms:pageSlot>

		<div class="row">
			<cms:pageSlot position="vikingPage1Section3A" var="feature" element="div" class="col-sm-8 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>

			<cms:pageSlot position="vikingPage1Section3B" var="feature" element="div" class="col-sm-4 margin-bottom-20">
                <cms:component component="${feature}" />
            </cms:pageSlot>
		</div>

		<cms:pageSlot position="vikingPage1Section4" var="feature">
			<cms:component component="${feature}" element="div" class="clr col-xs-12" />
		</cms:pageSlot>
	</div>

</template:page>
