<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<div class="row">
		<div class="col-sm-12">
			<div class="product-details page-title product-info">
				<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
					<h2 class="name">${fn:escapeXml(product.name)}</h2>
					<h3 class="name"><span class="sku"><spring:theme code="product.itemNumber" /></span><span class="code">${fn:escapeXml(product.code)}</span></h3>
				</ycommerce:testId>
				<%--ATP <fmt:formatDate value="${product.stock.stockDate}" pattern="dd-MM-yyyy"/>--%>
			</div>
            <product:productEnquiryDetails  />
            <c:if test="${!(themeName eq 'sigma')}">
                <c:choose>
                    <c:when test="${product.stock.stockRange eq 1}">
                        <c:set var="color" value="green"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="color" value="yellow"/>
                    </c:otherwise>
                </c:choose>
                <div class="product-stock-status clearfix">
                    <span class="left"><spring:theme code="product.stock.status" /></span> <div class="stock-selector" style="background-color: ${color};"></div>
                </div>
                <div class="product-details">
                    <product:productPromotionSection product="${product}"/>
                    <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                        <product:productPricePanel product="${product}" />
                    </ycommerce:testId>
                    <div class="description">${ycommerce:sanitizeHTML(product.summary)}</div>
                        <%--<product:productReviewSummary product="${product}" showLinks="true"/>--%>
                </div>
            </c:if>

			<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
				<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
			</cms:pageSlot>

			<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
				<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
			</cms:pageSlot>
		</div>
	</div>
