<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="image-gallery js-gallery product-image-gallery">
    <c:choose>
        <c:when test="${galleryImages == null || galleryImages.size() == 0}">
            <div class="carousel image-gallery__image js-gallery-image">
                <div class="item">
                    <div>
                        <spring:theme code="img.missingProductImage.responsive.product" text="/" var="imagePath"/>
                        <c:choose>
                            <c:when test="${originalContextPath ne null}">
                                <c:choose>
                                    <c:when test='${fn:startsWith(imagePath, originalContextPath)}'>
                                        <c:url value="${imagePath}" var="imageUrl" context="/"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <c:url value="${imagePath}" var="imageUrl" />
                            </c:otherwise>
                        </c:choose>
                        <img class="lazyOwl" src="${imageUrl}"/>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="carousel image-gallery__image js-gallery-image">
                <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
                    <div class="item">
                        <div>
                            <c:choose>
                                <c:when test="${not empty container.thumbnail.youtubeVideoId}">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="lazyOwl embed-responsive-item youtubeVideo" src="https://www.youtube.com/embed/${container.thumbnail.youtubeVideoId}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <%-- href="${container.product.url}" --%>
                                    <a class="js-show-big-image">
                                        <img class="lazyOwl" src="${container.product.url}"
                                             data-zoom-image="${not empty container.superZoom.url ? container.superZoom.url : container.product.url}"
                                             alt="${fn:escapeXml(container.thumbnail.altText)}" />
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <product:productGalleryThumbnail galleryImages="${galleryImages}" />
        </c:otherwise>
    </c:choose>
</div>
