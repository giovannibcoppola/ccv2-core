ACC.vessels = {

	_autoload: [
        "init",
		["vesselSearch", $("#filterVesselAction").length != 0],
        ["userInfo", $("#vesselModal").length != 0],
        ["bookServiceLink", $(".book-service-link").length != 0],
		["vesselCategoryToggle", $(".vessel-category").length != 0]
    ],

    init: function () {

    },

    bookServiceLink: function(){
        $(document).on("click","a .book-service-link",function(e){
            e.preventDefault();

            var url = $(this).attr("data-url");
            window.open(url);
        });
    },

	vesselSearch: function () {

		var flag='ascending';
	    $("#searchInput").on("keyup", function() {
	        var value = $(this).val().toLowerCase();
	        $("#vesseltab li").filter(function() {
	            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
	        });
	    });

		function FilterTable(){
			var $searchContainer = $("#search-input-container");
			var $searchIcon = $('#filterVesselAction.vessel-results-header__icon');
		    if ($searchContainer.hasClass('open')) {
		        $searchContainer.slideUp().removeClass('open');
				$searchIcon.removeClass('hover');
		    } else {
		        $searchContainer.slideDown().addClass('open');
				$searchIcon.addClass('hover');
		    }
		}

		function SortTable(n) {
		    if(flag==='ascending'){
		        flag='descending';
		    }
		    else{
		        flag='ascending';
		    }
		    var table, rows, switching, i, listx, listy, shouldSwitch, x1, y1;
		   vesseltab = document.getElementById("vesseltab");

				switching = true;
				while (switching) {
					switching = false;
					rows = value.querySelectorAll("li")
					console.log('insided loop table rows' + rows);
					for (i = 0; i < (rows.length - 1); i++) {
						shouldSwitch = false;
						listx = rows[i].getElementsByClassName("vessel-user-detail")[0];
						listy = rows[i + 1].getElementsByClassName("vessel-user-detail")[0];

						x1 = listx.getElementsByTagName("span")[0].innerHTML;
						y1 = listy.getElementsByTagName("span")[0].innerHTML;
						if (flag === 'ascending') {
							if (x1.toLowerCase() > y1.toLowerCase()) {
								shouldSwitch = true;
								break;
							}
						} else {
							if (x1.toLowerCase() < y1.toLowerCase()) {
								shouldSwitch = true;
								break;
							}
						}
					}
					if (shouldSwitch) {
						rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
						switching = true;
					}
				}
		}

		function SortVessel(param) {
			if(flag==='ascending'){
				flag='descending';
			}
			else{
				flag='ascending';
			}

			var table, rows, switching, i, listx, listy, shouldSwitch, x1, y1;
			$('[id^=vesseltab]').each(function(index, value) {
				switching = true;
				while (switching) {
					switching = false;
					rows = $(value).find("li")
					for (i = 0; i < (rows.length - 1); i++) {
						shouldSwitch = false;
						if (param === "vessel") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[0];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[0];
						}
						if (param === "flag") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[1];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[1];
						}
						if (param === "call") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[2];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[2];
						}
						if (param === "certificate") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[2];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[2];
						}
						if (param === "owner") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[3];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[3];
						}
						if (param === "actionRequired") {
							listx = rows[i].getElementsByClassName("vessel-repeat-entry")[3];
							listy = rows[i + 1].getElementsByClassName("vessel-repeat-entry")[3];
						}
						x1 = listx.getElementsByTagName("span")[0].innerHTML;
						y1 = listy.getElementsByTagName("span")[0].innerHTML;
						if (flag === 'ascending') {
							if (x1.toLowerCase() > y1.toLowerCase()) {
								shouldSwitch = true;
								break;
							}
						} else {
							if (x1.toLowerCase() < y1.toLowerCase()) {
								shouldSwitch = true;
								break;
							}
						}
					}
					if (shouldSwitch) {
						rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
						switching = true;
					}
				}
			})
		}

		$("#sortVesselAction.userSortLink").on('click', function(e){
			e.preventDefault();
			SortTable(0);
		});
        if($("#vesseltab.vessel-sort-default").length> 0){
            SortVessel('certificate');
        }
		$("#sortVesselAction.vesselSortLink").on('click', function(e){
			e.preventDefault();
			SortVessel('certificate');
		});

		$("#filterVesselAction").on('click', function(e){
			e.preventDefault();
			FilterTable();
		});

    },

    userInfo: function () {
        $("#vesselModalLink").colorbox({
            close:'<i class="glyphicon glyphicon-remove"></i>',
            inline:true,
            href:"#vesselModal",
            height:"500px",
            onComplete: function(){
                $('#vesselID').multipleSelect({
                    placeholder: "SELECT"
                });
                var title = $(this).data("title");
                $('#cboxTitle').html('<div class="headline"><span class="headline-text">' + title + '</span></div>');
            }
        });
        $(document).on("click",".modal-close", function(){
            ACC.colorbox.close();
        });

    },

	vesselCategoryToggle: function () {
		$('.vessel-category').click(function (e) {
			e.preventDefault();
			$(this).closest("li").find("[class^='glyphicon']").toggleClass('glyphicon-chevron-down glyphicon-chevron-right');
			$(this).closest("li").find("[class^='vessel-category-list']").slideToggle(10);
		});
	}

};
