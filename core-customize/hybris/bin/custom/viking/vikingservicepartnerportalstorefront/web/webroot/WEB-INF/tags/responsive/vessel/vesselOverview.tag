<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<div class="container vessel-search">

    <div class="row">
            <div class="col-sm-7">
                <h2><spring:theme code="text.vessel.overview.title" /></h2>
            </div>
    </div>
 <c:if test="${searchVesselOverviewData ne 'empty'}">
    <div class="row vessel-header">
        <div class="col-sm-8">
            <div class="row form-group">
                <label for="vesselName" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.overview.name" />:</label>
                <div class="col-sm-8 col-md-9 col-lg-10">
                    <input type="text" name="vesselName" id="rfcVesselName" placeholder="<spring:theme code="text.vessel.overview.name" />" class="form-control" value="${searchVesselOverviewData.vesselName}"/>
                </div>
            </div>
            <div class="row form-group">
                <label for="formerVesselName" class="col-sm-4 col-md-3 col-lg-2 control-label"><spring:theme code="text.vessel.overview.formerName" />:</label>
                <div class="col-sm-8 col-md-9 col-lg-10">
                   <c:forEach items="${searchVesselOverviewData.formerName}" var="formerResult">
                                                     ${formerResult.formerName} &nbsp
                   </c:forEach>
                </div>
            </div>
            <div class="row form-group">
                <label for="flagState" class="col-sm-4 col-md-3 col-lg-2 control-label"><spring:theme code="text.vessel.overview.flag" />:</label>
                <div class="col-sm-8 col-md-9 col-lg-10">
                    <select class="form-control input-sm config-selector config_type" name="rfcFlagState" id="rfcFlagState">
                        <!--<option selected="${searchVesselOverviewData.flagState}"><spring:theme code="text.createvessel.select" /></option>-->
                        <c:forEach items="${flagStates}" var="flagState" >
                            <c:choose>
                                <c:when test="${searchVesselOverviewData.flagState == flagState.name}" >
                                    <option selected="selected" value="${flagState.code}">${flagState.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${flagState.code}">${flagState.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <label for="callSignal" class="col-sm-4 col-md-3 col-lg-2 control-label label-input"><spring:theme code="text.vessel.overview.call" />:</label>
                <div class="col-sm-8 col-md-9 col-lg-10">
                    <input type="text" id="rfcCallSignal" name="callSignal" placeholder="<spring:theme code="text.vessel.overview.call" />" class="form-control" value="${searchVesselOverviewData.callSignal}"/>
                </div>
            </div>
            <div class="row form-group">
                <label for="owner" class="col-sm-4 col-md-3 col-lg-2 control-label"><spring:theme code="text.vessel.overview.owner" />:</label>
                <div class="col-sm-8 col-md-9 col-lg-10">
                    ${searchVesselOverviewData.owner}
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row form-group">
                <label for="vesselID" class="col-sm-4 control-label"><spring:theme code="text.vessel.overview.vesselId" />:</label>
                <div class="col-sm-8">
                    ${searchVesselOverviewData.vesselID}
                    <input type="hidden" name="rfcVesselID" id="rfcVesselID" value="${searchVesselOverviewData.vesselID}"/>
                </div>
            </div>
        </div>
        <div class="col-xs-8 pad-top-20">
            <button type="button" class="btn btn-primary"  id="changeVesselRFC"><spring:theme code="text.vessel.overview.savebutton" /></button> <br>
        </div>
        <div class="col-xs-4 pad-top-20" style="text-align: right">
            <a class="btn btn-primary" href="${contextPath}/vesselSearch/vesselOverview/vesseldoc?vesselID=${searchVesselOverviewData.vesselID}&vesselName=${searchVesselOverviewData.vesselName}"><spring:theme code="text.vessel.overview.vesseldoc.link" /></a>
        </div>
    </div>

    <div class="vessel-results-header clearfix">
        <div class="col-xs-4 col-sm-6 vessel-results-header-title">
            <spring:theme code="text.vessel.overview.eqlist" />
        </div>
        <div id="vessel-sort-container" class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
            <div id="sortVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
            <div id="filterVesselAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i></div>
            <div id="addEpuipmentAction" class="vessel-results-header__icon" title='<spring:theme code="text.vessel.search.add" />' data-title='<spring:theme code="text.vessel.equipment.modal.title"/>'>
                <a href="${request.contextPath}/vesselSearch/addEquipment/${searchVesselOverviewData.vesselID}" >
                    <i class="glyphicon glyphicon-plus"></i>
                </a>
            </div>
            <%--<a href="#"><i class="glyphicon glyphicon-list-alt"></i></a>
            <a href="#"><i class="glyphicon glyphicon-cog"></i></a>--%>
        </div>
        <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text" placeholder='<spring:theme code="text.vessel.search.placeholder" />'></div>
    </div>
    <ul class="clr vessel-results-list clearfix">
        <li class="vessel-results-list-header">
            <div class="col-sm-3"><spring:theme code="text.vessel.overview.list.group" /></div>
            <div class="col-sm-3"><spring:theme code="text.vessel.overview.list.type" /></div>
            <div class="col-sm-2"><spring:theme code="text.vessel.overview.list.month" /></div>
            <div class="col-sm-2"><spring:theme code="text.vessel.overview.list.year" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.overview.list.dismantle" /></div>
        </li>
    </ul>
    <ul id="vesseltab" class="clr vessel-results-list clearfix">
        <c:forEach items="${entryList.vesselEquipmentDataList}" var="result">
            <li>
                <a href="${contextPath}/vesselSearch/serviceOverview?serialNo=${ycommerce:encodeUrl(result.serialNo)}&equipmentId=${result.equipmentID}&name=${ycommerce:encodeUrl(result.equipmentName)}&type=${fn:escapeXml(result.type)}">
                <div class="col-sm-3 vessel-name"><span>${result.equipmentName}</span>
                <p>${result.serialNo}</p></div>
                <div class="col-sm-3"><p>${result.type}</p></div>
                <div class="col-sm-2"><p>${result.month}</p></div>
                <div class="col-sm-2"><p>${result.year}</p></div>

                        <%--  <div class="col-sm-2"><a href="${request.contextPath}/vesselSearch/dismantleEquipment/${result.serialNo}?vesselID=${searchVesselOverviewData.vesselID}&signal=${searchVesselOverviewData.callSignal}"><i class="glyphicon glyphicon-trash"></i></a></div>--%>
                        <div class="col-sm-2 vessel-repeat-entry dismantle-equipment-link" data-url="${request.contextPath}/vesselSearch/dismantleEquipment/${ycommerce:encodeUrl(result.serialNo)}?vesselID=${searchVesselOverviewData.vesselID}&signal=${searchVesselOverviewData.callSignal}">
                            <i class="glyphicon glyphicon-trash"></i>
                        </div>

          </a>
      </li>
  </c:forEach>
</ul>
</c:if>
</div>
