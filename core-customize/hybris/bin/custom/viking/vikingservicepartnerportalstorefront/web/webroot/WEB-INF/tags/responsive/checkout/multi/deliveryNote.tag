<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<div class="delivery-note-headline">
	<span class="headline" for="deliveryNote">
		<spring:theme code="checkout.multi.deliveryMethod.deliveryNote" />
	</span>
</div>

<textarea class="form-control delivery-note-textArea" name="deliveryNote" maxlength="255" rows="2" >${cartData.deliveryNote}</textarea>

