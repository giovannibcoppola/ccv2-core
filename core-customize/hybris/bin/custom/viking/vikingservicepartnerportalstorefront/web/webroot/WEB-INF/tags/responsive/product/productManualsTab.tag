<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="tab-manuals">
	<ycommerce:testId code="productFiles_content_label">
		<c:forEach items="${product.manuals}" var="file">
			<li>
				<a href="${file.downloadURL}">
					<span> ${file.altText}</span>
				</a>
			</li>
		</c:forEach>
	</ycommerce:testId>
</div>