<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-12">
            <h1 style="color:#35625B;"><spring:theme code="text.checklists.overview.checklist"/>&nbsp;${name}</h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <h3><spring:theme code="text.checklists.checklist"/></h3>
        <div class="checklist-section-content">
            <table class="responsive-table-cell manuals-table" width="100%">
                <tbody>
                <tr>
                    <td colspan="4"></td>
                </tr>
                <c:forEach items="${checklists.children}" var="checklist">
                    <tr class="subsection azureDownload" onclick="" onmouseover="this.bgColor='#E8E8E8'"
                        onmouseout="this.bgColor='white'" data-url="${checklist.uri}" file-name="${checklist.name}" data-legacy="true" fileExtensionRequired="false">
                        <td>${checklist.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

        </div>
    </div>
</div>
<br><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr valign="top">
        <td width="100%" bgcolor="#505a69" height="3">
            <div align="center"><b><font color="#FFFFFF">Copyright to this checklist belongs to VIKING LIFE-SAVING
                EQUIPMENT A/S.</font></b><br>
                <b><font color="#FFFFFF">All content is confidential and not to be used in other contexts.</font></b>.
            </div>
        </td>
    </tr>
    </tbody>
</table>