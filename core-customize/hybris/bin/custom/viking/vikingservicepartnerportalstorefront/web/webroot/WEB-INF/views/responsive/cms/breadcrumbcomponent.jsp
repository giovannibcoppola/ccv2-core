<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>

<c:if test="${fn:length(breadcrumbs) > 0}">

	<c:choose>
		<c:when test="${customerPortalLook}">
			<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
		</c:when>
		<c:otherwise>
			<div class="breadcrumb-section">
				<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
			</div>
		</c:otherwise>
	</c:choose>

</c:if>