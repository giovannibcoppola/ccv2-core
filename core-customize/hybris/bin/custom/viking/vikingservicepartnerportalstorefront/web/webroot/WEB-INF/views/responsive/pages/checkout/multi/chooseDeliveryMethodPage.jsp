<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

<div class="row">
    <div class="col-sm-6">
        <div class="checkout-headline">
            <span class="glyphicon glyphicon-lock"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<ycommerce:testId code="checkoutStepTwo">
					<div class="checkout-shipping">
						<multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" />
						<div class="checkout-indent">
							<div class="ponumber row">
								<div class="form-group">
									<div class="col-lg-12">
										<span class="control-label headline" for="ponumber">
											<spring:theme code="text.account.order.orderDetails.purchaseOrderNumber" />
										</span>
										<input id="ponumber" name="purchaseOrderNumber" class="form-control"  type="text" value="${cartData.purchaseOrderNumber}"/>
									</div>
								</div>
							</div>
							<div class="headline"><spring:theme code="checkout.summary.deliveryMode.selectDeliveryMethodForOrder" /></div>
							<c:choose>
								<c:when test="${isOnlyCertificates}" >
									<p><spring:theme code="checkout.multi.deliveryMethod.certificates.only" htmlEscape="false"/></p>
                                    </br>
									<form id="selectDeliveryMethodForm" action="${request.contextPath}/checkout/multi/delivery-method/select" method="get" class="hidden">
										<div class="form-group">
											<multi-checkout:deliveryMethodSelector deliveryMethods="${deliveryMethods}" selectedDeliveryMethodId="${cartData.deliveryMode.code}"/>
										</div>
										<div class="form-group">
											<multi-checkout:deliveryNote  cartData="${cartData}"/>
										</div>
									</form>
								</c:when>
								<c:otherwise>
									<form id="selectDeliveryMethodForm" action="${request.contextPath}/checkout/multi/delivery-method/select" method="get">
										<div class="form-group">
											<multi-checkout:deliveryMethodSelector deliveryMethods="${deliveryMethods}" selectedDeliveryMethodId="${cartData.deliveryMode.code}"/>
										</div>
										<div class="form-group">
											<multi-checkout:deliveryNote  cartData="${cartData}"/>
										</div>
									</form>
								</c:otherwise>
							</c:choose>
							<p><spring:theme code="checkout.multi.deliveryMethod.message1" htmlEscape="false"/></p>
							<p><spring:theme code="checkout.multi.deliveryMethod.message2" htmlEscape="false"/></p>
							<p><spring:theme code="checkout.multi.deliveryMethod.message3" htmlEscape="false"/></p>
						</div>
					</div>
					<button id="deliveryMethodSubmit" type="button" class="btn btn-primary btn-block checkout-next"><spring:theme code="checkout.multi.deliveryMethod.continue"/></button>
				</ycommerce:testId>
			</jsp:body>
		</multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
    </div>

    <div class="col-sm-12 col-lg-12">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>

</template:page>
