<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="container">
    <c:choose>
        <c:when test="${not empty vesselName}">
            <h1><spring:theme code="text.vesseldoc.overview.headline" arguments="${vesselName}"/></h1>
        </c:when>
        <c:otherwise>
            <h1><spring:theme code="text.vesseldoc.overview.headline.no.name"/></h1>
        </c:otherwise>
    </c:choose>
    </br>

    <div class="row">
        <div class="col-md-6">
            <h4><spring:theme code="text.vesseldoc.operation.header"/></h4>
        </div>
    </div>
    <ul class="clr vessel-results-list clearfix">
        <li class="vessel-results-list-header">
            <div class="col-sm-5"><spring:theme code="text.vesseldoc.documents.list.name"/></div>
            <div class="col-sm-2"><spring:theme code="text.vesseldoc.documents.list.type"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.variant"/></div>
            <div class="col-sm-2"><spring:theme code="text.vesseldoc.documents.list.position"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.edition"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.uploaded"/></div>
        </li>
    </ul>
    <ul id="operationdoc" class="clr vessel-results-list clearfix manuals-table">
        <c:forEach items="${vesselOperationalDocs}" var="operationdoc">
            <li class="subsection azureDownload" onclick="" onmouseover="this.bgColor='#E8E8E8'"
                onmouseout="this.bgColor='white'"
                data-url="${operationdoc.downloadUrl}" file-name="${operationdoc.fileName}"  data-legacy="null" fileExtensionRequired="false">
                <div class="col-sm-5">${operationdoc.fileName}</div>
                <div class="col-sm-2">${operationdoc.systemType}</div>
                <div class="col-sm-1">${operationdoc.systemVariant}</div>
                <div class="col-sm-2">${operationdoc.mesPosition}</div>
                <div class="col-sm-1">${operationdoc.documentEdition}</div>
                <div class="col-sm-1">${operationdoc.uploaded}</div>
            </li>
        </c:forEach>
    </ul>

    </br>
    </br>

    <div class="row">
        <div class="col-md-6">
            <h4><spring:theme code="text.vesseldoc.installation.header"/></h4>
        </div>
    </div>
    <ul class="clr vessel-results-list clearfix ">
        <li class="vessel-results-list-header">
            <div class="col-sm-5"><spring:theme code="text.vesseldoc.documents.list.name"/></div>
            <div class="col-sm-2"><spring:theme code="text.vesseldoc.documents.list.type"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.variant"/></div>
            <div class="col-sm-2"><spring:theme code="text.vesseldoc.documents.list.position"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.edition"/></div>
            <div class="col-sm-1"><spring:theme code="text.vesseldoc.documents.list.uploaded"/></div>
        </li>
    </ul>
    <ul id="specificdoc" class="clr vessel-results-list clearfix manuals-table">
        <c:forEach items="${vesselInstallationDocs}" var="installationDoc">
            <li class="subsection azureDownload" onclick="" onmouseover="this.bgColor='#E8E8E8'"
                onmouseout="this.bgColor='white'"
                data-url="${installationDoc.downloadUrl}" file-name="${installationDoc.fileName}" data-legacy="null" fileExtensionRequired="false">
                <div class="col-sm-5">${installationDoc.fileName}</div>
                <div class="col-sm-2">${installationDoc.systemType}</div>
                <div class="col-sm-1">${installationDoc.systemVariant}</div>
                <div class="col-sm-2">${installationDoc.mesPosition}</div>
                <div class="col-sm-1">${installationDoc.documentEdition}</div>
                <div class="col-sm-1">${installationDoc.uploaded}</div>
            </li>
        </c:forEach>
    </ul>

    <br><br>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr valign="top">
            <td width="100%" bgcolor="#505a69" height="3">
                <div align="center"><b><font color="#FFFFFF">Copyright to this checklist belongs to VIKING LIFE-SAVING
                    EQUIPMENT A/S.</font></b><br>
                    <b><font color="#FFFFFF">All content is confidential and not to be used in other
                        contexts.</font></b>.
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>