ACC.enquiry = {

    _autoload: [
        "bindEnquiryButtons",
        "bindFieldValidation",
        "bindComment"
    ],


    bindEnquiryButtons: function () {
        $("#createEnquiryModalLink").colorbox({
            close:'<i class="glyphicon glyphicon-remove"></i>',
            inline:true,
            href:"#enquiryCreatedModal",

            onComplete: function(){
                var title = $(this).data("title");
                $('#cboxTitle').html('<div class="headline"><span class="headline-text">' + title + '</span></div>');
            }
        });
        $(document).on("click",".modal-close", function(){
            ACC.colorbox.close();
            window.location = ACC.config.encodedContextPath;
        });

        $('.js-cancel-enquiry-button').click(function () {
            var cartPageUrl = $(this).attr("data-cancelEnquiryUrl");
            window.location = cartPageUrl;
        });

        // $('.js-send-enquiry-button').click(function () {
        //     var sendEnquiryUrl = $(this).attr("data-sendEnquiryUrl");
        //
        //     var formData = {
        //         'company': $("#company").val(),
        //         'title': $("#title").val(),
        //         'name': $("#name").val(),
        //         'addressline': $("#address").val(),
        //         'postalcode': $("#postalCode").val(),
        //         'city': $("#city").val(),
        //         'country': $("#country").val(),
        //         'phone': $("#phone").val(),
        //         'email': $("#email").val(),
        //         'deliveryCountry': $("#countryIso").val(),
        //         'company': $("#company").val(),
        //         'hasComment': $("#hasComment").val(),
        //         'comment' : $("#comment").val()
        //     };
        //
        //     $.ajax({
        //         type: 'POST',
        //         url: sendEnquiryUrl,
        //         data: formData,
        //         encode: true
        //     }).done(function (response) {
        //     })
        // });
    },

    bindFieldValidation: function () {
        $('.enquiry_input_mandatory').keyup(function () {
            var allRequiredFieldsPopulated = false;
            var countEmptyInputFields = 0;

            $('.enquiry_input_mandatory').each(function () {
                if (!$(this).val() || 0 === $(this).val().length) {
                    countEmptyInputFields = countEmptyInputFields + 1;
                }
            });

            if (countEmptyInputFields == 0) {
                allRequiredFieldsPopulated = true;
            }

            if (allRequiredFieldsPopulated) {
                $('.js-send-enquiry-button').prop('disabled', false);
            } else {
                $('.js-send-enquiry-button').prop('disabled', true);
            }
        })
    },

    bindComment: function () {
        var comment = $('.enquiryForm-component #comment');
        comment.hide();

        $('.enquiryForm-component #yes, .enquiryForm-component #no').change(function () {
            if ($('.enquiryForm-component #yes')[0].checked) {
                comment.show();
            } else {
                comment.hide();
            }
        })
    }
};
