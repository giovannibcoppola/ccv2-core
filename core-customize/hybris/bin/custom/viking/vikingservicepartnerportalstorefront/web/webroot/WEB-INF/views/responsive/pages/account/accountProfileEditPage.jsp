<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<div class="account-section-header">
    <div class="row">
        <div class="container-lg col-md-12">
            <spring:theme code="text.account.profile.updatePersonalDetails"/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 ">
        <div class="col-md-6">
            <h3><spring:theme code="text.account.profile.approvedFacility"/></h3>
            <div class="account-section-content">
                <div class="account-section-form">
                    <form:form method="GET" action="#" modelAttribute="updateApprovedFacilityForm">
                        <div class="row form-group">
                            <label for="afcompany" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.company"/>:</label>
                            <input type="text" class="col-md-6" name="companyName" id="afcompany"
                                   value="${approvedFacilityData.name}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afaddress1" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.address1"/>:</label>
                            <input type="text" class="col-md-6" name="address1" id="afaddress1"
                                   value="${approvedFacilityData.address1}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afaddress2" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.address2"/>:</label>
                            <input type="text" class="col-md-6" name="address2" id="afaddress2"
                                   value="${approvedFacilityData.address2}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afaddress3" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.address3"/>:</label>
                            <input type="text" class="col-md-6" name="address3" id="afaddress3"
                                   value="${approvedFacilityData.address3}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afpostalCode" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.postalCode"/>:</label>
                            <input type="text" class="col-md-6" name="postalCode" id="afpostalCode"
                                   value="${approvedFacilityData.postalCode}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afcity" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.city"/>:</label>
                            <input type="text" class="col-md-6" name="city" id="afcity"
                                   value="${approvedFacilityData.city}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afcountry" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.country"/>:</label>
                            <input type="text" class="col-md-6" name="country" id="afcountry"
                                   value="${approvedFacilityData.country}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afphoneCountryCode" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.phoneCountryCode"/>:</label>
                            <input type="text" class="col-md-6" name="phoneCountryCode" id="afphoneCountryCode"
                                   value="${approvedFacilityData.phoneCountryCode}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afphone" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.phone"/>:</label>
                            <input type="text" class="col-md-6" name="phone" id="afphone"
                                   value="${approvedFacilityData.phone}"/>
                        </div>
                        <div class="row form-group">
                            <label for="affaxCountryCode" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.faxCountryCode"/>:</label>
                            <input type="text" class="col-md-6" name="faxCountryCode" id="affaxCountryCode"
                                   value="${approvedFacilityData.faxCountryCode}"/>
                        </div>
                        <div class="row form-group">
                            <label for="affax" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.fax"/>:</label>
                            <input type="text" class="col-md-6" name="fax" id="affax"
                                   value="${approvedFacilityData.fax}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afemail" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.primaryEmail"/>:</label>
                            <input type="text" class="col-md-6" name="email" id="afemail"
                                   value="${approvedFacilityData.primaryEmail}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afadditionalEmail2" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.additionalEmail2"/>:</label>
                            <input type="text" class="col-md-6" name="additionalEmail2" id="afadditionalEmail2"
                                   value="${approvedFacilityData.additionalEmail2}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afadditionalEmail3" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.additionalEmail3"/>:</label>
                            <input type="text" class="col-md-6" name="additionalEmail3" id="afadditionalEmail3"
                                   value="${approvedFacilityData.additionalEmail3}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afadditionalEmail4" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.additionalEmail4"/>:</label>
                            <input type="text" class="col-md-6" name="additionalEmail4" id="afadditionalEmail4"
                                   value="${approvedFacilityData.additionalEmail4}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afadditionalEmail5" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.additionalEmail5"/>:</label>
                            <input type="text" class="col-md-6" name="additionalEmail5" id="afadditionalEmail5"
                                   value="${approvedFacilityData.additionalEmail5}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afadditionalEmail6" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.additionalEmail6"/>:</label>
                            <input type="text" class="col-md-6" name="additionalEmail6" id="afadditionalEmail6"
                                   value="${approvedFacilityData.additionalEmail6}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afwebsite" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.website"/>:</label>
                            <input type="text" class="col-md-6" name="website" id="afwebsite"
                                   value="${approvedFacilityData.website}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afserviceStationManager" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.serviceStationManager"/>:</label>
                            <input type="text" class="col-md-6" name="serviceStationManager"
                                   id="afserviceStationManager" value="${approvedFacilityData.serviceStationManager}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afserviceStationPhone" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.serviceStationPhone"/>:</label>
                            <input type="text" class="col-md-6" name="serviceStationPhone"
                                   id="afserviceStationPhone" value="${approvedFacilityData.serviceStationPhone}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afserviceStationEmail" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.serviceStationEmail"/>:</label>
                            <input type="text" class="col-md-6" name="serviceStationEmail"
                                   id="afserviceStationEmail" value="${approvedFacilityData.serviceStationEmail}"/>
                        </div>
                        <div class="row form-group">
                            <label for="afafterHoursPhone" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.approvedFacility.afterHoursPhone"/>:</label>
                            <input type="text" class="col-md-6" name="afterHoursPhone" id="afafterHoursPhone"
                                   value="${approvedFacilityData.afterHoursPhone}"/>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <h3><spring:theme code="text.account.profile.invoiceAddress"/></h3>
            <div class="account-section-content">
                <div class="account-section-form">
                    <form:form method="GET" action="#" modelAttribute="updateAddressForm">
                        <div class="row form-group">
                            <label for="companyName" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.company"/>:</label>
                            <input type="text" class="col-md-6" name="companyName" id="company"
                                   value="${addressData.companyName}"/>
                        </div>
                        <div class="row form-group">
                            <label for="address" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.address"/>:</label>
                            <input type="text" class="col-md-6" name="address" id="address"
                                   value="${addressData.line1}"/>
                        </div>
                        <div class="row form-group">
                            <label for="postalCode" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.postalCode"/>:</label>
                            <input type="text" class="col-md-6" name="postalCode" id="postalCode"
                                   value="${addressData.postalCode}"/>
                        </div>
                        <div class="row form-group">
                            <label for="city" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.city"/>:</label>
                            <input type="text" class="col-md-6" name="city" id="city"
                                   value="${addressData.town}"/>
                        </div>
                        <div class="row form-group">
                            <label for="country" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.country"/>:</label>
                            <input type="text" class="col-md-6" name="country" id="country"
                                   value="${addressData.country.name}"/>
                        </div>
                        <div class="row form-group">
                            <label for="phone" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.phone"/>:</label>
                            <input type="text" class="col-md-6" name="phone" id="phone"
                                   value="${addressData.phone}"/>
                        </div>
                        <div class="row form-group">
                            <label for="email" class="col-md-4 control-label"><spring:theme
                                    code="text.account.profile.invoiceAddress.email"/>:</label>
                            <input type="text" class="col-md-6" name="email" id="email"
                                   value="${addressData.email}"/>
                        </div>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-lg col-md-6">
    <div class="account-section-content">
        <div class="account-section-form">
            <form:form action="#" method="post" modelAttribute="updateProfileForm">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-6">
                        <div class="accountUpdateAction">
                            <ycommerce:testId code="personalDetails_savePersonalDetails_button">
                                <button type="button" class="btn btn-primary btn-block" id="updateProfileSubmit">
                                    <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                </button>
                            </ycommerce:testId>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6">
                        <div class="accountUpdateAction">
                            <ycommerce:testId code="personalDetails_cancelPersonalDetails_button">
                                <button type="button" class="btn btn-default btn-block backToHome" id="updateProfileCancel">
                                    <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                </button>
                            </ycommerce:testId>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
