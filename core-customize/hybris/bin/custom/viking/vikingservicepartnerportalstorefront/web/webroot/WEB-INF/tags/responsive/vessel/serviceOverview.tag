<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="global-alerts">
    <div id="js-cookie-notification" class="alert hide alert-info alert-dismissable cookie-alert--top">
        <button class="js-cookie-notification-accept close" aria-hidden="true" data-dismiss="alert" type="button">
            <spring:theme code="text.cookie.notification.accept"/>
        </button>
        <p><spring:theme code="send.notification.to.ipad.message"/></p>
    </div>
</div>


<div class="container vessel-search">

    <h2><spring:theme code="text.vessel.service.title"/></h2>

    <div class="row vessel-header">
        <div class="col-xs-12 pad-bottom-20">
            <spring:theme code="text.vessel.service.serialNo"/>: ${serialNo} - ${equipmentName}
        </div>
    </div>

    <div class="vessel-results-header clearfix">
        <div class="col-xs-4 col-sm-6 vessel-results-header-title">
            <spring:theme code="text.vessel.service.list"/> (${fn:length(entryList)})
        </div>
        <div id="vessel-sort-container" class="col-xs-8 col-sm-6 vessel-results-header__actions text-right">
            <div id="sortVesselAction" class="vessel-results-header__icon"
                 title='<spring:theme code="text.vessel.search.sort" />'><i class="glyphicon glyphicon-sort"></i></div>
            <div id="filterVesselAction" class="vessel-results-header__icon"
                 title='<spring:theme code="text.vessel.search.filter" />'><i class="glyphicon glyphicon-filter"></i>
            </div>
            <%--<a href="#"><i class="glyphicon glyphicon-list-alt"></i></a>
            <a href="#"><i class="glyphicon glyphicon-cog"></i></a>--%>
        </div>
        <div id="search-input-container" class="col-xs-12"><input class="form-control" id="searchInput" type="text"
                                                                  placeholder='<spring:theme code="text.vessel.search.placeholder" />'>
        </div>
    </div>
    <ul class="clr vessel-results-list clearfix service-list">
        <li class="vessel-results-list-header sort-by-notification">
            <div class="col-sm-3"><spring:theme code="text.vessel.service.list.notification" /></div>
            <div class="col-sm-2"><spring:theme code="text.vessel.service.list.inspection" /></div>
            <div class="col-sm-2"><spring:theme code="text.vessel.service.list.nextinspection" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.service.list.certificate" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.service.list.edit" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.service.list.dated-item-list" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.service.list.ipad" /></div>
            <div class="col-sm-1"><spring:theme code="text.vessel.service.list.label" /></div>
        </li>
    </ul>
    <ul id="vesseltab" class="clr vessel-results-list clearfix service-list">
        <c:forEach items="${entryList}" var="result">
            <li>
                <div class="col-sm-3 service-name">
                    <c:choose>
                        <c:when test="${result.certificate}" >
                            <p><strong>${result.notificationNumber}</strong></p>
                        </c:when>
                        <c:otherwise>
                            <p class="hidden"><strong>${result.notificationNumber}</strong></p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-sm-2"><p>${result.inspectionDate}</p></div>
                <div class="col-sm-2"><p>${result.dateForNextInspection}</p></div>
                <div class="col-sm-1">
                    <c:if test="${result.certificate}">
                        <a href="${storefrontRoot}/vesselSearch/getCertificate?notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}&serialNo=${ycommerce:encodeUrl(serialNo)}" target="_blank">
                            <i class="glyphicon glyphicon-file"></i>
                        </a>
                    </c:if>
                </div>
                <div class="col-sm-1 service-edit no-pad-right">
                    <c:if test="${not empty result.openNotification}">
                        <a href="${storefrontRoot}/vesselSearch/loadForm?notificationId=${ycommerce:encodeUrl(result.notificationNumber)}&formId=${ycommerce:encodeUrl(type)}&serviceStation=${ycommerce:encodeUrl(serviceStation)}&prefillId=${ycommerce:encodeUrl(result.prefillNotification)}">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                    </c:if>
                </div>
                <div class="col-sm-1 content-contains-icon">
                    <c:choose>
                        <c:when test="${result.datedItemsList}" >
                            <a href="${storefrontRoot}/vesselSearch/getDatedItemsList?notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}&serialNo=${ycommerce:encodeUrl(serialNo)}" target="_blank">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </a>
                        </c:when>
                        <c:when test="${not empty result.openNotification and result.certificate}">
                            <a href="${storefrontRoot}/vesselSearch/createDatedItemsList?notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}" target="_blank">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </a>
                        </c:when>
                    </c:choose>
                </div>
                <div class="col-sm-1 content-contains-icon">
                    <c:if test="${not empty result.openNotification and not empty result.ipadNotification}">
                        <a id="send-notification-to-ipad"
                           href="${storefrontRoot}/vesselSearch/sendNotificationToIPad?serviceStation=${ycommerce:encodeUrl(serviceStation)}&notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}">
                            <i class="glyphicon glyphicon-phone"></i>
                        </a>
                    </c:if>
                </div>
               <div class="col-sm-1 content-contains-icon">
                   <c:if test="${not empty result.openNotification and result.containerLabel}">
                       <a href="${storefrontRoot}/vesselSearch/getContainerLabel?notificationNumber=${ycommerce:encodeUrl(result.notificationNumber)}&equipmentId=${ycommerce:encodeUrl(equipmentId)}" target="_blank">
                           <i class="glyphicon glyphicon-tag"></i>
                       </a>
                   </c:if>
                </div>
            </li>
        </c:forEach>
    </ul>

</div>