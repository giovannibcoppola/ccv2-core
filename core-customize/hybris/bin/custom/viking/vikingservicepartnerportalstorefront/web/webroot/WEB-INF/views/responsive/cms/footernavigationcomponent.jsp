<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${component.visible}">
	<div class="container ${customerPortalLook ? ' hidden' : ''}">
	    <div class="footer__top">
	        <div class="row">
	            <div class="footer__left col-xs-12 col-sm-11 col-sm-offset-1 col-md-10 col-md-offset-2">
	                <div class="row">
	                	<c:forEach items="${component.navigationNode.children}" var="childLevel1">
		                	<c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
							   <div class="footer__nav--container col-xs-12 col-sm-4">
		                	       <c:if test="${component.wrapAfter > i.index}">
	                                   <h3 class="title">${fn:escapeXml(childLevel1.title)}</h3>
	                               </c:if>
	                               <ul class="footer__nav--links">
	                                   <c:forEach items="${childLevel1.children}" var="childLevel2" begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
	                                        <c:forEach items="${childLevel2.entries}" var="childlink" >
		                                        <cms:component component="${childlink.item}" evaluateRestriction="true" element="li" class="footer__link"/>
	                                        </c:forEach>
	                                   </c:forEach>
	                               </ul>
	                		   </div>
						    </c:forEach>
	                	</c:forEach>
	               </div>
	           </div>
	           <%--<div class="footer__right col-xs-12 col-md-2">
	               <c:if test="${showLanguageCurrency}">
	                   <div class="row">
	                       <div class="col-xs-12 col-sm-6 col-md-12 footer__dropdown">
	                           <footer:languageSelector languages="${languages}" currentLanguage="${currentLanguage}" />
	                       </div>
	                       <div class="col-xs-12 col-sm-6 col-md-12 footer__dropdown">
	                           <footer:currencySelector currencies="${currencies}" currentCurrency="${currentCurrency}" />
	                       </div>
	                   </div>
	               </c:if>
			   </div>--%>
	        </div>
	    </div>
	</div>

	<div class="footer__bottom">
	    <div class="footer__copyright">
	        <div class="container">
				<c:if test="${not isEnquiryFlow}">
				<div class="site-footer__logo">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot>
			    </div>
				</c:if>
				<div class="site-footer__standout">${notice}</div>
				<div class="site-footer__company-info">
			        <span class="company-info__address">
			            <span class="company-info__street company-info__line">Saedding Ringvej 13</span>
			            <span class="company-info__zipcity company-info__line">6710 Esbjerg</span>
			            <span class="company-info__country company-info__line">Denmark</span>
			        </span>
			        <span class="company-info__contact">
			            <span class="company-info__tel company-info__line"><a href="tel:+45 76 11 81 00">Tel. +45 76 11 81 00</a></span>
			            <span class="company-info__fax company-info__line"><a href="fax:+45 76 11 81 01">Fax +45 76 11 81 01</a></span>
			            <span class="company-info__mail company-info__line"><a href="mailto:VIKING@VIKING-life.com">VIKING@VIKING-life.com</a></span>
			            <span class="company-info__mail company-info__line-end">CVR: 15016213</span>
			        </span>
			    </div>
			</div>
	    </div>
	</div>
</c:if>
