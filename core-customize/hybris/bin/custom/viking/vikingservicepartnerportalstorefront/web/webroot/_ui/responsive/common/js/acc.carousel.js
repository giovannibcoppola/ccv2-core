ACC.carousel = {

	_autoload: [
		["bindCarousel", $(".js-owl-carousel").length >0],
		"bindJCarousel"
	],

	carouselConfig:{
		"default":{
			navigation:true,
			navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
			pagination:false,
			loop: false,
			itemsCustom : [[0, 2], [640, 4], [1024, 5], [1400, 6]]
		},
		"rotating-image":{
			navigation:false,
			pagination:true,
			singleItem : true,
			loop: false
		},
		"lazy-reference":{
			navigation:true,
			navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
			pagination:false,
			itemsDesktop : [5000,6],
			itemsDesktopSmall : [1200,4],
			itemsTablet: [768,3],
			itemsMobile : [480,1],
			lazyLoad:true,
			loop: false
		}
	},

	bindCarousel: function(){

		$(".js-owl-carousel").each(function(){
			var $c = $(this)
			$.each(ACC.carousel.carouselConfig,function(key,config){
				if($c.hasClass("js-owl-"+key)){
					var $e = $(".js-owl-"+key);
					$e.owlCarousel(config);
				}
			});
		});

	},

	bindJCarousel: function ()
	{
		$(".modal").colorbox({
			onComplete: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			},
			onClosed: function ()
			{
				ACC.common.refreshScreenReaderBuffer();
			}
		});
		$('.svw').each( function(){
	          $( this).waitForImages( function(){
	               $(this).slideView({toolTip: true, ttOpacity: 0.6, autoPlay: true, autoPlayTime: 8000});
	          });
	    });
	}

};
