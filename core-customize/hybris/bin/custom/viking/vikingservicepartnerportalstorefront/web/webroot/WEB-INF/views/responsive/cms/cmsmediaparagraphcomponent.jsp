<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="container news-container" data-pk="${component.pk}">
	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="javascript:void(0)">
						<h4 style="display:inline;font-weight:700;">${headerText}</h4>
					</a>
				</h4>
			</div>
			<div id="" class="panel-collapse collapse">
				<div class="panel-body">
					<c:if test="${not empty media.url}">
						<img src="${media.url}" alt="${media.altText}" style="float:right;padding: 0px 0px 20px 20px;">
					</c:if>
					<c:if test="${not empty youtubeVideo}">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/${youtubeVideo}" style="clear:both;float:right;padding: 0px 0px 20px 20px;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</c:if>
					<div style="padding-bottom:10px;">
						${content}
					</div>
					<c:forEach items="${files.medias}" var="file">
						<div style="margin-top:10px;">
							<a href="${file.url}" target="_blank" style="color:#1d346b;text-decoration: underline;">${file.altText}</a>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</div>
