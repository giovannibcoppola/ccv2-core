<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product/accordion"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty product.certificates}">
	<input type="checkbox" id="product-accordion-certificates" />
	<label for="product-accordion-certificates">
		<h3>${fn:escapeXml(title)}</h3>
		<i class="glyphicon glyphicon-chevron-down"></i>
	</label>
	<div class="product-accordion-item">
		<product:certificates product="${product}" />
	</div>
</c:if>
