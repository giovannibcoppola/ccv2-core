/**
 *
 */
package com.viking.partner.controllers.pages;

import com.viking.core.model.ShowAllCertificatesModel;
import com.viking.core.util.CertificatesHelper;
import com.viking.facades.integration.VikingVesselFacade;
import com.viking.facades.integration.data.VikingEquipmentData;
import com.viking.facades.integration.data.VikingEquipmentDataWrapperFrontEnd;
import com.viking.partner.controllers.ControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.b2bcommercefacades.company.B2BUserFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author prabhakar
 *
 */
@Controller
@RequestMapping("/vesselPage")
public class VikingEquipmentPageController extends AbstractPageController
{
	private static final String VESSEL_CMS_PAGE = "homepage";

	@Resource (name="vikingVesselFacade")
	private VikingVesselFacade vikingVesselFacade;

	@Resource (name = "b2bUserFacade")
	private B2BUserFacade b2bUserFacade;

	@Resource (name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource (name = "certificatesHelper")
	private CertificatesHelper certificatesHelper;

	@RequestMapping(method = RequestMethod.GET)
	public String getVesselEquipmentPage(final Model model,
										 @RequestParam(name = "imoNumber") final String imoNumber)
			throws CMSItemNotFoundException
	{
		final ContentPageModel vesselCMSPage = getContentPageForLabelOrId(VESSEL_CMS_PAGE);
		storeCmsPageInModel(model, vesselCMSPage);
		CustomerData customer = b2bUserFacade.getCustomerForUid(customerFacade.getCurrentCustomerUid());
		final VikingEquipmentDataWrapperFrontEnd wrapperFrontEnd = vikingVesselFacade.getVikingEquipmentDataListMap(imoNumber, customer.getUnit().getUid());

		model.addAttribute("vesselName", wrapperFrontEnd.getVesselName());
		//model.addAttribute("owner", owner);
		model.addAttribute("flagState", wrapperFrontEnd.getFlagState());
		//model.addAttribute("classedBy", classedBy);
		//model.addAttribute("callSign", callSign);
		model.addAttribute("imoNumber", wrapperFrontEnd.getImo());

		final Optional<ShowAllCertificatesModel> showAllCertificates = certificatesHelper.findShowAllCertificates(customer.getUid());

		if (showAllCertificates.isPresent()) {
			model.addAttribute("showAllCertificates", Boolean.TRUE);
			wrapperFrontEnd.getEquipmentMap().entrySet().forEach(entry -> {
				entry.getValue().forEach(data -> data.setLastCertificate(showAllCertificates.get().getUrl() + imoNumber));
			});
		}

		model.addAttribute("vesselDataMap", wrapperFrontEnd.getEquipmentMap());
		if(MapUtils.isNotEmpty(wrapperFrontEnd.getEquipmentMap())) {
			model.addAttribute("totalEquipments", getTotalEquipments(wrapperFrontEnd.getEquipmentMap()));
		}
		setUpMetaDataForContentPage(model, vesselCMSPage);
		return ControllerConstants.Views.Pages.Layout.VikingVesselDetailPage;
	}

	private int getTotalEquipments(final Map<String, List<VikingEquipmentData>> vesselDataMap) {
		return vesselDataMap.values().stream().mapToInt(List::size).sum();
	}
}
