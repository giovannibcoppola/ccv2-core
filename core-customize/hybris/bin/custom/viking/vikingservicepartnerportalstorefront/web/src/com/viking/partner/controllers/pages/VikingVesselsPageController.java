/**
 *
 */
package com.viking.partner.controllers.pages;

import com.viking.partner.controllers.ControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.viking.facades.integration.VikingVesselFacade;
import com.viking.facades.integration.data.VikingVesselData;


/**
 * @author prabhakar
 *
 */
@Controller
@RequestMapping("/ListVesselsPage")
public class VikingVesselsPageController extends AbstractPageController
{

	private static final String LIST_VESSELS_CMS_PAGE = "homepage";
	private final Logger LOG = Logger.getLogger(VikingVesselsPageController.class);

	@Autowired
	private VikingVesselFacade vikingVesselFacade;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public String getVesselsPage(final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel listVesselsCMSPage = getContentPageForLabelOrId(LIST_VESSELS_CMS_PAGE);
		storeCmsPageInModel(model, listVesselsCMSPage);
		setUpMetaDataForContentPage(model, listVesselsCMSPage);
		final UserModel user = userService.getCurrentUser();
		if (user instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customerModel = (B2BCustomerModel) user;
			final B2BUnitModel b2bUnit = customerModel.getDefaultB2BUnit();
			if (b2bUnit != null)
			{
				final Set<PrincipalGroupModel> userGroups = customerModel.getAllGroups();
				final List<PrincipalGroupModel> groups = userGroups.stream()
						.filter(p -> p.getUid().equals(B2BConstants.B2BMANAGERGROUP))
						.collect(Collectors.<PrincipalGroupModel> toList());
				final String unitId = StringUtils.trim(b2bUnit.getUid());
				if (groups.size() > 0 && groups.get(0) != null) //manager user
				{
					final List<VikingVesselData> vesselDatas = vikingVesselFacade.getAllVesselDataListByCustomerId(unitId);
					sortByActionRequired(vesselDatas);
					model.addAttribute("vesselsData", vesselDatas);
				}
				else //normal user
				{
					final List<VikingVesselData> vesselsBothInHybrisAndServiceNowForThisUser = new ArrayList<>();
					final List<VikingVesselData> hybrisVesselDataList = vikingVesselFacade.getVesselDataListByUserId(customerModel.getUid());
					if (CollectionUtils.isNotEmpty(hybrisVesselDataList))
					{
						final List<VikingVesselData> serviceNowVesselDataList = vikingVesselFacade.getAllVesselDataListByCustomerId(unitId);
						for (final VikingVesselData hybrisVesselData : hybrisVesselDataList)
						{
							for (final VikingVesselData serviceNowVesselData : serviceNowVesselDataList)
							{
								if (hybrisVesselData.getVesselId().equalsIgnoreCase(serviceNowVesselData.getImo()))
								{
									vesselsBothInHybrisAndServiceNowForThisUser.add(serviceNowVesselData);
								}
							}
						}
					}
					sortByActionRequired(vesselsBothInHybrisAndServiceNowForThisUser);
					model.addAttribute("vesselsData", vesselsBothInHybrisAndServiceNowForThisUser); //vesselsBothInHybrisAndServiceNowForThisUser filter has to be implemented for normal users
				}
			}
			else
			{

				LOG.info("User is Not assigned to any Default B2B");
			}
		}
		return ControllerConstants.Views.Pages.Layout.VikingVesselListPage;
	}

	private void sortByActionRequired(List<VikingVesselData> input) {
		Collections.sort(input, new Comparator<VikingVesselData>() {
			public int compare(final VikingVesselData object1, final VikingVesselData object2) {
				return object2.getActionRequired().compareTo(object1.getActionRequired());
			}
		});
	}


}
