package com.viking.partner.controllers.pages;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.viking.erpservices.service.VikingFormsPreFillDataService;
import com.viking.erpservices.service.VikingIPADNotificationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.viking.core.data.EquipmentData;
import com.viking.core.integration.azure.VesselDocsData;
import com.viking.core.util.EquipmentToYformIdTranslator;
import com.viking.erpservices.service.VikingNotificationService;
import com.viking.erpservices.service.impl.DefaultVikingChangeVesselService;
import com.viking.erpservices.service.impl.DefaultVikingCreateVesselService;
import com.viking.erpservices.service.impl.DefaultVikingDetailsVesselService;
import com.viking.erpservices.service.impl.DefaultVikingEquipmentVesselService;
import com.viking.erpservices.service.impl.DefaultVikingFindVesselsService;
import com.viking.facades.Manuals.VikingManualsFacade;
import com.viking.facades.equipment.VikingEquipmentFacade;
import com.viking.facades.flagstate.VikingFlagStateFacade;
import com.viking.facades.flagstate.data.FlagStateData;
import com.viking.facades.forms.VikingFormsFacade;
import com.viking.facades.forms.impl.DefaultVikingNotificationPrefillFacade;
import com.viking.partner.forms.ChangeVesselForm;
import com.viking.partner.forms.CreateVesselForm;
import com.viking.partner.forms.SearchVesselForm;
import com.viking.search.data.ChangeVesselResponseData;
import com.viking.search.data.CreateVesselResponseData;
import com.viking.search.data.DropdownData;
import com.viking.search.data.SearchVesselData;
import com.viking.search.data.SearchVesselDataList;
import com.viking.search.data.SearchVesselOverviewData;
import com.viking.search.data.VesselEquipmentDataList;
import com.viking.search.data.VesselNotificationDataList;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.Session;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.ERROR_MESSAGES_HOLDER;


@Controller
@RequestMapping("/vesselSearch")
public class VesselSearchController extends AbstractPageController {

    public static final String VIKING_FORM_PAGE = "vikingFormPage";
    private static final Logger LOG = Logger.getLogger(VesselSearchController.class);
    private static final String VESSEL_CMS_PAGE_LABEL = "vesselsearchpage";
    private static final String VESSEL_OVERVIEW_CMS_PAGE_LABEL = "vesseloverviewpage";
    private static final String VESSEL_DOC_CMS_PAGE_LABEL = "vesseldocpage";
    private static final String SERVICE_OVERVIEW_CMS_PAGE_LABEL = "serviceoverviewpage";
    private static final String CONDAMNATION ="condemnation";
    public static final String ADD_EQUIPMENT_CMS_PAGE_LABEL = "addequipmentpage";
    @Resource(name = "vikingDetailsVesselsService")
    private DefaultVikingDetailsVesselService vikingDetailsVesselsService;

    @Resource(name = "vikingFindVesselsService")
    private DefaultVikingFindVesselsService vikingFindVesselsService;

    @Resource(name = "vikingEquipmentVesselService")
    private DefaultVikingEquipmentVesselService vikingEquipmentVesselService;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Resource(name = "vikingNotificationVesselService")
    private VikingNotificationService vikingNotificationService;

    @Resource(name = "vikingFormsFacade")
    private VikingFormsFacade vikingFormsFacade;

    @Resource(name = "vikingCreateVesselService")
    private DefaultVikingCreateVesselService vikingCreateVesselService;

    @Resource(name = "vikingChangeVesselService")
    private DefaultVikingChangeVesselService vikingChangeVesselService;

    @Resource(name = "vikingNotificationPrefillFacade")
    private DefaultVikingNotificationPrefillFacade vikingNotificationPrefillFacade;

    @Resource(name = "vikingFlagStateFacade")
    private VikingFlagStateFacade vikingFlagStateFacade;

    @Resource(name = "vikingEquipmentFacade")
    private VikingEquipmentFacade vikingEquipmentFacade;

    @Resource(name = "vikingManualsFacade")
    private VikingManualsFacade vikingManualsFacade;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "equipmentToYformIdTranslator")
    private EquipmentToYformIdTranslator equipmentToYformIdTranslator;

    @Resource(name = "vikingFormsPreFillDataService")
    private VikingFormsPreFillDataService vikingFormsPreFillDataService;

    @Resource(name = "vikingIPADNotificationService")
    private VikingIPADNotificationService vikingIPADNotificationService;

    private final String REDIRECT_ADD_EQUIPMENT = REDIRECT_PREFIX + "/vesselSearch/addEquipment/";

    @GetMapping(value = "/home")
    public String vesselSearch(Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        LOG.info("NEW CONTROLLER : VESSEL");
        Session currentSession = getSessionService().getCurrentSession();
        currentSession.setAttribute("callSignalVal", "empty");
        List<DropdownData> dropdownDataList = new ArrayList<>();

        DropdownData dropdownData0 = new DropdownData();
        DropdownData dropdownData1 = new DropdownData();
        DropdownData dropdownData2 = new DropdownData();
        DropdownData dropdownData3 = new DropdownData();


        dropdownData0.setId("vesselId");
        dropdownData0.setName("Vessel ID");

        dropdownData1.setId("vesselName");
        dropdownData1.setName("Name of Vessel");


        dropdownData2.setId("formerVesselName");
        dropdownData2.setName("Former Name of Vessel");

        dropdownData3.setId("serialNumber");
        dropdownData3.setName("Equipment Serial Number");

        dropdownDataList.add(dropdownData0);
        dropdownDataList.add(dropdownData1);
        dropdownDataList.add(dropdownData2);
        dropdownDataList.add(dropdownData3);


        List<SearchVesselData> entryList = new ArrayList<>();


        SearchVesselData testData1 = new SearchVesselData();
        testData1.setVesselName("vessel 1");
        testData1.setVesselID("000011");
        testData1.setFlagState("Bahamas");
        testData1.setCallSignal("C6QD7");
        testData1.setOwner("Kegel Co ltd");

        SearchVesselData testData2 = new SearchVesselData();
        testData2.setVesselName("vessel 2");
        testData2.setVesselID("000022");
        testData2.setFlagState("Bahamas");
        testData2.setCallSignal("KD37R");
        testData2.setOwner("Kegel Co ltd");

        SearchVesselData testData3 = new SearchVesselData();
        testData3.setVesselName("vessel 3");
        testData3.setVesselID("000033");
        testData3.setFlagState("Denmark");
        testData3.setCallSignal("OLP2W");
        testData3.setOwner("Kegel Co ltd");

        SearchVesselData testData4 = new SearchVesselData();
        testData4.setVesselName("vessel 4");
        testData4.setVesselID("000044");
        testData4.setFlagState("Bahamas");
        testData4.setCallSignal("OLP2W84");
        testData4.setOwner("Kegel Co ltd");

        SearchVesselData testData5 = new SearchVesselData();
        testData5.setVesselName("vessel 5");
        testData5.setVesselID("000055");
        testData5.setFlagState("Denmark");
        testData5.setCallSignal("OLP2W891");
        testData5.setOwner("Kegel Co ltd");

        SearchVesselData testData6 = new SearchVesselData();
        testData6.setVesselName("vessel 6");
        testData6.setVesselID("00006");
        testData6.setFlagState("Denmark");
        testData6.setCallSignal("OLP2WGF1");
        testData6.setOwner("Kegel Co ltd");

        SearchVesselData testData7 = new SearchVesselData();
        testData7.setVesselName("vessel 7");
        testData7.setVesselID("000077");
        testData7.setFlagState("Denmark");
        testData7.setCallSignal("OLP2WKK");
        testData7.setOwner("Kegel Co ltd 7");

        SearchVesselData testData8 = new SearchVesselData();
        testData8.setVesselName("vessel 8");
        testData8.setVesselID("000088");
        testData8.setFlagState("Denmark");
        testData8.setCallSignal("OLP2WMM");
        testData8.setOwner("Kegel Co ltd 8");

        SearchVesselData testData9 = new SearchVesselData();
        testData9.setVesselName("vessel 9");
        testData9.setVesselID("000099");
        testData9.setFlagState("Denmark");
        testData9.setCallSignal("OLP2WEWW");
        testData9.setOwner("Kegel Co ltd 9");

        SearchVesselData testData10 = new SearchVesselData();
        testData10.setVesselName("vessel 10");
        testData10.setVesselID("000010");
        testData10.setFlagState("Denmark");
        testData10.setCallSignal("OLP2W10");
        testData10.setOwner("Kegel Co ltd 10 OWN");


        entryList.add(testData1);
        entryList.add(testData2);
        entryList.add(testData3);
        entryList.add(testData4);
        entryList.add(testData5);
        entryList.add(testData6);
        entryList.add(testData7);
        entryList.add(testData8);
        entryList.add(testData9);
        entryList.add(testData10);


        model.addAttribute("pageID", "home");
        model.addAttribute("entryList", entryList);
        model.addAttribute("dropdownDataList", dropdownDataList);

        storeCmsPageInModel(model, getContentPageForLabelOrId(VESSEL_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(VESSEL_CMS_PAGE_LABEL));


        return getViewForPage(model);


    }

    @RequestMapping(value = "/rfcSearchResult", method = {RequestMethod.POST, RequestMethod.GET})
    public String rfcSearchResult(final SearchVesselForm searchVesselForm, final BindingResult bindingResultfinal, Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {
        LOG.info("NEW CONTROLLER : VESSEL RFC");


        String searchTerm = searchVesselForm.getSearchTerm();
        model.addAttribute("entryList", "empty");

        String searchType = searchVesselForm.getSearchType();

        LOG.info("search term value and type : " + searchTerm + " :: " + searchType);

        if (searchType.equalsIgnoreCase("vesselId")) {
            try {
                LOG.info("Search vessel by vesselId RFC Calling ..");
                SearchVesselDataList vesselByName = vikingFindVesselsService.findVesselByIMO(searchTerm);
                model.addAttribute("entryList", vesselByName);

            } catch (Exception e) {
                LOG.error("error while getting result from RFC " + e);
            }


        }

        if (searchType.equalsIgnoreCase("vesselName")) {
            try {
                LOG.info("Search vessel by name function RFC Calling ..");
                String newSearchTerm = searchTerm.toUpperCase();
                SearchVesselDataList vesselByName = vikingFindVesselsService.findVesselByName(newSearchTerm);
                model.addAttribute("entryList", vesselByName);

            } catch (Exception e) {
                LOG.error("error while getting result from RFC " + e);
            }


        }


        if (searchType.equalsIgnoreCase("formerVesselName")) {

            try {
                LOG.info("Search vessel by former Vessel Name function RFC Calling ..");
                String updatedSearchTerm = searchTerm.toUpperCase();
                SearchVesselDataList vesselByName = vikingFindVesselsService.findVesselByFormerName(updatedSearchTerm);
                model.addAttribute("entryList", vesselByName);

            } catch (Exception e) {
                LOG.error("error while getting result from RFC " + e);
            }

        }

        if (searchType.equalsIgnoreCase("serialNumber")) {

            try {
                LOG.info("Search Vessel by serial Number function RFC Calling ..");
                SearchVesselDataList vesselByName = vikingFindVesselsService.findVesselBySerialNumber(searchTerm);
                model.addAttribute("entryList", vesselByName);

            } catch (Exception e) {
                LOG.error("error while getting result from RFC " + e);
            }

        }


        // vikingFindVesselsService.findVesselByIMO()


        List<DropdownData> dropdownDataList = new ArrayList<>();

        DropdownData dropdownData0 = new DropdownData();
        DropdownData dropdownData1 = new DropdownData();
        DropdownData dropdownData2 = new DropdownData();
        DropdownData dropdownData3 = new DropdownData();


        dropdownData0.setId("vesselId");
        dropdownData0.setName("Vessel ID");

        dropdownData1.setId("vesselName");
        dropdownData1.setName("Name of Vessel");


        dropdownData2.setId("formerVesselName");
        dropdownData2.setName("Former Name of Vessel");

        dropdownData3.setId("serialNumber");
        dropdownData3.setName("Equipment Serial Number");

        if (searchType.equalsIgnoreCase("vesselId")) {
            dropdownDataList.add(dropdownData0);
            dropdownDataList.add(dropdownData1);
            dropdownDataList.add(dropdownData2);
            dropdownDataList.add(dropdownData3);
        }

        if (searchType.equalsIgnoreCase("vesselName")) {
            dropdownDataList.add(dropdownData1);
            dropdownDataList.add(dropdownData0);
            dropdownDataList.add(dropdownData2);
            dropdownDataList.add(dropdownData3);
        }

        if (searchType.equalsIgnoreCase("formerVesselName")) {
            dropdownDataList.add(dropdownData2);
            dropdownDataList.add(dropdownData1);
            dropdownDataList.add(dropdownData0);
            dropdownDataList.add(dropdownData3);
        }

        if (searchType.equalsIgnoreCase("serialNumber")) {

            dropdownDataList.add(dropdownData3);
            dropdownDataList.add(dropdownData2);
            dropdownDataList.add(dropdownData1);
            dropdownDataList.add(dropdownData0);

        }


        model.addAttribute("searchValue", searchTerm);
        model.addAttribute("dropdownDataList", dropdownDataList);
        storeCmsPageInModel(model, getContentPageForLabelOrId(VESSEL_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(VESSEL_CMS_PAGE_LABEL));


        return getViewForPage(model);

    }

    @RequestMapping(value = "/vesselOverview", method = {RequestMethod.POST, RequestMethod.GET})
    public String vesselOverview(final Model model, @RequestParam(value = "vesselID", required = false) final String vesselID, @RequestParam(value = "signal", required = false) final String signal)
            throws CMSItemNotFoundException {

        LOG.info("VESSEL : Overview : For the vesselID : " + vesselID + " call signel : " + signal);
        model.addAttribute("searchVesselOverviewData", "empty");

        SearchVesselOverviewData searchVesselOverviewData = vikingDetailsVesselsService.detailsVessel(vesselID);
        searchVesselOverviewData.setVesselID(vesselID);
        String callSignalVal = getSessionService().getAttribute("callSignalVal");

        if (!callSignalVal.equalsIgnoreCase("empty")) {
            searchVesselOverviewData.setCallSignal(callSignalVal);
        } else {
            searchVesselOverviewData.setCallSignal(signal);
        }
        model.addAttribute("searchVesselOverviewData", searchVesselOverviewData);


        final VesselEquipmentDataList vesselEquipmentList = vikingEquipmentVesselService.getVesselEquipmentList(vesselID);
        model.addAttribute("entryList", vesselEquipmentList);
        model.addAttribute("pageID", "vesselOverview");


        storeCmsPageInModel(model, getContentPageForLabelOrId(VESSEL_OVERVIEW_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(VESSEL_OVERVIEW_CMS_PAGE_LABEL));
        return getViewForPage(model);


    }

    @GetMapping(value = "/vesselOverview/vesseldoc")
    public String vesselDoc(final Model model, @RequestParam(value = "vesselID", required = false) final String vesselID, @RequestParam(value = "vesselName", required = false) final String vesselName)
            throws CMSItemNotFoundException {

        VesselDocsData vesselDocuments = vikingManualsFacade.getVesselDocuments(vesselID);
        model.addAttribute("vesselName", vesselName);

        if (vesselDocuments != null) {
            model.addAttribute("vesselOperationalDocs", vesselDocuments.getOperation());
            model.addAttribute("vesselInstallationDocs", vesselDocuments.getInstallation());
        }

        model.addAttribute("portalAccessKey", configurationService.getConfiguration().getString("azure.client.portal.accesskey"));

        storeCmsPageInModel(model, getContentPageForLabelOrId(VESSEL_DOC_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(VESSEL_DOC_CMS_PAGE_LABEL));
        return getViewForPage(model);
    }

    @GetMapping(value = "/serviceOverview")
    @Cacheable
    public String serviceOverview(final Model model,
                                  @RequestParam(value = "serialNo", required = false) final String serialNo,
                                  @RequestParam(value = "equipmentId", required = false) final String equipmentId,
                                  @RequestParam(value = "name", required = false) final String equipmentName,
                                  @RequestParam(value = "type", required = false) final String type)
            throws CMSItemNotFoundException {

        final String uid = customerFacade.getCurrentCustomer().getUid();
        VesselNotificationDataList notifications = null;
        try {
            notifications = vikingNotificationService.getNotifications(serialNo, uid, equipmentId);
        } catch (Exception ex) {
            LOG.error("Not able to retrieve notifications ", ex);
        }

        if (notifications != null) {
            model.addAttribute("entryList", notifications.getVesselNotificationDataList());
        }

        model.addAttribute("serialNo", serialNo);
        model.addAttribute("equipmentId", equipmentId);
        model.addAttribute("equipmentName", equipmentName);
        LOG.debug("Translating equipment type [" + type + "]");
        model.addAttribute("type", equipmentToYformIdTranslator.convertToId(type));
        model.addAttribute("serviceStation", uid);

        storeCmsPageInModel(model, getContentPageForLabelOrId(SERVICE_OVERVIEW_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SERVICE_OVERVIEW_CMS_PAGE_LABEL));
        return getViewForPage(model);
    }

    /**
     * Get or download certificate and redirect to the received media url.
     *
     * @param model              the model
     * @param notificationNumber the notification number
     */
    @GetMapping(value = "/getCertificate")
    @Cacheable
    public String getCertificate(final Model model,
                                  @RequestParam(value = "notificationNumber", required = true) final String notificationNumber,
                                  @RequestParam(value = "serialNo", required = true) final String serialNo
                                 )
            throws CMSItemNotFoundException {

        try {
            return "redirect:" + vikingNotificationService.getCertificate(notificationNumber, serialNo);
        } catch (Exception ex) {
            LOG.error("Not able to retrieve Certificate ", ex);
        }

        throw new IllegalStateException("No Certificate could be downloaded!");
    }

    /**
     * Get or download dated items list and redirect to the received media url.
     *
     * @param model              the model
     * @param notificationNumber the notification number
     */
    @GetMapping(value = "/getDatedItemsList")
    @Cacheable
    public String getDatedItemsList(final Model model,
                                    @RequestParam(value = "notificationNumber", required = true) final String notificationNumber,
                                    @RequestParam(value = "serialNo", required = true) final String serialNo
                                    )
            throws CMSItemNotFoundException {

        try {
            return "redirect:" + vikingNotificationService.getDatedItemsList(notificationNumber, serialNo);
        } catch (Exception ex) {
            LOG.error("Not able to retrieve Dated Items List", ex);
        }

        throw new IllegalStateException("No Dated Items List could be downloaded!");
    }

    /**
     * Get or download container label and redirect to the received media url.
     *
     * @param model              the model
     * @param notificationNumber the notification number
     */
    @GetMapping(value = "/getContainerLabel")
    @Cacheable
    public String getContainerLabel(final Model model,
                                    @RequestParam(value = "notificationNumber", required = true) final String notificationNumber,
                                    @RequestParam(value = "equipmentId", required = true) final String equipmentId
    )
            throws CMSItemNotFoundException {

        final String serviceStation = customerFacade.getCurrentCustomer().getUid();
        try {
            return "redirect:" + vikingNotificationService.createContainerLabel(notificationNumber, serviceStation, equipmentId);
        } catch (Exception ex) {
            LOG.error("Not able to retrieve Container Label", ex);
        }

        throw new IllegalStateException("No Container Label could be downloaded!");
    }

    /**
     * Create dated items list and redirect to the received media url.
     *
     * @param model              the model
     * @param notificationNumber the notification number
     */
    @GetMapping(value = "/createDatedItemsList")
    @Cacheable
    public String createDatedItemsList(final Model model,
                                    @RequestParam(value = "notificationNumber", required = true) final String notificationNumber
    )
            throws CMSItemNotFoundException {

        try {
            return "redirect:" + vikingNotificationService.createDatedItemsList(notificationNumber);
        } catch (Exception ex) {
            LOG.error("Not able to create Dated Items List", ex);
        }

        throw new IllegalStateException("No Dated Items List could be created!");
    }

    /**
     * Service overview.
     *
     * @param model              the model
     * @param notificationNumber the notification number
     * @param serviceStation     the service station
     */
    @GetMapping(value = "/sendNotificationToIPad")
    @ResponseBody
    public void serviceOverview(final Model model,
                                @RequestParam(value = "notificationNumber", required = false) final String notificationNumber,
                                @RequestParam(value = "serviceStation", required = false) final String serviceStation) {

        vikingIPADNotificationService.sendNotificationToIPad(serviceStation, notificationNumber);
    }

    @RequestMapping(value = "/createVesselRFC", produces = "application/json", method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Map<String, String> createVesselRFC(final CreateVesselForm createVesselForm, final BindingResult bindingResultfinal, Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {

        final Map<String, String> map = new HashMap<String, String>();
        LOG.info("new " + request.getParameter("description"));
        LOG.info("new " + request.getParameter("flagState"));
        LOG.info("new " + request.getParameter("signal"));
        String description = createVesselForm.getDescription();
        String flagState = createVesselForm.getFlagState();
        String signal = createVesselForm.getSignal();

        LOG.info("CREATE VESSEL RFC CALL !!! Desciption : " + description + " Flag State : " + flagState + " Signal : " + signal);

        CreateVesselResponseData vesselData = vikingCreateVesselService.createVessel(description, flagState, signal);
        String vesselID = vesselData.getVesselId();
        String message = vesselData.getMessage();
        map.put("vesselID", vesselID);
        map.put("message", message);

        LOG.info("CREATED VESSEL ID : " + vesselID + " Message : " + message);

        return map;

    }

    @GetMapping(value = "/loadForm")
    public String loadForm(final Model model, @RequestParam(value = "notificationId", required = false) final String notificationId, @RequestParam(value = "formId") final String formId, @RequestParam(value = "serviceStation", required = false) final String serviceStation, @RequestParam(value = "prefillId", required = false) final String prefillId, final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException {

        LOG.debug("Converting equipment ID [" + formId + "] to yForm id");
        storeCmsPageInModel(model, getContentPageForLabelOrId(VIKING_FORM_PAGE + equipmentToYformIdTranslator.convertToYFormId(formId)));
        if(!vikingFormsPreFillDataService.isCreditCheckPass(serviceStation, formId) && !formId.equalsIgnoreCase(CONDAMNATION)) {
            GlobalMessages.addErrorMessage(model, "text.error.certificate.no.credit");
        }
        return getViewForPage(model);
    }


    @RequestMapping(value = "/changeVesselRFC", produces = "application/json", method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody
    Map<String, String> changeVesselRFC(final ChangeVesselForm changeVesselForm, final BindingResult bindingResultfinal, Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws CMSItemNotFoundException {

        final Map<String, String> map = new HashMap<String, String>();


        String vesselID = request.getParameter("vesselID");
        String description = request.getParameter("description");
        String flagState = request.getParameter("flagState");
        String signal = request.getParameter("signal");
        LOG.info("VESSEL ID " + vesselID);
        LOG.info("DESCRIPTION  " + description);
        LOG.info("FLAG STATE " + flagState);
        LOG.info("SIGNAL  " + signal);

        Session currentSession = getSessionService().getCurrentSession();
        currentSession.setAttribute("callSignalVal", signal);

        LOG.info("CHANGE VESSEL RFC CALL !!! Desciption : " + description + " Flag State : " + flagState + " Signal : " + signal);

        ChangeVesselResponseData changeVesselResponseData = vikingChangeVesselService.changeVessel(vesselID, description, flagState, signal);
        String message = changeVesselResponseData.getMessage();
        map.put("message", message);
        LOG.info("Changed VESSEL ID : " + vesselID + " Message : " + message);

        return map;

    }

    @GetMapping(value = "/addEquipment/{vesselId}")
    public String loadAddEquipmentPage(@PathVariable final String vesselId, final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EQUIPMENT_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EQUIPMENT_CMS_PAGE_LABEL));

        final String serialNo = request.getParameter("serialnumber");

        model.addAttribute("vesselId", vesselId);
        model.addAttribute("serialNumber", serialNo);

        if (StringUtils.isNotEmpty(serialNo)) {
            LOG.debug("Perform equipment search for serial number:" + serialNo);
            final EquipmentData equipmentData = vikingEquipmentFacade.searchEquipment(serialNo);
            model.addAttribute("equipmentData", equipmentData);
        }

        final String addEquipment = request.getParameter("add");
        if (Boolean.parseBoolean(addEquipment)) {
            LOG.debug("Adding equipment with serial number: " + serialNo + " to vessel with id: " + vesselId);
            final String type = request.getParameter("type");
            model.addAttribute("type", equipmentToYformIdTranslator.convertToId(type));
            final String uid = customerFacade.getCurrentCustomer().getUid();
            model.addAttribute("serviceStation", uid);
            try {
                vikingEquipmentFacade.addEquipment(serialNo, vesselId);
            } catch (Exception exception) {
                LOG.error("Not able to add Equipment", exception);
            }
            try {
                final VesselNotificationDataList notifications = vikingNotificationService.getNotifications(serialNo, uid);
                model.addAttribute("notifications", notifications.getVesselNotificationDataList());
            } catch (Exception exception) {
                LOG.error( "Not able to retrieve notifications", exception);
                return handleException(vesselId, redirectModel, exception, "text.add.equipment.notification.failure");
            }
        }

        return getViewForPage(model);
    }

    private String handleException(String vesselId, RedirectAttributes redirectModel, Exception exception, String flashMessage) {
        GlobalMessages.addFlashMessage(redirectModel, ERROR_MESSAGES_HOLDER, flashMessage, null);
        return REDIRECT_ADD_EQUIPMENT + vesselId;
    }

    @GetMapping(value = "/dismantleEquipment/{serialNo}")
    public String dismatleEquipment(@PathVariable final String serialNo, @RequestParam(value = "vesselID", required = false) final String vesselID, @RequestParam(value = "signal", required = false) final String signal, final Model model, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(VESSEL_OVERVIEW_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(VESSEL_OVERVIEW_CMS_PAGE_LABEL));

        try {
            LOG.info("new " + serialNo);
            vikingEquipmentFacade.dismantleEquipment(serialNo);
        } catch (Exception exception) {
            LOG.debug("Not able to dismantle Equipment ", exception);
        }

        return REDIRECT_PREFIX + "/vesselSearch/vesselOverview/?vesselID=" + vesselID + "&signal=" + signal;
    }


    @ModelAttribute("flagStates")
    public Collection<FlagStateData> getFlagStates() {
        return vikingFlagStateFacade.getAll();
    }

    @ModelAttribute("storefrontRoot")
    public String getStorefrontRoot() {
        return getConfigurationService().getConfiguration().getString("vikingservicepartnerportalstorefront.webroot");
    }


}
