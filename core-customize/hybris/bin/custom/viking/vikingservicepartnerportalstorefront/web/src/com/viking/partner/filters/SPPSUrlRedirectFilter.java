package com.viking.partner.filters;

import de.hybris.platform.acceleratorfacades.urlencoder.UrlEncoderFacade;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter will check if the url has old "spps" in the url and redirect it to the correct new url.
 *
 * @author chiranjit
 */
public class SPPSUrlRedirectFilter extends OncePerRequestFilter
{
    public static final String SPPS = "/spps/servicepartnerportal/en/login";
    private static final Logger LOG = Logger.getLogger(UrlEncoderFilter.class.getName());
    protected static final String SERVICEPARTNERPORTAL_EN_LOGIN = "/servicepartnerportal/en/login";

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {

        final String requestURL = request.getRequestURL().toString();
        if(requestURL.contains(SPPS)) {
            response.sendRedirect(SERVICEPARTNERPORTAL_EN_LOGIN);
        } else {

            filterChain.doFilter(request, response);
        }

    }
}
