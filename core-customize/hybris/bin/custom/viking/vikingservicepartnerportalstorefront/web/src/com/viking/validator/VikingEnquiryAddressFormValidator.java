/**
 *
 */
package com.viking.validator;

import com.viking.forms.VikingEnquiryAddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chiranjitchakraborty
 *
 */
@Component("vikingEnquiryAddressFormValidator")
public class VikingEnquiryAddressFormValidator implements Validator
{
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public void validate(final Object object, final Errors errors) {
		final VikingEnquiryAddressForm addressForm = (VikingEnquiryAddressForm) object;
		validateMandatoryFields(addressForm, errors);
	}

	/**
	 * @param addressForm
	 * @param errors
	 */
	private void validateMandatoryFields(final VikingEnquiryAddressForm addressForm, final Errors errors) {
		validateFieldNotNull(addressForm.getCompany(), VikingEnquiryAddressField.COMPANY, errors);
		validateFieldNotNull(addressForm.getName(), VikingEnquiryAddressField.NAME, errors);
		validateAddressLine1Field(addressForm.getLine1(), VikingEnquiryAddressField.LINE1, errors);
		validateFieldNotNull(addressForm.getPostcode(), VikingEnquiryAddressField.POSTCODE, errors);
		validateFieldNotNull(addressForm.getCity(), VikingEnquiryAddressField.CITY, errors);
		validateFieldNotNull(addressForm.getCountryIso(), VikingEnquiryAddressField.COUNTRY, errors);
		validateEmailField(addressForm.getEmail(), VikingEnquiryAddressField.EMAIL, errors);
		validateFieldNotNull(addressForm.getPreferredContactMethod(), VikingEnquiryAddressField.PREFERRED_CONTACT_METHOD, errors);
		if(addressForm.hasComment()) {
			validateFieldNotNull(addressForm.getCountryIso(), VikingEnquiryAddressField.COMMENT, errors);
		}
	}

	private void validateAddressLine1Field(final String addressField, final VikingEnquiryAddressField fieldType, Errors errors) {
		if (StringUtils.isEmpty(addressField) || !StringUtils.containsWhitespace(addressField)) {
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected  void validateEmailField(final String addressField, final VikingEnquiryAddressField fieldType, final Errors errors)
	{
		if (StringUtils.isEmpty(addressField) || StringUtils.length(addressField) > 255 || !validateEmailAddress(addressField)) {
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected  boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = Pattern.compile(configurationService.getConfiguration().getString(WebConstants.EMAIL_REGEX))
				.matcher(email);
		return matcher.matches();
	}


	protected static void validateFieldNotNull(final String addressField, final VikingEnquiryAddressField fieldType, final Errors errors)
	{
		if (StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> arg0)
	{
		// YTODO Auto-generated method stub
		return false;
	}

	protected enum 	VikingEnquiryAddressField
	{
		 NAME("name", "vikingEnquiryAddressForm.name.invalid")
		, LINE1("line1", "vikingEnquiryAddressForm.line1.invalid")
		, CITY("city", "vikingEnquiryAddressForm.city.invalid")
		, POSTCODE("postcode", "vikingEnquiryAddressForm.postcode.invalid")
		, COUNTRY("countryIso", "vikingEnquiryAddressForm.country.invalid")
		, COMPANY("company", "vikingEnquiryAddressForm.company.invalid")
		, PREFERRED_CONTACT_METHOD("preferredContactMethod", "vikingEnquiryAddressForm.preferredContactMethod.invalid")
		, EMAIL("email", "vikingEnquiryAddressForm.email.invalid")
		, COMMENT("comment", "vikingEnquiryAddressForm.comment.invalid");

		private final String fieldKey;
		private final String errorKey;

		private VikingEnquiryAddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
