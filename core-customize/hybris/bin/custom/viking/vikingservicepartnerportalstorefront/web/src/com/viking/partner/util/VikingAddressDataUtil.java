/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.util;

import com.viking.partner.forms.VikingAddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.commercefacades.user.data.AddressData;


public class VikingAddressDataUtil extends AddressDataUtil {

    public void convertBasic(final AddressData source, final VikingAddressForm target) {

        target.setAddressId(source.getId());
        target.setTitleCode(source.getTitleCode());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setLine1(source.getLine1());
        target.setLine2(source.getLine2());
        target.setTownCity(source.getTown());
        target.setPostcode(source.getPostalCode());
        if(source.getCountry()!= null) {
            target.setCountryIso(source.getCountry().getIsocode());
        }
        target.setCompName(source.getCompanyName());
        target.setPhone(source.getPhone());
    }

    public void convert(final AddressData source, final VikingAddressForm target) {
        convertBasic(source, target);
    }


    public AddressData convertToAddressData(final VikingAddressForm addressForm) {
        final AddressData addressData = super.convertToAddressData(addressForm);
        addressData.setCompanyName(addressForm.getCompName());
        return addressData;
    }


}
