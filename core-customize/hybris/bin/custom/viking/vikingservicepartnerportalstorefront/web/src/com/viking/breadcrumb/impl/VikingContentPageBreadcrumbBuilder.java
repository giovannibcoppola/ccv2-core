package com.viking.breadcrumb.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VikingContentPageBreadcrumbBuilder extends ContentPageBreadcrumbBuilder {

    private static final Logger LOG = LogManager.getLogger(VikingContentPageBreadcrumbBuilder.class);

    private static final String LAST_LINK_CLASS = "active";

    private CMSPageService cmsPageService;

    /**
     * @param page - page to build up breadcrumb for
     * @return breadcrumb for given page
     */
    public List<Breadcrumb> getBreadcrumbs(final ContentPageModel page) {
        try {
            final List<Breadcrumb> breadcrumbs = new ArrayList<>();

            final String pageLabel = page.getLabel();

            final String[] labels = pageLabel.split("/");

            for (final String label : labels) {
                // The first label will always be empty
                if (StringUtils.isEmpty(label)) {
                    continue;
                }

                final ContentPageModel contentPage = cmsPageService.getPageForLabelOrId(label);

                if (!page.getUid().equalsIgnoreCase(contentPage.getUid())) {
                    breadcrumbs.add(new Breadcrumb(contentPage.getLabel(), contentPage.getTitle(), LAST_LINK_CLASS));
                } else {
                    breadcrumbs.add(new Breadcrumb("#", contentPage.getTitle(), LAST_LINK_CLASS));
                }
            }

            return breadcrumbs;
        } catch (Exception e) {
            LOG.debug("Unable to determine breadcrumbs for page " + page.getLabel(), e);

            String title = page.getTitle();
            if (title == null) {
                title = page.getName();
            }
            return Collections.singletonList(new Breadcrumb("#", title, LAST_LINK_CLASS));
        }
    }

    @Required
    public void setCmsPageService(CMSPageService cmsPageService) {
        this.cmsPageService = cmsPageService;
    }
}
