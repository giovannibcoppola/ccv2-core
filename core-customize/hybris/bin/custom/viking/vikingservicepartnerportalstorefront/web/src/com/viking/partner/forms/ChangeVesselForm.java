package com.viking.partner.forms;

public class ChangeVesselForm {

    String vesselID;
    String description;
    String flagState;
    String signal;

    public String getVesselID() {
        return vesselID;
    }

    public void setVesselID(String vesselID) {
        this.vesselID = vesselID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlagState() {
        return flagState;
    }

    public void setFlagState(String flagState) {
        this.flagState = flagState;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }


}
