package com.viking.variants.impl;

import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;
import org.springframework.beans.factory.annotation.Required;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class VikingVariantSortStrategy implements VariantSortStrategy {

    private List<String> sortingFieldsOrder;
    private Map<String, Comparator<String>> comparators;
    private Comparator<String> defaultComparator;

    @Override
    public int compare(final VariantOptionData product1, final VariantOptionData product2) {
        if (sortingFieldsOrder != null) {
            for (final String field : sortingFieldsOrder) {
                final int result = getComparator(field).compare(getVariantValue(field, product1), getVariantValue(field, product2));
                if (result != 0) {
                    return result;
                }
            }
        }
        return -1;
    }

    private String getVariantValue(final String field, final VariantOptionData variant) {
        for (final VariantOptionQualifierData variantOptionQualifier : variant.getVariantOptionQualifiers()) {
            if (field.equals(variantOptionQualifier.getQualifier())) {
                return variantOptionQualifier.getValue();
            }
        }
        return null;
    }

    private Comparator<String> getComparator(final String field) {
        final Comparator<String> comparator = comparators.get(field);
        if (comparator == null) {
            return defaultComparator;
        }
        return comparator;
    }

    @Override
    @Required
    public void setSortingFieldsOrder(final List<String> sortingFieldsOrder) {
        this.sortingFieldsOrder = sortingFieldsOrder;
    }

    @Override
    @Required
    public void setComparators(final Map<String, Comparator<String>> comparators) {
        this.comparators = comparators;
    }

    @Override
    @Required
    public void setDefaultComparator(final Comparator<String> defaultComparator) {
        this.defaultComparator = defaultComparator;
    }
}
