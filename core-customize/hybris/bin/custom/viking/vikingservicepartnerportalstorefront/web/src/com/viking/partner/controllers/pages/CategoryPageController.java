/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.controllers.pages;


import com.viking.core.enquiry.service.EnquiryService;
import com.viking.breadcrumb.impl.VikingSearchBreadcrumbBuilder;

import com.viking.core.service.VikingCommerceCategoryService;
import com.viking.core.service.VikingCommerceStockService;
import com.viking.core.service.impl.DefaultVikingCommerceCategoryService;
import com.viking.facades.enquiry.EnquiryFacade;
import com.viking.core.storefront.util.VikingPageTitleResolver;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Controller for a category page
 */
@Controller
@RequestMapping(value = "/**/c")
public class CategoryPageController extends AbstractCategoryPageController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryPageController.class);

    @Resource(name = "vikingDefaultCommerceStockService")
    private VikingCommerceStockService vikingCommerceStockService;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "vikingSearchBreadcrumbBuilder")
    private VikingSearchBreadcrumbBuilder vikingSearchBreadcrumbBuilder;

    @Resource(name = "vikingPageTitleResolver")
    private VikingPageTitleResolver vikingPageTitleResolver;

    @Autowired
    private EnquiryService enquiryService;

    @Resource(name = "enquiryFacade")
    private EnquiryFacade enquiryFacade;

    @Resource(name = "vikingCommerceCategoryService")
    private VikingCommerceCategoryService vikingCommerceCategoryService;

    @ModelAttribute(name = "isEnquiryFlow")
    public boolean isEnquiry() {
        return enquiryFacade.isEnquiryFlow();
    }


    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
    public String category(@PathVariable("categoryCode") final String categoryCode, // NOSONAR
                           @RequestParam(value = "q", required = false) final String searchQuery,
                           @RequestParam(value = "page", defaultValue = "0") final int page,
                           @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                           @RequestParam(value = "sort", required = false) final String sortCode, final Model model,
                           final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException
    {
        LOG.info("VIK-2750: Checking if current user is allowed to see category with code {}.", categoryCode);
        if (vikingCommerceCategoryService.getCategoryAuthorizedForUser(categoryCode).isPresent())
        {
            LOG.info("VIK-2750: user is allowed, doing a Solr search for category with code {}.", categoryCode);
            return performSearchAndGetResultsPage(categoryCode, searchQuery, page, showMode, sortCode, model, request, response);
        } else {
            LOG.error("VIK-2750: Category with code {} not authorized for current user!", categoryCode);
            throw new UnknownIdentifierException("Category is not authorized for current user");
        }
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
    public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
                                                      @RequestParam(value = "q", required = false) final String searchQuery,
                                                      @RequestParam(value = "page", defaultValue = "0") final int page,
                                                      @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                      @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
    }

    @ResponseBody
    @RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/results", method = RequestMethod.GET)
    public SearchResultsData<ProductData> getResults(@PathVariable("categoryCode") final String categoryCode,
                                                     @RequestParam(value = "q", required = false) final String searchQuery,
                                                     @RequestParam(value = "page", defaultValue = "0") final int page,
                                                     @RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
                                                     @RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException {
        return performSearchAndGetResultsData(categoryCode, searchQuery, page, showMode, sortCode);
    }

    protected String performSearchAndGetResultsPage(final String categoryCode, final String searchQuery,
                                                    final int page, // NOSONAR
                                                    final ShowMode showMode, final String sortCode, final Model model, final HttpServletRequest request,
                                                    final HttpServletResponse response) throws UnsupportedEncodingException
    {
        LOG.info("VIK-2750: Looking up category with code {}.", categoryCode);
        final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);


        LOG.info("VIK-2750: Looking up redirection needed for category with code {}.", categoryCode);
        final String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
        if (StringUtils.isNotEmpty(redirection))
        {
            LOG.info("VIK-2750: Redirect for category with code {}: {}", categoryCode, redirection);
            return redirection;
        }

        LOG.info("VIK-2750: getCategoryPage for category with code {}.", categoryCode);
        final CategoryPageModel categoryPage = getCategoryPage(category);

        LOG.info("VIK-2750: CategorySearchEvaluator for category with code {}.", categoryCode);

        LOG.info("VIK-2750:  CategorySearchEvaluator categoryCode {}.", categoryCode);
        LOG.info("VIK-2750:  CategorySearchEvaluator searchQuery {}.", searchQuery);
        LOG.info("VIK-2750:  CategorySearchEvaluator page {}.", page);
        LOG.info("VIK-2750:  CategorySearchEvaluator showMode {}.", showMode);
        LOG.info("VIK-2750:  CategorySearchEvaluator sortCode {}.", sortCode);
        LOG.info("VIK-2750:  CategorySearchEvaluator categoryPage {}.", categoryPage != null ? categoryPage.getUid() : "null");
        final CategorySearchEvaluator categorySearch = new CategorySearchEvaluator(categoryCode, searchQuery, page, showMode,
                sortCode, categoryPage);

        ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = null;

        LOG.info("VIK-2750: Before Solr search for category with code {}.", categoryCode);
        try
        {
            categorySearch.doSearch();
            searchPageData = categorySearch.getSearchPageData();
        }
        catch (final ConversionException e) // NOSONAR
        {
            LOG.error("VIK-2750: Error while doing Solr search for category for code {}.", categoryCode, e);
            searchPageData = createEmptySearchResult(categoryCode);
        }
//        if(searchPageData.getResults() != null) {
//            for (ProductData productData : searchPageData.getResults()) {
//                StockData stockData = new StockData();
//                stockData.setStockLevel(getVikingCommerceStockService().getStockLevelForProduct(productData.getCode()));
//                stockData.setStockLevelStatus(getVikingCommerceStockService().getStockLevelStatusForProduct(productData.getCode()));
//                stockData.setStockDate(getVikingCommerceStockService().getStockLevelDateForProduct(productData.getCode()));
//                stockData.setStockRange(getVikingCommerceStockService().getStockLevelRange(productData.getCode()));
//                productData.setStock(stockData);
//            }
//        }
        LOG.info("VIK-2750: checking for category with code {} if showCategoriesOnly {}.", categoryCode, categorySearch.isShowCategoriesOnly());
        final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

        LOG.info("VIK-2750: checking for subcategories for category with code {}.", categoryCode);
        //start - update facet values based on authorization on third level categories
        List<CategoryData> categoryFacet = searchPageData.getSubCategories();

        LOG.info("VIK-2750: subcategory facets for category with code {}.", categoryCode);

        if(CollectionUtils.isNotEmpty(categoryFacet)) {
            LOG.info("VIK-2750: Before auth, {} Facets found for category with code {}", categoryFacet.size(), categoryCode);
            for(CategoryData facet : categoryFacet) {
               LOG.info("VIK-2750: Before auth, Facet code for category with code {}: {} name: {}", facet.getCode(), facet.getName(), categoryCode);
           }
        } else {
            LOG.info("VIK-2750: Before auth, categoryFacet is empty for category with code {}", categoryCode);
        }

        if(categoryFacet != null)
        {
            int listPosition = 0;
            while(listPosition < categoryFacet.size())
            {
                CategoryData categoryData = categoryFacet.get(listPosition);
                if (vikingCommerceCategoryService.getCategoryAuthorizedForUser(categoryData.getCode()).isEmpty())
                {
                    LOG.info("VIK-2750: User NOT ALLOWED to see subcategory facet with code {} for category with code {}, removing...", categoryData.getCode() , categoryCode);
                    categoryFacet.remove(listPosition);
                } else {
                    LOG.info("VIK-2750: User is allowed to see subcategory facet with code {} for category with code {}", categoryData.getCode() , categoryCode);
                    listPosition++;
                }
            }

            if(CollectionUtils.isNotEmpty(categoryFacet)) {
                LOG.info("VIK-2750: After auth, {} Facets remain for category with code {}", categoryFacet.size(), categoryCode);
                for(CategoryData facet : categoryFacet) {
                    LOG.info("VIK-2750: After auth, Facet code for category with code {}: {} name: {}", facet.getCode(), facet.getName(), categoryCode);
                }
            } else {
                LOG.info("VIK-2750: After auth, categoryFacet is empty for category with code {}", categoryCode);
            }

            searchPageData.setSubCategories(categoryFacet);
        }
        //end

        LOG.info("VIK-2750: Before storeCMSPageInModel for category with code {}", categoryCode);
        storeCmsPageInModel(model, categorySearch.getCategoryPage());
        storeContinueUrl(request);

        LOG.info("VIK-2750: Before populate model for category with {}", categoryCode);
        populateModel(model, searchPageData, showMode);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, vikingSearchBreadcrumbBuilder.getBreadcrumbs(categoryCode, searchPageData));
        model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("pageType", PageType.CATEGORY.name());
        model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());
        model.addAttribute("seoText", category.getSeoText());

        LOG.info("VIK-2750: Before update page title for category with code {}", categoryCode);
        updatePageTitle(category, model);

        final RequestContextData requestContextData = getRequestContextData(request);
        requestContextData.setCategory(category);
        requestContextData.setSearch(searchPageData);

        LOG.info("VIK-2750: Before SEO Meta data addition for category with code {}", categoryCode);
        if (searchQuery != null)
        {
            model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        }

        setUpMetaDataForCategoryPage(model, category);
        LOG.info("VIK-2750: Before return for category with code {}", categoryCode);
        return getViewPage(categorySearch.getCategoryPage());

    }

    protected void setUpMetaDataForCategoryPage(final Model model, final CategoryModel categoryModel) {
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(categoryModel.getKeywords().stream()
                .map(keywordModel -> keywordModel.getKeyword()).collect(Collectors.toSet()));
        final String metaDescription = getDescriptionForCategoryPage(categoryModel);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    private String getDescriptionForCategoryPage(CategoryModel categoryModel) {
        if(StringUtils.isNotEmpty(categoryModel.getDescription())) {
            return MetaSanitizerUtil.sanitizeDescription(categoryModel.getDescription());
        }

        else return getMessageSource().getMessage("default.metadescription.categorypages", null, getI18nService().getCurrentLocale()).replace("{categoryName}", categoryModel.getName());
    }

    protected void updatePageTitle(final CategoryModel category, final Model model)
    {
        storeContentPageTitleInModel(model, vikingPageTitleResolver.resolveCategoryPageTitle(category));
    }

    protected class VikingCategorySearchEvaluator extends CategorySearchEvaluator
    {
        private final String categoryCode;
        private final SearchQueryData searchQueryData = new SearchQueryData();
        private final int page;
        private final ShowMode showMode;
        private final String sortCode;
        private CategoryPageModel categoryPage;
        private boolean showCategoriesOnly;
        private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

        public VikingCategorySearchEvaluator(final String categoryCode, final String searchQuery, final int page,
                                             final ShowMode showMode, final String sortCode, final CategoryPageModel categoryPage)
        {
            super(categoryCode,searchQuery,page,showMode,sortCode,categoryPage);
            this.categoryCode = categoryCode;
            this.searchQueryData.setValue(searchQuery);
            this.page = page;
            this.showMode = showMode;
            this.sortCode = sortCode;
            this.categoryPage = categoryPage;
        }

        public void doSearch()
        {
            showCategoriesOnly = false;
            if (searchQueryData.getValue() == null)
            {
                // Direct category link without filtering
                searchPageData = getProductSearchFacade().categorySearch(categoryCode);
                if (categoryPage != null)
                {
                    showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
                            && CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
                }
            }
            else
            {
                // We have some search filtering
                if (categoryPage == null || !categoryHasDefaultPage(categoryPage))
                {
                    // Load the default category page
                    categoryPage = getDefaultCategoryPage();
                }

                final SearchStateData searchState = new SearchStateData();
                searchState.setQuery(searchQueryData);

                final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
                searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
            }
        }

    }

    public VikingCommerceStockService getVikingCommerceStockService() {
        return vikingCommerceStockService;
    }

    public void setVikingCommerceStockService(VikingCommerceStockService vikingCommerceStockService) {
        this.vikingCommerceStockService = vikingCommerceStockService;
    }
}
