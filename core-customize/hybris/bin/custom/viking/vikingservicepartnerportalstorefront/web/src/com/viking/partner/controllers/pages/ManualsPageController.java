package com.viking.partner.controllers.pages;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualData;
import com.viking.core.util.ManualsHelper;
import com.viking.facades.Manuals.VikingManualsFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/service-manuals")
public class ManualsPageController extends AbstractSearchPageController {

    private static final Logger LOG = LogManager.getLogger(ManualsPageController.class);

    private static final String MANUAL_SERVICE_CMS_PAGE_LABEL = "service-manuals";
    @Resource(name = "vikingManualsFacade")
    private VikingManualsFacade vikingManualsFacade;
    @Resource(name = "userService")
    private UserService userService;
    @Resource(name = "manualsHelper")
    private ManualsHelper manualsHelper;

    @RequestMapping(value = "/{manual}", method = RequestMethod.GET)
    public String getManual(@PathVariable("manual") final String manual, final Model model) throws CMSItemNotFoundException {

        if (manualsHelper.isAccessDenied("/service-manuals/" + manual)) {
            return REDIRECT_PREFIX + ROOT;
        }

        final ManualData manualData = getManualData(manual);

        model.addAttribute("manuals", manualData);

        model.addAttribute("userid", getUserId());

        model.addAttribute("bulletins", getBulletins(manual));

        if (manualData != null) {
            model.addAttribute("name", manualData.getName());
        }

        storeCmsPageInModel(model, getContentPageForLabelOrId(MANUAL_SERVICE_CMS_PAGE_LABEL));

        return getViewForPage(model);
    }

    @RequestMapping(value = "/confirmationLetterAccept", produces = "application/json", method = {RequestMethod.POST, RequestMethod.GET})
    public Map<String, String> confirmationLetterAccept(@RequestParam("bulletinsId") final String bulletinsId, @RequestParam("manualsId") final String manualsId, final Model model) throws CMSItemNotFoundException {
        final Map<String, String> map = new HashMap<String, String>();

        String userId = userService.getCurrentUser().getUid();

        try {
            vikingManualsFacade.setManualBulletinsAccepted(bulletinsId, manualsId, userId);
        } catch (IOException e) {

            String message = "Bulletins successfully accepted";
            map.put("message", message);
        }
        return map;
    }

    @RequestMapping(value = "/download", method = {RequestMethod.GET})
    public void download(@RequestParam("url") final String url, @RequestParam("legacy") final String legacy, @RequestParam("extensionReq") final String extensionReq, @RequestParam("filename") final String filename, final HttpServletResponse response) {
        final String mediaUrl = vikingManualsFacade.download(url, legacy, Boolean.valueOf(extensionReq), filename);

        LOG.debug("Adding location header for url: " + mediaUrl);
        response.addHeader("location", mediaUrl);
    }

    private String getUserId() {
        return userService.getCurrentUser().getUid();
    }

    private ManualData getManualData(String manualId) {
        return vikingManualsFacade.getManual(manualId);
    }

    private List<BulletinsData> getBulletins(String manualId) {
        return vikingManualsFacade.getBulletins(manualId);
    }
}
