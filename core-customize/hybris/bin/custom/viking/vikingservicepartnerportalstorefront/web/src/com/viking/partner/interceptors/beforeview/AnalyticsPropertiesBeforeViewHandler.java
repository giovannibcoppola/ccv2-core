/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.interceptors.beforeview;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.config.ConfigIntf;

import static com.viking.core.productchanneldata.service.impl.DefaultProductChannelDataService.CUSTOMER_PORTAL_SITE_UID;
import static com.viking.core.productchanneldata.service.impl.DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
import static com.viking.core.productchanneldata.service.impl.DefaultProductChannelDataService.SERVICE_PARTNER_PORTAL_SITE_UID;


public class AnalyticsPropertiesBeforeViewHandler implements BeforeViewHandler {
    private static final String JIRAFE_API_URL = "jirafeApiUrl";
    private static final String JIRAFE_API_TOKEN = "jirafeApiToken";
    private static final String JIRAFE_APPLICATION_ID = "jirafeApplicationId";
    private static final String JIRAFE_VERSION = "jirafeVersion";
    private static final String ANALYTICS_TRACKING_ID = "googleAnalyticsTrackingId";
    private static final String GOOGLE_TAG_MANAGER_ID = "googleTagManagerId";
    private static final String JIRAFE_DATA_URL = "jirafeDataUrl";
    private static final String JIRAFE_SITE_ID = "jirafeSiteId";
    private static final String JIRAFE_PREFIX = "jirafe";
    private static final String GOOGLE_PREFIX = "googleAnalyticsTrackingId";
    // HashMap used to cache jirafe properties from config.properties. Not Using Cache implementations as it is
    // a simple,non growable cache s
    private static Map<String, String> jirafeMapCache = new HashMap<>();

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;
    @Resource(name = "hostConfigService")
    private HostConfigService hostConfigService;
    @Resource(name = "commonI18NService")
    private CommonI18NService commonI18NService;
    // Listener - listens to changes on the frontend and update the MapCache.
    private ConfigIntf.ConfigChangeListener cfgChangeListener;
	private static final String GA_SAFETYSTHOP_ID = "gtm.safetyshop.id";
	private static final String GA_SERVICEPARTNER_ID = "gtm.servicepartnerportal.id";
	private static final String GA_CUSTOMERPORTAL_ID = "gtm.customerportal.id";

    private static final String GTM_SAFETYSTHOP_ID = "gtm.safetyshop.tags.id";
    private static final String GTM_SERVICEPARTNER_ID = "gtm.servicepartnerportal.tags.id";
    private static final String GTM_CUSTOMERPORTAL_ID = "gtm.customerportal.tags.id";


    @Override
    public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView) {
        // Create the change listener and register it to listen when the config properties are changed in the platform
        if (cfgChangeListener == null) {
            registerConfigChangeLister();
        }
        final String serverName = request.getServerName();
        // Add config properties for google analytics
        if(modelAndView.getModel().get("siteUid") != null) {
            final String siteUid = modelAndView.getModel().get("siteUid").toString();
            switch (siteUid) {
                case SAFETY_SHOP_SITE_UID:
                    modelAndView.addObject(ANALYTICS_TRACKING_ID, configurationService.getConfiguration().getString(GA_SAFETYSTHOP_ID, ""));
                    modelAndView.addObject(GOOGLE_TAG_MANAGER_ID, configurationService.getConfiguration().getString(GTM_SAFETYSTHOP_ID, ""));
                    break;
                case SERVICE_PARTNER_PORTAL_SITE_UID:
                    modelAndView.addObject(ANALYTICS_TRACKING_ID, configurationService.getConfiguration().getString(GA_SERVICEPARTNER_ID, ""));
                    modelAndView.addObject(GOOGLE_TAG_MANAGER_ID, configurationService.getConfiguration().getString(GTM_SERVICEPARTNER_ID,""));
                    break;
                case CUSTOMER_PORTAL_SITE_UID:
                    modelAndView.addObject(ANALYTICS_TRACKING_ID, configurationService.getConfiguration().getString(GA_CUSTOMERPORTAL_ID,""));
                    modelAndView.addObject(GOOGLE_TAG_MANAGER_ID, configurationService.getConfiguration().getString(GTM_CUSTOMERPORTAL_ID,""));
                    break;
                default:
            }
        }

        // Add config properties for jirafe analytics
        addHostProperty(serverName, modelAndView, ThirdPartyConstants.Jirafe.API_URL, JIRAFE_API_URL);
        addHostProperty(serverName, modelAndView, ThirdPartyConstants.Jirafe.API_TOKEN, JIRAFE_API_TOKEN);
        addHostProperty(serverName, modelAndView, ThirdPartyConstants.Jirafe.APPLICATION_ID, JIRAFE_APPLICATION_ID);
        addHostProperty(serverName, modelAndView, ThirdPartyConstants.Jirafe.VERSION, JIRAFE_VERSION);
        addHostProperty(serverName, modelAndView, ThirdPartyConstants.Jirafe.DATA_URL, JIRAFE_DATA_URL);

        // Lookup a currency specific jirafe site id first, and only if it is missing fallback to the default site id
        final String currencyIso = commonI18NService.getCurrentCurrency().getIsocode().toLowerCase();
        final String currSpecKey = ThirdPartyConstants.Jirafe.SITE_ID + "." + currencyIso;
        final String nonSpecKey = ThirdPartyConstants.Jirafe.SITE_ID;
        if (jirafeMapCache.get(currSpecKey) == null) {
            final String currencySpecificJirafeSiteId = hostConfigService.getProperty(currSpecKey, serverName);
            jirafeMapCache.put(currSpecKey, currencySpecificJirafeSiteId);
        }
        if (jirafeMapCache.get(currSpecKey) != null
                && org.apache.commons.lang.StringUtils.isNotBlank(jirafeMapCache.get(currSpecKey).toString())) {
            modelAndView.addObject(JIRAFE_SITE_ID, jirafeMapCache.get(currSpecKey));
        } else {
            // Fallback to the non-currency specific value
            if (jirafeMapCache.get(nonSpecKey) == null) {
                final String jirafeSiteId = hostConfigService.getProperty(ThirdPartyConstants.Jirafe.SITE_ID, serverName);
                jirafeMapCache.put(nonSpecKey, jirafeSiteId);
            }
            modelAndView.addObject(JIRAFE_SITE_ID, jirafeMapCache.get(nonSpecKey));
        }
    }

    protected void registerConfigChangeLister() {
        final ConfigIntf config = Registry.getMasterTenant().getConfig();
        cfgChangeListener = new ConfigChangeListener();
        config.registerConfigChangeListener(cfgChangeListener);
    }

    protected void addHostProperty(final String serverName, final ModelAndView modelAndView, final String configKey,
                                   final String modelKey) {
        /*
         * Changes made to cache the jirafe properties files in a HashMap. The first time the pages are accessed the
         * values are read from the properties file & written on to a cache and the next time onwards it is accessed from
         * the cache.
         */
        if (jirafeMapCache.get(configKey) == null) {
            jirafeMapCache.put(configKey, hostConfigService.getProperty(configKey, serverName));
        }
        modelAndView.addObject(modelKey, jirafeMapCache.get(configKey));
    }

    protected class ConfigChangeListener implements ConfigIntf.ConfigChangeListener {
        @Override
        public void configChanged(final String key, final String newValue) {
            // Config Listener listen to changes on the platform config and updates the cache.
            if (key.startsWith(JIRAFE_PREFIX) || key.startsWith(GOOGLE_PREFIX)) {
                jirafeMapCache.remove(key);
                jirafeMapCache.put(key, newValue);
            }
        }
    }
}
