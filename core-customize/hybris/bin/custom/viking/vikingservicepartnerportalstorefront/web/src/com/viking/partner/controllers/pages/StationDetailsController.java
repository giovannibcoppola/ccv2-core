package com.viking.partner.controllers.pages;


import com.viking.core.data.VikingTechnicianData;
import com.viking.core.data.VikingTotalCreditData;
import com.viking.facades.station.VikingStationFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;


@Controller
public class StationDetailsController extends AbstractPageController {

    private static final String TECHNICIAN_OVERVIEW_CMS_PAGE_LABEL = "technicianoverviewpage";
    private static final String CREDIT_OVERVIEW_CMS_PAGE_LABEL = "creditoverviewpage";

    @Resource(name = "vikingStationFacade")
    private VikingStationFacade vikingStationFacade;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @RequestMapping(value = "/technician-overview", method = {RequestMethod.GET})
    public String getTechnicians(Model model) throws CMSItemNotFoundException {
        List<VikingTechnicianData> technicians = vikingStationFacade.getTechnicians();

        model.addAttribute("stationId", customerFacade.getCurrentCustomerUid());
        model.addAttribute("technicians", technicians);

        storeCmsPageInModel(model, getContentPageForLabelOrId(TECHNICIAN_OVERVIEW_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(TECHNICIAN_OVERVIEW_CMS_PAGE_LABEL));

        return getViewForPage(model);
    }

    @RequestMapping(value = "/credit-overview", method = {RequestMethod.GET})
    public String getCredits(Model model) throws CMSItemNotFoundException {
        List<VikingTotalCreditData> credits = vikingStationFacade.getCredits();

        model.addAttribute("stationId", customerFacade.getCurrentCustomerUid());
        model.addAttribute("credits", credits);

        storeCmsPageInModel(model, getContentPageForLabelOrId(CREDIT_OVERVIEW_CMS_PAGE_LABEL));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CREDIT_OVERVIEW_CMS_PAGE_LABEL));
        return getViewForPage(model);
    }


}
