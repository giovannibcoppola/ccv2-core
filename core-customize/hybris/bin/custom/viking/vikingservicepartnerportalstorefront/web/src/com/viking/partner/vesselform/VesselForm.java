/**
 *
 */
package com.viking.partner.vesselform;

/**
 * @author Srikanth
 *
 */
public class VesselForm
{
	String userId;
	String vesselID;
	String userName;

	/**
	 * @return the userId
	 */
	public String getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *           the userId to set
	 */
	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the vesselID
	 */
	public String getVesselID()
	{
		return vesselID;
	}

	/**
	 * @param vesselID
	 *           the vesselID to set
	 */
	public void setVesselID(final String vesselID)
	{
		this.vesselID = vesselID;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *           the userName to set
	 */
	public void setUserName(final String userName)
	{
		this.userName = userName;
	}

}
