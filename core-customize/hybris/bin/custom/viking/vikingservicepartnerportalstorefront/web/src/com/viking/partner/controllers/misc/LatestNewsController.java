package com.viking.partner.controllers.misc;

import com.viking.facades.news.VikingNewsFacade;
import com.viking.news.data.NewsData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class LatestNewsController extends AbstractController {

    @Resource(name = "vikingNewsFacade")
    private VikingNewsFacade vikingNewsFacade;

    @RequestMapping(value = "/latestNews", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void getLatestNews(final Model model) {

        final List<NewsData> news = vikingNewsFacade.getLatestNewsForCurrentSite();
        model.addAttribute("news", news);

    }
}
