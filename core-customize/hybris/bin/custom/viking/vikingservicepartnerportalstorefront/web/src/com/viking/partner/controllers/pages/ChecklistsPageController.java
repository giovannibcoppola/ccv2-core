package com.viking.partner.controllers.pages;

import com.viking.core.integration.azure.ChecklistsData;
import com.viking.facades.Manuals.VikingChecklistsFacade;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;

@Controller
@RequestMapping("/checklists")
public class ChecklistsPageController extends AbstractSearchPageController {
    private static final String CHECKLISTS_SERVICE_CMS_PAGE_LABEL = "checklists";
    @Resource(name = "vikingChecklistsFacade")
    private VikingChecklistsFacade vikingChecklistsFacade;
    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @RequestMapping(value = "/{checklist}", method = RequestMethod.GET)
    public String getManual(@PathVariable("checklist") final String checklist, final Model model) throws CMSItemNotFoundException {

        final ChecklistsData checklistsData = vikingChecklistsFacade.getChecklist(checklist);

        model.addAttribute("checklists", checklistsData);

        if (checklistsData != null) {
            model.addAttribute("name", checklistsData.getName());
        } else {
            model.addAttribute("name", checklist);
        }

        model.addAttribute("portalAccessKey", configurationService.getConfiguration().getString("azure.client.portal.accesskey"));

        storeCmsPageInModel(model, getContentPageForLabelOrId(CHECKLISTS_SERVICE_CMS_PAGE_LABEL));

        return getViewForPage(model);
    }
}
