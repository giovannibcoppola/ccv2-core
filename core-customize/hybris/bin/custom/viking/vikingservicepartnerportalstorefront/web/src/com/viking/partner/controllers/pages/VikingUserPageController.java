/**
 *
 */
package com.viking.partner.controllers.pages;

import com.viking.core.user.service.VikingUserPageService;

import com.viking.facades.integration.VikingVesselFacade;
import com.viking.facades.vessels.VesselData;
import com.viking.facades.vessels.VikingAssignVesselsFacade;
import com.viking.facades.vessels.data.VikingVesselPageData;

import com.viking.partner.controllers.ControllerConstants;
import com.viking.partner.vesselform.VesselForm;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author prabhakar
 *
 */
@Controller
@RequestMapping("/AssignVessels")
public class VikingUserPageController extends AbstractPageController
{
	private static final String USER_CMS_PAGE = "userCMSPage";

	private static final Logger LOG = Logger.getLogger(VikingUserPageController.class);

	@Resource(name = "userService")
	private UserService userService;

	@Autowired
	private VikingVesselFacade vikingVesselFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "vikingUserPageService")
	private VikingUserPageService vikingUserPageService;

	@Resource(name = "vikingAssignVesselsFacade")
	private VikingAssignVesselsFacade vikingAssignVesselsFacade;

	@SuppressWarnings("boxing")
	@RequestMapping(method = RequestMethod.GET)
	public String getUsersPage(final Model model) throws CMSItemNotFoundException
	{
		final List<CustomerData> allB2bCustomers = vikingAssignVesselsFacade.getAllUsers();
		if (allB2bCustomers != null)
		{
			model.addAttribute("customers", allB2bCustomers);
		}
		final List<VikingVesselPageData> vesselPageData = vikingAssignVesselsFacade.getVessels();
		if (vesselPageData != null)
		{
			model.addAttribute("vesselPageData", vesselPageData);
		}
		final int vesselAndUserAssignments = vikingAssignVesselsFacade.vesselAndUserAssignments();
		model.addAttribute("vesselSize", vesselAndUserAssignments);

		final UserModel user = userService.getCurrentUser();
		if (user instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customerModel = (B2BCustomerModel) user;
			final B2BUnitModel b2bUnit = customerModel.getDefaultB2BUnit();
			if (b2bUnit != null)
			{
				final Set<PrincipalGroupModel> userGroups = customerModel.getAllGroups();
				final List<PrincipalGroupModel> groups = userGroups.stream()
						.filter(p -> p.getUid().equals(B2BConstants.B2BADMINGROUP)).collect(Collectors.<PrincipalGroupModel> toList());
				final String unitId = StringUtils.trim(b2bUnit.getUid());
				if (groups.size() > 0 && groups.get(0) != null) //manager user
				{
					model.addAttribute("vesselsData", vikingVesselFacade.getAllVesselDataListByCustomerId(unitId));
				}
			}
		}
		final ContentPageModel usersCMSPage = getContentPageForLabelOrId(USER_CMS_PAGE);
		storeCmsPageInModel(model, usersCMSPage);
		setUpMetaDataForContentPage(model, usersCMSPage);
		return ControllerConstants.Views.Pages.Account.AccountVesselAndUserPage;

	}

	@SuppressWarnings("boxing")
	@RequestMapping(value = "{vesselID}/deleteuser/{id:.+}", method = RequestMethod.GET)
	public String delete(@PathVariable final String id, @PathVariable final String vesselID, final Model model)
			throws CMSItemNotFoundException
	{
		vikingAssignVesselsFacade.removeVesselFromUser(vesselID, id);
		final List<CustomerData> allB2bCustomers = vikingAssignVesselsFacade.getAllUsers();
		if (allB2bCustomers != null)
		{
			model.addAttribute("customers", allB2bCustomers);
		}
		final List<VikingVesselPageData> vesselPageData = vikingAssignVesselsFacade.getVessels();
		if (vesselPageData != null)
		{
			model.addAttribute("vesselPageData", vesselPageData);
		}
		final int vesselAndUserAssignments = vikingAssignVesselsFacade.vesselAndUserAssignments();
		model.addAttribute("vesselSize", vesselAndUserAssignments);
		final UserModel user = userService.getCurrentUser();
		if (user instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customerModel = (B2BCustomerModel) user;
			final B2BUnitModel b2bUnit = customerModel.getDefaultB2BUnit();
			if (b2bUnit != null)
			{
				final Set<PrincipalGroupModel> userGroups = customerModel.getAllGroups();
				final List<PrincipalGroupModel> groups = userGroups.stream()
						.filter(p -> p.getUid().equals(B2BConstants.B2BADMINGROUP)).collect(Collectors.<PrincipalGroupModel> toList());
				final String unitId = StringUtils.trim(b2bUnit.getUid());
				if (groups.size() > 0 && groups.get(0) != null) //manager user
				{
					model.addAttribute("vesselsData", vikingVesselFacade.getAllVesselDataListByCustomerId(unitId));
				}
			}
		}

		final ContentPageModel usersCMSPage = getContentPageForLabelOrId(USER_CMS_PAGE);
		storeCmsPageInModel(model, usersCMSPage);
		setUpMetaDataForContentPage(model, usersCMSPage);
		return ControllerConstants.Views.Pages.Account.AccountVesselAndUserPage;

	}

	@SuppressWarnings("boxing")
	@RequestMapping(value = "/saveDetails", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String save(@ModelAttribute("vessel") final VesselForm vesselForm, final Model model) throws CMSItemNotFoundException
	{
		final VesselData vesselData = new VesselData();
		if (vesselForm != null && vesselForm.getUserName() != null && vesselForm.getVesselID() != null
				&& StringUtils.isNotEmpty(vesselForm.getVesselID()))
		{
			vesselData.setUserName(vesselForm.getUserName());
			String[] vesselIDArray = vesselForm.getVesselID().split(",");

			for ( String vesselID : vesselIDArray ){

                String[] vesselIDs = vesselID.split("_");

                vesselData.setVesselName(vesselIDs[0]);
				vesselData.setVesselId(vesselIDs[1]);
				vikingAssignVesselsFacade.saveVesselsToUser(vesselData);
			}

		}
		final List<CustomerData> allB2bCustomers = vikingAssignVesselsFacade.getAllUsers();
		if (allB2bCustomers != null)
		{
			model.addAttribute("customers", allB2bCustomers);
		}
		final List<VikingVesselPageData> vesselPageData = vikingAssignVesselsFacade.getVessels();
		if (vesselPageData != null)
		{
			model.addAttribute("vesselPageData", vesselPageData);
		}
		final int vesselAndUserAssignments = vikingAssignVesselsFacade.vesselAndUserAssignments();
		model.addAttribute("vesselSize", vesselAndUserAssignments);
		final UserModel user = userService.getCurrentUser();
		if (user instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customerModel = (B2BCustomerModel) user;
			final B2BUnitModel b2bUnit = customerModel.getDefaultB2BUnit();
			if (b2bUnit != null)
			{
				final Set<PrincipalGroupModel> userGroups = customerModel.getAllGroups();
				final List<PrincipalGroupModel> groups = userGroups.stream()
						.filter(p -> p.getUid().equals(B2BConstants.B2BADMINGROUP)).collect(Collectors.<PrincipalGroupModel> toList());
				final String unitId = StringUtils.trim(b2bUnit.getUid());
				if (groups.size() > 0 && groups.get(0) != null) //manager user
				{
					model.addAttribute("vesselsData", vikingVesselFacade.getAllVesselDataListByCustomerId(unitId));
				}
			}
		}

		final ContentPageModel usersCMSPage = getContentPageForLabelOrId(USER_CMS_PAGE);
		storeCmsPageInModel(model, usersCMSPage);
		setUpMetaDataForContentPage(model, usersCMSPage);
		return ControllerConstants.Views.Pages.Account.AccountVesselAndUserPage;
	}
}