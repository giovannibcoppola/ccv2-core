package com.viking.partner.controllers.integration;

import com.viking.erpservices.service.forms.xml.VikingFormData;
import com.viking.erpservices.service.forms.xml.VikingFormsPreFillData;
import com.viking.erpservices.service.impl.DefaultVikingFormsPreFillDataService;
import com.viking.facades.forms.VikingFormsFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/vikingservicepartnerportalstorefront/vikingForms")
public class VikingFormsController extends BaseIntegrationController
{
	private static final Logger LOG = Logger.getLogger(VikingFormsController.class);

	@Autowired private DefaultVikingFormsPreFillDataService vikingFormsPreFillDataService;

	@Resource(name = "vikingFormsFacade")
	private VikingFormsFacade vikingFormsFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@RequestMapping(value = "/get_prefill_data/", method = RequestMethod.GET, produces = {MediaType.APPLICATION_XML_VALUE})
	public @ResponseBody
	VikingFormsPreFillData getPrefillData(final HttpServletRequest request, @RequestParam(value = "notificationId", required = false)  String notificationId, @RequestParam(value = "prefillId", required = false)  String prefillId){

		initializeSiteFromRequest(request);

		if(notificationId.contains(",")) {
			int indexOfComma = notificationId.indexOf(",");
			notificationId = notificationId.substring(0, indexOfComma);
		}

		if(prefillId.contains(",")) {
			int indexOfComma = prefillId.indexOf(",");
			prefillId = prefillId.substring(0, indexOfComma);
		}

		LOG.info("VIK-3151: NotificationId [" + notificationId + "]");
		LOG.info("VIK-3151: PrefillId [" + prefillId + "]");

		VikingFormsPreFillData preFillData = vikingFormsPreFillDataService.getData(notificationId, prefillId);

		if(preFillData != null && CollectionUtils.isNotEmpty(preFillData.getVikingFormData())) {
			for(VikingFormData vikingFormData : preFillData.getVikingFormData()) {
				LOG.info("VIK-3151: " + vikingFormData.getKey() + ": " + vikingFormData.getValue());
			}
		}

		return preFillData;
	}

	@RequestMapping(value = "/submitCertificateNotificationChecklist", method = RequestMethod.POST)
	public void submitNotificationyFormToSAP(@RequestBody String xml, @RequestParam(value = "notificationId", required = false)  String notificationId, @RequestParam(value = "serviceStation", required = false)  String serviceStation) {

		vikingFormsFacade.submitNotificationYFormToSAP(notificationId, serviceStation, xml);
	}

	@RequestMapping(value = "/submitCertificateNotificationChecklistAndSavePDF", method = RequestMethod.POST)
	public void submitNotificationyFormToSAPAndSavePDF(@RequestBody String xml, @RequestParam(value = "notificationId", required = false)  String notificationId, @RequestParam(value = "serviceStation", required = false)  String serviceStation, HttpServletResponse response) {

		vikingFormsFacade.submitNotificationYFormToSAPandSavePDF(notificationId, serviceStation, xml, "X");
	}


	@RequestMapping(value = "/getChecklistPDF", method = RequestMethod.GET)
	public void getChecklistPDF(
			@RequestParam(value = "notificationId", required = false) String notificationId,
			HttpServletResponse response) {
		if (notificationId == null) {
			LOG.error("notifivationId is null");
			return;
		}
		try {
			MediaModel mediaModel = mediaService.getMedia("form_pdf_" + notificationId);
			InputStream is = mediaService.getStreamFromMedia(mediaModel);
			response.setContentType("application/pdf");
			response.setHeader("Target", "_blank");
			IOUtils.copy(is, response.getOutputStream());
			response.getOutputStream().flush();
		} catch (IOException e) {
			LOG.error(e);
		} catch (UnknownIdentifierException | AmbiguousIdentifierException e) {
			LOG.error("Could not find media for " + notificationId);
			LOG.error(e);
		}
	}

	@RequestMapping(value = "/submitCondemnation", method = RequestMethod.POST)
	public void submitCondemnationToSAP(@RequestBody String xml) {
		vikingFormsFacade.submitCondemnationToSAP(xml);
	}
}

