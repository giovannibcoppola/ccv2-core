/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author chiranjitchakraborty
 */
public class VikingEnquiryAddressForm
{
	private String company;
	private String addressId;
	private String name;
	private String line1;
	private String line2;
	private String city;
	private String regionIso;
	private String postcode;
	private String countryIso;
	private String phone;
	private String email;
	private String comment;
	private String preferredContactMethod;
	private String customerReferenceNumber;
	private boolean hasComment;

	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	@NotNull(message = "{address.firstName.invalid}")
	@Size(min = 1, max = 255, message = "{address.firstName.invalid}")
	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	@NotNull(message = "{address.line1.invalid}")
	@Size(min = 1, max = 255, message = "{address.line1.invalid}")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@NotNull(message = "{address.townCity.invalid}")
	@Size(min = 1, max = 255, message = "{address.townCity.invalid}")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getRegionIso()
	{
		return regionIso;
	}

	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	@NotNull(message = "{address.postcode.invalid}")
	@Size(min = 1, max = 10, message = "{address.postcode.invalid}")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@NotNull(message = "{address.country.invalid}")
	@Size(min = 1, max = 255, message = "{address.country.invalid}")
	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(final String value)
	{
		phone = value;
	}
	public String getCompany() {
		return company;
	}

	@NotNull(message = "{address.company.invalid}")
	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	@NotNull(message = "{address.email.invalid}")
	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	@NotNull(message = "{address.comment.invalid}")
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPreferredContactMethod() {
		return preferredContactMethod;
	}

	@NotNull(message = "{address.preferredContactMethod.invalid}")
	public void setPreferredContactMethod(String preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}

	public String getCustomerReferenceNumber() {
		return customerReferenceNumber;
	}

	public void setCustomerReferenceNumber(String customerReferenceNumber) {
		this.customerReferenceNumber = customerReferenceNumber;
	}

	public boolean hasComment() {
		return hasComment;
	}

	@NotNull(message = "{address.hasComment.invalid}")
	public void setHasComment(boolean hasComment) {
		this.hasComment = hasComment;
	}

}
