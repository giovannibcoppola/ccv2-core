package com.viking.partner.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class VikingAddressForm extends AddressForm
{
	private String compName;

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}
}
