package com.viking.partner.controllers.pages;

import com.viking.facades.user.VikingUserFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/my-company/organization-management/manage-users")
public class VikingUserManagementPageController extends AbstractSearchPageController {

    private static final Logger LOG = LogManager.getLogger(VikingUserManagementPageController.class);

    protected static final String ORGANIZATION_MANAGEMENT_CMS_PAGE = "organizationManagement";

    @Resource (name = "messageSource")
    private MessageSource messageSource;
    @Resource ( name = "i18nService")
    private I18NService i18nService;

    @Resource (name = "userFacade")
    private VikingUserFacade vikingUserFacade;

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    @RequireHardLogIn
    public String disableUser(@RequestParam("user") final String user, final Model model, final RedirectAttributes redirectModel)
            throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
        final List<Breadcrumb> breadcrumbs = createManageUnitsDetailsBreadcrumbs(user);
        model.addAttribute("breadcrumbs", breadcrumbs);

        try {
            vikingUserFacade.removeUser(user);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.confirmation.user.removed");
        } catch (ModelRemovalException exception) {
            LOG.debug(exception);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.confirmation.user.remove.failed");
        } catch (IllegalArgumentException exception) {
            LOG.debug(exception);
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "text.confirmation.user.unable.to.remove.self");
        }

        return REDIRECT_PREFIX + "/my-company/organization-management/manage-users/";
    }

    private List<Breadcrumb> createManageUnitsDetailsBreadcrumbs(final String uid)
    {
        final List<Breadcrumb> breadcrumbs = this.createManageUnitsBreadcrumbs();
        breadcrumbs.add(new Breadcrumb(String.format("/my-company/organization-management/manage-units/details/?unit=%s",
                urlEncode(uid)), messageSource.getMessage("text.company.manage.units.details", new Object[]
                { uid }, "View Unit: {0} ", i18nService.getCurrentLocale()), null));
        return breadcrumbs;
    }

    private List<Breadcrumb> createManageUnitsBreadcrumbs()
    {
        final List<Breadcrumb> breadcrumbs = this.getBreadcrumbs(null);
        breadcrumbs.add(new Breadcrumb("/my-company/organization-management/manage-units/", messageSource.getMessage(
                "text.company.manage.units", null, i18nService.getCurrentLocale()), null));
        return breadcrumbs;
    }

    private List<Breadcrumb> getBreadcrumbs(final String resourceKey) throws IllegalArgumentException
    {
        final List<Breadcrumb> breadcrumbs = new ArrayList<Breadcrumb>();

        if (StringUtils.isNotBlank(resourceKey))
        {
            breadcrumbs.add(new Breadcrumb("#", getMessageSource()
                    .getMessage(resourceKey, null, getI18nService().getCurrentLocale()), null));
        }

        return breadcrumbs;
    }
}
