/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.interceptors.beforecontroller;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeControllerHandler;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;


/**
 */
public class SetSiteSettingsBeforeControllerHandler implements BeforeControllerHandler
{
	private static final Logger LOG = Logger.getLogger(SetSiteSettingsBeforeControllerHandler.class);

	private static final String CUSTOMER_PORTAL_LOOK = "customerPortalLook";

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;


	@Override
	public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
			final HandlerMethod handler)
	{

			if (cmsSiteService.getCurrentSite() != null)
			{
				request.setAttribute(CUSTOMER_PORTAL_LOOK, cmsSiteService.getCurrentSite().isCustomerPortalLookAndFeel());
		}

		return true;
	}




	public CMSSiteService getCmsSiteService() {
		return cmsSiteService;
	}

	public void setCmsSiteService(
			CMSSiteService cmsSiteService) {
		this.cmsSiteService = cmsSiteService;
	}
}
