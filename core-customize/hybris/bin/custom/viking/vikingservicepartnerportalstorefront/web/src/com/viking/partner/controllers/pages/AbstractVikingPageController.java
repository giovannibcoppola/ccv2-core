package com.viking.partner.controllers.pages;

import javax.annotation.Resource;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.stream.Collectors;

import com.viking.facades.enquiry.EnquiryFacade;

/**
 * Viking implementation of Base controller for all page controllers. Provides common functionality for all page controllers.
 *
 * @author javier.gomez
 */
public class AbstractVikingPageController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(AbstractVikingPageController.class);

    @Resource(name = "enquiryFacade")
    private EnquiryFacade enquiryFacade;
    @ModelAttribute(name = "isEnquiryFlow")
    public boolean isEnquiry() {
        return enquiryFacade.isEnquiryFlow();
    }
    @Override
    protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage) {
        final String metaKeywords = contentPage.getKeywords();
        final String metaDescription = getMetaDescriptionForContentPage(contentPage);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    protected void setUpMetaDataForProductDetailPage(final Model model, final ProductData productData) {
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
        final String metaDescription = getMetaDescriptionForProductPage(productData);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    protected void setUpMetaDataForCategoryPage(final Model model, final CategoryModel categoryModel) {
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(categoryModel.getKeywords().stream()
                .map(keywordModel -> keywordModel.getKeyword()).collect(Collectors.toSet()));
        final String metaDescription = getMetaDescriptionForCategoryPage(categoryModel);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    private String getMetaDescriptionForContentPage(ContentPageModel contentPage) {
        if (StringUtils.isNotEmpty(contentPage.getDescription())) {
            return MetaSanitizerUtil.sanitizeDescription(contentPage.getDescription());
        } else
            return MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage("default.metadescription.contentpages", null, getI18nService().getCurrentLocale()));
    }

    private String getMetaDescriptionForCategoryPage(CategoryModel categoryModel) {
        if (StringUtils.isNotEmpty(categoryModel.getDescription())) {
            return MetaSanitizerUtil.sanitizeDescription(categoryModel.getDescription());
        } else
            return MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage("default.metadescription.categorypages", null,
                    getI18nService().getCurrentLocale()).replace("{categoryName}", categoryModel.getName()));
    }

    private String getMetaDescriptionForProductPage(final ProductData productData) {
        if (StringUtils.isNotEmpty(productData.getMetadescription())) {
            return MetaSanitizerUtil.sanitizeDescription(productData.getMetadescription());
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(productData.getLevel3categoryName()).append(" ").append(productData.getCode()).append(" ").append(getFirstSentenceProductShortDescription(productData.getName()));
            return MetaSanitizerUtil.sanitizeDescription(sb.toString());
        }
    }

    private String getFirstSentenceProductShortDescription(String shortDescription) {
        if (StringUtils.isNotEmpty(shortDescription)) {
            if (shortDescription.contains(".")) {
                return shortDescription.substring(0, shortDescription.indexOf(".") + 1);
            }

            return shortDescription;
        }

        return "";
    }
}
