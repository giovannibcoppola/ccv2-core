package com.viking.breadcrumb.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.service.VikingCommerceCategoryService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Viking implementation of {@link SearchBreadcrumbBuilder}
 *
 * @author javier.gomez
 */
public class VikingSearchBreadcrumbBuilder extends SearchBreadcrumbBuilder {

    private static final String LAST_LINK_CLASS = "active";

    @Resource(name = "vikingCommerceCategoryService")
    private VikingCommerceCategoryService vikingCommerceCategoryService;

    @Override
    public List<Breadcrumb> getBreadcrumbs(final String categoryCode, final String searchText, final boolean emptyBreadcrumbs) {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();

        try {
            if (StringUtils.isEmpty(categoryCode) || (StringUtils.isNotEmpty(categoryCode) && !categoryCode.equalsIgnoreCase(VikingCoreConstants.ALL_PRODUCTS_CATEGORY_CODE))) {
                Optional<CategoryModel> categoryModelOptional = vikingCommerceCategoryService.getCategoryAuthorizedForUser(VikingCoreConstants.ALL_PRODUCTS_CATEGORY_CODE);
                if(categoryModelOptional.isPresent()) {
                    breadcrumbs.add(getAllProductsBreadcrumb(categoryModelOptional.get()));
                }
            }
        } catch (UnknownIdentifierException e) {
            //Customer does not have rights to see this category, so All Products is not shown on the breadcrumb
        }

        breadcrumbs.addAll(super.getBreadcrumbs(categoryCode, searchText, emptyBreadcrumbs));
        return breadcrumbs;
    }

    @Override
    protected void createBreadcrumbCategoryHierarchyPath(final String categoryCode, final boolean emptyBreadcrumbs,
                                                         final List<Breadcrumb> breadcrumbs)
    {
        // Create category hierarchy path for breadcrumb
        final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
        final Collection<CategoryModel> categoryModels = new ArrayList<>();
        final CategoryModel lastCategoryModel = getCommerceCategoryService().getCategoryForCode(categoryCode);
        categoryModels.addAll(lastCategoryModel.getSupercategories());
        categoryBreadcrumbs.add(getCategoryBreadcrumb(lastCategoryModel, !emptyBreadcrumbs ? LAST_LINK_CLASS : ""));

        while (!categoryModels.isEmpty())
        {
            final CategoryModel categoryModel = categoryModels.iterator().next();
            if (!(categoryModel instanceof ClassificationClassModel))
            {
                if (categoryModel != null)
                {
                    if(BooleanUtils.isNotTrue(categoryModel.isRootCategory())){
                        categoryBreadcrumbs.add(getCategoryBreadcrumb(categoryModel));
                    }
                    categoryModels.clear();
                    categoryModels.addAll(categoryModel.getSupercategories());
                }
            }
            else
            {
                categoryModels.remove(categoryModel);
            }
        }
        Collections.reverse(categoryBreadcrumbs);
        breadcrumbs.addAll(categoryBreadcrumbs);
    }

    private Breadcrumb getAllProductsBreadcrumb(CategoryModel categoryModel) {
        String url = getCategoryModelUrlResolver().resolve(categoryModel);
        return new Breadcrumb(url, categoryModel.getName(), null);
    }
}
