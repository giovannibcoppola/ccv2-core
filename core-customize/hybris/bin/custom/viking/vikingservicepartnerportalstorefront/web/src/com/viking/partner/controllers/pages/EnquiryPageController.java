package com.viking.partner.controllers.pages;

import com.viking.core.storefront.util.VikingPageTitleResolver;
import com.viking.facades.country.VikingCountryFacade;
import com.viking.facades.enquiry.EnquiryFacade;
import com.viking.forms.VikingEnquiryAddressForm;
import com.viking.partner.controllers.ControllerConstants;
import com.viking.validator.VikingEnquiryAddressFormValidator;
import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.*;

/**
 * Controller for enquiry page
 */
@Controller
public class EnquiryPageController extends AbstractVikingPageController {

    private static final Logger LOG = LogManager.getLogger(EnquiryPageController.class);

    private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";
    private static final String REDIRECT_THANK_YOU = REDIRECT_PREFIX + ControllerConstants.Views.Pages.ENQUIRY.INQUIRY_THANK_YOU_URL;
    private static final String REDIRECT_INQUIRY_FORM = REDIRECT_PREFIX + ControllerConstants.Views.Pages.ENQUIRY.ENQUIRY_URL;
    private static final String ENQUIRY_CMS_PAGE = "enquiry";
    private static final String INQUIRY_THANK_YOU_CMS_PAGE = "inquiryThankYou";
    private static final String UNSUCCESSFUL_MESSAGE = "text.enquiry.created.unsuccessfully.message";
    private static final String VALIDATION_ERROR_MESSAGE = "text.enquiry.validation.error.message";

    public static final String VIKING_ENQUIRY_ADDRESS_FORM = "vikingEnquiryAddressForm";

    @Resource(name = "cartFacade")
    private CartFacade cartFacade;

    @Resource(name = "i18NFacade")
    private I18NFacade i18NFacade;

    @Resource(name = "userFacade")
    private UserFacade userFacade;

    @Resource(name = "vikingCountryFacade")
    private VikingCountryFacade vikingCountryFacade;

    @Resource(name = "enquiryFacade")
    private EnquiryFacade enquiryFacade;

    @Resource
    private CustomerNameStrategy customerNameStrategy;

    @Resource
    private VikingEnquiryAddressFormValidator vikingEnquiryAddressFormValidator;

    @Resource(name = "vikingPageTitleResolver")
    private VikingPageTitleResolver vikingPageTitleResolver;

    @Resource(name = "simpleBreadcrumbBuilder")
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

    @ModelAttribute(name = "countries")
    public List<CountryData> getCountries() {
        List<CountryData> countries = vikingCountryFacade.getAllCountries();

        Collections.sort(countries, Comparator.comparing(CountryData::getName));

        return countries;
    }

    @ModelAttribute(name = "isEnquiryFlow")
    public boolean isEnquiry() {
        return enquiryFacade.isEnquiryFlow();
    }

    @RequestMapping(value = ControllerConstants.Views.Pages.ENQUIRY.ENQUIRY_URL,method = RequestMethod.GET)
    public String getEnquiryPage(final Model model, final RedirectAttributes redirectModel, final @ModelAttribute(VIKING_ENQUIRY_ADDRESS_FORM) VikingEnquiryAddressForm vikingEnquiryAddressForm) throws CMSItemNotFoundException {
        SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

        if (!cartFacade.hasEntries())
        {
            LOG.info("Missing or empty cart");

            // No session cart or empty session cart. Bounce back to the cart page.
            GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.checkout.empty.cart",
                    null);
            return REDIRECT_CART_URL;
        }

        return getEnquiryPage(model, new VikingEnquiryAddressForm());
    }

    private String getEnquiryPage(Model model, VikingEnquiryAddressForm vikingEnquiryAddressForm) throws CMSItemNotFoundException {
        final ContentPageModel enquiryPage = getContentPageForLabelOrId(ENQUIRY_CMS_PAGE);
        model.addAttribute("cartEntries", cartFacade.getSessionCart().getEntries());
        model.addAttribute(VIKING_ENQUIRY_ADDRESS_FORM,  vikingEnquiryAddressForm);
        storeCmsPageInModel(model, enquiryPage);
        setUpMetaDataForContentPage(model, enquiryPage);
        updatePageTitle(model, enquiryPage);

        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.enquiryform"));

        return ControllerConstants.Views.Fragments.Enqiury.EnquiryPage;
    }

    /**
     *
     * @param model The model
     * @return the thank you page
     * @throws CMSItemNotFoundException if a cms item has not been found
     */
    @RequestMapping(value = ControllerConstants.Views.Pages.ENQUIRY.INQUIRY_THANK_YOU_URL,method = RequestMethod.GET)
    public String getInquiryThankYouPage(Model model) throws CMSItemNotFoundException {
        final ContentPageModel inquiryThankYouPage = getContentPageForLabelOrId(INQUIRY_THANK_YOU_CMS_PAGE);
        storeCmsPageInModel(model, inquiryThankYouPage);
        setUpMetaDataForContentPage(model, inquiryThankYouPage);
        updatePageTitle(model, inquiryThankYouPage);
        model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
        model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.inquirythankyou"));

        return ControllerConstants.Views.Fragments.Enqiury.InquiryThankYouPage;
    }

    /**
     *
     * @param vikingEnquiryAddressForm the vikingEnquiryAddressForm
     * @param bindingResult the bindResult
     * @param attributes the redirectAttributes
     * @return redirects to the thankyoupage
     */
    @RequestMapping(value = ControllerConstants.Views.Pages.ENQUIRY.ENQUIRY_URL,method = RequestMethod.POST)
    public String addEnquiry(final VikingEnquiryAddressForm vikingEnquiryAddressForm, final BindingResult bindingResult, RedirectAttributes attributes) {
        vikingEnquiryAddressFormValidator.validate(vikingEnquiryAddressForm, bindingResult);
        if (!bindingResult.hasErrors()) {
            try {
                enquiryFacade.saveEnquiry(buildAddressData(vikingEnquiryAddressForm), vikingEnquiryAddressForm.getComment());
                cartFacade.removeSessionCart();
            } catch(Exception e) {
                LOG.error(e);
                return getRedirectToInquiryForm(attributes, UNSUCCESSFUL_MESSAGE);
            }
        }else{
            attributes.addFlashAttribute(VIKING_ENQUIRY_ADDRESS_FORM, vikingEnquiryAddressForm);
            getViewWithBindingErrorMessages(attributes, bindingResult);
            return REDIRECT_INQUIRY_FORM;
        }
        return REDIRECT_THANK_YOU;
    }

    protected void getViewWithBindingErrorMessages(final RedirectAttributes attributes, final BindingResult bindingErrors) {
        Map<String, String> errors  = new HashMap<>();
        for (final ObjectError error : bindingErrors.getAllErrors())
        {
            errors.put(String.valueOf(((FieldError) error).getField()), error.getCode());
        }
        attributes.addFlashAttribute("errors", errors);
    }

    private String getRedirectToInquiryForm(RedirectAttributes attributes, String message) {
        GlobalMessages.addFlashMessage(attributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
                message);
        return REDIRECT_INQUIRY_FORM;
    }

    private AddressData buildAddressData( final VikingEnquiryAddressForm vikingEnquiryAddressForm) {
        final AddressData address = new AddressData();
        final String[] nameArray = customerNameStrategy.splitName(vikingEnquiryAddressForm.getName());
        address.setFirstName(nameArray[0]);
        address.setLastName(nameArray[1]);
        address.setEmail(vikingEnquiryAddressForm.getEmail());
        address.setPhone(vikingEnquiryAddressForm.getPhone());
        address.setCountry(i18NFacade.getCountryForIsocode(vikingEnquiryAddressForm.getCountryIso()));
        address.setLine1(vikingEnquiryAddressForm.getLine1());
        address.setLine2(vikingEnquiryAddressForm.getLine2());
        address.setPostalCode(vikingEnquiryAddressForm.getPostcode());
        address.setTown(vikingEnquiryAddressForm.getCity());
        address.setCompanyName(vikingEnquiryAddressForm.getCompany());
        address.setCustomerReferenceNumber(vikingEnquiryAddressForm.getCustomerReferenceNumber());
        address.setPreferredContactMethod(vikingEnquiryAddressForm.getPreferredContactMethod());
        return address;
    }

    protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
    {
        storeContentPageTitleInModel(model, vikingPageTitleResolver.resolveContentPageTitle(cmsPage));
    }
}
