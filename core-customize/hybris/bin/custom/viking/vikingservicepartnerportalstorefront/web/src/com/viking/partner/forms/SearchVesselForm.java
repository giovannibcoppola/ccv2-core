package com.viking.partner.forms;

public class SearchVesselForm {


    private String searchTerm;

    private String searchType;

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }





    public String getSearchTerm()
    {
        return searchTerm;
    }

    public void setSearchTerm(final String searchTerm)
    {
        this.searchTerm = searchTerm;
    }

}
