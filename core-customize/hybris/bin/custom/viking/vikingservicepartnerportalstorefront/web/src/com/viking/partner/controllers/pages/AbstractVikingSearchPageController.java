package com.viking.partner.controllers.pages;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.cms2.jalo.pages.ContentPage;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

public class AbstractVikingSearchPageController extends AbstractSearchPageController {

    private static final String SEARCH_META_DESCRIPTION_ON = "search.meta.description.on";
    private static final String SEARCH_META_DESCRIPTION_RESULTS = "search.meta.description.results";

    protected void setUpMetaDataForContentPage(final Model model, final ContentPageModel contentPage) {
        final String metaKeywords = contentPage.getKeywords();
        final String metaDescription = getMetaDescriptionForContentPage(contentPage);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    protected void setUpMetaDataSearchPage(final Model model, final String searchText) {
        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
        final String metaDescription = getMetaDescriptionForSearchPage(searchText);

        setUpMetaData(model, metaKeywords, metaDescription);
    }

    private String getMetaDescriptionForContentPage(ContentPageModel contentPage) {
        if(StringUtils.isNotEmpty(contentPage.getDescription())) {
            return MetaSanitizerUtil.sanitizeDescription(contentPage.getDescription());
        }

        else return MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage("default.metadescription.contentpages", null, getI18nService().getCurrentLocale()));
    }

    private String getMetaDescriptionForSearchPage(final String searchText) {
            return MetaSanitizerUtil
                    .sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null,
                            SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " "
                            + getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
                            getI18nService().getCurrentLocale())
                            + " " + VikingCoreConstants.VIKING);
    }
}
