package com.viking.breadcrumb.impl;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Viking implementation of {@link ProductBreadcrumbBuilder}
 *
 * @author javier.gomez
 */
public class VikingProductBreadcrumbBuilder extends ProductBreadcrumbBuilder {

    private CommerceCategoryService commerceCategoryService;
    private static final String LAST_LINK_CLASS = "active";

    @Override
    public List<Breadcrumb> getBreadcrumbs(final String productCode)
    {
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        try {
            breadcrumbs.add( getAllProductsBreadcrumb(VikingCoreConstants.ALL_PRODUCTS_CATEGORY_CODE));
        } catch (UnknownIdentifierException e) {
            //Customer does not have rights to see this category, so All Products is not shown on the breadcrumb
        }

        breadcrumbs.addAll(super.getBreadcrumbs(productCode));
        return breadcrumbs;
    }

    @Override
    protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels, final CategoryModel toDisplay)
    {
        CategoryModel categoryToDisplay = toDisplay;
        for (final CategoryModel categoryModel : categoryModels)
        {
            if (BooleanUtils.isNotTrue(categoryModel.isRootCategory()) && getProductAndCategoryHelper().isValidProductCategory(categoryModel) )
            {
                if (categoryToDisplay == null)
                {
                    categoryToDisplay = categoryModel;
                }
                if (getBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null)
                {
                    break;
                }
            }
        }
        return categoryToDisplay;
    }

    private Breadcrumb getAllProductsBreadcrumb(String categoryCode) {
        final CategoryModel categoryModel = getCommerceCategoryService().getCategoryForCode(categoryCode);
        String url = getCategoryModelUrlResolver().resolve(categoryModel);
        return new Breadcrumb(url, categoryModel.getName(), null);
    }

    protected CommerceCategoryService getCommerceCategoryService()
    {
        return commerceCategoryService;
    }

    @Required
    public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService)
    {
        this.commerceCategoryService = commerceCategoryService;
    }
}