/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.viking.vikingyforms.controllers;

/**
 */
public interface VikingyformsControllerConstants
{
	// implement here controller constants used by this extension
}
