/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.viking.interceptors;

import com.viking.constants.VikingearlyloginaddonWebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * secureportaladdon will use our customized login.jsp
 * in vikingearlyloginaddon
 */
public class VikingSecurePortalBeforeViewHandler implements BeforeViewHandler {
    public static final String VIEW_NAME_MAP_KEY = "viewName";
    private final Map<String, Map<String, String>> spViewMap;
    private final CMSSiteService cmsSiteService;

    /**
     * Mandatory attribute
     *
     * @param spViewMap      map containing the path to the view
     * @param cmsSiteService site service
     */
    public VikingSecurePortalBeforeViewHandler(final Map<String, Map<String, String>> spViewMap, final CMSSiteService cmsSiteService) {
        this.spViewMap = spViewMap;
        this.cmsSiteService = cmsSiteService;
    }

    @Override public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView) throws Exception {
        final String viewName = modelAndView.getViewName();

		if (cmsSiteService.getCurrentSite().isRequiresAuthentication() && spViewMap.containsKey(viewName)){
			//secureCustomerPortalBeforeViewHandler set the view to an OOTB location.
			//This line replaces it with the view from the vikingearlyloginaddon addon
			//located in vikingearlyloginaddon/acceleratoraddon/web/webroot/WEB-INF/views/responsive/pages
			modelAndView.setViewName(VikingearlyloginaddonWebConstants.VIEW_PAGE_PREFIX + spViewMap.get(viewName).get(VIEW_NAME_MAP_KEY));
		}
    }

}
