package com.viking.interceptors.beforecontroller;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.secureportaladdon.interceptors.SecurePortalRequestProcessor;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class VikingSecurePortalBeforeControllerHandlerTest {

    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private UserService userService;
    @Mock
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
    @Mock
    private RedirectStrategy redirectStrategy;
    @Mock
    private SecurePortalRequestProcessor requestProcessor;
    @Mock
    private SessionService sessionService;

    @InjectMocks
    private VikingSecurePortalBeforeControllerHandler vikingSecurePortalBeforeControllerHandler;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        vikingSecurePortalBeforeControllerHandler.setDefaultLoginUri("/login");
        vikingSecurePortalBeforeControllerHandler.setCheckoutLoginUri("/login");
        vikingSecurePortalBeforeControllerHandler.setUnsecuredUris(Collections.emptySet());
        vikingSecurePortalBeforeControllerHandler.setControlUris(Collections.emptySet());
    }

    @Test
    public void testBeforeController() throws Exception {

        final CMSSiteModel cmsSiteModel = mock(CMSSiteModel.class);
        final UserModel userModel = mock(UserModel.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HandlerMethod handler = mock(HandlerMethod.class);

        when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.isRequiresAuthentication()).thenReturn(true);
        when(requestProcessor.skipSecureCheck()).thenReturn(false);
        when(userService.getCurrentUser()).thenReturn(userModel);
        when(userService.isAnonymousUser(userModel)).thenReturn(true);
        when(request.getRequestURI()).thenReturn("/cart");
        when(siteBaseUrlResolutionService.getWebsiteUrlForSite(eq(cmsSiteModel), eq(true), eq("/login"))).thenReturn("https://servicepartnerportal.viking-life.com/login");

        vikingSecurePortalBeforeControllerHandler.beforeController(request, response, handler);

        //verify that the redirection url is relative
        verify(redirectStrategy, times(1)).sendRedirect(eq(request), eq(response), eq("/login"));

    }
}
