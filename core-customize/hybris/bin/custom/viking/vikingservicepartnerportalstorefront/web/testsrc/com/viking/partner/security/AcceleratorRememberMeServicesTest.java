package com.viking.partner.security;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;

@UnitTest
public class AcceleratorRememberMeServicesTest {


  private AcceleratorRememberMeServices acceleratorRememberMeServices;



  @Mock
  private CMSSiteModel cmsSiteModel;

  @Mock
  private CMSSiteService cmsSiteService;

  @Mock
   UserDetailsService userDetailsService;

  @Before
  public void setUp() throws Exception
  {
    MockitoAnnotations.initMocks(this);
    acceleratorRememberMeServices = new AcceleratorRememberMeServices("asas", userDetailsService);
    acceleratorRememberMeServices.setCmsSiteService(cmsSiteService);

  }

  @Test
  public void testLoginForUserNotPartofCustomerGroup()
  {

    given(cmsSiteService.getCurrentSite()).willReturn(cmsSiteModel);
    given(cmsSiteModel.getAlwaysRemember()).willReturn(Boolean.TRUE);

    Assert.assertTrue(acceleratorRememberMeServices.rememberMeRequested(null, null));
  }

}
