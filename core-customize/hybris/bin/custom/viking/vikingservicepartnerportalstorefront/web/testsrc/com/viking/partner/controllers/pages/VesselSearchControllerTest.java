package com.viking.partner.controllers.pages;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.viking.core.util.EquipmentToYformIdTranslator;
import com.viking.erpservices.service.VikingNotificationService;
import com.viking.erpservices.service.impl.DefaultVikingChangeVesselService;
import com.viking.erpservices.service.impl.DefaultVikingCreateVesselService;
import com.viking.erpservices.service.impl.DefaultVikingDetailsVesselService;
import com.viking.erpservices.service.impl.DefaultVikingEquipmentVesselService;
import com.viking.erpservices.service.impl.DefaultVikingFindVesselsService;
import com.viking.facades.Manuals.VikingManualsFacade;
import com.viking.facades.equipment.VikingEquipmentFacade;
import com.viking.facades.flagstate.VikingFlagStateFacade;
import com.viking.facades.forms.VikingFormsFacade;
import com.viking.facades.forms.impl.DefaultVikingNotificationPrefillFacade;
import com.viking.search.data.VesselNotificationDataList;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.cms2.data.PagePreviewCriteriaData;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;

import static com.viking.partner.controllers.pages.VesselSearchController.ADD_EQUIPMENT_CMS_PAGE_LABEL;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;


@UnitTest
public class VesselSearchControllerTest {
    private final Model model = Mockito.spy(new BindingAwareModelMap());
    @InjectMocks
    VesselSearchController vesselSearchController;
    @Mock
    private DefaultVikingDetailsVesselService vikingDetailsVesselsService;
    @Mock
    private DefaultVikingFindVesselsService vikingFindVesselsService;
    @Mock
    private DefaultVikingEquipmentVesselService vikingEquipmentVesselService;
    @Mock
    private CustomerFacade customerFacade;
    @Mock
    private VikingNotificationService vikingNotificationService;
    @Mock
    private VikingFormsFacade vikingFormsFacade;
    @Mock
    private DefaultVikingCreateVesselService vikingCreateVesselService;
    @Mock
    private DefaultVikingChangeVesselService vikingChangeVesselService;
    @Mock
    private DefaultVikingNotificationPrefillFacade vikingNotificationPrefillFacade;
    @Mock
    private VikingFlagStateFacade vikingFlagStateFacade;
    @Mock
    private VikingEquipmentFacade vikingEquipmentFacade;
    @Mock
    private VikingManualsFacade vikingManualsFacade;
    @Mock
    private ConfigurationService configurationService;
    @Mock
    private EquipmentToYformIdTranslator equipmentToYformIdTranslator;
    @Mock
    private HttpServletRequest request;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private CMSPageService cmsPageService;
    @Mock
    private StoreSessionFacade storeSessionFacade;
    @Mock
    private PageTitleResolver pageTitleResolver;
    @Mock
    private SessionService sessionService;
    @Mock
    private HostConfigService hostConfigService;
    @Mock
    private MessageSource messageSource;
    @Mock
    private I18NService i18nService;
    @Mock
    private SiteConfigService siteConfigService;
    @Mock
    private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
    @Mock
    private UserFacade userFacade;
    @Mock
    private BaseSiteService baseSiteService;
    @Mock
    private ConsentFacade consentFacade;
    @Mock
    private CMSPreviewService cmsPreviewService;
    @Mock
    private ContentPageModel theContentPageModel = new ContentPageModel();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addEquipmentIsCorrect() throws Exception {
        //Given
        final String theId = "theId";
        final PagePreviewCriteriaData thePagePreviewData = new PagePreviewCriteriaData();
        final CustomerData theCustomerData = new CustomerData();
        final String theSerialNumber = "theSerialNumber";
        final String theCustomerUid = "theCustomerUid";
        final PageTemplateModel thePageTemplate = new PageTemplateModel();
        RedirectAttributes redirectModel = new RedirectAttributesModelMap();
        theCustomerData.setUid(theCustomerUid);
        setUpMocks(thePagePreviewData, theCustomerData, theSerialNumber, theCustomerUid, thePageTemplate);

        //When
        final String actualResult = vesselSearchController.loadAddEquipmentPage(theId, request, model, redirectModel);

        //Then
        Assert.assertEquals("pages/targetPage", actualResult);
    }


    @Test
    public void getNotificationFailsReturnsCorrectError() throws Exception {
        //Given
        final String theId = "theId";
        final PagePreviewCriteriaData thePagePreviewData = new PagePreviewCriteriaData();
        final CustomerData theCustomerData = new CustomerData();
        final String theSerialNumber = "theSerialNumber";
        final String theCustomerUid = "theCustomerUid";
        final PageTemplateModel thePageTemplate = new PageTemplateModel();
        RedirectAttributes redirectModel = new RedirectAttributesModelMap();
        theCustomerData.setUid(theCustomerUid);
        setUpMocks(thePagePreviewData, theCustomerData, theSerialNumber, theCustomerUid, thePageTemplate);
        doThrow(new BackendException("failed")).when(vikingNotificationService).getNotifications(theSerialNumber, theCustomerUid);
        //When
        final String actualResult = vesselSearchController.loadAddEquipmentPage(theId, request, model, redirectModel);

        //Then
        Assert.assertTrue(redirectModel.getFlashAttributes().containsKey("accErrorMsgs"));
        Assert.assertEquals("redirect:/vesselSearch/addEquipment/theId", actualResult);
    }

    private void setUpMocks(PagePreviewCriteriaData thePagePreviewData, CustomerData theCustomerData, String theSerialNumber, String theCustomerUid, PageTemplateModel thePageTemplate) throws Exception {
        when(request.getParameter("add")).thenReturn("true");
        when(request.getParameter("serialnumber")).thenReturn(theSerialNumber);
        when(theContentPageModel.getTitle()).thenReturn("theTitle");
        when(theContentPageModel.getMasterTemplate()).thenReturn(thePageTemplate);
        when(cmsPreviewService.getPagePreviewCriteria()).thenReturn(thePagePreviewData);
        when(cmsPageService.getPageForLabelOrId(ADD_EQUIPMENT_CMS_PAGE_LABEL, thePagePreviewData)).thenReturn(theContentPageModel);
        when(equipmentToYformIdTranslator.convertToId(anyString())).thenReturn("theType");
        when(customerFacade.getCurrentCustomer()).thenReturn(theCustomerData);
        when(vikingNotificationService.getNotifications(theSerialNumber, theCustomerUid)).thenReturn(new VesselNotificationDataList());
        when(cmsPageService.getFrontendTemplateName(thePageTemplate)).thenReturn("targetPage");
    }
}
