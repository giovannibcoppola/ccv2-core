/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.interceptors.beforeview;

import com.viking.partner.interceptors.beforecontroller.SetSiteSettingsBeforeControllerHandler;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.servicelayer.session.SessionService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;


@UnitTest
public class SetSiteSettingsBeforeControllerHandlerTest
{

	private static final Logger LOG = Logger.getLogger(SetSiteSettingsBeforeControllerHandlerTest.class);

	private final SetSiteSettingsBeforeControllerHandler restorationHandler = new SetSiteSettingsBeforeControllerHandler();


	@Mock
	private CMSSiteService cmsSiteService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		restorationHandler.setCmsSiteService(cmsSiteService);


	}

	@Test
	public void doBefore() {

		restorationHandler.beforeController(null, null, null);

	}









}
