package com.viking.partner.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

@UnitTest
public class SPPSUrlRedirectFilterTest {

    @InjectMocks
    private SPPSUrlRedirectFilter sppsUrlRedirectFilter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDoFilter_sppsUrl() throws ServletException, IOException {
        final StringBuffer url = new StringBuffer("/spps/servicepartnerportal/en/login");

        when(request.getRequestURL()).thenReturn(url);

        sppsUrlRedirectFilter.doFilterInternal(request, response, filterChain);

        verify(response, times(1)).sendRedirect(sppsUrlRedirectFilter.SERVICEPARTNERPORTAL_EN_LOGIN);
    }

    @Test
    public void testDoFilter_no_sppsUrl() throws ServletException, IOException {
        final StringBuffer url = new StringBuffer("/servicepartnerportal/en/login");

        when(request.getRequestURL()).thenReturn(url);

        sppsUrlRedirectFilter.doFilterInternal(request, response, filterChain);

        verify(response, never()).sendRedirect(sppsUrlRedirectFilter.SERVICEPARTNERPORTAL_EN_LOGIN);
        verify(filterChain, times(1)).doFilter(request, response);
    }
}
