/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.controllers.integration;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class VikingFormsControllerTest
{

	@InjectMocks
	private final VikingFormsController controller = Mockito.spy(new VikingFormsController());

	@Mock
	private  MediaService mediaService;

	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);

		BDDMockito.given(mediaService.getMedia(Mockito.anyString())).willThrow(
				UnknownIdentifierException.class);
	}



	@Test
	public void shoudlNotThrowNullpointerWhenMediaNotfound()
	{

		controller.getChecklistPDF(null, null);
		controller.getChecklistPDF("test", null);
	}


}
