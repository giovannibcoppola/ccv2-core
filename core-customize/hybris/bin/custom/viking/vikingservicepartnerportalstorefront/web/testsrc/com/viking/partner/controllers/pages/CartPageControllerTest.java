/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.partner.controllers.pages;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.viking.core.storefront.util.VikingPageTitleResolver;
import com.viking.facades.enquiry.EnquiryFacade;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.commercefacades.consent.ConsentFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import junit.framework.Assert;

import static com.viking.partner.controllers.pages.CartPageController.HIDE_CART_OPTIONS;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
public class CartPageControllerTest
{

	private final CartPageController controller = Mockito.spy(new CartPageController());

	private final Model page = Mockito.spy(new BindingAwareModelMap());

	@Mock
	private SiteConfigService service;

	@Mock
	private SessionService sessionService;

	@Mock
	private CartRestorationData restorationData;

	@Mock
	private EnquiryFacade enquiryFacade;

	@Mock
	private CMSPageService cmsPageService;

	@Mock
	private StoreSessionFacade storeSessionFacade;

	@Mock
	private CustomerFacade customerFacade;

	@Mock
	private PageTitleResolver pageTitleResolver;

	@Mock
	private HostConfigService hostConfigService;

	@Mock
	private MessageSource messageSource;

	@Mock
	private I18NService i18nService;

	@Mock
	private SiteConfigService siteConfigService;

	@Mock
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Mock
	private UserFacade userFacade;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private ConsentFacade consentFacade;

	@Mock
	private CMSPreviewService cmsPreviewService;

	@Mock
	private CartFacade cartFacade;

	@Mock
	private AcceleratorCheckoutFacade checkoutFacade;

	@Mock
	private SaveCartFacade saveCartFacade;

	@Mock
	private VikingPageTitleResolver vikingPageTitleResolver;

	@InjectMocks
	CartPageController cartPageController;

	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);
		//
		BDDMockito.given(controller.getSiteConfigService()).willReturn(service);
		restorationData = mock(CartRestorationData.class);
	}

	@Test
	public void testNullProperty()
	{
		BDDMockito.given(service.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn(null);

		Assert.assertFalse(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testSomeStringProperty()
	{
		BDDMockito.given(service.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("someString");

		Assert.assertFalse(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testTrueStringProperty()
	{
		BDDMockito.given(service.getBoolean(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS, false)).willReturn(Boolean.TRUE);

		Assert.assertTrue(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testZeroStringProperty()
	{
		BDDMockito.given(service.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("0");

		Assert.assertFalse(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testEmptyStringProperty()
	{
		BDDMockito.given(service.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn("");

		Assert.assertFalse(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testEmptyEmptyStringProperty()
	{
		BDDMockito.given(service.getProperty(CartPageController.SHOW_CHECKOUT_STRATEGY_OPTIONS)).willReturn(" ");

		Assert.assertFalse(controller.isCheckoutStrategyVisible());
	}

	@Test
	public void testSetupCartPageNullRestoration()
	{
		final CartPageController cartPageController = getCartPageController();
		BDDMockito.given(sessionService.getAttribute(WebConstants.CART_RESTORATION)).willReturn(null);
		cartPageController.setupCartPageRestorationData(page);
		Assert.assertTrue(page.containsAttribute("showModifications"));
	}

	@Test
	public void testSetupCartPageErrorRestoration()
	{
		final CartPageController cartPageController = getCartPageController();
		BDDMockito.given(sessionService.getAttribute(WebConstants.CART_RESTORATION)).willReturn(restorationData);
		BDDMockito.given(sessionService.getAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS)).willReturn(
				WebConstants.CART_RESTORATION_ERROR_STATUS);
		cartPageController.setupCartPageRestorationData(page);

		Assert.assertTrue(page.containsAttribute("restorationErrorMsg"));
	}

	@Test
	public void testSetupCartPageSuccessfulRestoration()
	{
		final CartPageController cartPageController = getCartPageController();
		BDDMockito.given(sessionService.getAttribute(WebConstants.CART_RESTORATION)).willReturn(restorationData);
		cartPageController.setupCartPageRestorationData(page);

		Assert.assertTrue(page.containsAttribute("restorationData"));
		Assert.assertTrue(page.containsAttribute("showModifications"));
	}

	public CartPageController getCartPageController()
	{
		return new CartPageController()
		{
			@Override
			protected SessionService getSessionService()
			{
				return sessionService;
			}
		};

	}

	@Test
	public void hideCartOptionsIsCorrectForSafetyShop() throws CMSItemNotFoundException {
		//Given
		final BaseSiteModel baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid("");
		CartData cartData = new CartData();
		when(enquiryFacade.isEnquiryFlow()).thenReturn(true);
		when(cartFacade.getSessionCartWithEntryOrdering(false)).thenReturn(cartData);
		when(cmsPageService.getPageForLabelOrId(any(), any())).thenReturn(mock(ContentPageModel.class));
		when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

		//When
		cartPageController.prepareDataForPage(page);

		//Then
		verify(page, Mockito.times(1)).addAttribute(HIDE_CART_OPTIONS, true);
	}

	@Test
	public void hideCartOptionsIsCorrectForNotSafetyShop() throws CMSItemNotFoundException {
		//Given
		final BaseSiteModel baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid("");
		CartData cartData = new CartData();
		when(enquiryFacade.isEnquiryFlow()).thenReturn(false);
		when(cartFacade.getSessionCartWithEntryOrdering(false)).thenReturn(cartData);
		when(cmsPageService.getPageForLabelOrId(any(), any())).thenReturn(mock(ContentPageModel.class));
		when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);

		//When
		cartPageController.prepareDataForPage(page);

		//Then
		verify(page, Mockito.times(0)).addAttribute(HIDE_CART_OPTIONS, true);
	}
}
