package com.viking.validator;

import com.viking.forms.VikingEnquiryAddressForm;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.BindingResult;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@UnitTest
public class VikingEnquiryAddressFormValidatorTest {

    @InjectMocks
    private VikingEnquiryAddressFormValidator vikingEnquiryAddressFormValidator;

    @Mock
    private BindingResult bindingResult;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testValidate() {
        VikingEnquiryAddressForm vikingEnquiryAddressForm = new VikingEnquiryAddressForm();
        vikingEnquiryAddressFormValidator.validate(vikingEnquiryAddressForm,bindingResult);

        verify(bindingResult, times(1)).rejectValue("company", "vikingEnquiryAddressForm.company.invalid");
    }
}
