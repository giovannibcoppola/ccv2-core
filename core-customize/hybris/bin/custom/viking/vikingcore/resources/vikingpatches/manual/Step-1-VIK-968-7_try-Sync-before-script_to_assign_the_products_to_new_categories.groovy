package vikingpatches.manual

import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.store.BaseStoreModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils

def modelService = spring.getBean "modelService"
def catalogVersionService = spring.getBean "catalogVersionService"
def productService = spring.getBean "productService"
def mediaContainerService = spring.getBean "mediaContainerService"
def mediaService = spring.getBean "mediaService"
def categoryService = spring.getBean "categoryService"
def flexibleSearchService = spring.getBean "flexibleSearchService"
def baseStoreService = spring.getBean "baseStoreService"
//Catalogs
def PARTNER_PRODUCT_CATALOG = "partnerProductCatalog"
def STAGED_CATALOG_VERSION = "Staged"
def DEFAULT_CATALOG = "Default"
def PARTNER_BASESTORE_UID = "partner"
def partnerProductCatalog = catalogVersionService.getCatalogVersion(PARTNER_PRODUCT_CATALOG, STAGED_CATALOG_VERSION)
def defaultCatalog = catalogVersionService.getCatalogVersion(DEFAULT_CATALOG, STAGED_CATALOG_VERSION)
def products = productService.getAllProductsForCatalogVersion(partnerProductCatalog)
def langs = ['en', 'in', 'us', 'it', 'ko', 'da', 'fi', 'es', 'tr', 'no', 'pl', 'fr', 'ru', 'ja', 'se', 'is', 'de', 'zh', 'et', 'hi', 'cs', 'sv', 'hu'] ////zh_TW, es_CO



println products.size() + '  products found'

products.each {
    product ->
        def productCode = product.code
        try {
            def target = productService.getProductForCode(defaultCatalog, productCode)
            // ================== ESSENTIAL  ==================
            langs.each {
                lang ->
                    if (!StringUtils.isBlank(product.getName(new Locale(lang)))) {
                        target.setName(product.getName(new Locale(lang)), new Locale(lang))
                    } //Name
                    if (!StringUtils.isBlank(product.getDescription(new Locale(lang)))) {
                        target.setDescription(product.getDescription(new Locale(lang)))
                    } // Description
                    if (!StringUtils.isBlank(product.getMetadescription(new Locale(lang)))) {
                        target.setMetadescription(product.getMetadescription(new Locale(lang)), new Locale(lang))
                    } //metadescription
                    if (!StringUtils.isBlank(product.getRemarks(new Locale(lang)))) {
                        target.setRemarks(product.getRemarks(new Locale(lang)), new Locale(lang))
                    } //Remarks
                    if (!StringUtils.isBlank(product.getSummary(new Locale(lang)))) {
                        target.setSummary(product.getSummary(new Locale(lang)), new Locale(lang))
                    } //summary
            }

            if (CollectionUtils.isNotEmpty(product.getStores(new Locale('en')))) {
                target.setStores(product.getStores(new Locale('en')))
            } else {
                final Set<BaseStoreModel> stores = new HashSet<BaseStoreModel>();
                final BaseStoreModel store = baseStoreService.getBaseStoreForUid(PARTNER_BASESTORE_UID);
                stores.add(store)
                target.setStores(stores, new Locale('en'))
            }

            target.setModelNumber(product.modelNumber);
            target.setApprovalStatus(product.approvalStatus);
            target.setPRDHA(product.PRDHA);

            // ================ DOCUMENTATION =====================================
            if (CollectionUtils.isNotEmpty(product.galleryImages)) {
                println 'galleryImages' + product.galleryImages
                List<MediaContainerModel> mediaContainers = new ArrayList<>()
                product.galleryImages.each {
                    gallery ->
                        if (null != gallery) {
                            try {
                                MediaContainerModel container = new MediaContainerModel();
                                container.setCatalogVersion(defaultCatalog);
                                container.setQualifier(gallery.qualifier);
                                mediaContainers.add(flexibleSearchService.getModelByExample(container))
                            } catch (final Exception exc) {

                                println 'galleryImages is not found !!!'
                            }

                        }
                }
                target.setGalleryImages(mediaContainers);
            }
            if (product.picture != null && !StringUtils.isBlank(product.picture.code)) {
                try {
                    target.setPicture(mediaService.getMedia(defaultCatalog, product.picture.code));
                } catch (final Exception e) {
                    println 'Picture is not found !!!'

                }
            }
            //=============================================================================================
            //CATEGORIES CONTAINING THIS PRODUCT
            List<CategoryModel> superCategories = product.supercategories
            if (CollectionUtils.isNotEmpty(superCategories)) {
                List<CategoryModel> superCategoriesFromDefaultCatalog = new ArrayList<CategoryModel>()
                superCategories.each {
                    superCategorie ->
                        if (superCategorie != null) {
                            try {
                                superCategoriesFromDefaultCatalog.add(categoryService.getCategoryForCode(defaultCatalog, superCategorie.code))
                            } catch (Exception exception2) {
                            }
                        }
                }
                target.setSupercategories(superCategoriesFromDefaultCatalog);
            }
            target.setSegments(product.segments);
            //=============================================================================================
            // ADMINISTRATION
            target.setLOVEM(product.LOVEM);
            target.setPRDHA(product.PRDHA);
            target.setArticleStatus(product.articleStatus);
            target.setBlacklistedBaseStores(product.blacklistedBaseStores);
            target.setBundleTemplates(product.bundleTemplates);
            target.setBuyerIDS(product.buyerIDS);
            target.setComments(product.comments);
            target.setConditionalBundleRules(product.conditionalBundleRules);
            target.setContentUnit(product.contentUnit);
            target.setData_sheet(product.data_sheet);
            target.setDeliveryModes(product.deliveryModes);
            target.setDeliveryTime(product.deliveryTime);
            if (CollectionUtils.isNotEmpty(product.detail)) {
                target.setDetail(product.detail);
            }
            target.setEan(product.ean);
            target.setEndLineNumber(product.endLineNumber);
            target.setErpGroupBuyer(product.erpGroupBuyer);
            target.setEurope1Discounts(product.europe1Discounts);
            target.setEurope1Prices(product.europe1Prices);
            target.setErpGroupSupplier(product.erpGroupSupplier);
            target.setEurope1Taxes(product.europe1Taxes);
            target.setFeatures(product.features);
            target.setLinkComponents(product.linkComponents);
            target.setLogo(product.logo);
            target.setManufacturerAID(product.manufacturerAID);
            target.setManufacturerName(product.manufacturerName);
            target.setManufacturerTypeDescription(product.manufacturerTypeDescription);
            target.setMaxOrderQuantity(product.maxOrderQuantity);
            target.setMinOrderQuantity(product.minOrderQuantity);
            target.setNormal(product.normal);
            target.setOfflineDate(product.offlineDate);
            target.setOnlineDate(product.onlineDate);
            target.setOrder(product.order);
            target.setOrderQuantityInterval(product.orderQuantityInterval);
            if (CollectionUtils.isNotEmpty(product.others)) {
                target.setOthers(product.others);
            }
            target.setOwnEurope1Discounts(product.ownEurope1Discounts);
            target.setProductDetailComponents(product.productDetailComponents);
            target.setProductFeatureComponents(product.productFeatureComponents);
            target.setProductListComponents(product.productListComponents);
            target.setProductOrderLimit(product.productOrderLimit);
            target.setProductReviews(product.productReviews);
            target.setPromotions(product.promotions);
            target.setRestrictions(product.restrictions);
            target.setReviewCount(product.reviewCount);
            target.setReviewRating(product.reviewRating);
            target.setSegment(product.segment);
            target.setSequenceId(product.sequenceId);
            target.setSoldIndividually(product.soldIndividually);
            target.setSpecialTreatmentClasses(product.specialTreatmentClasses);
            target.setStartLineNumber(product.startLineNumber);
            target.setStockLevels(product.stockLevels);
            target.setSupplierAlternativeAID(product.supplierAlternativeAID);
            target.setTargetBundleRules(product.targetBundleRules);
            target.setThumbnail(product.thumbnail);
            target.setThumbnails(product.thumbnails);
            target.setUntypedFeatures(product.untypedFeatures);
            target.setVendors(product.vendors);
            target.setViking_certificationDocuments(product.viking_certificationDocuments);
            target.setViking_drawings(product.viking_drawings);
            target.setViking_files(product.viking_files);
            target.setViking_manuals(product.viking_manuals);
            target.setViking_productinfo(product.viking_productinfo);
            target.setViking_videos(product.viking_videos);
            target.setVikingdangerlevel(product.vikingdangerlevel);
            target.setXmlcontent(product.xmlcontent);
            //=============================================================================================
            modelService.save(target)

        } catch (final Exception ex) {
            println ' | ---- Product ' + productCode + ' not found in ' + DEFAULT_CATALOG + ' Catalog'
            // ================== ESSENTIAL  ==================
            ProductModel source = productService.getProductForCode(partnerProductCatalog, productCode)
            def target = modelService.create(ProductModel.class);
            target.setCode(source.code);
            target.setCatalogVersion(defaultCatalog)
            langs.each {
                lang ->
                    if (StringUtils.isNotBlank(source.getName(new Locale(lang)))) {
                        target.setName(source.getName(new Locale(lang)), new Locale(lang))
                    } //Name
                    if (StringUtils.isNotBlank(source.getDescription(new Locale(lang)))) {
                        target.setDescription(source.getDescription(new Locale(lang)))
                    } // Description
                    if (StringUtils.isNotBlank(source.getMetadescription(new Locale(lang)))) {
                        target.setMetadescription(source.getMetadescription(new Locale(lang)), new Locale(lang))
                    } //metadescription
                    if (StringUtils.isNotBlank(source.getRemarks(new Locale(lang)))) {
                        target.setRemarks(source.getRemarks(new Locale(lang)), new Locale(lang))
                    } //Remarks
                    if (StringUtils.isNotBlank(source.getSummary(new Locale(lang)))) {
                        target.setSummary(source.getSummary(new Locale(lang)), new Locale(lang))
                    } //summary
            }
            // Stores
            if (CollectionUtils.isNotEmpty(source.getStores(new Locale('en')))) {
                target.setStores(source.getStores(new Locale('en')))
            } else {
                final Set<BaseStoreModel> stores = new HashSet<BaseStoreModel>();
                final BaseStoreModel store = baseStoreService.getBaseStoreForUid(PARTNER_BASESTORE_UID);
                stores.add(store)
                target.setStores(stores, new Locale('en'))
            }
            target.setModelNumber(source.modelNumber)
            target.setApprovalStatus(source.approvalStatus)
            target.setPRDHA(source.PRDHA)

            // ================ DOCUMENTATION =====================================
            if (CollectionUtils.isNotEmpty(source.galleryImages)) {

                List<MediaContainerModel> mediaContainers = new ArrayList<>()
                source.galleryImages.each {
                    gallery ->
                        println gallery.qualifier
                        if (null != gallery) {
                            try {
                                MediaContainerModel container = new MediaContainerModel();
                                container.setCatalogVersion(defaultCatalog);
                                container.setQualifier(gallery.qualifier);
                                mediaContainers.add(flexibleSearchService.getModelByExample(container))
                                target.setGalleryImages(mediaContainers);
                            } catch (final Exception exc) {
                            }
                        }
                }
            }
            println(' picture : ' + source.picture)
            if (source.picture != null && !StringUtils.isBlank(source.picture.code)) {
                try {
                    target.setPicture(mediaService.getMedia(defaultCatalog, source.picture.code));
                } catch (final Exception exception) {
                }
            }

            //=============================================================================================
            //CATEGORIES CONTAINING THIS PRODUCT
            List<CategoryModel> superCategories = source.supercategories
            if (CollectionUtils.isNotEmpty(superCategories)) {
                List<CategoryModel> superCategoriesFromDefaultCatalog = new ArrayList<CategoryModel>()
                superCategories.each {
                    superCategorie ->
                        if (superCategorie != null) {
                            try {
                                superCategoriesFromDefaultCatalog.add(categoryService.getCategoryForCode(defaultCatalog, superCategorie.code))
                            } catch (Exception exception2) {
                            }
                        }
                }
                target.setSupercategories(superCategoriesFromDefaultCatalog);
            }
            target.setSegments(source.segments);

            //=============================================================================================
            // ADMINISTRATION
            target.setLOVEM(source.LOVEM);
            target.setPRDHA(source.PRDHA);
            target.setArticleStatus(source.articleStatus);
            target.setBlacklistedBaseStores(source.blacklistedBaseStores);
            target.setBundleTemplates(source.bundleTemplates);
            target.setBuyerIDS(source.buyerIDS);
            target.setComments(source.comments);
            target.setConditionalBundleRules(source.conditionalBundleRules);
            target.setContentUnit(source.contentUnit);
            target.setData_sheet(source.data_sheet);
            target.setDeliveryModes(source.deliveryModes);
            target.setDeliveryTime(source.deliveryTime);
            if (CollectionUtils.isNotEmpty(source.detail)) {
                target.setDetail(source.detail);
            }
            target.setEan(source.ean);
            target.setEndLineNumber(source.endLineNumber);
            target.setErpGroupBuyer(source.erpGroupBuyer);
            target.setEurope1Discounts(source.europe1Discounts);
            target.setEurope1Prices(source.europe1Prices);
            target.setErpGroupSupplier(source.erpGroupSupplier);
            target.setEurope1Taxes(source.europe1Taxes);
            target.setFeatures(source.features);
            target.setLinkComponents(source.linkComponents);
            target.setLogo(source.logo);
            target.setManufacturerAID(source.manufacturerAID);
            target.setManufacturerName(source.manufacturerName);
            target.setManufacturerName(source.manufacturerTypeDescription);
            target.setMaxOrderQuantity(source.maxOrderQuantity);
            target.setMinOrderQuantity(source.minOrderQuantity);
            target.setNormal(source.normal);
            target.setOfflineDate(source.offlineDate);
            target.setOnlineDate(source.onlineDate);
            target.setOrder(source.order);
            target.setOrderQuantityInterval(source.orderQuantityInterval);
            if (CollectionUtils.isNotEmpty(source.others)) {
                target.setOthers(source.others);
            }
            target.setOwnEurope1Discounts(source.ownEurope1Discounts);
            target.setProductDetailComponents(source.productDetailComponents);
            target.setProductFeatureComponents(source.productFeatureComponents);
            target.setProductListComponents(source.productListComponents);
            target.setProductOrderLimit(source.productOrderLimit);
            target.setProductReviews(source.productReviews);
            target.setPromotions(source.promotions);
            target.setRestrictions(source.restrictions);
            target.setReviewCount(source.reviewCount);
            target.setReviewRating(source.reviewRating);
            target.setSegment(source.segment);
            target.setSequenceId(source.sequenceId);
            target.setSoldIndividually(source.soldIndividually);
            target.setSpecialTreatmentClasses(source.specialTreatmentClasses);
            target.setStartLineNumber(source.startLineNumber);
            target.setStockLevels(source.stockLevels);
            target.setSupplierAlternativeAID(source.supplierAlternativeAID);
            target.setTargetBundleRules(source.targetBundleRules);
            target.setThumbnail(source.thumbnail);
            target.setThumbnails(source.thumbnails);
            target.setUntypedFeatures(source.untypedFeatures);
            target.setVendors(source.vendors);
            target.setViking_certificationDocuments(source.viking_certificationDocuments);
            target.setViking_drawings(source.viking_drawings);
            target.setViking_files(source.viking_files);
            target.setViking_manuals(source.viking_manuals);
            target.setViking_productinfo(source.viking_productinfo);
            target.setViking_videos(source.viking_videos);
            target.setVikingdangerlevel(source.vikingdangerlevel);
            target.setXmlcontent(source.xmlcontent);
            //=============================================================================================
            modelService.save(target)
        }
}
