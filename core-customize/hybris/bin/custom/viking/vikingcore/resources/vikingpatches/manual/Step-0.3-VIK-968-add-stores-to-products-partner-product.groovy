package vikingpatches.manual

import de.hybris.platform.store.BaseStoreModel
import java.util.Locale;

def modelService = spring.getBean "modelService"
def catalogVersionService = spring.getBean "catalogVersionService"
def productService = spring.getBean "productService"

def baseStoreService = spring.getBean "baseStoreService"
//Catalogs
def PARTNER_PRODUCT_CATALOG = "safetyshopProductCatalog"
def STAGED_CATALOG_VERSION = "Online"
def PARTNER_BASESTORE_UID = "partner"
def partnerProductCatalog = catalogVersionService.getCatalogVersion(PARTNER_PRODUCT_CATALOG, STAGED_CATALOG_VERSION)
def products = productService.getAllProductsForCatalogVersion(partnerProductCatalog)

println products.size() + '  products found'

products.filter{p -> p.getStores() == null || p.getStores().isEmpty()}.each {
    product ->
        final Set<BaseStoreModel> stores = new HashSet<BaseStoreModel>();
        final BaseStoreModel store = baseStoreService.getBaseStoreForUid(PARTNER_BASESTORE_UID);
        stores.add(store)
        product.setStores(stores, Locale.ENGLISH)
        println product.code;
        try {
            modelService.save(product);
            println "done"
        }catch(def error){
            println error;

        }

}
modelService.saveAll(products);

println "done"