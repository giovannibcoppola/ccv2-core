def STAGED_CATALOG_VERSION = "Online"
def DEFAULT_CATALOG = "partnerProductCatalog"

def vikingCommerceCategoryService = spring.getBean "vikingCommerceCategoryService"

def modelService = spring.getBean "modelService"

def flexibleSearchService = spring.getBean "flexibleSearchService"

def catalogVersionService = spring.getBean "catalogVersionService"

def defaultCatalog = catalogVersionService.getCatalogVersion(DEFAULT_CATALOG, STAGED_CATALOG_VERSION)

def result = flexibleSearchService.search("Select {PK} from {category} where {catalogVersion} = " + defaultCatalog.getPk() ).getResult();

println "total categories:  " + result.size();

result.each {

    cat ->
        println "category : "+ cat.getCode();
        try {
            def store = vikingCommerceCategoryService.getStoreIdForCategory(cat).get();
            println "store: " + store;
            cat.setStoreId(store);

            modelService.save(cat);
            println "done";
        }catch(def e) {
            println e;
        }

}

//modelService.saveAll(result)