import de.hybris.platform.core.model.media.MediaFormatModel

def mediaService = spring.getBean "mediaService"
def cmsMediaFormatDao = spring.getBean "cmsMediaFormatDao"

def catalogVersionService = spring.getBean "catalogVersionService"

def productService = spring.getBean "productService"
def modelService = spring.getBean "modelService"

def DEFAULT_CATALOG = "Default"
//Catalogs
def defaultCV = catalogVersionService.getCatalogVersion(DEFAULT_CATALOG, "Staged")
def products = productService.getAllProductsForCatalogVersion(defaultCV)

def batchList = products.collate 1000
println("total number of batches of 1000 products : ${batchList.size()}")

List<MediaFormatModel> list = new ArrayList<>();
for (MediaFormatModel mediaFormatModel : cmsMediaFormatDao.getAllMediaFormats()) {
    if (mediaFormatModel.getQualifier().equals("300Wx300H")) {
        list.add(mediaFormatModel)
    }
}
final MediaFormatModel mediaFormatModel1 = list.get(0);

batchList.eachWithIndex { productSubList, i ->
    println("processing of 1000 products for the batch no ${i + 1}")
    productSubList.each { product ->
        if (product.getPicture() != null) {
            def thePicture = product.getPicture()
            try {
                def theMedia = mediaService.getMedia(defaultCV, product.getPicture().getCode())
                theMedia.setMediaFormat(mediaFormatModel1)
                modelService.save(thePicture)
            }
            catch (Exception e) {
                println("Error" + product.getCode())
            }
        }
    }
    println("processed successfully")
}

