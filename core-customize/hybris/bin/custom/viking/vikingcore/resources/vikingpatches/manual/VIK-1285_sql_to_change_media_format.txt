Step 1) Change the TypePkString for each env.

Update MediaFormat set TypePkString='8796147417170',p_conversion='-resize 300X300', p_conversionstrategy='imageMagickMediaConversionStrategy' where p_qualifier='300Wx300H'


Step 2) Create new conversion group
UPDATE ConversionGroup;code[unique=true];name[lang=en];supportedFormats(qualifier);
                             ;VikingConversionGroup;Viking conversion with working File;96Wx96H,1200Wx1200H,515Wx515H,365Wx246H,300Wx300H,65Wx65H,30Wx30H;


UPDATE MediaConversionCronJob;code[unique=true];job(code);sessionLanguage(isocode);includedFormats(qualifier);catalogVersion(catalog(id),version);asynchronous
;VikingMediaConversionCronjob;mediaConversionJob;en;96Wx96H,1200Wx1200H,515Wx515H,365Wx246H,300Wx300H,65Wx65H,30Wx30H;Default:Staged;true

