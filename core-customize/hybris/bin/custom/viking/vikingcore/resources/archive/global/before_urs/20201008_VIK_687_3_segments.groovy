package archive.global.before_urs

import de.hybris.platform.core.Registry
import java.sql.ResultSet
conn = Registry.getCurrentTenant().getDataSource().getConnection(true)
statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)
delete = """
DELETE FROM attributedescriptors
WHERE  QualifierInternal  = 'segments'
  AND  p_extensionname  = 'vikingcore'
  AND TypePkString IN (SELECT PK FROM composedtypes WHERE InternalCode = 'RelationDescriptor')
  AND EnclosingTypePK IN (select PK from composedtypes where InternalCode = 'LiferaftVariantProduct')
"""
statement.executeUpdate(delete)
conn.commit()
statement.close()
conn.close()
