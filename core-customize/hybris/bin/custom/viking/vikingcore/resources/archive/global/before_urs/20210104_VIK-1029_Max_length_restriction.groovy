package archive.global.before_urs

import de.hybris.platform.core.Registry;
conn = Registry.getCurrentTenant().getDataSource().getConnection();
stmt = conn.createStatement();
alterProductslpTableQuery = "ALTER TABLE productslp ALTER COLUMN p_metadescription nvarchar(155)";
println(" --- Start ALTER TABLE productslp ALTER COLUMN p_metadescription nvarchar(155) --- ")
stmt.executeUpdate(alterProductslpTableQuery)
println(" --- END ALTER TABLE productslp ALTER COLUMN p_metadescription nvarchar(155) --- ")