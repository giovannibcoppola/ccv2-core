package archive.global.after_urs

import org.apache.log4j.Logger;
import org.apache.commons.collections.CollectionUtils
def catalogVersionService = spring.getBean "catalogVersionService"

def productService = spring.getBean "productService"
def modelService = spring.getBean "modelService"

def PARTNER_PRODUCT_CATALOG = "partnerProductCatalog"
def DEFAULT_CATALOG = "Default"
def SAFETY_SHOP_CATALOG = "safetyshopProductCatalog"
//Catalogs
def defaultCV = catalogVersionService.getCatalogVersion(DEFAULT_CATALOG, "Staged")
def partnerProductCV = catalogVersionService.getCatalogVersion(PARTNER_PRODUCT_CATALOG, "Online")
def safetyshopCV = catalogVersionService.getCatalogVersion(SAFETY_SHOP_CATALOG, "Online")

def partnerProducts = productService.getAllProductsForCatalogVersion(partnerProductCV)
def defaulltProducts = productService.getAllProductsForCatalogVersion(defaultCV)
def safetyshopProducts = productService.getAllProductsForCatalogVersion(safetyshopCV)

def products = partnerProducts+defaulltProducts+safetyshopProducts

def batchList = products.collate 1000
println("total number of batches of 1000 products : ${batchList.size()}")

batchList.eachWithIndex {productSubList, i ->
    println("processing of 1000 products for the batch no ${i+1}")
    productSubList.each { product ->
      if (CollectionUtils.isNotEmpty(product.getStores(new Locale('en')))) {
        product.setStoreList(product.getStores(new Locale('en')))
      }
    }
    modelService.saveAll(productSubList)
    println("processed successfully")
}
