package archive.global.after_urs

import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils

def PARTNER_PRODUCT_CATALOG = "partnerProductCatalog"
def STAGED_CATALOG_VERSION = "Staged"
def DEFAULT_CATALOG = "Default"
def DEFAULT_IMAGES_FOLDER = "images"

def modelService = spring.getBean "modelService"
def catalogVersionService = spring.getBean "catalogVersionService"
def productService = spring.getBean "productService"
def mediaContainerService = spring.getBean "mediaContainerService"
def mediaService = spring.getBean "mediaService"


//Catalogs
def partnerProductCatalog = catalogVersionService.getCatalogVersion(PARTNER_PRODUCT_CATALOG, STAGED_CATALOG_VERSION)
def defaultCatalog = catalogVersionService.getCatalogVersion(DEFAULT_CATALOG, STAGED_CATALOG_VERSION)
def products = productService.getAllProductsForCatalogVersion(partnerProductCatalog)


String newPicture
String oldFileName
String extension
String fileName
String copyMediaCode
suffix = '_copy.'

println('+-- ' + products.size() + '  products found -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+')
println('+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+')
println('+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+')
System.out.format("%1s %85s %88s %21s %n", '| product       |', '  fileName |', '    new picture  |', '     updated  |')
println('+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+')

products.each {
    product ->
        if (product.picture != null && !StringUtils.isBlank(product.picture.code) && CollectionUtils.isNotEmpty(product.detail)) {
            println 'product : ' + product.code
            pictureProductCode = product.picture.code
            pictureDetails = product.detail
            fileName = StringUtils.substringAfterLast(pictureProductCode, "/")

            if (StringUtils.isBlank(fileName)) {
                fileName = StringUtils.substringBeforeLast(product.picture.realFileName, "_")
            }
            println ' fileName ::::::::::' + fileName
            System.out.format("%1s %85s", '| ' + product.pk + ' |', pictureProductCode + ' |')

            if (pictureDetails.size() == 1) { //1 details with same filename > Take details
                def detail = StringUtils.substringAfterLast(product.detail.iterator().next().code, "/")
                if (StringUtils.isEmpty(detail)) {
                    detail = StringUtils.substringBeforeLast(product.detail.iterator().next().realFileName, "_")
                }
                if (fileName == detail) {
                    newPicture = product.detail.iterator().next().code
                }
            } else if (pictureDetails.size() > 1) { // nothing
                def frequency = indexOfAll(fileName, pictureDetails).size()
                println 'frequency ' + frequency
                //Multiple details with the same filename > Take image
                if (frequency == 1) {
                    newPicture = indexOfAll(fileName, pictureDetails).get(0)
                }
                // Otherwise
                //Details with different filename > Take image
                //Multiple details, one with same filename > Take details with same filename
            }

            if (newPicture != null && !pictureProductCode.equals(newPicture)) {
                product.setPicture(mediaService.getMedia(partnerProductCatalog, newPicture))
                System.out.format("%90s %18s %n", newPicture + ' | ', true)
            } else {
                System.out.format("%90s %18s %n", pictureProductCode + ' | ', false)
            }
        }

        // ====================  Start Clone Others Media into Gallery Images  ====================
        println ' | ---- Start Copy others ' + product.others + ' Media into Gallery Images ' + product.galleryImages.qualifier + '  ---- '
        MediaContainerModel mediaContainer
        try {
            mediaContainer = mediaContainerService.getMediaContainerForQualifier(product.code + '-Others')
        } catch (final Exception ex) {
            mediaContainer = modelService.create(MediaContainerModel.class)
            mediaContainer.setQualifier(product.code + '-Others')
        }
        mediaContainer.setCatalogVersion(partnerProductCatalog)
        mediaContainer.setMedias(product.others)
        List<MediaContainerModel> mediaContainers = new ArrayList<>()
        mediaContainers.add(mediaContainer)
        mediaContainers.addAll(product.galleryImages)
        product.setGalleryImages(mediaContainers)
        println ' | ---- END Copy others ' + product.others + ' Media into Gallery Images ' + product.galleryImages.qualifier + '  ---- '
        // ====================  END Clone Others Media into Gallery Images       ====================
        // ====================  Start Clone picture into Default Catalog staged  ====================
        def copyPicture
        oldFileName = StringUtils.substringBeforeLast(product.picture.code, ".")
        extension = StringUtils.substringAfterLast(product.picture.code, ".")
        copyMediaCode = oldFileName + suffix + extension

        try {
            copyPicture = mediaService.getMedia(defaultCatalog, copyMediaCode)
        } catch (final Exception ex) {
            copyPicture = modelService.create(MediaModel.class)
            copyPicture.setCode(copyMediaCode)
            copyPicture.setCatalogVersion(defaultCatalog)
            copyPicture.setFolder(mediaService.getFolder(DEFAULT_IMAGES_FOLDER))
            modelService.save(copyPicture)
            mediaService.duplicateData(product.picture, copyPicture)
        }
        // ====================  END Clone picture into Default Catalog staged  ====================
        // ====================  Start Clone Gallery Images into Default Catalog staged  ====================
        println ' | ---- Start Gallery Images  ' + '' + ' into ' + DEFAULT_CATALOG + ' Catalog  ---- '
        List<MediaContainerModel> mediaContainers_ = new ArrayList<>()

        println 'product.galleryImages : ' + product.galleryImages.size()

        if (CollectionUtils.isNotEmpty(product.galleryImages)) {
            product.galleryImages.each {
                gallery ->
                    if (null != gallery) {
                        println ' | ---- Start Clone Qualifier ' + gallery.qualifier + ' into gallery Images on ' + DEFAULT_CATALOG + ' Catalog  ---- '
                        MediaContainerModel mediaContainer_
                        try {
                            mediaContainer = mediaContainerService.getMediaContainerForQualifier(gallery.qualifier)
                        } catch (final Exception ex) {
                            mediaContainer_ = modelService.create(MediaContainerModel.class)
                            mediaContainer_.setQualifier(gallery.qualifier)
                            mediaContainer_.setCatalogVersion(defaultCatalog)
                            modelService.save(mediaContainer_)
                        }

                        List<MediaModel> mediaList = new ArrayList<>()
                        gallery.medias.each {
                            media ->
                                println ' | ---- Start Clone medias ' + media.code + ' into gallery Images on ' + DEFAULT_CATALOG + ' Catalog  ---- '
                                def picture
                                oldFileName = StringUtils.substringBeforeLast(media.code, ".")
                                extension = StringUtils.substringAfterLast(media.code, ".")
                                copyMediaCode = oldFileName + suffix + extension
                                try {
                                    mediaService.getMedia(defaultCatalog, copyMediaCode)
                                } catch (final Exception ex) {
                                    picture = modelService.create(MediaModel.class)
                                    picture.setCode(copyMediaCode)
                                    picture.setCatalogVersion(defaultCatalog)
                                    picture.setFolder(mediaService.getFolder(DEFAULT_IMAGES_FOLDER))
                                    modelService.save(picture)
                                    mediaService.duplicateData(media, picture)
                                    mediaList.add(picture)
                                    mediaContainer_.setMedias(mediaList)
                                    modelService.save(mediaContainer_)
                                }
                                println ' | ---- END Clone medias ' + media.code + ' into gallery Images on ' + DEFAULT_CATALOG + ' Catalog  ---- '
                        }
                        println ' | ---- END Clone Qualifier ' + gallery.qualifier + ' into gallery Images on ' + DEFAULT_CATALOG + ' Catalog  ---- '
                        if (null != mediaContainer_) {
                            mediaContainers_.add(mediaContainer_)
                        }
                    }
            }

        }
        // ====================  END Clone Gallery Images into Default Catalog staged  ====================

        //Update the product infos
        modelService.save(product)
}

static <T> Map indexOfAll(T obj, List<T> list) {
    final Map indexList = new HashMap<>();
    for (int i = 0; i < list.size(); i++) {
        if (list.get(i).code.contains(obj)) {
            indexList.put(i, list.get(i).code);
        }
    }
    return indexList;
}


