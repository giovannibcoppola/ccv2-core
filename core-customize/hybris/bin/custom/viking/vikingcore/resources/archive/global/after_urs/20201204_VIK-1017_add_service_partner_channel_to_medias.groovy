package archive.global.after_urs

import de.hybris.platform.core.model.media.MediaContainerModel
import de.hybris.platform.core.model.media.MediaModel;
import java.util.HashSet;
import com.viking.core.enums.Channel;

def flexibleSearchService = spring.getBean("flexibleSearchService")
def modelService = spring.getBean("modelService");

def query = "select {m.pk} from {Media as m JOIN CatalogVersion as cv ON {m.catalogVersion} = {cv.pk} JOIN Catalog as cat ON {cat.pk} = {cv.catalog}} where {cat.id} = 'partnerProductCatalog'";

def searchResults = flexibleSearchService.search(query).getResult();
for(int i=0;i<searchResults.size();i++) {
    MediaModel media = searchResults.get(i);

    HashSet channels = new HashSet();
    channels.addAll(media.getChannels());

    if(!channels.contains(Channel.SERVICE_PORTAL)) {
        channels.add(Channel.SERVICE_PORTAL);
    }

    media.setChannels(channels);

   // modelService.save(media);
}
modelService.saveAll(searchResults);

def queryMediaContainers = "select {mc.pk} from {MediaContainer as mc JOIN CatalogVersion as cv ON {mc.catalogVersion} = {cv.pk} JOIN Catalog as cat ON {cat.pk} = {cv.catalog}} where {cat.id} = 'partnerProductCatalog'";

def searchResultsMediaContainers = flexibleSearchService.search(queryMediaContainers).getResult();
for(int i=0;i<searchResultsMediaContainers.size();i++) {
    MediaContainerModel mediaContainerModel = searchResultsMediaContainers.get(i);

    HashSet channelsMediaContainers = new HashSet();
    channelsMediaContainers.addAll(mediaContainerModel.getChannels());

    if(!channelsMediaContainers.contains(Channel.SERVICE_PORTAL)) {
        channelsMediaContainers.add(Channel.SERVICE_PORTAL);
    }

    mediaContainerModel.setChannels(channelsMediaContainers);

    //modelService.save(mediaContainerModel);
}

modelService.saveAll(searchResultsMediaContainers);