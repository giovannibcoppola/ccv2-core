import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Products {


    public static void main(String[] args) {


        HashMap<String,String> dataMap =new HashMap<>();

        try {
            BufferedReader in = new BufferedReader(new FileReader("c:\\viking\\Products.csv"));
            while(in.ready()) {
                String line = in.readLine();

                String[] data = line.split(",");

                if(dataMap.containsKey(data[0])) {
                    dataMap.put(data[0], dataMap.get(data[0]) + "," + data[1]);
                } else {
                    dataMap.put(data[0],data[1]);
                }

            }
            PrintWriter out = new PrintWriter(new FileWriter("c:\\viking\\output1.impex"));
            PrintWriter out1 = new PrintWriter(new FileWriter("c:\\viking\\output2.impex"));


            for(Map.Entry<String,String> entry : dataMap.entrySet()) {
                out.println(";;" + entry.getKey() + ";" + entry.getValue());
                out1.println(";;"+entry.getKey()+";Product "+entry.getKey()+";Description for product "+entry.getKey());
            }


            in.close();
            out.close();
            out1.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }




    }
}
