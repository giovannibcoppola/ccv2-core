package com.viking.core.service.impl;

import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;

/**
 * The type Default viking enquiry receiver email service.
 */
public class DefaultVikingEnquiryReceiverEmailService implements VikingEnquiryReceiverEmailService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingEnquiryReceiverEmailService.class);
    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Override
    public EnquiryReceiverEmailModel findEnquiryNotificationInfo(final OrderModel enquiry) {
        EnquiryReceiverEmailModel eReceiver = new EnquiryReceiverEmailModel();
        String isoCode = enquiry.getDeliveryAddress().getCountry().getIsocode();
        eReceiver.setRegionIsoCode(isoCode);
        try {
            final EnquiryReceiverEmailModel receiver = flexibleSearchService.getModelByExample(eReceiver);
            eReceiver.setReceiverEmail(receiver.getReceiverEmail());
            eReceiver.setSalesOrg(receiver.getSalesOrg());
            eReceiver.setReceiverEmail(receiver.getReceiverEmail());
            eReceiver.setCustomer(receiver.getCustomer());
            eReceiver.setPlant(receiver.getPlant());
        } catch (ModelNotFoundException | AmbiguousIdentifierException ex) {
            LOG.debug(MessageFormat.format("No EnquiryReceiverEmailModel found for region code: {0}", isoCode));
        }
        return eReceiver;
    }
}
