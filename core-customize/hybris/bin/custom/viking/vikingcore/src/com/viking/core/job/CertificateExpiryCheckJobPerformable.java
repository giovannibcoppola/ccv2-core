package com.viking.core.job;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fest.util.Files;
import org.springframework.beans.factory.annotation.Required;

import com.viking.core.model.CertificateExpiryCheckCronJobModel;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

public class CertificateExpiryCheckJobPerformable extends AbstractJobPerformable<CertificateExpiryCheckCronJobModel> {

    private static final Logger LOG = LogManager.getLogger(CertificateExpiryCheckJobPerformable.class);
    public static final String BODY = "See Excel file attached";

    private static String GET_PRODUCTS_THAT_HAVE_CERTIFICATES_THAT_EXPIRE_IN_THREE_MONTHS = "SELECT DISTINCT {P.PK}\n" +
            "FROM {Product AS P\n" +
            "JOIN ProductToCertificates AS PC ON {P.PK}={PC.source}\n" +
            "JOIN Media AS M ON {PC.target}={M.PK}\n" +
            "JOIN CatalogVersion AS CV ON {P.catalogVersion}={CV.PK}\n" +
            "JOIN Catalog AS C ON {C.PK}={CV.catalog}\n" +
            "}\n" +
            "WHERE {M.expiryDate} IS NOT NULL\n" +
            "AND (\n" +
            "    MONTH({M.expiryDate}) = MONTH(getdate()) AND YEAR({M.expiryDate}) = YEAR(getdate())\n" +
            "    OR\n" +
            "    MONTH({M.expiryDate}) = MONTH(DATEADD(month, 1, getdate())) AND YEAR({M.expiryDate}) = YEAR(DATEADD(month, 1, getdate()))\n" +
            "    OR\n" +
            "    MONTH({M.expiryDate}) = MONTH(DATEADD(month, 2, getdate())) AND YEAR({M.expiryDate}) = YEAR(DATEADD(month, 2, getdate()))\n" +
            "    )\n" +
            "AND {C.id} = 'Default'\n" +
            "AND {CV.version} = 'Staged'";

    private EmailService emailService;

    @Override
    public PerformResult perform(final CertificateExpiryCheckCronJobModel certificateExpiryCheckCronJobModel)
    {
        LOG.info("Starting Expiry Check Cronjob");
        //Get the Products that have certificates that are about to expire the coming three month
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(GET_PRODUCTS_THAT_HAVE_CERTIFICATES_THAT_EXPIRE_IN_THREE_MONTHS);
        final List<ProductModel> productsWithSoonToBeExpiredCertificate = flexibleSearchService.<ProductModel>search(flexibleSearchQuery).getResult();

        LOG.info("1. Found {} products with certificates that will expire this month.", productsWithSoonToBeExpiredCertificate.size());

        //Build the Excel file
        final Month currentMonth = LocalDate.now().getMonth();
        final Month nextMonth = currentMonth.plus(1);
        final Month inTwoMonths = nextMonth.plus(1);

        final String monthsString = currentMonth.getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + nextMonth.getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + inTwoMonths.getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
        final String filename = StringUtils.lowerCase("product_certificates_expire_" + monthsString + ".xlsx");

        try (XSSFWorkbook workbook = new XSSFWorkbook();
             FileOutputStream outputStream = new FileOutputStream(filename)) {
            // Finds the workbook instance for XLSX file

            // Add sheet to the XLSX workbook
            XSSFSheet sheet = workbook.createSheet();
            LOG.info("Creating excel: {}", filename);

            Row header = sheet.createRow(0);
            Cell headerCell = header.createCell(0);
            headerCell.setCellValue((String) "Material Number");
            headerCell = header.createCell(1);
            headerCell.setCellValue((String) "Name");
            headerCell = header.createCell(2);
            headerCell.setCellValue((String) "Certificate Code");
            headerCell = header.createCell(3);
            headerCell.setCellValue((String) "Alternative Name");
            headerCell = header.createCell(4);
            headerCell.setCellValue((String) "Expiry Date");

            SimpleDateFormat ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy");


            int rowNum = 1;
            for (ProductModel productModel : productsWithSoonToBeExpiredCertificate) {
                Row row = sheet.createRow(rowNum++);
                for (MediaModel certificate : productModel.getCertificates()) {

                    if(certificate.getExpiryDate() != null && certificateExpiresInThreeMonths(certificate.getExpiryDate())) {
                        final String dateString = ddmmyyyy.format(certificate.getExpiryDate());
                        LOG.info("Certificate {} expires in the coming three months on {}.", certificate.getCode(), dateString);
                        Cell cell = row.createCell(0);
                        cell.setCellValue((String) productModel.getCode());
                        cell = row.createCell(1);
                        cell.setCellValue((String) productModel.getName());
                        cell = row.createCell(2);
                        cell.setCellValue((String) certificate.getCode());
                        cell = row.createCell(3);
                        cell.setCellValue((String) certificate.getAltTextLocalized());
                        cell = row.createCell(4);
                        cell.setCellValue((String) ddmmyyyy.format(certificate.getExpiryDate()));
                    } else if (certificate.getExpiryDate() == null) {
                        LOG.info("Certificate {} doesn't have expiry date.", certificate.getCode());
                    } else {
                        final String dateString = ddmmyyyy.format(certificate.getExpiryDate());
                        LOG.info("Certificate {} doesn't expire in three months, but on {}.", certificate.getCode(), dateString);
                    }
                }
            }

            workbook.write(outputStream);

        } catch (IOException e) {
            LOG.error("Error while generating Excel file: ", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }

        LOG.info("2. Built Excel file called '{}'.", filename);

        //Build an email message and add the attachment to it
        final String fullMonthsString = currentMonth.getDisplayName(TextStyle.FULL, Locale.ENGLISH) + "/" + nextMonth.getDisplayName(TextStyle.FULL, Locale.ENGLISH) + "/" + inTwoMonths.getDisplayName(TextStyle.FULL, Locale.ENGLISH);

        final EmailAttachmentModel emailAttachment;
        try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename))) {
            emailAttachment = emailService.createEmailAttachment(dataInputStream, filename, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            final String subject = "Products with certificates that are to expire in " + fullMonthsString;
            final EmailAddressModel fromEmail = emailService.getOrCreateEmailAddressForEmail("noreply@viking-life.com", "noreply");
            final EmailAddressModel toEmail = emailService.getOrCreateEmailAddressForEmail(certificateExpiryCheckCronJobModel.getReceiverEmail(), "Viking E-Commerce");

            final EmailMessageModel emailMessage = emailService.createEmailMessage(Collections.singletonList(toEmail), null, null, fromEmail, null, subject, BODY, Collections.singletonList(emailAttachment));

            emailService.send(emailMessage);
            LOG.info("3. Sent email to '{}'.", toEmail.getEmailAddress());
        } catch (FileNotFoundException e) {
            LOG.error("File not found! ", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        } catch (IOException e) {
            LOG.error("Error while creating attachment", e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }

        File myObj = new File(filename);
        Files.delete(myObj);

        LOG.info("4. Deleted file '{}'.", filename);

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private boolean certificateExpiresInThreeMonths(Date expiryDate) {
        final LocalDate currentDate = LocalDate.now();
        final LocalDate nextMonthDate = currentDate.plusMonths(1);
        final LocalDate inTwoMonthsDate = nextMonthDate.plusMonths(1);

        final Month currentMonth = currentDate.getMonth();
        final Month nextMonth = nextMonthDate.getMonth();
        final Month inTwoMonths = inTwoMonthsDate.getMonth();

        final int currentMonthYear = currentDate.getYear();
        final int nextMonthYear = nextMonthDate.getYear();
        final int inTwoMonthsYear = inTwoMonthsDate.getYear();

        final LocalDate expiry = LocalDate.ofInstant(expiryDate.toInstant(), ZoneId.of("CET"));
        return (expiry.getMonth() == currentMonth && expiry.getYear() == currentMonthYear) ||
               (expiry.getMonth() == nextMonth && expiry.getYear() == nextMonthYear) ||
               (expiry.getMonth() == inTwoMonths && expiry.getYear() == inTwoMonthsYear);
    }

    public EmailService getEmailService() {
        return emailService;
    }

    @Required
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
}
