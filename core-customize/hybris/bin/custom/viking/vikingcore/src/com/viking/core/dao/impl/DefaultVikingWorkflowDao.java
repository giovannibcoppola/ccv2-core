package com.viking.core.dao.impl;

import com.viking.core.dao.VikingWorkflowDao;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * class implement Viking Workflow Dao
 */
public class DefaultVikingWorkflowDao extends AbstractItemDao implements VikingWorkflowDao {

    private static final String GET_ALL_WORKFLOWS_ACTION_QUERY = "SELECT {wa.pk} FROM {Workflow AS w JOIN WorkflowAction AS wa ON {w.pk} = {wa.workflow} " +
                                        "JOIN WorkflowActionStatus AS was ON {wa.status} = {was.pk} " +
                                        "JOIN WorkflowItemAttachment AS wia ON {wia:workflow}={w:PK} } " +
                                        "WHERE {w.name}= ?name AND ( {was.code} = 'in_progress' OR {was.code} = 'pending' ) " +
                                        "AND {wia:item}= ?item";
    @Override
    public List<WorkflowActionModel> findAllWorkflowActionInProgressForProduct(String name, ItemModel item) {
        final Map<String, Object> queryParameters = new HashMap<String, Object>();
        queryParameters.put("name", name);
        queryParameters.put("item", item.getPk().toString());
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_WORKFLOWS_ACTION_QUERY, queryParameters);
        final SearchResult<WorkflowActionModel> searchRes = search(query);
        return searchRes.getResult();
    }
}
