package com.viking.core.service;

import com.viking.core.jalo.NotificationPrefill;

import java.util.List;

public interface VikingNotificationPrefillService {
    List<NotificationPrefill> getNotifications(String notificationId);
}
