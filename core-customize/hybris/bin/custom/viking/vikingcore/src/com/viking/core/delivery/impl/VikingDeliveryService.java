package com.viking.core.delivery.impl;

import com.viking.core.util.DeliveryCostHelper;
import de.hybris.platform.commerceservices.delivery.impl.DefaultDeliveryService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.util.PriceValue;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class VikingDeliveryService extends DefaultDeliveryService {

    private static final Logger LOG = Logger.getLogger(VikingDeliveryService.class);

    private DeliveryCostHelper deliveryCostHelper;

    @Override
    public PriceValue getDeliveryCostForDeliveryModeAndAbstractOrder(final DeliveryModeModel deliveryMode,
                                                                     final AbstractOrderModel abstractOrder) {
        validateParameterNotNull(deliveryMode, "deliveryMode model cannot be null");
        validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");

        final List<ZoneDeliveryModeValueModel> values = deliveryCostHelper.findZoneDeliveryModeValuesForOrder(abstractOrder, deliveryMode);

        if (CollectionUtils.isNotEmpty(values) && values.get(0).getPercentage() != null) {
            LOG.debug("Using percentage to calculate delivery cost. Percentage is: " + values.get(0).getPercentage());
            return deliveryCostHelper.calculateDeliveryCostForPercentage(abstractOrder, values.get(0), deliveryMode);
        } else {
            return super.getDeliveryCostForDeliveryModeAndAbstractOrder(deliveryMode, abstractOrder);
        }
    }

    @Required
    public void setDeliveryCostHelper(DeliveryCostHelper deliveryCostHelper) {
        this.deliveryCostHelper = deliveryCostHelper;
    }
}
