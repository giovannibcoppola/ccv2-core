package com.viking.core.service.impl;


import de.hybris.platform.impex.model.cronjob.ImpExImportCronJobModel;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.impl.DefaultImportService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VikingImportService extends DefaultImportService {

    private static final Logger LOG = LoggerFactory.getLogger(VikingImportService.class);

    @Override
    public ImportResult importData(ImpExResource script) {
        LOG.info("Using Viking custom import - importData ImpExResource");
        ImportConfig config = new ImportConfig();
        config.setScript(script);
        config.setRemoveOnSuccess(false);
        return this.importData(config);
    }

    @Override
    public ImportResult importData(ImportConfig config) {
        LOG.info("Using Viking custom import - importData importConfig");
        config.setRemoveOnSuccess(Boolean.FALSE);
        return super.importData(config);
    }

    @Override
    protected void importData(ImpExImportCronJobModel cronJob, boolean synchronous, boolean removeOnSuccess) {
        LOG.info("Using Viking custom import - importData ImpExImportCronJobModel");
        boolean removeOnSuccessOverride = false;
        super.importData(cronJob, synchronous, removeOnSuccessOverride);
    }
}
