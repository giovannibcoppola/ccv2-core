package com.viking.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

public class VikingEnquiryNotificationSalesEvent extends OrderProcessingEvent {

    public VikingEnquiryNotificationSalesEvent(OrderProcessModel process) {
        super(process);
    }
}
