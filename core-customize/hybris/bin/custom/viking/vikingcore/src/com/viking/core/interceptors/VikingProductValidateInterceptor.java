package com.viking.core.interceptors;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;


/**
 * Prepare interceptor for {@link ProductModel}
 *
 * @author amine.elharrak
 */
public class VikingProductValidateInterceptor implements ValidateInterceptor<ProductModel> {

    public static final String VIKING_CLASSIFICATION = "VikingClassification";
    public static final String ERP_CLASSIFICATION = "ERPClassification";

    @Override
    public void onValidate(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        final String catalogVersion = productModel.getCatalogVersion().getVersion();
        final String catalogId = productModel.getCatalogVersion().getCatalog().getId();

        if (!isValidCategory(catalogVersion, catalogId, productModel)) {
            throw new InterceptorException("The SuperCategory should be existing on the same catalog as the product ");
        }
    }

    private boolean isValidCategory(String catalogVersion, String catalogId, ProductModel productModel) {
        final Collection<CategoryModel> categories =  productModel.getSupercategories();
        if (CollectionUtils.isNotEmpty(categories)) {
            for (CategoryModel category : categories) {
                if (VIKING_CLASSIFICATION.equals(category.getCatalogVersion().getCatalog().getId())
                        || ERP_CLASSIFICATION.equals(category.getCatalogVersion().getCatalog().getId())) {
                    continue;
                }
                if (!(catalogId.equals(category.getCatalogVersion().getCatalog().getId())
                        && catalogVersion.equals(category.getCatalogVersion().getVersion()))) {
                    return false;
                }
            }
        }
        return true;
    }

}