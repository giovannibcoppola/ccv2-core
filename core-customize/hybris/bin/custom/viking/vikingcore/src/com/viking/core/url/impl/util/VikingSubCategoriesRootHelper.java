package com.viking.core.url.impl.util;

import de.hybris.platform.category.model.CategoryModel;
import org.apache.commons.lang3.BooleanUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VikingSubCategoriesRootHelper {

    private VikingSubCategoriesRootHelper(){

    }

    /**
     * get categories without root category
     *
     * @param paths
     * @return List<CategoryModel>
     */
    public static List<CategoryModel> getSubCategoriesOfRoot(Collection<List<CategoryModel>> paths) {
        List<CategoryModel> categoryModelList = paths.iterator().next();
        List<CategoryModel> categoryModelListReturn = new ArrayList<>();
        categoryModelList.stream().forEach(categoryModel -> {
            if(BooleanUtils.isNotTrue(categoryModel.isRootCategory())) {
                categoryModelListReturn.add(categoryModel);
            }
        });
        return categoryModelListReturn;
    }
}
