package com.viking.core.service;

import com.viking.core.model.ApprovedFacilityModel;
import com.viking.facades.approvedfacility.data.ApprovedFacilityData;

import java.util.Optional;

public interface VikingApprovedFacilityService {
    ApprovedFacilityModel getApprovedFacility();

    ApprovedFacilityModel setApprovedFacility(ApprovedFacilityData approvedFacility);
}
