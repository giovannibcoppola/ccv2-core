package com.viking.core.service;

import com.viking.core.data.VikingTechnicianData;
import com.viking.core.data.VikingTotalCreditData;

import java.util.List;

/**
 * Interface to handle station
 */
public interface VikingStationService {

    /**
     * Get technicans overview data for specified station.
     * @param stationId
     * @return
     */
    List<VikingTechnicianData> getTechnicians(final String stationId);

    List<VikingTotalCreditData>getCredits(final String stationId);
}
