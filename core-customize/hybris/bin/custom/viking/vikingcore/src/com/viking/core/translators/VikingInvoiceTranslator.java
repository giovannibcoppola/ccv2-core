package com.viking.core.translators;

import com.viking.core.model.VikingInvoiceModel;
import com.viking.core.service.VikingInvoiceService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Date;

public class VikingInvoiceTranslator extends AbstractSpecialValueTranslator {

    private static final String INVOICE_SERVICE = "vikingInvoiceService";
    private static final String MODEL_SERVICE = "modelService";
    private static final String FLEXIBLE_SEARCH_SERVICE = "flexibleSearchService";

    private VikingInvoiceService vikingInvoiceService;
    private ModelService modelService;
    private FlexibleSearchService flexibleSearchService;

    @Override
    public void performImport(final String value, final Item processedItem) throws ImpExException {
        try {
            final String billingDocumentId = processedItem.getAttribute("documentNumber").toString();
            final String orderNumber = processedItem.getAttribute("orderNumber").toString();
            final Date invoiceDate = (Date) processedItem.getAttribute("date");
            
            final OrderModel order = findOrder(orderNumber);

            boolean isInvoiceExisting = order.getInvoices().stream().anyMatch(invoice -> invoice.getId().equals(billingDocumentId));

            if (isInvoiceExisting) {
                return;
            }

            final VikingInvoiceModel invoice = getModelService().create(VikingInvoiceModel.class);
            invoice.setId(billingDocumentId);
            invoice.setErporder(order);
            invoice.setDateIssued(invoiceDate);
            getModelService().save(invoice);
        } catch (JaloSecurityException e) {
            throw new ImpExException(e);
        }
    }

    private OrderModel findOrder(String orderNumber) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE + "} " +
                "WHERE {" + OrderModel.VIKINGERPORDERID + "} = ?" + OrderModel.VIKINGERPORDERID + " " +
                "AND {" + OrderModel.VERSIONID +"} IS NULL");

        query.addQueryParameter(OrderModel.VIKINGERPORDERID, orderNumber);

        return (OrderModel) getFlexibleSearchService().search(query).getResult().get(0);
    }

    private VikingInvoiceService getVikingInvoiceService() {
        if (vikingInvoiceService == null) {
            vikingInvoiceService = (VikingInvoiceService) Registry.getApplicationContext().getBean(INVOICE_SERVICE);
        }

        return vikingInvoiceService;
    }

    private ModelService getModelService() {
        if (modelService == null) {
            modelService = (ModelService) Registry.getApplicationContext().getBean(MODEL_SERVICE);
        }

        return modelService;
    }

    public FlexibleSearchService getFlexibleSearchService() {
        if (flexibleSearchService == null) {
            flexibleSearchService = (FlexibleSearchService) Registry.getApplicationContext().getBean(FLEXIBLE_SEARCH_SERVICE);
        }
        return flexibleSearchService;
    }
}
