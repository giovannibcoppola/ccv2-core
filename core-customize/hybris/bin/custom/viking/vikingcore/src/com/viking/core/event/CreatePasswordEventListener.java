package com.viking.core.event;

import com.viking.core.model.CreatePasswordProcessModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.springframework.beans.factory.annotation.Required;


/**
 * Listener for "create password" functionality event.
 */
public class CreatePasswordEventListener extends AbstractAcceleratorSiteEventListener<CreatePasswordEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    protected BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Override
    protected void onSiteEvent(final CreatePasswordEvent event) {
        final CreatePasswordProcessModel createPasswordEmailProcess = getBusinessProcessService()
                .createProcess("CreatePassword-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
                        "createPasswordEmailProcess");
        createPasswordEmailProcess.setSite(event.getSite());
        createPasswordEmailProcess.setCustomer(event.getCustomer());
        createPasswordEmailProcess.setToken(event.getToken());
        createPasswordEmailProcess.setLanguage(event.getLanguage());
        createPasswordEmailProcess.setCurrency(event.getCurrency());
        createPasswordEmailProcess.setStore(event.getBaseStore());
        createPasswordEmailProcess.setDefaultB2BUnitId(event.getDefaultB2BUnitId());
        getModelService().save(createPasswordEmailProcess);
        getBusinessProcessService().startProcess(createPasswordEmailProcess);
    }

    @Override
    protected SiteChannel getSiteChannelForEvent(final CreatePasswordEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }
}
