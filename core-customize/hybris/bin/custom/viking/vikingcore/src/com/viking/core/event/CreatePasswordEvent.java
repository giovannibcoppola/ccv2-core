package com.viking.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class CreatePasswordEvent extends AbstractCommerceUserEvent<BaseSiteModel> {
    private String token;
    private String defaultB2BUnitId;

    /**
     * Default constructor
     */
    public CreatePasswordEvent() {
        super();
    }

    /**
     * Parameterized Constructor
     *
     * @param token
     */
    public CreatePasswordEvent(final String token) {
        super();
        this.token = token;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    public String getDefaultB2BUnitId() {
        return defaultB2BUnitId;
    }

    public void setDefaultB2BUnitId(String defaultB2BUnitId) {
        this.defaultB2BUnitId = defaultB2BUnitId;
    }
}
