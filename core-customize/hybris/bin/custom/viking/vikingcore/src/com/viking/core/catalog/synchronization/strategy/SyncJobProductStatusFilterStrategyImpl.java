package com.viking.core.catalog.synchronization.strategy;

import com.viking.core.model.ProductStatusRestrictedCatalogVersionSyncJobModel;
import com.viking.core.catalog.synchronization.VikingCatalogSynchronizationService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SyncJobProductStatusFilterStrategyImpl implements SyncJobApplicableItemStrategy {

    private static final Logger LOG = Logger.getLogger(VikingCatalogSynchronizationService.class);

    private Set<ArticleApprovalStatus> getArticleApprovalStatesToExclude(final ProductStatusRestrictedCatalogVersionSyncJobModel syncItemJob) {
        final ProductStatusRestrictedCatalogVersionSyncJobModel productStatusRestrictedCatalogVersionSyncJobModel = syncItemJob;
        return productStatusRestrictedCatalogVersionSyncJobModel.getExcludedApprovalStates();
    }

    @Override
    public List<ItemModel> filterItems(final List<ItemModel> inputItems, final SyncItemJobModel syncItemJob) {
        if (syncItemJob instanceof ProductStatusRestrictedCatalogVersionSyncJobModel) {
            final Set<ArticleApprovalStatus> excludedApprovalStates = getArticleApprovalStatesToExclude((ProductStatusRestrictedCatalogVersionSyncJobModel) syncItemJob);

            // Filter the products on ArticleApprovalState before continuing
            final List<ProductModel> productsToSkip = CollectionUtils.emptyIfNull(inputItems).stream()
                    .filter(ProductModel.class::isInstance)
                    .map(ProductModel.class::cast)
                    .filter(p -> excludedApprovalStates.contains(p.getApprovalStatus()))
                    .collect(Collectors.toUnmodifiableList());

            if (CollectionUtils.isNotEmpty(productsToSkip)) {
                if (LOG.isInfoEnabled()) {
                    final String excludedApprovalCodes = excludedApprovalStates.stream()
                            .map(ArticleApprovalStatus::getCode)
                            .collect(Collectors.joining(","));
                    final String excludedProductCodes = productsToSkip.stream()
                            .map(ProductModel::getCode)
                            .collect(Collectors.joining(","));
                    LOG.info("Skip products with ApprovalState: " + excludedApprovalCodes + ". The following product are skipped " + excludedProductCodes);
                }
                return ListUtils.subtract(inputItems, productsToSkip);
            } else {
                LOG.debug("No products found to skip");
            }
        }
        return inputItems;
    }
}
