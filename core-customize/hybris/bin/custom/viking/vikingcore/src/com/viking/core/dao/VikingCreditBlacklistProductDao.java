package com.viking.core.dao;

import com.viking.core.model.CreditBlacklistProductModel;

import java.util.List;

/**
 * DAO for blacklisted products for credit page
 */
public interface VikingCreditBlacklistProductDao {

    /**
     * Get all products blacklisted for credit page
     * @return a list of blacklisted product codes
     */
    List<String> getAllCreditBlacklistProductCodes();
}
