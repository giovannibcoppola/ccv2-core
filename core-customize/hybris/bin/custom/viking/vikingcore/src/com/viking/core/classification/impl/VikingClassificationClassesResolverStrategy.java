package com.viking.core.classification.impl;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.impl.DefaultClassificationClassesResolverStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class VikingClassificationClassesResolverStrategy extends DefaultClassificationClassesResolverStrategy {

    private static final  Logger LOG = Logger.getLogger(VikingClassificationClassesResolverStrategy.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private CatalogVersionService catalogVersionService;


    @Override
    public Set<ClassificationClassModel> resolve(final ProductModel item)
    {
        final Set<ClassificationClassModel> classes =  new HashSet<>(super.resolve(item));
        try {
            final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME);
            final ProductModel productModel = productService.getProduct(catalogVersionModel, item.getCode());
            final Set<ClassificationClassModel> masterClasses = super.resolve(productModel);
            classes.addAll(masterClasses);
        } catch(Exception ex) {
            if(LOG.isDebugEnabled()) {
                LOG.debug("No product found in DefaultCatalog for productcode: " + item.getCode());
            }
        }
        return classes;
    }

}
