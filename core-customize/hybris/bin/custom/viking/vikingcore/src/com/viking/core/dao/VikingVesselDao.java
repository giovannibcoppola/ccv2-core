/**
 *
 */
package com.viking.core.dao;

import java.util.List;

import com.viking.core.model.VikingVesselModel;


/**
 * @author Prabhakar
 *
 */
public interface VikingVesselDao
{
	List<VikingVesselModel> getAssignedVesselsByUserId(String userId);
}
