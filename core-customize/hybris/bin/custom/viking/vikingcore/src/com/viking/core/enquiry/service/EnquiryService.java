package com.viking.core.enquiry.service;

import com.viking.core.jalo.VikingEnquiryOrder;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;

/**
 * Enquiry Service
 */
public interface EnquiryService {

    /**
     * Check if enquiry flow is enabled for current basestore
     * @return
     */
    boolean isEnquiryFlow();

    /**
     * Creates Enquiry From Cart
     * @param cart
     * @return
     * @throws InvalidCartException
     */
    VikingEnquiryOrderModel createEnquiryFromCart(CartModel cart) throws InvalidCartException;

    /**
     * Is Enquiry Order
     * @param orderModel
     * @return
     */
    public boolean isEnquiryOrder(final OrderModel orderModel);
}
