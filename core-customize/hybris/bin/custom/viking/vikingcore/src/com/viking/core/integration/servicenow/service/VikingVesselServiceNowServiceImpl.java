/**
 *
 */
package com.viking.core.integration.servicenow.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.sap.conn.jco.JCoFunction;
import com.viking.core.dao.VikingVesselDao;
import com.viking.core.integration.servicenow.data.RawJsonVesselEquipmentResponseData;
import com.viking.core.integration.servicenow.data.RawJsonVesselsResponseData;
import com.viking.core.integration.servicenow.data.VesselData;
import com.viking.core.integration.servicenow.data.VesselEquipmentData;
import com.viking.core.integration.servicenow.data.VesselEquipmentDataWrapper;
import com.viking.core.model.VikingVesselModel;
import com.viking.core.util.ContentHelper;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Prabhakar
 *
 */
public class VikingVesselServiceNowServiceImpl implements VikingVesselServiceNowService {

    public static final String ACCEPT = "accept";
    private static final Logger LOG = LoggerFactory.getLogger(VikingVesselServiceNowServiceImpl.class);

    private static final String VESSEL_DATA_GET_REQUEST_URL = "servicenow.clinet.customer.vesseldata.url";
    private static final String VESSEL_EQUIPMENT_GET_REQUEST_URL = "servicenow.clinet.customer.vesseldetaildata.url";
    private static final String USERNAME = "servicenow.client.username";
    private static final String PASSWORD = "servicenow.client.password";
    private static final String ZGET_LAST_NOTIF_CERT_EQUI_HYB3 = "ZGET_LAST_NOTIF_CERT_EQUI_HYB3";
    public static final String EX_URL = "EX_URL";
    public static final String I_QMNUM = "I_QMNUM";
    public static final String I_EQUNR = "I_EQUNR";
    public static final String J_CO_STATELESS = "JCoStateless";
    public static final String HYBRIS = "Hybris";
    public static final int SIZE_NOTIFICATION_NUMBER = 12;
    public static final String PAD_STR = "0";
    public static final String CPC = "CPC";
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BASIC = "Basic ";
    public static final String SEPARATOR_AUTH = ":";

    private ConfigurationService configurationService;
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;
    private VikingVesselDao vikingVesselDao;
    private ContentHelper contentHelper;

    @Override
    public List<VesselData> getAllVesselDataList(final String customerId) throws IOException {
        final HttpResponse response = getHttpResponse(configurationService.getConfiguration().getString(VESSEL_DATA_GET_REQUEST_URL) + customerId);

        final ObjectMapper objectMapper = getObjectMapper();
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        final String content = IOUtils.toString(response.getEntity().getContent());
        /*
        if(LOG.isDebugEnabled()) {
            LOG.debug(content);
        }
        */
        final RawJsonVesselsResponseData responseData = objectMapper.readValue(IOUtils.toInputStream(content), RawJsonVesselsResponseData.class);
        //return responseData.getResult().getVessels();
        return getFilteredVesselData(responseData.getResult().getVessels());
    }


    private List<VesselData> getFilteredVesselData(List<VesselData> result) {
        return  CollectionUtils.isNotEmpty(result)?
                result
                        .stream()
                        .filter(VesselData::isPartOfContract)
                        .collect(Collectors.toList())
                : Collections.emptyList();
    }


    @Override
    public VesselEquipmentDataWrapper getVesselEquipmentData(final String imoId, final String customerId) throws IOException {
        final String detailsUrl = configurationService.getConfiguration().getString(VESSEL_EQUIPMENT_GET_REQUEST_URL);

        final HttpResponse httpResponse = getHttpResponse(MessageFormat.format(detailsUrl, imoId));

        final ObjectMapper objectMapper = getObjectMapper();
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        final String content = IOUtils.toString(httpResponse.getEntity().getContent());

        final RawJsonVesselEquipmentResponseData vesselDetailsResponse = objectMapper.readValue(IOUtils.toInputStream(content),
                RawJsonVesselEquipmentResponseData.class);

        final VesselEquipmentDataWrapper wrapper = new VesselEquipmentDataWrapper();

        final List<VesselEquipmentData> datas = getFilteredVesselEquipmentData(vesselDetailsResponse.getResult().getEquipments());
        //final List<VesselEquipmentData> datas = vesselDetailsResponse.getResult().getEquipments();

        try {
            final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection(J_CO_STATELESS, HYBRIS);

            final Map<String, String> contentUrlMap = new HashMap<>();

            for (VesselEquipmentData data : datas) {
                final JCoFunction function = managedConnection.getFunction(ZGET_LAST_NOTIF_CERT_EQUI_HYB3);
                final String lastNotificationNumber = data.getLastNotificationNumber();
                final String equipmentNumber = data.getEquipmentNumber();

                if (StringUtils.isNotEmpty(lastNotificationNumber)) {
                    final String _12_digit_lastNotificationNumber  = StringUtils.leftPad(lastNotificationNumber, SIZE_NOTIFICATION_NUMBER, PAD_STR);

                    function.getImportParameterList().setValue(I_QMNUM, _12_digit_lastNotificationNumber);
                    function.getImportParameterList().setValue(I_EQUNR, equipmentNumber);

                    managedConnection.execute(function);

                    final String contentUrl = function.getExportParameterList().getString(EX_URL);

                    //contentUrl is *not* unique and causes the location and locationhash to change each time it's repeated, invalidating previous Media that also have this contentUrl in this loop
                    if (StringUtils.isNotBlank(contentUrl)) {
                        if(!contentUrlMap.containsKey(lastNotificationNumber)) {
                            final Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia(CPC + lastNotificationNumber, contentUrl);
                            if (mediaOptional.isPresent()) {
                                final String url = mediaOptional.get().getURL();
                                data.setLastCertificate(url);
                                contentUrlMap.put(lastNotificationNumber, url);
                            }
                        } else {
                            data.setLastCertificate(contentUrlMap.get(lastNotificationNumber));
                        }
                    }
                }
            }

            vesselDetailsResponse.getResult().setEquipments(datas);

        } catch (BackendException e) {
            LOG.info("Error during sap erp rfc call (VikingVesselServiceNowServiceImpl) for module : " + ZGET_LAST_NOTIF_CERT_EQUI_HYB3, e);
        }

        wrapper.setImo(vesselDetailsResponse.getResult().getImo());
        wrapper.setFlagState(vesselDetailsResponse.getResult().getFlagState());
        wrapper.setVesselName(vesselDetailsResponse.getResult().getVesselName());

        wrapper.setEquipmentDataList(vesselDetailsResponse.getResult().getEquipments());
        return wrapper;
    }


    private List<VesselEquipmentData> getFilteredVesselEquipmentData(List<VesselEquipmentData> result){
        return  CollectionUtils.isNotEmpty(result)?
                result
                        .stream()
                        .filter(VesselEquipmentData::isPartOfContract)
                        .collect(Collectors.toList())
                : Collections.emptyList();
    }


    private ObjectMapper getObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    private HttpResponse getHttpResponse(String url) throws IOException {
        final HttpClient client = HttpClientBuilder.create().build();
        final String authStr = configurationService.getConfiguration().getString(USERNAME) + SEPARATOR_AUTH
                + configurationService.getConfiguration().getString(PASSWORD);
        final String encoding = Base64.getEncoder().encodeToString(authStr.getBytes());


        final HttpGet request = new HttpGet(url);
        // add request header
        request.addHeader(ACCEPT, APPLICATION_JSON);
        request.addHeader(AUTHORIZATION, BASIC + encoding);

        return client.execute(request);
    }


    @Override
    public List<VikingVesselModel> getAssignedVesselById(final String userId) {
        return vikingVesselDao.getAssignedVesselsByUserId(userId);
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Required
    public void setVikingVesselDao(final VikingVesselDao vikingVesselDao) {
        this.vikingVesselDao = vikingVesselDao;
    }

    @Required
    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    @Required
    public void setContentHelper(ContentHelper contentHelper) {
        this.contentHelper = contentHelper;
    }
}
