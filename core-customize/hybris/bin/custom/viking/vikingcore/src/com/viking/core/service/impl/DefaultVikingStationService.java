package com.viking.core.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.data.VikingTechnicianData;
import com.viking.core.data.VikingTotalCreditData;
import com.viking.core.service.VikingCreditBlacklistService;
import com.viking.core.service.VikingStationService;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingStationService implements VikingStationService {

    private static final Logger LOG = LogManager.getLogger(DefaultVikingStationService.class);

    private static final String TECHNICIAN_QUALIFICATIONS = "ZGET_SERVTECH_QUALIF";
    private static final String STATION_NUMBER = "IM_SERV_STAT_NO";
    private static final String QUALIFICATIONS_TABLE = "ZT_QUALIFICATIONS";
    private static final String TOTAL_CREDITS = "ZGET_CREDI_TOTALT_ZCERT";
    private static final String TOTAL_CREDITS_TABLE = "ET_CREDI_TOTALT";


    private JCoManagedConnectionFactory jCoManagedConnectionFactory;
    private VikingCreditBlacklistService vikingCreditBlacklistService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<VikingTechnicianData> getTechnicians(String stationId) {

        List<VikingTechnicianData> technicians = new ArrayList<>();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");

            final JCoFunction function = managedConnection.getFunction(TECHNICIAN_QUALIFICATIONS);
            function.getImportParameterList().setValue(STATION_NUMBER, stationId);

            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable(QUALIFICATIONS_TABLE);

            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final VikingTechnicianData technician = new VikingTechnicianData();
                technician.setNumber(table.getString("TECH_NUMBER"));
                technician.setName(table.getString("NAME"));
                technician.setGroup(table.getString("QUALIFIC_GRP"));
                technician.setQualification(table.getString("QUALIFICATION"));
                technician.setValidFrom(table.getString("VALID_FROM"));
                final String valid_to = table.getString("VALID_TO");
                technician.setValidTo(valid_to);

                final LocalDate validTo = LocalDate.parse(valid_to);
                technician.setIsValid(validTo.isAfter(LocalDate.now().minusDays(1)));

                technicians.add(technician);
            }
        } catch (Exception exception) {
            LOG.error(MessageFormat.format("Not able to get technician qualification data for stationID: {0}", stationId), exception);
        }

        return technicians;
    }

    @Override
    public List<VikingTotalCreditData> getCredits(String stationId) {
        List<VikingTotalCreditData> credits = new ArrayList<>();

        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");

            final JCoFunction function = managedConnection.getFunction(TOTAL_CREDITS);
            function.getImportParameterList().setValue(STATION_NUMBER, stationId);

            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable(TOTAL_CREDITS_TABLE);

            final List<String> blacklistedProducts = vikingCreditBlacklistService.getAllCreditBlacklistedProductCodes();

            for (int i = 0; i < table.getNumRows(); i++) {
                table.setRow(i);
                final VikingTotalCreditData credit = new VikingTotalCreditData();
                credit.setMaterialNumber(StringUtils.stripStart(table.getString("MATNR"),"0"));
                Integer convert = (int) Float.parseFloat(table.getString("CREDIT"));
                credit.setCredit(convert.toString());
                credit.setEquipment(table.getString("EQUIPMENT"));

                if (blacklistedProducts.contains(credit.getMaterialNumber())) {
                    continue;
                }

                credits.add(credit);
            }
        } catch (Exception exception) {
            LOG.error(MessageFormat.format("Not able to get total credit data for stationID: {0}", stationId), exception);
        }

        // Sort credits by material number
        Collections.sort(credits, new Comparator<VikingTotalCreditData>() {
            public int compare(final VikingTotalCreditData object1, final VikingTotalCreditData object2) {
                return object1.getMaterialNumber().compareTo(object2.getMaterialNumber());
            }
        });

        return credits;
    }

    @Required
    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    @Required
    public void setVikingCreditBlacklistService(VikingCreditBlacklistService vikingCreditBlacklistService) {
        this.vikingCreditBlacklistService = vikingCreditBlacklistService;
    }
}
