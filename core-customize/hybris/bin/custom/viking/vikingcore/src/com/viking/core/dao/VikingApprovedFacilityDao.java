package com.viking.core.dao;

import com.viking.core.model.ApprovedFacilityModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;

import java.util.Optional;

public interface VikingApprovedFacilityDao {

    Optional<ApprovedFacilityModel> getApprovedFacilityForUser(B2BCustomerModel customer);

}
