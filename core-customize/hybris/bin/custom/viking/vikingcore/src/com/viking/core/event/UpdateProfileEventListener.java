package com.viking.core.event;

import com.viking.core.model.UpdateProfileProcessModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.List;


/**
 * Listener for "create password" functionality event.
 */
public class UpdateProfileEventListener extends AbstractAcceleratorSiteEventListener<UpdateProfileEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;

    protected BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    /**
     * @return the modelService
     */
    protected ModelService getModelService() {
        return modelService;
    }

    /**
     * @param modelService the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Override
    protected void onSiteEvent(final UpdateProfileEvent event) {
        final UpdateProfileProcessModel updateProfileEmailProcess = getBusinessProcessService()
                .createProcess("UpdateProfile-" + ((B2BCustomerModel)event.getCustomer()).getDefaultB2BUnit().getUid() + "-" + System.currentTimeMillis(),
                        "updateProfileEmailProcess");
        updateProfileEmailProcess.setB2BUnitId(event.getB2BUnitId());
        updateProfileEmailProcess.setSite(event.getSite());
        updateProfileEmailProcess.setCustomer(event.getCustomer());
        updateProfileEmailProcess.setLanguage(event.getLanguage());
        updateProfileEmailProcess.setCurrency(event.getCurrency());
        updateProfileEmailProcess.setStore(event.getBaseStore());
        final Collection<AddressModel> addresses = ((B2BCustomerModel) event.getCustomer()).getDefaultB2BUnit().getAddresses();
        if (CollectionUtils.isNotEmpty(addresses)) {
            updateProfileEmailProcess.setAddress(addresses.iterator().next());
        }
        updateProfileEmailProcess.setApprovedFacility(event.getApprovedFacility());
        getModelService().save(updateProfileEmailProcess);
        getBusinessProcessService().startProcess(updateProfileEmailProcess);
    }

    @Override
    protected SiteChannel getSiteChannelForEvent(final UpdateProfileEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }
}
