package com.viking.core.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.service.VikingInvoiceService;
import com.viking.core.util.ContentHelper;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

/**
 * {@inheritDoc}
 */
public class DefaultVikingInvoiceService implements VikingInvoiceService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingInvoiceService.class);

    private static final String PDF_FUNCTION = "YINVOICE_PDF";

    private JCoManagedConnectionFactory jCoManagedConnectionFactory;
    private ContentHelper contentHelper;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInvoiceUrl(final String billingDocumentNumber) {
        try {
            JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");

            final JCoFunction function = managedConnection.getFunction(PDF_FUNCTION);
            function.getImportParameterList().setValue("VBELN", billingDocumentNumber);

            managedConnection.execute(function);

            JCoTable table = function.getTableParameterList().getTable("URI_INV");

            Optional<CatalogUnawareMediaModel> mediaOptional = contentHelper.downloadContentToMedia("SPC_" + billingDocumentNumber, table.getString("URI") + table.getString("MIMETYPE"));
            if (mediaOptional.isPresent()) {
                return mediaOptional.get().getURL();
            } else {
                return StringUtils.EMPTY;
            }

        } catch (Exception exception) {
            LOG.error("Not able to get invoice data for billingDocumentNumber: " + billingDocumentNumber, exception);
            return StringUtils.EMPTY;
        }


    }

    @Required
    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }

    @Required
    public void setContentHelper(ContentHelper contentHelper) {
        this.contentHelper = contentHelper;
    }
}
