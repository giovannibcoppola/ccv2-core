package com.viking.core.outbound.impl;

import com.viking.core.jalo.VikingEnquiryOrder;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultB2CCustomerHelper;
import org.springframework.beans.factory.annotation.Required;

/**
 * Viking Helper class for b2c customer processes within order
 */
public class VikingB2CCustomerHelper extends DefaultB2CCustomerHelper {

    private B2BUnitService b2BUnitService;

    /**
     * Determine OTC for different basestore, when replicate registered user is false. If we hit this helper with a B2B order, then take customer number from root unit
     *
     * @param order for which customer needs to be determined
     * @return customer ID if any is found
     */
    @Override
    public String determineB2CCustomer(final OrderModel order) {
        if(order instanceof VikingEnquiryOrderModel) {
           return  order.getStore().getCustomerNumber();
        }
       else if (SiteChannel.B2B.equals(order.getSite().getChannel())) {
            return b2BUnitService.getRootUnit(order.getUnit()).getUid();
        }

        return Boolean.TRUE.equals(getsAPGlobalConfigurationService().getProperty("replicateregistereduser")) ? ((CustomerModel) order
                .getUser()).getSapConsumerID() : order.getStore().getCustomerNumber();
    }

    @Required
    public void setB2BUnitService(B2BUnitService b2BUnitService) {
        this.b2BUnitService = b2BUnitService;
    }
}
