package com.viking.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * Used to display localized label values of enums
 * (the facets must have been indexes by @{link {@link com.hybris.backoffice.solrsearch.resolvers.CollectionValueResolver}}
 */
public class EnumDisplayNameProvider extends AbstractFacetValueDisplayNameProvider {
    private static final Logger logger = LoggerFactory.getLogger(EnumDisplayNameProvider.class);
    private final EnumerationService enumerationService;
    private final CommonI18NService commonI18NService;

    /**
     * Mandatory services
     * @param enumerationService  enums
     * @param commonI18NService  internationalization
     */
    public EnumDisplayNameProvider(final EnumerationService enumerationService, final CommonI18NService commonI18NService) {
        this.enumerationService = enumerationService;
        this.commonI18NService = commonI18NService;
    }

    @Override public String getDisplayName(final SearchQuery searchQuery, final IndexedProperty indexedProperty, final String s) {

        final String[] fromSolr = split(s, ":");
        if (fromSolr.length < 2) {
            return StringUtils.EMPTY;
        }

        final String hybrisEnum = fromSolr[0];
        final String value = fromSolr[1];


        try {
            final String enumName = Class.forName(hybrisEnum).getSimpleName();
            final HybrisEnumValue enumerationValue = enumerationService.getEnumerationValue(enumName, value);

            final String queryLang = searchQuery.getLanguage();

            final Locale locale = isNotBlank(queryLang) ? commonI18NService.getLocaleForIsoCode(queryLang) : Locale.ENGLISH;
            //
            final String enumerationName = enumerationService.getEnumerationName(enumerationValue, locale);

            if (isBlank(enumerationName) && logger.isDebugEnabled()) {
                logger.debug("Return raw value since the localized enumeration value was not found: lang={}, enum={}]", queryLang, value);
            }

            return StringUtils.defaultIfEmpty(enumerationName, value);

        } catch (ClassNotFoundException e) {
            logger.error("Enumeration not found", e);
        }

        return value;

    }
}
