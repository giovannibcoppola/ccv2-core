package com.viking.core.attributehandlers;

import com.viking.core.model.VikingInvoiceModel;
import com.viking.core.service.VikingInvoiceService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.springframework.beans.factory.annotation.Required;

public class InvoiceLinkToPdfDynamicAttributeHandler extends AbstractDynamicAttributeHandler<String, VikingInvoiceModel> {

    private VikingInvoiceService vikingInvoiceService;

    @Override
    public String get(final VikingInvoiceModel invoice) {
        return vikingInvoiceService.getInvoiceUrl(invoice.getId());
    }

    @Override
    public void set(final VikingInvoiceModel model, final String value) {
        // Not supported
    }

    @Required
    public void setVikingInvoiceService(VikingInvoiceService vikingInvoiceService) {
        this.vikingInvoiceService = vikingInvoiceService;
    }
}
