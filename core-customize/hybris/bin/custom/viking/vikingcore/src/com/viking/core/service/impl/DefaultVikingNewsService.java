package com.viking.core.service.impl;

import com.viking.core.dao.VikingNewsDao;
import com.viking.core.model.CMSMediaParagraphComponentModel;
import com.viking.core.service.VikingNewsService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class DefaultVikingNewsService implements VikingNewsService {

    private static final Logger LOG = LogManager.getLogger(DefaultVikingNewsService.class);

    private static final String NUMBER_OF_NEWS = "number.of.latest.news.to.show";
    private static final int DEFAULT_NUMBER_OF_NEWS = 3;

    private VikingNewsDao vikingNewsDao;
    private ConfigurationService configurationService;
    private UserService userService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CMSMediaParagraphComponentModel> getLatestsNewsForSite(BaseSiteModel site) {
        final int numberOfNews = configurationService.getConfiguration().getInt(NUMBER_OF_NEWS, DEFAULT_NUMBER_OF_NEWS);
        final UserModel user = userService.getCurrentUser();
        final List<CMSMediaParagraphComponentModel> newsForSite = vikingNewsDao.getNewsForSite(site);
        final List<CMSMediaParagraphComponentModel> visibleNews = new ArrayList<>();

        for (CMSMediaParagraphComponentModel news : newsForSite) {
            if (isAccessAllowed(news, user)) {
                visibleNews.add(news);
            }

            if (visibleNews.size() == numberOfNews) {
                break;
            }
        }

        return visibleNews;
    }

    @Override
    public boolean isAccessAllowed(final CMSMediaParagraphComponentModel news, final UserModel user) {
        if(CollectionUtils.isEmpty(news.getRestrictions())) {
            LOG.debug("No restrictions for news [" + news.getHeaderText() + "]. Customer is allowed to view.");
            return true;
        }

        final List<CMSUserGroupRestrictionModel> newsAuthRestrictions = news.getRestrictions().stream().filter(CMSUserGroupRestrictionModel.class::isInstance).map(CMSUserGroupRestrictionModel.class::cast).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(newsAuthRestrictions)) {
            LOG.debug("No restrictions for news [" + news.getHeaderText() + "]. Customer is allowed to view.");
            return true;
        }

        final List<UserGroupModel> restrictionAuthGroups = newsAuthRestrictions.stream().map(CMSUserGroupRestrictionModel::getUserGroups).flatMap(Collection::stream).collect(Collectors.toList());
        final Set<PrincipalGroupModel> customerAuthGroups = user.getGroups();

        // Verify if any of customer auth groups is in news auth groups
        final boolean showNewsForThisUser = customerAuthGroups.stream().anyMatch(restrictionAuthGroups::contains);
        return showNewsForThisUser;
    }

    @Override
    public CMSMediaParagraphComponentModel findNewsForCatalogVersion(final String uid, final CatalogVersionModel catalogVersion) {
        return vikingNewsDao.findNewsForCatalogVersion(uid, catalogVersion);
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Required
    public void setVikingNewsDao(VikingNewsDao vikingNewsDao) {
        this.vikingNewsDao = vikingNewsDao;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
