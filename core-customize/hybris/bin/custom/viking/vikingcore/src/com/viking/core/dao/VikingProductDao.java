package com.viking.core.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.Date;
import java.util.List;

public interface VikingProductDao {

    List<ProductModel> findProductsWithModifiedTimeGreaterThan(final Date modifiedTime);
}
