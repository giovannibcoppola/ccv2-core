package com.viking.core.interceptors;

import com.viking.core.service.VikingCommerceCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Prepare interceptor for {@link CategoryModel}. It adds StoreId.
 *
 * @author chiranjit
 */
public class VikingCategoryPrepareInterceptor implements PrepareInterceptor<CategoryModel> {

    @Autowired
    private VikingCommerceCategoryService vikingCommerceCategoryService;
    @Override
    public void onPrepare(CategoryModel categoryModel, InterceptorContext interceptorContext) {
        vikingCommerceCategoryService.getStoreIdForCategory(categoryModel).ifPresent(categoryModel::setStoreId);
    }

}
