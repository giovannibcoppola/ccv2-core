package com.viking.core.enquiry.hook.impl;

import com.viking.core.event.EnquirySubmitEvent;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.commerceservices.event.QuoteBuyerOrderPlacedEvent;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.order.hook.impl.CommercePlaceQuoteOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class VikingPlaceEnquiryOrderMethodHook implements CommercePlaceOrderMethodHook
{
    private static final Logger LOG = Logger.getLogger(VikingPlaceEnquiryOrderMethodHook.class);
    private EventService eventService;

    @Override
    public void afterPlaceOrder(final CommerceCheckoutParameter commerceCheckoutParameter,
                                final CommerceOrderResult commerceOrderResult)
    {
        final OrderModel order = commerceOrderResult.getOrder();
        ServicesUtil.validateParameterNotNullStandardMessage("order", order);

        if (order != null && order instanceof VikingEnquiryOrderModel)
        {
            if (LOG.isDebugEnabled())
            {
                LOG.debug(String.format("Enquiry Order has been placed. Enquiry Order Code : [%s]", order.getCode()));
            }
        final EnquirySubmitEvent event = new EnquirySubmitEvent((VikingEnquiryOrderModel) order);
        event.setSite(order.getSite());
        eventService.publishEvent(event);
        }
    }

    @Override
    public void beforePlaceOrder(final CommerceCheckoutParameter commerceCheckoutParameter) {
        // not implemented
    }

    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter commerceCheckoutParameter,
                                  final CommerceOrderResult commerceOrderResult) {
        // not implemented
    }

    protected EventService getEventService()
    {
        return eventService;
    }

    @Required
    public void setEventService(final EventService eventService)
    {
        this.eventService = eventService;
    }
}