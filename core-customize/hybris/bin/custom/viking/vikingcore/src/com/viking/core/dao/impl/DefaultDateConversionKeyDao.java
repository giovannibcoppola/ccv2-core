package com.viking.core.dao.impl;

import com.viking.core.dao.DateConversionKeyDao;
import com.viking.core.model.DateConversionKeyModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class DefaultDateConversionKeyDao implements DateConversionKeyDao {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<DateConversionKeyModel> getAll() {
        SearchResult<DateConversionKeyModel> searchResult = flexibleSearchService.search("SELECT {" + DateConversionKeyModel.PK + "} FROM {" + DateConversionKeyModel._TYPECODE + "}");
        return searchResult.getResult();
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
