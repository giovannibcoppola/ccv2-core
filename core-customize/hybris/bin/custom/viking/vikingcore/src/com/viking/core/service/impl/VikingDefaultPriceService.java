package com.viking.core.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.impl.DefaultPriceService;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.sap.sapmodel.services.SAPUnitService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoStructure;
import com.sap.conn.jco.JCoTable;
import com.viking.core.service.VikingPriceInformation;
import com.viking.core.service.VikingPriceService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.ObjectUtils;


public class VikingDefaultPriceService extends DefaultPriceService implements VikingPriceService
{

	private static final Logger LOG = Logger.getLogger(VikingDefaultPriceService.class);

	private static final String PRICE_FALLBACK_ENABLE = "price.fallback.enable";

	private JCoManagedConnectionFactory jCoManagedConnectionFactory;
	private CommonI18NService commonI18NService;
	private UserService userService;
	private BaseStoreService baseStoreService;
	private SAPUnitService sapUnitService;
	private ConfigurationService configurationService;
	private CartService cartService;

	private final Map<String, CacheEntry> cache = new HashMap<>();

	private class CacheEntry
	{

		private Map<String, JCoTable> tables = new HashMap<>();

		private Date lastCall = new Date();

		public Map<String, JCoTable> getTables()
		{
			return tables;
		}

		public void setTables(final Map<String, JCoTable> tables)
		{
			this.tables = tables;
		}

		public Date getLastCall()
		{
			return lastCall;
		}

		public void setLastCall(final Date lastCall)
		{
			this.lastCall = lastCall;
		}
	}

	@Override
	public List<PriceInformation> getPriceInformationsForProduct(final ProductModel product)
	{

		if (ignorePrice()){
			final PriceValue priceValue = new PriceValue("EUR", 0, false);
			return Collections.singletonList(new VikingPriceInformation(priceValue, null, StringUtils.EMPTY));
		}

		final List<PriceInformation> prices = new ArrayList<>();

		try {
			final JCoTable result = callPriceATP(product.getCode(), "IT_VBAP");
			if (result != null) {
				if (result.getNumRows() > 0) {
					result.firstRow();
					BigDecimal price = result.getBigDecimal("KZWI1");
					BigDecimal discount = result.getBigDecimal("KZWI6");
					LOG.debug("Product [" + product.getCode() + "] - [KZWI1: " + price + " ] - [KZWI6: " + discount + "]");

					if (discount != null) {
						price = price.add(discount);
					}

					final PriceValue priceValue = new PriceValue(result.getString("WAERK"), price.doubleValue(), baseStoreService.getCurrentBaseStore().isNet());

					final TaxValue tax = new TaxValue("SAP tax", result.getBigDecimal("MWSBP").doubleValue(), true, result.getBigDecimal("MWSBP").doubleValue(), result.getString("WAERK"));
					final String uomName = getUnitName(result);

					LOG.info("price for product " + product.getCode() + " " + priceValue.getValue() + " " + priceValue.getCurrencyIso() + " " + uomName);
					LOG.info("tax for product " + product.getCode() + " " + tax.getValue() + " " + priceValue.getCurrencyIso());

					setCurrencyInSessionAndCart(priceValue.getCurrencyIso());
					prices.add(new VikingPriceInformation(priceValue, tax, uomName));
				}
			}
		} catch (final BackendException e) {
			LOG.error("Error during call sap ERP backend for prices", e);
		}

		if(CollectionUtils.isEmpty(prices)) {
			try {
				final boolean isPriceFallbackEnabled = configurationService.getConfiguration().getBoolean(PRICE_FALLBACK_ENABLE, Boolean.FALSE);
				if (isPriceFallbackEnabled) {
					final List<PriceInformation> priceInformationsForProduct = super.getPriceInformationsForProduct(product);
					if(CollectionUtils.isNotEmpty(priceInformationsForProduct)) {
						for (PriceInformation priceInformation : priceInformationsForProduct) {
							prices.add(new VikingPriceInformation(priceInformation.getPriceValue(), null, null));
						}
						setCurrencyInSessionAndCart(priceInformationsForProduct.get(0).getPriceValue().getCurrencyIso());
					}
				}
			} catch (ConversionException ex) {
				LOG.error(ex);
			}
		}
		//Uncomment below to get default prices
		//getCommonI18NService().setCurrentCurrency(baseStoreService.getCurrentBaseStore().getDefaultCurrency());
		//prices.add(new VikingPriceInformation(new PriceValue(commonI18NService.getCurrentCurrency().getIsocode(), 10.00d, baseStoreService.getCurrentBaseStore().isNet()), new TaxValue("Sap tax", 2.00d, true, 2.00d, commonI18NService.getCurrentCurrency().getIsocode()), "Item"));
		return prices;
	}

	private boolean ignorePrice() {
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return currentBaseStore != null && BooleanUtils.isTrue(currentBaseStore.getIgnorePrice());
	}

	private void setCurrencyInSessionAndCart(final String isoCode) {
		CurrencyModel currency = getCommonI18NService().getCurrency(isoCode);
		getCommonI18NService().setCurrentCurrency(currency);
		if(currency != null && cartService.hasSessionCart()) {
			final CartModel cartModel = cartService.getSessionCart();
			if(cartModel.getCurrency() == null || !currency.equals(cartModel.getCurrency())) {
				cartModel.setCurrency(currency);
			}
		}
	}

	private String getUnitName(JCoTable result) {
		try {
			final UnitModel uom = sapUnitService.getUnitForSAPCode(result.getString("VRKME"));
			return uom.getName();
		} catch (Exception ex) {
			LOG.debug("No unit found for code: " + result.getString("VRKME"), ex);
			return StringUtils.EMPTY;
		}
	}


	@Override
	public JCoTable callPriceATP(final String productCode, final String resultTable) throws BackendException
	{
		final UserModel currentUser = userService.getCurrentUser();
		JCoTable jcoTable = null;

		if (currentUser != null && currentUser instanceof B2BCustomerModel)
		{
			final B2BUnitModel unit = ((B2BCustomerModel) currentUser).getDefaultB2BUnit();
			if (unit != null && unit.getSapSalesOrganization() != null)
			{
				final String b2bUnit = StringUtils.leftPad(unit.getUid(), 10, "0");
				final String b2bSalesUnit = unit.getSapSalesOrganization().getCode();
				final String material = StringUtils.leftPad(productCode, 18, "0");

				jcoTable = getjCoTable(b2bUnit, b2bSalesUnit, material, resultTable);
			}
		} else if (userService.isAnonymousUser(currentUser) && !ObjectUtils.isEmpty(baseStoreService.getCurrentBaseStore().getSapSalesOrganization())) {
			final BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
			LOG.debug("Get price for product: " + productCode + " and customer: " + baseStore.getCustomerNumber() + " and SAPSalesOrg: " + baseStore.getSapSalesOrganization().getCode());
			jcoTable = getjCoTable(baseStore.getCustomerNumber(), baseStore.getSapSalesOrganization().getCode(), productCode, resultTable);
		}
		return jcoTable;
	}

	private JCoTable getjCoTable(final String b2bUnit, final String b2bSalesUnit, final String material, final String resultTable) throws BackendException {
		final String cacheKey = b2bUnit + b2bSalesUnit + material;

		if (cache.size() > 5000) {
			cache.clear();
		}

		if (cache.containsKey(cacheKey) && new Date().getTime() - 300000 < cache.get(cacheKey).lastCall.getTime()) {
			return cache.get(cacheKey).getTables().get(resultTable);
		} else if (cache.containsKey(cacheKey)) {
			cache.remove(cacheKey);
		}

		final JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");
		final JCoFunction function = managedConnection.getFunction("YHYBRIS_PRICE_ATP_02");
		function.getImportParameterList().setValue("IDENTIFIER", "Hybris");
		final JCoStructure orderHeaderIn = function.getImportParameterList().getStructure("ORDER_HEADER_IN");
		orderHeaderIn.setValue("DOC_TYPE", "OR");
		orderHeaderIn.setValue("SALES_ORG", b2bSalesUnit);
		orderHeaderIn.setValue("DISTR_CHAN", "01");
		orderHeaderIn.setValue("DIVISION", "00");
		orderHeaderIn.setValue("ORD_REASON", "ZR");

		final JCoTable orderItemsIn = function.getTableParameterList().getTable("ORDER_ITEMS_IN");
		orderItemsIn.appendRow();
		orderItemsIn.setValue("ITM_NUMBER", "000010");
		orderItemsIn.setValue("HG_LV_ITEM", "000000");
		orderItemsIn.setValue("MATERIAL", material);
		orderItemsIn.setValue("DLV_GROUP", "000");
		orderItemsIn.setValue("TARGET_QTY", "0000000000001");
		orderItemsIn.setValue("TARGET_QU", "PCS");
		orderItemsIn.setValue("REQ_QTY", "0000000000000");
		orderItemsIn.setValue("REQ_TIME", "00:00:00");

		final JCoTable orderPartners = function.getTableParameterList().getTable("ORDER_PARTNERS");
		orderPartners.appendRow();
		orderPartners.setValue("PARTN_ROLE", "AG");
		orderPartners.setValue("PARTN_NUMB", b2bUnit);
		orderPartners.setValue("ITM_NUMBER", "000000");

		managedConnection.execute(function);

		LOG.info("GMESSEGE: " + function.getExportParameterList().getValue("GMESSAGE"));

		final CacheEntry cacheEntry = new CacheEntry();
		cacheEntry.getTables().put("IT_VBAP", function.getTableParameterList().getTable("IT_VBAP"));
		cacheEntry.getTables().put("IT_VBEP", function.getTableParameterList().getTable("IT_VBEP"));
		cacheEntry.setLastCall(new Date());
		cache.put(cacheKey, cacheEntry);

		return function.getTableParameterList().getTable(resultTable);
	}

	private String lookupConfig(final String key)
	{
		return configurationService.getConfiguration().getString(key, StringUtils.EMPTY);
	}

	public JCoManagedConnectionFactory getjCoManagedConnectionFactory()
	{
		return jCoManagedConnectionFactory;
	}

	@Required
	public void setjCoManagedConnectionFactory(final JCoManagedConnectionFactory jCoManagedConnectionFactory)
	{
		this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	@Override
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Required
	public void setBaseStoreService(BaseStoreService baseStoreService) {
		this.baseStoreService = baseStoreService;
	}

	@Required
	public void setSapUnitService(SAPUnitService sapUnitService) {
		this.sapUnitService = sapUnitService;
	}

	@Required
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
	@Required
	public void setCartService(CartService cartService) {
		this.cartService = cartService;
	}
}
