package com.viking.core.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.viking.core.integration.azure.ChecklistsData;
import com.viking.core.integration.azure.ChecklistsResponseData;
import com.viking.core.service.VikingChecklistsService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

public class VikingDefaultChecklistsService implements VikingChecklistsService {
    private static final Logger LOG = LoggerFactory.getLogger(VikingChecklistsService.class);
    private static final String CHECKLISTS_DATA_GET_REQUEST_URL = "azure.client.checklists.url";

    private static final String CHECKLISTS_ACCESS_KEY = "azure.client.portal.accesskey";
    private ConfigurationService configurationService;

    private List<ChecklistsData> checklistsDataList = null;
    private ChecklistsData foundChecklistData = null;

    @Override
    public ChecklistsData getChecklist(String id){
        checklistsDataList = getChecklists();

        this.foundChecklistData = null;
        ChecklistsData checklistsData = getChecklistById(this.checklistsDataList, id);

        return checklistsData;
    }

    private ChecklistsData getChecklistById(List<ChecklistsData> checklistsDataList, String id){

        for (ChecklistsData checklistsData : checklistsDataList){
            if (checklistsData.getId().equals(id)) {
                this.foundChecklistData = checklistsData;
                break;
            }
            if (checklistsData.getChildren() != null){
                getChecklistById(checklistsData.getChildren(), id);
            }
        }
        return this.foundChecklistData;
    }

    private List<ChecklistsData> getChecklists() {
        try {
            final HttpResponse response = getHttpResponse(configurationService.getConfiguration().getString(CHECKLISTS_DATA_GET_REQUEST_URL), configurationService.getConfiguration().getString(CHECKLISTS_ACCESS_KEY));

            final ObjectMapper objectMapper = getObjectMapper();

            final ChecklistsResponseData responseData = objectMapper.readValue(response.getEntity().getContent(), ChecklistsResponseData.class);
            return responseData.getChecklists();
        }catch (IOException exception) {
            LOG.error(" Not able to get Checklists", exception);
            return Collections.emptyList();
        }
    }

    private ObjectMapper getObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    private HttpResponse getHttpResponse(String url, String key) throws IOException {
        final SSLConnectionSocketFactory sslsf;

        try {
            sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault(),
                    NoopHostnameVerifier.INSTANCE);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new PlainConnectionSocketFactory())
                .register("https", sslsf)
                .build();

        final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);

        cm.setMaxTotal(100);
        final HttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setConnectionManager(cm)
                .build();

        final HttpGet request = new HttpGet(url);
        // add request header
        request.addHeader("x-functions-key", key);
        //request.addHeader("Authorization", "No");

        return httpClient.execute(request);
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
