package com.viking.core.catalog.synchronization.strategy;

import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.core.model.ItemModel;

import java.util.List;

public interface SyncJobApplicableItemStrategy {

    /**
     * Flters the inputItems for a given context i.e. given <code>syncItemJob </code>.
     *
     * @return the items to sync are returned. The ones to skip are removed from the returned list
     */
    List<ItemModel> filterItems(final List<ItemModel> inputItems, final SyncItemJobModel syncItemJob);

}
