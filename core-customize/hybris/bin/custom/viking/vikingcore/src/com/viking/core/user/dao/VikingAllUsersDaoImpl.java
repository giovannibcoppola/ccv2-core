/**
 *
 */
package com.viking.core.user.dao;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.viking.core.model.VikingVesselModel;


/**
 * @author Srikanth
 *
 */
public class VikingAllUsersDaoImpl implements VikingUserPage
{

	private static final String FIND_REMOVE_USER = "SELECT {" + B2BCustomerModel.PK + "} FROM {" + B2BCustomerModel._TYPECODE
			+ "} where {" + B2BCustomerModel.UID + "}= ?uid";

	private static final String FIND_USER = "SELECT {" + B2BCustomerModel.PK + "} FROM {" + B2BCustomerModel._TYPECODE
			+ "} where {" + B2BCustomerModel.NAME + "}= ?name";

	private static final String FIND_ALL_USER = "SELECT {" + B2BCustomerModel.PK + "} FROM {" + B2BCustomerModel._TYPECODE + "}";

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.dao.VikingUserPage#getAllUsers()
	 */
	@Override
	public List<B2BCustomerModel> getAllUsers()
	{
		// YTODO Auto-generated method stub

		final StringBuilder query = new StringBuilder(FIND_ALL_USER);
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final SearchResult<B2BCustomerModel> search = flexibleSearchService.search(searchQuery);
		if (CollectionUtils.isNotEmpty(search.getResult()))
		{
			return search.getResult();
		}
		else
		{
			return null;
		}

	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.dao.VikingUserPage#getVesselIds()
	 */
	@Override
	public VikingVesselModel getVesselIds(final String vesselID)
	{
		// YTODO Auto-generated method stub
		final FlexibleSearchQuery query = new FlexibleSearchQuery("select {pk} from {VikingVessel} where {vesselId}=?Id");
		query.addQueryParameter("Id", vesselID);
		final SearchResult searchResult = flexibleSearchService.search(query);
		final List<VikingVesselModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			return result.get(0);
		}
		else
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.dao.VikingUserPage#removeUser()
	 */
	@Override
	public B2BCustomerModel removeUser(final String id)
	{
		// YTODO Auto-generated method stub
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_REMOVE_USER);
		query.addQueryParameter("uid", id);
		final SearchResult searchResult = flexibleSearchService.search(query);
		final List<B2BCustomerModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			return result.get(0);
		}
		else
		{
			return null;
		}
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.dao.VikingUserPage#getUser(java.lang.String)
	 */
	@Override
	public B2BCustomerModel getUser(final String name)
	{
		// YTODO Auto-generated method stub
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_USER);
		query.addQueryParameter("name", name);
		final SearchResult searchResult = flexibleSearchService.search(query);
		final List<B2BCustomerModel> result = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(result))
		{
			return result.get(0);
		}
		else
		{
			return null;
		}
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.dao.VikingUserPage#getAllVessels()
	 */
	@Override
	public List<VikingVesselModel> getAllVessels()
	{
		// YTODO Auto-generated method stub
		final StringBuilder query = new StringBuilder("select {pk} from {VikingVessel}");
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
		final SearchResult<VikingVesselModel> search = flexibleSearchService.search(searchQuery);

		if (CollectionUtils.isNotEmpty(search.getResult()))
		{
			return search.getResult();
		}
		else
		{
			return null;
		}
	}

}
