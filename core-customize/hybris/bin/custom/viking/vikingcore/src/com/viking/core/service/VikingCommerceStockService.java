package com.viking.core.service;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Date;

public interface VikingCommerceStockService extends CommerceStockService {

    StockLevelStatus getStockLevelStatusForProduct(String productCode);

    Long getStockLevelForProduct(String productCode);

    Date getStockLevelDateForProduct(String productCode);

    int getStockLevelRange(String productCode);
}
