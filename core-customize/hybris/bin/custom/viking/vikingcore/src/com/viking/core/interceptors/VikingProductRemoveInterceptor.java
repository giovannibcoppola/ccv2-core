package com.viking.core.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.user.UserService;

/**
 * Prepare interceptor for {@link ProductModel}
 *
 * @author amine.elharrak
 */
public class VikingProductRemoveInterceptor implements RemoveInterceptor<ProductModel> {

    private final UserService userService;

    @Autowired
    public VikingProductRemoveInterceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onRemove(final ProductModel productModel, final InterceptorContext interceptorContext) throws InterceptorException {
        if(!userService.isAdmin(userService.getCurrentUser())) {
            throw new InterceptorException("Cannot remove product !");
        }
    }
}
