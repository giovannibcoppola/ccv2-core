package com.viking.core.integration.servicenow.data;

public class RawJsonVesselsResponseData {

    private VesselsResponseData result;

    public void setResult(final VesselsResponseData result) {
        this.result = result;
    }

    public VesselsResponseData getResult() {
        return result;
    }
}
