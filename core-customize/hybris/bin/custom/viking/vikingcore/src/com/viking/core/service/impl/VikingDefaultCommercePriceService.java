package com.viking.core.service.impl;

import com.viking.core.service.VikingCommercePriceService;
import com.viking.core.service.VikingPriceService;
import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import org.apache.log4j.Logger;

import java.util.List;

public class VikingDefaultCommercePriceService extends DefaultCommercePriceService implements VikingCommercePriceService {

    private static final Logger LOG = Logger.getLogger(VikingDefaultCommercePriceService.class);

    @Override
    public PriceInformation getFromPriceForProduct(ProductModel product) {
        List<PriceInformation> priceInformations = getPriceService().getPriceInformationsForProduct(product);
        return priceInformations.isEmpty() ? null : priceInformations.get(0);
    }

    @Override
    public PriceInformation getWebPriceForProduct(ProductModel product) {
        List<PriceInformation> priceInformations = getPriceService().getPriceInformationsForProduct(product);
        return priceInformations.isEmpty() ? null : priceInformations.get(0);
    }
}
