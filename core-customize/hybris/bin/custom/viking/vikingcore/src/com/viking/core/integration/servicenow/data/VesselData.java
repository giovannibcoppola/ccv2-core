/**
 *
 */
package com.viking.core.integration.servicenow.data;

/**
 * @author Prabhakar
 *
 */
public class VesselData
{

	private String name;
	private String imo;
	private String certificateStatus;
	private String actionRequired;
	private String ticketId;
	private boolean partOfContract;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImo() {
		return imo;
	}

	public void setImo(String imo) {
		this.imo = imo;
	}

	public String getCertificateStatus() {
		return certificateStatus;
	}

	public void setCertificateStatus(String certificateStatus) {
		this.certificateStatus = certificateStatus;
	}

	public String getActionRequired() {
		return actionRequired;
	}

	public void setActionRequired(String actionRequired) {
		this.actionRequired = actionRequired;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public boolean isPartOfContract() {
		return partOfContract;
	}

	public void setPartOfContract(boolean partOfContract) {
		this.partOfContract = partOfContract;
	}
}
