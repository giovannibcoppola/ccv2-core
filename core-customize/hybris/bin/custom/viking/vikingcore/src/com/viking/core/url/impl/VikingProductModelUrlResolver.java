package com.viking.core.url.impl;

import com.viking.core.url.impl.util.VikingSubCategoriesRootHelper;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;

/**
 * Viking implementation of ProductModelUrlResolver extending {@link DefaultProductModelUrlResolver}
 *
 * @author javier.gomez
 */
public class VikingProductModelUrlResolver extends DefaultProductModelUrlResolver {

    @Override
    protected String resolveInternal(final ProductModel source)
    {
        final ProductModel baseProduct = getProductAndCategoryHelper().getBaseProduct(source);

        String url = getPattern();

        if (url.contains("{category-path}"))
        {
            url = url.replace("{category-path}", this.buildPathString(getCategoryPath(baseProduct)));
        }

        if (url.contains("{product-code}"))
        {
            url = url.replace("{product-code}", urlEncode(source.getCode()));
        }

        return url;
    }

    @Override
    protected List<CategoryModel> getCategoryPath(final CategoryModel category)
    {
        final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
        // Return first - there will always be at least 1
        return VikingSubCategoriesRootHelper.getSubCategoriesOfRoot(paths);
    }


}
