package com.viking.core.service.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.viking.core.dao.VikingManualBulletinsAcceptedDao;
import com.viking.core.integration.azure.*;
import com.viking.core.jalo.ManualBulletinsAccepted;
import com.viking.core.model.ManualBulletinsAcceptedModel;
import com.viking.core.service.VikingManualsService;
import com.viking.core.util.ContentHelper;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.viking.core.constants.VikingCoreConstants.AZURE_DOCUMENTS_FOLDER;

public class VikingDefaultManualsService implements VikingManualsService {
    private static final Logger LOG = LogManager.getLogger(VikingManualsService.class);

    private static final String MANUALS_DATA_GET_REQUEST_URL = "azure.client.manuals.url";
    private static final String ADDITIONAL_MANUALS_DATA_GET_REQUEST_URL = "azure.client.additional.manuals.url";
    private static final String ADDITIONAL_MANUALS_DATA_PDF_DOWNLOAD_URL = "azure.client.additional.manuals.pdf.download.url";

    private static final String EDITOR_ACCESS_KEY = "azure.client.editor.accesskey";
    private static final String MANUALS_ACCESS_KEY = "azure.client.portal.accesskey";
    private static final String ADDITIONAL_DATA_KEY = "azure.client.additional.accesskey";

    private ContentHelper contentHelper;
    private MediaService mediaService;
    private ConfigurationService configurationService;
    private ModelService modelService;
    private UserService userService;
    private VikingManualBulletinsAcceptedDao manualBulletinsAcceptedDao;
    private List<ManualsData> manualsDataList = null;


    @Override
    public List<ManualsData> getManuals(){
        try {
            final String url = configurationService.getConfiguration().getString(MANUALS_DATA_GET_REQUEST_URL);

            LOG.debug("Get manuals for URL: " + url);

            final HttpResponse response = getHttpResponse(url, configurationService.getConfiguration().getString(MANUALS_ACCESS_KEY));

            final ObjectMapper objectMapper = getObjectMapper();

            final ManualsResponseData responseData = objectMapper.readValue(response.getEntity().getContent(), ManualsResponseData.class);
            final List<ManualsData> manuals = responseData.getManuals();

            for (ManualsData manual : manuals) {
                final List<BulletinsData> bulletins = manual.getBulletins();

                for (BulletinsData bulletin : bulletins) {
                    final String text = bulletin.getConfirmationLetter().getText();
                    bulletin.getConfirmationLetter().setText(text.replaceAll("\"", "'"));
                }
            }

            return manuals;
        } catch(IOException exception){
            LOG.error(" Not able to get Manuals", exception);
            return Collections.emptyList();
        }
    }

    @Override
    public ManualData getManual(String id) {
        LOG.debug("Get Manual for ID: " + id);
        manualsDataList = getManuals();

        for (ManualsData manualsData : this.manualsDataList) {
            if (manualsData.getId().equals(id)) {
                try {
                    final String url = configurationService.getConfiguration().getString(MANUALS_DATA_GET_REQUEST_URL) + "/" + id + "/version/" + manualsData.getPublished();
                    LOG.debug("Get ManualData for url: " + url);
                    final HttpResponse response = getHttpResponse(url, configurationService.getConfiguration().getString(MANUALS_ACCESS_KEY));

                    final ObjectMapper objectMapper = getObjectMapper();
                    final ManualData manualData = objectMapper.readValue(response.getEntity().getContent(), ManualData.class);
                    return manualData;
                } catch(IOException exception){
                    LOG.error(" Not able to get Manual", exception);
                }
            }
        }

        return null;
    }

    @Override
    public List<BulletinsData> getBulletins(String id) {
        LOG.debug("Get Bulletins for ID: " + id);
        if (this.manualsDataList == null || this.manualsDataList.isEmpty()) {
            this.manualsDataList = this.getManuals();
        }

        for (ManualsData manualsData : this.manualsDataList) {
            if (manualsData.getId().equals(id)) {
                final List<BulletinsData> bulletinsDataList = manualsData.getBulletins();
                for (BulletinsData bulletinsData : bulletinsDataList){
                    Long timeStamp = bulletinsData.getDate();
                    bulletinsData.setViewDate(getViewDate(timeStamp));
                    bulletinsData.setAccepted(isBulletinAccepted(bulletinsData, manualsData.getId()));
                }

                Collections.sort(bulletinsDataList, new Comparator<BulletinsData>() {
                    public int compare(BulletinsData o1, BulletinsData o2) {
                        if (getDate(o1.getDate()) == null || getDate(o2.getDate()) == null)
                            return 0;
                        return (getDate(o2.getDate()).compareTo(getDate(o1.getDate())));
                    }
                });

                return bulletinsDataList;
            }
        }

        return null;
    }

    @Override
    public List<ManualBulletinsAccepted> getManualBulletinsAccepted(String bulletinsId, String manualId, String userId) {
        return manualBulletinsAcceptedDao.getManualBulletinsAccpted(bulletinsId, manualId, userId);
    }

    @Override
    public void setManualBulletinsAccepted(String bulletinsId, String manualsId, String userId) throws IOException{
        final ManualBulletinsAcceptedModel manualBulletinsAccepted = modelService.create(ManualBulletinsAcceptedModel.class);

        manualBulletinsAccepted.setBulletinsId(bulletinsId);
        manualBulletinsAccepted.setManualsId(manualsId);
        manualBulletinsAccepted.setUserId(userId);
        modelService.save(manualBulletinsAccepted);
    }

    private ObjectMapper getObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    private HttpResponse getHttpResponse(String url, String key) throws IOException {
        final SSLConnectionSocketFactory sslsf;

        try {
            sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault(),
                    NoopHostnameVerifier.INSTANCE);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new PlainConnectionSocketFactory())
                .register("https", sslsf)
                .build();

        final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
        cm.setMaxTotal(100);

        boolean useProxy = configurationService.getConfiguration().getBoolean("azure.client.use.proxy.for.download", false);
        HttpClient httpClient;

        if (useProxy) {
            final HttpHost proxy = new HttpHost("10.39.78.26");

            httpClient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .setConnectionManager(cm)
                    .setProxy(proxy)
                    .build();
        } else {
            httpClient = HttpClients.custom()
                    .setSSLSocketFactory(sslsf)
                    .setConnectionManager(cm)
                    .build();
        }

        final HttpGet request = new HttpGet(url);
        request.addHeader("x-functions-key", key);

        return httpClient.execute(request);
    }

    @Override
    public String getViewDate(long timestamp){

        final String java_date = getDate(timestamp);

        String[] parts = java_date.split("-");
        final String year = parts[0];
        final String month = parts[1];
        final String day = parts[2];
        String viewDate = null;

        switch (month) {
            case "01":
                viewDate = day + " January " + year;
                break;
            case "02":
                viewDate = day + " February " + year;
                break;
            case "03":
                viewDate = day + " March " + year;
                break;
            case "04":
                viewDate = day + " April " + year;
                break;
            case "05":
                viewDate = day + " May " + year;
                break;
            case "06":
                viewDate = day + " June " + year;
                break;
            case "07":
                viewDate = day + " July " + year;
                break;
            case "08":
                viewDate = day + " August " + year;
                break;
            case "09":
                viewDate = day + " September " + year;
                break;
            case "10":
                viewDate = day + " October " + year;
                break;
            case "11":
                viewDate = day + " November " + year;
                break;
            case "12":
                viewDate = day + " December " + year;
                break;
        }

        return viewDate;
    }

    @Override
    public String download(String url, final String legacy, final boolean extensionReq, String fileName) {
        try {
            String key;

            LOG.debug("Calling Azure API for download url " + url + " and legacy document: " + legacy);

            if ("null".equalsIgnoreCase(legacy)) {
                key = configurationService.getConfiguration().getString(ADDITIONAL_DATA_KEY);
                url = url + key;
            } else if ("true".equalsIgnoreCase(legacy)) {
                key = configurationService.getConfiguration().getString(MANUALS_ACCESS_KEY);
            } else {
                key = configurationService.getConfiguration().getString(EDITOR_ACCESS_KEY);
            }

            if (url.contains("path=")) {
                final int pathIndex = url.indexOf("path=");
                final String pathParam = url.substring(pathIndex + 5);
                final String encodedPathParam = URLEncoder.encode(pathParam, StandardCharsets.UTF_8.toString());
                url = url.replace(pathParam, encodedPathParam);
                if (StringUtils.isEmpty(fileName)) {
                    fileName = url.substring(pathIndex + 5);
                }
            } else if (url.contains("file/")) {
                url = url.replaceAll(" ", "%20");
                if (StringUtils.isEmpty(fileName)) {
                    fileName = url.substring(url.lastIndexOf("/") + 1);
                }
            }

            LOG.debug("Encoded download url " + url);

            final HttpResponse httpResponse = getHttpResponse(url, key);

            LOG.debug("STATUSLINE: " + httpResponse.getStatusLine().toString());

            if (httpResponse.getStatusLine().getStatusCode() != 200) {
                throw new IllegalStateException("Response from Azure was not StatusCode 200, but " + httpResponse.getStatusLine().getStatusCode());
            }

            Optional<CatalogUnawareMediaModel> existingMedia = contentHelper.findExistingCatalogUnawareMediaForContent(fileName);

            CatalogUnawareMediaModel media;
            if (existingMedia.isPresent() ) {
                media = (CatalogUnawareMediaModel) existingMedia.get();
            } else {
                media = modelService.create(CatalogUnawareMediaModel.class);
                media.setRealFileName(fileName);
                media.setCode(fileName);
                media.setFolder(mediaService.getFolder(AZURE_DOCUMENTS_FOLDER));
                modelService.save(media);
            }

            mediaService.setStreamForMedia(media, httpResponse.getEntity().getContent());
            if (extensionReq) {
                media.setMime("application/pdf");
            }
            modelService.save(media);

            return media.getURL();
        } catch (Exception e) {
            LOG.error("Not able to download from Azure", e);
        }

        return null;
    }

    @Override
    public VesselDocsData getVesselDocuments(final String imo) {
        InputStream is = null;

        try {
            final String key = configurationService.getConfiguration().getString(ADDITIONAL_DATA_KEY);
            final String url = MessageFormat.format(configurationService.getConfiguration().getString(ADDITIONAL_MANUALS_DATA_GET_REQUEST_URL), imo, key);
            LOG.debug("Get VesselDoc for url: " + url);

            is = new URL(url).openStream();

            final ObjectMapper objectMapper = getObjectMapper();
            final VesselDocsData vesselDocsData = objectMapper.readValue(is, VesselDocsData.class);

            for (VesselDocData doc : vesselDocsData.getOperation()) {
                updateUrlAndUploaded(doc);
            }

            for (VesselDocData doc : vesselDocsData.getInstallation()) {
                updateUrlAndUploaded(doc);
            }

            return vesselDocsData;
        } catch (IOException | UnsupportedOperationException ex) {
            LOG.error("Not able to get VesselDoc for IMO " + imo, ex);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ex) {
                LOG.debug("Exception happened while closing InputStream", ex);
            }
        }

        return null;
    }

    private void updateUrlAndUploaded(final VesselDocData doc) {
        final String azureDownloadUrl = configurationService.getConfiguration().getString(ADDITIONAL_MANUALS_DATA_PDF_DOWNLOAD_URL);
        doc.setDownloadUrl(MessageFormat.format(azureDownloadUrl, doc.getId()));
        LOG.debug("Converting timestamp: " + doc.getUploaded() + " to a readable date");
        doc.setUploaded(getDate(Long.valueOf(doc.getUploaded())));
    }

    private String getDate(long timestamp){
        //convert seconds to milliseconds
        Date date = new Date(timestamp*1000L);
        // format of the date
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd");
        jdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));

        return jdf.format(date);
    }


    public boolean isBulletinAccepted(final BulletinsData bulletin, final String manualsId, final String uid) {
        final List<ManualBulletinsAccepted> manualBulletinsAccepted = manualBulletinsAcceptedDao.getManualBulletinsAccpted(bulletin.getId(), manualsId, uid);

        if (CollectionUtils.isNotEmpty(manualBulletinsAccepted) || bulletin.isLegacy()) {
            // If there is a manualBulletinsAccepted record OR if bulletin is legacy - then its accepted
            return true;
        }

        // If there is no manualBulletinsAccepted record AND bulletin is not legacy - then its not accepted
        return false;
    }

    private boolean isBulletinAccepted(BulletinsData bulletin, String manualsId) {
        final String userId = userService.getCurrentUser().getUid();

        return isBulletinAccepted(bulletin, manualsId, userId);
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Required
    public void setManualBulletinsAcceptedDao(VikingManualBulletinsAcceptedDao manualBulletinsAcceptedDao) {
        this.manualBulletinsAcceptedDao = manualBulletinsAcceptedDao;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setContentHelper(ContentHelper contentHelper) {
        this.contentHelper = contentHelper;
    }

    @Required
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }
}
