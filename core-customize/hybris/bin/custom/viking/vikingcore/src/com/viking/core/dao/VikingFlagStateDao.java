package com.viking.core.dao;

import com.viking.core.model.FlagStateModel;

import java.util.List;

/**
 * DAO to get Flagstates
 */
public interface VikingFlagStateDao {

    /**
     * Get all Flagstates
     * @return
     */
    List<FlagStateModel> getAll();
}
