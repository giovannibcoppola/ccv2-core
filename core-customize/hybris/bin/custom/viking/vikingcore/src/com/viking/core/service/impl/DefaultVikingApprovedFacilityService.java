package com.viking.core.service.impl;

import com.viking.core.dao.VikingApprovedFacilityDao;
import com.viking.core.model.ApprovedFacilityModel;
import com.viking.core.service.VikingApprovedFacilityService;
import com.viking.facades.approvedfacility.data.ApprovedFacilityData;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

public class DefaultVikingApprovedFacilityService implements VikingApprovedFacilityService {
    private VikingApprovedFacilityDao vikingApprovedFacilityDao;
    private ModelService modelService;
    private UserService userService;

    @Override
    public ApprovedFacilityModel getApprovedFacility() {

        B2BCustomerModel currentUser = (B2BCustomerModel) userService.getCurrentUser();
        Optional<ApprovedFacilityModel> approvedFacilityOptional = vikingApprovedFacilityDao.getApprovedFacilityForUser(currentUser);

        if (approvedFacilityOptional.isPresent()) {
            return approvedFacilityOptional.get();
        } else {
            final ApprovedFacilityModel facility = modelService.create(ApprovedFacilityModel.class);
            facility.setCustomer(currentUser);
            currentUser.setApprovedFacility(facility);
            modelService.saveAll(facility, currentUser);
            return facility;
        }
    }

    @Override
    public ApprovedFacilityModel setApprovedFacility(ApprovedFacilityData data) {

        final ApprovedFacilityModel approvedFacility = getApprovedFacility();
        B2BCustomerModel customer = (B2BCustomerModel) userService.getCurrentUser();
        approvedFacility.setCustomer(customer);
        approvedFacility.setName(data.getName());
        approvedFacility.setAddress1(data.getAddress1());
        approvedFacility.setAddress2(data.getAddress2());
        approvedFacility.setAddress3(data.getAddress3());
        approvedFacility.setPostalCode(data.getPostalCode());
        approvedFacility.setCity(data.getCity());
        approvedFacility.setCountry(data.getCountry());
        approvedFacility.setPhoneCountryCode(data.getPhoneCountryCode());
        approvedFacility.setPhone(data.getPhone());
        approvedFacility.setFaxCountryCode(data.getFaxCountryCode());
        approvedFacility.setFax(data.getFax());
        approvedFacility.setEmail(data.getPrimaryEmail());
        approvedFacility.setAdditionalEmail2(data.getAdditionalEmail2());
        approvedFacility.setAdditionalEmail3(data.getAdditionalEmail3());
        approvedFacility.setAdditionalEmail4(data.getAdditionalEmail4());
        approvedFacility.setAdditionalEmail5(data.getAdditionalEmail5());
        approvedFacility.setAdditionalEmail6(data.getAdditionalEmail6());
        approvedFacility.setWebsite(data.getWebsite());
        approvedFacility.setServiceStationManager(data.getServiceStationManager());
        approvedFacility.setServiceStationPhone(data.getServiceStationPhone());
        approvedFacility.setServiceStationEmail(data.getServiceStationEmail());
        approvedFacility.setAfterHoursPhone(data.getAfterHoursPhone());

        customer.setApprovedFacility(approvedFacility);
        modelService.saveAll(approvedFacility, customer);

        return approvedFacility;
    }

    @Required
    public void setVikingApprovedFacilityDao(VikingApprovedFacilityDao vikingApprovedFacilityDao) {
        this.vikingApprovedFacilityDao = vikingApprovedFacilityDao;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
