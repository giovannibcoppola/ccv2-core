package com.viking.core.event;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualsData;
import com.viking.core.model.EnquiryModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import java.util.List;
import java.util.Map;

public class BulletinReminderEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    private Map<ManualsData, List<BulletinsData>> manualsToEmail;

    /**
     * Default constructor
     */
    public BulletinReminderEvent() {
        super();
    }

    public Map<ManualsData, List<BulletinsData>> getManualsToEmail() {
        return manualsToEmail;
    }

    public void setManualsToEmail(Map<ManualsData, List<BulletinsData>> manualsToEmail) {
        this.manualsToEmail = manualsToEmail;
    }
}
