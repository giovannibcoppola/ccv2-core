package com.viking.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.CategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;

import java.util.Collection;


public interface VikingCategoryDao extends CategoryDao
{
	Collection<CategoryModel> findCategories(CatalogVersionModel catalogVersion);
	Collection<CategoryModel> findCategoriesAllowedForPrincipalGroupModels(CatalogVersionModel catalogVersion, Collection<PrincipalGroupModel> principalGroupModels);
}
