package com.viking.core.event;

import com.viking.core.model.ApprovedFacilityModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class UpdateProfileEvent extends AbstractCommerceUserEvent<BaseSiteModel> {
    private String b2BUnitId;
    private ApprovedFacilityModel approvedFacility;

    /**
     * Default constructor
     */
    public UpdateProfileEvent() {
        super();
    }

    /**
     * Parameterized Constructor
     *
     * @param token
     */
    public UpdateProfileEvent(final String token) {
        super();
    }

    public void setB2BUnitId(String b2BUnitId) {
        this.b2BUnitId = b2BUnitId;
    }

    public String getB2BUnitId() {
        return b2BUnitId;
    }

    public ApprovedFacilityModel getApprovedFacility() {
        return approvedFacility;
    }

    public void setApprovedFacility(ApprovedFacilityModel approvedFacility) {
        this.approvedFacility = approvedFacility;
    }
}
