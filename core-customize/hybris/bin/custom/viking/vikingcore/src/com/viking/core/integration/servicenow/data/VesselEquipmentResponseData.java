/**
 *
 */
package com.viking.core.integration.servicenow.data;

import java.util.List;


/**
 * @author Prabhakar
 *
 */
public class VesselEquipmentResponseData
{
	private List<VesselEquipmentData> equipments;
	private String imo;
	private String flagState;
	private String vesselName;
	private String assumedContrParty;
	private String customerNumberAssumedContrParty;
	private String callSignal;
	private String classedBy;

	/**
	 * @return the result
	 */
	public List<VesselEquipmentData> getEquipments()
	{
		return equipments;
	}

	/**
	 * @param equipments
	 *           the result to set
	 */
	public void setEquipments(final List<VesselEquipmentData> equipments)
	{
		this.equipments = equipments;
	}

	public String getImo() {
		return imo;
	}

	public void setImo(String imo) {
		this.imo = imo;
	}

	public String getFlagState() {
		return flagState;
	}

	public void setFlagState(String flagState) {
		this.flagState = flagState;
	}

	public String getAssumedContrParty() {
		return assumedContrParty;
	}

	public void setAssumedContrParty(String assumedContrParty) {
		this.assumedContrParty = assumedContrParty;
	}

	public String getCustomerNumberAssumedContrParty() {
		return customerNumberAssumedContrParty;
	}

	public void setCustomerNumberAssumedContrParty(String customerNumberAssumedContrParty) {
		this.customerNumberAssumedContrParty = customerNumberAssumedContrParty;
	}

	public String getCallSignal() {
		return callSignal;
	}

	public void setCallSignal(String callSignal) {
		this.callSignal = callSignal;
	}

	public String getClassedBy() {
		return classedBy;
	}

	public void setClassedBy(String classedBy) {
		this.classedBy = classedBy;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
}
