package com.viking.core.dao;

import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Optional;

/**
 * Viking dao for PointOfService
 */
public interface VikingPointOfServiceDao extends PointOfServiceDao {

    /**
     * Get PointOfService by display name. It is expected that display name is unique, even though no logic
     * is enforcing this.
     *
     * @param name
     * @return
     */
    Optional<PointOfServiceModel> getPosByDisplayName(String name);
}
