package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.sap.orderexchange.constants.PartnerCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.PartnerRoles;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultPartnerContributor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class VikingPartnerContributor extends DefaultPartnerContributor {

    private static final String ADDRESS_CHANGED = "addressChanged";
    @Autowired
    private EnquiryService enquiryService;
    @Autowired
    private VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService;

    @Override
    public List<Map<String, Object>> createRows(OrderModel order) {
        final List<Map<String, Object>> result = super.createRows(order);

        if (SiteChannel.B2B.equals(order.getSite().getChannel())) {
            result.forEach(map -> {
                map.put(PartnerCsvColumns.FIRST_NAME, order.getDeliveryAddress().getCompany());
                map.put(PartnerCsvColumns.LAST_NAME, StringUtils.EMPTY);
                map.put(PartnerCsvColumns.TITLE, StringUtils.EMPTY);
            });
        }

        if (!ObjectUtils.isEmpty(order.getEntries().get(0).getDeliveryPointOfService())) {
            Optional<Map<String, Object>> partnerRowOptional = result.stream()
                    .filter(row -> PartnerRoles.SHIP_TO.getCode().equals(row.get(PartnerCsvColumns.PARTNER_ROLE_CODE)))
                    .findAny();

            partnerRowOptional.ifPresent(row -> result.remove(row));

            final String b2cCustomer = getB2CCustomerHelper().determineB2CCustomer(order);
            final String sapcommon_Customer = b2cCustomer != null ? b2cCustomer : order.getStore().getSAPConfiguration()
                    .getSapcommon_referenceCustomer();

            final String deliveryAddressNumber = getShipToAddressNumber();
            Map<String, Object> row = mapAddressData(order, order.getEntries().get(0).getDeliveryPointOfService().getAddress());
            row.put(PartnerCsvColumns.PARTNER_ROLE_CODE, PartnerRoles.SHIP_TO.getCode());
            row.put(PartnerCsvColumns.PARTNER_CODE, sapcommon_Customer);
            row.put(PartnerCsvColumns.DOCUMENT_ADDRESS_ID, deliveryAddressNumber);
            getBatchIdAttributes().forEach(row::putIfAbsent);
            row.put("dh_batchId", order.getCode());
            result.add(row);
        }

        if (order instanceof VikingEnquiryOrderModel) {
            EnquiryReceiverEmailModel enquiryReceiver = vikingEnquiryReceiverEmailService.findEnquiryNotificationInfo(order);
            result.forEach(map -> {
                map.put(PartnerCsvColumns.PARTNER_CODE, enquiryReceiver.getCustomer());
            });
        }
        return result;
    }

    @Override
    public Set<String> getColumns() {
        Set<String> columns = super.getColumns();
        columns.add(VikingCoreConstants.INQUIRY);
        columns.add(ADDRESS_CHANGED);
        return columns;
    }

    @Override
    protected Map<String, Object> mapAddressData(final OrderModel order, final AddressModel address) {
        final Map<String, Object> row = super.mapAddressData(order, address);
        row.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));
        if (hasDefaultShippingAddress(order)) {
            row.put(ADDRESS_CHANGED, hasAddressChanged(address, ((B2BCustomerModel) order.getUser()).getDefaultB2BUnit().getShippingAddress()));
        } else{
            row.put(ADDRESS_CHANGED, true);
        }
        return row;
    }

    private Boolean hasAddressChanged(AddressModel address, AddressModel shippingAddress) {
        return address.getCountry() != shippingAddress.getCountry()
                || StringUtils.compare(address.getCompany(),shippingAddress.getCompany()) != 0
                || StringUtils.compare(address.getPostalcode(), shippingAddress.getPostalcode()) != 0
                || StringUtils.compare(address.getLine1(), shippingAddress.getLine1()) != 0
                || StringUtils.compare(address.getLine2(), shippingAddress.getLine2()) != 0
                || StringUtils.compare(address.getTown(), shippingAddress.getTown()) != 0
                || StringUtils.compare(address.getPhone1(), shippingAddress.getPhone1()) != 0;
    }

    private boolean hasDefaultShippingAddress(OrderModel order) {
        return order.getUser() instanceof B2BCustomerModel
                && ((B2BCustomerModel) order.getUser()).getDefaultB2BUnit() != null
                && ((B2BCustomerModel) order.getUser()).getDefaultB2BUnit().getShippingAddress() != null;
    }
}
