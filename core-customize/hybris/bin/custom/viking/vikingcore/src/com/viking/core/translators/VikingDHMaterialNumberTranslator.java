/*datahub customization for MATMAS product code 0 leading*/

package com.viking.core.translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.apache.commons.lang.StringUtils;


public class VikingDHMaterialNumberTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(String code, Item item) throws JaloInvalidParameterException {

        code= StringUtils.stripStart(code,"0");

        return code;

    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return null;
    }
}
