package com.viking.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;

public class SendNewsEmailEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    private PK cmsNewsItemPK;

    /**
     * Default constructor
     */
    public SendNewsEmailEvent() {
        super();
    }

    public PK getCmsNewsItemPK() {
        return cmsNewsItemPK;
    }

    public void setCmsNewsItemPK(PK cmsNewsItemPK) {
        this.cmsNewsItemPK = cmsNewsItemPK;
    }
}
