package com.viking.core.strategies.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.dao.RegionToBaseStoreDao;
import com.viking.core.model.RegionToBaseStoreModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.basecommerce.strategies.BaseStoreSelectorStrategy;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class VikingBaseStoreSelectorStrategy implements BaseStoreSelectorStrategy {
    private static final Logger LOG = Logger.getLogger(VikingBaseStoreSelectorStrategy.class);
    private static final String GLOBAL_BASESTORE = "webshop-global";
    private static final String PUBLIC_WEBSHOP_SITE_UID = "publicwebshop";
    private BaseSiteService baseSiteService;
    private SessionService sessionService;
    private RegionToBaseStoreDao regionToBaseStoreDao;

    public VikingBaseStoreSelectorStrategy() {
    }

    public BaseStoreModel getCurrentBaseStore() {
        BaseStoreModel result = null;
        BaseSiteModel currentSite = baseSiteService.getCurrentBaseSite();
        String regionIsocode = sessionService.getAttribute(VikingCoreConstants.REGION_ISOCODE);

        LOG.debug("Country region iso code: " + regionIsocode);

        if (currentSite != null && PUBLIC_WEBSHOP_SITE_UID.equalsIgnoreCase(currentSite.getUid())) {
            List<BaseStoreModel> storeModels = currentSite.getStores();

            if(StringUtils.isNotBlank(regionIsocode)) {
                try {
                    RegionToBaseStoreModel regionToBaseStoreModel = regionToBaseStoreDao.findRegionToBaseStoreByRegionIsocode(regionIsocode);
                    String BaseStoreUid = regionToBaseStoreModel.getBaseStore().getUid();

                    LOG.debug("BaseStoreUid: " + BaseStoreUid);

                    result = storeModels.stream().filter(e -> e.getUid().equals(BaseStoreUid)).findAny().orElse(null);
                } catch (Exception e){
                    LOG.debug("No match in region to basestore", e);
                    result = storeModels.stream().filter(ne -> ne.getUid().equals(GLOBAL_BASESTORE)).findAny().orElse(null);
                }
            } else {
                result = storeModels.stream().filter(e -> e.getUid().equals(GLOBAL_BASESTORE)).findAny().orElse(null);
            }
        } else if (currentSite != null) {
            List<BaseStoreModel> storeModels = currentSite.getStores();
            if (CollectionUtils.isNotEmpty(storeModels)) {
                result = storeModels.get(0);
            }
        }

        return result;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Required
    public void setRegionToBaseStoreDao(RegionToBaseStoreDao regionToBaseStoreDao) {
        this.regionToBaseStoreDao = regionToBaseStoreDao;
    }
}
