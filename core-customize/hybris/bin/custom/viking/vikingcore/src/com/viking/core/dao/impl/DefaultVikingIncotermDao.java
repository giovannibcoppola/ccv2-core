package com.viking.core.dao.impl;

import com.viking.core.dao.VikingIncotermDao;
import com.viking.core.model.VikingIncotermModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.internal.dao.SortParameters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultVikingIncotermDao extends DefaultGenericDao<VikingIncotermModel> implements VikingIncotermDao
{
    public DefaultVikingIncotermDao()
    {
        super(VikingIncotermModel._TYPECODE);
    }


    private DefaultVikingIncotermDao(final String typecode)
    {
        super(typecode);

    }


    @Override
    public List<VikingIncotermModel> findVikingIncotermForCode(final String code)
    {
        final SortParameters sortParameters = new SortParameters();
        sortParameters.addSortParameter(VikingIncotermModel.CREATIONTIME, SortParameters.SortOrder.DESCENDING);

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(VikingIncotermModel.CODE, code);

        return find(params, sortParameters);
    }


    @Override
    public List<VikingIncotermModel> findAllVikingIncoterms()
    {
        final SortParameters sortParameters = new SortParameters();
        sortParameters.addSortParameter(VikingIncotermModel.CODE, SortParameters.SortOrder.ASCENDING);

        return find(sortParameters);
    }




}
