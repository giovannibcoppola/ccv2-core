package com.viking.core.job;

import com.viking.core.dao.VikingProductDao;
import com.viking.core.event.VikingProductCreationEvent;
import com.viking.core.event.VikingProductUpdateEvent;
import com.viking.core.model.WorkFlowCreationCronJobModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;
import java.util.stream.Collectors;

public class WorkFlowCreationJobPerformable  extends AbstractJobPerformable<CronJobModel>
{
    private static final Logger LOG = Logger.getLogger(WorkFlowCreationJobPerformable.class);

    private EventService eventService;

    private TimeService timeService;

    private VikingProductDao vikingProductDao;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {
        final WorkFlowCreationCronJobModel workFlowCreationCronJob = (WorkFlowCreationCronJobModel) cronJob;

        final Date lastDataFetchTime = workFlowCreationCronJob.getLastDataFetchTime() != null ? workFlowCreationCronJob.getLastDataFetchTime() : getTimeService().getCurrentTime() ;

        final List<ProductModel> products =  vikingProductDao.findProductsWithModifiedTimeGreaterThan(lastDataFetchTime);

        workFlowCreationCronJob.setLastDataFetchTime(getTimeService().getCurrentTime());

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("products to consider for workflows by  %s",
                    products.stream().map(ProductModel::getCode).collect(Collectors.joining(", "))));
        }

        products.forEach(product -> triggerWorkFlows(product, lastDataFetchTime));
        modelService.save(workFlowCreationCronJob);
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private void triggerWorkFlows(ProductModel productModel, Date lastDataFetchTime) {
        if (lastDataFetchTime.before(productModel.getCreationtime())) {
            final VikingProductCreationEvent event = new VikingProductCreationEvent(productModel);
            getEventService().publishEvent(event);
        } else if (lastDataFetchTime.before(productModel.getSapFieldModifiedTime())) {
            final VikingProductUpdateEvent event = new VikingProductUpdateEvent(productModel);
            getEventService().publishEvent(event);
        }
    }

    protected EventService getEventService()
    {
        return eventService;
    }

    @Required
    public void setEventService(final EventService eventService)
    {
        this.eventService = eventService;
    }

    protected TimeService getTimeService()
    {
        return timeService;
    }

    @Required
    public void setTimeService(final TimeService timeService)
    {
        this.timeService = timeService;
    }


    public VikingProductDao getVikingProductDao() {
        return vikingProductDao;
    }

    public void setVikingProductDao(VikingProductDao vikingProductDao) {
        this.vikingProductDao = vikingProductDao;
    }

}
