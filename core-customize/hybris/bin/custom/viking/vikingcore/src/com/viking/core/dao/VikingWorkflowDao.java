package com.viking.core.dao;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.List;

/**
 * interface Viking Workflow Dao
 */
public interface VikingWorkflowDao {

    /**
     * find all workflow inprogress for product
     *
     * @param name
     * @param item
     * @return List<WorkflowActionModel>
     */
    List<WorkflowActionModel> findAllWorkflowActionInProgressForProduct(String name, ItemModel item);

}
