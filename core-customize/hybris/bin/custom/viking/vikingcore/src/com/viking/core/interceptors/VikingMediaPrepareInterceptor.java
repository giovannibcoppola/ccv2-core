package com.viking.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.commons.collections.CollectionUtils;
import de.hybris.platform.core.model.media.MediaModel;

/**
 * Prepare interceptor for {@link MediaModel}. It adds channels if not present from its MediaContainer
 *
 * @author chiranjit
 */
public class VikingMediaPrepareInterceptor implements PrepareInterceptor<MediaModel> {

    @Override
    public void onPrepare(MediaModel mediaModel, InterceptorContext interceptorContext) throws InterceptorException {
        if(!hasChannel(mediaModel) && mediaModel.getMediaContainer() != null && CollectionUtils.isNotEmpty(mediaModel.getMediaContainer().getChannels())) {
            mediaModel.setChannels(mediaModel.getMediaContainer().getChannels());
        }
    }

    private boolean hasChannel(MediaModel mediaModel) {
        return CollectionUtils.isNotEmpty(mediaModel.getChannels());
    }

}
