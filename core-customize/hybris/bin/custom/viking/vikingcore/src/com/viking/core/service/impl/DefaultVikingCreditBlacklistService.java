package com.viking.core.service.impl;

import com.viking.core.dao.VikingCreditBlacklistProductDao;
import com.viking.core.service.VikingCreditBlacklistService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingCreditBlacklistService implements VikingCreditBlacklistService {

    private VikingCreditBlacklistProductDao vikingCreditBlacklistProductDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getAllCreditBlacklistedProductCodes() {
        return vikingCreditBlacklistProductDao.getAllCreditBlacklistProductCodes();
    }

    @Required
    public void setVikingCreditBlacklistProductDao(VikingCreditBlacklistProductDao vikingCreditBlacklistProductDao) {
        this.vikingCreditBlacklistProductDao = vikingCreditBlacklistProductDao;
    }
}
