/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.core.constants;

/**
 * Global class for all VikingCore constants. You can add global constants for your extension into this class.
 */
public final class VikingCoreConstants extends GeneratedVikingCoreConstants {
    public static final String EXTENSIONNAME = "vikingcore";
    private VikingCoreConstants() {
        //empty
    }

    // implement here constants used by this extension
    public static final String VIKING = "VIKING";
    public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
    public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
    public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
    public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
    public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
    public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
    public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";
    public static final String DOWNLOADED_SAP_CONTENT_FOLDER = "downloadedContentFromSAP";
    public static final String AZURE_DOCUMENTS_FOLDER = "azureDocuments";
    public static final String SAP_STORE = "sap";
    public static final String REGION_ISOCODE = "region-isocode";
    public static final String YACHTING_ROOT_CATEGORY = "10002";
    public static final String ALL_PRODUCTS_CATEGORY_CODE = "allproducts";
    public static final String INQUIRY = "inquiry";
    public static final String CLASSIFICATION_STORE = "classification";
    public static final String ERP_CATEGORY_CODE = "ERP";

    public static final String DEFAULT_CATALOG_VERSION_NAME = "Staged";
    public static final String ONLINE_CATALOG_VERSION_NAME = "Online";
    public static final String DEFAULT_CATALOG = "Default";
    public static final String SERVICEPARTNERPORTAL_UID = "servicepartnerportal";
    public static final String SAFETY_SHOP_CATEGORY_CODE = "safetyShop";
    public static final String SAFETYSHOP_STORE = "safetyshop";
    public static final String SAFETYSHOP = "safetyshop";
    public static final String PARTNER_STORE = "partner";
    public static final String SERVICE_PARTNER_PORTAL_CATEGORY_CODE = "servicePartnerPortal";
    public static final String PARTNER_PRODUCT_CATALOG = "partnerProductCatalog";
    public static final String SAFETYSHOP_PRODUCT_CATALOG = "safetyshopProductCatalog";
    public static final String SDS_CATEGORY_CODE = "SDS";
    public static final String SDS_STORE = "sds";
    public static final String SDS = "sds";


}
