package com.viking.core.service;

import com.viking.core.model.VikingIncotermModel;

import java.util.List;

public interface VikingIncotermService {
    List<VikingIncotermModel> findVikingIncotermForCode(String code);

    List<VikingIncotermModel> findAllVikingIncoterms();
}
