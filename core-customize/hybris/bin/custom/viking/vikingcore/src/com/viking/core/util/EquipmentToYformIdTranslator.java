package com.viking.core.util;

import com.viking.core.dao.EquipmentToYFormDao;
import com.viking.core.model.EquipmentToYformModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

/**
 * Translates a yForm type to id
 */
public class EquipmentToYformIdTranslator {

    private static final Logger LOG = LogManager.getLogger(EquipmentToYformIdTranslator.class);

    private EquipmentToYFormDao equipmentToYFormDao;

    /**
     * Translates name to id based on map maintained in Backoffice.
     *
     * @param type to translate
     * @return the id for yForm
     */
    public String convertToId(final String type) {
        final EquipmentToYformModel equipmentToYform = equipmentToYFormDao.getByType(type);

        if (equipmentToYform == null) {
            LOG.debug("No EquipmentToYform found for type [" + type + "]");
            return StringUtils.EMPTY;
        }

        LOG.debug("TYPE " + type + "converted to yForm " + equipmentToYform.getId());
        return equipmentToYform.getId();
    }

    /**
     * Translates name to type based on map maintained in Backoffice.
     *
     * @param id to translate
     * @return the id for yForm
     */
    public String convertToYFormId(final String id) {
        final EquipmentToYformModel equipmentToYform = equipmentToYFormDao.getById(id);

        if (equipmentToYform == null) {
            LOG.debug("No EquipmentToYform found for id [" + id + "]");
            return StringUtils.EMPTY;
        }

        LOG.debug("ID " + id + "converted to yForm " + equipmentToYform.getYForm());
        return equipmentToYform.getYForm();

    }

    @Required
    public void setEquipmentToYFormDao(EquipmentToYFormDao equipmentToYFormDao) {
        this.equipmentToYFormDao = equipmentToYFormDao;
    }
}
