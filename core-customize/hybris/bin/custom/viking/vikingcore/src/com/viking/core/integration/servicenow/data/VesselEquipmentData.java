/**
 *
 */
package com.viking.core.integration.servicenow.data;

/**
 * @author Prabhakar
 *
 */
public class VesselEquipmentData
{
	private String serialNumber;
	private String quantity;
	private String bookedPort;
	private String eta;
	private String etd;
	private String serviceBooked;
	private String certificateStatus;
	private String actionRequired;
	private String nextInspection;
	private String lastNotificationNumber;
	private String equipment;
	private String modelCategory;
	private String equipmentNumber;
	private String equipmentDescription;
	private String ticketId;
	private String ticketState;
	private String notificationState;
	private String lastCertificate;
	private boolean partOfContract;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getBookedPort() {
		return bookedPort;
	}

	public void setBookedPort(String bookedPort) {
		this.bookedPort = bookedPort;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getServiceBooked() {
		return serviceBooked;
	}

	public void setServiceBooked(String serviceBooked) {
		this.serviceBooked = serviceBooked;
	}

	public String getCertificateStatus() {
		return certificateStatus;
	}

	public void setCertificateStatus(String certificateStatus) {
		this.certificateStatus = certificateStatus;
	}

	public String getActionRequired() {
		return actionRequired;
	}

	public void setActionRequired(String actionRequired) {
		this.actionRequired = actionRequired;
	}

	public String getNextInspection() {
		return nextInspection;
	}

	public void setNextInspection(String nextInspection) {
		this.nextInspection = nextInspection;
	}

	public String getLastNotificationNumber() {
		return lastNotificationNumber;
	}

	public void setLastNotificationNumber(String lastNotificationNumber) {
		this.lastNotificationNumber = lastNotificationNumber;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getModelCategory() {
		return modelCategory;
	}

	public void setModelCategory(String modelCategory) {
		this.modelCategory = modelCategory;
	}

	public String getEquipmentNumber() {
		return equipmentNumber;
	}

	public void setEquipmentNumber(String equipmentNumber) {
		this.equipmentNumber = equipmentNumber;
	}

	public String getEquipmentDescription() {
		return equipmentDescription;
	}

	public void setEquipmentDescription(String equipmentDescription) {
		this.equipmentDescription = equipmentDescription;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketState() {
		return ticketState;
	}

	public void setTicketState(String ticketState) {
		this.ticketState = ticketState;
	}

	public String getNotificationState() {
		return notificationState;
	}

	public void setNotificationState(String notificationState) {
		this.notificationState = notificationState;
	}

	public String getLastCertificate() {
		return lastCertificate;
	}

	public void setLastCertificate(String lastCertificate) {
		this.lastCertificate = lastCertificate;
	}

	public boolean isPartOfContract() {
		return partOfContract;
	}

	public void setPartOfContract(boolean partOfContract) {
		this.partOfContract = partOfContract;
	}
}
