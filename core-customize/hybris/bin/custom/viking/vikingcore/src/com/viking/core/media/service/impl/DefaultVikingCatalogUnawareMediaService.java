package com.viking.core.media.service.impl;

import com.viking.core.media.service.VikingCatalogUnawareMediaService;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.MediaDao;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Chiranjit Chakraborty
 */

public class DefaultVikingCatalogUnawareMediaService implements VikingCatalogUnawareMediaService {

    @Resource
    private MediaDao mediaDao;

    @Override
    public Optional<CatalogUnawareMediaModel> getCatalogUnawareMediaForCode(String code) {
        ServicesUtil.validateParameterNotNull(code, "code cannot be null");
        final List<MediaModel> result = mediaDao.findMediaByCode(code);
        if(CollectionUtils.isNotEmpty(result)) {
            final List<CatalogUnawareMediaModel> catalogUnawareMedias = result.stream()
                    .filter(media -> media instanceof CatalogUnawareMediaModel)
                    .map(media -> (CatalogUnawareMediaModel)media)
                    .collect(Collectors.toList());
            ServicesUtil.validateIfSingleResult(catalogUnawareMedias, "No CatalogUnawareMedia with code " + code + " can be found.", "More than one media with code " + code + " found.");
            return Optional.of(catalogUnawareMedias.get(0));
        }
        return Optional.empty();
    }
}
