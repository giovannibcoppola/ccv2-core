package com.viking.core.dao.impl;

import com.viking.core.dao.EquipmentToYFormDao;
import com.viking.core.model.EquipmentToYformModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class DefaultEquipmentToYFormDao extends AbstractItemDao implements EquipmentToYFormDao {

    private static final Logger LOG = LogManager.getLogger(DefaultEquipmentToYFormDao.class);

    private static final String ID_QUERY = "SELECT {" + EquipmentToYformModel.PK + "} FROM {" + EquipmentToYformModel._TYPECODE + "} " +
            "WHERE {" + EquipmentToYformModel.ID + "} = ?" + EquipmentToYformModel.ID;

    private static final String TYPE_QUERY = "SELECT {" + EquipmentToYformModel.PK + "} FROM {" + EquipmentToYformModel._TYPECODE + "} " +
            "WHERE {" + EquipmentToYformModel.TYPE + "} = ?" + EquipmentToYformModel.TYPE;

    @Override
    public EquipmentToYformModel getById(final String id) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(ID_QUERY);
        query.addQueryParameter(EquipmentToYformModel.ID, id);

        final SearchResult<EquipmentToYformModel> search = getFlexibleSearchService().search(query);
        final List<EquipmentToYformModel> result = search.getResult();

        LOG.debug("Found " + result.size() + " number of EquipmentToYFormModels for id " + id);

        if (CollectionUtils.isNotEmpty(result)) {
            return result.get(0);
        }

        return null;
    }

    @Override
    public EquipmentToYformModel getByType(final String type) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(TYPE_QUERY);
        query.addQueryParameter(EquipmentToYformModel.TYPE, type);

        final SearchResult<EquipmentToYformModel> search = getFlexibleSearchService().search(query);
        final List<EquipmentToYformModel> result = search.getResult();

        LOG.debug("Found " + result.size() + " number of EquipmentToYFormModels for id " + type);

        if (CollectionUtils.isNotEmpty(result)) {
            return result.get(0);
        }

        return null;
    }
}
