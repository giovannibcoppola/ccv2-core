package com.viking.core.job;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.dao.VikingManualBulletinsAcceptedDao;
import com.viking.core.event.BulletinReminderEvent;
import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualsData;
import com.viking.core.jalo.ManualBulletinsAccepted;
import com.viking.core.model.BulletinReminderCronJobModel;
import com.viking.core.service.VikingManualsService;
import com.viking.core.util.ManualsHelper;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class BulletinReminderJobPerformable extends AbstractJobPerformable<BulletinReminderCronJobModel> {

    private static final Logger LOG = LogManager.getLogger(BulletinReminderJobPerformable.class);

    private static final String PARTNER_BASESITE_ID = "servicepartnerportal";
    public static final String SERVICE_MANUALS_URL_PREFIX = "/service-manuals/";

    private VikingManualsService vikingManualsService;
    private VikingManualBulletinsAcceptedDao manualBulletinsAcceptedDao;
    private BaseSiteService baseSiteService;
    private EventService eventService;
    private ManualsHelper manualsHelper;
    private CMSAdminSiteService cmsAdminSiteService;

    @Override
    public PerformResult perform(BulletinReminderCronJobModel job) {
        final String manualId = job.getManualId();
        List<ManualsData> manuals = vikingManualsService.getManuals();

        if (StringUtils.isNotEmpty(manualId)) {
            final Optional<ManualsData> manualOptional = manuals.stream()
                    .filter(manualData -> manualId.equals(manualData.getId()))
                    .findAny();

            if (manualOptional.isPresent()) {
                manuals = Arrays.asList(manualOptional.get());
            } else {
                LOG.debug(MessageFormat.format("No manual found for id [{0}]", manualId));
                return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
            }
        }

        final List<B2BCustomerModel> serviceStations = getServiceStations();

        for (B2BCustomerModel serviceStation : serviceStations) {
            if (clearAbortRequestedIfNeeded(job)) {
                LOG.debug("Aborting BulletinReminderCronjob");
                return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
            }

            handleManuals(manuals, serviceStation);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private void handleManuals(final List<ManualsData> manuals, final B2BCustomerModel serviceStation) {
        LOG.debug(MessageFormat.format("Handle manuals for service station [{0}]", serviceStation.getUid()));

        final Map<ManualsData, List<BulletinsData>> manualsToEmail = new HashMap<>();

        for (ManualsData manual : manuals) {
            if (manualsHelper.isAccessDenied(SERVICE_MANUALS_URL_PREFIX + manual.getId(), serviceStation, cmsAdminSiteService.getSiteForId(VikingCoreConstants.SERVICEPARTNERPORTAL_UID))) {
                LOG.debug(MessageFormat.format("Service station [{0}] is not allowed to see manual [{1}]", serviceStation.getUid(), manual.getId()));
                continue;
            }

            final List<BulletinsData> bulletins = manual.getBulletins();

            updateBulletinsAccepted(bulletins, serviceStation, manual.getId());

            final List<BulletinsData> notAcceptedBulletins = bulletins.stream()
                    .filter(bulletinData -> !bulletinData.isAccepted())
                    .collect(Collectors.toList());

            LOG.debug(MessageFormat.format("Found [{0}] not accepted bulletins for manual [{1}] and service station [{2}]", notAcceptedBulletins.size(), manual.getId(), serviceStation.getUid()));

            if (CollectionUtils.isNotEmpty(notAcceptedBulletins)) {
                manualsToEmail.put(manual, notAcceptedBulletins);
            }
        }

        if (MapUtils.isNotEmpty(manualsToEmail)) {
            sendReminderEmail(serviceStation, manualsToEmail);
        }
    }

    private void updateBulletinsAccepted(final List<BulletinsData> bulletins, final B2BCustomerModel serviceStation, final String manualId) {
        bulletins.forEach(bulletin -> {
            final Long timeStamp = bulletin.getDate();
            bulletin.setViewDate(vikingManualsService.getViewDate(timeStamp));
            bulletin.setAccepted(vikingManualsService.isBulletinAccepted(bulletin, manualId, serviceStation.getUid()));
        });
    }

    private void sendReminderEmail(final B2BCustomerModel serviceStation, final Map<ManualsData, List<BulletinsData>> manualsToEmail) {
        final BulletinReminderEvent event = new BulletinReminderEvent();
        event.setSite(baseSiteService.getBaseSiteForUID(PARTNER_BASESITE_ID));
        event.setCustomer(serviceStation);
        event.setManualsToEmail(manualsToEmail);
        eventService.publishEvent(event);
    }

    private List<B2BCustomerModel> getServiceStations() {
        final B2BCustomerModel example = new B2BCustomerModel();
        example.setServicePartnerPortalUser(true);

        return flexibleSearchService.getModelsByExample(example);
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

    @Required
    public void setVikingManualsService(VikingManualsService vikingManualsService) {
        this.vikingManualsService = vikingManualsService;
    }

    @Required
    public void setManualBulletinsAcceptedDao(VikingManualBulletinsAcceptedDao manualBulletinsAcceptedDao) {
        this.manualBulletinsAcceptedDao = manualBulletinsAcceptedDao;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }

    @Required
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Required
    public void setManualsHelper(ManualsHelper manualsHelper) {
        this.manualsHelper = manualsHelper;
    }

    @Required
    public void setCmsAdminSiteService(CMSAdminSiteService cmsAdminSiteService) {
        this.cmsAdminSiteService = cmsAdminSiteService;
    }
}
