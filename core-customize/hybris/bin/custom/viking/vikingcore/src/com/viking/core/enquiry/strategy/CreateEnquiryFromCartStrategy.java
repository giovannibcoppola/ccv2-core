package com.viking.core.enquiry.strategy;

import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.CreateOrderFromCartStrategy;

public interface CreateEnquiryFromCartStrategy {

   VikingEnquiryOrderModel createEnquiryFromCart(CartModel cart) throws InvalidCartException;

}
