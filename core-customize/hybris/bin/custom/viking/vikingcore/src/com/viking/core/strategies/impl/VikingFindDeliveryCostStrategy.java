package com.viking.core.strategies.impl;

import com.viking.core.util.DeliveryCostHelper;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.order.strategies.calculation.impl.DefaultFindDeliveryCostStrategy;
import de.hybris.platform.util.PriceValue;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class VikingFindDeliveryCostStrategy extends DefaultFindDeliveryCostStrategy {

    private static final Logger LOG = LogManager.getLogger(VikingFindDeliveryCostStrategy.class);

    private DeliveryCostHelper deliveryCostHelper;

    @Override
    public PriceValue getDeliveryCost(final AbstractOrderModel order) {
        List<ZoneDeliveryModeValueModel> values = deliveryCostHelper.findZoneDeliveryModeValuesForOrder(order, order.getDeliveryMode());

        if (CollectionUtils.isNotEmpty(values) && values.get(0).getPercentage() != null && order.getDeliveryMode() != null) {
            LOG.debug("Using percentage to calculate delivery cost. Percentage is: " + values.get(0).getPercentage());
            return deliveryCostHelper.calculateDeliveryCostForPercentage(order, values.get(0), order.getDeliveryMode());
        } else {
            return super.getDeliveryCost(order);
        }
    }

    @Required
    public void setDeliveryCostHelper(DeliveryCostHelper deliveryCostHelper) {
        this.deliveryCostHelper = deliveryCostHelper;
    }
}
