package com.viking.core.event;

import com.viking.core.service.VikingWorkflowService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowItemAttachmentModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

/**
 * Class that shares common functionality for both ProductCreation and ProductUpdate workflow
 *
 * @param <T> extends AbstractEvent
 *
 * @author javier.gomez
 */
public abstract class AbstractVikingProductEventListener<T extends AbstractEvent> extends AbstractEventListener<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractVikingProductEventListener.class);

    private static final String PREFIX_PRODUCT_MANAGER_USERGROUP = "wfl_pm_";
    private static final String PREFIX_MARKETING_USERGROUP = "wfl_ma_";
    private static final String MANAGEMENT = "MANAGEMENT";
    private static final String MARKETING = "MARKETING";
    private static final List<String> VALID_CATEGORY_PREFIX_FOR_WORKFLOW = Arrays.asList("01", "0201", "0202", "03", "0401", "0402", "0403", "05", "11");
    private static final String VIKING_PR_UPDATE = "VIKING_PR_UPDATE";
    private static final String VIKING_PR_CREATION = "VIKING_PR_CREATION";
    private static final String WORKFLOW_NAME_SEPARATOR = "-";

    private WorkflowTemplateService workflowTemplateService;
    private WorkflowService workflowService;
    private UserService userService;
    private WorkflowProcessingService workflowProcessingService;
    private ModelService modelService;

    @Resource
    private VikingWorkflowService vikingWorkflowService;

    public boolean isWorkflowNeeded(ProductModel productModel) {
        if (StringUtils.isNotEmpty(getPrefixCategoryValidForGroup(productModel))) {
            return true;
        }

        return false;
    }

    private String getPrefixCategoryValidForGroup(ProductModel productModel) {
        if (CollectionUtils.isNotEmpty(productModel.getSupercategories())) {
            List<String> categoryCodes = productModel.getSupercategories().stream().map(CategoryModel::getCode).collect(Collectors.toList());
            for (String categoryCode : categoryCodes) {
                String prefix = getCategoryCodePrefixValidForWorkflow(categoryCode);
                if (StringUtils.isNotEmpty(prefix)) {
                    return prefix;
                }
            }
        }

        return null;
    }

    private String getCategoryCodePrefixValidForWorkflow(String categoryCode) {
        for (String validCategoryPrefix : VALID_CATEGORY_PREFIX_FOR_WORKFLOW) {
            if (categoryCode.startsWith(validCategoryPrefix)) {
                return validCategoryPrefix;
            }
        }

        return null;
    }

    public void triggerProductApprovalWorkflow(ProductModel productModel, String workflowCode) {
        WorkflowModel resultWorkflow = createAndStartWorkflow(productModel, workflowCode);
        if (resultWorkflow != null) {
            PrincipalModel productManagerGroup = getGroupFromProduct(productModel, PREFIX_PRODUCT_MANAGER_USERGROUP);
            PrincipalModel marketingGroup = getGroupFromProduct(productModel, PREFIX_MARKETING_USERGROUP);
            resultWorkflow.getActions().forEach(action -> {
                if (action.getName().toUpperCase().contains(MANAGEMENT)) {
                    action.setPrincipalAssigned(productManagerGroup);
                    getModelService().save(action);
                } else if (action.getName().toUpperCase().contains(MARKETING)) {
                    action.setPrincipalAssigned(marketingGroup);
                    getModelService().save(action);
                }
            });
            getWorkflowProcessingService().startWorkflow(resultWorkflow);
        }
    }

    private PrincipalModel getGroupFromProduct(ProductModel productModel, String prefixGroup) {
        String prefixCategoryForGroup = getPrefixCategoryValidForGroup(productModel);
        return getUserService().getUserGroupForUID(prefixGroup + prefixCategoryForGroup);
    }

    /**
     * Creates and starts a workflow with the given template for the given {@link ItemModel}.
     *
     * @param item                 the {@link ItemModel} for which to start the workflow
     * @param workflowTemplateCode template code of the workflow to be started
     * @return instance of created {@link WorkflowModel}
     */
    private WorkflowModel createAndStartWorkflow(final ItemModel item,
                                                 final String workflowTemplateCode) {
        WorkflowModel resultWorkflow = null;

        try {
            if (hasExistingWorkflow(item, workflowTemplateCode)) {
                return null;
            }
            final WorkflowTemplateModel workflowTemplate = getWorkflowTemplateService()
                    .getWorkflowTemplateForCode(workflowTemplateCode);
            resultWorkflow = getWorkflowService()
                    .createWorkflow(workflowTemplateCode, workflowTemplate, singletonList(item),
                            getUserService().getAdminUser());
            if(item instanceof ProductModel) {
                final String workflowName = resultWorkflow.getName() + WORKFLOW_NAME_SEPARATOR + ((ProductModel) item).getCode();
                resultWorkflow.setName(workflowName);
                modelService.save(resultWorkflow);
            }

        } catch (final UnknownIdentifierException | IllegalArgumentException e) //NOSONAR
        {
            LOGGER.error("No workflow template found. Cannot create workflow for code {}.", workflowTemplateCode);
        }

        return resultWorkflow;
    }

    private boolean hasExistingWorkflow(ItemModel item, String workflowTemplateCode) {
        List<WorkflowActionModel> workflowsAction = new ArrayList<WorkflowActionModel>();
        final List<WorkflowActionModel> allWorkflowActionInProgressForProduct = vikingWorkflowService.findAllWorkflowActionInProgressForProduct(workflowTemplateCode, item);
        if(CollectionUtils.isNotEmpty(allWorkflowActionInProgressForProduct)) {
            workflowsAction.addAll(allWorkflowActionInProgressForProduct);
        }
        if(VIKING_PR_UPDATE.equals(workflowTemplateCode)){
            final List<WorkflowActionModel> vikingPrCreation = vikingWorkflowService.findAllWorkflowActionInProgressForProduct(VIKING_PR_CREATION, item);
            if(CollectionUtils.isNotEmpty(vikingPrCreation)) {
                workflowsAction.addAll(vikingPrCreation);
            }
        }
        return CollectionUtils.isNotEmpty(workflowsAction);
    }


    protected WorkflowTemplateService getWorkflowTemplateService() {
        return workflowTemplateService;
    }

    @Required
    public void setWorkflowTemplateService(final WorkflowTemplateService workflowTemplateService) {
        this.workflowTemplateService = workflowTemplateService;
    }

    protected WorkflowService getWorkflowService() {
        return workflowService;
    }

    @Required
    public void setWorkflowService(final WorkflowService workflowService) {
        this.workflowService = workflowService;
    }

    protected UserService getUserService() {
        return userService;
    }

    @Required
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    protected WorkflowProcessingService getWorkflowProcessingService() {
        return workflowProcessingService;
    }

    @Required
    public void setWorkflowProcessingService(final WorkflowProcessingService workflowProcessingService) {
        this.workflowProcessingService = workflowProcessingService;
    }

    protected ModelService getModelService() {
        return modelService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
