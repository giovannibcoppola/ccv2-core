package com.viking.core.dao.impl;

import com.viking.core.dao.VikingCreditBlacklistProductDao;
import com.viking.core.model.CreditBlacklistProductModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;

import java.util.List;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class DefaultVikingCreditBlacklistProductDao extends AbstractItemDao implements VikingCreditBlacklistProductDao {

    private static final String ALL_BLACKLISTED_PRODUCT_CODES = "SELECT {" + CreditBlacklistProductModel.PK + "} FROM {" + CreditBlacklistProductModel._TYPECODE + "}";


    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getAllCreditBlacklistProductCodes() {
        final List<CreditBlacklistProductModel> result = getFlexibleSearchService().<CreditBlacklistProductModel>search(ALL_BLACKLISTED_PRODUCT_CODES).getResult();
        return result.stream().map(CreditBlacklistProductModel::getProductCode).collect(Collectors.toList());
    }
}
