package com.viking.core.event;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.PublishEventContext;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.Objects;

/**
 * Event to handle Viking Product Creation Workflow
 *
 * @author javier.gomez
 */
public class VikingProductCreationEvent extends AbstractEvent {

    private ProductModel productModel;

    public VikingProductCreationEvent() {

    }

    public VikingProductCreationEvent(ProductModel productModel) {
        this.productModel = productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public ProductModel getProductModel() {
        return productModel;
    }
}
