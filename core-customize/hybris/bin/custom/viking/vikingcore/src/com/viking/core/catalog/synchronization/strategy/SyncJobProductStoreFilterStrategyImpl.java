package com.viking.core.catalog.synchronization.strategy;

import com.viking.core.catalog.synchronization.VikingCatalogSynchronizationService;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SyncJobProductStoreFilterStrategyImpl implements SyncJobApplicableItemStrategy {

    private static final Logger LOG = Logger.getLogger(VikingCatalogSynchronizationService.class);

    private BaseStoreService baseStoreService;

    @Override
    public List<ItemModel> filterItems(final List<ItemModel> inputItems, final SyncItemJobModel syncItemJob) {
        final List<BaseStoreModel> baseStores = getApplicableBaseStores(syncItemJob);

        final List<ProductModel> productsToSkip = new ArrayList<>();
        for (ItemModel item : inputItems) {
            if (item instanceof ProductModel) {
                final ProductModel product = (ProductModel) item;
                if (CollectionUtils.isEmpty(product.getStoreList()) || !CollectionUtils.containsAny( product.getStoreList(), baseStores)) {
                    productsToSkip.add(product);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(productsToSkip)) {
            if (LOG.isInfoEnabled()) {
                final String baseStoreCodes = baseStores.stream()
                        .map(BaseStoreModel::getUid)
                        .collect(Collectors.joining(","));
                final String excludedProductCodes = productsToSkip.stream()
                        .map(ProductModel::getCode)
                        .collect(Collectors.joining(","));
                LOG.info("Skip products not linked to basestore(s): " + baseStoreCodes + ". The following product are skipped " + excludedProductCodes);
            }
            return ListUtils.subtract(inputItems, productsToSkip);
        } else {
            LOG.debug("No products found to skip");
        }
        return inputItems;
    }

    private List<BaseStoreModel> getApplicableBaseStores(final SyncItemJobModel syncItemJob) {
        return baseStoreService.getAllBaseStores().stream()
                .filter(store -> store.getCatalogs() != null)
                .filter(store -> store.getCatalogs().contains(syncItemJob.getTargetVersion().getCatalog()))
                .collect(Collectors.toUnmodifiableList());
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}
