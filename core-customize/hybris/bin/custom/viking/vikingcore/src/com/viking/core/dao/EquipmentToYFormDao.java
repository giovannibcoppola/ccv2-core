package com.viking.core.dao;

import com.viking.core.model.EquipmentToYformModel;

public interface EquipmentToYFormDao {

    EquipmentToYformModel getById(String id);

    EquipmentToYformModel getByType(String type);
}
