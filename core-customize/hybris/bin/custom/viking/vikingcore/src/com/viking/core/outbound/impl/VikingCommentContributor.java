package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.RawItemContributor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

public class VikingCommentContributor implements RawItemContributor<OrderModel> {

    private static final String COMMENT = "comment";
    private static final String COUNTER = "counter";
    private static final String TEXT_ID  = "textId ";
    public static final int COMMENT_MAX_LENGTH = 132;
    public static final String TEXT_ID_Z3 = "Z3";
    public static final String TEXT_ID_Z6 = "Z6";

    private Map<String, String> batchIdAttributes;

    @Autowired
    private EnquiryService enquiryService;

    @Override
    public Set<String> getColumns() {
        Set<String> columns = new HashSet<>(Arrays.asList(OrderCsvColumns.ORDER_ID, COMMENT, COUNTER,VikingCoreConstants.INQUIRY, TEXT_ID));
        columns.addAll(batchIdAttributes.keySet());
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel order) {
        final List<Map<String, Object>> result = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(order.getB2bcomments())) {
            int counter = 0;
            final String comment = order.getB2bcomments().iterator().next().getComment();

            final List<String> comments = splitComment(comment);
            for (final String SplitComment : comments) {
                counter = setDataInResult(order, result, counter, SplitComment, TEXT_ID_Z3);
            }
        }

        if(StringUtils.isNotEmpty(order.getDeliveryNote())) {
            int counter = 0;
            final List<String> deliveryNotes = splitComment(order.getDeliveryNote());
            for (final String deliveryNote : deliveryNotes) {
                counter = setDataInResult(order, result, counter, deliveryNote, TEXT_ID_Z6);
            }
        }

        return result;
    }

    private int setDataInResult(OrderModel order, List<Map<String, Object>> result, int counter, String data, String textIdZ6) {
        Map<String, Object> row = new HashMap<>();
        row.put(OrderCsvColumns.ORDER_ID, order.getCode());
        row.put(COMMENT, data);
        row.put(COUNTER, StringUtils.leftPad(Integer.toString(counter), 6, "0"));
        row.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));
        row.put(TEXT_ID, textIdZ6);
        result.add(row);
        counter++;
        return counter;
    }

    private List<String> splitComment(String comment) {
        List<String> comments = new ArrayList<>();

        int length = comment.length();
        for (int i = 0; i < length; i += COMMENT_MAX_LENGTH) {
            comments.add(comment.substring(i, Math.min(length, i + COMMENT_MAX_LENGTH)));
        }

        return comments;
    }

    @Required
    public void setBatchIdAttributes(Map<String, String> batchIdAttributes) {
        this.batchIdAttributes = batchIdAttributes;
    }
}
