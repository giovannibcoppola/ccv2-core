package com.viking.core.util;

import com.viking.core.strategies.impl.VikingFindDeliveryCostStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeValueModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import org.apache.commons.lang.math.IntRange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;
import java.util.stream.Collectors;

public class DeliveryCostHelper {

    private static final Logger LOG = LogManager.getLogger(DeliveryCostHelper.class);

    private FlexibleSearchService flexibleSearchService;

    public boolean isEligibleToDeliveryCost(AbstractOrderEntryModel entry) {
        final String prdha = entry.getProduct().getPRDHA();
        boolean isWithinRange = prdha != null && new IntRange(1101021000, 1101030000).containsInteger(Integer.parseInt(prdha));
        return !isWithinRange;
    }

    public List<ZoneDeliveryModeValueModel> findZoneDeliveryModeValuesForOrder(final AbstractOrderModel order, final DeliveryModeModel deliveryMode) {
        if (order.getDeliveryAddress() == null || deliveryMode == null) {
            LOG.warn("Could not find ZoneDeliveryModeValues for order [" + order.getCode() + "] due to missing delivery address or missing delivery mode!");
            return Collections.emptyList();
        }

        final CurrencyModel curr = order.getCurrency();
        final CountryModel country = order.getDeliveryAddress().getCountry();

        Map params = new HashMap();
        params.put("me", deliveryMode);
        params.put("curr", curr);
        params.put("country", country);
        params.put("amount", order.getSubtotal());

        String query = "SELECT {v." + ZoneDeliveryModeValueModel.PK + "} " + "FROM {" + ZoneDeliveryModeValueModel._TYPECODE + " AS v " + "JOIN ZoneCountryRelation AS z2cRel " + "ON {v." + "zone" + "}={z2cRel." + "source" + "} } " + "WHERE " + "{v." + "deliveryMode" + "} = ?me AND " + "{v." + "currency" + "} = ?curr AND " + "{v." + "minimum" + "} <= ?amount AND " + "{z2cRel." + "target" + "} = ?country " + "ORDER BY {v." + "minimum" + "} DESC ";
        return flexibleSearchService.search(query, params).getResult();
    }

    public PriceValue calculateDeliveryCostForPercentage(final AbstractOrderModel order, final ZoneDeliveryModeValueModel value, final DeliveryModeModel deliveryMode) {
        List<AbstractOrderEntryModel> entriesEligibleToDeliveryCost = order.getEntries().stream()
                .filter(entry -> isEligibleToDeliveryCost(entry))
                .collect(Collectors.toList());

        final List<Double> entryTotals = entriesEligibleToDeliveryCost.stream()
                .map(entry -> entry.getTotalPrice())
                .collect(Collectors.toList());

        final double entriesTax = entriesEligibleToDeliveryCost.stream()
                .map(entry -> entry.getTaxValues())
                .flatMap(Collection::stream)
                .map(tax -> tax.getAppliedValue())
                .mapToDouble(amount -> amount.doubleValue())
                .sum();

        final double totalOfEntriesWithDeliveryCost = entryTotals.stream()
                .mapToDouble(amount -> amount.doubleValue())
                .sum();

        final double percentage = value.getPercentage() / 100D;
        final double totalAmountForDeliveryCostCalculation = totalOfEntriesWithDeliveryCost + entriesTax;
        final double totalDeliveryCost = Math.round(percentage * totalAmountForDeliveryCostCalculation * 100D) / 100D;

        LOG.debug("Total of entries eligible to delivery: " + totalOfEntriesWithDeliveryCost + " and calculated delivery cost: " + totalDeliveryCost);

        return new PriceValue(order.getCurrency().getIsocode(), totalDeliveryCost, ((ZoneDeliveryModeModel) deliveryMode).getNet());
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
