package com.viking.core.event;

import com.viking.core.model.OrderNotificationForSalesRespProcessModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.springframework.beans.factory.annotation.Required;


/**
 * Listener for order confirmation events.
 */
public class OrderNotificationForSalesRespEventListener extends AbstractAcceleratorSiteEventListener<OrderNotificationForSalesRespEvent> {
    private ModelService modelService;
    private BusinessProcessService businessProcessService;
    private FlexibleSearchService flexibleSearchService;

    @Override
    protected void onSiteEvent(final OrderNotificationForSalesRespEvent event) {
        final OrderModel order = event.getProcess().getOrder();

        final OrderNotificationForSalesRespProcessModel process = (OrderNotificationForSalesRespProcessModel) businessProcessService.createProcess(
                "orderNotificationForSalesRespProcess-" + order.getCode() + "-" + System.currentTimeMillis(),
                "orderNotificationForSalesRespEmail");
        process.setOrder(order);
        process.setSite(order.getSite());
        process.setStore(order.getStore());
        process.setLanguage(order.getLanguage());
        modelService.save(process);
        businessProcessService.startProcess(process);
    }

    @Override
    protected SiteChannel getSiteChannelForEvent(final OrderNotificationForSalesRespEvent event) {
        final OrderModel order = event.getProcess().getOrder();
        ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
        final BaseSiteModel site = order.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
        return site.getChannel();
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }
}
