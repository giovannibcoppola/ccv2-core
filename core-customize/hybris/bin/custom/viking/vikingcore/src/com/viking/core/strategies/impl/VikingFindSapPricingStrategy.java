package com.viking.core.strategies.impl;

import com.viking.core.service.VikingPriceInformation;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserNetCheckingStrategy;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class VikingFindSapPricingStrategy extends AbstractBusinessService implements FindPriceStrategy,
        FindDiscountValuesStrategy, FindTaxValuesStrategy, UserNetCheckingStrategy {

    private PriceService priceService;

    @Override
    public List<DiscountValue> findDiscountValues(AbstractOrderEntryModel entry) throws CalculationException {
        return Collections.EMPTY_LIST;
    }

    @Override
    public List<DiscountValue> findDiscountValues(AbstractOrderModel order) throws CalculationException {
        return Collections.EMPTY_LIST;
    }

    @Override
    public PriceValue findBasePrice(AbstractOrderEntryModel entry) throws CalculationException {
        List<PriceInformation> priceInformations = getPriceService().getPriceInformationsForProduct(entry.getProduct());
        return priceInformations.isEmpty() ? new PriceValue("EUR", 0, true) : priceInformations.get(0).getPriceValue();
    }

    @Override
    public Collection<TaxValue> findTaxValues(AbstractOrderEntryModel entry) throws CalculationException {
        List<PriceInformation> priceInformations = getPriceService().getPriceInformationsForProduct(entry.getProduct());
        if(priceInformations != null && !priceInformations.isEmpty()) {
            VikingPriceInformation vikingPriceInformation = (VikingPriceInformation) priceInformations.get(0);
            if(vikingPriceInformation.getTax() != null) {
                return Collections.singletonList(vikingPriceInformation.getTax());
            }
        }
        return Collections.EMPTY_LIST;
    }

    public PriceService getPriceService() {
        return priceService;
    }

    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }

    @Override
    public boolean isNetUser(UserModel userModel) {
        return true;
    }
}
