/**
 *
 */
package com.viking.core.integration.servicenow.data;

import java.util.List;


/**
 * @author Prabhakar
 *
 */
public class VesselsResponseData
{
	private String assumedContrParty;
	private String customerNumberAssumedContrParty;
	private List<VesselData> vessels;

	/**
	 * @return the result
	 */
	public List<VesselData> getVessels()
	{
		return vessels;
	}

	/**
	 * @param vessels
	 *           the vessels to set
	 */
	public void setVessels(final List<VesselData> vessels)
	{
		this.vessels = vessels;
	}

	public String getAssumedContrParty() {
		return assumedContrParty;
	}

	public void setAssumedContrParty(String assumedContrParty) {
		this.assumedContrParty = assumedContrParty;
	}

	public String getCustomerNumberAssumedContrParty() {
		return customerNumberAssumedContrParty;
	}

	public void setCustomerNumberAssumedContrParty(String customerNumberAssumedContrParty) {
		this.customerNumberAssumedContrParty = customerNumberAssumedContrParty;
	}
}
