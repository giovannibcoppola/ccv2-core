package com.viking.core.attributehandlers;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.apache.commons.lang3.StringUtils;

public class VikingAddressLine1Attribute extends AbstractDynamicAttributeHandler<String, AddressModel> {

    private static final String STREET_AND_STREET_NUMBER_SEPARATOR = " ";

    @Override
    public String get(AddressModel addressModel) {
        if (addressModel == null) {
            throw new IllegalArgumentException("address model is required");
        } else {
            return addressModel.getStreetname() + STREET_AND_STREET_NUMBER_SEPARATOR + addressModel.getStreetnumber() ;
        }
    }

    @Override
    public void set(AddressModel addressModel, String value) {
        if (addressModel != null && StringUtils.isNotEmpty(value)) {

            final String[] strings = value.split(STREET_AND_STREET_NUMBER_SEPARATOR);
            if(strings.length > 1 && strings[strings.length - 1].matches(".*\\d.*")) {
                final String streetnumber = value.substring(value.lastIndexOf(STREET_AND_STREET_NUMBER_SEPARATOR) + 1);
                final String streetname = value.substring(0, value.lastIndexOf(STREET_AND_STREET_NUMBER_SEPARATOR));
                addressModel.setStreetname(streetname);
                addressModel.setStreetnumber(streetnumber);
            } else {
                addressModel.setStreetname(value);
            }
        }
    }
}
