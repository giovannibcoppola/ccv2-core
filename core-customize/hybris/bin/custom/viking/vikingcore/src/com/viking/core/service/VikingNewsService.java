package com.viking.core.service;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

/**
 * Service to handle news components
 */
public interface VikingNewsService {

    /**
     * Get latest news for specified BaseSite
     *
     * @param site BaseSite to use
     * @return
     */
    List<CMSMediaParagraphComponentModel> getLatestsNewsForSite(final BaseSiteModel site);


    /**
     * Is service station allowed to see the nes
     * @param news
     * @param user
     * @return
     */
    boolean isAccessAllowed(CMSMediaParagraphComponentModel news, UserModel user);

    /**
     * Find news for given uid and catalogversion
     * @param uid
     * @param catalogVersion
     * @return
     */
    CMSMediaParagraphComponentModel findNewsForCatalogVersion(String uid, CatalogVersionModel catalogVersion);
}
