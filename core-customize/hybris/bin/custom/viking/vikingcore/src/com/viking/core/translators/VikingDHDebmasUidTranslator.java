/*datahub customization for DEBMAS uid 0 leading*/

package com.viking.core.translators;

import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import org.apache.commons.lang.StringUtils;


public class VikingDHDebmasUidTranslator extends AbstractValueTranslator {

    @Override
    public Object importValue(String uid, Item item) throws JaloInvalidParameterException {

        uid= StringUtils.stripStart(uid,"0");

        return uid;

    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return null;
    }
}
