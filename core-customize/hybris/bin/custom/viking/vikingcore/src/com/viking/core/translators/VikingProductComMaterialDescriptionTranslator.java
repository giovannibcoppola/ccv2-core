/*datahub customization for MATMAS product code 0 leading*/

package com.viking.core.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Locale;


public class VikingProductComMaterialDescriptionTranslator extends AbstractValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingProductComMaterialDescriptionTranslator.class);

    private DefaultModelService modelService;

    @Override
    public Object importValue(String materialDescription, Item item) throws JaloInvalidParameterException {
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if(StringUtils.isNotEmpty(materialDescription)) {
                try {
                    final Product product = (Product)item;
                    final ProductModel productModel = modelService.get(product);
                    if(productModel != null && StringUtils.isNotEmpty(productModel.getName(Locale.ENGLISH))) {
                        return productModel.getName(Locale.ENGLISH);
                    }
                }catch (Exception e) {
                    return materialDescription;
                }
            }

        return materialDescription;

    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return null;
    }
}
