package com.viking.core.translators;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.event.VikingProductCreationEvent;
import com.viking.core.event.VikingProductUpdateEvent;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class VikingERPCategoryTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingERPCategoryTranslator.class);

    private static final String IGNORE = "ignore";

    private DefaultModelService modelService;
    private CatalogVersionService catalogVersionService;
    private CategoryService categoryService;

    @Override
    public void performImport(final String categoryCode, final Item processedItem) throws ImpExException {
        if (catalogVersionService == null) {
            catalogVersionService = (DefaultCatalogVersionService) Registry.getApplicationContext().getBean(
                    "catalogVersionService");
        }
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }
        if (categoryService == null) {
            categoryService = (DefaultCategoryService) Registry.getApplicationContext().getBean(
                    "categoryService");
        }

        if (StringUtils.isNotEmpty(categoryCode) && !categoryCode.contains(IGNORE)) {

            CatalogVersionModel catalogVersionModel = null;
            try {
                catalogVersionModel = catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME);
            } catch (Exception e) {
                LOG.error("classification system is not available: " + VikingCoreConstants.DEFAULT_CATALOG + " : " + VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME);
            }
            if (catalogVersionModel != null) {
                try {
                    attachProductToCategory(categoryCode, (Product) processedItem, catalogVersionModel);
                } catch (UnknownIdentifierException e) {
                    LOG.error("category not found in Default catalog with code: " + categoryCode + ". Creating new category.");
                    final String rootCategoryCode = categoryCode.length() < 2 ? categoryCode : categoryCode.substring(0, 2);
                    LOG.warn("category not found in Default catalog with code: " + categoryCode + ". Attaching to " + rootCategoryCode + " instead.");
                    try {
                        attachProductToCategory(rootCategoryCode, (Product) processedItem, catalogVersionModel);
                    } catch (UnknownIdentifierException inner) {
                        LOG.error("Root category code: " + rootCategoryCode + " Not available. Product was not attached!");
                    }

                } catch (AmbiguousIdentifierException e) {
                    LOG.error("multiple category  found in Default catalog with code: " + categoryCode, e);
                }
            }
        }
    }

    private void attachProductToCategory(String categoryCode, Product processedItem, CatalogVersionModel catalogVersionModel) {
        final CategoryModel categoryModel = categoryService.getCategoryForCode(catalogVersionModel, categoryCode);
        if (categoryModel != null) {
            setCategoryToProduct(processedItem, catalogVersionModel, categoryModel);
        }
    }

    private void setCategoryToProduct(Product processedItem, CatalogVersionModel classificationSystemVersionModel, CategoryModel categoryModel) {
        ProductModel productModel = (ProductModel) modelService.get(processedItem);
        final List<CategoryModel> superCategories = new ArrayList<>(productModel.getSupercategories());
        boolean isExisting = false;
        if (CollectionUtils.isNotEmpty(superCategories)) {
            isExisting = productModel.getSupercategories().stream()
                    .anyMatch(cat -> cat.equals(categoryModel));
        }
        if (!isExisting) {
            superCategories.add(categoryModel);
            productModel.setSupercategories(superCategories);
            modelService.save(productModel);
        }
    }

}
