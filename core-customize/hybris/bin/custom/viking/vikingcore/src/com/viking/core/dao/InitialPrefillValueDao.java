package com.viking.core.dao;

import com.viking.core.model.InitialPrefillValueModel;

import java.util.List;

public interface InitialPrefillValueDao {

    List<InitialPrefillValueModel> getAll();
}
