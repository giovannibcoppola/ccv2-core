package com.viking.core.enquiry.service.impl;

import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.enquiry.strategy.CreateEnquiryFromCartStrategy;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.store.services.BaseStoreService;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;

/**
 * {@inheritDoc}
 */
public class DefaultEnquiryService implements EnquiryService {

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private CreateEnquiryFromCartStrategy createEnquiryFromCartStrategy;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEnquiryFlow() {
        return baseStoreService.getCurrentBaseStore().isEnquiryFlow();
    }

    @Override
    public VikingEnquiryOrderModel createEnquiryFromCart(CartModel cart) throws InvalidCartException {
        return createEnquiryFromCartStrategy.createEnquiryFromCart(cart);
    }

    @Override
    public boolean isEnquiryOrder(OrderModel orderModel) {
        return orderModel instanceof VikingEnquiryOrderModel;
    }

}
