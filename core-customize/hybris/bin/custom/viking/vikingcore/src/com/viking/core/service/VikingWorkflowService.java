package com.viking.core.service;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import java.util.List;

/**
 * interface Viking Workflow Service
 */

public interface VikingWorkflowService {

    /**
     * find all workflow inprogress for product
     *
     * @param name
     * @param item
     * @return List<WorkflowActionModel>
     */

    List<WorkflowActionModel> findAllWorkflowActionInProgressForProduct(String name, ItemModel item);

}
