package com.viking.core.dao;

import com.viking.core.model.DateConversionKeyModel;

import java.util.List;

public interface DateConversionKeyDao {

    List<DateConversionKeyModel> getAll();
}
