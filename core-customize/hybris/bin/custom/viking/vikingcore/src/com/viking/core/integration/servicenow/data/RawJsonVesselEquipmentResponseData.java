package com.viking.core.integration.servicenow.data;

public class RawJsonVesselEquipmentResponseData {

    private VesselEquipmentResponseData result;

    public VesselEquipmentResponseData getResult() {
        return result;
    }

    public void setResult(VesselEquipmentResponseData result) {
        this.result = result;
    }
}
