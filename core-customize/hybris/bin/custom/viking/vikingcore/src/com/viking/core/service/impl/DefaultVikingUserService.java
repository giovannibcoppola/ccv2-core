package com.viking.core.service.impl;

import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.viking.core.service.VikingUserService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.core.jco.connection.JCoConnection;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.daos.UserGroupDao;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class DefaultVikingUserService extends DefaultUserService implements VikingUserService {
    private static final Logger LOG = Logger.getLogger(DefaultVikingUserService.class);

    private static final String ZCERT_RFC_HYBRIS = "ZCERT_RFC_HYBRIS";
    private static final String KUNRG = "KUNRG";
    private static final String AUTH_ = "AUTH_";
    private static final String AUTH_CODE_COLUMN_1 = "EQART";
    private static final String AUTH_CODE_COLUMN_2 = "EQART2";
    private static final String AUTH_CODE_COLUMN_3 = "EQART3";
    private static final String AUTH_CODE_COLUMN_4 = "EQART4";
    private static final String AUTH_CODE_COLUMN_5 = "EQART5";
    private static final String AUTH_CODE_COLUMN_6 = "EQART6";
    private static final String AUTH_RESULT_TABLE = "IT_ZCERT";
    private static final String CUSTOMER_GROUP_ID = "customergroup";

    @Autowired
    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    @Autowired
    private UserGroupDao userGroupDao;

    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<B2BCustomerModel> getServicePartnerCustomers() {
        final B2BCustomerModel example = new B2BCustomerModel();
        example.setServicePartnerPortalUser(true);

        return flexibleSearchService.getModelsByExample(example);
    }

    @Override
    public void updateCustomerAuthorizationGroupsFromSAP() {
        try {
            Set<PrincipalGroupModel> sapAuthGroups = getCustomerAuthorizationGroupsFromSAP();

            UserModel currentUser = getCurrentUser();

            if (currentUser == null) {
                LOG.debug("Current user was null");
                return;
            }

            Set<PrincipalGroupModel> currentGroups = currentUser.getAllGroups();

            List<PrincipalGroupModel> currentAuthGroups = currentGroups.stream()
                    .filter(group -> group.getUid().startsWith(AUTH_))
                    .collect(Collectors.toList());

            currentGroups.removeAll(currentAuthGroups);
            currentGroups.addAll(sapAuthGroups);
            currentUser.setGroups(currentGroups);

            getModelService().save(currentUser);
        } catch (Exception e) {
            LOG.error("Error during update of customer authorization groups", e);
        }
    }

    private Set<PrincipalGroupModel> getCustomerAuthorizationGroupsFromSAP() throws BackendException {
        final Set<PrincipalGroupModel> hybrisAuthGroups = new HashSet<>();

        final JCoTable resultTable = getResultTableFromSAP(AUTH_RESULT_TABLE);

        if (resultTable != null && !resultTable.isEmpty()) {
            LOG.debug("Authorization group from SAP result table IT_ZCERT row count: " + resultTable.getNumRows());
            LOG.debug("Authorization group from SAP result table IT_ZCERT column count: " + resultTable.getNumColumns());

            final Set<Integer> sapAuthGroups = new HashSet<>();
            for (int i = 0; i < resultTable.getNumRows(); i++) {
                resultTable.setRow(i);
                LOG.debug("Authorization group from SAP result table - analysing row: " + i);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_1);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_2);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_3);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_4);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_5);
                getSAPAuthGroup(resultTable, sapAuthGroups, AUTH_CODE_COLUMN_6);
            }

            for (final Integer sapAuthGroup : sapAuthGroups) {
                LOG.debug("SAP Authorization group raw: " + sapAuthGroup);
                final String userGroupId = AUTH_ + sapAuthGroup;
                UserGroupModel userGroup = userGroupDao.findUserGroupByUid(userGroupId);

                if (userGroup == null) {
                    LOG.debug("Authorization group from SAP system not found in Hybris. Will be created.");
                    userGroup = createUserGroup(sapAuthGroup, userGroupId);
                }

                hybrisAuthGroups.add(userGroup);
                LOG.debug("Authorization group model: " + userGroup.getUid());
            }
        }

        return hybrisAuthGroups;
    }

    private UserGroupModel createUserGroup(final Integer sapAuthGroup, final String userGroupId) {
        final UserGroupModel userGroupModel = getModelService().create(UserGroupModel.class);
        userGroupModel.setUid(userGroupId);
        userGroupModel.setName("Authorization Group " + sapAuthGroup);
        userGroupModel.setGroups(new HashSet<>(Arrays.asList(userGroupDao.findUserGroupByUid(CUSTOMER_GROUP_ID))));
        getModelService().save(userGroupModel);
        return userGroupModel;
    }

    private void getSAPAuthGroup(final JCoTable resultTable, final Set<Integer> authorizationGroups, final String column) {
        if (resultTable.getInt(column) > 0) {
            authorizationGroups.add(resultTable.getInt(column));
            LOG.debug("Found authorization group: " + resultTable.getInt(column));
        }
    }

    private JCoTable getResultTableFromSAP(String resultTable) throws BackendException {

        JCoConnection managedConnection = jCoManagedConnectionFactory.getManagedConnection("JCoStateless", "Hybris");

        final JCoFunction function = managedConnection.getFunction(ZCERT_RFC_HYBRIS);
        final String uid = ((B2BCustomerModel) getCurrentUser()).getDefaultB2BUnit().getUid();
        final String leftPaddeduid = StringUtils.leftPad(uid, 10, "0");
        function.getImportParameterList().setValue(KUNRG, leftPaddeduid);
        LOG.info("Executing ZCERT_RFC_HYBRIS function for customer id: " + uid + "[" + leftPaddeduid + "]");

        managedConnection.execute(function);

        return function.getTableParameterList().getTable(resultTable);

    }
}
