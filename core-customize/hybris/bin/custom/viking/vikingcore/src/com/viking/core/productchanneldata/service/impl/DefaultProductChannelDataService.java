package com.viking.core.productchanneldata.service.impl;

import com.viking.core.model.LongChannelTextModel;
import com.viking.core.model.SimpleChannelTextModel;
import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * DefaultProductChannelDataService overridden to be able to customize
 */
public class DefaultProductChannelDataService implements ProductChannelDataService {

    public static final String DEFAULT_CHANNEL_CODE = "DEFAULT";
    public static final String VIKING_SAFETY_SHOP_CHANNEL_CODE = "VIKINGSAFETYSHOP";
    public static final String SERVICE_PORTAL_CHANNEL_CODE = "SERVICE_PORTAL";
    public static final String CUSTOMER_PORTAL_CHANNEL_CODE = "CUSTOMER_PORTAL";
    public static final String SDS_PORTAL_CHANNEL_CODE = "SDS";

    public static final String SAFETY_SHOP_SITE_UID = "safetyshop";
    public static final String SERVICE_PARTNER_PORTAL_SITE_UID = "servicepartnerportal";
    public static final String CUSTOMER_PORTAL_SITE_UID = "customerportal";
    public static final String SDS_PORTAL_SITE_UID = "sds";

    @Resource(name = "cmsSiteService")
    private CMSSiteService cmsSiteService;

    @Override
    public List<MediaModel> getFilteredMediasForChannel(final List<MediaModel> mediaModelList) {
        List<MediaModel> sortedMediaModelList= mediaModelList.stream()
                .sorted(Comparator.comparingInt(MediaModel::getOrder))
                .collect(Collectors.toList());

        return getFilteredMediasForChannel(sortedMediaModelList, getChannelCodeBySite());
    }

    @Override
    public List<MediaContainerModel> getFilteredMediaContainersForChannel(final List<MediaContainerModel> mediaContainerModelList) {
        List<MediaContainerModel> sortedMediaContainerModelList= mediaContainerModelList.stream()
                .sorted(Comparator.comparingInt(MediaContainerModel::getOrder))
                .collect(Collectors.toList());

        return getFilteredMediaContainersForChannel(sortedMediaContainerModelList, getChannelCodeBySite());
    }

    @Override
    public Optional<String> getTeaserForChannel(ProductModel productModel) {
        Optional<String> teaserText = Optional.empty();
        if (CollectionUtils.isNotEmpty(productModel.getTeasers())) {
            final String channelCode = getChannelCodeBySite();
            teaserText = productModel.getTeasers().stream()
                    .filter(teaser -> teaser.getChannels().stream().anyMatch(channel -> channelCode.equals(channel.getCode())))
                    .map(SimpleChannelTextModel::getText)
                    .findFirst();
        }
        if (teaserText.isEmpty() && StringUtils.isNotEmpty(productModel.getDefaultTeaser())) {
            return Optional.of(productModel.getDefaultTeaser());
        } else {
            return teaserText;
        }
    }

    @Override
    public Optional<String> getLongDescriptionForChannel(ProductModel productModel) {
        Optional<String> longText = Optional.empty();
        if (CollectionUtils.isNotEmpty(productModel.getLongDescriptions())) {
            String channelCode = getChannelCodeBySite();
            longText = productModel.getLongDescriptions().stream()
                    .filter(longDescription -> longDescription.getChannels().stream().anyMatch(channel -> channelCode.equals(channel.getCode())))
                    .map(LongChannelTextModel::getLongText)
                    .findFirst();
        }
        if (longText.isEmpty() && StringUtils.isNotEmpty(productModel.getDefaultLongDescription())) {
            return Optional.of(productModel.getDefaultLongDescription());
        } else {
            return longText;
        }
    }

    @Override
    public Collection<SimpleChannelTextModel> getFilteredTeasersForChannel(Collection<SimpleChannelTextModel> simpleChannelTextModelList) {
        if(CollectionUtils.isNotEmpty(simpleChannelTextModelList)) {
            String channelCode = getChannelCodeBySite();
            return getFilteredTeasersForChannel(simpleChannelTextModelList, channelCode);
        }

        return new ArrayList<>();
    }

    @Override
    public Collection<LongChannelTextModel> getFilteredLongDescriptionsForChannel(Collection<LongChannelTextModel> longChannelTextModelList) {
        if(CollectionUtils.isNotEmpty(longChannelTextModelList)) {
            String channelCode = getChannelCodeBySite();
            return getFilteredLongDescriptionsForChannel(longChannelTextModelList, channelCode);
        }

        return new ArrayList<>();
    }

    private String getChannelCodeBySite() {
        String channelCode;
        final String currentSiteUid = cmsSiteService.getCurrentSite().getUid();
        switch (currentSiteUid) {
            case SAFETY_SHOP_SITE_UID:
                channelCode = VIKING_SAFETY_SHOP_CHANNEL_CODE;
                break;
            case SERVICE_PARTNER_PORTAL_SITE_UID:
                channelCode = SERVICE_PORTAL_CHANNEL_CODE;
                break;
            case CUSTOMER_PORTAL_SITE_UID:
                channelCode = CUSTOMER_PORTAL_CHANNEL_CODE;
                break;
            case SDS_PORTAL_SITE_UID:
                channelCode = SDS_PORTAL_CHANNEL_CODE;
                break;
            default:
                channelCode = DEFAULT_CHANNEL_CODE;
                break;
        }
        return channelCode;
    }

    private Collection<LongChannelTextModel> getFilteredLongDescriptionsForChannel(Collection<LongChannelTextModel> longChannelTextModelList, String channelCode) {
        if (CollectionUtils.isNotEmpty(longChannelTextModelList)) {
            final Collection<LongChannelTextModel> longChannelTextModels = new ArrayList<>(getLongDescriptionsForChannelCode(longChannelTextModelList, channelCode));
            if (!DEFAULT_CHANNEL_CODE.equals(channelCode)) {
                longChannelTextModels.addAll(getLongDescriptionsForChannelCode(longChannelTextModelList, DEFAULT_CHANNEL_CODE));
            }
            return longChannelTextModels;
        }
        return Collections.emptyList();
    }

    private List<LongChannelTextModel> getLongDescriptionsForChannelCode(Collection<LongChannelTextModel> longChannelTextModels, String channelCode) {
        return longChannelTextModels.stream().filter(
                longChannelTextModel -> longChannelTextModel.getChannels()
                        .stream()
                        .anyMatch(channel -> channelCode.equals(channel.getCode())))
                .collect(Collectors.toList());
    }

    private Collection<SimpleChannelTextModel> getFilteredTeasersForChannel(Collection<SimpleChannelTextModel> simpleChannelTextModelList, String channelCode) {
        if (CollectionUtils.isNotEmpty(simpleChannelTextModelList)) {
            final Collection<SimpleChannelTextModel> simpleChannelTextModels = new ArrayList<>(getTeasersForChannelCode(simpleChannelTextModelList, channelCode));
            if (!DEFAULT_CHANNEL_CODE.equals(channelCode)) {
                simpleChannelTextModels.addAll(getTeasersForChannelCode(simpleChannelTextModelList, DEFAULT_CHANNEL_CODE));
            }
            return simpleChannelTextModels;
        }
        return Collections.emptyList();
    }

    private List<SimpleChannelTextModel> getTeasersForChannelCode(Collection<SimpleChannelTextModel> simpleChannelTextModelList, String channelCode) {
        return simpleChannelTextModelList.stream().filter(
                simpleChannelTextModel -> simpleChannelTextModel.getChannels()
                        .stream()
                        .anyMatch(channel -> channelCode.equals(channel.getCode())))
                .collect(Collectors.toList());
    }

    private List<MediaModel> getFilteredMediasForChannel(List<MediaModel> mediaModelList, String channelCode) {
        if (CollectionUtils.isNotEmpty(mediaModelList)) {
            final List<MediaModel> mediaModels = new ArrayList<>(getMediasForChannelCode(mediaModelList, channelCode));
            if (!DEFAULT_CHANNEL_CODE.equals(channelCode)) {
                mediaModels.addAll(getMediasForChannelCode(mediaModelList, DEFAULT_CHANNEL_CODE));
            }
            return mediaModels;
        }
        return Collections.emptyList();
    }

    private List<MediaModel> getMediasForChannelCode(List<MediaModel> mediaModelList, String channelCode) {
        return mediaModelList.stream().filter(
                mediaModel -> mediaModel.getChannels()
                        .stream()
                        .anyMatch(channel -> channelCode.equals(channel.getCode())))
                .collect(Collectors.toList());
    }

    private List<MediaContainerModel> getFilteredMediaContainersForChannel(List<MediaContainerModel> mediaContainerModelList, String channelCode) {
        if (CollectionUtils.isNotEmpty(mediaContainerModelList)) {
            final List<MediaContainerModel> mediaContainerModels = new ArrayList<>(getMediacontainersForChannelCode(mediaContainerModelList, channelCode));
            if (!DEFAULT_CHANNEL_CODE.equals(channelCode)) {
                mediaContainerModels.addAll(getMediacontainersForChannelCode(mediaContainerModelList, DEFAULT_CHANNEL_CODE));
            }
            return mediaContainerModels;
        }
        return Collections.emptyList();
    }

    private List<MediaContainerModel> getMediacontainersForChannelCode(List<MediaContainerModel> mediaContainerModelList, String channelCode) {
        return mediaContainerModelList.stream().filter(
                mediaContainerModel -> mediaContainerModel.getChannels()
                        .stream()
                        .anyMatch(channel -> channelCode.equals(channel.getCode())))
                .collect(Collectors.toList());
    }

}
