package com.viking.core.util;

import com.viking.core.dao.DateConversionKeyDao;
import com.viking.core.model.DateConversionKeyModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.StringReader;
import java.util.*;

/**
 * Helper for yForm XML.
 */
public class YFormXMLhelper {

    private final static Logger LOG = LogManager.getLogger(YFormXMLhelper.class);

    private static final String CAUSE_OF_CONDEMNATION = "cause.of.condemnation";

    private DateConversionKeyDao dateConversionKeyDao;
    private ConfigurationService configurationService;

    public Map<String, Map<String, String>> transformNotificationYFormXML(final String input) throws XMLStreamException {

        final Map<String, Map<String, String>> transformationResult = new HashMap<>();

        final XMLStreamReader reader = getXmlStreamReader(input);

        boolean exceptionOccured = false;

        while (reader.hasNext()) {
            if (!exceptionOccured) {
                reader.next();
            }
            exceptionOccured = false;

            try {
                if (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
                    final String elementName = reader.getLocalName();
                    String elementText = reader.getElementText();

                    // Do not put element to result if elementName contains "control" - it is an auto generated name in yform and should never be
                    // published to SAP, since it causes function module to fail.
                    if (elementName.contains("-") && StringUtils.isNotEmpty(elementText) && !StringUtils.containsIgnoreCase(elementName, "control")) {
                        final int index = elementName.lastIndexOf("-");

                        final Map<String, String> row = new HashMap<>();
                        row.put("FEGRP", elementName.substring(0, index));
                        row.put("FECOD", elementName.substring(index + 1));

                        if (isDateConversionRequired(elementName.substring(0, index) + "-" + elementName.substring(index + 1))) {
                            elementText = formatDate(elementText);
                        }

                        row.put("FETXT", elementText);

                        transformationResult.put(elementName, row);
                    }
                }
            } catch (XMLStreamException exception) {
                LOG.debug("Exception happened when parsing yForm XML to SAP structure", exception);
                exceptionOccured = true;
            }
        }

        return transformationResult;
    }

    private boolean isDateConversionRequired(String key) {
        List<DateConversionKeyModel> dateConversions = dateConversionKeyDao.getAll();

        for (DateConversionKeyModel dateConversionKey : dateConversions) {
            if (dateConversionKey.getKey().equalsIgnoreCase(key)) {
                return true;
            }
        }

        return false;
    }

    private String formatDate(final String dateString) {
        // If the date string matches format 0001-01-01 we need to flip it to match 01-01-0001
        if (dateString.matches("^\\d{4}\\-\\d{2}\\-\\d{2}$")) {
            final String[] strings = dateString.split("-");
            return strings[2] + "-" + strings[1] + "-" + strings[0];
        } else {
            return dateString;
        }
    }

    public Map<String, String> transformCondemnationXML(final String input) throws XMLStreamException {

        final Map<String, String> transformationResult = new HashMap<>();

        final XMLStreamReader reader = getXmlStreamReader(input);

        boolean exceptionOccured = false;

        while (reader.hasNext()) {
            if (!exceptionOccured) {
                reader.next();
            }
            exceptionOccured = false;

            try {
                if (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
                    final String elementName = reader.getLocalName();
                    final String elementText = reader.getElementText();

                    final Map<String, String> nameMap = buildNameMap();

                    final String key = nameMap.get(elementName);
                    if (StringUtils.isNotEmpty(key)) {
                        if ("Z_CONDEMNATION2".equals(key) && isOtherCauseOfCondemnation(elementText)) {
                            transformationResult.put(key, "OTHER");
                            transformationResult.put("Z_OTHER", elementText.length() < 30 ? elementText : elementText.substring(0, 30));
                        } else {
                            transformationResult.put(key, elementText);
                        }
                    }
                }
            } catch (XMLStreamException exception) {
                LOG.debug("Exception happened when parsing condemnation XML to SAP structure", exception);
                exceptionOccured = true;
            }
        }

        return transformationResult;
    }

    private boolean isOtherCauseOfCondemnation(final String elementText) {
        // if Z_CONDEMNATION2 is OTHER, then put elementText into Z_OTHER
        // its not known if OTHER was selected, so we need to match against the possible values
        // to see if elementText matches. If no match, then value must be other.
        final String condemantionCauses = configurationService.getConfiguration().getString(CAUSE_OF_CONDEMNATION);

        final List<String> condemantionCausesList = new ArrayList<>();
        condemantionCausesList.addAll(Arrays.asList(condemantionCauses.split(",")));

        return !condemantionCausesList.contains(elementText);
    }

    private Map<String, String> buildNameMap() {
        final Map<String, String> map = new HashMap<>();
        map.put("control-97", "Z_TYPEOFSHIP"); //Type of vessel the equipment belong to
        map.put("control-87", "Z_CONDEMNATION2"); //Cause of condemnation2
        map.put("Z_CONDEMNATION1", "Z_CONDEMNATION1"); //Cause of condemnation1
        map.put("CERT-0160", "Z_CONDEMNCOMMENTS1");
        map.put("CERT-0170", "Z_CONDEMNCOMMENTS2");
        map.put("CERT-0180", "Z_CONDEMNCOMMENTS3");
        map.put("CERT-0010", "Z_CONDEMNBY2"); //Service station
        map.put("NEW-ADM1-0310", "Z_CONDEMNBY"); //Technician
        map.put("NEW-ADM1-0210", "Z_CONDEMNDATE");
        map.put("control-98", "I_STATUS");
        map.put("NEW-ADM1-0040", "I_EQUNR");

        return map;
    }

    private XMLStreamReader getXmlStreamReader(String input) throws XMLStreamException {
        final XMLInputFactory factory = XMLInputFactory.newInstance();
        return factory.createXMLStreamReader(new StringReader(input));
    }

    @Required
    public void setDateConversionKeyDao(DateConversionKeyDao dateConversionKeyDao) {
        this.dateConversionKeyDao = dateConversionKeyDao;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }
}
