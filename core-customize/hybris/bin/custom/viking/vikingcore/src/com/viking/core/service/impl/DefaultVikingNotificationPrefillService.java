package com.viking.core.service.impl;

import com.viking.core.dao.impl.DefaultVikingNotificationPrefillDao;
import com.viking.core.jalo.NotificationPrefill;
import com.viking.core.service.VikingNotificationPrefillService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class DefaultVikingNotificationPrefillService implements VikingNotificationPrefillService {
    private DefaultVikingNotificationPrefillDao notificationPrefillDao;

    @Required
    public void setNotificationPrefillDao(final DefaultVikingNotificationPrefillDao notificationPrefillDao) {
        this.notificationPrefillDao = notificationPrefillDao;
    }

    @Override
    public List<NotificationPrefill> getNotifications(String notificationId) {
        return notificationPrefillDao.getNotifications(notificationId);
    }
}
