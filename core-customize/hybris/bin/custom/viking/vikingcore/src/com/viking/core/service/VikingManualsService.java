package com.viking.core.service;

import com.viking.core.integration.azure.BulletinsData;
import com.viking.core.integration.azure.ManualData;
import com.viking.core.integration.azure.ManualsData;
import com.viking.core.integration.azure.VesselDocsData;
import com.viking.core.jalo.ManualBulletinsAccepted;

import java.io.IOException;
import java.util.List;

public interface VikingManualsService {
    List<ManualsData> getManuals();

    ManualData getManual(String id);

    List<BulletinsData> getBulletins(String id);

    List<ManualBulletinsAccepted> getManualBulletinsAccepted(String bulletinsId, String manualId, String userId );

    void setManualBulletinsAccepted(String bulletinsId, String manualsId, String userId) throws  IOException;

    String getViewDate(long timestamp);

    String download(String url, String legacy, boolean extensionReq, String filename);

    boolean isBulletinAccepted(final BulletinsData bulletin, final String manualsId, final String uid);

    VesselDocsData getVesselDocuments(String imo);
}
