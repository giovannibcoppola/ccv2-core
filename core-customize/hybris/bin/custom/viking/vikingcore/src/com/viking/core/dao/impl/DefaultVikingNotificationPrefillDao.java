package com.viking.core.dao.impl;

import com.viking.core.dao.VikingNotificationPrefillDao;
import com.viking.core.jalo.NotificationPrefill;
import com.viking.core.model.NotificationPrefillModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultVikingNotificationPrefillDao extends AbstractItemDao implements VikingNotificationPrefillDao {

    private static final String GET_NOTIFICATION_QUERY = "select {" + NotificationPrefillModel.PK + "} from {"
            + NotificationPrefillModel._TYPECODE + " }  where {" + NotificationPrefillModel.NOTIFICATIONID + "}=?notificationId";

    @Override
    public List<NotificationPrefill> getNotifications(String notification) {
        final Map<String, Object> queryParameters = new HashMap<String, Object>();
        queryParameters.put("notificationId", notification);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_NOTIFICATION_QUERY, queryParameters);
        final SearchResult<NotificationPrefill> searchRes = getFlexibleSearchService().search(query);
        return searchRes.getResult();
    }
}
