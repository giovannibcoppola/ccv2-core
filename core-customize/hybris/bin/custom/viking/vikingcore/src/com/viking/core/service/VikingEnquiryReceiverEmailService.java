package com.viking.core.service;

import com.viking.core.model.EnquiryReceiverEmailModel;
import de.hybris.platform.core.model.order.OrderModel;


/**
 * The interface Viking enquiry receiver email service.
 */
public interface VikingEnquiryReceiverEmailService {
    /**
     * Find enquiry notification info enquiry receiver email model.
     *
     * @param enquiry the enquiry
     * @return the enquiry receiver email model
     */
    EnquiryReceiverEmailModel findEnquiryNotificationInfo(OrderModel enquiry);
}
