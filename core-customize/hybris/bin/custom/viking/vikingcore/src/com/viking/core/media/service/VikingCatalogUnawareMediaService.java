package com.viking.core.media.service;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;

import java.util.Optional;

/**
 * @author Chiranjit Chakraborty
 */
public interface VikingCatalogUnawareMediaService {

    public Optional<CatalogUnawareMediaModel> getCatalogUnawareMediaForCode(final String code);
}
