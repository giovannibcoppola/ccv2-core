package com.viking.core.util;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.media.service.VikingCatalogUnawareMediaService;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Optional;

public class ContentHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ContentHelper.class);
    public static final String APPLICATION_PDF = "application/pdf";

    private ModelService modelService;
    private MediaService mediaService;
    private VikingCatalogUnawareMediaService vikingCatalogUnawareMediaService;


    public Optional<CatalogUnawareMediaModel> downloadContentToMedia(final String code, String contentUrl) {

        if (StringUtils.isBlank(contentUrl)) {
            LOG.debug("Received empty url in content helper");
            return Optional.empty();
        }

        try {
            LOG.debug("[URL - " + contentUrl + " ][Code - " + code + "]");

            contentUrl = getCorrectedContentUrl(contentUrl);

            Optional<CatalogUnawareMediaModel> existingMedia = findExistingCatalogUnawareMediaForContent(code);

            CatalogUnawareMediaModel media;
            if (existingMedia.isPresent()) {
                media = (CatalogUnawareMediaModel) existingMedia.get();
            } else {
                 media = createCatalogUnawareMediaModel(code);
            }
            final URL url = new URL(contentUrl);
            final InputStream input = url.openStream();
            mediaService.setStreamForMedia(media, input);
            modelService.save(media);
            return Optional.of(media);
        } catch (IOException exception) {
            LOG.debug("Not able to download content", exception);
            return Optional.empty();
        }
    }

    private String getCorrectedContentUrl(String contentUrl) {
        if (contentUrl.contains(APPLICATION_PDF)) {
            LOG.debug("Removing application/pdf from content url");

            contentUrl = contentUrl.replace(APPLICATION_PDF, "");
        }
        return contentUrl;
    }

    /**
     * Checks if a certain Media already exists on Hybris, else it returns Optional.empty
     * @param code
     * @return CatalogUnawareMediaModel
     */
    public Optional<CatalogUnawareMediaModel> checkIfMediaExistsOrReturnEmpty(final String code) {
        return findExistingCatalogUnawareMediaForContent(code);
    }

    /**
     * Download Content To Media If Not Existing Or Else Return Existing. It does not updates the media if present with the new content
     * @param code
     * @param contentUrl
     * @return CatalogUnawareMediaModel
     */
    public Optional<CatalogUnawareMediaModel> downloadContentToMediaIfNotExisting(final String code, String contentUrl) {

        if (StringUtils.isBlank(contentUrl)) {
            LOG.debug("Received empty url in content helper");
            return Optional.empty();
        }

        try {
            LOG.debug("[URL - " + contentUrl + " ][Code - " + code + "]");

            final Optional<CatalogUnawareMediaModel> existingMedia = findExistingCatalogUnawareMediaForContent(code);

            if (existingMedia.isPresent()) {
               return existingMedia;
            } else {
                contentUrl = getCorrectedContentUrl(contentUrl);

                CatalogUnawareMediaModel media = createCatalogUnawareMediaModel(code);
                final URL url = new URL(contentUrl);
                final InputStream input = url.openStream();
                mediaService.setStreamForMedia(media, input);
                modelService.save(media);
                return Optional.of(media);
            }
        } catch (IOException exception) {
            LOG.debug("Not able to download content", exception);
            return Optional.empty();
        }
    }

    private CatalogUnawareMediaModel createCatalogUnawareMediaModel(String code) {
        CatalogUnawareMediaModel media = modelService.create(CatalogUnawareMediaModel.class);
        media.setCode(code);
        media.setRealFileName(code);
        media.setMime(APPLICATION_PDF);
        media.setFolder(mediaService.getFolder(VikingCoreConstants.DOWNLOADED_SAP_CONTENT_FOLDER));
        modelService.save(media);
        return media;
    }

    public Optional<CatalogUnawareMediaModel> findExistingCatalogUnawareMediaForContent(final String code) {
        try {
            return vikingCatalogUnawareMediaService.getCatalogUnawareMediaForCode(code);
        } catch (UnknownIdentifierException | AmbiguousIdentifierException exception) {
            LOG.debug(MessageFormat.format("Zero or more media found with code: {0}", code), exception);
            return Optional.empty();
        }
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }

    public void setVikingCatalogUnawareMediaService(VikingCatalogUnawareMediaService vikingCatalogUnawareMediaService) {
        this.vikingCatalogUnawareMediaService = vikingCatalogUnawareMediaService;
    }
}
