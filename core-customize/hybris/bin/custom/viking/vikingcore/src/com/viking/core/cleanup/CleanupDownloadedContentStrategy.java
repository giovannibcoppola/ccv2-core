package com.viking.core.cleanup;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.servicelayer.internal.model.MaintenanceCleanupJobModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CleanupDownloadedContentStrategy implements MaintenanceCleanupStrategy<CatalogUnawareMediaModel, CronJobModel> {

    private static final Logger LOG = LoggerFactory.getLogger(CleanupDownloadedContentStrategy.class);

    private static final String QUERY = "SELECT {" + CatalogUnawareMediaModel.PK + "} FROM {" + CatalogUnawareMediaModel._TYPECODE + "}" +
            " WHERE {" + CatalogUnawareMediaModel.FOLDER + "} = ?" + CatalogUnawareMediaModel.FOLDER +
            " AND {" + CatalogUnawareMediaModel.MODIFIEDTIME + "} < ?" + CatalogUnawareMediaModel.MODIFIEDTIME;

    private ModelService modelService;
    private MediaService mediaService;

    @Override
    public FlexibleSearchQuery createFetchQuery(CronJobModel cronjob) {
        if (cronjob.getJob() instanceof MaintenanceCleanupJobModel) {
            final MaintenanceCleanupJobModel job = (MaintenanceCleanupJobModel) cronjob.getJob();

            final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY);

            query.addQueryParameter(CatalogUnawareMediaModel.FOLDER, mediaService.getFolder(VikingCoreConstants.DOWNLOADED_SAP_CONTENT_FOLDER));
            final LocalDate localDate = LocalDate.now().minusDays(job.getThreshold());
            query.addQueryParameter(CatalogUnawareMediaModel.MODIFIEDTIME, Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            query.setResultClassList(Arrays.asList(CatalogUnawareMediaModel.class));
            return query;
        }

        return null;
    }

    @Override
    public void process(List<CatalogUnawareMediaModel> elements) {
        LOG.debug(MessageFormat.format("Removing {0} CatalogUnawareMedias", elements.size()));
        modelService.removeAll(elements);
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }
}
