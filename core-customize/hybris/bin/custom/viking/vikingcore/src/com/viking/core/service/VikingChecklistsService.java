package com.viking.core.service;

import com.viking.core.integration.azure.ChecklistsData;

import java.io.IOException;
import java.util.List;

public interface VikingChecklistsService {
    ChecklistsData getChecklist(String id);
}
