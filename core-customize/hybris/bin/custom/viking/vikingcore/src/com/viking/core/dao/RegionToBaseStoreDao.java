package com.viking.core.dao;

import com.viking.core.model.RegionToBaseStoreModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

public interface RegionToBaseStoreDao {
    RegionToBaseStoreModel findRegionToBaseStoreByRegionIsocode(String isocode);

    List<RegionToBaseStoreModel> findAllRegionToBaseStore();

    /**
     * Find the RegionToBaseStoreModel from the provided basestore
     * @param baseStore
     * @return
     */
    Optional<RegionToBaseStoreModel> findRegionToBaseStoreByBaseStore(final BaseStoreModel baseStore);
}
