package com.viking.core.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * The type Viking street number translator.
 *
 * @author amine.elharrak
 */
public class VikingStreetNumberTranslator extends AbstractSpecialValueTranslator {

    public static final String IGNORED_TAG = "<ignore>";
    private static final Logger LOG = Logger.getLogger(VikingStreetNumberTranslator.class);
    private DefaultModelService modelService;

    @Override
    public void performImport(final String streetNumber, final Item processedItem) throws ImpExException {
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }
        boolean IsStreetNumberIgnored = IGNORED_TAG.equalsIgnoreCase(streetNumber);
        if (IsStreetNumberIgnored) {
            try {
                final Address address = (Address) processedItem;
                final AddressModel addressModel = modelService.get(address);
                addressModel.setStreetnumber(StringUtils.EMPTY);
                modelService.save(addressModel);
            } catch (Exception e) {
                LOG.error("Could not set  Street number, the address is : " + processedItem);
            }
        }
    }

}
