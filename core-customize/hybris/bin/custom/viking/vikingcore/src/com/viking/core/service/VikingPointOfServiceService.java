package com.viking.core.service;

import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import java.util.Optional;

/**
 * Viking service for PointOfService
 */
public interface VikingPointOfServiceService extends PointOfServiceService {

    /**
     * Find the point of service based on display name. It is expected that display name is unique even though no logic
     * is present to enforce this.
     *
     * @param name
     * @return
     */
    Optional<PointOfServiceModel> getPointOfServiceByDisplayName(String name);
}
