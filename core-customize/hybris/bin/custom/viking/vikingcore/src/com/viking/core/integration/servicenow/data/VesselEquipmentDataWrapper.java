package com.viking.core.integration.servicenow.data;


import java.util.ArrayList;
import java.util.List;

public class VesselEquipmentDataWrapper {
    private String imo;
    private String vesselName;
    private String flagState;

    private List<VesselEquipmentData> equipmentDataList = new ArrayList<>();

    public List<VesselEquipmentData> getEquipmentDataList() {
        return equipmentDataList;
    }

    public void setEquipmentDataList(List<VesselEquipmentData> equipmentDataList) {
        this.equipmentDataList = equipmentDataList;
    }

    public String getImo() {
        return imo;
    }

    public void setImo(String imo) {
        this.imo = imo;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getFlagState() {
        return flagState;
    }

    public void setFlagState(String flagState) {
        this.flagState = flagState;
    }
}
