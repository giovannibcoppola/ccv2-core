package com.viking.core.listeners;

import com.viking.core.event.SendNewsEmailEvent;
import com.viking.core.model.CMSMediaParagraphComponentModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CMSMediaParagraphComponentListener implements AfterSaveListener {

    private static final String PARTNER_BASESITE_ID = "servicepartnerportal";
    public static final String ONLINE = "Online";

    private ModelService modelService;
    private EventService eventService;
    private BaseSiteService baseSiteService;

    @Override
    public void afterSave(Collection<AfterSaveEvent> collection) {

        Collection<CMSMediaParagraphComponentModel> components = getValidComponents(collection) ;

        if(!components.isEmpty())
        {
            for(CMSMediaParagraphComponentModel componentModel : components)
            {
                if (isValidToTriggerNewsEvent(componentModel)) {
                    // Trigger email event
                    final SendNewsEmailEvent event = new SendNewsEmailEvent();
                    event.setSite(baseSiteService.getBaseSiteForUID(PARTNER_BASESITE_ID));
                    event.setCmsNewsItemPK(componentModel.getPk());
                    eventService.publishEvent(event);

                    componentModel.setSendEmail(Boolean.FALSE);
                    modelService.save(componentModel);
                }
            }
        }

    }
    private boolean isValidToTriggerNewsEvent(CMSMediaParagraphComponentModel model) {
        return hasOnlineCatalogVersion(model) && hasSendEmailChangedToTrue(model);
    }

    private boolean hasOnlineCatalogVersion(CMSMediaParagraphComponentModel model) {
        return model.getCatalogVersion() != null && ONLINE.equalsIgnoreCase(model.getCatalogVersion().getVersion());
    }

    private boolean hasSendEmailChangedToTrue(CMSMediaParagraphComponentModel model) {
        return Boolean.TRUE.equals(model.getSendEmail());
    }

    private Collection<CMSMediaParagraphComponentModel> getValidComponents(Collection<AfterSaveEvent> collection) {

        List<CMSMediaParagraphComponentModel> results = new ArrayList<>();

        collection.forEach(event -> {
            if(modelService.get(event.getPk()) instanceof CMSMediaParagraphComponentModel)
            {
                results.add(modelService.get(event.getPk())) ;
            }
        });

        return results ;
    }


    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
