package com.viking.core.service;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

/**
 * Service to handle invoices
 */
public interface VikingInvoiceService {

    /**
     * Get invoice url from SAP
     * @param documentId documentId
     */
    String getInvoiceUrl(String documentId);
}
