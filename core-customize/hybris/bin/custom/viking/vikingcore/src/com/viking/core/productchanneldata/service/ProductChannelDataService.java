package com.viking.core.productchanneldata.service;

import com.viking.core.model.LongChannelTextModel;
import com.viking.core.model.SimpleChannelTextModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Interface for methods of product channel specific
 */
public interface ProductChannelDataService {

    /**
     * Method that filters the medias for the current site
     *
     * @param mediaModelList List of MediaModel
     * @return The filtered medias valid for the current site
     */
    List<MediaModel> getFilteredMediasForChannel(final List<MediaModel> mediaModelList);

    /**
     * Method that filters the media containers for the current site
     *
     * @param mediaContainerModelList List of MediaContainerModel
     * @return The filtered medias valid for the current site
     */
    List<MediaContainerModel> getFilteredMediaContainersForChannel(final List<MediaContainerModel> mediaContainerModelList);

    /**
     * Method that returns the first valid teaser for the given product on the current site
     *
     * @param productModel ProductModel
     * @return the first valid teaser for the given product on the current site
     */
    Optional<String> getTeaserForChannel(ProductModel productModel);

    /**
     * Method that returns the first valid longDescription for the given product on the current site
     *
     * @param productModel ProductModel
     * @return the first valid longDescription for the given product on the current site
     */
    Optional<String> getLongDescriptionForChannel(ProductModel productModel);


    /**
     * Method that filters the teasers for the current site
     *
     * @param simpleChannelTextModelList List of SimpleChannelTextModel
     * @return The filtered SimpleChannelTextModel instances valid for the current site
     */
    Collection<SimpleChannelTextModel> getFilteredTeasersForChannel(final Collection<SimpleChannelTextModel> simpleChannelTextModelList);

    /**
     * Method that filters the long descriptions for the current site
     *
     * @param longChannelTextModelList List of LongChannelTextModel
     * @return The filtered LongChannelTextModel instances valid for the current site
     */
    Collection<LongChannelTextModel> getFilteredLongDescriptionsForChannel(final Collection<LongChannelTextModel> longChannelTextModelList);
}
