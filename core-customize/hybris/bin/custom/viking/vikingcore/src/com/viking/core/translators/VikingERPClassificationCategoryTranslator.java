package com.viking.core.translators;

import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.impl.DefaultClassificationClassesResolverStrategy;
import de.hybris.platform.classification.impl.DefaultClassificationSystemService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.sap.sapmodel.jalo.ERPVariantProduct;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.servicelayer.type.impl.DefaultTypeService;
import de.hybris.platform.variants.model.VariantTypeModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class VikingERPClassificationCategoryTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingERPClassificationCategoryTranslator.class);

    protected static final String CATALOG_ID = "ERPClassification";
    protected static final String CATALOG_VERSION_NAME = "1.0";
    public static final String VARIANT_PRODUCT = "ERPVariantProduct";
    private static final String IGNORE = "ignore";

    private DefaultModelService modelService;
    private DefaultClassificationSystemService defaultClassificationSystemService;
    private DefaultClassificationClassesResolverStrategy defaultClassificationClassesResolverStrategy;

    @Override
    public void performImport(final String categoryCode, final Item processedItem) throws ImpExException {
        if (defaultClassificationClassesResolverStrategy == null) {
            defaultClassificationClassesResolverStrategy = (DefaultClassificationClassesResolverStrategy) Registry.getApplicationContext().getBean(
                    "defaultClassificationClassesResolverStrategy");
        }
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if (defaultClassificationSystemService == null) {
            defaultClassificationSystemService = (DefaultClassificationSystemService) Registry.getApplicationContext().getBean(
                    "defaultClassificationSystemService");
        }

        if(StringUtils.isNotEmpty(categoryCode) && !categoryCode.contains(IGNORE)) {
            ClassificationSystemVersionModel catalogVersionModel = null;
            try {
                catalogVersionModel = defaultClassificationSystemService.getSystemVersion(CATALOG_ID, CATALOG_VERSION_NAME);
            } catch (Exception e) {
                LOG.error("classification system is not available: "+ CATALOG_ID + " : "+ CATALOG_VERSION_NAME);
            }
            if(catalogVersionModel != null) {
                try {
                    final ClassificationClassModel categoryModel = defaultClassificationSystemService.getClassForCode(catalogVersionModel, categoryCode);
                    if( categoryModel != null) {
                        setCategoryToProduct((Product) processedItem, catalogVersionModel, categoryModel);
                    }
                }catch (UnknownIdentifierException e) {
                    LOG.error("category not found in Default catalog with code: "+ categoryCode + ". Creating new category.");
                    final ClassificationClassModel newCategory = modelService.create(ClassificationClassModel.class);
                    newCategory.setCode(categoryCode);
                    newCategory.setCatalogVersion(catalogVersionModel);
                    newCategory.setName(categoryCode);
                    modelService.save(newCategory);
                    setCategoryToProduct((Product)processedItem, catalogVersionModel, newCategory);
                } catch (AmbiguousIdentifierException e) {
                    LOG.error("multiple category  found in Default catalog with code: "+ categoryCode, e);
                }
            }
        }
    }

    private void setCategoryToProduct(Product processedItem, ClassificationSystemVersionModel classificationSystemVersionModel, ClassificationClassModel categoryModel) {
        ProductModel erpVariantProductModel = (ProductModel) modelService.get(processedItem);
        final List<CategoryModel> superCategories = new ArrayList<>(erpVariantProductModel.getSupercategories());
        if(CollectionUtils.isNotEmpty(superCategories)) {
            final List<CategoryModel> classificationCategories = erpVariantProductModel.getSupercategories().stream()
                    .filter(cat -> (cat instanceof ClassificationClassModel && cat.getCatalogVersion().equals(classificationSystemVersionModel)))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(classificationCategories)) {
                superCategories.removeAll(classificationCategories);
            }
        }
        superCategories.add(categoryModel);

        erpVariantProductModel.setSupercategories(superCategories);
        modelService.save(erpVariantProductModel);
    }


}
