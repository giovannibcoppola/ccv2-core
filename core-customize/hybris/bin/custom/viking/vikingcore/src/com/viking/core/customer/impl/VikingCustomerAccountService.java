package com.viking.core.customer.impl;

import com.sap.hybris.sapcustomerb2c.outbound.DefaultB2CSapCustomerAccountService;
import de.hybris.platform.b2bacceleratorservices.customer.B2BCustomerAccountService;
import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.util.Assert;

import java.util.Date;

public class VikingCustomerAccountService extends DefaultB2CSapCustomerAccountService {

    @Override
    public void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException
    {
        Assert.hasText(token, "The field [token] cannot be empty");
        Assert.hasText(newPassword, "The field [newPassword] cannot be empty");

        final SecureToken data = getSecureTokenService().decryptData(token);
        if (getTokenValiditySeconds() > 0L)
        {
            final long delta = new Date().getTime() - data.getTimeStamp();
            if (delta / 1000 > getTokenValiditySeconds())
            {
                throw new IllegalArgumentException("token expired");
            }
        }

        final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);
        if (customer == null)
        {
            throw new IllegalArgumentException("user for token not found");
        }
        if (!token.equals(customer.getToken()))
        {
            throw new TokenInvalidatedException();
        }
        customer.setToken(null);
        customer.setLoginDisabled(false);
        getModelService().save(customer);

        // Changed to get password encoding from the customer instead of the default password encoding.
        getUserService().setPassword(data.getData(), newPassword, customer.getPasswordEncoding());
    }
}
