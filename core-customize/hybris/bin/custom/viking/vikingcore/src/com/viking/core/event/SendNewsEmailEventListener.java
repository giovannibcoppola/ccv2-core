package com.viking.core.event;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import com.viking.core.model.NewsEmailProcessModel;
import com.viking.core.service.VikingNewsService;
import com.viking.core.service.VikingUserService;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

public class SendNewsEmailEventListener extends AbstractAcceleratorSiteEventListener<SendNewsEmailEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;
    private CommonI18NService commonI18NService;
    private BaseStoreService baseStoreService;
    private VikingUserService vikingUserService;
    private VikingNewsService vikingNewsService;

    @Override
    protected SiteChannel getSiteChannelForEvent(final SendNewsEmailEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }

    @Override
    protected void onSiteEvent(final SendNewsEmailEvent event) {
        // Get service stations
        final List<B2BCustomerModel> serviceStations = vikingUserService.getServicePartnerCustomers();
        final List<B2BCustomerModel> relevantStations = new ArrayList<>();

        // Filter service stations based on restrictions
        final CMSMediaParagraphComponentModel newsToSend = modelService.get(event.getCmsNewsItemPK());
        serviceStations.forEach(station -> {
            if (vikingNewsService.isAccessAllowed(newsToSend, station)) {
                relevantStations.add(station);
            }
        });

        // Create process for each relevant service station
        relevantStations.forEach(station -> {
            final NewsEmailProcessModel process = businessProcessService.createProcess(
                    "NewsEmailProcess-" + station.getUid() + "-" + System.currentTimeMillis(),
                    "newsEmailProcess");
            process.setSite(event.getSite());
            process.setStore(baseStoreService.getBaseStoreForUid("partner"));
            process.setLanguage(commonI18NService.getCurrentLanguage());
            process.setCustomer(station);
            process.setNewsToSend(newsToSend);
            modelService.save(process);

            // Start process
            businessProcessService.startProcess(process);
        });
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Required
    public void setCommonI18NService(final CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }

    @Required
    public void setVikingNewsService(VikingNewsService vikingNewsService) {
        this.vikingNewsService = vikingNewsService;
    }

    @Required
    public void setVikingUserService(VikingUserService vikingUserService) {
        this.vikingUserService = vikingUserService;
    }
}
