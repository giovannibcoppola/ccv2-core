/**
 *
 */
package com.viking.core.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.viking.core.dao.VikingVesselDao;
import com.viking.core.model.VikingVesselModel;


/**
 * @author Prabhakar
 *
 */
public class DefaultVikingVesselDao extends AbstractItemDao implements VikingVesselDao
{

	private static final String GET_ALL_VESSELS_QUERY = "select {v:" + VikingVesselModel.PK + "} from {"
			+ VikingVesselModel._TYPECODE + " as v} where {v:" + VikingVesselModel.PK
			+ "} in ({{select {rel:source} from {VikingVessel2UserRelation as rel JOIN " + UserModel._TYPECODE
			+ " as u ON {rel:target}= {u:" + UserModel.PK + "}} where {u:" + UserModel.UID + "}=?uid}})";

	@Override
	public List<VikingVesselModel> getAssignedVesselsByUserId(final String userId)
	{
		final Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("uid", userId);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_VESSELS_QUERY, queryParameters);
		final SearchResult<VikingVesselModel> searchRes = search(query);
		return searchRes.getResult();
	}

}
