package com.viking.core.dao.impl;

import com.viking.core.dao.InitialPrefillValueDao;
import com.viking.core.model.InitialPrefillValueModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public class DefaultInitialPrefillValueDao implements InitialPrefillValueDao {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<InitialPrefillValueModel> getAll() {
        SearchResult<InitialPrefillValueModel> searchResult = flexibleSearchService.search("SELECT {" + InitialPrefillValueModel.PK + "} FROM {" + InitialPrefillValueModel._TYPECODE + "}");
        return searchResult.getResult();
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
