package com.viking.core.service.impl;

import com.viking.core.service.VikingCommerceStoreService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.viking.core.constants.VikingCoreConstants.*;

/**
 * Viking implementation of {@link VikingCommerceStoreService}
 *
 * @author amine.elharrak
 */

public class DefaultVikingCommerceStoreService implements VikingCommerceStoreService {

    @Autowired
    private CatalogVersionService catalogVersionService;

    @Override
    public CatalogVersionModel getOnlineCatalogVersion(final String store) {
        return PARTNER_STORE.equals(store) ? catalogVersionService.getCatalogVersion(PARTNER_PRODUCT_CATALOG, ONLINE_CATALOG_VERSION_NAME)
                : catalogVersionService.getCatalogVersion(SAFETYSHOP_PRODUCT_CATALOG, ONLINE_CATALOG_VERSION_NAME);
    }

}
