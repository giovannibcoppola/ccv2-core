package com.viking.core.dao.impl;

import com.viking.core.dao.VikingProductDao;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.*;

public class DefaultVikingProductDao extends AbstractItemDao implements VikingProductDao {

    protected final String QUERY =  "SELECT {p:PK} from  {Product as p " +
            "JOIN CatalogVersion as cv ON {p:catalogVersion}={cv:PK} " +
            "JOIN Catalog as c ON {cv:catalog} = {c:PK}} " +
            "WHERE {c:id}='Default' " +
            "AND {cv:version} = 'Staged' " +
            "AND ({p:creationtime} >= ?modifiedtime OR {p:sapFieldModifiedTime} >= ?modifiedtime)";


    @Override
    public List<ProductModel> findProductsWithModifiedTimeGreaterThan(Date modifiedTime) {

        final Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("modifiedtime", modifiedTime);

        final SearchResult<ProductModel> searchResult = getFlexibleSearchService().search(QUERY, queryParams);

        return searchResult.getCount() > 0 ? searchResult.getResult() : Collections.emptyList();
    }
}
