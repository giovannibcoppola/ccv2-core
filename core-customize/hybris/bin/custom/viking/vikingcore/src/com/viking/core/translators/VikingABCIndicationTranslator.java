package com.viking.core.translators;

import com.viking.core.jalo.ABCIndication;
import com.viking.core.model.ABCIndicationModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.impl.DefaultClassificationClassesResolverStrategy;
import de.hybris.platform.classification.impl.DefaultClassificationSystemService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.impl.DefaultEnumerationService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VikingABCIndicationTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingABCIndicationTranslator.class);

    protected static final String CATALOG_ID = "ERPClassification";
    protected static final String CATALOG_VERSION_NAME = "1.0";
    public static final String VARIANT_PRODUCT = "ERPVariantProduct";
    private static final String IGNORE = "ignore";

    private DefaultModelService modelService;


    @Override
    public void performImport(final String abcIndicationCode, final Item processedItem) throws ImpExException {
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }


        if(StringUtils.isNotEmpty(abcIndicationCode) && !abcIndicationCode.contains(IGNORE)) {
            ABCIndicationModel abcIndicationModel = null;
            try {
                final Product product =(Product) processedItem;
                final ProductModel productModel = modelService.get(product);
                List<ABCIndicationModel> abcIndicationModelList = new ArrayList<ABCIndicationModel>();
                ABCIndicationModel newABCIndicationModel = null;
                if(CollectionUtils.isNotEmpty(productModel.getAbcIndication())) {
                    abcIndicationModelList.addAll(productModel.getAbcIndication());
                    Optional<ABCIndicationModel> optionalABCIndicationModel = abcIndicationModelList.stream()
                            .filter(abc -> abcIndicationCode.equals(abc.getIndication()))
                            .findAny();
                    if(optionalABCIndicationModel.isEmpty()){
                        newABCIndicationModel = createABCIndication(abcIndicationCode);
                    }
                } else {
                    newABCIndicationModel = createABCIndication(abcIndicationCode);
                }
               if(newABCIndicationModel != null) {
                   abcIndicationModelList.add(newABCIndicationModel);
                   productModel.setAbcIndication(abcIndicationModelList);
                   modelService.save(productModel);
               }

            } catch (Exception e) {
                LOG.error("Could not set ABCIndication for code : "+ abcIndicationCode + " to product :"+ processedItem);
            }
        }
    }

    private ABCIndicationModel createABCIndication(String abcIndicationCode) {
        final ABCIndicationModel newABCindiationModel = modelService.create(ABCIndicationModel.class);
        newABCindiationModel.setIndication(abcIndicationCode);
        modelService.save(newABCindiationModel);
        return newABCindiationModel;
    }

}
