package com.viking.core.service;

import com.sap.conn.jco.JCoTable;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;

public interface VikingPriceService extends PriceService {

    JCoTable callPriceATP(String productCode, String resultTable) throws BackendException;
}
