package com.viking.core.interceptors;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.log4j.Logger;

/**
 * Prepare interceptor for {@link B2BCustomerModel}. It adds StoreId.
 *
 * @author giovannicoppola
 */
public class VikingB2BUnitPrepareInterceptor implements PrepareInterceptor<B2BCustomerModel> {

    private static final Logger LOG = Logger.getLogger(VikingB2BUnitPrepareInterceptor.class);

    @Override
    public void onPrepare(B2BCustomerModel model, InterceptorContext ctx) throws InterceptorException {
        if (model instanceof B2BCustomerModel) {
            final B2BCustomerModel customer = (B2BCustomerModel) model;
            if(ctx.isModified(model, "groups")) {
                LOG.info("#### VIK-2329 Investigation ####");
                LOG.info("Something is modifying the groups for the customer " + customer.getUid());
                for(StackTraceElement stackTraceElement : Thread.currentThread().getStackTrace()) {
                    LOG.info(stackTraceElement.toString());
                }
            }
        }
    }
}
