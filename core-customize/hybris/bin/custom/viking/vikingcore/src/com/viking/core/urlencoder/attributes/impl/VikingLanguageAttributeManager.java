package com.viking.core.urlencoder.attributes.impl;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.acceleratorservices.urlencoder.attributes.impl.DefaultLanguageAttributeManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class VikingLanguageAttributeManager extends DefaultLanguageAttributeManager {
    private static final Logger LOG = Logger.getLogger(VikingLanguageAttributeManager.class);

    @Override
    public void updateAndSyncForAttrChange(final String value) {
        if (isValid(value)) {
            // Set region relation
            getSessionService().setAttribute(VikingCoreConstants.REGION_ISOCODE, findCountryIsoCode(value));

            // Set language
            super.updateAndSyncForAttrChange(value);

            // Set currency
            getStoreSessionService().setCurrentCurrency(getCommerceCommonI18NService().getDefaultCurrency().getIsocode());
        }
    }

    private String findCountryIsoCode(final String languageCode) {
        final Map<String, String> languageToCountry = new HashMap<>();
        languageToCountry.put("da", "DK");
        languageToCountry.put("se", "SE");
        languageToCountry.put("no", "NO");
        languageToCountry.put("us", "US");

        final String country = languageToCountry.get(languageCode);

        LOG.debug("Country iso code: " + country);

        return StringUtils.isNotBlank(country) ? country : "EN";
    }

}
