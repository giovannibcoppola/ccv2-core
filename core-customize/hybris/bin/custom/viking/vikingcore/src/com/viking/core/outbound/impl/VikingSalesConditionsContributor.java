/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.core.configuration.model.SAPConfigurationModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.SalesConditionCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.SaporderexchangeConstants;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultSalesConditionsContributor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Builds the Row map for the CSV files for the Sales Conditions in an Order
 */
public class VikingSalesConditionsContributor extends DefaultSalesConditionsContributor {

    private static final String DEFAULT_DLV_COST_COND_CODE = "ZFRE";

    @Autowired
    private EnquiryService enquiryService;


    @Override
    protected void createDeliveryCostRow(final OrderModel order, final List<Map<String, Object>> result) {

        // This fixes an issue with HANA DB where 0 is replaced with -0.00000256. If issue is fixed or when solution is migrated to another
        // database this contributor should be removed.
        if (order.getDeliveryCost() > 0) {
            final Map<String, Object> row = new HashMap<>();
            row.put(OrderCsvColumns.ORDER_ID, order.getCode());
            row.put(SalesConditionCsvColumns.CONDITION_ENTRY_NUMBER, SaporderexchangeConstants.HEADER_ENTRY);
            row.put(SalesConditionCsvColumns.CONDITION_CODE, getDeliveryCostConditionTypes(order));
            row.put(SalesConditionCsvColumns.CONDITION_VALUE, order.getDeliveryCost());
            row.put(SalesConditionCsvColumns.CONDITION_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
            row.put(SalesConditionCsvColumns.CONDITION_COUNTER, getConditionCounterDeliveryCost());
            row.put(SalesConditionCsvColumns.ABSOLUTE, Boolean.TRUE);
            getBatchIdAttributes().forEach(row::putIfAbsent);
            row.put("dh_batchId", order.getCode());
            result.add(row);
        }
    }

    protected String getDeliveryCostConditionTypes(final OrderModel order) {
        final SAPConfigurationModel sapConfiguration = order.getStore().getSAPConfiguration();
        if (sapConfiguration != null) {
            return sapConfiguration.getSaporderexchange_deliveryCostConditionType();
        }

        return DEFAULT_DLV_COST_COND_CODE;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel order)
    {
        List<Map<String, Object>> mapList = super.createRows(order);
        if(CollectionUtils.isNotEmpty(mapList)) {
            final Map<String, Object> row = new HashMap<>();
            row.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));
            mapList.add(row);
        }
        return mapList;
    }

    @Override
    public Set<String> getColumns() {
        Set<String> columns = super.getColumns();
        columns.add(VikingCoreConstants.INQUIRY);
        return columns;
    }

}
