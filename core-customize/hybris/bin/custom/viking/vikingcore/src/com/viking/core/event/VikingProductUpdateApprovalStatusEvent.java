package com.viking.core.event;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

/**
 * Event to handle Viking Product Update Approval Status
 *
 * @author amine.elharrak
 */
public class VikingProductUpdateApprovalStatusEvent extends AbstractEvent {

    private String productCode;
    private String store;

    /**
     * Instantiates a new Viking product update approval status event.
     *
     * @param productCode the product code
     * @param store       the store
     */
    public VikingProductUpdateApprovalStatusEvent(String productCode, String store) {
        this.productCode = productCode;
        this.store = store;
    }

    /**
     * Gets product code.
     *
     * @return the product code
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets product code.
     *
     * @param productCode the product code
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * Gets store.
     *
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * Sets store.
     *
     * @param store the store
     */
    public void setStore(String store) {
        this.store = store;
    }
}
