package com.viking.core.search.solrfacetsearch.provider.impl;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ImageValueProvider;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * VikingImageValueProvider extends ImageValueProvider to make sure the product images are retrieved correctly.
 */
public class VikingImageValueProvider extends ImageValueProvider {

    @Resource
    private ProductChannelDataService productChannelDataService;

    @Override
    protected MediaModel findMedia(ProductModel product, MediaFormatModel mediaFormat) {
        if (product != null && mediaFormat != null) {
            List<MediaContainerModel> filteredMediaContainerModelList = productChannelDataService.getFilteredMediaContainersForChannel(product.getGalleryImages());
            if (CollectionUtils.isNotEmpty(filteredMediaContainerModelList)) {
                MediaModel media = handleProductChannelDataImages(filteredMediaContainerModelList, mediaFormat);
                if (media != null) return media;
            }

            // Failed to find media in product
            if (isVariantProductModel(product))
                return findMedia(((VariantProductModel) product).getBaseProduct(), mediaFormat);
        }
        return null;
    }

    private MediaModel handleProductChannelDataImages(List<MediaContainerModel> galleryImages, MediaFormatModel mediaFormat) {
        if (galleryImages != null && !galleryImages.isEmpty()) {
            // Search each media container in the gallery for an image of the right format
            for (final MediaContainerModel container : galleryImages) {
                try {
                    final MediaModel media = getMediaContainerService().getMediaForFormat(container, mediaFormat);
                    if (media != null) {
                        return media;
                    }
                } catch (final ModelNotFoundException ignore) {
                    // ignore
                }
            }
        }
        return null;
    }

    private boolean isVariantProductModel(ProductModel product) {
        return product instanceof VariantProductModel;
    }
}
