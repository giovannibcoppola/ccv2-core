/*datahub customization for MATMAS product code 0 leading*/

package com.viking.core.translators;

import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.catalog.impl.DefaultCatalogService;
import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.servicelayer.type.impl.DefaultTypeService;
import de.hybris.platform.variants.model.VariantTypeModel;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;


public class VikingBaseProductTranslator extends AbstractValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingBaseProductTranslator.class);

    protected static final String CATALOG_ID = "Default";
    protected static final String CATALOG_VERSION_NAME = "Staged";
    public static final String VARIANT_PRODUCT = "ERPVariantProduct";

    private DefaultProductService defaultProductService;
    private DefaultModelService modelService;
    private DefaultCatalogVersionService defaultCatalogVersionService;
    private DefaultTypeService defaultTypeService;

    @Override
    public Object importValue(String productCode, Item item) throws JaloInvalidParameterException {
        if (defaultProductService == null) {
            defaultProductService = (DefaultProductService) Registry.getApplicationContext().getBean(
                    "defaultProductService");
        }
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if (defaultCatalogVersionService == null) {
            defaultCatalogVersionService = (DefaultCatalogVersionService) Registry.getApplicationContext().getBean(
                    "defaultCatalogVersionService");
        }
        if (defaultTypeService == null) {
            defaultTypeService = (DefaultTypeService) Registry.getApplicationContext().getBean(
                    "defaultTypeService");
        }

        if(StringUtils.isNotEmpty(productCode)) {
            final CatalogVersionModel catalogVersionModel = defaultCatalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION_NAME);
            if(catalogVersionModel != null) {
                try {
                    final ProductModel productModel = defaultProductService.getProductForCode(catalogVersionModel, productCode);
                    if(productModel != null) {
                        return modelService.getSource(productModel);
                    }
                }catch (Exception e) {
                    LOG.error("product not found in Default catalog with code: "+ productCode +". New Product is created.");
                    final ProductModel newProduct = modelService.create(ProductModel.class);
                     newProduct.setCode(productCode);
                     newProduct.setCatalogVersion(catalogVersionModel);
                     newProduct.setVariantType((VariantTypeModel) defaultTypeService.getComposedTypeForCode(VARIANT_PRODUCT));
                     newProduct.setName(productCode);
                     modelService.save(newProduct);
                    return modelService.getSource(newProduct);
                }
            }
        }

        return productCode;

    }

    @Override
    public String exportValue(Object o) throws JaloInvalidParameterException {
        return null;
    }
}
