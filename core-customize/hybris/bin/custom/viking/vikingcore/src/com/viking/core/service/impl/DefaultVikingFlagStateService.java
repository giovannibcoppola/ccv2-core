package com.viking.core.service.impl;

import com.viking.core.dao.VikingFlagStateDao;
import com.viking.core.model.FlagStateModel;
import com.viking.core.service.VikingFlagStateService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingFlagStateService implements VikingFlagStateService {

    private VikingFlagStateDao vikingFlagStateDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FlagStateModel> getAll() {
        return vikingFlagStateDao.getAll();
    }

    @Required
    public void setVikingFlagStateDao(VikingFlagStateDao vikingFlagStateDao) {
        this.vikingFlagStateDao = vikingFlagStateDao;
    }
}
