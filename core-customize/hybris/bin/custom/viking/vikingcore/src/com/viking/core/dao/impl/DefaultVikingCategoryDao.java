package com.viking.core.dao.impl;

import com.viking.core.dao.VikingCategoryDao;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.daos.impl.DefaultCategoryDao;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


public class DefaultVikingCategoryDao extends DefaultCategoryDao implements VikingCategoryDao
{
	@Override
	public Collection<CategoryModel> findCategories(CatalogVersionModel catalogVersion)
	{
		final StringBuilder query = new StringBuilder("SELECT {cat." + CategoryModel.PK + "} ");
		query.append("FROM {" + CategoryModel._TYPECODE + " AS cat} ");
		query.append("WHERE {cat." + CategoryModel.CATALOGVERSION + "} = (?" + CategoryModel.CATALOGVERSION + ")");

		final Map<String, Object> params = new HashMap<String, Object>(2);
		params.put(CategoryModel.CATALOGVERSION, catalogVersion);;

		final SearchResult<CategoryModel> searchRes = search(query.toString(), params);
		return searchRes.getResult();
	}

	@Override
	public Collection<CategoryModel> findCategoriesAllowedForPrincipalGroupModels(final CatalogVersionModel catalogVersion, final Collection<PrincipalGroupModel> principalGroupModels) {
		final Set<String> groups = principalGroupModels.stream().map(pgm -> pgm.getPk().toString()).collect(Collectors.toSet());

		final StringBuilder query = new StringBuilder("SELECT DISTINCT {C." + CategoryModel.PK + "} ");
		query.append("FROM {" + CategoryModel._TYPECODE + " AS C JOIN " + PrincipalGroupModel._CATEGORY2PRINCIPALRELATION + " AS CPR ON {C." + CategoryModel.PK +" }={CPR." + Link.SOURCE +"}} ");
		query.append("WHERE {C." + CategoryModel.CATALOGVERSION + "} = (?" + CategoryModel.CATALOGVERSION + ")");
		query.append(" AND {CPR." + Link.TARGET + "} IN (?principalGroupModels)");

		final Map<String, Object> params = new HashMap<>(2);
		params.put(CategoryModel.CATALOGVERSION, catalogVersion);
		params.put("principalGroupModels", groups);

		final SearchResult<CategoryModel> searchRes = search(query.toString(), params);
		return searchRes.getResult();
	}
}
