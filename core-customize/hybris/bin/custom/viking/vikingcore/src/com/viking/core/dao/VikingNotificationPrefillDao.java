package com.viking.core.dao;

import com.viking.core.jalo.NotificationPrefill;

import java.util.List;

public interface VikingNotificationPrefillDao {
    List<NotificationPrefill> getNotifications(String notification);
}
