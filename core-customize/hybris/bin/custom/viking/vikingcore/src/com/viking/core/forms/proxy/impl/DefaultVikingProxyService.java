package com.viking.core.forms.proxy.impl;

import com.viking.core.forms.proxy.VikingProxyService;
import de.hybris.platform.xyformsservices.proxy.impl.DefaultProxyService;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;

public class DefaultVikingProxyService extends DefaultProxyService implements VikingProxyService {


    public String rewriteURL(String applicationId, String formId, String formDataId, boolean editable, Map<String, String[]> requestParameterMap) throws MalformedURLException {

        String url_wo_request_params =  super.rewriteURL(applicationId, formId, formDataId, editable);

        URIBuilder builder = null;
        try
        {
            builder = new URIBuilder(url_wo_request_params);
        }
        catch (final URISyntaxException e)
        {
            throw new MalformedURLException(e.getMessage());
        }

        if(!requestParameterMap.isEmpty()){
            for (Map.Entry<String, String[]> entry : requestParameterMap.entrySet())
            {
                builder.addParameter(entry.getKey(),entry.getValue()[0]);
            }
        }

        return builder.toString();
    }
}
