package com.viking.core.catalog.synchronization.strategy;

import com.viking.core.catalog.synchronization.VikingCatalogSynchronizationService;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SyncJobCategoryStoreFilterStrategyImpl implements SyncJobApplicableItemStrategy {

    private static final Logger LOG = Logger.getLogger(VikingCatalogSynchronizationService.class);

    private BaseStoreService baseStoreService;

    @Override
    public List<ItemModel> filterItems(final List<ItemModel> inputItems, final SyncItemJobModel syncItemJob) {
        final List<BaseStoreModel> baseStores = getApplicableBaseStores(syncItemJob);

        final List<CategoryModel> categoriesToSkip = new ArrayList<>();
        for (ItemModel item : inputItems) {
            if (item instanceof CategoryModel) {
                final CategoryModel category = (CategoryModel) item;
                if (StringUtils.isEmpty(category.getStoreId()) || baseStores.stream().map(BaseStoreModel::getUid).noneMatch(s -> s.equals(category.getStoreId()))) {
                    categoriesToSkip.add(category);
                }
            }
        }
        if (CollectionUtils.isNotEmpty(categoriesToSkip)) {
            if (LOG.isInfoEnabled()) {
                final String baseStoreCodes = baseStores.stream()
                        .map(BaseStoreModel::getUid)
                        .collect(Collectors.joining(","));
                final String excludedCategoryCodes = categoriesToSkip.stream()
                        .map(CategoryModel::getCode)
                        .collect(Collectors.joining(","));
                LOG.info("Skip categories not linked to basestore(s): " + baseStoreCodes + ". The following categories are skipped " + excludedCategoryCodes);
            }
            return ListUtils.subtract(inputItems, categoriesToSkip);
        } else {
            LOG.debug("No categories found to skip");
        }
        return inputItems;
    }

    private List<BaseStoreModel> getApplicableBaseStores(final SyncItemJobModel syncItemJob) {
        return baseStoreService.getAllBaseStores().stream()
                .filter(store -> store.getCatalogs().contains(syncItemJob.getTargetVersion().getCatalog()))
                .collect(Collectors.toUnmodifiableList());
    }

    @Required
    public void setBaseStoreService(final BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}
