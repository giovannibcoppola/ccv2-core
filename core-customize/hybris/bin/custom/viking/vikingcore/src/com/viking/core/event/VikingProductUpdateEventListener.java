package com.viking.core.event;

/**
 * Listener that handles {@link VikingProductUpdateEvent}
 *
 * @author javier.gomez
 */
public class VikingProductUpdateEventListener extends AbstractVikingProductEventListener<VikingProductUpdateEvent> {

    private static final String PRODUCT_UPDATE_WORKFLOW_TEMPLATE_CODE = "VIKING_PR_UPDATE";

    @Override
    protected void onEvent(VikingProductUpdateEvent event) {
        if (event != null && event.getProductModel() != null && isWorkflowNeeded(event.getProductModel())) {
            triggerProductApprovalWorkflow(event.getProductModel(), PRODUCT_UPDATE_WORKFLOW_TEMPLATE_CODE);
        }
    }
}
