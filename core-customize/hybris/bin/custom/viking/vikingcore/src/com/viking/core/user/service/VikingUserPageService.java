/**
 *
 */
package com.viking.core.user.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;

import java.util.List;

import com.viking.core.model.VikingVesselModel;


/**
 * @author Srikanth
 *
 */
public interface VikingUserPageService
{
	public List<B2BCustomerModel> getAllUsers();

	public VikingVesselModel getVesselIds(String vesselID);

	public B2BCustomerModel removeUser(String id);

	public B2BCustomerModel getUser(String name);

	public List<VikingVesselModel> getAllVikingVessels();

}
