package com.viking.core.dao.impl;

import com.viking.core.dao.VikingApprovedFacilityDao;
import com.viking.core.model.ApprovedFacilityModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DefaultVikingApprovedFacilityDao extends AbstractItemDao implements VikingApprovedFacilityDao {

    private static final String GET_APPROVED_FACILITY_QUERY = "select {" + ApprovedFacilityModel.PK + "} from {"
            + ApprovedFacilityModel._TYPECODE + " }  where {" + ApprovedFacilityModel.CUSTOMER + "}=?" + ApprovedFacilityModel.CUSTOMER;

    @Override
    public Optional<ApprovedFacilityModel> getApprovedFacilityForUser(final B2BCustomerModel customer) {
        final Map<String, Object> queryParameters = new HashMap<String, Object>();
        queryParameters.put(ApprovedFacilityModel.CUSTOMER, customer);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_APPROVED_FACILITY_QUERY, queryParameters);

        try {
            return Optional.of(getFlexibleSearchService().searchUnique(query));
        } catch (ModelNotFoundException | AmbiguousIdentifierException exception) {
            return Optional.empty();
        }
    }
}
