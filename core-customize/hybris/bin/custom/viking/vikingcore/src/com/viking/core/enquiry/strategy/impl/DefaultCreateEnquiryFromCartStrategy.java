package com.viking.core.enquiry.strategy.impl;

import com.viking.core.enquiry.strategy.CreateEnquiryFromCartStrategy;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.impl.DefaultCreateOrderFromCartStrategy;
import de.hybris.platform.order.strategies.ordercloning.CloneAbstractOrderStrategy;

import javax.annotation.Resource;

public class DefaultCreateEnquiryFromCartStrategy extends DefaultCreateOrderFromCartStrategy implements CreateEnquiryFromCartStrategy {

    @Resource
    private CloneAbstractOrderStrategy cloneAbstractOrderStrategy;

    @Override
    public VikingEnquiryOrderModel createEnquiryFromCart(CartModel cart) throws InvalidCartException {
        final VikingEnquiryOrderModel vikingEnquiryOrderModel = cloneAbstractOrderStrategy.clone(null, null, cart, generateOrderCode(cart), VikingEnquiryOrderModel.class,
                OrderEntryModel.class);

        return vikingEnquiryOrderModel;
    }
}
