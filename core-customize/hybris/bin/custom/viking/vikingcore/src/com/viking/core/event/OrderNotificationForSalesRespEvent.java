package com.viking.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

public class OrderNotificationForSalesRespEvent extends OrderProcessingEvent {

    public OrderNotificationForSalesRespEvent(OrderProcessModel process) {
        super(process);
    }
}
