package com.viking.core.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public interface VikingOrderEntryService {

    /**
     * Check if certificates are in the cart (products with hierarchy within range 1101021 - 1101030)
     * @param entry
     * @return
     */
    boolean isCertificateEntry(AbstractOrderEntryModel entry);
}
