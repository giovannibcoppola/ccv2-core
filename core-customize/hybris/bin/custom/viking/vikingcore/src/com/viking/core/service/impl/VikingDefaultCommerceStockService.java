package com.viking.core.service.impl;


import com.sap.conn.jco.JCoField;
import com.sap.conn.jco.JCoFieldIterator;
import com.sap.conn.jco.JCoTable;
import com.viking.core.service.VikingCommerceStockService;
import com.viking.core.service.VikingPriceInformation;
import com.viking.core.service.VikingPriceService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.stock.impl.DefaultCommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionFactory;
import de.hybris.platform.sap.core.jco.exceptions.BackendException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class VikingDefaultCommerceStockService extends DefaultCommerceStockService implements VikingCommerceStockService {
    private static final Logger LOG = Logger.getLogger(VikingDefaultCommerceStockService.class);

    private UserService userService;

    private JCoManagedConnectionFactory jCoManagedConnectionFactory;

    private VikingPriceService vikingPriceService;

    private ConfigurationService configurationService;

    private ProductService productService;

    @Override
    public StockLevelStatus getStockLevelStatusForProductAndBaseStore(final ProductModel product, final BaseStoreModel baseStore) {
        validateParameterNotNull(product, "product cannot be null");
        validateParameterNotNull(baseStore, "baseStore cannot be null");
        List<PriceInformation> prices = getVikingPriceService().getPriceInformationsForProduct(product);
        return prices == null || prices.isEmpty()? StockLevelStatus.OUTOFSTOCK  :StockLevelStatus.INSTOCK;
    }

    @Override
    public Long getStockLevelForProductAndBaseStore(final ProductModel product, final BaseStoreModel baseStore) {
        validateParameterNotNull(product, "product cannot be null");
        validateParameterNotNull(baseStore, "baseStore cannot be null");
        List<PriceInformation> prices = getVikingPriceService().getPriceInformationsForProduct(product);
        return prices == null || prices.isEmpty() ? 0 : 10000l;
    }

    @Override
    public StockLevelStatus getStockLevelStatusForProduct(final String productCode) {
        List<PriceInformation> prices = getVikingPriceService().getPriceInformationsForProduct(getProductService().getProductForCode(productCode));
        return prices == null || prices.isEmpty()? StockLevelStatus.OUTOFSTOCK  :StockLevelStatus.INSTOCK;
    }

    @Override
    public Long getStockLevelForProduct(final String productCode) {
        List<PriceInformation> prices = getVikingPriceService().getPriceInformationsForProduct(getProductService().getProductForCode(productCode));
        return prices == null || prices.isEmpty() ? 0 : 10000l;
    }

    @Override
    public Date getStockLevelDateForProduct(final String productCode) {
        return getStockLevel(productCode);
    }

    @Override
    public int getStockLevelRange(final String productCode) {
        long greenDays = getConfigurationService().getConfiguration().getLong("viking.stock.green.days") * 24l * 60l * 60l * 1000l;
        long  yellowDays = getConfigurationService().getConfiguration().getLong("viking.stock.yellow.days") * 24l * 60l * 60l * 1000l;

        Date stockDate = getStockLevelDateForProduct(productCode);
        Date now = new Date();

        if (stockDate.getTime() < now.getTime() + greenDays) {
            return 1;
        }
        if (stockDate.getTime() < now.getTime() + yellowDays) {
            return 2;
        }
        return 3;


    }


    private Date getStockLevel(final String productCode) {

        if(getUserService().getCurrentUser() instanceof B2BCustomerModel) {

            try {

                JCoTable result = getVikingPriceService().callPriceATP(productCode, "IT_VBEP");
                if (result != null) {
                    LOG.info("rows: " + result.getNumRows());
                    Date zero = new Date(0);
                    Date more = new Date(0);

                    int stock = 0;

                    if (result.getNumRows() > 0) {
                        for (int i = 0; i < result.getNumRows(); i++) {
                            result.setRow(i);
                            if (result.getInt("BMENG") > 0) {
                                if (result.getDate("EDATU").getTime() > more.getTime()) {
                                    more = result.getDate("EDATU");
                                    stock = result.getInt("BMENG");
                                }
                            } else {
                                if (result.getDate("EDATU").getTime() > zero.getTime()) {
                                    zero = result.getDate("EDATU");
                                }
                            }
                            JCoFieldIterator iterator = result.getFieldIterator();
                            while (iterator.hasNextField()) {
                                JCoField field = iterator.nextField();
                                LOG.info(field.getName() + " : " + field.getValue());
                            }
                            LOG.info("");
                        }
                    }
                    LOG.info(zero);
                    LOG.info(more);
                    LOG.info(stock);

                    return stock > 0 ? more : zero;
                }
            } catch (BackendException e) {
                LOG.error("Error during call sap ERP backend for stock data", e);
            }

        }
        return new Date(new Date().getTime() + 3650l *24l*60l *60l*1000l);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public JCoManagedConnectionFactory getjCoManagedConnectionFactory() {
        return jCoManagedConnectionFactory;
    }

    public void setjCoManagedConnectionFactory(JCoManagedConnectionFactory jCoManagedConnectionFactory) {
        this.jCoManagedConnectionFactory = jCoManagedConnectionFactory;
    }


    public VikingPriceService getVikingPriceService() {
        return vikingPriceService;
    }

    public void setVikingPriceService(VikingPriceService vikingPriceService) {
        this.vikingPriceService = vikingPriceService;
    }

    public ConfigurationService getConfigurationService() {
        return configurationService;
    }

    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}
