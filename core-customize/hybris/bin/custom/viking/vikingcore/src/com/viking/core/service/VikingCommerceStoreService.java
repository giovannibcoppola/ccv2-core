package com.viking.core.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;


/**
 * Custom interface for CommerceStoreService
 *
 * @author amine.elharrak
 */
public interface VikingCommerceStoreService {
    /**
     * Gets online catalog version.
     *
     * @param store the store
     * @return the online catalog version
     */
    CatalogVersionModel getOnlineCatalogVersion(final String store);
}
