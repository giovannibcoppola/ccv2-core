package com.viking.core.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

public class WebsiteBasestoreService {
    private static final Logger LOG = Logger.getLogger(WebsiteBasestoreService.class);
    private static final String X_FORWARDED_FOR = "X-FORWARDED-FOR";
    private static final String COUNTRY_REGION = "CountryRegion";
    private static final String ISOCODE = "IsoCode";
    private static final String API_KEY = "geolocation.service.key";
    private static final String GEOLOCATION_URL = "geolocation.service.url";

    private ConfigurationService configurationService;
    private SessionService sessionService;
    private HttpClient client = HttpClientBuilder.create().build();

    public void setDefaultUserBasestore(final HttpServletRequest servletRequest) {
        String regionIsocode = sessionService.getAttribute(VikingCoreConstants.REGION_ISOCODE);

        LOG.debug("Check country iso code: " + regionIsocode);

        if (StringUtils.isBlank(regionIsocode)) {
            String ipAddress = getIpAddress(servletRequest);
            String key = configurationService.getConfiguration().getString(API_KEY);
            String geoLocationUrl = configurationService.getConfiguration().getString(GEOLOCATION_URL);
            String url = String.format(geoLocationUrl, key, ipAddress);
            HttpUriRequest request = new HttpGet(url);

            try {
                HttpResponse response = client.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        String jsonResponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);

                        ObjectMapper mapper = new ObjectMapper();
                        JsonNode actualObj = mapper.readTree(jsonResponse);
                        String isoCode = actualObj.get(COUNTRY_REGION).get(ISOCODE).textValue();

                        LOG.debug("Found country region iso-code: " + isoCode);

                        sessionService.setAttribute(VikingCoreConstants.REGION_ISOCODE, isoCode);
                    }
                }
            } catch (Exception e) {
                LOG.debug("Error resolving country region iso-code", e);
            }
        }
    }

    private String getIpAddress(HttpServletRequest request) {
        String ipAddress = "";

        if (request != null) {
            ipAddress = request.getHeader(X_FORWARDED_FOR);
            if (StringUtils.isBlank(ipAddress)) {
                ipAddress = request.getRemoteAddr();
            }
        }
        return ipAddress;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }
}
