package com.viking.core.dao.impl;

import com.viking.core.dao.RegionToBaseStoreDao;
import com.viking.core.model.RegionToBaseStoreModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DefaultRegionToBaseStoreDao extends DefaultGenericDao<RegionToBaseStoreModel> implements RegionToBaseStoreDao {

    public DefaultRegionToBaseStoreDao() {super(RegionToBaseStoreModel._TYPECODE);}

    private DefaultRegionToBaseStoreDao(String typecode) {
        super(typecode);
    }

    @Override
    public RegionToBaseStoreModel findRegionToBaseStoreByRegionIsocode(String isocode) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(RegionToBaseStoreModel.REGIONISOCODE, isocode);

        return find(params).get(0);
    }

    @Override
    public List<RegionToBaseStoreModel> findAllRegionToBaseStore() {
        return find();
    }

    @Override
    public Optional<RegionToBaseStoreModel> findRegionToBaseStoreByBaseStore(final BaseStoreModel baseStore) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(RegionToBaseStoreModel.BASESTORE, baseStore);

        final List<RegionToBaseStoreModel> searchResult = find(params);
        
        return CollectionUtils.isNotEmpty(searchResult) ? Optional.of(searchResult.get(0)) : Optional.empty();
    }
}
