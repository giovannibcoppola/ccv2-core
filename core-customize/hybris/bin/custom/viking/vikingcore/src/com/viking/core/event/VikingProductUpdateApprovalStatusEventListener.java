package com.viking.core.event;

import com.viking.core.service.VikingCommerceStoreService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;

import javax.annotation.Resource;

/**
 * Listener that handles {@link VikingProductUpdateApprovalStatusEvent}
 *
 * @author amine.elharrak
 */
public class VikingProductUpdateApprovalStatusEventListener extends AbstractEventListener<VikingProductUpdateApprovalStatusEvent> {

    private static final Logger LOGGER = Logger.getLogger(VikingProductUpdateApprovalStatusEventListener.class);

    @Resource
    private VikingCommerceStoreService vikingCommerceStoreService;
    @Resource
    private ProductService productService;
    @Resource
    private ModelService modelService;

    @Override
    protected void onEvent(VikingProductUpdateApprovalStatusEvent vikingProductUpdateApprovalStatusEvent) {
        final String productCode = vikingProductUpdateApprovalStatusEvent.getProductCode();
        final String storeId = vikingProductUpdateApprovalStatusEvent.getStore();
        triggerProductApprovalStatus(productCode, storeId);
    }

    /**
     * Trigger product approval status.
     *
     * @param produtCode the produt code
     * @param storeId    the store id
     */
    protected void triggerProductApprovalStatus(final String produtCode,final String storeId) {
        try {
            final CatalogVersionModel catalogVersion = vikingCommerceStoreService.getOnlineCatalogVersion(storeId);
            final ProductModel onlineProduct = productService.getProductForCode(catalogVersion, produtCode);
            onlineProduct.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
            modelService.save(onlineProduct);
        } catch (Exception exception) {
            LOGGER.debug("No action to do there is no product in the Online version for the same store catalog version ");
        }
    }

}
