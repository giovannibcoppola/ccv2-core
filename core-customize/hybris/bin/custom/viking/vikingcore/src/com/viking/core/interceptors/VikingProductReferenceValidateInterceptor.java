package com.viking.core.interceptors;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

/**
 * Prepare interceptor for {@link ProductReferenceModel}
 *
 * @author amine.elharrak
 */
public class VikingProductReferenceValidateInterceptor implements ValidateInterceptor<ProductReferenceModel> {

    @Override
    public void onValidate(ProductReferenceModel productReference, InterceptorContext interceptorContext) throws InterceptorException {
        if (!productReference.getSource().getCatalogVersion().getPk().equals(productReference.getTarget().getCatalogVersion().getPk())) {
            throw new InterceptorException("The Product Reference should be existing on the same catalog as the product ");
        }
    }

}