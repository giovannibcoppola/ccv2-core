package com.viking.core.dao.impl;

import com.viking.core.dao.VikingManualBulletinsAcceptedDao;
import com.viking.core.jalo.ManualBulletinsAccepted;
import com.viking.core.model.ManualBulletinsAcceptedModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DefaultVikingManualBulletinsAcceptedDao extends AbstractItemDao implements VikingManualBulletinsAcceptedDao {

    private static final String GET_MANUALBULLETINSACCEPTED_QUERY = "select {" + ManualBulletinsAcceptedModel.PK + "} from {"
            + ManualBulletinsAcceptedModel._TYPECODE + " }  where {" + ManualBulletinsAcceptedModel.BULLETINSID + "}=?bulletinsId AND {" + ManualBulletinsAcceptedModel.MANUALSID + "}=?manualsId AND {" + ManualBulletinsAcceptedModel.USERID + "}=?userId";


    @Override
    public List<ManualBulletinsAccepted> getManualBulletinsAccpted(String bulletinsId, String manualId, String userId) {
        final Map<String, Object> queryParameters = new HashMap<String, Object>();
        queryParameters.put("bulletinsId", bulletinsId);
        queryParameters.put("manualsId", manualId);
        queryParameters.put("userId", userId);
        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_MANUALBULLETINSACCEPTED_QUERY, queryParameters);
        final SearchResult<ManualBulletinsAccepted> searchRes = getFlexibleSearchService().search(query);
        return searchRes.getResult();
    }
}
