package com.viking.core.util;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.restrictions.CMSUserGroupRestrictionModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ManualsHelper {

    private static final Logger LOG = Logger.getLogger(ManualsHelper.class);

    private FlexibleSearchService flexibleSearchService;
    private CMSSiteService cmsSiteService;
    private UserService userService;

    public boolean isAccessDenied(final String url) {
        return isAccessDenied(url, userService.getCurrentUser(), cmsSiteService.getCurrentSite());
    }

    public boolean isAccessDenied(final String url, final UserModel customer, final CMSSiteModel site) {
        // Find CMSLinkComponent for the page. Check the restrictions usergroups agains groups on current user
        final CMSLinkComponentModel example = new CMSLinkComponentModel();
        example.setUrl(url);
        final ContentCatalogModel contentCatalogModel = site.getContentCatalogs().get(0);
        example.setCatalogVersion(contentCatalogModel.getActiveCatalogVersion());
        final List<CMSLinkComponentModel> links = flexibleSearchService.getModelsByExample(example);

        if (CollectionUtils.isEmpty(links)) {
            LOG.debug("No link found for url [" + url + "]. Navigate to homepage");
            return true;
        }

        // If more links found, then grab the first for now.
        List<CMSUserGroupRestrictionModel> userGroupRestrictions = links.get(0).getRestrictions().stream()
                .filter(CMSUserGroupRestrictionModel.class::isInstance)
                .map(CMSUserGroupRestrictionModel.class::cast)
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(userGroupRestrictions)) {
            LOG.debug("No usergroup restriction for manual page with url [" + url + "]");
            return false;
        }

        final List<UserGroupModel> userGroupsWithAccess = userGroupRestrictions.stream().map(CMSUserGroupRestrictionModel::getUserGroups).flatMap(Collection::stream).collect(Collectors.toList());

        final List<PrincipalGroupModel> groups = customer.getGroups().stream().filter(group -> group.getUid().startsWith("AUTH_")).collect(Collectors.toList());

        return Collections.disjoint(userGroupsWithAccess, groups);
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setCmsSiteService(CMSSiteService cmsSiteService) {
        this.cmsSiteService = cmsSiteService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
