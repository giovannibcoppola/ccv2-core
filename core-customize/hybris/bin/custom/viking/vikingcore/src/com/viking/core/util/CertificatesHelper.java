package com.viking.core.util;

import com.viking.core.model.ShowAllCertificatesModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

public class CertificatesHelper {

    private static final Logger LOG = Logger.getLogger(CertificatesHelper.class);

    private FlexibleSearchService flexibleSearchService;
    private UserService userService;

    public Optional<ShowAllCertificatesModel> findShowAllCertificates(final String customerId) {
        final UserModel user = userService.getUserForUID(customerId);

        try {
            if (user instanceof B2BCustomerModel) {
                final ShowAllCertificatesModel example = new ShowAllCertificatesModel();
                final B2BUnitModel unit = ((B2BCustomerModel) user).getDefaultB2BUnit();
                example.setUnit(unit);
                return Optional.of(flexibleSearchService.getModelByExample(example));
            }
        } catch (ModelNotFoundException | AmbiguousIdentifierException exception) {
            LOG.debug("Exception finding ShowAllCertificatesModel for CustomerId: " + customerId, exception);
        }

        return Optional.empty();
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
