package com.viking.core.search.solrfacetsearch.provider.impl.populators;

import de.hybris.platform.solrfacetsearch.config.FacetType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.FacetValueField;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Populator that uses the facet.limit.default property rather than defaulting to 50 in all cases
 */
public class VikingFacetSearchQueryFacetsPopulator extends AbstractFacetSearchQueryPopulator {

    public static final String DEFAULT_FACET_SORT = "count";
    public static final Integer DEFAULT_LIMIT = 50;
    public static final Integer MINIMUM_COUNT = 1;
    private String defaultFacetSort;
    private Integer defaultFacetLimit;

    public VikingFacetSearchQueryFacetsPopulator() {
        //empty constructor
    }

    public String getDefaultFacetSort() {
        return this.defaultFacetSort;
    }

    @Autowired
    public void setDefaultFacetSort(String defaultFacetSort) {
        this.defaultFacetSort = defaultFacetSort;
    }

    public Integer getDefaultFacetLimit() {
        return defaultFacetLimit;
    }

    @Autowired
    public void setDefaultFacetLimit(Integer defaultFacetLimit) {
        this.defaultFacetLimit = defaultFacetLimit;
    }

    public void populate(SearchQueryConverterData source, SolrQuery target) {
        SearchQuery searchQuery = source.getSearchQuery();
        Map<String, FacetInfo> facetInfos = this.buildFacetInfos(searchQuery);
        int index = 0;

        for(Iterator<FacetInfo> var7 = facetInfos.values().iterator(); var7.hasNext(); ++index) {
            FacetInfo facetInfo = var7.next();
            FacetField facetField = facetInfo.getFacetField();
            String translatedField = this.getFieldNameTranslator().translate(searchQuery, facetField.getField(), FieldNameProvider.FieldType.INDEX);
            FacetType facetType = facetField.getFacetType();
            if (CollectionUtils.isEmpty(facetInfo.getSelectedValues())) {
                target.addFacetField(translatedField);
            } else {
                fillFacetByType(target, searchQuery, index, facetInfo, facetField, translatedField, facetType);
            }
        }

        target.setFacetSort(this.resolveFacetSort());
        String[] facetMincount = searchQuery.getRawParams().get("facet.mincount");
        if (facetMincount == null || facetMincount.length == 0) {
            target.setFacetMinCount(MINIMUM_COUNT);
        }

        String[] facetLimit = searchQuery.getRawParams().get("facet.limit");
        if (facetLimit == null || facetLimit.length == 0) {
            target.setFacetLimit(defaultFacetLimit != null ? defaultFacetLimit : DEFAULT_LIMIT);
        }

    }

    private void fillFacetByType(SolrQuery target, SearchQuery searchQuery, int index, FacetInfo facetInfo, FacetField facetField, String translatedField, FacetType facetType) {
        if (FacetType.REFINE.equals(facetType)) {
            populateRefineType(target, searchQuery, facetInfo, facetField, translatedField);
        } else if (FacetType.MULTISELECTAND.equals(facetType)) {
            populateMultiSelect(target, searchQuery, index, facetInfo, facetField, translatedField, SearchQuery.Operator.AND);
        } else if (FacetType.MULTISELECTOR.equals(facetType)) {
            populateMultiSelect(target, searchQuery, index, facetInfo, facetField, translatedField, SearchQuery.Operator.OR);
        } else {
            populateRefineType(target, searchQuery, facetInfo, facetField, translatedField);
        }
    }

    private void populateMultiSelect(SolrQuery target, SearchQuery searchQuery, int index, FacetInfo facetInfo, FacetField facetField, String translatedField, SearchQuery.Operator operator) {
        QueryField facetFilterQuery;
        String filterQuery;
        facetFilterQuery = new QueryField(facetField.getField(), operator, facetInfo.getSelectedValues());
        filterQuery = this.convertQueryField(searchQuery, facetFilterQuery);
        target.addFilterQuery("{!tag=fk" + index + "}" + filterQuery);
        target.addFacetField("{!ex=fk" + index + "}" + translatedField);
    }

    private void populateRefineType(SolrQuery target, SearchQuery searchQuery, FacetInfo facetInfo, FacetField facetField, String translatedField) {
        QueryField facetFilterQuery;
        String filterQuery;
        facetFilterQuery = new QueryField(facetField.getField(), SearchQuery.Operator.AND, facetInfo.getSelectedValues());
        filterQuery = this.convertQueryField(searchQuery, facetFilterQuery);
        target.addFilterQuery(filterQuery);
        target.addFacetField(translatedField);
    }

    protected Map<String, FacetInfo> buildFacetInfos(SearchQuery searchQuery) {
        Map<String, FacetInfo> facetInfos = new HashMap<>();
        Iterator<FacetField> facetFieldIterator = searchQuery.getFacets().iterator();

        FacetInfo facetInfo;
        while(facetFieldIterator.hasNext()) {
            FacetField facet = facetFieldIterator.next();
            facetInfo = new FacetInfo(facet);
            facetInfos.put(facet.getField(), facetInfo);
        }

        Iterator<FacetValueField> facetValueFieldIterator = searchQuery.getFacetValues().iterator();

        while(facetValueFieldIterator.hasNext()) {
            FacetValueField facetValue = facetValueFieldIterator.next();
            facetInfo = facetInfos.get(facetValue.getField());
            if (facetInfo == null) {
                FacetField facetField = new FacetField(facetValue.getField());
                facetInfo = new FacetInfo(facetField);
                facetInfos.put(facetValue.getField(), facetInfo);
            }

            if (CollectionUtils.isNotEmpty(facetValue.getValues())) {
                facetInfo.getSelectedValues().addAll(facetValue.getValues());
            }
        }

        return facetInfos;
    }

    protected String resolveFacetSort() {
        return this.defaultFacetSort != null ? this.defaultFacetSort : DEFAULT_FACET_SORT;
    }

    protected static class FacetInfo {
        private final FacetField facetField;
        private final Set<String> selectedValues;

        public FacetInfo(FacetField facetField) {
            this.facetField = facetField;
            this.selectedValues = new LinkedHashSet<>();
        }

        public FacetField getFacetField() {
            return this.facetField;
        }

        public Set<String> getSelectedValues() {
            return this.selectedValues;
        }
    }
}
