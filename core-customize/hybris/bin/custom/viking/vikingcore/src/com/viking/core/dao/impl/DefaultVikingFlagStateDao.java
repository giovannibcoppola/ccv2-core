package com.viking.core.dao.impl;

import com.viking.core.dao.VikingFlagStateDao;
import com.viking.core.model.FlagStateModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DefaultVikingFlagStateDao extends AbstractItemDao implements VikingFlagStateDao {

    private static final String ALL_QUERY = "SELECT {" + FlagStateModel.PK + "} FROM {" + FlagStateModel._TYPECODE + "} ORDER BY {" + FlagStateModel.NAME + "} ASC" ;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FlagStateModel> getAll() {
        SearchResult<FlagStateModel> searchResult = getFlexibleSearchService().search(ALL_QUERY);
        return searchResult.getResult();
    }
}
