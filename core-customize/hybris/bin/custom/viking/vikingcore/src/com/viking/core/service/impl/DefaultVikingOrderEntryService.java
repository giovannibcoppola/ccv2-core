package com.viking.core.service.impl;

import com.viking.core.service.VikingOrderEntryService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.apache.commons.lang.math.IntRange;

public class DefaultVikingOrderEntryService implements VikingOrderEntryService {

    @Override
    public boolean isCertificateEntry(final AbstractOrderEntryModel entry) {
        final String prdha = entry.getProduct().getPRDHA();
        return prdha != null && new IntRange(1101021000,1101030000).containsInteger(Integer.parseInt(prdha));
    }
}
