package com.viking.core.forms.proxy;

import de.hybris.platform.xyformsservices.proxy.ProxyService;

import java.net.MalformedURLException;
import java.util.Map;

public interface VikingProxyService extends ProxyService {

    String rewriteURL(final String applicationId, final String formId, final String formDataId, final boolean editable, Map<String,String[]> requestParameterMap) throws MalformedURLException;
}
