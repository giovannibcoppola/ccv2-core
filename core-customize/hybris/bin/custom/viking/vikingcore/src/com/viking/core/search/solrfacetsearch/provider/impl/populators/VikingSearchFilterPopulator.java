package com.viking.core.search.solrfacetsearch.provider.impl.populators;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.FilterQueryOperator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchFilterQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchFiltersPopulator;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.beans.factory.annotation.Required;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.Sets.newHashSet;
import static com.viking.core.constants.VikingCoreConstants.SAFETYSHOP;
import static com.viking.core.constants.VikingCoreConstants.REGION_ISOCODE;

public class VikingSearchFilterPopulator extends SearchFiltersPopulator {

    private SessionService sessionService;
    private BaseSiteService baseSiteService;

    @Override
    public void populate(final SearchQueryPageableData source, final SolrSearchRequest target) {

        final BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();

        if ("publicwebshop".equalsIgnoreCase(currentBaseSite.getUid())) {
            final Map<String, SolrSearchFilterQueryData> filterQueriesMap = new HashMap<>();

            final SolrSearchFilterQueryData filter = new SolrSearchFilterQueryData();
            filter.setKey("visibleToRegions");
            filter.setOperator(FilterQueryOperator.OR);

            final String isocode = sessionService.getAttribute(REGION_ISOCODE);

            if (isGlobal(isocode)) {
                filter.setValues(newHashSet("Global"));
            } else {
                filter.setValues(newHashSet(isocode));
            }

            filterQueriesMap.put("visibleToRegions", filter);

            populateFilterQueries(target.getSearchQueryData(), filterQueriesMap);
        }

        super.populate(source, target);
    }

    private boolean isGlobal(final String isocode) {
        return !isocode.equalsIgnoreCase("DK") &&
                !isocode.equalsIgnoreCase("US") &&
                !isocode.equalsIgnoreCase("SE") &&
                !isocode.equalsIgnoreCase("NO");
    }

    @Required
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Required
    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}
