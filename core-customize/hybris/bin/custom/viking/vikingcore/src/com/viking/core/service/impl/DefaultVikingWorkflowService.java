package com.viking.core.service.impl;

import com.viking.core.dao.VikingWorkflowDao;
import com.viking.core.service.VikingWorkflowService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.workflow.model.WorkflowActionModel;

import javax.annotation.Resource;
import java.util.List;

/**
 * class implement Viking Workflow Service
 */

public class DefaultVikingWorkflowService implements VikingWorkflowService {

    @Resource
    VikingWorkflowDao vikingWorkflowDao;

    @Override
    public List<WorkflowActionModel> findAllWorkflowActionInProgressForProduct(String name, ItemModel item) {
        return item.getPk()!=null ? vikingWorkflowDao.findAllWorkflowActionInProgressForProduct( name, item) : null;
    }
}
