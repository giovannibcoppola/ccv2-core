package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultPaymentContributor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class VikingPaymentContributor extends DefaultPaymentContributor {

    @Autowired
    private EnquiryService enquiryService;


    @Override
    public List<Map<String, Object>> createRows(final OrderModel order) {
        List<Map<String, Object>> result = new ArrayList<>();

        if (SiteChannel.B2B.equals(order.getSite().getChannel())) {
            result = super.createRows(order);
        }
        final Map<String, Object> row = new HashMap<>();
        row.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));
        result.add(row);
        return result;
    }

    @Override
    public Set<String> getColumns() {
        Set<String> columns = super.getColumns();
        columns.add(VikingCoreConstants.INQUIRY);
        return columns;
    }
}
