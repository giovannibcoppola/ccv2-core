package com.viking.core.dao.impl;

import com.viking.core.dao.VikingNewsDao;
import com.viking.core.model.CMSMediaParagraphComponentModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
public class DefaultVikingNewsDao extends AbstractItemDao implements VikingNewsDao {

    private static final String QUERY_SITE = "SELECT {" + CMSMediaParagraphComponentModel.PK + "} FROM {" + CMSMediaParagraphComponentModel._TYPECODE +"} " +
            "WHERE {" + CMSMediaParagraphComponentModel.CATALOGVERSION + "} = ?" + CMSMediaParagraphComponentModel.CATALOGVERSION + " " +
            "ORDER BY {" + CMSMediaParagraphComponentModel.CREATIONTIME + "} DESC";

    private static final String QUERY_CATALOG_VERSION = "SELECT {" + CMSMediaParagraphComponentModel.PK + "} FROM {" + CMSMediaParagraphComponentModel._TYPECODE +"} " +
            "WHERE {" + CMSMediaParagraphComponentModel.CATALOGVERSION + "} = ?" + CMSMediaParagraphComponentModel.CATALOGVERSION + " " +
            "AND {" + CMSMediaParagraphComponentModel.UID + "} = ?" + CMSMediaParagraphComponentModel.UID;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CMSMediaParagraphComponentModel> getNewsForSite(final BaseSiteModel site) {

        final CatalogVersionModel contentCatalog = ((CMSSiteModel) site).getContentCatalogs().get(0).getActiveCatalogVersion();

        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SITE);
        query.addQueryParameter(CMSMediaParagraphComponentModel.CATALOGVERSION, contentCatalog);

        final SearchResult<CMSMediaParagraphComponentModel> search = getFlexibleSearchService().search(query);

        final List<CMSMediaParagraphComponentModel> news = search.getResult().stream().filter(component -> CollectionUtils.isNotEmpty(component.getSlots())).collect(Collectors.toList());

        return news;
    }

    @Override
    public CMSMediaParagraphComponentModel findNewsForCatalogVersion(final String uid, final CatalogVersionModel catalogVersion) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SITE);
        query.addQueryParameter(CMSMediaParagraphComponentModel.CATALOGVERSION, catalogVersion);
        query.addQueryParameter(CMSMediaParagraphComponentModel.UID, uid);

        final SearchResult<CMSMediaParagraphComponentModel> search = getFlexibleSearchService().search(query);

        if (search.getResult().size() == 1) {
            return search.getResult().get(0);
        }

        return null;
    }
}
