package com.viking.core.catalog.synchronization;

import com.viking.core.catalog.synchronization.strategy.SyncJobApplicableItemStrategy;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.catalog.synchronization.DefaultCatalogSynchronizationService;
import de.hybris.platform.core.model.ItemModel;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class VikingCatalogSynchronizationService extends DefaultCatalogSynchronizationService {

    private static final Logger LOG = Logger.getLogger(VikingCatalogSynchronizationService.class);

    private Collection<SyncJobApplicableItemStrategy> syncJobApplicableItemStrategies = new HashSet<>();

    @Override
    public List<ItemModel> getApplicableItems(final List<ItemModel> inputItems, final SyncItemJobModel syncItemJob) {
        List<ItemModel> itemsToSync = new ArrayList<>(inputItems);
        for (final SyncJobApplicableItemStrategy strategy : syncJobApplicableItemStrategies) {
            int itemCountBefore = itemsToSync.size();
            itemsToSync = strategy.filterItems(itemsToSync, syncItemJob);
            LOG.info("SyncJobApplicableItemStrategy " + strategy.getClass().getSimpleName() + " filtered " + (itemCountBefore - itemsToSync.size()) + " items");
        }
        return super.getApplicableItems(itemsToSync, syncItemJob);
    }

    public void setSyncJobApplicableItemStrategies(final Collection<SyncJobApplicableItemStrategy> syncJobApplicableItemStrategies) {
        this.syncJobApplicableItemStrategies = syncJobApplicableItemStrategies;
    }
}
