package com.viking.core.interceptors;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Prepare interceptor for {@link MediaContainerModel}. It adds channels if not present from its MediaContainer
 *
 * @author chiranjit
 */
public class VikingMediaContainerPrepareInterceptor implements PrepareInterceptor<MediaContainerModel> {

    public static final String VIKING_CONVERSION_GROUP = "VikingConversionGroup";
    private static final Logger LOG = Logger.getLogger(VikingMediaContainerPrepareInterceptor.class);

    @Autowired
    private ModelService modelService;

    @Override
    public void onPrepare(MediaContainerModel mediaContainerModel, InterceptorContext interceptorContext) throws InterceptorException {
        if(!hasConversionGroup(mediaContainerModel)) {
            final ConversionGroupModel conversionGroupExample = new ConversionGroupModel();
            conversionGroupExample.setCode(VIKING_CONVERSION_GROUP);
            try {
                final ConversionGroupModel conversionGroupModel = modelService.getByExample(conversionGroupExample);
                mediaContainerModel.setConversionGroup(conversionGroupModel);
            } catch (ModelNotFoundException e) {
                LOG.error(e);
            }

        }
    }

    private boolean hasConversionGroup(MediaContainerModel mediaContainerModel) {
        return mediaContainerModel.getConversionGroup() != null;
    }

}
