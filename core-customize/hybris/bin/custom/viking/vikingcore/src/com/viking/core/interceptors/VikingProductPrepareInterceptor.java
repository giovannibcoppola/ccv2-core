package com.viking.core.interceptors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.assertj.core.util.Sets;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.event.VikingProductUpdateApprovalStatusEvent;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.store.BaseStoreModel;

/**
 * Prepare interceptor for {@link ProductModel}
 *
 * @author javier.gomez
 */
public class VikingProductPrepareInterceptor implements PrepareInterceptor<ProductModel> {

    private EventService eventService;
    private TimeService timeService;
    private VikingProductUpdateApprovalStatusEvent productUpdateApprovalStatusEvent;

    private static final Logger LOG = Logger.getLogger(VikingProductPrepareInterceptor.class);

    @Override
    public void onPrepare(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        if (canTriggerProductUpdateWorkflow(productModel, interceptorContext)) {
            productModel.setSapFieldModifiedTime(timeService.getCurrentTime());
            productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
        }
        if (isDefaultStagedCatalog(productModel)) {
            triggerProductApprovalStatus(productModel, interceptorContext);
        }
    }

    protected void triggerProductApprovalStatus(final ProductModel productModel, final InterceptorContext interceptorContext) {
        if (interceptorContext.isModified(productModel, ProductModel.STORELIST)) {
            final ItemModelContextImpl context = (ItemModelContextImpl) productModel.getItemModelContext();
            final ModelValueHistory history = context.getValueHistory();
            final Set<String> attributes = Sets.newHashSet(history.getDirtyAttributes());
            if (attributes.contains(ProductModel.STORELIST)) {
                List<BaseStoreModel> removedStores = getRemovedStores(context);
                updateProductStores(productModel, removedStores);
            }
        }
    }

    private List<BaseStoreModel> getRemovedStores(final ItemModelContextImpl context) {
        final Collection<?> originalValues = (Collection<ProductModel>) getOriginalValueForAttribute(context, ProductModel.STORELIST);
        final Collection<?> currentValues = (Collection<ProductModel>) context.getRawPropertyValue(ProductModel.STORELIST);
        List stores = new ArrayList(originalValues);
        stores.removeAll(currentValues);
        return stores;
    }

    private void updateProductStores(final ProductModel productModel, final List<BaseStoreModel> stores) {
        if (CollectionUtils.isNotEmpty(stores)) {
            stores.forEach(
                    store -> {
                        if (org.apache.commons.lang.StringUtils.isNotEmpty(store.getUid())) {
                            productUpdateApprovalStatusEvent = new VikingProductUpdateApprovalStatusEvent(productModel.getCode(), store.getUid());
                            eventService.publishEvent(productUpdateApprovalStatusEvent);
                        }
                    }
            );
        }
    }

    private Object getOriginalValueForAttribute(final ItemModelContextImpl ctx, final String attr) {
        return ctx.isLoaded(attr) ? ctx.getOriginalValue(attr) : ctx.loadOriginalValue(attr);
    }

    private boolean canTriggerProductUpdateWorkflow(ProductModel productModel, InterceptorContext interceptorContext) {
        boolean workflowCanBeTriggered = false;

        if (!interceptorContext.isNew(productModel)
                && CollectionUtils.isNotEmpty(productModel.getSupercategories())
                && isSAPFieldModified(productModel, interceptorContext)
                && isDefaultCatalog(productModel)) {
            workflowCanBeTriggered = true;
        }

        return workflowCanBeTriggered;
    }

    private boolean isDefaultCatalog(ProductModel productModel) {
        return productModel.getCatalogVersion() != null
                && VikingCoreConstants.DEFAULT_CATALOG.equals(productModel.getCatalogVersion().getCatalog().getId());
    }

    private boolean isDefaultStagedCatalog(ProductModel productModel) {
        return isDefaultCatalog(productModel)
                && VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME.equals(productModel.getCatalogVersion().getVersion());
    }

    protected boolean isSAPFieldModified(ProductModel productModel, InterceptorContext interceptorContext) {
        if (interceptorContext.isModified(productModel, ProductModel.PRODUCTSTATUSSAP)){
            ItemModelContext itemModelContext = productModel.getItemModelContext();
            if (!(itemModelContext instanceof ItemModelContextImpl)){
                LOG.error("ItemModelContext not of expected type ItemModelContextImpl");
                LOG.error("Can determine if set value is a new value. Will assume it is a new value");
                return true;
            }
            Object newValue = productModel.getProductStatusSAP();
            Object originalValue =  getOriginalValueForAttribute((ItemModelContextImpl) itemModelContext,  ProductModel.PRODUCTSTATUSSAP);
            return !isSame(originalValue, newValue);
        }
        return false;
    }

    protected boolean isSame(final Object originalValue, final Object newValue) {
         if (originalValue != null) {
             return originalValue.equals(newValue);
         } else {
            return newValue == null;
        }
    }


    /**
     * Sets event service.
     *
     * @param eventService the event service
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * Gets event service.
     *
     * @return the event service
     */
    public EventService getEventService() {
        return this.eventService;
    }

    /**
     * Gets time service.
     *
     * @return the time service
     */
    protected TimeService getTimeService() {
        return timeService;
    }

    /**
     * Sets time service.
     *
     * @param timeService the time service
     */
    public void setTimeService(final TimeService timeService) {
        this.timeService = timeService;
    }
}
