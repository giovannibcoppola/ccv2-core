package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import com.viking.core.service.VikingOrderEntryService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.sap.orderexchange.constants.OrderEntryCsvColumns;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderEntryContributor;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

/**
 * Adding Viking specific attributes to order entry
 */
public class VikingOrderEntryContributor extends DefaultOrderEntryContributor {

    private static final Logger LOG = LogManager.getLogger(VikingOrderEntryContributor.class);

    private static final String DELIVERY_PLANT_CODE = "deliveryPlant";
    private static final String YOR_DELIVERY_PLANT = "1110";

    private VikingOrderEntryService vikingOrderEntryService;
    private VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService;

    @Autowired
    private EnquiryService enquiryService;

    @Override
    public Set<String> getColumns() {
        final Set<String> columns = super.getColumns();
        columns.add(DELIVERY_PLANT_CODE);
        columns.add(VikingCoreConstants.INQUIRY);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel order) {
        final List<Map<String, Object>> result = super.createRows(order);

        prepareMaterialNumbersForSAP(result);

        final List<AbstractOrderEntryModel> entries = order.getEntries();

        for (final AbstractOrderEntryModel entry : entries) {
            final Map<String, Object> row = new HashMap<>();

            // Add the CanonicalOrderItem key attributes
            row.put(OrderCsvColumns.ORDER_ID, order.getCode());
            row.put(OrderEntryCsvColumns.ENTRY_NUMBER, entry.getEntryNumber());

            //Handle plant
            String plant = "";

            if (order.getUser() instanceof B2BCustomerModel) {
                final B2BCustomerModel b2bCustomer = (B2BCustomerModel) order.getUser();
                final B2BUnitModel b2bUnit = b2bCustomer.getDefaultB2BUnit();

                if (vikingOrderEntryService.isCertificateEntry(entry)) {
                    plant = YOR_DELIVERY_PLANT;
                } else if (b2bUnit.getSapPlant() != null) {
                    plant = b2bUnit.getSapPlant().getCode();
                } else {
                    LOG.error("No plant found for B2BCustomers B2BUnit. Please maintain B2BUnit");
                }
            } else {
                plant = entry.getOrder().getStore().getSapPlant().getCode();
            }

            if (order instanceof VikingEnquiryOrderModel) {
                EnquiryReceiverEmailModel enquiryReceiver = vikingEnquiryReceiverEmailService.findEnquiryNotificationInfo(order);
                row.put(DELIVERY_PLANT_CODE, enquiryReceiver.getPlant());
            }else{
                row.put(DELIVERY_PLANT_CODE, plant);
            }
            LOG.debug("Plant id:" + plant);

            row.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));

            try {
                result.add(row);
            } catch (Exception e) {
                LOG.error("Cant add to result of createRows in CustomerDeliveryPlantContributor.");
            }
        }

        return result;
    }

    private void prepareMaterialNumbersForSAP(final List<Map<String, Object>> result) {
        result.forEach(map -> {
            Object productCode = map.get(OrderEntryCsvColumns.PRODUCT_CODE);

            if (productCode instanceof String && containsNumericsOnly((String) productCode)) {
                map.put(OrderEntryCsvColumns.PRODUCT_CODE, StringUtils.leftPad((String) productCode, 18, "0"));
            }
        });
    }

    private boolean containsNumericsOnly(String productCode) {
        return productCode.matches("[0-9]+");
    }

    @Autowired
    public void setVikingOrderEntryService(VikingOrderEntryService vikingOrderEntryService) {
        this.vikingOrderEntryService = vikingOrderEntryService;
    }

    @Autowired
    public void setVikingEnquiryReceiverEmailService(VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService) {
        this.vikingEnquiryReceiverEmailService = vikingEnquiryReceiverEmailService;
    }
}
