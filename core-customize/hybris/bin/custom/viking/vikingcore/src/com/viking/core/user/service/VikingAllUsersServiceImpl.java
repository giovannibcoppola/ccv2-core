/**
 *
 */
package com.viking.core.user.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;

import java.util.List;

import javax.annotation.Resource;

import com.viking.core.model.VikingVesselModel;
import com.viking.core.user.dao.VikingUserPage;


/**
 * @author Srikanth
 *
 */
public class VikingAllUsersServiceImpl implements VikingUserPageService
{
	@Resource(name = "vikingUserPage")
	VikingUserPage vikingUserPage;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.service.VikingUserPage#getAllUsers()
	 */
	@Override
	public List<B2BCustomerModel> getAllUsers()
	{
		// YTODO Auto-generated method stub
		final List<B2BCustomerModel> allUsers = vikingUserPage.getAllUsers();
		return allUsers;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.service.VikingUserPageService#getVesselIds()
	 */
	@Override
	public VikingVesselModel getVesselIds(final String vesselID)
	{
		// YTODO Auto-generated method stub
		final VikingVesselModel vesselIds = vikingUserPage.getVesselIds(vesselID);
		return vesselIds;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.service.VikingUserPageService#removeUser()
	 */
	@Override
	public B2BCustomerModel removeUser(final String id)
	{
		// YTODO Auto-generated method stub
		final B2BCustomerModel removeUser = vikingUserPage.removeUser(id);
		return removeUser;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.service.VikingUserPageService#getUser(java.lang.String)
	 */
	@Override
	public B2BCustomerModel getUser(final String name)
	{
		// YTODO Auto-generated method stub
		final B2BCustomerModel user = vikingUserPage.getUser(name);
		return user;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.viking.core.user.service.VikingUserPageService#getAllVikingVessels()
	 */
	@Override
	public List<VikingVesselModel> getAllVikingVessels()
	{
		// YTODO Auto-generated method stub
		final List<VikingVesselModel> allVessels = vikingUserPage.getAllVessels();
		return allVessels;
	}

}
