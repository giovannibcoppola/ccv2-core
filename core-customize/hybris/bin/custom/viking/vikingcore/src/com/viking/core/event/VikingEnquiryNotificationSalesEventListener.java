package com.viking.core.event;

import com.viking.core.model.EnquiryConfirmationProcessModel;
import com.viking.core.model.EnquiryModel;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

public class VikingEnquiryNotificationSalesEventListener extends AbstractAcceleratorSiteEventListener<VikingEnquiryNotificationSalesEvent> {
    private static final Logger LOG = LogManager.getLogger(VikingEnquiryNotificationSalesEventListener.class);
    private static final String BCC_EMAIL = "enquiry.bcc.email";
    private ModelService modelService;
    private BusinessProcessService businessProcessService;
    private FlexibleSearchService flexibleSearchService;
    private ConfigurationService configurationService;
    private UserService userService;

    @Autowired
    private VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService;


    @Override
    protected void onSiteEvent(final VikingEnquiryNotificationSalesEvent event) {
        final OrderModel enquiry = event.getProcess().getOrder();

        // Create process
        final EnquiryConfirmationProcessModel enquiryConfirmationProcessModel = businessProcessService.createProcess(
                "enquiryConfirmationEmailProcess-" + enquiry.getCode() + "-" + System.currentTimeMillis(),
                "enquiryConfirmationEmailProcess");
        enquiryConfirmationProcessModel.setSite(enquiry.getSite());
        enquiryConfirmationProcessModel.setStore(enquiry.getStore());
        enquiryConfirmationProcessModel.setLanguage(enquiry.getLanguage());
        enquiryConfirmationProcessModel.setVikingEnquiryOrder((VikingEnquiryOrderModel) enquiry);

        EnquiryReceiverEmailModel enquiryReceiver = vikingEnquiryReceiverEmailService.findEnquiryNotificationInfo(enquiry);
        enquiryConfirmationProcessModel.setBccEmail(enquiryReceiver.getReceiverEmail());

        enquiryConfirmationProcessModel.setUser(userService.getAdminUser());
        modelService.save(enquiryConfirmationProcessModel);

        // Start process
        businessProcessService.startProcess(enquiryConfirmationProcessModel);

        LOG.info("Started enquiryConfirmationEmailProcess for enqiury: " + enquiry.getCode() );
    }

    @Override
    protected SiteChannel getSiteChannelForEvent(final VikingEnquiryNotificationSalesEvent event) {
        final OrderModel order = event.getProcess().getOrder();
        ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
        final BaseSiteModel site = order.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
        return site.getChannel();
    }

    @Required
    public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setConfigurationService(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
