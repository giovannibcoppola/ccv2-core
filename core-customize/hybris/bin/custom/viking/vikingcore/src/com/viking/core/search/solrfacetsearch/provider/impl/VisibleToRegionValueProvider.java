package com.viking.core.search.solrfacetsearch.provider.impl;

import com.viking.core.dao.RegionToBaseStoreDao;
import com.viking.core.model.RegionToBaseStoreModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class VisibleToRegionValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider {
    private RegionToBaseStoreDao regionToBaseStoreDao;
    private BaseStoreService baseStoreService;
    private FieldNameProvider fieldNameProvider;

    @Override
    public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
                                                 final Object model) {
        if (model != null && model instanceof ProductModel) {
            final List<BaseStoreModel> visibleToBaseStores = findVisibleToBaseStores((ProductModel) model);

            if (CollectionUtils.isNotEmpty(visibleToBaseStores)) {
                final Collection<FieldValue> fieldValues = new ArrayList<>();

                for (final BaseStoreModel baseStore : visibleToBaseStores) {
                    fieldValues.addAll(createFieldValue(baseStore, indexedProperty));
                }

                return fieldValues;
            }
        }

        return Collections.emptyList();
    }

    private List<BaseStoreModel> findVisibleToBaseStores(final ProductModel model) {
        final Set<BaseStoreModel> blacklistedBaseStores = model.getBlacklistedBaseStores();

        final List<BaseStoreModel> allBaseStores = baseStoreService.getAllBaseStores();
        final List<BaseStoreModel> visibleToBaseStores = allBaseStores.stream()
                .filter(store -> "publicwebshop".equalsIgnoreCase(store.getCmsSites().iterator().next().getUid())).collect(toList());

        visibleToBaseStores.removeAll(blacklistedBaseStores);

        return visibleToBaseStores;
    }

    protected List<FieldValue> createFieldValue(final BaseStoreModel baseStore, final IndexedProperty indexedProperty) {
        final List<FieldValue> fieldValues = new ArrayList<>();

        final Optional<RegionToBaseStoreModel> regionToBaseStoreOptional = regionToBaseStoreDao.findRegionToBaseStoreByBaseStore(baseStore);

        Object value;

        if (regionToBaseStoreOptional.isPresent()) {
            value = regionToBaseStoreOptional.get().getRegionIsocode();
        } else {
            value = "Global";
        }

        final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);

        for (final String fieldName : fieldNames) {
            fieldValues.add(new FieldValue(fieldName, value));
        }

        return fieldValues;
    }

    @Required
    public void setFieldNameProvider(final FieldNameProvider fieldNameProvider) {
        this.fieldNameProvider = fieldNameProvider;
    }

    @Required
    public void setRegionToBaseStoreDao(RegionToBaseStoreDao regionToBaseStoreDao) {
        this.regionToBaseStoreDao = regionToBaseStoreDao;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}
