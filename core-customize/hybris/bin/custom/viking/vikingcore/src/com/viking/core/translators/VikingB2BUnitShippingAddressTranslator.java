package com.viking.core.translators;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;

public class VikingB2BUnitShippingAddressTranslator extends AbstractSpecialValueTranslator {

    private static final Logger LOG = Logger.getLogger(VikingB2BUnitShippingAddressTranslator.class);

    private DefaultModelService modelService;


    @Override
    public void performImport(final String isShippingAddress, final Item processedItem) throws ImpExException {
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if(BooleanUtils.toBoolean(isShippingAddress)) {
            try {
                final Address address =(Address) processedItem;
                final AddressModel addressModel = modelService.get(address);
                final B2BUnitModel b2bUnit = (B2BUnitModel)addressModel.getOwner();

               if(b2bUnit != null) {
                   b2bUnit.setShippingAddress(addressModel);
                   modelService.save(b2bUnit);
               }
            } catch (Exception e) {
                LOG.error("Could not set default Shipping address for the unit, the address is : "+ processedItem);
            }
        }
    }

}
