package com.viking.core.service.impl;

import com.viking.core.dao.VikingIncotermDao;
import com.viking.core.model.VikingIncotermModel;
import com.viking.core.service.VikingIncotermService;
import de.hybris.platform.sap.core.jco.connection.JCoManagedConnectionContainer;
import de.hybris.platform.sap.core.jco.connection.impl.JCoConnectionStateless;

import javax.annotation.Resource;
import java.util.List;

public class DefaultVikingIncotermService implements VikingIncotermService {

    private VikingIncotermDao vikingIncotermDao;

    @Override
    public List<VikingIncotermModel> findVikingIncotermForCode(String code) {
        return getVikingIncotermDao().findVikingIncotermForCode(code);
    }

    @Override
    public List<VikingIncotermModel> findAllVikingIncoterms() {
        return getVikingIncotermDao().findAllVikingIncoterms();
    }

    public VikingIncotermDao getVikingIncotermDao() {
        return vikingIncotermDao;
    }

    public void setVikingIncotermDao(VikingIncotermDao vikingIncotermDao) {
        this.vikingIncotermDao = vikingIncotermDao;
    }
}
