package com.viking.core.service;

import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

public class VikingPriceInformation extends PriceInformation {

    private TaxValue tax;
    private String uom;


    public VikingPriceInformation(final PriceValue price, final TaxValue tax, final String uom) {
        super(price);
        this.tax = tax;
        this.uom = uom;
    }

    public TaxValue getTax() {
        return tax;
    }

    public void setTax(TaxValue tax) {
        this.tax = tax;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
}
