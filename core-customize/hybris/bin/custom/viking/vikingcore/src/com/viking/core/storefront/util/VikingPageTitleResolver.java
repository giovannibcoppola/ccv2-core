package com.viking.core.storefront.util;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSContentPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.core.model.product.ProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Viking implementation of {@link PageTitleResolver}
 *
 * @author javier.gomez
 */
public class VikingPageTitleResolver extends PageTitleResolver {

    private CMSContentPageService cmsContentPageService;

    public String resolveContentPageTitle(final AbstractPageModel pageModel) {
        final StringBuilder builder = new StringBuilder();

        if (StringUtils.isNotEmpty(pageModel.getTitle())) {
            builder.append(pageModel.getTitle());
        } else {
            builder.append(StringUtils.capitalize(pageModel.getName())).append(TITLE_WORD_SEPARATOR).append(VikingCoreConstants.VIKING);
        }

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    public String resolveHomePageTitle() {
        final StringBuilder builder = new StringBuilder();

        AbstractPageModel homepage = getCmsContentPageService().getHomepage();
        if (StringUtils.isNotEmpty(homepage.getTitle())) {
            builder.append(homepage.getTitle());
        } else {
            builder.append(StringUtils.capitalize(homepage.getName())).append(TITLE_WORD_SEPARATOR).append(VikingCoreConstants.VIKING);
        }

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    @Override
    public String resolveCategoryPageTitle(final CategoryModel category) {
        final StringBuilder stringBuilder = new StringBuilder();
        final List<CategoryModel> categories = this.getCategoryPath(category);
        for (final CategoryModel c : categories) {
            stringBuilder.append(StringUtils.capitalize(c.getName())).append(TITLE_WORD_SEPARATOR);
        }

        stringBuilder.append(VikingCoreConstants.VIKING);

        return StringEscapeUtils.escapeHtml(stringBuilder.toString());
    }

    @Override
    public String resolveProductPageTitle(final ProductModel product) {
        // Lookup categories
        final List<CategoryModel> path = getCategoryPath(getProductAndCategoryHelper().getBaseProduct(product));

        // Construct page title
        final StringBuilder builder = new StringBuilder();

        // To get lowest category level of the product
        if (CollectionUtils.isNotEmpty(path)) {
            builder.append(StringUtils.capitalize(path.iterator().next().getName())).append(TITLE_WORD_SEPARATOR);
        }

        builder.append(product.getCode()).append(TITLE_WORD_SEPARATOR).append(VikingCoreConstants.VIKING);

        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    /**
     * For a category, we just want to show the 2 lowest levels of the category path
     *
     * @param category Category
     * @return the list of categories on the hierarchy of the current category
     */
    @Override
    protected List<CategoryModel> getCategoryPath(final CategoryModel category) {
        final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
        // Return first - there will always be at least 1
        List<CategoryModel> cat2ret = paths.iterator().next();
        if (CollectionUtils.isNotEmpty(cat2ret)) {
            Collections.reverse(cat2ret);
            if (cat2ret.size() > 2) {
                return cat2ret.subList(0, 2);
            } else {
                return cat2ret;
            }
        }

        return Collections.emptyList();
    }

    /**
     * For a product, we just want to show the Level-3 category of the category path.
     * E.g if the product belongs to category4 and the hierarchy is category1 - category2 - category3 - category4, we have to return category3 - category2 - category1, so in the title we will just show category3.
     * E.g if the product belongs to category2 and the hierarchy is category1 - category2 - category3 - category4, we have to return category2 - category1, so in the title we will just show category2.
     *
     * @param product Product
     * @return the list of categories on the hierarchy of the current category
     */
    @Override
    protected List<CategoryModel> getCategoryPath(final ProductModel product) {
        final CategoryModel category = getPrimaryCategoryForProduct(product);
        if (category != null) {
            final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
            // Return first - there will always be at least 1
            List<CategoryModel> cat2ret = paths.iterator().next();
            if (CollectionUtils.isNotEmpty(cat2ret)) {
                final List<CategoryModel> topThreeCategories = cat2ret.size() > 3 ? cat2ret.subList(0, 3) : cat2ret;
                Collections.reverse(topThreeCategories);
                return topThreeCategories;
            }
        }

        return Collections.emptyList();
    }

    public <STATE> String resolveSearchPageTitle(final String searchText) {
        final StringBuilder builder = new StringBuilder();
        if (!StringUtils.isEmpty(searchText)) {
            builder.append(searchText).append(TITLE_WORD_SEPARATOR);
        }
        builder.append(VikingCoreConstants.VIKING);
        return StringEscapeUtils.escapeHtml(builder.toString());
    }

    protected CMSContentPageService getCmsContentPageService() {
        return this.cmsContentPageService;
    }

    @Required
    public void setCmsContentPageService(CMSContentPageService cmsContentPageService) {
        this.cmsContentPageService = cmsContentPageService;
    }
}
