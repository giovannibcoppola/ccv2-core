package com.viking.core.dao;

import com.viking.core.model.VikingIncotermModel;

import java.util.List;

public interface VikingIncotermDao{
    List<VikingIncotermModel> findVikingIncotermForCode(String code);

    List<VikingIncotermModel> findAllVikingIncoterms();
}
