package com.viking.core.dao.impl;

import com.viking.core.dao.VikingPointOfServiceDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.storelocator.impl.DefaultPointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Optional;

/**
 * {@inheritDoc}
 */
public class DefaultVikingPointOfServiceDao extends DefaultPointOfServiceDao implements VikingPointOfServiceDao {

    private static final String DISPLAY_NAME_QUERY = "SELECT {" + PointOfServiceModel.PK + "} FROM {" + PointOfServiceModel._TYPECODE + "} " +
            "WHERE {" + PointOfServiceModel.DISPLAYNAME + "} = ?" + PointOfServiceModel.DISPLAYNAME;

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<PointOfServiceModel> getPosByDisplayName(final String name) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(DISPLAY_NAME_QUERY);
        query.addQueryParameter(PointOfServiceModel.DISPLAYNAME, name);

        final List<Object> result = getFlexibleSearchService().search(query).getResult();

        return CollectionUtils.isNotEmpty(result) ? Optional.of((PointOfServiceModel) result.get(0)) : Optional.empty();
    }
}
