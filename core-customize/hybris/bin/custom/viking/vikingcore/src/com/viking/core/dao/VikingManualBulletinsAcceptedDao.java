package com.viking.core.dao;

import com.viking.core.jalo.ManualBulletinsAccepted;


import java.util.List;

public interface VikingManualBulletinsAcceptedDao {
    List<ManualBulletinsAccepted> getManualBulletinsAccpted(String bulletinsId, String manualId, String userId );

}
