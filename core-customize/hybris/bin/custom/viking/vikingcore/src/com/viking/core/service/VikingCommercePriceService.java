package com.viking.core.service;

import de.hybris.platform.commerceservices.price.CommercePriceService;

public interface VikingCommercePriceService extends CommercePriceService {
}
