package com.viking.core.event;

import com.viking.core.jalo.EnquiryReceiverEmail;
import com.viking.core.model.EnquiryConfirmationProcessModel;
import com.viking.core.model.EnquiryModel;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class EnquirySubmitEventListener extends AbstractAcceleratorSiteEventListener<EnquirySubmitEvent> {

    private static final Logger LOG = LogManager.getLogger(EnquirySubmitEventListener.class);

    private static final String BCC_EMAIL = "enquiry.bcc.email";
    public static final String SAP_ENQUIRY_PROCESS = "sap-enquiry-process";

    private ModelService modelService;
    private BusinessProcessService businessProcessService;


    @Override
    protected SiteChannel getSiteChannelForEvent(EnquirySubmitEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }

    @Override
    protected void onSiteEvent(EnquirySubmitEvent event) {

        // Get query from event
        final VikingEnquiryOrderModel enquiry = event.getEnquiry();

        final String processCode = SAP_ENQUIRY_PROCESS + "-" + enquiry.getCode() + "-" + System.currentTimeMillis();
        final OrderProcessModel businessProcessModel = businessProcessService.createProcess(processCode,
                SAP_ENQUIRY_PROCESS);
        businessProcessModel.setOrder(enquiry);
        modelService.save(businessProcessModel);
        businessProcessService.startProcess(businessProcessModel);

        LOG.info(String.format("Started the process %s", processCode));

    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

}
