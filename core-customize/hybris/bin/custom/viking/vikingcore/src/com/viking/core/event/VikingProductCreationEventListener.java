package com.viking.core.event;

/**
 * Listener that handles {@link VikingProductCreationEvent}
 *
 * @author javier.gomez
 */
public class VikingProductCreationEventListener extends AbstractVikingProductEventListener<VikingProductCreationEvent> {

    private static final String PRODUCT_CREATION_WORKFLOW_TEMPLATE_CODE = "VIKING_PR_CREATION";

    @Override
    protected void onEvent(VikingProductCreationEvent event) {
        if (event != null && event.getProductModel() != null && isWorkflowNeeded(event.getProductModel())) {
            triggerProductApprovalWorkflow(event.getProductModel(), PRODUCT_CREATION_WORKFLOW_TEMPLATE_CODE);
        }
    }

}
