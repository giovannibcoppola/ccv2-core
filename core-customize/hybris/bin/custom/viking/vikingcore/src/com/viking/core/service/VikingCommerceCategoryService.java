package com.viking.core.service;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;

import java.util.Optional;

/**
 * Custom interface for CommerceCategoryService
 *
 * @author javier.gomez
 */
public interface VikingCommerceCategoryService extends CommerceCategoryService {

    /**
     * Method that returns the CategoryModel for the given categoryCode if the current user has right to see it.
     *
     * @param categoryCode Category code
     * @return Optional of CategoryModel
     */
    Optional<CategoryModel> getCategoryAuthorizedForUser(String categoryCode);

    Optional<String> getStoreIdForCategory(final CategoryModel categoryModel);
}
