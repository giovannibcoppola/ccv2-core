package com.viking.core.attributehandlers;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

public class VikingAddressLine2Attribute extends AbstractDynamicAttributeHandler<String, AddressModel> {

    @Override
    public String get(AddressModel addressModel) {
        if (addressModel == null) {
            throw new IllegalArgumentException("address model is required");
        } else {
            return addressModel.getAdditionalLine();
        }
    }

    @Override
    public void set(AddressModel addressModel, String value) {
        if (addressModel != null) {
            addressModel.setAdditionalLine(value);
        }

    }
}
