/**
 *
 */
package com.viking.core.integration.servicenow.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.viking.core.integration.servicenow.data.VesselData;
import com.viking.core.integration.servicenow.data.VesselEquipmentData;
import com.viking.core.integration.servicenow.data.VesselEquipmentDataWrapper;
import com.viking.core.model.VikingVesselModel;


/**
 * @author Prabhakar
 *
 */
public interface VikingVesselServiceNowService
{

	List<VesselData> getAllVesselDataList(String customerId) throws ClientProtocolException, IOException;

	VesselEquipmentDataWrapper getVesselEquipmentData(String imoId, String customerId) throws ClientProtocolException, IOException;

	List<VikingVesselModel> getAssignedVesselById(final String userId);
}
