package com.viking.core.dao;

import com.viking.core.model.CMSMediaParagraphComponentModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

/**
 * DAO to get news components
 */
public interface VikingNewsDao {

    /**
     * Get news for specified site
     *
     * @param site         Site to use
     * @return
     */
    List<CMSMediaParagraphComponentModel> getNewsForSite(final BaseSiteModel site);

    /**
     * Find the news for give uid and catalog version
     * @param uid
     * @param catalogVersion
     * @return
     */
    CMSMediaParagraphComponentModel findNewsForCatalogVersion(String uid, CatalogVersionModel catalogVersion);
}
