package com.viking.core.service;

import com.viking.core.model.FlagStateModel;

import java.util.List;

/**
 * Flagstate service
 */
public interface VikingFlagStateService {

    /**
     * Get all Flagstates
     *
     * @return
     */
    List<FlagStateModel> getAll();
}
