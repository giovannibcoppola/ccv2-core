package com.viking.core.event;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.PublishEventContext;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import org.springframework.util.CollectionUtils;

/**
 * Event to handle Viking Product Update Workflow
 *
 * @author javier.gomez
 */
public class VikingProductUpdateEvent extends AbstractEvent {

    private ProductModel productModel;

    public VikingProductUpdateEvent() {

    }

    public VikingProductUpdateEvent(ProductModel productModel) {
        this.productModel = productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public ProductModel getProductModel() {
        return productModel;
    }
}
