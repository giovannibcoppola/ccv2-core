package com.viking.core.outbound.impl;

import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import com.viking.core.service.VikingOrderEntryService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderContributor;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.viking.core.constants.VikingCoreConstants.SERVICEPARTNERPORTAL_UID;

/**
 * Adding Viking specific attributes to outbound order
 */
public class VikingOrderContributor extends DefaultOrderContributor {

    private static final Logger LOG = LogManager.getLogger(VikingOrderContributor.class);

    private static final String INCOTERMS1_CODE = "incoterms1";
    private static final String INCOTERMS2_CODE = "incoterms2";
    private static final String TRANSPORT_ZONE_CODE = "transportZone";

    private static final String PORTAL_IDENTIFIER = "portalIdentifier";
    private static final String PURCHASE_ORDER = "purchaseOrder";
    private static final String DEFAULT_B2C_TRANSPORTZONE = "0000000001";
    public static final String COULD_NOT_DETERMINE_PLANT_FOR_NON_B_2_B_CUSTOMER = "could not determine plant for non b2bCustomer";
    protected static final String EXW = "EXW";
    protected static final String EX_WORKS = "Ex-Works";
    protected static final String DOC_TYPE_YOR = "YOR";
    protected static final String SALES_ORG = "salesOrg";
    public static final String UNKNOWN_TRANSP_ZONE = "unknownTranspZone";

    private VikingOrderEntryService vikingOrderEntryService;
    private VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService;

    @Autowired
    private EnquiryService enquiryService;


    @Override
    public Set<String> getColumns() {
        final Set<String> columns = super.getColumns();
        columns.add(INCOTERMS1_CODE);
        columns.add(INCOTERMS2_CODE);
        columns.add(TRANSPORT_ZONE_CODE);
        columns.add(SALES_ORG);
        columns.add(PORTAL_IDENTIFIER);
        columns.add(PURCHASE_ORDER);
        columns.add(VikingCoreConstants.INQUIRY);
        return columns;
    }

    @Override
    public List<Map<String, Object>> createRows(final OrderModel order) {
        LOG.info("Datahub Order Extension code reached. Adding inco1 + inco2 + tranportZone");

        final List<Map<String, Object>> rowsList = super.createRows(order);

        // There will only ever be one row returned from super.
        final Map<String, Object> rows = rowsList.get(0);

        setIncoterms(order, rows);

        setTransportZone(order, rows);

        setSalesOrg(order, rows);

        setPurchaseOrder(order, rows);

        rows.put(PORTAL_IDENTIFIER, order.getSite().getPortalId());

        rows.put(VikingCoreConstants.INQUIRY, enquiryService.isEnquiryOrder(order));

        return rowsList;
    }

    private void setIncoterms(final OrderModel order, final Map<String, Object> rows) {
        String inco1 = StringUtils.EMPTY;
        String inco2 = StringUtils.EMPTY;
        if (order.getSite() != null && SERVICEPARTNERPORTAL_UID.equals(order.getSite().getUid())) {
            inco1 = EXW;
            inco2 = EX_WORKS;

            //Add YOR ordertype to inco2. Needed for Idoc. removed just before send to SAP.
            final boolean isYOROrderType = order.getEntries().stream().anyMatch(item -> vikingOrderEntryService.isCertificateEntry(item));
            if (isYOROrderType) {
                inco2 = DOC_TYPE_YOR + inco2;
            }
        } else if (order instanceof VikingEnquiryOrderModel) {
            //inco1 = order.getStore().getCustomerNumber();
            inco2 = COULD_NOT_DETERMINE_PLANT_FOR_NON_B_2_B_CUSTOMER;
        } else {
            LOG.error(MessageFormat.format("No delivery mode or incoterm found for order with code {0}", order.getCode()));
        }
        LOG.debug(MessageFormat.format("Incoterms 1: {0}", inco1));
        LOG.debug(MessageFormat.format("Incoterms 2: {0}", inco2));
        rows.put(INCOTERMS1_CODE, inco1);
        rows.put(INCOTERMS2_CODE, inco2);
    }

   private void setSalesOrg( final OrderModel order, final Map<String, Object> rows ){
        String salesOrg;
        B2BUnitModel b2bUnitModel = null;

        if (order.getUser() instanceof B2BCustomerModel) {
            b2bUnitModel = ((B2BCustomerModel) order.getUser()).getDefaultB2BUnit();
        }

        if (order instanceof VikingEnquiryOrderModel) {
            EnquiryReceiverEmailModel enquiryReceiver = vikingEnquiryReceiverEmailService.findEnquiryNotificationInfo(order);
            salesOrg = enquiryReceiver.getSalesOrg();
        } else {
            if (b2bUnitModel != null && b2bUnitModel.getSapSalesOrganization() != null) {
                salesOrg =  b2bUnitModel.getSapSalesOrganization().getCode();
            } else if (order.getStore().getSapSalesOrganization() != null) {
                salesOrg = order.getStore().getSapSalesOrganization().getCode();
            } else {
                salesOrg = order.getStore().getSAPConfiguration().getSapcommon_salesOrganization();
            }
        }

        rows.put(SALES_ORG, salesOrg);
       LOG.debug(MessageFormat.format("SalesOrg: {0}", salesOrg));
    }

    private void setTransportZone(final OrderModel order, final Map<String, Object> rows) {
        String transportZone;
        if (order.getUnit() != null) {
            transportZone = order.getUnit().getTransportZone();
        } else if (SiteChannel.B2C.equals(order.getSite().getChannel())) {
            transportZone = DEFAULT_B2C_TRANSPORTZONE;
        } else if (order instanceof VikingEnquiryOrderModel) {
            transportZone = DEFAULT_B2C_TRANSPORTZONE;
        } else {
            transportZone = UNKNOWN_TRANSP_ZONE;
        }
        LOG.debug(MessageFormat.format("TransportZone: {0}", transportZone));
        rows.put(TRANSPORT_ZONE_CODE, transportZone);
    }

    private void setPurchaseOrder(final OrderModel order, final Map<String, Object> rows) {
        if (order.getPurchaseOrderNumber() != null) {
            rows.put(PURCHASE_ORDER, order.getPurchaseOrderNumber());
        } else {
            rows.put(PURCHASE_ORDER, StringUtils.EMPTY);
        }
        LOG.debug(MessageFormat.format("PurchaseOrderNumber: {0}", rows.get(PURCHASE_ORDER)));
    }

    @Autowired
    public void setVikingOrderEntryService(VikingOrderEntryService vikingOrderEntryService) {
        this.vikingOrderEntryService = vikingOrderEntryService;
    }

    @Autowired
    public void setVikingEnquiryReceiverEmailService(VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService) {
        this.vikingEnquiryReceiverEmailService = vikingEnquiryReceiverEmailService;
    }
}
