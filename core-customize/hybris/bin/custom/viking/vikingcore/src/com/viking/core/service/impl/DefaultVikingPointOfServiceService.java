package com.viking.core.service.impl;

import com.viking.core.dao.VikingPointOfServiceDao;
import com.viking.core.service.VikingPointOfServiceService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.impl.DefaultPointOfServiceService;
import org.springframework.beans.factory.annotation.Required;

import java.util.Optional;

/**
 * {@inheritDoc}
 */
public class DefaultVikingPointOfServiceService extends DefaultPointOfServiceService implements VikingPointOfServiceService {

    private VikingPointOfServiceDao vikingPointOfServiceDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<PointOfServiceModel> getPointOfServiceByDisplayName(final String name) {
        return vikingPointOfServiceDao.getPosByDisplayName(name);
    }

    @Required
    public void setVikingPointOfServiceDao(VikingPointOfServiceDao vikingPointOfServiceDao) {
        this.vikingPointOfServiceDao = vikingPointOfServiceDao;
    }
}
