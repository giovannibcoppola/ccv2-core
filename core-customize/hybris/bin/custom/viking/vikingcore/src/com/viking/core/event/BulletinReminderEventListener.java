package com.viking.core.event;

import com.viking.core.model.BulletinReminderProcessModel;
import com.viking.core.model.EnquiryConfirmationProcessModel;
import com.viking.core.model.EnquiryModel;
import com.viking.core.model.EnquiryReceiverEmailModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

public class BulletinReminderEventListener extends AbstractAcceleratorSiteEventListener<BulletinReminderEvent> {

    private ModelService modelService;
    private BusinessProcessService businessProcessService;
    private CommonI18NService commonI18NService;
    private BaseStoreService baseStoreService;

    @Override
    protected SiteChannel getSiteChannelForEvent(BulletinReminderEvent event) {
        final BaseSiteModel site = event.getSite();
        ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
        return site.getChannel();
    }

    @Override
    protected void onSiteEvent(BulletinReminderEvent event) {
        // Create process
        final BulletinReminderProcessModel process = businessProcessService.createProcess(
                "bulletinReminderProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
                "bulletinReminderEmailProcess");
        process.setSite(event.getSite());
        process.setStore(baseStoreService.getBaseStoreForUid("partner"));
        process.setLanguage(commonI18NService.getCurrentLanguage());
        process.setCustomer(event.getCustomer());
        process.setManualsToEmail(event.getManualsToEmail());
        modelService.save(process);

        // Start process
        businessProcessService.startProcess(process);
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Required
    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    @Required
    public void setCommonI18NService(CommonI18NService commonI18NService) {
        this.commonI18NService = commonI18NService;
    }

    @Required
    public void setBaseStoreService(BaseStoreService baseStoreService) {
        this.baseStoreService = baseStoreService;
    }
}
