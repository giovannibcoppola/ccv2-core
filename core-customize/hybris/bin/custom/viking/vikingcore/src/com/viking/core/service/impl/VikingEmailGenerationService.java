package com.viking.core.service.impl;

import com.viking.core.model.ApprovedFacilityModel;
import com.viking.core.model.EnquiryConfirmationProcessModel;
import com.viking.core.model.NewsEmailProcessModel;
import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class VikingEmailGenerationService extends DefaultEmailGenerationService {

    private ModelService modelService;

    @Override
    public EmailMessageModel generate(final BusinessProcessModel businessProcessModel, final EmailPageModel emailPageModel) {
        final EmailMessageModel email = super.generate(businessProcessModel, emailPageModel);
        Optional<ApprovedFacilityModel> facility = Optional.empty();

        if (businessProcessModel instanceof EnquiryConfirmationProcessModel) {
            final String bccEmail = ((EnquiryConfirmationProcessModel) businessProcessModel).getBccEmail();
            final EmailAddressModel bccAddress = getEmailService().getOrCreateEmailAddressForEmail(bccEmail, "VIKING Life-Saving Equipment A/S");
            email.setBccAddresses(Collections.singletonList(bccAddress));
            modelService.save(email);

        } else if(businessProcessModel instanceof NewsEmailProcessModel){
            facility = Optional.of(((B2BCustomerModel)((NewsEmailProcessModel) businessProcessModel).getCustomer()).getApprovedFacility());
        } else if(businessProcessModel instanceof OrderProcessModel){
            facility = Optional.of(((B2BCustomerModel)((OrderProcessModel) businessProcessModel).getOrder().getUser()).getApprovedFacility());
        }

        if(facility.isPresent() && facility.get().getEmail() != null) {
            prepareEmailAddress(email, facility.get());
        }

        return email;
    }

    private void prepareEmailAddress(EmailMessageModel email, ApprovedFacilityModel facility) {
        final List<EmailAddressModel> bccAddress = new ArrayList<>();
        final EmailAddressModel emailTO = getOrCreateEmailAddressForEmail(facility.getEmail());

        addBccAddress(bccAddress, facility.getAdditionalEmail2());
        addBccAddress(bccAddress, facility.getAdditionalEmail3());
        addBccAddress(bccAddress, facility.getAdditionalEmail4());
        addBccAddress(bccAddress, facility.getAdditionalEmail5());
        addBccAddress(bccAddress, facility.getAdditionalEmail6());

        email.setToAddresses(Collections.singletonList(emailTO));
        email.setBccAddresses(bccAddress);
        modelService.save(email);
    }

    private EmailAddressModel getOrCreateEmailAddressForEmail(String email) {
        return StringUtils.isNotBlank(email) ? getEmailService().getOrCreateEmailAddressForEmail(email, "VIKING Life-Saving Equipment A/S") : null;
    }

    private void addBccAddress(List<EmailAddressModel> bccAddress, String email) {
        if(StringUtils.isNotBlank(email) ){
            bccAddress.add(getOrCreateEmailAddressForEmail(email));
        }
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }
}
