package com.viking.core.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;


public interface VikingUserService extends UserService
{
	void updateCustomerAuthorizationGroupsFromSAP();

	List<B2BCustomerModel> getServicePartnerCustomers();
}
