package com.viking.core.datahub;

import de.hybris.platform.sap.core.configuration.datahub.DataHubInitialLoadHandler;
import de.hybris.platform.sap.core.configuration.datahub.DataHubPingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.text.MessageFormat;

public class ApplicationListenerBean implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = LogManager.getLogger(ApplicationListenerBean.class);

    private DataHubPingService dataHubPingService;

    private DataHubInitialLoadHandler sapCoreDataHubInitialLoadHandler;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        LOG.info(MessageFormat.format("ContextRefreshedEvent source: {0}, context: {1} ", event.getSource().toString(), event.getApplicationContext().getDisplayName()));
        if (event.getSource().toString().startsWith("Root WebApplicationContext - /backoffice: startup date") && dataHubPingService.ping()) {
            LOG.info("Executing Initial Load of Hybris Datahub Configuration.");
            sapCoreDataHubInitialLoadHandler.executeInitialLoad();
        }
    }

    @Required
    public void setSapCoreDataHubInitialLoadHandler(final DataHubInitialLoadHandler sapCoreDataHubInitialLoadHandler) {
        this.sapCoreDataHubInitialLoadHandler = sapCoreDataHubInitialLoadHandler;
    }

    @Required
    public void setDataHubPingService(final DataHubPingService dataHubPingService) {
        this.dataHubPingService = dataHubPingService;
    }
}