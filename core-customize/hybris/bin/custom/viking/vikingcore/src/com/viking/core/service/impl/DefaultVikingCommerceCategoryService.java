package com.viking.core.service.impl;

import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.service.VikingCommerceCategoryService;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.impl.DefaultCommerceCategoryService;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static com.viking.core.constants.VikingCoreConstants.*;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Viking implementation of {@link VikingCommerceCategoryService}
 *
 * @author javier.gomez
 */
public class DefaultVikingCommerceCategoryService extends DefaultCommerceCategoryService implements VikingCommerceCategoryService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultVikingCommerceCategoryService.class);

    @Resource(name = "userService")
    private UserService userService;

    @Autowired
    private EnquiryService enquiryService;

    @Override
    public Optional<CategoryModel> getCategoryAuthorizedForUser(String categoryCode) {
        LOG.info("VIK-2750: Start of category logic for category code {}!", categoryCode);

        CategoryModel category = null;
        try {
            category = getCategoryForCode(categoryCode);
        }
        catch(Exception e) {
            LOG.error("VIK-2750: Category for code {} not found!", categoryCode, e);
            return Optional.empty();
        }

        //TO-DO should be removed once separate catalog for safetyshop  is ready.
        if(enquiryService.isEnquiryFlow()) {
            LOG.info("VIK-2750: Inquiry flow, returning category model for code {}!", categoryCode);
            return Optional.of(category);
        }

        LOG.info("VIK-2750: Checking permissions for category with code {}!", categoryCode);

        Set<PrincipalGroupModel> categoryAuthorizations = category.getAllowedPrincipals().stream().filter(pm -> pm instanceof PrincipalGroupModel).map(pm -> (PrincipalGroupModel) pm).collect(Collectors.toSet());
        Set<PrincipalGroupModel> userAuthorizations =  userService.getCurrentUser().getAllGroups();

        Set <PrincipalGroupModel> intersection = new HashSet<>(categoryAuthorizations);
        intersection.retainAll(userAuthorizations);

        for(PrincipalModel userGroup: intersection){
            if(userGroup.getUid().contains("AUTH_")){
                LOG.info("VIK-2750: End of flow, MATCH FOUND, user is allowed to see category with code {}, returning the category if it was found!", categoryCode);
                return Optional.of(category);
            }
        }

        LOG.info("VIK-2750: End of flow, NO MATCH, user not allowed to see category with code {}, returning Optional.empty()!", categoryCode);
        return Optional.empty();
    }

    @Override
    public Optional<String> getStoreIdForCategory(CategoryModel categoryModel) {
        if(!(categoryModel instanceof ClassificationClassModel)) {
            final Collection<CategoryModel> allSupercategories = categoryModel.getAllSupercategories();
            if (CollectionUtils.isNotEmpty(allSupercategories)) {
                final Optional<CategoryModel> rootCategory = allSupercategories.stream().filter(cat -> cat.isRootCategory()).findFirst();
              return  Optional.of(rootCategory.map(cat -> getStoreId(cat.getCode())).orElseGet(() -> SAP_STORE));
            }
            return Optional.of(getStoreId(categoryModel.getCode()));
        }
       return Optional.of(CLASSIFICATION_STORE);
    }

    private String getStoreId(final String categoryCode) {
        if(SAFETY_SHOP_CATEGORY_CODE.equals(categoryCode)) {
            return SAFETYSHOP_STORE;
        } else if (SERVICE_PARTNER_PORTAL_CATEGORY_CODE.equals(categoryCode)) {
            return PARTNER_STORE;
        } else if (SDS_CATEGORY_CODE.equals(categoryCode)) {
            return SDS_STORE;
        }
        return SAP_STORE;
    }
}
