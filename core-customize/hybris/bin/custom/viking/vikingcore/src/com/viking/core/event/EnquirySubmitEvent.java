package com.viking.core.event;

import com.viking.core.model.EnquiryModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

public class EnquirySubmitEvent extends AbstractCommerceUserEvent<BaseSiteModel> {

    private VikingEnquiryOrderModel enquiry;

    /**
     * Default constructor
     */
    public EnquirySubmitEvent() {
        super();
    }

    /**
     * Parameterized Constructor
     *
     * @param enquiry
     */
    public EnquirySubmitEvent(final VikingEnquiryOrderModel enquiry) {
        this.enquiry = enquiry;
    }

    public VikingEnquiryOrderModel getEnquiry() {
        return enquiry;
    }

    public void setEnquiry(VikingEnquiryOrderModel enquiry) {
        this.enquiry = enquiry;
    }
}
