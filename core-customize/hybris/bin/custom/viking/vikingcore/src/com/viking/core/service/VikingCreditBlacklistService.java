package com.viking.core.service;

import java.util.List;

/**
 * Service to handle blacklisted products
 */
public interface VikingCreditBlacklistService {

    /**
     * Find all the blacklisted products
     * @return all blacklisted product codes
     */
    List<String> getAllCreditBlacklistedProductCodes();
}
