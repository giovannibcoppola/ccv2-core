package com.viking.core.url.impl;

import com.viking.core.url.impl.util.VikingSubCategoriesRootHelper;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 * Viking implementation of CategoryModelUrlResolver extending {@link DefaultCategoryModelUrlResolver}
 *
 * @author javier.gomez
 */
public class VikingCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver {

    @Override
    protected String resolveInternal(final CategoryModel source)
    {
        // Work out values

        // Replace pattern values
        String url = getPattern();

        if (url.contains("{category-path}"))
        {
            final String categoryPath = buildPathString(getCategoryPath(source));
            if(StringUtils.isEmpty(categoryPath)) {
                url = url.replace("/{category-path}","");
            }
            else {
                url = url.replace("{category-path}", categoryPath);
            }
        }

        if (url.contains("{category-code}"))
        {
            final String categoryCode = urlEncode(source.getCode()).replaceAll("\\+", "%20");
            url = url.replace("{category-code}", categoryCode);
        }

        return url;

    }

    @Override
    protected List<CategoryModel> getCategoryPath(final CategoryModel category)
    {
        final Collection<List<CategoryModel>> paths = getCommerceCategoryService().getPathsForCategory(category);
        return VikingSubCategoriesRootHelper.getSubCategoriesOfRoot(paths);
    }
}
