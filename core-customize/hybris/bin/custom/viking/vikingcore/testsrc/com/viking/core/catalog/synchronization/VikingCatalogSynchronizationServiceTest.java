package com.viking.core.catalog.synchronization;

import com.viking.core.catalog.synchronization.strategy.SyncJobApplicableItemStrategy;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.catalog.synchronization.strategy.SyncJobApplicableTypesStrategy;
import de.hybris.platform.core.model.ItemModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingCatalogSynchronizationServiceTest {

    @InjectMocks
    private VikingCatalogSynchronizationService vikingCatalogSynchronizationService;

    @Mock
    SyncJobApplicableTypesStrategy syncJobApplicableTypesStrategy;

    @Mock
    SyncJobApplicableItemStrategy syncJobApplicableItemStrategy;

    @Test
    public void getApplicableItemsWithoutStrategy() {
        ItemModel item = mock(ItemModel.class);
        final SyncItemJobModel syncItemJob = mock(SyncItemJobModel.class);
        vikingCatalogSynchronizationService.getApplicableItems(Collections.singletonList(item), syncItemJob);
        verify(syncJobApplicableTypesStrategy).checkIfApplicable(item, syncItemJob);
    }

    @Test
    public void getApplicableItemsWithoutFiltering() {
        ItemModel item = mock(ItemModel.class);
        final SyncItemJobModel syncItemJob = mock(SyncItemJobModel.class);
        // Setup strategy without filter
        vikingCatalogSynchronizationService.setSyncJobApplicableItemStrategies(Collections.singleton(syncJobApplicableItemStrategy));
        when(syncJobApplicableItemStrategy.filterItems(Collections.singletonList(item), syncItemJob)).thenReturn(Collections.singletonList(item));

        vikingCatalogSynchronizationService.getApplicableItems(Collections.singletonList(item), syncItemJob);
        verify(syncJobApplicableItemStrategy).filterItems(Collections.singletonList(item), syncItemJob);
        verify(syncJobApplicableTypesStrategy).checkIfApplicable(item, syncItemJob);
    }

    @Test
    public void getApplicableItemsWithFiltering() {
        ItemModel item = mock(ItemModel.class);
        final SyncItemJobModel syncItemJob = mock(SyncItemJobModel.class);
        // Setup strategy with filter
        vikingCatalogSynchronizationService.setSyncJobApplicableItemStrategies(Collections.singleton(syncJobApplicableItemStrategy));
        when(syncJobApplicableItemStrategy.filterItems(Collections.singletonList(item), syncItemJob)).thenReturn(Collections.emptyList());

        vikingCatalogSynchronizationService.getApplicableItems(Collections.singletonList(item), syncItemJob);
        verify(syncJobApplicableItemStrategy).filterItems(Collections.singletonList(item), syncItemJob);
        verify(syncJobApplicableTypesStrategy, never()).checkIfApplicable(item, syncItemJob);
    }
}