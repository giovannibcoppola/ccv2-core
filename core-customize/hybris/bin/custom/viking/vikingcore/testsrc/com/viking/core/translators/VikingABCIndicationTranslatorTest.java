package com.viking.core.translators;

import com.viking.core.model.ABCIndicationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.sap.sapmodel.jalo.ERPVariantProduct;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
public class VikingABCIndicationTranslatorTest {

    @InjectMocks
    private VikingABCIndicationTranslator  vikingABCIndicationTranslator;

    @Mock
    private DefaultModelService modelService;

    @Mock
    private ABCIndicationModel abcIndicationModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_null_Code() throws JaloBusinessException {
        final Item varianytProduct = mock(Item.class);
        vikingABCIndicationTranslator.performImport(null, varianytProduct);

        verify(modelService, never()).save(any());

    }

    @Test
    public void testImport_Variant_Indication_Found() throws JaloBusinessException {
        final String cellValue = "E";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);

        when(modelService.get(any(Product.class))).thenReturn(variantModel);
        when(variantModel.getAbcIndication()).thenReturn(Arrays.asList(abcIndicationModel));
        when(abcIndicationModel.getIndication()).thenReturn("E");

        vikingABCIndicationTranslator.performImport(cellValue, variant);

        verify(modelService, never()).save(variantModel);
        verify(variantModel, never()).setAbcIndication(anyList());
        verify(modelService, never()).create(eq(ABCIndicationModel.class));
    }

    @Test
    public void testImport_Indication_Not_Found() throws JaloBusinessException {
        final String cellValue = "E";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);
        final ABCIndicationModel newAbcIndication = mock(ABCIndicationModel.class);

        when(modelService.get(any(Product.class))).thenReturn(variantModel);
        when(variantModel.getAbcIndication()).thenReturn(Arrays.asList(abcIndicationModel));
        when(abcIndicationModel.getIndication()).thenReturn("F");
        when(modelService.create(ABCIndicationModel.class)).thenReturn(newAbcIndication);

        vikingABCIndicationTranslator.performImport(cellValue, variant);

        verify(modelService, times(1)).save(variantModel);
        verify(variantModel, times(1)).setAbcIndication(eq(Arrays.asList(abcIndicationModel,newAbcIndication)));
        verify(modelService, times(1)).create(eq(ABCIndicationModel.class));
    }

    @Test
    public void testImport_Base_Indication_Empty() throws JaloBusinessException {

        final String cellValue = "E";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);
        final ABCIndicationModel newAbcIndication = mock(ABCIndicationModel.class);

        when(modelService.get(any(Product.class))).thenReturn(variantModel);
        when(modelService.create(ABCIndicationModel.class)).thenReturn(newAbcIndication);

        vikingABCIndicationTranslator.performImport(cellValue, variant);

        verify(modelService, times(1)).save(variantModel);
        verify(variantModel, times(1)).setAbcIndication(eq(Arrays.asList(newAbcIndication)));
        verify(modelService, times(1)).create(eq(ABCIndicationModel.class));
    }

}
