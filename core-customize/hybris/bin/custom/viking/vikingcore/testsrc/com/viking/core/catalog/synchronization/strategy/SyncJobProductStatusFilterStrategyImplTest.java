package com.viking.core.catalog.synchronization.strategy;

import com.viking.core.model.ProductStatusRestrictedCatalogVersionSyncJobModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SyncJobProductStatusFilterStrategyImplTest {

    @InjectMocks
    private SyncJobProductStatusFilterStrategyImpl syncJobProductStatusFilterStrategy;

    @Mock
    ProductModel item;
    @Mock
    ProductStatusRestrictedCatalogVersionSyncJobModel syncItemJob;

    @Before
    public void setUp() throws Exception {
        when(item.getCode()).thenReturn("Test product with APPROVED state");
        when(item.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
        when(syncItemJob.getExcludedApprovalStates()).thenReturn(Collections.singleton(ArticleApprovalStatus.CHECK));
    }

    @Test
    public void filterItems() {
        assertEquals(Collections.singletonList(item), syncJobProductStatusFilterStrategy.filterItems(Collections.singletonList(item), syncItemJob));
    }

    @Test
    public void filterItemsNull() {
        assertEquals(Collections.emptyList(), syncJobProductStatusFilterStrategy.filterItems(Collections.emptyList(), syncItemJob));
    }

    @Test
    public void filterItemsNonProducts() {
        CategoryModel nonProduct = mock(CategoryModel.class);
        final List<ItemModel> filteredItems = syncJobProductStatusFilterStrategy.filterItems(Collections.singletonList(nonProduct), syncItemJob);
        assertEquals(Collections.singletonList(nonProduct), filteredItems);
    }

    @Test
    public void filterItemsChecked() {
        when(item.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
        final List<ItemModel> filteredItems = syncJobProductStatusFilterStrategy.filterItems(Collections.singletonList(item), syncItemJob);
        assertEquals(Collections.emptyList(), filteredItems);
    }
}