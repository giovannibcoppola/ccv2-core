package com.viking.core.interceptors;

import com.viking.core.enums.Channel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import static org.mockito.Mockito.*;

/**
 * Unit test to cover class {@link VikingMediaPrepareInterceptor}
 *
 * @author chiranjit
 */
@UnitTest
public class VikingMediaPrepareInterceptorTest {

    @InjectMocks
    private VikingMediaPrepareInterceptor vikingMediaPrepareInterceptor;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private MediaModel mediaModel;

    @Mock
    private MediaContainerModel mediaContainerModel;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInterceptor_Media_has_Channel() throws InterceptorException {
        when(mediaModel.getChannels()).thenReturn(Collections.singleton(Channel.VIKINGSAFETYSHOP));
        vikingMediaPrepareInterceptor.onPrepare(mediaModel, interceptorContext);
        verify(mediaModel, never()).setChannels(anySet());
    }

    @Test
    public void testInterceptor_Media_has_no_Channel_no_MediaContainer() throws InterceptorException {
        vikingMediaPrepareInterceptor.onPrepare(mediaModel, interceptorContext);
        verify(mediaModel, never()).setChannels(anySet());
    }

    @Test
    public void testInterceptor_Media_has_no_Channel_MediaContainer_has_no_Channel() throws InterceptorException {
        when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);

        vikingMediaPrepareInterceptor.onPrepare(mediaModel, interceptorContext);
        verify(mediaModel, never()).setChannels(anySet());
    }

    @Test
    public void testInterceptor_Media_has_no_Channel_MediaContainer_has_Channel() throws InterceptorException {
        when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
        final Set<Channel> channels = Collections.singleton(Channel.VIKINGSAFETYSHOP);
        when(mediaContainerModel.getChannels()).thenReturn(channels);

        vikingMediaPrepareInterceptor.onPrepare(mediaModel, interceptorContext);
        verify(mediaModel, times(1)).setChannels(eq(channels));
    }
}
