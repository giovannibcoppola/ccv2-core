package com.viking.core.attributehandler;

import com.viking.core.attributehandlers.VikingAddressLine2Attribute;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.testframework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingAddressLine2AttributeTest {

    @InjectMocks
    private VikingAddressLine2Attribute vikingAddressLine2Attribute;

    @Mock
    private AddressModel addressModel;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet(){
        when(addressModel.getAdditionalLine()).thenReturn("abc 12");

        final String line1 = vikingAddressLine2Attribute.get(addressModel);

        assertEquals("abc 12", line1);
    }

    @Test
    public void testSet(){
        vikingAddressLine2Attribute.set(addressModel, "abc 12");

        verify(addressModel).setAdditionalLine("abc 12");
    }
}
