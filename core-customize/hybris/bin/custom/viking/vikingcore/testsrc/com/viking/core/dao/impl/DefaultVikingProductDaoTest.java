package com.viking.core.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class DefaultVikingProductDaoTest {

    @InjectMocks
    private DefaultVikingProductDao defaultVikingProductDao;

    @Mock
    private FlexibleSearchService flexibleSearchService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindProductsWithModifiedTimeGreaterThan() {
        final Date date = new Date();
        final ProductModel productModel = mock(ProductModel.class);
        final SearchResult<ProductModel> searchResult = mock(SearchResult.class);

        when(flexibleSearchService.search(eq(defaultVikingProductDao.QUERY), any(HashMap.class))).thenReturn(searchResult);
        when(searchResult.getCount()).thenReturn(1);
        when(searchResult.getResult()).thenReturn(Arrays.asList(productModel));

        final List<ProductModel> productsWithModifiedTimeGreaterThan =  defaultVikingProductDao.findProductsWithModifiedTimeGreaterThan(date);

        assertEquals(productModel, productsWithModifiedTimeGreaterThan.get(0));
    }

}
