package com.viking.core.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * @author amine.elharrak
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingStreetNumberTranslatorTest {
    @InjectMocks
    private VikingStreetNumberTranslator vikingStreetNumberTranslator;

    @Mock
    private DefaultModelService modelService;
    @Mock
    private Address address;
    @Mock
    private AddressModel addressModel;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPerformImport_IsStreetNumberIgnored() throws ImpExException {
        final String IGNORED_TAG = "<ignore>";
        when(modelService.get(address)).thenReturn(addressModel);
        when(addressModel.getStreetnumber()).thenReturn(IGNORED_TAG);
        vikingStreetNumberTranslator.performImport(IGNORED_TAG, address);
        verify(modelService, times(1)).save(addressModel);
    }

}