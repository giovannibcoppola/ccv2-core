package com.viking.core.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.variants.jalo.VariantProduct;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VikingProductComMaterialDescriptionTranslatorTest {

    @InjectMocks
    private VikingProductComMaterialDescriptionTranslator vikingProductComMaterialDescriptionTranslator;

    @Mock
    private DefaultModelService modelService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImportValue_product_no_existingValue() {
       final Product product = mock(Product.class);
       final  String name = "test";
       final ProductModel productModel = mock(ProductModel.class);

       when(productModel.getName(Locale.ENGLISH)).thenReturn(StringUtils.EMPTY);
       when(modelService.get(product)).thenReturn(productModel);

       final String result = String.valueOf(vikingProductComMaterialDescriptionTranslator.importValue(name,product));

        assertEquals(name,result);
    }

    @Test
    public void testImportValue_product_with_existingValue() {
        final Product product = mock(Product.class);
        final  String name = "test";
        final ProductModel productModel = mock(ProductModel.class);

        when(productModel.getName(Locale.ENGLISH)).thenReturn("existing");
        when(modelService.get(product)).thenReturn(productModel);

        final String result = String.valueOf(vikingProductComMaterialDescriptionTranslator.importValue(name,product));

        assertEquals("existing",result);
    }


    @Test
    public void testImportValue_variantProduct_no_existingValue() {
        final VariantProduct product = mock(VariantProduct.class);
        final  String name = "test";
        final ERPVariantProductModel productModel = mock(ERPVariantProductModel.class);

        when(productModel.getName(Locale.ENGLISH)).thenReturn(StringUtils.EMPTY);
        when(modelService.get(product)).thenReturn(productModel);

        final String result = String.valueOf(vikingProductComMaterialDescriptionTranslator.importValue(name,product));

        assertEquals(name,result);
    }

    @Test
    public void testImportValue_variantProduct_with_existingValue() {
        final VariantProduct product = mock(VariantProduct.class);
        final  String name = "test";
        final ERPVariantProductModel productModel = mock(ERPVariantProductModel.class);

        when(productModel.getName(Locale.ENGLISH)).thenReturn("existing");
        when(modelService.get(product)).thenReturn(productModel);

        final String result = String.valueOf(vikingProductComMaterialDescriptionTranslator.importValue(name,product));

        assertEquals("existing",result);
    }
}
