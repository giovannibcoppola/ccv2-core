package com.viking.core.service.impl;

import static com.viking.core.constants.VikingCoreConstants.*;
import com.viking.core.enquiry.service.EnquiryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author javier.gomez
 */
@UnitTest
public class DefaultVikingCommerceCategoryServiceTest {

    @InjectMocks
    private DefaultVikingCommerceCategoryService vikingCommerceCategoryService;

    @Mock
    private UserService userService;

    @Mock
    private EnquiryService enquiryService;

    @Mock
    private CategoryService categoryService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetCategoryAuthorizedForUser_wrongCategoryCode() {
        when(categoryService.getCategoriesForCode(anyString())).thenReturn(null);

        Assert.assertTrue(vikingCommerceCategoryService.getCategoryAuthorizedForUser("categoryCode").isEmpty());
    }

    @Test
    public void testGetCategoryAuthorizedForUser_IsEnquiryFlow() {
        CategoryModel categoryModel = mock(CategoryModel.class);
        when(categoryService.getCategoriesForCode(anyString())).thenReturn(Arrays.asList(categoryModel));

        when(enquiryService.isEnquiryFlow()).thenReturn(Boolean.TRUE);
        Assert.assertEquals(categoryModel, vikingCommerceCategoryService.getCategoryAuthorizedForUser("categoryCode").get());
    }

    @Test
    public void testGetCategoryAuthorizedForUser_IsNotEnquiryFlow_invalidAllowedPrincipals() {
        PrincipalGroupModel principalGroupModel1 = mock(PrincipalGroupModel.class);
        PrincipalGroupModel principalGroupModel2 = mock(PrincipalGroupModel.class);

        UserModel userModel = mock(UserModel.class);
        when(userModel.getAllGroups()).thenReturn(new HashSet<>(Arrays.asList(principalGroupModel1)));

        CategoryModel categoryModel = mock(CategoryModel.class);
        when(categoryModel.getAllowedPrincipals()).thenReturn(Arrays.asList(principalGroupModel2));

        when(userService.getCurrentUser()).thenReturn(userModel);
        when(categoryService.getCategoriesForCode(anyString())).thenReturn(Arrays.asList(categoryModel));

        when(enquiryService.isEnquiryFlow()).thenReturn(Boolean.FALSE);

        Assert.assertTrue(vikingCommerceCategoryService.getCategoryAuthorizedForUser("categoryCode").isEmpty());
    }

    @Test
    public void testGetCategoryAuthorizedForUser_IsNotEnquiryFlow_validAllowedPrincipals() {
        PrincipalGroupModel principalGroupModel = mock(PrincipalGroupModel.class);
        when(principalGroupModel.getUid()).thenReturn("AUTH_1001");

        UserModel userModel = mock(UserModel.class);
        when(userModel.getAllGroups()).thenReturn(new HashSet<>(Arrays.asList(principalGroupModel)));

        CategoryModel categoryModel = mock(CategoryModel.class);
        when(categoryModel.getAllowedPrincipals()).thenReturn(Arrays.asList(principalGroupModel));

        when(userService.getCurrentUser()).thenReturn(userModel);
        when(categoryService.getCategoriesForCode(anyString())).thenReturn(Arrays.asList(categoryModel));

        when(enquiryService.isEnquiryFlow()).thenReturn(Boolean.FALSE);

        Assert.assertTrue(vikingCommerceCategoryService.getCategoryAuthorizedForUser("categoryCode").isPresent());
    }

    @Test
    public void testGetStoreIdForCategory_Classification(){
        final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);

        final Optional<String> result = vikingCommerceCategoryService.getStoreIdForCategory(categoryModel);

        Assert.assertEquals(CLASSIFICATION_STORE, result.get());
    }


    @Test
    public void testGetStoreIdForCategory_saperp(){
        final CategoryModel categoryModel = mock(CategoryModel.class);
        final CategoryModel superecategoryModel = mock(CategoryModel.class);

        when(categoryModel.getAllSupercategories()).thenReturn(Collections.singleton(superecategoryModel));
        when(superecategoryModel.isRootCategory()).thenReturn(true);
        when(superecategoryModel.getCode()).thenReturn(ERP_CATEGORY_CODE);

        final Optional<String> result = vikingCommerceCategoryService.getStoreIdForCategory(categoryModel);

        Assert.assertEquals(SAP_STORE, result.get());
    }

    @Test
    public void testGetStoreIdForCategory_partner(){
        final CategoryModel categoryModel = mock(CategoryModel.class);
        final CategoryModel superecategoryModel = mock(CategoryModel.class);

        when(categoryModel.getAllSupercategories()).thenReturn(Collections.singleton(superecategoryModel));
        when(superecategoryModel.isRootCategory()).thenReturn(true);
        when(superecategoryModel.getCode()).thenReturn(SERVICE_PARTNER_PORTAL_CATEGORY_CODE);

        final Optional<String> result = vikingCommerceCategoryService.getStoreIdForCategory(categoryModel);

        Assert.assertEquals(PARTNER_STORE, result.get());
    }

    @Test
    public void testGetStoreIdForCategory_safetshop(){
        final CategoryModel categoryModel = mock(CategoryModel.class);
        final CategoryModel superecategoryModel = mock(CategoryModel.class);

        when(categoryModel.getAllSupercategories()).thenReturn(Collections.singleton(superecategoryModel));
        when(superecategoryModel.isRootCategory()).thenReturn(true);
        when(superecategoryModel.getCode()).thenReturn(SAFETY_SHOP_CATEGORY_CODE);

        final Optional<String> result = vikingCommerceCategoryService.getStoreIdForCategory(categoryModel);

        Assert.assertEquals(SAFETYSHOP_STORE, result.get());
    }
}
