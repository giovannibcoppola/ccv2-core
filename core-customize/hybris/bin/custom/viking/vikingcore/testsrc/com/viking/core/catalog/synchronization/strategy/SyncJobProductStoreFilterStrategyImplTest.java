package com.viking.core.catalog.synchronization.strategy;

import com.google.common.collect.ImmutableList;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SyncJobProductStoreFilterStrategyImplTest {

    @InjectMocks
    private SyncJobProductStoreFilterStrategyImpl syncJobProductStoreFilterStrategy;

    @Mock
    BaseStoreService baseStoreService;

    @Mock
    CatalogModel targetCatalog;
    @Mock
    CatalogVersionModel targetCatalogVersion;
    @Mock
    ProductModel item;
    @Mock
    SyncItemJobModel syncItemJob;
    @Mock
    BaseStoreModel store;
    @Mock
    BaseStoreModel otherStore;

    @Before
    public void setUp() throws Exception {
        when(store.getUid()).thenReturn("Store1");
        when(otherStore.getUid()).thenReturn("OtherStore");
        when(baseStoreService.getAllBaseStores()).thenReturn(ImmutableList.of(otherStore, store));

        when(item.getCode()).thenReturn("Product in catalog of store1");
        when(item.getStoreList()).thenReturn(Collections.singleton(store));

        when(syncItemJob.getTargetVersion()).thenReturn(targetCatalogVersion);
        when(targetCatalogVersion.getCatalog()).thenReturn(targetCatalog);
        when(store.getCatalogs()).thenReturn(Collections.singletonList(targetCatalog));
    }

    @Test
    public void filterItems() {
        assertEquals(Collections.singletonList(item), syncJobProductStoreFilterStrategy.filterItems(Collections.singletonList(item), syncItemJob));
    }

    @Test
    public void filterItemsNull() {
        assertEquals(Collections.emptyList(), syncJobProductStoreFilterStrategy.filterItems(Collections.emptyList(), syncItemJob));
    }

    @Test
    public void filterItemsNonProducts() {
        CategoryModel nonProduct = mock(CategoryModel.class);
        final List<ItemModel> filteredItems = syncJobProductStoreFilterStrategy.filterItems(Collections.singletonList(nonProduct), syncItemJob);
        assertEquals(Collections.singletonList(nonProduct), filteredItems);
    }

    @Test
    public void filterItemsOtherStore() {
        ProductModel productInOtherStore = mock(ProductModel.class);
        when(productInOtherStore.getCode()).thenReturn("Product in otherstore");
        when(productInOtherStore.getStoreList()).thenReturn(Collections.singletonList(otherStore));

        final List<ItemModel> filteredItems = syncJobProductStoreFilterStrategy.filterItems(Collections.singletonList(productInOtherStore), syncItemJob);
        assertEquals(Collections.emptyList(), filteredItems);
    }
}