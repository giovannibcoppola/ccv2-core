package com.viking.core.interceptors;

import com.viking.core.service.VikingCommerceCategoryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Unit test to cover class {@link VikingMediaContainerPrepareInterceptor}
 *
 * @author chiranjit
 */
@UnitTest
public class VikingCategoryPrepareInterceptorTest {

    @InjectMocks
    private VikingCategoryPrepareInterceptor vikingCategoryPrepareInterceptor;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private CategoryModel categoryModel;
    
    @Mock
    private VikingCommerceCategoryService vikingCommerceCategoryService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInterceptor_Category_StoreId() throws InterceptorException {
        when(vikingCommerceCategoryService.getStoreIdForCategory(categoryModel)).thenReturn(Optional.of("test"));
        vikingCategoryPrepareInterceptor.onPrepare(categoryModel, interceptorContext);
        verify(categoryModel, times(1)).setStoreId(eq("test"));
    }


    @Test
    public void testInterceptor_Category_no_StoreId() throws InterceptorException {
        when(vikingCommerceCategoryService.getStoreIdForCategory(categoryModel)).thenReturn(Optional.empty());
        vikingCategoryPrepareInterceptor.onPrepare(categoryModel, interceptorContext);
        verify(categoryModel, never()).setStoreId(anyString());
    }

}
