package com.viking.core.productchanneldata.service.impl;

import com.viking.core.enums.Channel;
import com.viking.core.model.LongChannelTextModel;
import com.viking.core.model.SimpleChannelTextModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import net.sf.ehcache.search.parser.MCriteria;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;

import static de.hybris.platform.testframework.Assert.assertEquals;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class DefaultProductChannelDataServiceTest {

    @InjectMocks
    private DefaultProductChannelDataService defaultProductChannelDataService;
    @Mock
    private CMSSiteService cmsSiteService;
    @Mock
    private ProductModel productModel;
    @Mock
    private Channel channel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetFilteredMediasForChannel_WithoutMedias() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Collections.emptyList());

        assertTrue(CollectionUtils.isEmpty(filteredMedias));
    }

    @Test
    public void testGetFilteredMediasForChannel_WithMedias_NoChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getChannels()).thenReturn(Collections.emptySet());

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Arrays.asList(mediaModel));

        assertTrue(CollectionUtils.isEmpty(filteredMedias));
    }

    @Test
    public void testGetFilteredMediasForChannel_WithMedias_WrongChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(Channel.CUSTOMER_PORTAL.getCode());

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Arrays.asList(mediaModel));

        assertTrue(CollectionUtils.isEmpty(filteredMedias));
    }

    @Test
    public void testGetFilteredMediasForChannel_WithMedias_RightChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Arrays.asList(mediaModel));

        assertEquals(1, filteredMedias.size());
    }

    public void testGetFilteredMediaForChannel__WrongChannel_DefaultChannelFallback() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(Channel.CUSTOMER_PORTAL.getCode());

        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(Channel.DEFAULT)));

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Arrays.asList(mediaContainerModel));

        assertTrue(CollectionUtils.isNotEmpty(filteredContainerMedias));
    }

    @Test
    public void testGetFilteredMediaContainersForChannel_WithoutMedias() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Collections.emptyList());

        assertTrue(CollectionUtils.isEmpty(filteredContainerMedias));
    }

    @Test
    public void testGetFilteredMediaContainersForChannel_WithMedias_NoChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getChannels()).thenReturn(Collections.emptySet());

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Arrays.asList(mediaContainerModel));

        assertTrue(CollectionUtils.isEmpty(filteredContainerMedias));
    }

    @Test
    public void testGetFilteredMediaContainersForChannel_WithMedias_WrongChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(Channel.CUSTOMER_PORTAL.getCode());

        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Arrays.asList(mediaContainerModel));

        assertTrue(CollectionUtils.isEmpty(filteredContainerMedias));
    }

    @Test
    public void testGetFilteredMediaContainersForChannel_WithMedias_RightChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Arrays.asList(mediaContainerModel));

        assertEquals(1, filteredContainerMedias.size());
        assertEquals(0,  filteredContainerMedias.get(0).getOrder());
    }

    @Test
    public void testGetFilteredMediaContainersForChannel_WithMedias_WrongChannel_DefaultChannelFallback() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(Channel.CUSTOMER_PORTAL.getCode());

        MediaContainerModel mediaContainerModel = mock(MediaContainerModel.class);
        when(mediaContainerModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(Channel.DEFAULT)));

        List<MediaContainerModel> filteredContainerMedias = defaultProductChannelDataService.getFilteredMediaContainersForChannel(Arrays.asList(mediaContainerModel));

        assertTrue(CollectionUtils.isNotEmpty(filteredContainerMedias));
    }

    @Test
    public void testGetTeaserForChannel_WithoutTeasers(){
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        assertTrue(defaultProductChannelDataService.getTeaserForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetTeaserForChannel_WithTeasers_NoChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        SimpleChannelTextModel simpleChannelTextModel = mock(SimpleChannelTextModel.class);
        when(productModel.getTeasers()).thenReturn(Arrays.asList(simpleChannelTextModel));

        assertTrue(defaultProductChannelDataService.getTeaserForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetTeaserForChannel_WithTeasers_WrongChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.DEFAULT_CHANNEL_CODE);

        SimpleChannelTextModel simpleChannelTextModel = mock(SimpleChannelTextModel.class);
        when(simpleChannelTextModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        when(productModel.getTeasers()).thenReturn(Arrays.asList(simpleChannelTextModel));

        assertTrue(defaultProductChannelDataService.getTeaserForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetTeaserForChannel_WithTeasers_RightChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        SimpleChannelTextModel simpleChannelTextModel = mock(SimpleChannelTextModel.class);
        when(simpleChannelTextModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        when(productModel.getTeasers()).thenReturn(Arrays.asList(simpleChannelTextModel));
        when(simpleChannelTextModel.getText()).thenReturn("test");

        assertTrue(defaultProductChannelDataService.getTeaserForChannel(productModel).isPresent());
    }

    @Test
    public void testGetTeaserForChannel_WithTeasers_NoChannel_defaultTeaser() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        SimpleChannelTextModel simpleChannelTextModel = mock(SimpleChannelTextModel.class);
        when(productModel.getTeasers()).thenReturn(Arrays.asList(simpleChannelTextModel));
        when(productModel.getDefaultTeaser()).thenReturn("test");

        assertTrue(defaultProductChannelDataService.getTeaserForChannel(productModel).isPresent());
    }


    @Test
    public void testGetLongDescriptionForChannel_WithoutTeasers(){
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        assertTrue(defaultProductChannelDataService.getLongDescriptionForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetLongDescriptionForChannel_WithTeasers_NoChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        LongChannelTextModel longChannelTextModel = mock(LongChannelTextModel.class);
        when(productModel.getLongDescriptions()).thenReturn(Arrays.asList(longChannelTextModel));

        assertTrue(defaultProductChannelDataService.getLongDescriptionForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetLongDescriptionForChannel_WithTeasers_WrongChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.DEFAULT_CHANNEL_CODE);

        LongChannelTextModel longChannelTextModel = mock(LongChannelTextModel.class);
        when(longChannelTextModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        when(productModel.getLongDescriptions()).thenReturn(Arrays.asList(longChannelTextModel));

        assertTrue(defaultProductChannelDataService.getLongDescriptionForChannel(productModel).isEmpty());
    }

    @Test
    public void testGetLongDescriptionForChannel_WithTeasers_RightChannel() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        LongChannelTextModel longChannelTextModel = mock(LongChannelTextModel.class);
        when(longChannelTextModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        when(productModel.getLongDescriptions()).thenReturn(Arrays.asList(longChannelTextModel));
        when(longChannelTextModel.getLongText()).thenReturn("test");

        assertTrue(defaultProductChannelDataService.getLongDescriptionForChannel(productModel).isPresent());
    }

    @Test
    public void testGetLongDescriptionForChannel_WithTeasers_NoChannel_defaultLongDescription() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        LongChannelTextModel longChannelTextModel = mock(LongChannelTextModel.class);
        when(productModel.getLongDescriptions()).thenReturn(Arrays.asList(longChannelTextModel));
        when(productModel.getDefaultLongDescription()).thenReturn("test");

        assertTrue(defaultProductChannelDataService.getLongDescriptionForChannel(productModel).isPresent());
    }

    @Test
    public void testGetFilteredMediasForChannel_Media_Order_defaultValue() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);

        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        MediaModel mediaModel = mock(MediaModel.class);
        when(mediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Arrays.asList(mediaModel));
        assertEquals( 0 ,filteredMedias.get(0).getOrder() );
    }
    @Test
    public void testGetFilteredMediasForChannel_Media_Order_withOrder() {
        CMSSiteModel theSite = new CMSSiteModel();
        final String theUid = DefaultProductChannelDataService.SAFETY_SHOP_SITE_UID;
        theSite.setUid(theUid);
        when(cmsSiteService.getCurrentSite()).thenReturn(theSite);
        when(channel.getCode()).thenReturn(DefaultProductChannelDataService.VIKING_SAFETY_SHOP_CHANNEL_CODE);

        MediaModel firstMediaModel = mock(MediaModel.class);
        when(firstMediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));
        when(firstMediaModel.getOrder()).thenReturn(0);

        MediaModel nextMediaModel = mock(MediaModel.class);
        when(nextMediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));
        when(nextMediaModel.getOrder()).thenReturn(1);

        MediaModel lastMediaModel = mock(MediaModel.class);
        when(lastMediaModel.getChannels()).thenReturn(new HashSet<>(Arrays.asList(channel)));
        when(lastMediaModel.getOrder()).thenReturn(2);

        List<MediaModel> filteredMedias = defaultProductChannelDataService.getFilteredMediasForChannel(Arrays.asList(firstMediaModel, nextMediaModel, lastMediaModel));
        assertEquals( 0 ,filteredMedias.get(0).getOrder());
        assertEquals( 1 ,filteredMedias.get(1).getOrder());
        assertEquals( 2 ,filteredMedias.get(2).getOrder());
    }
}
