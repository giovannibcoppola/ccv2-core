package com.viking.core.interceptors;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.google.common.collect.Sets;
import com.viking.core.constants.VikingCoreConstants;
import com.viking.core.event.VikingProductUpdateApprovalStatusEvent;
import com.viking.core.service.VikingCommerceStoreService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.store.BaseStoreModel;

import static com.viking.core.constants.VikingCoreConstants.DEFAULT_CATALOG;
import static com.viking.core.constants.VikingCoreConstants.PARTNER_STORE;
import static com.viking.core.constants.VikingCoreConstants.SAFETYSHOP;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test to cover class {@link VikingProductPrepareInterceptor}
 *
 * @author javier.gomez
 */
@UnitTest
public class VikingProductPrepareInterceptorTest {
    @Spy
    @InjectMocks
    private VikingProductPrepareInterceptor vikingProductPrepareInterceptor;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private ProductModel productModel;

    @Mock
    private CategoryModel categoryModel;

    @Mock
    private TimeService timeService;

    @Mock
    private ItemModelContextImpl context;

    @Mock
    private ModelValueHistory history;

    @Mock
    private ModelService modelService;
    @Mock
    private ProductService productService;
    @Mock
    private CatalogVersionModel catalogVersion;
    @Mock
    private VikingCommerceStoreService vikingCommerceStoreService;
    @Mock
    private BaseStoreModel default_;
    @Mock
    private BaseStoreModel safetyshop;
    @Mock
    private BaseStoreModel partner;
    @Mock
    private Collection<BaseStoreModel> storeList;
    @Mock
    private Collection<BaseStoreModel> storeList2;

    @Mock
    private EventService eventService;
    @Mock
    private VikingProductUpdateApprovalStatusEvent vikingPrdUpdateApprovalStatusEvent;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        safetyshop.setUid(SAFETYSHOP);
        partner.setUid(PARTNER_STORE);
        default_.setUid(DEFAULT_CATALOG);
        storeList = Arrays.asList(safetyshop, partner);
        storeList2 = Arrays.asList(safetyshop);
        when(productModel.getStoreList()).thenReturn(storeList);
        ModelValueHistory history = mock(ModelValueHistory.class);
        when(productModel.getItemModelContext()).thenReturn(context);
        when(context.getValueHistory()).thenReturn(history);
        when(history.getDirtyAttributes()).thenReturn(Sets.newHashSet(ProductModel.STORELIST));
        when(context.isLoaded(ProductModel.STORELIST)).thenReturn(Boolean.TRUE);

        final CatalogModel catalog = new CatalogModel();
        catalog.setId(VikingCoreConstants.DEFAULT_CATALOG);
        final CatalogVersionModel catalogVersion = new CatalogVersionModel();
        catalogVersion.setCatalog(catalog);
        catalogVersion.setVersion(VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME);
        when(productModel.getCatalogVersion()).thenReturn(catalogVersion);
    }

    /**
     * Test interceptor update product sap field.
     *
     * @throws InterceptorException the interceptor exception
     */
    @Test
    public void testInterceptor_UpdateProductSAPField() throws InterceptorException {
        final Date date = new Date();
        when(interceptorContext.isNew(productModel)).thenReturn(Boolean.FALSE);
        Mockito.when(interceptorContext.isModified(productModel, "productStatusSAP")).thenReturn(Boolean.TRUE);
        when(productModel.getSupercategories()).thenReturn(Arrays.asList(categoryModel));
        when(timeService.getCurrentTime()).thenReturn(date);


        ItemModelContextImpl itemModelContext = Mockito.mock(ItemModelContextImpl.class);

        Mockito.when(productModel.getItemModelContext()).thenReturn(itemModelContext);
        Mockito.when(productModel.getProductStatusSAP()).thenReturn("60");
        Mockito.when(itemModelContext.loadOriginalValue("productStatusSAP")).thenReturn("30");

        vikingProductPrepareInterceptor.onPrepare(productModel, interceptorContext);

        verify(productModel, times(1)).setSapFieldModifiedTime(date);
        verify(productModel, times(1)).setApprovalStatus(Mockito.anyObject());
    }

    /**
     * Test interceptor viking prd update approval status event.
     *
     * @throws InterceptorException the interceptor exception
     */
    @Test
    public void testInterceptor_triggerProductApprovalStatus() throws InterceptorException {
        when(context.getOriginalValue(ProductModel.STORELIST)).thenReturn(Arrays.asList(storeList));
        when(context.getRawPropertyValue(ProductModel.STORELIST)).thenReturn(Arrays.asList(storeList2));
        vikingProductPrepareInterceptor.onPrepare(productModel, interceptorContext);
        verify(vikingProductPrepareInterceptor, times(1)).triggerProductApprovalStatus(productModel, interceptorContext);
    }

    /**
     * Test interceptor viking prd update approval status event.
     *
     * @throws InterceptorException the interceptor exception
     */
    @Test
    public void testInterceptor_VikingPrdUpdateApprovalStatusEvent() throws InterceptorException {
        when(productModel.getCode()).thenReturn("0000010");
        vikingPrdUpdateApprovalStatusEvent = new VikingProductUpdateApprovalStatusEvent(productModel.getCode(), partner.getUid());
        eventService.publishEvent(vikingPrdUpdateApprovalStatusEvent);
        verify(eventService, times(1)).publishEvent(any(VikingProductUpdateApprovalStatusEvent.class));
    }

    @Test
    public void isSameTest(){
        Assert.assertTrue(vikingProductPrepareInterceptor.isSame(null, null));
        Assert.assertTrue(vikingProductPrepareInterceptor.isSame("test", "test"));
        Assert.assertFalse(vikingProductPrepareInterceptor.isSame("test", "dfdf"));
        Assert.assertFalse(vikingProductPrepareInterceptor.isSame("test", null));
        Assert.assertFalse(vikingProductPrepareInterceptor.isSame(null, "test"));
    }

    @Test
    public void isSAPFieldModifiedTest(){
        ProductModel productModel = Mockito.mock(ProductModel.class);
        InterceptorContext interceptorContext = Mockito.mock(InterceptorContext.class);
        Assert.assertFalse(vikingProductPrepareInterceptor.isSAPFieldModified(productModel, interceptorContext));

    }

    @Test
    public void isSAPFieldModifiedTestOriginalValueNull(){
        ProductModel productModel = Mockito.mock(ProductModel.class);
        InterceptorContext interceptorContext = Mockito.mock(InterceptorContext.class);
        ItemModelContextImpl itemModelContext = Mockito.mock(ItemModelContextImpl.class);

        Mockito.when(productModel.getItemModelContext()).thenReturn(itemModelContext);
        Mockito.when(interceptorContext.isModified(productModel, "productStatusSAP")).thenReturn(Boolean.TRUE);
        Mockito.when(productModel.getProductStatusSAP()).thenReturn("60");

        Assert.assertTrue(vikingProductPrepareInterceptor.isSAPFieldModified(productModel, interceptorContext));

    }

    @Test
    public void isSAPFieldModifiedTestOtherValueSet(){
        ProductModel productModel = Mockito.mock(ProductModel.class);
        InterceptorContext interceptorContext = Mockito.mock(InterceptorContext.class);
        ItemModelContextImpl itemModelContext = Mockito.mock(ItemModelContextImpl.class);

        Mockito.when(productModel.getItemModelContext()).thenReturn(itemModelContext);
        Mockito.when(interceptorContext.isModified(productModel, "productStatusSAP")).thenReturn(Boolean.TRUE);
        Mockito.when(productModel.getProductStatusSAP()).thenReturn("60");
        Mockito.when(itemModelContext.loadOriginalValue("productStatusSAP")).thenReturn("30");

        Assert.assertTrue(vikingProductPrepareInterceptor.isSAPFieldModified(productModel, interceptorContext));

    }


    @Test
    public void isSAPFieldModifiedTestSameValueSet(){
        ProductModel productModel = Mockito.mock(ProductModel.class);
        InterceptorContext interceptorContext = Mockito.mock(InterceptorContext.class);
        ItemModelContextImpl itemModelContext = Mockito.mock(ItemModelContextImpl.class);

        Mockito.when(productModel.getItemModelContext()).thenReturn(itemModelContext);
        Mockito.when(interceptorContext.isModified(productModel, "productStatusSAP")).thenReturn(Boolean.TRUE);
        Mockito.when(productModel.getProductStatusSAP()).thenReturn("60");
        Mockito.when(itemModelContext.loadOriginalValue("productStatusSAP")).thenReturn("60");

        Assert.assertFalse(vikingProductPrepareInterceptor.isSAPFieldModified(productModel, interceptorContext));

    }

}
