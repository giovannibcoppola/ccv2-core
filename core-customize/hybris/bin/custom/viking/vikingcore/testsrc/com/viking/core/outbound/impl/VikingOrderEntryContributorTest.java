package com.viking.core.outbound.impl;

import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.model.VikingSapPlantModel;
import com.viking.core.service.VikingOrderEntryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingOrderEntryContributorTest {

    private static final String DELIVERY_PLANT_CODE = "deliveryPlant";

    @InjectMocks
    private VikingOrderEntryContributor testObj;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private OrderModel orderMock;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private OrderEntryModel orderEntryMock;
    @Mock
    private B2BCustomerModel b2bCustomerMock;
    @Mock
    private B2BUnitModel unitMock;
    @Mock
    private LanguageModel languageMock;
    @Mock
    private VikingSapPlantModel plantMock;
    @Mock
    private VikingOrderEntryService vikingOrderEntryServiceMock;
    @Mock
    private EnquiryService enquiryService;

    @Before
    public void setUp() {
        testObj.setBatchIdAttributes(new HashMap<>());

        when(orderMock.getEntries()).thenReturn(Collections.singletonList(orderEntryMock));
        when(orderMock.getUser()).thenReturn(b2bCustomerMock);
        when(orderMock.getLanguage()).thenReturn(languageMock);
        when(orderMock.getLanguage().getFallbackLanguages()).thenReturn(Collections.singletonList(languageMock));
        when(languageMock.getIsocode()).thenReturn("EN");
        when(b2bCustomerMock.getDefaultB2BUnit()).thenReturn(unitMock);
    }

    @Test
    public void testColumnsContainPlant() {
        // When
        final Set<String> result = testObj.getColumns();

        // Then
        assertTrue(result.contains(DELIVERY_PLANT_CODE));
        assertTrue(result.contains(OrderCsvColumns.ORDER_ID));
    }

    @Test
    public void testRowshasEmptyPlantValueWhenNoPlantOnUnit() {
        // Given
        when(unitMock.getSapPlant()).thenReturn(null);

        // When
        List<Map<String, Object>> result = testObj.createRows(orderMock);

        // Then
        assertTrue(result.stream().allMatch(map -> StringUtils.isEmpty((String) map.get(DELIVERY_PLANT_CODE))));
    }

    @Test
    public void testOneRowhasPlantValueWhenPlantExistsOnUnit() {
        // Given
        when(unitMock.getSapPlant()).thenReturn(plantMock);
        when(plantMock.getCode()).thenReturn("1234");

        // When
        List<Map<String, Object>> result = testObj.createRows(orderMock);

        // Then
        List<Map<String, Object>> mapsWithPlant = result.stream().filter(map -> map.get(DELIVERY_PLANT_CODE) != null).collect(Collectors.toList());

        assertTrue(mapsWithPlant.stream().allMatch(map -> "1234".equals(map.get(DELIVERY_PLANT_CODE))));
    }
}