package com.viking.core.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingB2BUnitShippingAddressTranslatorTest {

    @InjectMocks
    private VikingB2BUnitShippingAddressTranslator vikingB2BUnitShippingAddressTranslator;

    @Mock
    private DefaultModelService modelService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPerformImport_isShippingAddress_true() throws ImpExException {
        final Address address = mock(Address.class);
        final AddressModel addressModel = mock(AddressModel.class);
        final B2BUnitModel b2BUnitModel = mock(B2BUnitModel.class);

        when(modelService.get(address)).thenReturn(addressModel);
        when(addressModel.getOwner()).thenReturn(b2BUnitModel);

        vikingB2BUnitShippingAddressTranslator.performImport("true", address);

        verify(b2BUnitModel).setShippingAddress(eq(addressModel));
        verify(modelService).save(eq(b2BUnitModel));
    }

    @Test
    public void testPerformImport_isShippingAddress_false() throws ImpExException {
        final Address address = mock(Address.class);
        final AddressModel addressModel = mock(AddressModel.class);
        final B2BUnitModel b2BUnitModel = mock(B2BUnitModel.class);

        when(modelService.get(address)).thenReturn(addressModel);
        when(addressModel.getOwner()).thenReturn(b2BUnitModel);

        vikingB2BUnitShippingAddressTranslator.performImport("false", address);

        verify(b2BUnitModel, never()).setShippingAddress(eq(addressModel));
        verify(modelService, never()).save(eq(b2BUnitModel));
    }

}
