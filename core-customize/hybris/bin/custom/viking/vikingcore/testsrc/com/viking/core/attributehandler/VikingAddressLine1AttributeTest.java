package com.viking.core.attributehandler;

import com.viking.core.attributehandlers.VikingAddressLine1Attribute;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.testframework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingAddressLine1AttributeTest {

    @InjectMocks
    private VikingAddressLine1Attribute vikingAddressLine1Attribute;

    @Mock
    private AddressModel addressModel;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGet(){
        when(addressModel.getStreetname()).thenReturn("street");
        when(addressModel.getStreetnumber()).thenReturn("12");

        final String line1 = vikingAddressLine1Attribute.get(addressModel);

        assertEquals("street 12", line1);
    }

    @Test
    public void testSet(){
        vikingAddressLine1Attribute.set(addressModel, "street 12");

       verify(addressModel).setStreetnumber("12");
       verify(addressModel).setStreetname("street");

    }
}
