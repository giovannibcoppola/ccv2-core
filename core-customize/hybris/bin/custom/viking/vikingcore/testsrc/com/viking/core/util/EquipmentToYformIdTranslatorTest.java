package com.viking.core.util;

import com.viking.core.dao.EquipmentToYFormDao;
import com.viking.core.model.EquipmentToYformModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.definition.rule.Unit;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EquipmentToYformIdTranslatorTest {

    @InjectMocks
    private EquipmentToYformIdTranslator testObj = new EquipmentToYformIdTranslator();

    @Mock
    private EquipmentToYFormDao equipmentToYFormDaoMock;
    @Mock
    private EquipmentToYformModel equipmentToYformMock;

    @Test
    public void convertToYFormWhereIdExists() {
        // Given
        final String id = "1001";
        Mockito.when(equipmentToYFormDaoMock.getById(id)).thenReturn(equipmentToYformMock);
        Mockito.when(equipmentToYformMock.getYForm()).thenReturn("Y_qm_reply_raft");

        // When
        final String result = testObj.convertToYFormId(id);

        //Then
        Assert.assertEquals("Y_qm_reply_raft", result);
    }

    @Test
    public void convertToYFormWhereTypeExists() {
        // Given
        final String type = "Yachting raft";
        Mockito.when(equipmentToYFormDaoMock.getByType(type)).thenReturn(equipmentToYformMock);
        Mockito.when(equipmentToYformMock.getId()).thenReturn("1001");

        // When
        final String result = testObj.convertToId(type);

        //Then
        Assert.assertEquals("1001", result);
    }
}