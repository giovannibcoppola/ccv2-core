package com.viking.core.search.solrfacetsearch.provider.impl;

import com.viking.core.productchanneldata.service.ProductChannelDataService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingImageValueProviderTest {
    @Mock
    ProductModel productModel;
    @Mock
    MediaFormatModel mediaFormatModel;
    @Mock
    MediaContainerService mediaContainerService;
    @Mock
    ProductChannelDataService productChannelDataService;
    @InjectMocks
    VikingImageValueProvider vikingImageValueProvider;

    @Test
    public void vikingImageValueProviderIsCorrect() {
        final MediaContainerModel theImageFileContainer = new MediaContainerModel();
        final List<MediaContainerModel> theImageFileContainers = Collections.singletonList(theImageFileContainer);

        when(productModel.getGalleryImages()).thenReturn(theImageFileContainers);
        when(productChannelDataService.getFilteredMediaContainersForChannel(productModel.getGalleryImages())).thenReturn(theImageFileContainers);

        final MediaModel expectedMediaModel = new MediaModel();
        when(mediaContainerService.getMediaForFormat(theImageFileContainer, mediaFormatModel)).thenReturn(expectedMediaModel);

        final MediaModel actualMediaModel = vikingImageValueProvider.findMedia(productModel, mediaFormatModel);

        assertEquals(expectedMediaModel, actualMediaModel);
    }

    @Test
    public void noProductIsCorrect() {
        final MediaModel actualMediaModel = vikingImageValueProvider.findMedia(null, mediaFormatModel);
        assertNull(actualMediaModel);
    }

    @Test
    public void noMediaFormatIsCorrect() {
        final MediaModel actualMediaModel = vikingImageValueProvider.findMedia(productModel, null);
        assertNull(actualMediaModel);
    }

    @Test
    public void noMediaContainersFiltered() {
        final MediaContainerModel theImageFileContainer = new MediaContainerModel();
        final List<MediaContainerModel> theImageFileContainers = Collections.singletonList(theImageFileContainer);

        when(productModel.getGalleryImages()).thenReturn(theImageFileContainers);
        when(productChannelDataService.getFilteredMediaContainersForChannel(productModel.getGalleryImages())).thenReturn(Collections.emptyList());

        MediaModel actualMediaModel = vikingImageValueProvider.findMedia(productModel, mediaFormatModel);
        assertNull(actualMediaModel);
    }

}