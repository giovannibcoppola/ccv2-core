package com.viking.core.model.impl;

import com.viking.core.model.EnquiryReceiverEmailModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EnquiryReceiverEmailModelTest {

    @Mock
    private FlexibleSearchService flexibleSearchService;
    @Mock
    private EnquiryReceiverEmailModel enquiryReceiver;

    @Test
    public void findEnquiryNotificationInfo() {
        // Given
        when(enquiryReceiver.getRegionIsoCode()).thenReturn("DE");
        when(enquiryReceiver.getSalesOrg()).thenReturn("6110");
        when(enquiryReceiver.getReceiverEmail()).thenReturn("sales-d@viking-life.com");
        when(enquiryReceiver.getCustomer()).thenReturn("OTCDE");
        when(enquiryReceiver.getPlant()).thenReturn("6110");

        assertEquals("DE", enquiryReceiver.getRegionIsoCode());
        assertEquals("6110", enquiryReceiver.getSalesOrg());
        assertEquals("sales-d@viking-life.com", enquiryReceiver.getReceiverEmail());
        assertEquals("OTCDE", enquiryReceiver.getCustomer());
        assertEquals("6110", enquiryReceiver.getPlant());
    }


    @Test(expected = NullPointerException.class)
    public void nullEnquiryReceiverEmailModel() {
        EnquiryReceiverEmailModel eReceiver = flexibleSearchService.getModelByExample(enquiryReceiver);
        eReceiver.getRegionIsoCode();
        eReceiver.getSalesOrg();
        eReceiver.getCustomer();
        eReceiver.getPlant();
        eReceiver.getReceiverEmail();
    }

}