package com.viking.core.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.mediaconversion.model.ConversionGroupModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

/**
 * Unit test to cover class {@link VikingMediaContainerPrepareInterceptor}
 *
 * @author chiranjit
 */
@UnitTest
public class VikingMediaContainerPrepareInterceptorTest {

    @InjectMocks
    private VikingMediaContainerPrepareInterceptor vikingMediaContainerPrepareInterceptor;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private MediaContainerModel mediaContainerModel;
    
    @Mock
    private ConversionGroupModel conversionGroupModel;
    
    @Mock
    private ModelService modelService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInterceptor_MediaContaier_has_ConversionGroup() throws InterceptorException {
        when(mediaContainerModel.getConversionGroup()).thenReturn(conversionGroupModel);
        vikingMediaContainerPrepareInterceptor.onPrepare(mediaContainerModel, interceptorContext);
        verify(mediaContainerModel, never()).setConversionGroup(any(ConversionGroupModel.class));
    }


    @Test
    public void testInterceptor_MediaContaier_has_no_ConversionGroup__ConversionGroup_Found() throws InterceptorException {
        when(modelService.getByExample(any(ConversionGroupModel.class))).thenReturn(conversionGroupModel);

        vikingMediaContainerPrepareInterceptor.onPrepare(mediaContainerModel, interceptorContext);

        verify(mediaContainerModel, times(1)).setConversionGroup(eq(conversionGroupModel));
    }

}
