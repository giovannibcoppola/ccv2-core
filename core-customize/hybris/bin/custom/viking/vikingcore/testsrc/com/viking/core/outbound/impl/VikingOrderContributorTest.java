package com.viking.core.outbound.impl;

import com.viking.core.enquiry.service.EnquiryService;
import com.viking.core.model.EnquiryReceiverEmailModel;
import com.viking.core.model.VikingEnquiryOrderModel;
import com.viking.core.model.VikingSapPlantModel;
import com.viking.core.model.VikingSapSalesOrganizationModel;
import com.viking.core.service.VikingEnquiryReceiverEmailService;
import com.viking.core.service.VikingOrderEntryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.sap.orderexchange.constants.OrderCsvColumns;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static com.viking.core.constants.VikingCoreConstants.SERVICEPARTNERPORTAL_UID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VikingOrderContributorTest {

    private static final String INCOTERMS1_CODE = "incoterms1";
    private static final String INCOTERMS2_CODE = "incoterms2";

    @InjectMocks
    private VikingOrderContributor testObj;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private OrderModel orderMock;
    @Mock
    private DeliveryModeModel deliveryModeMock;
    @Mock
    private AddressModel deliveryAddressMock;
    @Mock
    private B2BCustomerModel b2bCustomerMock;
    @Mock
    private CustomerModel b2cCustomerMock;
    @Mock
    private B2BUnitModel b2bUnitMock;
    @Mock
    private VikingSapPlantModel sapPlantMock;
    @Mock
    private OrderEntryModel orderEntryMock;
    @Mock
    private ProductModel productMock;
    @Mock
    private VikingOrderEntryService vikingOrderEntryServiceMock;
    @Mock
    private EnquiryService enquiryService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private VikingEnquiryOrderModel vikingEnquiryOrderModel;
    @Mock
    private BaseStoreModel baseStoreModel;
    @Mock
    private CMSSiteModel cmsSiteModel;
    @Mock
    private VikingSapSalesOrganizationModel vikingSapSalesOrganizationModel;
    @Mock
    private BaseSiteModel baseSiteModel;
    @Mock
    private VikingEnquiryReceiverEmailService vikingEnquiryReceiverEmailService;
    @Mock
    private EnquiryReceiverEmailModel enquiryReceiver;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        testObj.setBatchIdAttributes(new HashMap<>());



        when(orderMock.getDeliveryMode()).thenReturn(deliveryModeMock);
        when(b2bCustomerMock.getDefaultB2BUnit()).thenReturn(b2bUnitMock);
    }

    @Test
    public void testColumnsContainsIncoterms() {
        // When
        final Set<String> result = testObj.getColumns();

        // Then
        assertTrue(result.contains(OrderCsvColumns.ORDER_ID));
        assertTrue(result.contains(INCOTERMS1_CODE));
        assertTrue(result.contains(INCOTERMS2_CODE));
    }

    @Test
    public void tesOrderWithYOROrderType() {
        // Given
        when(orderMock.getSite().getUid()).thenReturn(SERVICEPARTNERPORTAL_UID);
        when(orderMock.getUser()).thenReturn(b2bCustomerMock);
        when(orderMock.getEntries()).thenReturn(Collections.singletonList(orderEntryMock));

        when(orderEntryMock.getProduct()).thenReturn(productMock);
        when(vikingOrderEntryServiceMock.isCertificateEntry(orderEntryMock)).thenReturn(true);

        // When
        final List<Map<String, Object>> result = testObj.createRows(orderMock);

        // Then
        final Map<String, Object> rows = result.get(0);
        assertEquals(VikingOrderContributor.EXW, rows.get(INCOTERMS1_CODE));
        assertEquals(VikingOrderContributor.DOC_TYPE_YOR+VikingOrderContributor.EX_WORKS, rows.get(INCOTERMS2_CODE));
    }

    @Test
    public void test_enquiryFlow() {
        when(vikingEnquiryOrderModel.getDeliveryMode()).thenReturn(null);
        when(vikingEnquiryOrderModel.getStore()).thenReturn(baseStoreModel);
        when(baseStoreModel.getCustomerNumber()).thenReturn("OTCDE");
        when(baseStoreModel.getSapSalesOrganization()).thenReturn(vikingSapSalesOrganizationModel);
        when(vikingSapSalesOrganizationModel.getCode()).thenReturn("6110");
        when(vikingEnquiryOrderModel.getSite()).thenReturn(cmsSiteModel);
        when(cmsSiteModel.getChannel()).thenReturn(SiteChannel.B2B);
        when(cmsSiteModel.getPortalId()).thenReturn("111");
        when(enquiryService.isEnquiryOrder(vikingEnquiryOrderModel)).thenReturn(true);
        // Given
        when(enquiryReceiver.getRegionIsoCode()).thenReturn("DE");
        when(enquiryReceiver.getSalesOrg()).thenReturn("6110");
        when(enquiryReceiver.getReceiverEmail()).thenReturn("sales-d@viking-life.com");
        when(enquiryReceiver.getCustomer()).thenReturn("OTCDE");
        when(enquiryReceiver.getPlant()).thenReturn("6110");
        when(vikingEnquiryReceiverEmailService.findEnquiryNotificationInfo(vikingEnquiryOrderModel)).thenReturn(enquiryReceiver);

        // When
        final List<Map<String, Object>> result = testObj.createRows(vikingEnquiryOrderModel);
        // Then
        final Map<String, Object> rows = result.get(0);
        assertEquals(StringUtils.EMPTY, rows.get(INCOTERMS1_CODE));
        assertEquals("could not determine plant for non b2bCustomer", rows.get(INCOTERMS2_CODE));
        assertEquals("111", rows.get("portalIdentifier"));
        assertEquals("6110", rows.get("salesOrg"));
    }
}
