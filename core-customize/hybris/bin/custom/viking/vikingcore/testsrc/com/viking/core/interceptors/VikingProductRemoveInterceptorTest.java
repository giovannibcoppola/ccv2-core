package com.viking.core.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit test to cover class {@link VikingProductRemoveInterceptor}
 *
 * @author amine.elharrak
 */
@UnitTest
public class VikingProductRemoveInterceptorTest {

    @InjectMocks
    private VikingProductRemoveInterceptor vikingProductRemoveInterceptor;

    @Mock
    private ProductModel productModel;
    @Mock
    private InterceptorContext interceptorContext;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = InterceptorException.class)
    public void test_onRemove() throws InterceptorException {
        vikingProductRemoveInterceptor.onRemove(productModel, interceptorContext);
        verify(vikingProductRemoveInterceptor, times(1)).onRemove(productModel, interceptorContext);
    }

}
