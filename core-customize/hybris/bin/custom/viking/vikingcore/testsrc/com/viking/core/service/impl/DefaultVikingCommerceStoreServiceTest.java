package com.viking.core.service.impl;

import com.viking.core.service.VikingCommerceStoreService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static com.viking.core.constants.VikingCoreConstants.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author amine.elharrak
 */
@UnitTest
public class DefaultVikingCommerceStoreServiceTest {

    @Mock
    private VikingCommerceStoreService vikingCommerceStoreService;
    @Mock
    private CatalogModel catalogModel;
    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(catalogModel.getId()).thenReturn(PARTNER_PRODUCT_CATALOG);
        when(catalogVersionModel.getCatalog()).thenReturn(catalogModel);
        when(catalogVersionModel.getVersion()).thenReturn(ONLINE_CATALOG_VERSION_NAME);
        when(vikingCommerceStoreService.getOnlineCatalogVersion(PARTNER_STORE)).thenReturn(catalogVersionModel);
    }

    @Test
    public void testGetOnlineCatalogVersion_partnerStoreCode() {
        Assert.assertNotNull(vikingCommerceStoreService.getOnlineCatalogVersion(PARTNER_STORE).getCatalog());
        Assert.assertEquals(vikingCommerceStoreService.getOnlineCatalogVersion(PARTNER_STORE).getCatalog().getId(), PARTNER_PRODUCT_CATALOG);
    }

    @Test
    public void testGetOnlineCatalogVersion_wrongStoreID() {
        Assert.assertFalse(vikingCommerceStoreService.getOnlineCatalogVersion(PARTNER_STORE).getCatalog().getId().equals(SAFETYSHOP_PRODUCT_CATALOG));
    }


}

