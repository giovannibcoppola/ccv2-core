package com.viking.core.service.impl;

import com.viking.core.service.VikingWorkflowService;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class VikingWorkflowServiceTest {

    @Mock
    private VikingWorkflowService vikingWorkflowService;


    @Mock
    private ModelService modelService;

    @Before
    public void testSetup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAllWorkflowActionInProgressForProduct()
    {
        final List<WorkflowActionModel> result = vikingWorkflowService.findAllWorkflowActionInProgressForProduct("VIKING_PR_CREATION", modelService.get(PK.parse("8796093066078")));
        Assert.assertTrue(result.isEmpty());
    }
}
