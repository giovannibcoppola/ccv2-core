package com.viking.core.classification.impl;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.impl.DefaultClassificationClassesResolverStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@UnitTest
public class VikingClassificationClassesResolverStrategyTest {

    @Spy
    @InjectMocks
    private VikingClassificationClassesResolverStrategy vikingClassificationClassesResolverStrategy;

    @Mock
    private ProductService productService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testResolveProduct_ProductInDefault() {
        final ProductModel productModel = mock(ProductModel.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        final ClassificationClassModel classificationClassModel = mock(ClassificationClassModel.class);
        final Set<ClassificationClassModel> defaultProductCategories = new HashSet<>();
        defaultProductCategories.add(classificationClassModel);
        final ProductModel defaultProduct = mock(ProductModel.class);

        when(catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);

        doReturn(Collections.EMPTY_SET).when((DefaultClassificationClassesResolverStrategy)vikingClassificationClassesResolverStrategy).resolve(productModel);
        when(productModel.getCode()).thenReturn("12345");
        when(productService.getProduct(catalogVersionModel,"12345")).thenReturn(defaultProduct);
        doReturn(defaultProductCategories).when((DefaultClassificationClassesResolverStrategy)vikingClassificationClassesResolverStrategy).resolve(productModel);

        final Set<ClassificationClassModel> result = vikingClassificationClassesResolverStrategy.resolve(productModel);

        Assert.assertEquals(classificationClassModel, result.iterator().next());

    }
}
