package com.viking.core.util;

import com.viking.core.dao.DateConversionKeyDao;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.xml.stream.XMLStreamException;
import java.util.Map;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class YFormXMLhelperTest {

    @InjectMocks
    private YFormXMLhelper testObj;

    @Mock
    private DateConversionKeyDao dateConversionKeyDao;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void transformyFormXML() throws XMLStreamException {
        // Given
        final String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<form xmlns:fr=\"http://orbeon.org/oxf/xml/form-runner\" fr:data-format-version=\"4.0.0\">\n" +
                "    <identification>\n" +
                "        <NEW-ADM1-0010>Test</NEW-ADM1-0010>\n" +
                "        <NEW-ADM1-0020>123</NEW-ADM1-0020>\n" +
                "        <NEW-ADM1-0150>Test</NEW-ADM1-0150>\n" +
                "        <NEW-ADM1-0200>Test</NEW-ADM1-0200>\n" +
                "        <NEW-ADM1-0040>123</NEW-ADM1-0040>\n" +
                "        <NEW-ADM1-0050>123</NEW-ADM1-0050>\n" +
                "        <NEW-ADM1-0060>123</NEW-ADM1-0060>\n" +
                "        <control-4/>\n" +
                "    </identification>\n" +
                "    <liferaft-condition>\n" +
                "        <NEW-ADM1-0250>yes</NEW-ADM1-0250>\n" +
                "        <NEW-ADM1-0260>dry</NEW-ADM1-0260>\n" +
                "        <NEW-ADM1-0270>yes</NEW-ADM1-0270>\n" +
                "        <NEW-ADM1-0280>upper-tube</NEW-ADM1-0280>\n" +
                "    </liferaft-condition>\n" +
                "    <tests>\n" +
                "        <NEW-ADM1-0001>yes</NEW-ADM1-0001>\n" +
                "        <NEW-ADM1-0002>2018-09-06</NEW-ADM1-0002>\n" +
                "        <NEW-ADM1-0030>yes</NEW-ADM1-0030>\n" +
                "        <NEW-ADM1-0004>2018-09-06</NEW-ADM1-0004>\n" +
                "        <NEW-ADM1-0500>yes</NEW-ADM1-0500>\n" +
                "        <NEW-ADM1-0006>2018-09-13</NEW-ADM1-0006>\n" +
                "        <NEW-DL-0010>yes</NEW-DL-0010>\n" +
                "        <NEW-DL-0020>2018-09-06</NEW-DL-0020>\n" +
                "    </tests>\n" +
                "    <equipments>\n" +
                "        <NEW-ADM1-0160>Test</NEW-ADM1-0160>\n" +
                "        <NEW-ADM1-0170>autoflug-tr-1</NEW-ADM1-0170>\n" +
                "        <NEW-ADM1-0180>123</NEW-ADM1-0180>\n" +
                "        <NEW-DATE-0020>2018-09-06</NEW-DATE-0020>\n" +
                "        <NEW-DATE-0040>2018-09-06</NEW-DATE-0040>\n" +
                "        <NEW-DATE-0090>2018-09-06</NEW-DATE-0090>\n" +
                "        <NEW-DATE-0110>2018-09-06</NEW-DATE-0110>\n" +
                "        <NEW-DATE-0130>2018-09-06</NEW-DATE-0130>\n" +
                "        <NEW-DATE-0150>2018-09-06</NEW-DATE-0150>\n" +
                "        <NEW-DATE-0170>2018-09-06</NEW-DATE-0170>\n" +
                "        <NEW-DATE-0240>Test</NEW-DATE-0240>\n" +
                "        <NEW-DATE-0250>Test</NEW-DATE-0250>\n" +
                "        <NEW-DATE-0260>2018-09-06</NEW-DATE-0260>\n" +
                "        <NEW-DATE-0180>Test</NEW-DATE-0180>\n" +
                "        <NEW-DATE-0190>Test</NEW-DATE-0190>\n" +
                "        <NEW-DATE-0200>2018-09-06</NEW-DATE-0200>\n" +
                "        <NEW-HRU-0010>thanner-dk84</NEW-HRU-0010>\n" +
                "        <NEW-HRU-0020>Test</NEW-HRU-0020>\n" +
                "        <NEW-DATE-0030>2018-09-06</NEW-DATE-0030>\n" +
                "        <NEW-HRU-0050>Test</NEW-HRU-0050>\n" +
                "        <NEW-HRU-0060>2018-09-06</NEW-HRU-0060>\n" +
                "        <NEW-DATE-0219>lite-gb</NEW-DATE-0219>\n" +
                "        <NEW-DATE-0220>2018-09-06</NEW-DATE-0220>\n" +
                "        <NEW-DATE-0235>lite-gb</NEW-DATE-0235>\n" +
                "        <NEW-DATE-0236>2018-09-06</NEW-DATE-0236>\n" +
                "        <NEW-DATE-0229>lite-gb</NEW-DATE-0229>\n" +
                "        <NEW-DATE-0230>2018-09-06</NEW-DATE-0230>\n" +
                "        <NEW-DATE-0237>rm1</NEW-DATE-0237>\n" +
                "        <NEW-DATE-0238>2018-09-06</NEW-DATE-0238>\n" +
                "        <NEW-DATE-0050>Test</NEW-DATE-0050>\n" +
                "        <NEW-DATE-0051>Test</NEW-DATE-0051>\n" +
                "        <NEW-DATE-0060>Test</NEW-DATE-0060>\n" +
                "        <NEW-DATE-0070>Test</NEW-DATE-0070>\n" +
                "        <NEW-DATE-0071>Test</NEW-DATE-0071>\n" +
                "        <control-94NEW-DATE-0072>Test</control-94NEW-DATE-0072>\n" +
                "        <NEW-DATE-0073>Test</NEW-DATE-0073>\n" +
                "        <NEW-DATE-0074>Test</NEW-DATE-0074>\n" +
                "    </equipments>\n" +
                "    <cylinders>\n" +
                "        <NEW-CCYL-0010>Test</NEW-CCYL-0010>\n" +
                "        <NEW-CCYL-0020>Test</NEW-CCYL-0020>\n" +
                "        <NEW-CCYL-0030>Test</NEW-CCYL-0030>\n" +
                "        <NEW-CCYL-0040>Test</NEW-CCYL-0040>\n" +
                "        <NEW-CCYL-0050>Test</NEW-CCYL-0050>\n" +
                "        <NEW-CCYL-0060>Test</NEW-CCYL-0060>\n" +
                "        <NEW-CCYL-0070>Test</NEW-CCYL-0070>\n" +
                "        <NEW-CCYL-0080>Test</NEW-CCYL-0080>\n" +
                "        <NEW-CCYL-0090>Test</NEW-CCYL-0090>\n" +
                "        <NEW-CCYL-0100>Test</NEW-CCYL-0100>\n" +
                "        <NEW-CCYL-0110>Test</NEW-CCYL-0110>\n" +
                "        <NEW-CCYL-0120>Test</NEW-CCYL-0120>\n" +
                "        <NEW-CCYL-0130>Test</NEW-CCYL-0130>\n" +
                "        <NEW-CCYL-0140>Test</NEW-CCYL-0140>\n" +
                "        <NEW-CCYL-0150>Test</NEW-CCYL-0150>\n" +
                "        <NEW-CCYL-0160>Test</NEW-CCYL-0160>\n" +
                "    </cylinders>\n" +
                "    <verification>\n" +
                "        <NEW-ADM1-0210>2018-09-06</NEW-ADM1-0210>\n" +
                "        <CERT-0020>2018-09-06</CERT-0020>\n" +
                "        <NEW-ADM1-0220>2018-09-06</NEW-ADM1-0220>\n" +
                "        <CERT-0010>Test</CERT-0010>\n" +
                "        <control-30>Test</control-30>\n" +
                "    </verification>\n" +
                "    <comments>\n" +
                "        <CERT-0160>Test</CERT-0160>\n" +
                "        <CERT-0170>Test</CERT-0170>\n" +
                "        <CERT-0180>Test</CERT-0180>\n" +
                "        <CERT-0190>Test</CERT-0190>\n" +
                "        <CERT-0200>Test</CERT-0200>\n" +
                "    </comments>\n" +
                "    <alternative-owner-address>\n" +
                "        <CERT-0040>Test</CERT-0040>\n" +
                "        <CERT-0050>Test</CERT-0050>\n" +
                "        <CERT-0060>Test</CERT-0060>\n" +
                "        <CERT-0070>Test</CERT-0070>\n" +
                "        <CERT-0080>Test</CERT-0080>\n" +
                "    </alternative-owner-address>\n" +
                "</form>\n";

        // When
        final Map<String, Map<String, String>> result = testObj.transformNotificationYFormXML(xmlString);

        // Then
        Assert.assertNotNull("Result does not contain NEW-ADM1-0010", result.get("NEW-ADM1-0010"));
        Assert.assertNotNull("Result does not contain NEW-ADM1-0020", result.get("NEW-ADM1-0020"));
        Assert.assertNotNull("Result does not contain CERT-0050", result.get("CERT-0050"));
        Assert.assertNotNull("Result does not contain CERT-0040", result.get("CERT-0040"));
        Assert.assertNotNull("Result does not contain NEW-ADM1-0250", result.get("NEW-ADM1-0250"));
        Assert.assertEquals(85, result.values().size());
    }
}