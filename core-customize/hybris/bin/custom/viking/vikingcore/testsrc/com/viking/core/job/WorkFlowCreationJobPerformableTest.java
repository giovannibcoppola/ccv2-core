package com.viking.core.job;


import com.viking.core.dao.VikingProductDao;
import com.viking.core.event.VikingProductCreationEvent;
import com.viking.core.event.VikingProductUpdateEvent;
import com.viking.core.jalo.WorkFlowCreationCronJob;
import com.viking.core.model.WorkFlowCreationCronJobModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WorkFlowCreationJobPerformableTest {

    @InjectMocks
    private WorkFlowCreationJobPerformable workFlowCreationJobPerformable;

    @Mock
    private EventService eventService;

    @Mock
    private TimeService timeService;

    @Mock
    private VikingProductDao vikingProductDao;

    @Mock
    private ModelService modelService;

    @Captor
    private ArgumentCaptor<AbstractEvent> eventArgumentCaptor;

    @Before
    public  void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPerform(){
        final ProductModel updateProductModel = mock(ProductModel.class);
        final ProductModel createProductModel = mock(ProductModel.class);
        final WorkFlowCreationCronJobModel workFlowCreationCronJobModel = mock(WorkFlowCreationCronJobModel.class);

        final Date lastFetchDate = new GregorianCalendar(2021, Calendar.FEBRUARY, 11).getTime();
        final Date creationTime = new GregorianCalendar(2021, Calendar.FEBRUARY, 12).getTime();
        final Date creationTimeOfUpdateProduct = new GregorianCalendar(2020, Calendar.FEBRUARY, 12).getTime();
        final Date sapModificationTime = new GregorianCalendar(2021, Calendar.FEBRUARY, 13).getTime();
        final Date currentDate = new Date();

        when(workFlowCreationCronJobModel.getLastDataFetchTime()).thenReturn(lastFetchDate);
        when(updateProductModel.getSapFieldModifiedTime()).thenReturn(sapModificationTime);
        when(updateProductModel.getCreationtime()).thenReturn(creationTimeOfUpdateProduct);
        when(createProductModel.getCreationtime()).thenReturn(creationTime);
        when(timeService.getCurrentTime()).thenReturn(currentDate);
        when(vikingProductDao.findProductsWithModifiedTimeGreaterThan(lastFetchDate)).thenReturn(Arrays.asList(updateProductModel, createProductModel));

        final PerformResult performResult = workFlowCreationJobPerformable.perform(workFlowCreationCronJobModel);

        assertEquals(CronJobResult.SUCCESS, performResult.getResult());
        assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
        verify(workFlowCreationCronJobModel).setLastDataFetchTime(currentDate);
        verify(eventService, times(2)).publishEvent(eventArgumentCaptor.capture());

        List<AbstractEvent> events = eventArgumentCaptor.getAllValues();

        assertEquals(1, events.stream().filter(e -> e instanceof VikingProductUpdateEvent).count());
        assertEquals(1, events.stream().filter(e -> e instanceof VikingProductCreationEvent).count());
    }
}

