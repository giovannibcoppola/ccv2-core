package com.viking.core.search.solrfacetsearch.provider.impl

import com.viking.core.enums.ApprovalAuthority
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.enumeration.EnumerationService
import de.hybris.platform.servicelayer.i18n.CommonI18NService
import de.hybris.platform.solrfacetsearch.config.IndexedProperty
import de.hybris.platform.solrfacetsearch.search.SearchQuery
import org.junit.Test
import spock.lang.Specification

@UnitTest
class EnumDisplayNameProviderTest extends Specification {
    EnumerationService enumerationService = Stub()
    CommonI18NService commonI18NService = Stub()
    SearchQuery searchQuery = Stub()
    IndexedProperty indexedProperty = Stub()

    EnumDisplayNameProvider provider = new EnumDisplayNameProvider(enumerationService, commonI18NService)

    @Test
    def "localized label for enumeration"() {
        given:
        def thePkg = "com.viking.core.enums."
        def theEnum = "ApprovalAuthority"
        def value = "ISO"
        when:
        def displayName = provider.getDisplayName(searchQuery, indexedProperty, "${thePkg}${theEnum}:${value}")

        then:

        searchQuery.getLanguage() >> "de"
        commonI18NService.getLocaleForIsoCode("de") >> Locale.GERMAN
        enumerationService.getEnumerationValue(theEnum, value) >> ApprovalAuthority.ISO
        enumerationService.getEnumerationName(ApprovalAuthority.ISO, Locale.GERMAN) >> "ISO_DE"
        displayName == "ISO_DE"
    }

    @Test
    def "return enumeration code when lang not found"() {
        given:
        def thePkg = "com.viking.core.enums."
        def theEnum = "ApprovalAuthority"
        def value = "ISO"
        when:
        def displayName = provider.getDisplayName(searchQuery, indexedProperty, "${thePkg}${theEnum}:${value}")

        then:

        searchQuery.getLanguage() >> "it"
        commonI18NService.getLocaleForIsoCode("it") >> Locale.ITALIAN
        enumerationService.getEnumerationValue(theEnum, value) >> ApprovalAuthority.ISO
        displayName == "ISO"
    }


}
