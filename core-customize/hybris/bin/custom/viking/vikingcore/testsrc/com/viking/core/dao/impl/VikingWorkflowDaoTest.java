package com.viking.core.dao.impl;

import com.viking.core.dao.VikingWorkflowDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

@UnitTest
public class VikingWorkflowDaoTest {
    @Mock
    private VikingWorkflowDao vikingWorkflowDao;

    @Mock
    private ModelService modelService;

    @Before
    public void testSetup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAllWorkflowActionInProgressForProduct()
    {
        final List<WorkflowActionModel> result = vikingWorkflowDao.findAllWorkflowActionInProgressForProduct("VIKING_PR_CREATION", modelService.get(PK.parse("8796093066078")));
        Assert.assertTrue(result.isEmpty());
    }
}
