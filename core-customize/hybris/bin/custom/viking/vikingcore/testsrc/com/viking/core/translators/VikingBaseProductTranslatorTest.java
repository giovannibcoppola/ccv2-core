package com.viking.core.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.impl.DefaultCatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import de.hybris.platform.servicelayer.type.impl.DefaultTypeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class VikingBaseProductTranslatorTest {

    @InjectMocks
    private VikingBaseProductTranslator vikingBaseProductTranslator;

    @Mock
    private DefaultProductService defaultProductService;
    @Mock
    private DefaultModelService modelService;
    @Mock
    private DefaultCatalogVersionService defaultCatalogVersionService;
    @Mock
    private DefaultTypeService defaultTypeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_null_Code() throws JaloBusinessException {
        final Item varianytProduct = mock(Item.class);
        final Object result = vikingBaseProductTranslator.importValue(null, varianytProduct);

        assertEquals(null, result);

    }

    @Test
    public void testImport_Code_Found() throws JaloBusinessException {
        final Item varianytProduct = mock(Item.class);
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        when(defaultCatalogVersionService.getCatalogVersion(eq(vikingBaseProductTranslator.CATALOG_ID), eq(vikingBaseProductTranslator.CATALOG_VERSION_NAME))).thenReturn(catalogVersionModel);
        when(defaultProductService.getProductForCode(eq(catalogVersionModel), eq(cellValue))).thenReturn(productModel);
        when(modelService.getSource(productModel)).thenReturn(product);

        final Object result = vikingBaseProductTranslator.importValue(cellValue, varianytProduct);

        assertEquals(product, result);
    }

    @Test
    public void testImport_Code_NOT_Found() throws JaloBusinessException {
        final Item varianytProduct = mock(Item.class);
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

        when(defaultCatalogVersionService.getCatalogVersion(eq(vikingBaseProductTranslator.CATALOG_ID), eq(vikingBaseProductTranslator.CATALOG_VERSION_NAME))).thenReturn(catalogVersionModel);
        when(defaultProductService.getProductForCode(eq(catalogVersionModel), eq(cellValue))).thenThrow(UnknownIdentifierException.class);
        when(modelService.create(ProductModel.class)).thenReturn(productModel);
        when(modelService.getSource(productModel)).thenReturn(product);

        final Object result = vikingBaseProductTranslator.importValue(cellValue, varianytProduct);

        assertEquals(product, result);
    }

    @Test
    public void testExport() throws JaloBusinessException {
        final Item varianytProduct = mock(Item.class);

        final String result = vikingBaseProductTranslator.exportValue(varianytProduct);

        assertEquals(null, result);
    }
}
