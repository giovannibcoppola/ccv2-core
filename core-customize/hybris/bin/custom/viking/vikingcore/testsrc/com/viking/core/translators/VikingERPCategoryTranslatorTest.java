package com.viking.core.translators;

import com.viking.core.constants.VikingCoreConstants;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.sap.sapmodel.jalo.ERPVariantProduct;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
public class VikingERPCategoryTranslatorTest {

    @InjectMocks
    private VikingERPCategoryTranslator vikingERPCategoryTranslator;

    @Mock
    private DefaultModelService modelService;

    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private CategoryService categoryService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_null_Code() throws JaloBusinessException {
        final Item variantProduct = mock(Item.class);
        vikingERPCategoryTranslator.performImport(null, variantProduct);

        verify(modelService, never()).save(any());

    }

    @Test
    public void testImport_Variant_Code_Found() throws JaloBusinessException {
        final String cellValue = "123456";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        final CategoryModel categoryModel = mock(CategoryModel.class);

        when(catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(categoryService.getCategoryForCode(eq(catalogVersionModel), eq(cellValue))).thenReturn(categoryModel);
        when(modelService.get(any(Product.class))).thenReturn(variantModel);

        vikingERPCategoryTranslator.performImport(cellValue, variant);

        verify(modelService, times(1)).save(variantModel);
        verify(variantModel, times(1)).setSupercategories(anyCollection());
    }


    @Test
    public void testImport_Base_Code_NotFound() throws JaloBusinessException {
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        final CategoryModel categoryModel = mock(CategoryModel.class);

        when(catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(categoryService.getCategoryForCode(eq(catalogVersionModel), eq(cellValue))).thenThrow(UnknownIdentifierException.class);
        when(modelService.get(any(Product.class))).thenReturn(productModel);
        when(modelService.create(CategoryModel.class)).thenReturn(categoryModel);

        vikingERPCategoryTranslator.performImport(cellValue, product);

        verify(modelService, times(1)).save(productModel);
        verify(modelService, times(1)).save(categoryModel);
        verify(productModel, times(1)).setSupercategories(anyCollection());
    }

    @Test
    public void testImport_NewProduct() throws JaloBusinessException {
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
        final CategoryModel categoryModel = mock(CategoryModel.class);

        when(catalogVersionService.getCatalogVersion(VikingCoreConstants.DEFAULT_CATALOG, VikingCoreConstants.DEFAULT_CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(categoryService.getCategoryForCode(eq(catalogVersionModel), eq(cellValue))).thenReturn(categoryModel);
        when(modelService.get(any(Product.class))).thenReturn(productModel);

        vikingERPCategoryTranslator.performImport(cellValue, product);

    }

}
