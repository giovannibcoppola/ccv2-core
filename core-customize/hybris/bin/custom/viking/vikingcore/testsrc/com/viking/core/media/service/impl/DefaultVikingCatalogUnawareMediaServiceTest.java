package com.viking.core.media.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.impl.MediaDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Chiranjit Chakraborty
 */
@UnitTest
public class DefaultVikingCatalogUnawareMediaServiceTest {

    private static final String MEDIA_CODE = "test";

    @InjectMocks
    private DefaultVikingCatalogUnawareMediaService  defaultVikingCatalogUnawareMediaService;

    @Mock
    private MediaDao mediaDao;

    @Mock
    private CatalogUnawareMediaModel catalogUnawareMediaModel;

    @Mock
    private MediaModel mediaModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetCatalogUnawareMedia_no_code(){
        defaultVikingCatalogUnawareMediaService.getCatalogUnawareMediaForCode(null);
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testGetCatalogUnawareMedia_no_CatalogAwareMedia(){

        when(mediaDao.findMediaByCode(MEDIA_CODE)).thenReturn(Arrays.asList(mediaModel));

       final Optional<CatalogUnawareMediaModel> result = defaultVikingCatalogUnawareMediaService.getCatalogUnawareMediaForCode("test");

       assertFalse(result.isPresent());
    }

    @Test
    public void testGetCatalogUnawareMedia_with_CatalogAwareMedia(){

        when(mediaDao.findMediaByCode(MEDIA_CODE)).thenReturn(Arrays.asList(mediaModel, catalogUnawareMediaModel));

        final Optional<CatalogUnawareMediaModel> result = defaultVikingCatalogUnawareMediaService.getCatalogUnawareMediaForCode("test");

        assertFalse(result.isEmpty());
        assertEquals(catalogUnawareMediaModel, result.get());
    }

    @Test(expected = AmbiguousIdentifierException.class)
    public void testGetCatalogUnawareMedia_with_duplicate_CatalogAwareMedia(){
        final CatalogUnawareMediaModel catalogUnawareMediaModelDuplicate = mock(CatalogUnawareMediaModel.class);
        when(mediaDao.findMediaByCode(MEDIA_CODE)).thenReturn(Arrays.asList(mediaModel, catalogUnawareMediaModel, catalogUnawareMediaModelDuplicate));

        defaultVikingCatalogUnawareMediaService.getCatalogUnawareMediaForCode("test");
    }
}
