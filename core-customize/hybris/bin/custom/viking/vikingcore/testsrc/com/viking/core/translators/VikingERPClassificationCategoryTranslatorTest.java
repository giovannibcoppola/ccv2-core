package com.viking.core.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.impl.DefaultClassificationClassesResolverStrategy;
import de.hybris.platform.classification.impl.DefaultClassificationSystemService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.sap.sapmodel.jalo.ERPVariantProduct;
import de.hybris.platform.sap.sapmodel.model.ERPVariantProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@UnitTest
public class VikingERPClassificationCategoryTranslatorTest {

    @InjectMocks
    private VikingERPClassificationCategoryTranslator vikingERPClassificationCategoryTranslator;

    @Mock
    private DefaultModelService modelService;
    @Mock
    private DefaultClassificationSystemService defaultClassificationSystemService;
    @Mock
    private DefaultClassificationClassesResolverStrategy defaultClassificationClassesResolverStrategy;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_null_Code() throws JaloBusinessException {
        final Item variantProduct = mock(Item.class);
        vikingERPClassificationCategoryTranslator.performImport(null, variantProduct);

        verify(modelService, never()).save(any());

    }

    @Test
    public void testImport_Variant_Code_Found() throws JaloBusinessException {
        final String cellValue = "123456";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);
        final ClassificationSystemVersionModel catalogVersionModel = mock(ClassificationSystemVersionModel.class);
        final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);

        when(defaultClassificationSystemService.getSystemVersion(vikingERPClassificationCategoryTranslator.CATALOG_ID, vikingERPClassificationCategoryTranslator.CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(defaultClassificationSystemService.getClassForCode(eq(catalogVersionModel), eq(cellValue))).thenReturn(categoryModel);
        when(modelService.get(any(Product.class))).thenReturn(variantModel);

        vikingERPClassificationCategoryTranslator.performImport(cellValue, variant);

        verify(modelService, times(1)).save(variantModel);
        verify(variantModel, times(1)).setSupercategories(anyCollection());
    }

    @Test
    public void testImport_Base_Code_Found() throws JaloBusinessException {
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final ClassificationSystemVersionModel catalogVersionModel = mock(ClassificationSystemVersionModel.class);
        final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);

        when(defaultClassificationSystemService.getSystemVersion(vikingERPClassificationCategoryTranslator.CATALOG_ID, vikingERPClassificationCategoryTranslator.CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(defaultClassificationSystemService.getClassForCode(eq(catalogVersionModel), eq(cellValue))).thenReturn(categoryModel);
        when(modelService.get(any(Product.class))).thenReturn(productModel);

        vikingERPClassificationCategoryTranslator.performImport(cellValue, product);

        verify(modelService, times(1)).save(productModel);
        verify(productModel, times(1)).setSupercategories(anyCollection());
    }

    @Test
    public void testImport_Variant_Code_NotFound() throws JaloBusinessException {
        final String cellValue = "123456";
        final ERPVariantProductModel variantModel = mock(ERPVariantProductModel.class);
        final ERPVariantProduct variant = mock(ERPVariantProduct.class);
        final ClassificationSystemVersionModel catalogVersionModel = mock(ClassificationSystemVersionModel.class);
        final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);

        when(defaultClassificationSystemService.getSystemVersion(vikingERPClassificationCategoryTranslator.CATALOG_ID, vikingERPClassificationCategoryTranslator.CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(defaultClassificationSystemService.getClassForCode(eq(catalogVersionModel), eq(cellValue))).thenThrow(UnknownIdentifierException.class);
        when(modelService.get(any(Product.class))).thenReturn(variantModel);
        when(modelService.create(ClassificationClassModel.class)).thenReturn(categoryModel);

        vikingERPClassificationCategoryTranslator.performImport(cellValue, variant);

        verify(modelService, times(1)).save(variantModel);
        verify(modelService, times(1)).save(categoryModel);
        verify(variantModel, times(1)).setSupercategories(anyCollection());
    }

    @Test
    public void testImport_Base_Code_NotFound() throws JaloBusinessException {
        final String cellValue = "123456";
        final ProductModel productModel = mock(ProductModel.class);
        final Product product = mock(Product.class);
        final ClassificationSystemVersionModel catalogVersionModel = mock(ClassificationSystemVersionModel.class);
        final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);

        when(defaultClassificationSystemService.getSystemVersion(vikingERPClassificationCategoryTranslator.CATALOG_ID, vikingERPClassificationCategoryTranslator.CATALOG_VERSION_NAME)).thenReturn(catalogVersionModel);
        when(defaultClassificationSystemService.getClassForCode(eq(catalogVersionModel), eq(cellValue))).thenThrow(UnknownIdentifierException.class);
        when(modelService.get(any(Product.class))).thenReturn(productModel);
        when(modelService.create(ClassificationClassModel.class)).thenReturn(categoryModel);

        vikingERPClassificationCategoryTranslator.performImport(cellValue, product);

        verify(modelService, times(1)).save(productModel);
        verify(modelService, times(1)).save(categoryModel);
        verify(productModel, times(1)).setSupercategories(anyCollection());
    }

}
