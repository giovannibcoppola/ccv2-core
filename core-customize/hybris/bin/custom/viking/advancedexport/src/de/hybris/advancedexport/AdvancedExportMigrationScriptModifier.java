package de.hybris.advancedexport;

import de.hybris.platform.catalog.jalo.classification.impex.ProductFeatureValueTranslator;
import de.hybris.platform.impex.jalo.exp.ScriptGenerator;
import de.hybris.platform.impex.jalo.exp.generator.HeaderLibraryGenerator;
import de.hybris.platform.impex.jalo.exp.generator.MigrationScriptModifier;
import de.hybris.platform.impex.jalo.media.MediaDataTranslator;
import de.hybris.platform.impex.jalo.translators.TaxValuesTranslator;
import de.hybris.platform.impex.jalo.translators.UserPasswordTranslator;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Extension from MigrationScriptModifier to remove/disable password re-encoder which was always added
 * <p>Creation Date: 24.04.20 2020</p>
 */
public class AdvancedExportMigrationScriptModifier extends MigrationScriptModifier {
    private static final Logger LOG = getLogger(AdvancedExportMigrationScriptModifier.class);

    @SuppressWarnings("squid:S1192")
    @Override
    public void init(ScriptGenerator generator) {
        LOG.info("Script modifier - add additional ignore columns");
        if (!(generator instanceof AdvancedTypeExportScriptGenerator)) {
            generator.addIgnoreColumn("Item", "allDocuments");
            generator.addIgnoreColumn("Website", "rootNavigationElements");
            generator.addIgnoreColumn("Catalog", "rootCategories");
            generator.addIgnoreColumn("CatalogVersion", "rootCategories");
            generator.addIgnoreColumn("LuceneIndex", "indexConfigurations");
            generator.addIgnoreColumn("LuceneIndex", "languageConfigurations");
            generator.addIgnoreColumn("IndexConfiguration", "attributeConfigurations");
            generator.addIgnoreColumn("IndexConfiguration", "pendingUpdates");
            generator.addIgnoreColumn("StyleContainer", "owner");
            generator.addIgnoreColumn("Product", "features");
            generator.addIgnoreColumn("Product", "untypedFeatures");
            generator.addIgnoreColumn("ClassificationSystem", "catalogVersions");
            generator.addIgnoreColumn("ClassificationClass", "declaredClassificationAttributeAssignments");
            generator.addIgnoreColumn("PDTRow", "dateRange");
            generator.addIgnoreColumn("Template", "content");
        }
        LOG.info("Script modifier - add additional special columns");
        if (generator instanceof HeaderLibraryGenerator) { // Only makes sense for imports, not exports
            generator.addSpecialColumn("User", "password[translator=" + UserPasswordTranslator.class.getName() + ']');
        }
        generator.addSpecialColumn("Media", "media[translator=" + MediaDataTranslator.class.getName() + ']');
        LOG.info("Script modifier - add additional modifier");
       // generator.addAdditionalModifier("ProductFeature", "value", "translator", "de.hybris.platform.catalog.jalo.classification.impex.ProductFeatureValueTranslator");
        generator.addAdditionalModifier("OrderEntry", "taxValues", "translator", TaxValuesTranslator.class.getName());
        generator.addAdditionalModifier("CartEntry", "taxValues", "translator", TaxValuesTranslator.class.getName());

        generator.addAdditionalColumn("ProductFeature", "value[translator="+ ProductFeatureValueTranslator.class.getName() + "]");

        generator.addIgnoreColumn("ProductFeature", "booleanValue");
        generator.addIgnoreColumn("ProductFeature", "numberValue");
        generator.addIgnoreColumn("ProductFeature", "stringValue");
        generator.addIgnoreColumn("ProductFeature", "rawValue");

        if (!(generator instanceof AdvancedTypeExportScriptGenerator)) {
            generator.addAdditionalModifier("Media", "mediaFormat", "forceWrite", "true");
            generator.addAdditionalModifier("Item", "owner", "forceWrite", "true");
            generator.addAdditionalModifier("PDTRow", "pg", "forceWrite", "true");
            generator.addAdditionalModifier("PDTRow", "product", "forceWrite", "true");
            generator.addAdditionalModifier("PDTRow", "ug", "forceWrite", "true");
            generator.addAdditionalModifier("PDTRow", "user", "forceWrite", "true");
            generator.addAdditionalModifier("AttributeValueAssignment", "systemVersion", "forceWrite", "true");
        }
    }
}
