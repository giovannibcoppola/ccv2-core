package de.hybris.advancedexport;

import de.hybris.advancedexport.jalo.AdvancedExportColumnExpressionReplacement;
import de.hybris.advancedexport.jalo.AdvancedExportModifier;
import de.hybris.advancedexport.jalo.AdvancedTypeExportConfiguration;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.constants.ImpExConstants;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.exp.generator.ExportScriptGenerator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.JaloItemNotFoundException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SearchResult;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearchException;
import de.hybris.platform.jalo.flexiblesearch.QueryOptions;
import de.hybris.platform.jalo.security.UserRight;
import de.hybris.platform.jalo.type.AttributeDescriptor;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.RelationDescriptor;
import de.hybris.platform.jalo.type.RelationType;
import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;


/**
 * Advanced Export Script Generator
 *
 * @author brendan dobbs (hybris)
 */
@SuppressWarnings({"deprecated"}) // Hybris delivered class
public class AdvancedTypeExportScriptGenerator extends ExportScriptGenerator {
    public static final String TYPE_CODE_MEDIA = "Media";
    public static final String WHERE = " WHERE ";
    public static final String JAVA_LANG_DOUBLE = "java.lang.Double";
    public static final String NUMBER_FORMAT = "numberFormat";
    public static final String JAVA_LANG_LONG = "java.lang.Long";
    public static final String JAVA_LANG_INTEGER = "java.Lang.Integer";
    public static final String FORMAT_DOUBLE = "#.##";
    public static final String FORMAT_INTEGER = "##0";
    public static final String ORDER_ENTRY = "OrderEntry";
    public static final String DATE_FORMAT_SIMPLE = "yyyy-MM-dd HH:mm:ss";
    private final Set<ComposedType> exportTypes;
    private final Set<CatalogVersion> catalogVersions;
    private final Set<AttributeDescriptor> blacklistedAttributes;
    private final Set<AttributeDescriptor> relationAttribAsRelationAttributes;
    private final Set<AdvancedExportModifier> additionalConfiguredModifiers;
    private final Set<ComposedType> blacklistedTypes;
    private AdvancedTypeExportConfiguration exportConfig;
    private final Map<String, ComposedType> readOnlyAttributes;
    private final Map<String, ComposedType> writeOnlyAttributes;
    private final StringBuilder manualQueries = new StringBuilder();
    private final JdbcTemplate jdbcTemplate;
    private final Set<CatalogVersion> backofficeCatalogVersions;
    private final Map<ComposedType, Set<AttributeDescriptor>> typesWithConfiguredUniqueAttributes = new HashMap<>();
    private final Set<AdvancedExportColumnExpressionReplacement> columnExpressionReplacements;
    private final Date fromDate;
    private static final List<String> DONT_LOG_IGNORED_ATTRIBUTE_NAMES = Arrays.asList("pk", "sealed", "owner", "creationtime", "savedvalues", "alldocuments", "assignedcockpititemtemplates");

    private static final Logger LOG = getLogger(AdvancedTypeExportScriptGenerator.class);


    private static final String TYPE_CODE_ADDRESS = "Address";


    /**
     * constructor
     *  @param types                              list of types to export (use Item for all, includes all subtypes)
     * @param catalogVersions                    list of catalog versions to export (leave empty for all)
     * @param blacklistedAttributes              list of blacklisted attributes
     * @param blacklistedTypes                   list of blacklisted types
     * @param languages                          list of languages to export (leave empty for all active languages)
     * @param relationAttribAsRelationAttributes list of relation attributes that should be exported as relations instead of attributes.  Used eg. when dealing with a circular dependency.
     * @param additionalModifiers                set of additional modifiers (eg. to configure unique columns on types that have no unique attributes)
     * @param columnExpressionReplacements       set of column descriptor replacements. Used eg. to use a documentid instead of a unique reference to a composed type with optional (nullable) unique attributes
     */
    @SuppressWarnings("squid:S00107")
    public AdvancedTypeExportScriptGenerator(final List<ComposedType> types, final Set<CatalogVersion> catalogVersions, final Set<AttributeDescriptor> blacklistedAttributes, final Set<ComposedType> blacklistedTypes,
                                             final Set<Language> languages, final Set<AttributeDescriptor> relationAttribAsRelationAttributes, Set<AdvancedExportModifier> additionalModifiers, Set<AdvancedExportColumnExpressionReplacement> columnExpressionReplacements, final Date fromDate) {
        this.fromDate = fromDate;
        this.catalogVersions = catalogVersions;
        this.blacklistedAttributes = mergeWithSubDescriptors(SetUtils.emptyIfNull(blacklistedAttributes));
        this.relationAttribAsRelationAttributes = mergeWithSubDescriptors(SetUtils.emptyIfNull(relationAttribAsRelationAttributes));
        this.blacklistedTypes = blacklistedTypes;
        this.exportTypes = findAllSubTypes(types);
        if (languages == null || languages.isEmpty()) {
            this.setLanguages(getAllActiveLanguages());
        } else {
            this.setLanguages(languages);
        }
        this.additionalConfiguredModifiers = SetUtils.emptyIfNull(additionalModifiers);
        this.registerScriptModifier(new AdvancedExportMigrationScriptModifier());
        this.readOnlyAttributes = new LinkedHashMap<>();
        this.writeOnlyAttributes = new LinkedHashMap<>();
        jdbcTemplate = (JdbcTemplate) Registry.getApplicationContext().getBean("jdbcTemplate");
        backofficeCatalogVersions = getBackofficeCatalogVersions();
        this.columnExpressionReplacements = SetUtils.emptyIfNull(columnExpressionReplacements);
    }

    /**
     * Convenient constructor to be used with a cronjob export configuration
     *
     * @param exportConfig AdvancedTypeExportConfiguration
     */
    public AdvancedTypeExportScriptGenerator(final AdvancedTypeExportConfiguration exportConfig) {
        this(exportConfig.getTypesToExport(), exportConfig.getCatalogVersionsToExport(), exportConfig.getBlacklistedAttributes(), exportConfig.getBlacklistedTypes(), exportConfig.getLanguages(), exportConfig.getRelationAttribAsRelationAttributes(), exportConfig.getModifiers(), exportConfig.getColumnExpressionReplacements(), exportConfig.getFromDate());
        this.exportConfig = exportConfig;
    }

    public Set<Language> getAllActiveLanguages() {
        CommonI18NService commonI18NService = (CommonI18NService) Registry.getApplicationContext().getBean("commonI18NService");
        ModelService modelService = (ModelService) Registry.getApplicationContext().getBean("modelService");
        return commonI18NService.getAllLanguages().stream().filter(l -> Boolean.TRUE.equals(l.getActive())).map(modelService::<Language>getSource).collect(Collectors.toSet());
    }

    protected Set<AttributeDescriptor> mergeWithSubDescriptors(final Set<AttributeDescriptor> blkAttrs) {
        final Set<AttributeDescriptor> allAttributeDescriptors = new HashSet<>(blkAttrs);
        for (final AttributeDescriptor ad : blkAttrs) {
            allAttributeDescriptors.addAll(ad.getAllSubAttributeDescriptors());
        }
        return allAttributeDescriptors;
    }

    /**
     * override of parent to be able to ignore blacklisted attributes, even if they are added by the MigrationScriptModifier
     *
     * @param typeString code of the composed type
     * @param column     qualifier of the attribute
     * @param modifier   modifier (eg unique, forceWrite, ...)
     * @param value      value of the modifier
     */
    @Override
    public void addAdditionalModifier(String typeString, String column, String modifier, String value) {
        try {
            ComposedType type = TypeManager.getInstance().getComposedType(typeString);
            AttributeDescriptor ad = type.getAttributeDescriptor(column);
            addAdditionalModifier(ad, modifier, value);
        } catch (JaloItemNotFoundException var8) {
            LOG.warn("Can not find type " + typeString + ", will not add additional modifier " + modifier + " to attribute " + column);
        }

    }

    /**
     * Add additional modifiers (like eg. forceWrite, unique etc)
     * Override of parent to be able to add forceWrite with true, even if they are added by the MigrationScriptModifier with null
     *
     * @param ad       attribute descriptor
     * @param modifier column modifier (eg forceWrite, translator, lang, ...)
     * @param value    modifier value (eg true, or the name of a translator)
     */
    private void addAdditionalModifier(AttributeDescriptor ad, String modifier, String value) {
        if (!blacklistedAttributes.contains(ad)) {
            // Fix hybris bug where foreWrite is initialized with null for all owners. This breaks the forceWrite functionality
            if ("forceWrite".equalsIgnoreCase(modifier) && value == null && !ad.isWritable()) {
                value = "true";
            }
            super.addAdditionalModifier(ad.getEnclosingType().getCode(), ad.getQualifier(), modifier, value);
            for (AttributeDescriptor subAd : ad.getAllSubAttributeDescriptors()) {
                if (!blacklistedAttributes.contains(subAd)) {
                    addAdditionalModifier(subAd, modifier, value);
                } else {
                    LOG.info("Ignoring additional modifier {} {} for subAttribute {}:{}", modifier, value, subAd.getEnclosingType().getCode(), subAd.getQualifier());
                }
            }
        } else {
            LOG.info("Ignoring additional modifier {} {} for attribute {}:{}", modifier, value, ad.getEnclosingType().getCode(), ad.getQualifier());
        }

    }

    /**
     * Recursively find all subtypes for given parent types
     *
     * @param types parent types
     * @return subtypes of all parent types
     */
    private Set<ComposedType> findAllSubTypes(final List<ComposedType> types) {
        final Set<ComposedType> allExportTypes = new LinkedHashSet<>();
        for (final ComposedType type : types) {
            allExportTypes.addAll(findSubTypes(type));
        }
        return allExportTypes;
    }

    @Override
    protected Set<ComposedType> determineInitialTypes() {
        return exportTypes;
    }

    /**
     * Find all subtypes for a given parent, recursively.  Filtered out are abstract, jaloOnly, blacklisted types, relations and types without actual data
     *
     * @param parent a composed type (use 'Item' to export all)
     * @return set of composed types (incl. parent).
     */
    protected Set<ComposedType> findSubTypes(final ComposedType parent) {

        assert parent != null;

        final Set<ComposedType> allSubTypes = new LinkedHashSet<>();
        boolean isSkipRelationType = skipRelationType(parent);
        if (!parent.isAbstract() && !parent.isJaloOnly() && haveItemsForType(parent) && !blacklistedTypes.contains(parent) && !isSkipRelationType) {
            allSubTypes.add(parent);
        } else {
            boolean hasItems = !parent.isAbstract() && !parent.isJaloOnly() && haveItemsForType(parent);
            LOG.info("Skipping item {} because abstract:{},jaloOnly:{}, blacklisted: {}, hasItems: {}, skipRelationType: {}", parent.getCode(), parent.isAbstract(), parent.isJaloOnly(), blacklistedTypes.contains(parent), hasItems, isSkipRelationType);
        }

        if (!blacklistedTypes.contains(parent) && CollectionUtils.isNotEmpty(parent.getSubTypes())) {
            for (final ComposedType type : parent.getSubTypes()) {
                // append all the sub types of we have any

                allSubTypes.addAll(findSubTypes(type));
            }
        }

        return allSubTypes;
    }

    /**
     * Unless relationAttribAsRelationAttributes contains the given composedType, we don't need to export it
     *
     * @param composedType the type to check
     * @return true if composedType is 1. a RelationType and 2. defined in relationAttribAsRelationAttributes for either source or target
     */
    private boolean skipRelationType(ComposedType composedType) {
        if (composedType instanceof RelationType) {
            RelationType rel = (RelationType) composedType;
            boolean hasRelationAttribAsRelationAttributes = relationAttribAsRelationAttributes.contains(rel.getTargetAttributeDescriptor()) || relationAttribAsRelationAttributes.contains(rel.getSourceAttributeDescriptor());
            return !hasRelationAttribAsRelationAttributes;
        }
        return false;
    }

    /**
     * @param type composed type
     * @return Select query (incl. catalogVersion, excl backofficeCatalogVersions if relevant) for given type
     */
    protected String generateQueryForType(final ComposedType type) {
        // query that returns all items but no subtypes
        StringBuilder query = new StringBuilder("SELECT {PK} FROM {" + type.getCode() + "!}");
        if (isCatalogItem(type)) {

            if (CollectionUtils.isNotEmpty(catalogVersions)) {
                query.append(WHERE).append("{").append(getCatalogVersionAttribute(type).getQualifier()).append("} IN (");
                for (final CatalogVersion catalogVersion : catalogVersions) {
                    query.append(catalogVersion.getPK().toString()).append(",");
                }

                query = new StringBuilder(StringUtils.removeEnd(query.toString(), ","));
                query.append(")");
            } else if (!type.getCode().equalsIgnoreCase(TYPE_CODE_MEDIA) && CollectionUtils.isNotEmpty(backofficeCatalogVersions)) {
                query.append(WHERE)
                        .append("({").append(getCatalogVersionAttribute(type).getQualifier()).append("} IS NULL OR ")  // FIX FOR missing media items
                        .append("{").append(getCatalogVersionAttribute(type).getQualifier()).append("} NOT IN (");
                query.append(backofficeCatalogVersions.stream().map(CatalogVersion::getPK).map(PK::toString).collect(Collectors.joining(",")));
                query.append("))");
            }
        }
        if (type.getCode().equalsIgnoreCase(TYPE_CODE_MEDIA)) {
            // Do not export the backoffice configuration as that will mess up the backoffice in the target system.
            addWhereOrAndToQuery(query);
            query.append(" {code} NOT IN ('cockpitng-config', 'cockpitng-widgets-config')");
        } else if (type.getCode().equalsIgnoreCase("ComposedType")) {
            // We need to export the ComposedType as some items (eg. indexedType are referencing composedTypes
            // But these ones raise an error:
            // due to cannot mark type Composed Type 'InMemoryCart'[8796098330706] extends Cart ([]) as jalo-only singleton since the jalo class (class de.hybris.platform.servicelayer.internal.jalo.order.InMemoryCart) does not implement the interface de.hybris.platform.jalo.JaloOnlySingletonItem
            addWhereOrAndToQuery(query);
            query.append(" {code} NOT IN ('CachedPromotionOrderAddFreeGiftAction', 'CachedPromotionNullAction','CachedPromotionOrderAdjustTotalAction','CachedPromotionOrderChangeDeliveryModeAction','CachedPromotionOrderEntryAdjustAction','InMemoryCartEntry','CachedPromotionOrderEntryConsumed','InMemoryCart','MultiAddressInMemoryCart','CachedPromotionResult')");
        } else  if (type.getCode().equalsIgnoreCase(TYPE_CODE_ADDRESS)) {
            addWhereOrAndToQuery(query);
            query.append(" {owner} IS NOT NULL");
        } else  if (type.getCode().equalsIgnoreCase(ORDER_ENTRY)) {
            addWhereOrAndToQuery(query);
            query.append(" {product} IS NOT NULL");
        }
        if(fromDate != null) {
            addWhereOrAndToQuery(query);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_SIMPLE);
            final String fromDateTime = simpleDateFormat.format(fromDate);
            query.append(" {creationtime} >= '" + fromDateTime + "' OR {modifiedtime} >= '" + fromDateTime + "'");
        }
        query.append(" ORDER BY {PK}");

        System.out.println(query.toString());

        return query.toString();
    }

    private void addWhereOrAndToQuery(StringBuilder query) {
        if (query.toString().contains(WHERE)) {
            query.append(" AND ");
        } else {
            query.append(WHERE);
        }
    }

    @Override
    protected void writeScript() throws IOException {
        writeComment(" -------------------------------------------------------");
        writeComment("# used 'header validation mode' during script generation was: " + ImpExManager.getImportStrictMode().getCode());
        final Locale thisLocale = JaloSession.getCurrentSession().getSessionContext().getLanguage().getLocale();
        writeBeanShell("impex.setLocale( new Locale( \"" + thisLocale.getLanguage() + "\" , \"" + thisLocale.getCountry() + "\" ) )");
        writeComment(" -------------------------------------------------------");
        // Why we are looping twice over the entire tree ?
        // Because of circular and other complex dependencies
        // We first create the items with only the unique & non-optional data
        // Then we run through the list again to update the items with all the optional data
        // Examples: language has relation with ... language, Catalog has relation with CatalogVersion and Catalog
        for (final ComposedType type : getTypes()) {
            if (LOG.isDebugEnabled()) {
                LOG.info("generating essential script statements for type " + type.getCode());
            }

            getScriptWriter().writeSrcLine("");
            writeComment("---- Extension: " + type.getExtensionName() + " ---- Type: " + type.getCode() + " ---- Essential: true ----");
            boolean hasUniqueColumns = hasUniqueColumns(type);
            if (isUserRightType(type)) {
                writeComment("---- Skipping essential export for UserRight ----");
            } else if (!hasUniqueColumns && isUseDocumentID()) {
                writeComment("---- Skipping essential export for type because no unique columns are detected ----");
            } else if("YFormDataHistory".equalsIgnoreCase(type.getCode())){
                writeComment("---- Skipping essential export for type " +type.getCode()+" because we only want this data once ----");
            } else {
                writeTargetFileStatement(type, "-essential.csv");
                writeHeader(type, true);
                writeExportStatement(type, false);
            }
        }
        for (final ComposedType type : getTypes()) {
            if (LOG.isDebugEnabled()) {
                LOG.info("generating script statements for type " + type.getCode());
            }

            getScriptWriter().writeSrcLine("");
            writeComment("---- Extension: " + type.getExtensionName() + " ---- Type: " + type.getCode() + " ---- Essential: false ----");

            writeTargetFileStatement(type, "-full.csv");
            writeHeader(type, false);
            writeExportStatement(type, false);
        }
        getScriptWriter().writeSrcLine("");
        // Yes, this is hackish...  But for some reason the above impexes do not respect the encoded password on the target system. Re-importing the passwords explicitly seems to do the trick
        if(exportTypes.stream().anyMatch(t->"User".equalsIgnoreCase(t.getCode())|| "Employee".equalsIgnoreCase(t.getCode())|| "Customer".equalsIgnoreCase(t.getCode()) || "B2BCustomer".equalsIgnoreCase(t.getCode()) )){
            writeComment("---- Extension: core ---- Type: User ---- Manual: true ----");
            getScriptWriter().writeSrcLine("#% impex.setTargetFile( \"User-password.csv\" );");
            getScriptWriter().writeSrcLine("INSERT_UPDATE User;UID[unique=true];passwordEncoding;encodedPassword;");
            getScriptWriter().writeSrcLine("#% impex.exportItemsFlexibleSearch( \"SELECT {PK} FROM {User} WHERE {uid} NOT IN ('admin','anonymous') ORDER BY {PK}\");");
        }
    }

    /**
     * Write the export statement, we use a flexible search so we don't export any sub types
     *
     * @param type            parent type
     * @param includeSubTypes IGNORED in current implementation
     * @throws IOException when that happens
     */
    @Override
    protected void writeExportStatement(final ComposedType type, final boolean includeSubTypes) throws IOException {
        // if we don't have a catalog item or we have one but no catalog versions to filter
        // then we use the normal exportItems method of Exporter
        if (shouldGenerateCustomExportQuery(type) || fromDate != null) {
            // we have catalog versions then we use the flexible search method
            writeBeanShell("impex.exportItemsFlexibleSearch( \"" + generateQueryForType(type) + "\")");
        } else {
            writeBeanShell("impex.exportItems( \"" + type.getCode() + "\" , " + false + " )");
        }

    }

    private boolean shouldGenerateCustomExportQuery(ComposedType type) {
        String typeCode = type.getCode();
        return (isCatalogItem(type) && (!CollectionUtils.isEmpty(catalogVersions) || !CollectionUtils.isEmpty(backofficeCatalogVersions))) || TYPE_CODE_MEDIA.equalsIgnoreCase(typeCode) || "ComposedType".equalsIgnoreCase(typeCode);
    }

    /**
     * Enhances the standard blacklisting functionality by adding a more generic blacklist of common attributes e.g.
     * creationTime
     */
    @SuppressWarnings("squid:S1066")
    protected boolean isNotIgnoreColumn(final ComposedType type, final AttributeDescriptor ad) {
        final boolean isBlacklistedAttribute = CollectionUtils.isNotEmpty(blacklistedAttributes) && blacklistedAttributes.contains(ad);
        final boolean isIgnoredColumn = super.isIgnoreColumn(type, ad.getQualifier());
        final boolean isJaloAttribute = ad.getXMLDefinition() != null && ad.getXMLDefinition().contains("persistence type=\"jalo\"");
        final boolean isDynamicAttribute = ad.getAttributeHandler() != null && !"templateScript".equalsIgnoreCase(ad.getQualifier());
        final boolean isIgnoreRelation = isIgnoreRelationColumn(ad);
        final boolean isNotReadable = !ad.isReadable();
        if (!isBlacklistedAttribute && !isDynamicAttribute && !isIgnoredColumn && !isJaloAttribute && !isIgnoreRelation) {
            if (isNotReadable) {
                writeOnlyAttributes.put(type.getCode() + ":" + ad.getQualifier(), type);
            }
        }

        final boolean ignore = isBlacklistedAttribute || isIgnoredColumn || isDynamicAttribute || isJaloAttribute || isNotReadable || isIgnoreRelation;
        if (ignore && !DONT_LOG_IGNORED_ATTRIBUTE_NAMES.contains(ad.getQualifier().toLowerCase())) {
            LOG.info("Ignoring composed type qualifier [{}:{}] because isBlacklistedAttribute:{}, isIgnoredColumn:{}, isDynamicAttribute: {}, isJaloAttribute: {}, isNotReadable: {}, isIgnoreRelation: {}", type.getCode(), ad.getQualifier(), isBlacklistedAttribute, isIgnoredColumn, isDynamicAttribute, isJaloAttribute, isNotReadable, isIgnoreRelation);
        }
        return !ignore;
    }

    /**
     * check if relation column should be ignored
     *
     * @param ad attribute descriptor
     * @return true if ad is a relation attribute and shouldIgnoreRelation is true (see {@link #shouldIgnoreRelation}))
     */
    protected boolean isIgnoreRelationColumn(final AttributeDescriptor ad) {
        if (ad instanceof RelationDescriptor) {
            RelationDescriptor rel = (RelationDescriptor) ad;
            Boolean isIgnore = shouldIgnoreRelation(rel);
            return Objects.requireNonNullElse(isIgnore, true);
        }
        return false;
    }

    boolean isUniqueAttribute(AttributeDescriptor ad) {
        return ad.isUnique() || (typesWithConfiguredUniqueAttributes.containsKey(ad.getEnclosingType()) && typesWithConfiguredUniqueAttributes.get(ad.getEnclosingType()).contains(ad));
    }

    /**
     * Test if given relation descriptor should be ignored
     *
     * @param rel relation descriptor
     * @return true if relation is optional, initial or not writable, ...
     */
    private Boolean shouldIgnoreRelation(RelationDescriptor rel) {
        RelationType relationType = rel.getRelationType();
        boolean isSourceNavigable = relationType.isSourceNavigable();
        boolean isTargetNavigable = relationType.isTargetNavigable();
        boolean isOneToOne = relationType.isSourceTypeOne() && relationType.isTargetTypeOne();
        boolean isOneToMany = relationType.isSourceTypeOne() && !relationType.isTargetTypeOne();
        boolean isManyToOne = !relationType.isSourceTypeOne() && relationType.isTargetTypeOne();
        boolean isManyToMany = !relationType.isSourceTypeOne() && !relationType.isTargetTypeOne();
        Boolean isIgnore = null;
        if (isUniqueAttribute(rel) || !rel.isOptional()) {
            isIgnore = false;
        } else if (!rel.isWritable() && !rel.isInitial()) {
            isIgnore = true;
        } else if (isManyToMany) {
            // For many to many we update both source and target to take into account removals
            isIgnore = rel.isSource() != isSourceNavigable;
        } else if (isOneToOne || isManyToOne) {
            // For one to one or many to one, we prefer the source
            isIgnore = isSourceNavigable != rel.isSource();
        } else if (isOneToMany) {
            isIgnore = isTargetNavigable == rel.isSource();
        }
        if (isIgnore == null) {
            LOG.info("isIgnoreRelationColumn evaluated to NULL - Ignoring relation {} for [{}:{}] - isSource {}, isSourceNavigable:{}, isTargetNavigable:{}, oneToOne:{}, oneToMany:{}, manyToOne:{}, manyToMany:{}", rel.getRelationName(), rel.getEnclosingType().getCode(), rel.getQualifier(), rel.isSource(), isSourceNavigable, isTargetNavigable, isOneToOne, isOneToMany, isManyToOne, isManyToMany);
        } else if (Boolean.TRUE.equals(isIgnore) && !DONT_LOG_IGNORED_ATTRIBUTE_NAMES.contains(rel.getQualifier().toLowerCase())) {
            LOG.info("Ignoring relation {} for [{}:{}] - isWritable: {}, isSource {}, isSourceNavigable:{}, isTargetNavigable:{}, oneToOne:{}, oneToMany:{}, manyToOne:{}, manyToMany:{}", rel.getRelationName(), rel.getEnclosingType().getCode(), rel.getQualifier(), rel.isWritable(), rel.isSource(), isSourceNavigable, isTargetNavigable, isOneToOne, isOneToMany, isManyToOne, isManyToMany);
        }
        return isIgnore;
    }

    protected boolean overrideRelationAsAttribute(final AttributeDescriptor ad) {
        return !relationAttribAsRelationAttributes.contains(ad);
    }

    /**
     * Override to allow relations to be set inside the type, we need this to be able to handle deletes
     * Also add a custom type level import processor to be able to skip owner, creationtime and other non-unique initial non-writable attributes when the item already exists during import
     */

    @SuppressWarnings({"S3776"}) // Hybris delivered method
    protected void writeHeader(final ComposedType type, final boolean essentialColumnsOnly) throws IOException {
        // special case USER RIGHT
        if (isUserRightType(type)) {
            getScriptWriter().writeComment("SPECIAL CASE: Type UserRight will be exported with special logic (without header definition), see https://wiki.hybris.com/x/PIFvAg");
        } else {
            // gather columns string
            final Set<String> columns = new TreeSet<>();
            boolean hasUniqueColumns = hasUniqueColumns(type);
            if (!hasUniqueColumns && isUseDocumentID() && essentialColumnsOnly) {
                // Skip first time as we don't want to insert double items
                return;
            }
            addHeaderColumns(type, columns, hasUniqueColumns, essentialColumnsOnly);
            columns.addAll(getAdditionalColumns(type));
            final Map<Integer, String> line = new HashMap<>();
            int index = 0;
            final String firstColumn = generateFirstHeaderColumn(type, hasUniqueColumns) + "[processor=de.hybris.advancedexport.jalo.impl.AdvancedImportMultiThreadedImportProcessor]";
            line.put(index, firstColumn);
            index++;

            addDocumentIdentifierToLine(hasUniqueColumns, line, index);
            index++;

            addColumnsToLine(columns, line, index);
            getScriptWriter().write(line);
        }
    }

    /**
     * Add the given set of strings to the given map of strings starting from given index
     *
     * @param columns set of columns incl. modifiers
     * @param line    a map of header lines
     * @param index   the starting position
     */
    private void addColumnsToLine(Set<String> columns, Map<Integer, String> line, int index) {
        for (final Iterator<String> iter = columns.iterator(); iter.hasNext(); index++) {
            final String column = iter.next();
            if (column.length() != 0) {
                line.put(index, column);
            }
        }
    }

    /**
     * Add either pk or document reference
     *
     * @param hasUniqueColumns flag to indicate if unique columns are present (so
     * @param line             map of columns to be written
     * @param index            current index
     */
    private void addDocumentIdentifierToLine(boolean hasUniqueColumns, Map<Integer, String> line, int index) {
        if (isUseDocumentID()) {
            line.put(index, ImpExConstants.Syntax.DOCUMENT_ID_PREFIX + "Item");
        } else {
            line.put(index,
                    Item.PK + (hasUniqueColumns ? "" : ImpExConstants.Syntax.MODIFIER_START + ImpExConstants.Syntax.Modifier.UNIQUE + ImpExConstants.Syntax.MODIFIER_EQUAL + Boolean.TRUE + ImpExConstants.Syntax.MODIFIER_END));
        }
    }

    /**
     * test if given type is an EnumerationValue
     *
     * @param type composed type
     * @return true if so, false if not
     */
    private boolean isEnumerationValueType(ComposedType type) {
        return TypeManager.getInstance().getType("EnumerationValue").isAssignableFrom(type);
    }

    /**
     * test if given type is a UserRight
     *
     * @param type composed type
     * @return true if so, false if not
     */
    private boolean isUserRightType(ComposedType type) {
        return TypeManager.getInstance().getComposedType(UserRight.class).isAssignableFrom(type);
    }

    /**
     * test if given type has unique columns
     *
     * @param type composed type
     * @return true if so, false if not
     */
    private boolean hasUniqueColumns(ComposedType type) {
        boolean hasUnique = false;
        // Find any unique attribute on the composed type
        final Collection<AttributeDescriptor> attribs = type.getAttributeDescriptorsIncludingPrivate();
        for (final AttributeDescriptor ad : attribs) {
            if (ad.isUnique() || getAdditionalModifiers(type, ad.getQualifier()).get(ImpExConstants.Syntax.Modifier.UNIQUE) != null) {
                hasUnique = true;
            }
        }
        // If we have additional configured unique attributes, still return true
        if (!hasUnique && typesWithConfiguredUniqueAttributes.containsKey(type)) {
            hasUnique = true;
        }
        return hasUnique;
    }

    /**
     * Add header columns
     *
     * @param type    composed type
     * @param columns Set of columns to fill
     */
    private void addHeaderColumns(ComposedType type, Set<String> columns, boolean hasUniqueColumns, boolean essentialOnly) {
        final Collection<AttributeDescriptor> attribs = type.getAttributeDescriptorsIncludingPrivate();
        for (final AttributeDescriptor ad : attribs) {
            if (shouldIncludeColumnInExportHeader(type, ad)) {
                if ((essentialOnly && !isEssential(ad)) || (!essentialOnly && !isUniqueAttribute(ad) && hasUniqueColumns && ad.isInitial())) {
                    // Skip for non essential || non-unique initial columns when hasUniqueColumns
                } else {
                    final String code = ad.getAttributeType().getCode();
                    if(code.equals(JAVA_LANG_DOUBLE)) {
                        super.addAdditionalModifier(ad.getEnclosingType().getCode(), ad.getQualifier(), NUMBER_FORMAT, FORMAT_DOUBLE);
                    } else if(code.equals(JAVA_LANG_LONG) || code.equals(JAVA_LANG_INTEGER)) {
                        super.addAdditionalModifier(ad.getEnclosingType().getCode(), ad.getQualifier(), NUMBER_FORMAT, FORMAT_INTEGER);
                    }
                    addOptionalModifier(type.getCode(), ad);
                    addForceWriteModifier(type.getCode(), ad);
                    addHeaderColumn(columns, ad);
                }
            }
        }
    }

    private void addHeaderColumn(Set<String> columns, AttributeDescriptor ad) {
        if (ad.isLocalized()) {
            for (final Language lang : this.getLanguages()) {
                columns.add(generateColumn(ad, lang.getIsocode()));
            }
        } else {
            columns.add(generateColumn(ad, null));
        }
    }

    private boolean isEssential(AttributeDescriptor ad) {
        return isUniqueAttribute(ad) || ad.isInitial() || !ad.isOptional();
    }

    private void addForceWriteModifier(String typeCode, AttributeDescriptor ad) {
        if (!ad.isWritable()) {
            addAdditionalModifier(typeCode, ad.getQualifier(), ImpExConstants.Syntax.Modifier.FORCE_WRITE, "true");
        }
    }

    private void addOptionalModifier(String typeCode, AttributeDescriptor ad) {
        if (!ad.isOptional()) {
            addAdditionalModifier(typeCode, ad.getQualifier(), ImpExConstants.Syntax.Modifier.ALLOWNULL, "true");
        }
    }

    private boolean shouldIncludeColumnInExportHeader(ComposedType type, AttributeDescriptor ad) {
        return isNotIgnoreColumn(type, ad) &&
                !(ad instanceof RelationDescriptor && !(ad.isProperty() || isUniqueAttribute(ad)))
                && !ad.getQualifier().equals(Item.PK)
                && (ad.isInitial() || ad.isWritable() || isUniqueAttribute(ad)) && (!ad.getQualifier().equals("itemtype") || isEnumerationValueType(type))
                // export relations as attributes
                || (isNotIgnoreColumn(type, ad) && ad instanceof RelationDescriptor && overrideRelationAsAttribute(ad));
    }

    /*
     * (non-Javadoc)
     *
     * @see de.hybris.platform.impex.jalo.exp.generator.AbstractScriptGenerator#generateScript()
     */
    @Override
    public String generateScript() {
        for (AdvancedExportModifier modifier : additionalConfiguredModifiers) {
            addAdditionalModifier(modifier.getAttributeDescriptor(), modifier.getModifier(), modifier.getValue());
        }
        for(AdvancedExportColumnExpressionReplacement replacement:columnExpressionReplacements){
            addReplacedColumnExpression(replacement.getAttributeDescriptor().getEnclosingType().getCode(), replacement.getAttributeDescriptor().getQualifier(), replacement.getColumnExpression());
        }
        //addReplacedColumnExpression("RendererTemplate", "content", "&Item")
        initTypesWithConfiguredUniqueAttributes(additionalConfiguredModifiers);
        final String result = super.generateScript();
        if (exportConfig != null) {
            exportConfig.setLastExport(new Date());
        }
        logNotNullAttributeCount(readOnlyAttributes, "READONLY");
        logNotNullAttributeCount(writeOnlyAttributes, "WRITEONLY");
        return result;
    }

    private void initTypesWithConfiguredUniqueAttributes(Set<AdvancedExportModifier> additionalConfiguredModifiers) {
        for (AdvancedExportModifier modifier : additionalConfiguredModifiers) {
            if(modifier.getModifier().equalsIgnoreCase("unique")) {
                Set<AttributeDescriptor> uniqueAttributes;
                ComposedType type = modifier.getAttributeDescriptor().getEnclosingType();
                if (!typesWithConfiguredUniqueAttributes.containsKey(type)) {
                    uniqueAttributes = new HashSet<>();
                } else {
                    uniqueAttributes = typesWithConfiguredUniqueAttributes.get(type);
                }
                uniqueAttributes.add(modifier.getAttributeDescriptor());
                typesWithConfiguredUniqueAttributes.put(type, uniqueAttributes);
            }
        }

    }

    private void logNotNullAttributeCount(Map<String, ComposedType> attributes, String attributeType) {
        if (!attributes.isEmpty()) {
            for (Map.Entry<String, ComposedType> entry : attributes.entrySet()) {
                String attrib = entry.getKey();
                String[] typeAtribute = attrib.split(":");
                int count = countNotNullValues(attributes.get(attrib), typeAtribute[1]);
                LOG.warn("{} value count for {}:{}", attributeType, attrib, count);
                if (count > 0) {
                    addManualQuery(attributes.get(attrib), typeAtribute[1]);
                }
            }
        }
    }

    private void addManualQuery(ComposedType composedType, String qualifier) {
        String columnName = composedType.getAttributeDescriptor(qualifier).getDatabaseColumn();
        List<AttributeDescriptor> uniqueKeys = composedType.getAttributeDescriptorsIncludingPrivate().stream().filter(ad -> !"PK".equalsIgnoreCase(ad.getQualifier()) && ad.isUnique()).collect(Collectors.toList());
        StringBuilder selectQuery = new StringBuilder("SELECT ").append(columnName);
        int nrOfUniqueColumns = 0;
        for (AttributeDescriptor uniqueKey : uniqueKeys) {
            selectQuery.append(",").append(uniqueKey.getDatabaseColumn());
            nrOfUniqueColumns++;
        }
        final int uniqueColumnCount = nrOfUniqueColumns;
        selectQuery.append(" FROM ").append(composedType.getTable()).append(WHERE).append(columnName).append(" is not null");

        jdbcTemplate.query(selectQuery.toString(), new RowCallbackHandler() {
                    public void processRow(ResultSet resultSet) throws SQLException {
                        while (resultSet.next()) {
                            String columnValue = resultSet.getString(1);
                            manualQueries.append("UPDATE ").append(composedType.getTable()).append(" SET ").append(columnName).append("='").append(columnValue).append("'")
                                    .append(WHERE);
                            for (int i = 0; i < uniqueColumnCount; i++) {
                                manualQueries.append(i == 0 ? " " : " AND ").append(uniqueKeys.get(i).getDatabaseColumn()).append("='").append(resultSet.getString(i + 1)).append("'");
                            }
                            manualQueries.append("\n");
                        }
                    }
                }
        );
    }

    private int countNotNullValues(ComposedType composedType, String qualifier) {
        final String query = "SELECT COUNT({" + Item.PK + "}) "
                + " FROM {" + composedType.getCode() + "!}"
                + " WHERE {" + qualifier + "} !=null";
        QueryOptions options = QueryOptions.newBuilder().withQuery(query).withResultClasses(Collections.singletonList(Integer.class)).build();
        final de.hybris.platform.jalo.SearchResult<Integer> result = FlexibleSearch.getInstance().search(options);
        return result.getResult().iterator().next();
    }

    /**
     * Check if we have any items for this type by doing a count on a flexible search
     *
     * @param type composed type to check
     * @return true if at least one result is found in the DB for the given type.
     */
    protected boolean haveItemsForType(final ComposedType type) {
        try {
            final String query = generateQueryForType(type);
            SearchResult result =  null;
//            if(fromDate != null) {
//                Map<String,Date> map = new HashMap<>();
//                map.put(Item.CREATION_TIME, fromDate);
//                map.put(Item.MODIFIED_TIME, fromDate);
//                System.out.println(map.entrySet().stream().map(i -> i.getKey() + ":" +i.getValue()).collect(Collectors.joining(",")));
//                result = FlexibleSearch.getInstance().search(query, map, type.getJaloClass());
//            } else {
                result = FlexibleSearch.getInstance().search(query, type.getJaloClass());
//            }
            return result.getCount() > 0;
        } catch (FlexibleSearchException flex) {
            LOG.error("FlexibleSearchException in haveItemsForType for type {}: {} (isJaloOnly: {})", type.getCode(), flex.getMessage(), type.isJaloOnly());
            return false;
        } catch (JaloInvalidParameterException jalex) {
            LOG.error("JaloInvalidParameterException in haveItemsForType for type {}: {} (isJaloOnly: {}; isAbstract {})", type.getCode(), jalex.getMessage(), type.isJaloOnly(), type.isAbstract());
            return false;
        } catch (Exception ex) {
            LOG.error("Error in haveItemsForType for type {}: {} (isJaloOnly: {}; isAbstract {})", type.getCode(), ex.getMessage(), type.isJaloOnly(), type.isAbstract(), ex);
            return false;
        }
    }

    private Set<CatalogVersion> getBackofficeCatalogVersions() {
        return CatalogManager.getInstance().getAllCatalogVersions().stream().filter(cv -> "_boconfig".equalsIgnoreCase(cv.getCatalog().getId())).collect(Collectors.toSet());
    }

    public String getManualQueries() {
        return manualQueries.toString();
    }

    protected boolean isCatalogItem(final ComposedType type) {
        return CatalogManager.getInstance().isCatalogItem(type);
    }

    protected AttributeDescriptor getCatalogVersionAttribute(final ComposedType type) {
        return CatalogManager.getInstance().getCatalogVersionAttribute(type);
    }
}
