    package de.hybris.advancedexport.jalo.impl;

import com.viking.core.jalo.VikingSapSalesOrganization;
import com.viking.core.model.VikingSapSalesOrganizationModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

    public class VikingSapSalesOrganizationValueTranslator extends AbstractValueTranslator {

        private static Logger LOG = Logger.getLogger(VikingSapSalesOrganizationValueTranslator.class);

        public static final String ATTRIBUTE_VALUE_SEPARATOR = ":";
        private DefaultModelService modelService;

        public boolean isEmpty(String s) {
            return StringUtils.isEmpty(s);
        }

        @Override
        public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {

            if (modelService == null) {
                modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                        "modelService");
            }

            if(StringUtils.isNotEmpty(valueExpr) && valueExpr.contains(ATTRIBUTE_VALUE_SEPARATOR)) {
                final String[] values = valueExpr.split(ATTRIBUTE_VALUE_SEPARATOR);

                final VikingSapSalesOrganizationModel example = new VikingSapSalesOrganizationModel();
                example.setCode(values[0]);
                if(values.length > 1 && !isEmpty(values[1])) {
                    example.setEmail(values[1]);
                }
                final VikingSapSalesOrganizationModel vikingSapSalesOrganization  = modelService.getByExample(example);

                if(vikingSapSalesOrganization != null) {
                    return modelService.getSource(vikingSapSalesOrganization);
                }
            }
            return null;
        }

        @Override
        public String exportValue(Object object) throws JaloInvalidParameterException {
            if(object != null) {
                final VikingSapSalesOrganization vikingSapSalesOrganization = (VikingSapSalesOrganization)object;
                final String code = vikingSapSalesOrganization.getCode();
                final String email = vikingSapSalesOrganization.getEmail();
                return  code + ATTRIBUTE_VALUE_SEPARATOR + StringUtils.defaultString(email, StringUtils.EMPTY);
            }
            return StringUtils.EMPTY;
        }
    }
