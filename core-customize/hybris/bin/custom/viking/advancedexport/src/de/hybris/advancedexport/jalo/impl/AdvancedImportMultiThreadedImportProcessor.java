package de.hybris.advancedexport.jalo.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.StandardColumnDescriptor;
import de.hybris.platform.impex.jalo.imp.MultiThreadedImportProcessor;
import de.hybris.platform.impex.jalo.imp.ValueLine;
import de.hybris.platform.impex.jalo.imp.ValueLineTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Custom import processor to allow using a custom ValueLineTranslator
 * <p>Creation Date: 04.05.20 2020</p>
 */
public class AdvancedImportMultiThreadedImportProcessor extends MultiThreadedImportProcessor {
    private static final Logger LOG = getLogger(AdvancedImportMultiThreadedImportProcessor.class);
    private ValueLineTranslator translator;
    private ConfigurationService configurationService;

    /**
     * Default constructor  - Uses config 'advancedimport.multithreadedimport.processor.maxthreads' to set maxThreads of MultiThreadedImportProcessor
     */
    public AdvancedImportMultiThreadedImportProcessor() {
        setMaxThreads(getConfigurationService().getConfiguration().getInt("advancedimport.multithreadedimport.processor.maxthreads", 4));
    }

    @Override
    protected ValueLineTranslator getValueLineTranslator(ValueLine valueLine) {
        if (this.translator == null) {
            this.translator = new AdvancedImportValueLineTranslator();
        }
        return this.translator;
    }

    @Override
    protected void processItemUpdate(Item toUpdate, Map<StandardColumnDescriptor, Object> attributeValueMappings, ValueLine valueLine) throws ImpExException {
        if (attributeValueMappings.isEmpty()) {
            LOG.debug("Nothing to update for {}.{}", toUpdate.getComposedType().getCode(), toUpdate.getPK());
            Collection<StandardColumnDescriptor> unresolvedColumns = valueLine.getUnresolved(valueLine.getHeader().getColumns().stream().filter(cd -> cd instanceof StandardColumnDescriptor).map(cd -> (StandardColumnDescriptor) cd).collect(Collectors.toList()));
            valueLine.resolve(toUpdate, unresolvedColumns);
        } else {
            Map<String, Object> attributeValues = this.translateValueMappings(toUpdate, attributeValueMappings);// 846
            this.getHandlerForLine(valueLine).update(toUpdate, attributeValues, valueLine);// 847
        }
    }

    public ConfigurationService getConfigurationService() {
        if (configurationService == null) {
            configurationService = (ConfigurationService) Registry.getApplicationContext().getBean("configurationService");
        }
        return configurationService;
    }
}
