package de.hybris.advancedexport;

import static java.io.File.separatorChar;
import static org.slf4j.LoggerFactory.getLogger;

import de.hybris.advancedexport.model.AdvancedTypeExportCronJobModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ExportConfig;
import de.hybris.platform.servicelayer.impex.ExportConfig.ValidationMode;
import de.hybris.platform.servicelayer.impex.ExportResult;
import de.hybris.platform.servicelayer.impex.ExportService;
import de.hybris.platform.servicelayer.impex.ImpExResource;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.util.CSVConstants;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * AdvancedTypeExportJob as provided by hybris and improved by us
 * @author brendan
 *
 */
public class AdvancedTypeExportJob extends AbstractJobPerformable<AdvancedTypeExportCronJobModel> {

	private ExportService exportService;

	@Autowired
	private ConfigurationService configurationService;

	private static final Logger LOG = getLogger(AdvancedTypeExportJob.class);

	@Override
	public PerformResult perform(final AdvancedTypeExportCronJobModel cron) {
		final long startTime = System.currentTimeMillis();
		final String exportString = generateExportScript(cron);
		final long durationGenerateScript =  System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("***** Exporting types ********");
			LOG.info("Using script.......");
			LOG.info(exportString);
		}

		final ImpExResource exportResource = new StreamBasedImpExResource(new ByteArrayInputStream(exportString.getBytes(StandardCharsets.UTF_8)), CSVConstants.HYBRIS_ENCODING);

		final ExportConfig exportConfig = new ExportConfig();
		exportConfig.setFailOnError(false);
		exportConfig.setValidationMode(ValidationMode.RELAXED);
		exportConfig.setScript(exportResource);
		exportConfig.setSingleFile(true);

		cron.setExportScript(exportResource.getMedia());
		modelService.save(cron);

		final long startTimeExportData = System.currentTimeMillis();
		final ExportResult result = exportService.exportData(exportConfig);
		final long durationExportData =  System.currentTimeMillis()-startTimeExportData;
		final long startTimeExportMedia = System.currentTimeMillis();
		if (result.isSuccessful()) {
			copyExportedMediaToExportDir(result);
			cron.setExportedData(result.getExportedData());
			cron.setExportedMedia(result.getExportedMedia());
			modelService.save(cron);
		} else {
			copyExportedMediaToExportDir(result);
		}
		final long durationExportMedia =  System.currentTimeMillis()-startTimeExportMedia;
		final long totalDuration = System.currentTimeMillis()-startTime;
		LOG.info("Finished import with {}, {} ms (generateScript: {}, exportData: {}, exportMedia: {})", (result.isSuccessful()?"success":"error"),  totalDuration,durationGenerateScript,durationExportData, durationExportMedia);

		return result.isSuccessful() ? new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED) : new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
	}

	/**
	 * Copy the exportResult to the local fileSystem
	 * @param result exportResult
	 */
	private void copyExportedMediaToExportDir(final ExportResult result) {
		final String exportDir = configurationService.getConfiguration().getString("advancedexport.export.dir");
		if (StringUtils.isNotBlank(exportDir)) {
			final long startTime = System.currentTimeMillis();
			final File dir = new File(exportDir + (result.isError()?"/error/":""));
			try {
				if (!dir.exists()) {
					if (!dir.mkdirs()) {
						LOG.error("Directory " + exportDir + " does not exist. Unable to create it.");
					}
				} else if (dir.isDirectory() && dir.canWrite()) {
					final Path dirPath = Paths.get(dir.getAbsolutePath());
					copyExportedMediaFile(dirPath, result.getExportedData());
					copyExportedMediaFile(dirPath, result.getExportedMedia());

				} else {
					LOG.error("Unable to write to " + exportDir + " or it is not a directory");
				}
			} catch (final IOException ioe) {
				LOG.error("Unable to copy generated script files to " + exportDir, ioe);
			}
			final long totalDuration = System.currentTimeMillis()-startTime;
			LOG.info("Completed export of media to {} in {} ms", exportDir, totalDuration);
		}
	}

	private void copyExportedMediaFile(final Path targetDir, final ImpExMediaModel impexModel) throws IOException {
		Files.copy(Paths.get(findRealMediaPath(impexModel)), targetDir.resolve(impexModel.getRealFileName()));
	}

	private String findRealMediaPath(final ImpExMediaModel impexModel) {
		return String.valueOf(configurationService.getConfiguration().getProperty("HYBRIS_DATA_DIR")) + separatorChar +
				"media" + separatorChar +
				"sys_" + impexModel.getFolder().getTenantId() +
				separatorChar + impexModel.getLocation();
	}

	/**
	 * Use a custom export script generator to dynamically create an export script
	 *
	 * @param cron the main job
	 * @return script payload
	 */
	protected String generateExportScript(final AdvancedTypeExportCronJobModel cron) {
		final AdvancedTypeExportScriptGenerator generator = new AdvancedTypeExportScriptGenerator(modelService.getSource(cron.getExportConfiguration()));
		String script =  generator.generateScript();
		cron.setManualQueries(generator.getManualQueries());
		modelService.save(cron);
		return script;
	}

	/**
	 * @param exportService
	 *           the exportService to set
	 */
	public void setExportService(final ExportService exportService) {
		this.exportService = exportService;
	}

}
