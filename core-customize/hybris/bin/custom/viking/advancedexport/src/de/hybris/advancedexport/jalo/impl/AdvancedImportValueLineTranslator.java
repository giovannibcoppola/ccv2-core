package de.hybris.advancedexport.jalo.impl;

import de.hybris.platform.impex.jalo.header.StandardColumnDescriptor;
import de.hybris.platform.impex.jalo.imp.DefaultValueLineTranslator;
import de.hybris.platform.impex.jalo.imp.InsufficientDataException;
import de.hybris.platform.impex.jalo.imp.ValueLine;
import de.hybris.platform.jalo.Item;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Custom value line translator to skip  lines after using value entry translators
 * <p>Creation Date: 04.05.20 2020</p>
 */
public class AdvancedImportValueLineTranslator extends DefaultValueLineTranslator {
    private static final Logger LOG = getLogger(AdvancedImportValueLineTranslator.class);

    @Override
    public Map<StandardColumnDescriptor, Object> translateColumnValues(Collection<StandardColumnDescriptor> columnDescriptors, ValueLine valueLine, Item existingItem) throws InsufficientDataException {
        Collection<StandardColumnDescriptor> filteredDescriptors = columnDescriptors.stream().filter(cd -> !skipColumn(existingItem, cd, valueLine)).collect(Collectors.toSet());
        return super.translateColumnValues(filteredDescriptors, valueLine, existingItem);
    }

    private boolean skipColumn(Item existingItem, StandardColumnDescriptor cd, ValueLine valueLine) {
        boolean skipIt = isExisting(existingItem) && shouldSkipExistingColumn(cd);
        if (skipIt) {
            valueLine.resolve(existingItem, Collections.singleton(cd));
        }
        return skipIt;
    }

    /**
     * initial non-writable, non-unique columns should be skipped during import or they will raize an error (especially owner & creationtime)
     *
     * @param cd a standard column descriptor
     * @return true if it should be skipped, false otherwise
     */
    private boolean shouldSkipExistingColumn(StandardColumnDescriptor cd) {
        boolean shouldSkip = cd.isInitialOnly() && !cd.isWritable() && !cd.isUnique();
        if (shouldSkip && LOG.isDebugEnabled()) {
            LOG.info("Skipping initial non-writable attribute {}:{} because item is already existing", cd.getHeader().getConfiguredComposedType().getCode(), cd.getQualifier());
        }
        return shouldSkip;
    }

    /**
     * Is the item existing?
     *
     * @param item item about to be imported
     * @return true if it exists (is not null and pk is not null)
     */
    private boolean isExisting(Item item) {
        return item != null && item.getPK() != null;
    }
}
