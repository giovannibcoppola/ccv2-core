package de.hybris.advancedexport;

import com.viking.advancedexport.model.ExtendedImpExImportJobModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.enums.ErrorMode;
import de.hybris.platform.cronjob.jalo.CronJobManager;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.hac.facade.HacCacheFacade;
import de.hybris.platform.impex.enums.ImpExValidationModeEnum;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.cronjob.AdvancedImpExImportCronJob;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportCronJob;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportJob;
import de.hybris.platform.impex.jalo.exp.ImpExExportMedia;
import de.hybris.platform.impex.model.cronjob.AdvancedImpExImportCronJobModel;
import de.hybris.platform.jalo.enumeration.EnumerationManager;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ExportConfig;
import de.hybris.platform.servicelayer.impex.ExportResult;
import de.hybris.platform.servicelayer.impex.ExportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.CSVConstants;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Advanced Import Job - Sister of AdvancedExportJob
 *
 * @version 1.0
 *
 * <p>&copy; 2020 Osudio - All rights reserved</p>
 * <p>Creation Date: 20.04.20 2020</p>
 */
public class AdvancedImpExImportJobPerformable extends AbstractJobPerformable<AdvancedImpExImportCronJobModel> {
    private static final Logger LOG = getLogger(AdvancedImpExImportJobPerformable.class);

    private JdbcTemplate jdbcTemplate;
    private ModelService modelService;
    private ImpExManager impExManager;
    private ExportService exportService;
    private HacCacheFacade hacCacheFacade;
    private EnumerationService enumerationService;

    public AdvancedImpExImportJobPerformable() {
    }

    /**
     * Constructor with impex manager (eg. for testing)
     *
     * @param impExManager       impex manager instance
     * @param exportService      exportService instance
     * @param hacCacheFacade     hacCacheFacade instance
     * @param enumerationService enumerationService instance
     */
    public AdvancedImpExImportJobPerformable(ImpExManager impExManager, ExportService exportService, HacCacheFacade hacCacheFacade, EnumerationService enumerationService) {
        this.impExManager = impExManager;
        this.exportService = exportService;
        this.hacCacheFacade = hacCacheFacade;
        this.enumerationService = enumerationService;
    }

    protected List<String> executeSaveSettingsQuery(String query) {
        LOG.info("executeSaveSettingsQuery query  {}", query);
        return jdbcTemplate.queryForList(query, String.class);
    }

    private int executeUpdateQuery(String query) {
        return jdbcTemplate.update(query);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Required
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public PerformResult perform(AdvancedImpExImportCronJobModel advancedImportCronJob) {
        long startTime = System.currentTimeMillis();
        boolean backupSuccesful = exportSettings(advancedImportCronJob);
        if (!backupSuccesful) {
            if (advancedImportCronJob.getExportData() == null) {
                LOG.error("Error exporting settings, check job logs ");
            } else {
                LOG.error("Error exporting settings, check job with code: {}", advancedImportCronJob.getExportData().getCode());
            }
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }
        /*
         * As extending from the standard ImpexImportJob and then creating a bean fails with NPE and initialization exceptions,
         * we will have to
         * * create a default importCronJob
         * * copy our settings
         * * store the importCronJob on our advancedImportCronJob
         * * execute the default importCronJob
         */
        AdvancedImpExImportCronJob jaloAdvancedImportCronJob = modelService.getSource(advancedImportCronJob);
        ExtendedImpExImportJobModel example = new ExtendedImpExImportJobModel();
        example.setCode("vikingImpex-import");
        ExtendedImpExImportJobModel impExImportJob = modelService.getByExample(example);
       ImpExImportCronJob executableJob = getImpExManager().createDefaultImpExImportCronJob(modelService.getSource(impExImportJob));
        copyImportJobSettings(jaloAdvancedImportCronJob, executableJob);
        advancedImportCronJob.setImpexCronJob(modelService.get(executableJob));
        ImpExImportCronJob executedJob = getImpExManager().importData(executableJob, true, false);
        // perform import
        boolean importJobSuccess = false;
        LOG.info("Import completed. Executing manual queries");
        try {
            if (CronJobStatus.FINISHED.getCode().equals(executedJob.getStatus().getCode()) && CronJobResult.SUCCESS.getCode().equals(executedJob.getResult().getCode())) {
                importJobSuccess = true;
                executeUpdateQueries(advancedImportCronJob.getManualQueries(), "Manual query");
            }
        } catch (Exception ex) {
            LOG.error("Error while running manual queries: {}", ex.getMessage(), ex);
        }
        LOG.info("Manual queries executed. Restoring settings");
        boolean restoreResult = restoreSettings(advancedImportCronJob);
        flushRegionCache();
        LOG.info("Finished import with {}-{} and settings result {}  in: {} ms", executedJob.getStatus().getCode(), executedJob.getResult().getCode(), restoreResult, (System.currentTimeMillis() - startTime));
        return importJobSuccess && restoreResult ? new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED) : new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
    }

    protected boolean restoreSettings(AdvancedImpExImportCronJobModel advancedImportCronJob) {
        if (advancedImportCronJob.getExportData() == null) {
            LOG.info("No settings to restore. ExportData is empty");
            return true;
        } else {
            // Create an executable job, copy settings and exported data
            AdvancedImpExImportCronJob jaloAdvancedImportCronJob = modelService.getSource(advancedImportCronJob);
            ImpExImportCronJob executableJob = getImpExManager().createDefaultImpExImportCronJob();
            ImpExExportMedia exportedData = jaloAdvancedImportCronJob.getExportData().getExportedData();
            copyImportJobSettings(jaloAdvancedImportCronJob, executableJob);
            executableJob.setErrorMode(EnumerationManager.getInstance().getEnumerationValue(ErrorMode._TYPECODE, ErrorMode.FAIL.getCode()));
            executableJob.setMode(EnumerationManager.getInstance().getEnumerationValue(ImpExValidationModeEnum._TYPECODE, ImpExValidationModeEnum.IMPORT_STRICT.getCode()));
            executableJob.setJobMedia(exportedData);
            // save export settings job on import job
            advancedImportCronJob.setImpexImportSettingsJob(modelService.get(executableJob));
            modelService.save(advancedImportCronJob);
            // execute the job
            ImpExImportCronJob executedJob = getImpExManager().importData(executableJob, true, false);
            return CronJobStatus.FINISHED.getCode().equals(executedJob.getStatus().getCode()) && CronJobResult.SUCCESS.getCode().equals(executedJob.getResult().getCode());
        }
    }

    protected boolean exportSettings(AdvancedImpExImportCronJobModel advancedImportCronJob) {

        ExportConfig exportConfig = new ExportConfig();
        String scriptData = advancedImportCronJob.getImpexExportSettingsScript();
        exportConfig.setScript(new StreamBasedImpExResource(new ByteArrayInputStream(scriptData.getBytes(StandardCharsets.UTF_8)), CSVConstants.HYBRIS_ENCODING));
        exportConfig.setSynchronous(true);
        exportConfig.setFailOnError(true);
        exportConfig.setValidationMode(ExportConfig.ValidationMode.STRICT);

        ExportResult result = getExportService().exportData(exportConfig);
        advancedImportCronJob.setExportData(result.getExport());
        modelService.save(advancedImportCronJob);
        return result.isSuccessful();
    }

    protected void flushRegionCache() {
        LOG.info("Flushing region cache");
        getHacCacheFacade().clearCache();
    }

    protected void executeUpdateQueries(Collection<String> queries, String logPrefix) {
        LOG.info("{} query count: {}", logPrefix, queries == null ? "null" : queries.size());
        if (queries != null) {
            queries.forEach(query -> {
                int count = executeUpdateQuery(query);
                LOG.info(logPrefix + " affected {} rows for query {}", count, query);
            });
        }
    }

    protected void copyImportJobSettings(AdvancedImpExImportCronJob jaloImportCronJob, ImpExImportCronJob executableJob) {
        executableJob.setMediasMedia(jaloImportCronJob.getMediasImport());
        executableJob.setJobMedia(jaloImportCronJob.getDataImport());
        executableJob.setDumpingAllowed(jaloImportCronJob.isDumpingAllowed());
        executableJob.setDumpFileEncoding(jaloImportCronJob.getDumpFileEncoding());
        executableJob.setEmailAddress(jaloImportCronJob.getEmailAddress());
        executableJob.setSendEmail(jaloImportCronJob.isSendEmail());
        executableJob.setMaxThreads(jaloImportCronJob.getMaxThreads());
        executableJob.setLegacyMode(jaloImportCronJob.isLegacyMode());
        executableJob.setSessionCurrency(jaloImportCronJob.getSessionCurrency());
        executableJob.setSessionLanguage(jaloImportCronJob.getSessionLanguage());
        executableJob.setLocale(jaloImportCronJob.getLocale());
        executableJob.setUnzipMediasMedia(jaloImportCronJob.isUnzipMediasMedia());

        executableJob.setEnableCodeExecution(jaloImportCronJob.isEnableCodeExecution());
        executableJob.setEnableExternalSyntaxParsing(jaloImportCronJob.isEnableExternalSyntaxParsing());
        executableJob.setEnableExternalCodeExecution(jaloImportCronJob.isEnableExternalCodeExecution());
        executableJob.setMode(jaloImportCronJob.getMode());
        executableJob.setErrorMode(jaloImportCronJob.getErrorMode());
    }

    public ImpExManager getImpExManager() {
        if (this.impExManager == null) {
            this.impExManager = ImpExManager.getInstance();
        }
        return this.impExManager;
    }

    public ExportService getExportService() {
        if (exportService == null) {
            exportService = (ExportService) Registry.getApplicationContext().getBean("exportService");
        }
        return exportService;
    }

    public HacCacheFacade getHacCacheFacade() {
        if (hacCacheFacade == null) {
            hacCacheFacade = (HacCacheFacade) Registry.getApplicationContext().getBean("hacCacheFacade");
        }
        return hacCacheFacade;
    }

    public EnumerationService getEnumerationService() {
        if (enumerationService == null) {
            enumerationService = (EnumerationService) Registry.getApplicationContext().getBean("enumerationService");
        }
        return enumerationService;
    }

    @Required
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setImpExManager(ImpExManager impExManager) {
        this.impExManager = impExManager;
    }
}
