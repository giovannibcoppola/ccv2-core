    package de.hybris.advancedexport.jalo.impl;

import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class OrderValueTranslator extends AbstractValueTranslator {

    private static Logger LOG = Logger.getLogger(OrderValueTranslator.class);

    public static final String ATTRIBUTE_VALUE_SEPARATOR = ":";
    private DefaultB2BOrderService b2bOrderService;
    private DefaultModelService modelService;

    public boolean isEmpty(String s) {
        return StringUtils.isEmpty(s);
    }

    @Override
    public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {
        if (b2bOrderService == null) {
            b2bOrderService = (DefaultB2BOrderService) Registry.getApplicationContext().getBean(
                    "b2bOrderService");
        }
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if(StringUtils.isNotEmpty(valueExpr) && valueExpr.contains(ATTRIBUTE_VALUE_SEPARATOR)) {
            final String[] values = valueExpr.split(ATTRIBUTE_VALUE_SEPARATOR);
            OrderModel orderModel = null;
            if(values.length > 1 && !isEmpty(values[1])) {
                final OrderModel example = new OrderModel();
                example.setCode(values[0]);
                example.setVersionID(values[1]);

                orderModel = modelService.getByExample(example);

            } else {
                orderModel = b2bOrderService.getOrderForCode(values[0]);
            }

            if(orderModel != null) {
                return modelService.getSource(orderModel);
            }
        }
        return null;
    }

    @Override
    public String exportValue(Object object) throws JaloInvalidParameterException {
        if (modelService == null) {
            modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                    "modelService");
        }

        if(object != null) {
            Order order = (Order)object;
            OrderModel orderModel = modelService.toModelLayer(object);
            final String orderCode = orderModel.getCode();
            final String versionId = orderModel.getVersionID();
            return  orderCode + ATTRIBUTE_VALUE_SEPARATOR + StringUtils.defaultString(versionId, StringUtils.EMPTY);
        }
        return StringUtils.EMPTY;
    }
}
