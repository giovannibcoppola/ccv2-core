    package de.hybris.advancedexport.jalo.impl;

import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.order.OrderEntry;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


    public class OrderEntryValueTranslator extends AbstractValueTranslator {

        private static Logger LOG = Logger.getLogger(OrderEntryValueTranslator.class);

        public static final String ATTRIBUTE_VALUE_SEPARATOR = ":";
        private DefaultB2BOrderService b2bOrderService;
        private DefaultModelService modelService;

        public boolean isEmpty(String s) {
            return StringUtils.isEmpty(s);
        }

        @Override
        public Object importValue(String valueExpr, Item item) throws JaloInvalidParameterException {
            if (b2bOrderService == null) {
                b2bOrderService = (DefaultB2BOrderService) Registry.getApplicationContext().getBean(
                        "b2bOrderService");
            }
            if (modelService == null) {
                modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                        "modelService");
            }

            if(StringUtils.isNotEmpty(valueExpr) && valueExpr.contains(ATTRIBUTE_VALUE_SEPARATOR)) {
                final String[] values = valueExpr.split(ATTRIBUTE_VALUE_SEPARATOR);
                OrderModel orderModel = null;
                if(values.length > 2 && !isEmpty(values[2])) {
                    final OrderModel example = new OrderModel();
                    example.setCode(values[1]);
                    example.setVersionID(values[2]);

                    orderModel = modelService.getByExample(example);

                } else {
                    orderModel = b2bOrderService.getOrderForCode(values[1]);
                }

                if(orderModel != null && CollectionUtils.isNotEmpty(orderModel.getEntries())) {
                    Integer entryNumber = Integer.valueOf(values[0]);
                    return orderModel.getEntries().stream()
                            .filter(entry -> entryNumber.equals(entry.getEntryNumber()))
                            .findFirst()
                            .map(entryModel -> modelService.getSource(entryModel))
                            .orElse(null);
                }
            }
            return null;
        }

        @Override
        public String exportValue(Object object) throws JaloInvalidParameterException {
            if (modelService == null) {
                modelService = (DefaultModelService) Registry.getApplicationContext().getBean(
                        "modelService");
            }
            if(object != null) {
                final OrderEntry orderEntry = (OrderEntry)object;
                final OrderEntryModel orderEntryModel = modelService.toModelLayer(orderEntry);
                final String entryNumber = orderEntryModel.getEntryNumber().toString();
                final String orderCode = orderEntryModel.getOrder().getCode();
                final String versionId = orderEntryModel.getOrder().getVersionID();
                return  entryNumber
                        + ATTRIBUTE_VALUE_SEPARATOR
                        + StringUtils.defaultString(orderCode, StringUtils.EMPTY)
                        +  ATTRIBUTE_VALUE_SEPARATOR
                        + StringUtils.defaultString(versionId, StringUtils.EMPTY) ;
            }
            return StringUtils.EMPTY;
        }
    }
