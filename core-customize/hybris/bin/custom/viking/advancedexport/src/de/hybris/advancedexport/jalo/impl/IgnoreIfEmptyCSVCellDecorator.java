package de.hybris.advancedexport.jalo.impl;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Decorator for localized relational attributes
 * Eg. RendererTemplate has a content attribute with a CatalogVersion and a language. However, this catalogVersion is optional and causes error during re-import
 * <p>Creation Date: 06.05.20 2020</p>
 */
public class IgnoreIfEmptyCSVCellDecorator extends AbstractImpExCSVCellDecorator {
    @Override
    public String decorate(int position, Map<Integer, String> valueLine) {
        String parsedValue = valueLine.get(position);
        return StringUtils.isBlank(parsedValue) ? "<ignore>" : parsedValue;
    }
}
