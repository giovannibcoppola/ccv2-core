package de.hybris.platform.impex.jalo;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.cronjob.DefaultCronJobMediaDataHandler;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportCronJob;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.DefaultConfigurationService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.MediaUtil;
import de.hybris.platform.util.zip.SafeZipEntry;
import de.hybris.platform.util.zip.SafeZipInputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class VikingCronJobMediaDataHandler extends DefaultCronJobMediaDataHandler {

    private static final Logger LOG = Logger.getLogger(VikingCronJobMediaDataHandler.class.getName());
    private ImpExImportCronJob impCronJob = null;
    private final String MIGRATION_FOLDER = "migratiton.media.folder";
    private final String MIGRATION_FILENAME = "migratiton.media.file.name";
    private ConfigurationService configurationService;

    public VikingCronJobMediaDataHandler(ImpExImportCronJob cronJob) {
        super(cronJob);
        this.impCronJob = cronJob;
    }

    @Override
    protected File unzipMediasMedia(Media sourceMedia, String normalizedLocation) {
        if (configurationService == null) {
            configurationService = (DefaultConfigurationService) Registry.getApplicationContext().getBean(
                    "configurationService");
        }

        LOG.info("source file name: " + sourceMedia.getRealFileName());
        String migrationFileName = configurationService.getConfiguration().getString(MIGRATION_FILENAME);
        LOG.info("migration file name: " + migrationFileName);
        String migrationFolderPath = configurationService.getConfiguration().getString(MIGRATION_FOLDER);
        LOG.info("migration folder path: " + migrationFolderPath);

        if (sourceMedia.getRealFileName().equals(migrationFileName)) {
            File folder = new File(migrationFolderPath);
            LOG.info("folder exist: " + folder.exists() + " and folder path: " + folder.getAbsolutePath());
            return folder;
        } else {
           return super.unzipMediasMedia(sourceMedia, normalizedLocation);
        }
    }
}
