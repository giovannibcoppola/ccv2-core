/*
 *  
 * [y] hybris Platform
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.viking.advancedexport.jalo;

import de.hybris.platform.core.Tenant;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.directpersistence.LegacyFlagsUtils;
import de.hybris.platform.directpersistence.annotation.ForceJALO;
import de.hybris.platform.hmc.jalo.SavedValues;
import de.hybris.platform.impex.jalo.*;
import de.hybris.platform.impex.jalo.cronjob.CronJobDumpHandler;
import de.hybris.platform.impex.jalo.cronjob.CronJobErrorHandler;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportCronJob;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportJob;
import de.hybris.platform.impex.jalo.imp.*;
import de.hybris.platform.impex.jalo.media.MediaDataTranslator;
import de.hybris.platform.jalo.*;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

public class ExtendedImpExImportJob extends GeneratedExtendedImpExImportJob
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( ExtendedImpExImportJob.class.getName() );
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem( ctx, type, allAttributes );
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}



	public CronJob.CronJobResult performCronJob(CronJob cronJob) throws AbortCronJobException {
		boolean success = false;
		ImpExImportCronJob importCronJob = null;
		if (!(cronJob instanceof ImpExImportCronJob)) {
			throw new AbortCronJobException("Given cronjob is not instance of ImpExImportCronJob");
		} else {
			importCronJob = (ImpExImportCronJob)cronJob;
			Language sessionLanguage = null;

			try {
				sessionLanguage = this.getSession().getSessionContext().getLanguage();
				MediaDataTranslator.setMediaDataHandler(new VikingCronJobMediaDataHandler(importCronJob));
				success = this.performJob(importCronJob);
			} catch (AbortCronJobException var17) {
				throw var17;
			} catch (RuntimeException var18) {
				LOG.error("Error while performing import: " + var18.getMessage(), var18);
				success = false;
			} catch (Exception var19) {
				LOG.error("Error while performing import: " + var19.getMessage());
				if (LOG.isDebugEnabled()) {
					LOG.debug("Error while performing import: " + var19.getMessage(), var19);
				}

				success = false;
			} finally {
				try {
					this.getSession().getSessionContext().setLanguage(sessionLanguage);
				} catch (Exception var16) {
					LOG.error("Exception while restoring previous session language: " + var16.getMessage());
				}

				MediaDataTranslator.unsetMediaDataHandler();
			}

			if (success) {
				this.cleanup(importCronJob);
			}

			return importCronJob.getFinishedResult(success);
		}
	}

}
