package de.hybris.advancedexport.jalo.impl;

import de.hybris.advancedexport.jalo.impl.OrderEntryValueTranslator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.order.OrderEntry;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
public class OrderEntryValueTranslatorTest {

    @InjectMocks
    private OrderEntryValueTranslator orderEntryValueTranslator;

    @Mock
    private DefaultB2BOrderService b2bOrderService;
    @Mock
    private DefaultModelService modelService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_withBothCodeAndVersionID() throws JaloBusinessException {
        final Item orderEntryCancelRecordEntry = mock(Item.class);
        final String cellValue = "0:001000:1";
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderEntry orderEntry = mock(OrderEntry.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);

        when(modelService.getByExample(any(OrderModel.class))).thenReturn(orderModel);
        when(modelService.getSource(orderEntryModel)).thenReturn(orderEntry);
        when(orderModel.getEntries()).thenReturn(Arrays.asList(orderEntryModel));
        when(orderEntryModel.getEntryNumber()).thenReturn(0);

        final Object result = orderEntryValueTranslator.importValue(cellValue, orderEntryCancelRecordEntry);

        assertEquals(orderEntry, result);

    }

    @Test
    public void testImport_Order_withOnlyCode() throws JaloBusinessException {
        final Item orderEntryCancelRecordEntry = mock(Item.class);
        final String cellValue = "0:001000:";
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderEntry orderEntry = mock(OrderEntry.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);

        when(b2bOrderService.getOrderForCode(eq("001000"))).thenReturn(orderModel);
        when(modelService.getSource(orderEntryModel)).thenReturn(orderEntry);
        when(orderModel.getEntries()).thenReturn(Arrays.asList(orderEntryModel));
        when(orderEntryModel.getEntryNumber()).thenReturn(0);

        final Object result = orderEntryValueTranslator.importValue(cellValue, orderEntryCancelRecordEntry);

        assertEquals(orderEntry, result);
    }

    @Test
    public void testExport_withBothCodeAndVersionID() throws JaloBusinessException {
        final String cellValue = "0:001000:1";
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderEntry orderEntry = mock(OrderEntry.class);

        when(modelService.toModelLayer(orderEntry)).thenReturn(orderEntryModel);
        when(orderEntryModel.getEntryNumber()).thenReturn(0);
        when(orderEntryModel.getOrder()).thenReturn(orderModel);
        when(orderModel.getCode()).thenReturn("001000");
        when(orderModel.getVersionID()).thenReturn("1");

        final String result = orderEntryValueTranslator.exportValue(orderEntry);

        assertEquals(cellValue,result);
    }

    @Test
    public void testExport_withOnlyCode() throws JaloBusinessException {
        final String cellValue = "0:001000:";
        final OrderModel orderModel = mock(OrderModel.class);
        final OrderEntry orderEntry = mock(OrderEntry.class);
        final OrderEntryModel orderEntryModel = mock(OrderEntryModel.class);

        when(modelService.toModelLayer(orderEntry)).thenReturn(orderEntryModel);
        when(orderEntryModel.getEntryNumber()).thenReturn(0);
        when(orderEntryModel.getOrder()).thenReturn(orderModel);
        when(orderModel.getCode()).thenReturn("001000");

        final String result = orderEntryValueTranslator.exportValue(orderEntry);

        assertEquals(cellValue,result);
    }
}
