package de.hybris.advancedexport;

import com.viking.core.jalo.VikingSapSalesOrganization;
import com.viking.core.model.VikingSapSalesOrganizationModel;
import de.hybris.advancedexport.jalo.impl.VikingSapSalesOrganizationValueTranslator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@UnitTest
public class VikingSapSalesOrganizationValueTranslatorTest {

    @InjectMocks
    private VikingSapSalesOrganizationValueTranslator VikingSapSalesOrganizationValueTranslator;

    @Mock
    private DefaultB2BOrderService b2bOrderService;
    @Mock
    private DefaultModelService modelService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_Order_withBothCodeAndEmail() throws JaloBusinessException {
        final Item item = mock(Item.class);
        final String cellValue = "001000:a@b.com";
        final VikingSapSalesOrganizationModel vikingSapSalesOrganizationModel = mock(VikingSapSalesOrganizationModel.class);
        final VikingSapSalesOrganization vikingSapSalesOrganization = mock(VikingSapSalesOrganization.class);

        when(modelService.getByExample(any(VikingSapSalesOrganizationModel.class))).thenReturn(vikingSapSalesOrganizationModel);
        when(modelService.getSource(vikingSapSalesOrganizationModel)).thenReturn(vikingSapSalesOrganization);

        final Object result = VikingSapSalesOrganizationValueTranslator.importValue(cellValue, item);
        assertEquals(vikingSapSalesOrganization, result);
    }

    @Test
    public void testImport_Order_withOnlyCode() throws JaloBusinessException {
        final Item item = mock(Item.class);
        final String cellValue = "001000:";
        final VikingSapSalesOrganizationModel vikingSapSalesOrganizationModel = mock(VikingSapSalesOrganizationModel.class);
        final VikingSapSalesOrganization vikingSapSalesOrganization = mock(VikingSapSalesOrganization.class);

        when(modelService.getByExample(any(VikingSapSalesOrganizationModel.class))).thenReturn(vikingSapSalesOrganizationModel);
        when(modelService.getSource(vikingSapSalesOrganizationModel)).thenReturn(vikingSapSalesOrganization);

        VikingSapSalesOrganizationValueTranslator.importValue(cellValue, item);

        final Object result = VikingSapSalesOrganizationValueTranslator.importValue(cellValue, item);
        assertEquals(vikingSapSalesOrganization, result);
    }

    @Test
    public void testExport_Order_withBothCodeAndEmail() throws JaloBusinessException {
        final String cellValue = "001000:a@b.com";
        final VikingSapSalesOrganization vikingSapSalesOrganization = mock(VikingSapSalesOrganization.class);

        when(vikingSapSalesOrganization.getCode()).thenReturn("001000");
        when(vikingSapSalesOrganization.getEmail()).thenReturn("a@b.com");

        final String result = VikingSapSalesOrganizationValueTranslator.exportValue(vikingSapSalesOrganization);

        assertEquals(cellValue,result);
    }

    @Test
    public void testExport_Order_withOnlyCode() throws JaloBusinessException {
        final String cellValue = "001000:";
        final VikingSapSalesOrganization vikingSapSalesOrganization = mock(VikingSapSalesOrganization.class);

        when(vikingSapSalesOrganization.getCode()).thenReturn("001000");

        final String result = VikingSapSalesOrganizationValueTranslator.exportValue(vikingSapSalesOrganization);

        assertEquals(cellValue,result);
    }
}
