package de.hybris.advancedexport.jalo.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.advancedexport.jalo.impl.OrderValueTranslator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.services.impl.DefaultB2BOrderService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.order.OrderEntry;
import de.hybris.platform.servicelayer.internal.model.impl.DefaultModelService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@UnitTest
public class OrderValueTranslatorTest {

    @InjectMocks
    private OrderValueTranslator orderValueTranslator;

    @Mock
    private DefaultB2BOrderService b2bOrderService;
    @Mock
    private DefaultModelService modelService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testImport_Order_withBothCodeAndVersionID() throws JaloBusinessException {
        final Item orderEntry = mock(Item.class);
        final String cellValue = "001000:1";
        final OrderModel orderModel = mock(OrderModel.class);
        final Order order = mock(Order.class);

        when(modelService.getByExample(any(OrderModel.class))).thenReturn(orderModel);
        when(modelService.getSource(orderModel)).thenReturn(order);

        final Object result = orderValueTranslator.importValue(cellValue, orderEntry);

        assertEquals(order, result);

    }

    @Test
    public void testImport_Order_withOnlyCode() throws JaloBusinessException {
        final OrderEntry orderEntry = mock(OrderEntry.class);
        final String cellValue = "001000:";
        final OrderModel orderModel = mock(OrderModel.class);
        final Order order = mock(Order.class);

        when(b2bOrderService.getOrderForCode(eq("001000"))).thenReturn(orderModel);
        when(modelService.getSource(orderModel)).thenReturn(order);

        final Object result = orderValueTranslator.importValue(cellValue, orderEntry);

        assertEquals(order, result);
    }

    @Test
    public void testExport_Order_withBothCodeAndVersionID() throws JaloBusinessException {
        final String cellValue = "001000:1";
        final OrderModel orderModel = mock(OrderModel.class);
        final Order order = mock(Order.class);

        when(modelService.toModelLayer(order)).thenReturn(orderModel);
        when(orderModel.getCode()).thenReturn("001000");
        when(orderModel.getVersionID()).thenReturn("1");

        final String result = orderValueTranslator.exportValue(order);

        assertEquals(cellValue,result);
    }

    @Test
    public void testExport_Order_withOnlyCode() throws JaloBusinessException {
        final String cellValue = "001000:";
        final OrderModel orderModel = mock(OrderModel.class);
        final Order order = mock(Order.class);

        when(modelService.toModelLayer(order)).thenReturn(orderModel);
        when(orderModel.getCode()).thenReturn("001000");

        final String result = orderValueTranslator.exportValue(order);

        assertEquals(cellValue,result);
    }
}
