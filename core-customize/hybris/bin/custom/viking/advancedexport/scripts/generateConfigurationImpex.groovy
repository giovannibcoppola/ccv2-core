

import de.hybris.platform.catalog.model.SyncItemJobModel
import de.hybris.platform.cmsfacades.data.StructureTypeCategory

//INSERT_UPDATE AdvancedTypeExportConfiguration;code[unique=true];catalogVersionsToExport(catalog(id),version);languages(isocode);typesToExport(code);blacklistedTypes(code);
//;export-CMS-Staged;apparel-ukContentCatalog:Staged;en;CMSSite,AbstractCMSComponent,PageTemplate, ContentSlot, ContentSlotForTemplate, ContentSlotForPage, AbstractPage, Media, MediaContainer, AbstractRestriction, CMSNavigationNode;CatalogVersionSyncScheduleMedia,HeaderLibrary,LogFile,ImpExMedia
def languages = [] as Set
def catalogVersions = [] as Set
def blackListTypes = ['CatalogVersionSyncScheduleMedia','HeaderLibrary','LogFile','ImpExMedia'] as Set
def exportedTypes = [] as Set
def blackListedAttributes = ['Item:allDocuments','Item:savedValues','Media:URL','Media:URL2','PageTemplate:availableContentSlots','Item:assignedCockpitItemTemplates'] as Set


println "#---------"
println "#Catalog Version Data"
println "#---------"

println "INSERT_UPDATE AdvancedTypeExportConfiguration;code[unique=true];catalogVersionsToExport(catalog(id),version);languages(isocode);typesToExport(code);blacklistedTypes(code);blacklistedAttributes(enclosingType(code),qualifier);"
//ContentCatalog
print ";export-ContentCatalogs;"

spring.getBean("catalogService").getAllCatalogs().each { c ->
    if(c.getId().contains("Content")) {
        c.getCatalogVersions().each { cv ->
            catalogVersions.add(c.getId() + ':' + cv.getVersion() )
        }
    }
    c.getLanguages().each { l ->
        languages.add(l.getIsocode())
    }
}
print catalogVersions.join(',')
print ";"
print languages.join(',')
print ";"
spring.getBean("componentTypeFacade").getAllCmsItemTypes(Arrays.asList(StructureTypeCategory.values()), false).each { comp ->
    print comp.code + ','
    exportedTypes.add(comp.code)
}
println ';' + blackListTypes.join(',') + ';' + blackListedAttributes.join(',')
//ProductCatalog
catalogVersions = [] as Set
languages = [] as Set
print ";export-ProductCatalog;"
// Export ProductCatalog
spring.getBean("catalogService").getAllCatalogs().each { c ->
    if(c.getId().contains("Product")) {
        c.getCatalogVersions().each { cv ->
            catalogVersions.add(c.getId() + ':' + cv.getVersion() )
        }
    }
    c.getLanguages().each { l ->
        languages.add(l.getIsocode())
    }
}
print catalogVersions.join(',')
print ";"
print languages.join(',')
print ";"
spring.getBean("catalogService").getAllCatalogs().each { c ->
    if(c.getId().contains("Product")) {
        c.getCatalogVersions().each { cv ->
            if(cv.getVersion()=='Staged'){
                def latestSyncItemJobModel = cv.getSynchronizations().get(0);
                latestSyncItemJobModel.getRootTypes().each { rt->
                    print rt.getCode() + ',';
                    exportedTypes.add(rt.code)
                }
            }
        }

    }
}
println ';' + blackListTypes.join(',') + ';' + blackListedAttributes.join(',')
//ClassificationCatalog
catalogVersions = [] as Set
languages = ['de','en','da','se','no','us'] as Set
def classificationItemTypes = ['ClassificationClass','ClassificationAttribute','ClassAttributeAssignment','ClassificationAttributeValue','ClassificationAttributeUnit'] as Set
print ";export-ClassificationCatalog;"
spring.getBean("catalogService").getAllCatalogs().each { c ->
    if(c.getId().contains("Classification")) {
        c.getCatalogVersions().each { cv ->
            catalogVersions.add(c.getId() + ':' + cv.getVersion() )
        }
        c.getLanguages().each { l ->
            languages.add(l.getIsocode())
        }

    }
}
print catalogVersions.join(',')
print ";"
print languages.join(',')
print ";"
print classificationItemTypes.join(',')
println ';' + blackListTypes.join(',') + ';' + blackListedAttributes.join(',')
exportedTypes.addAll(classificationItemTypes)

println "#---------"
println "## Content Catalog ItemTypes based on syncronization configuration"
print "##"
spring.getBean("catalogService").getAllCatalogs().each { c ->
    if(c.getId().contains("Content")) {
        c.getCatalogVersions().each { cv ->
            if(cv.getVersion()=='Staged'){
                def latestSyncItemJobModel = cv.getSynchronizations().get(0);
                latestSyncItemJobModel.getRootTypes().each { rt->
                    print rt.getCode() + ',';
                    //exportedTypes.add(rt.code)
                }
            }
        }

    }
}
println ""
println "#---------"
println "#Non Catalog Version Data"
println "#---------"


languages = ['de','en','da','se','no','us'] as Set
catalogVersions = [] as Set
def nonCatalogBlackListTypes = ['CatalogOverview'] as Set
nonCatalogBlackListTypes.addAll(blackListTypes)
nonCatalogBlackListTypes.addAll(exportedTypes)
println "INSERT_UPDATE AdvancedTypeExportConfiguration;code[unique=true];languages(isocode);typesToExport(code);blacklistedTypes(code);blacklistedAttributes(enclosingType(code),qualifier);"
println ';export-NonCatalogData;' + languages.join(',') + ';Item,Link;'+ nonCatalogBlackListTypes.join(',') + ';' + blackListedAttributes.join(',')

println "#---------"
println "#Export Job configs"
println "#---------"
println "INSERT_UPDATE ServicelayerJob; code[unique = true]       ; springId"
println "; advancedTypeExportJob ; advancedTypeExportJob"
println "INSERT_UPDATE AdvancedTypeExportCronJob;code[unique=true];job(code);exportConfiguration(code);sessionLanguage(isocode)"
println ";export-ContentCatalogs-CronJob;advancedTypeExportJob;export-ContentCatalogs;en;"
println ";export-ProductCatalog-CronJob;advancedTypeExportJob;export-ProductCatalog;en;"
println ";export-ClassificationCatalog-CronJob;advancedTypeExportJob;export-ClassificationCatalog;en;"
println ";export-NonCatalogData-CronJob;advancedTypeExportJob;export-NonCatalogData;en;"

return "Done. Check output tab."