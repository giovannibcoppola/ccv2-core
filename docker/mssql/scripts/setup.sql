IF DB_ID('hybris') IS NOT NULL
	PRINT N'SETUP: hybris db exists. Skipping setup.';  
GO

IF DB_ID('hybris') IS NOT NULL
  set noexec on               -- prevent creation when already exists


PRINT N'SETUP: Creating DB hybris.';  
create database hybris;
GO

use hybris;
GO

PRINT N'SETUP: Creating login hybris.';  
create login hybris with password='Hybr1sUs3r(!)';
GO

PRINT N'SETUP: Creating user hybris.';  
create user hybris for login hybris;
GO

grant control to hybris;
GO

ALTER DATABASE hybris SET READ_COMMITTED_SNAPSHOT ON;
ALTER DATABASE hybris SET ALLOW_SNAPSHOT_ISOLATION ON;
GO
set noexec off               -- resume execution for current session
