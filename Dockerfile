FROM openjdk:11-jdk
MAINTAINER jo.g@osudio.com
RUN mkdir -p /opt/hybris && useradd  -U -d /opt/hybris -u 1000 -s /bin/bash hybris
COPY hybris-wrap.sh /opt/
COPY tail-log.sh /opt/
RUN apt-get update && apt-get install -y unzip augeas-tools bash lsof && \
    	chmod +x /opt/*.sh && chown -R hybris /opt/hybris && \
        wget -O /usr/local/bin/gosu https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64 && \
	chmod +x /usr/local/bin/gosu
COPY env.awk /opt/
WORKDIR /opt/
VOLUME ["/opt/hybris"]
EXPOSE 8000 8001 8009 8010 9001 9002 1099
ENTRYPOINT ["/opt/hybris-wrap.sh"]
CMD ["run"]


