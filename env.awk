BEGIN {
	print "set /augeas/load/Properties/lens Properties.lns"
	print "set /augeas/load/Properties/incl /opt/hybris/config/local.properties"
	print "load"
    FS = "=" 
}
{

    v = $2;
    j=$2; for (i=3;i<=NF;i++) {
        v = v "=" $i
    }
    FS="_"; $0 = $1;
    k=$2; for (i=3;i<=NF;i++) {
        if (length($i) > 0) {
            if (!skip) {
                k = k "." $i;
            } else {
                k = k "_" $i;
            }
        } else { skip = 1; }
    }
    print "set /files/opt/hybris/config/local.properties/"k " " v;
    FS = "=";
    skip = 0;
}
END {print "save"}
